Untitled
- Delay -
active: false
- Duration - 
lowMin: 2000.0
lowMax: 2000.0
- Count - 
min: 0
max: 280
- Emission - 
lowMin: 0.0
lowMax: 0.0
highMin: 100.0
highMax: 100.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Life - 
lowMin: 0.0
lowMax: 0.0
highMin: 600.0
highMax: 600.0
relative: false
scalingCount: 3
scaling0: 1.0
scaling1: 1.0
scaling2: 1.0
timelineCount: 3
timeline0: 0.0
timeline1: 0.6849315
timeline2: 1.0
- Life Offset - 
active: false
- X Offset - 
active: false
- Y Offset - 
active: false
- Spawn Shape - 
shape: point
- Spawn Width - 
lowMin: 0.0
lowMax: 0.0
highMin: 0.0
highMax: 0.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Spawn Height - 
lowMin: 0.0
lowMax: 0.0
highMin: 0.0
highMax: 0.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- X Scale - 
lowMin: 0.0
lowMax: 0.0
highMin: 32.0
highMax: 32.0
relative: false
scalingCount: 2
scaling0: 1.0
scaling1: 0.25490198
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Velocity - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 201.0
highMax: 30.0
relative: false
scalingCount: 2
scaling0: 1.0
scaling1: 0.98039216
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Angle - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 360.0
highMax: 1.0
relative: false
scalingCount: 4
scaling0: 1.0
scaling1: 1.0
scaling2: 1.0
scaling3: 1.0
timelineCount: 4
timeline0: 0.0
timeline1: 0.34246576
timeline2: 0.760274
timeline3: 1.0
- Rotation - 
active: false
- Wind - 
active: false
- Gravity - 
active: false
- Tint - 
colorsCount: 9
colors0: 0.6117647
colors1: 0.015686275
colors2: 0.015686275
colors3: 0.72156864
colors4: 0.015686275
colors5: 0.015686275
colors6: 0.52156866
colors7: 0.0
colors8: 0.0
timelineCount: 3
timeline0: 0.0
timeline1: 0.99
timeline2: 1.0
- Transparency - 
lowMin: 0.0
lowMax: 0.0
highMin: 1.0
highMax: 1.0
relative: false
scalingCount: 4
scaling0: 0.10526316
scaling1: 0.6315789
scaling2: 1.0
scaling3: 1.0
timelineCount: 4
timeline0: 0.0
timeline1: 0.5958904
timeline2: 0.89041096
timeline3: 1.0
- Options - 
attached: false
continuous: false
aligned: false
additive: false
behind: false
premultipliedAlpha: false
spriteMode: single
simple.png

