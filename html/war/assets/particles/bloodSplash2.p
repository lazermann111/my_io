outer
- Delay -
active: false
- Duration - 
lowMin: 300.0
lowMax: 300.0
- Count - 
min: 0
max: 50
- Emission - 
lowMin: 500.0
lowMax: 500.0
highMin: 1000.0
highMax: 1000.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Life - 
lowMin: 500.0
lowMax: 500.0
highMin: 1400.0
highMax: 2000.0
relative: false
scalingCount: 2
scaling0: 0.0
scaling1: 1.0
timelineCount: 2
timeline0: 0.0
timeline1: 0.9794521
- Life Offset - 
active: false
- X Offset - 
active: false
- Y Offset - 
active: false
- Spawn Shape - 
shape: point
- Spawn Width - 
lowMin: 0.0
lowMax: 0.0
highMin: 0.0
highMax: 0.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Spawn Height - 
lowMin: 0.0
lowMax: 0.0
highMin: 0.0
highMax: 0.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- X Scale - 
lowMin: 16.0
lowMax: 16.0
highMin: 32.0
highMax: 32.0
relative: false
scalingCount: 5
scaling0: 0.0
scaling1: 1.0
scaling2: 0.6938776
scaling3: 0.7346939
scaling4: 0.0
timelineCount: 5
timeline0: 0.0
timeline1: 0.19178082
timeline2: 0.42465752
timeline3: 0.6643836
timeline4: 1.0
- Velocity - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 200.0
highMax: 400.0
relative: false
scalingCount: 3
scaling0: 1.0
scaling1: 0.3469388
scaling2: 0.1632653
timelineCount: 3
timeline0: 0.0
timeline1: 0.20547946
timeline2: 0.7808219
- Angle - 
active: true
lowMin: 1.0
lowMax: 360.0
highMin: -180.0
highMax: 180.0
relative: false
scalingCount: 2
scaling0: 0.0
scaling1: 0.12244898
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Rotation - 
active: false
- Wind - 
active: true
lowMin: 40.0
lowMax: -40.0
highMin: -90.0
highMax: 90.0
relative: false
scalingCount: 2
scaling0: 0.0
scaling1: 1.0
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Gravity - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 0.0
highMax: -300.0
relative: false
scalingCount: 2
scaling0: 0.0
scaling1: 0.97959185
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Tint - 
colorsCount: 3
colors0: 0.8862745
colors1: 0.0
colors2: 0.0
timelineCount: 1
timeline0: 0.0
- Transparency - 
lowMin: 0.0
lowMax: 0.0
highMin: 1.0
highMax: 1.0
relative: false
scalingCount: 3
scaling0: 0.24561404
scaling1: 1.0
scaling2: 0.0
timelineCount: 3
timeline0: 0.0
timeline1: 0.42465752
timeline2: 1.0
- Options - 
attached: false
continuous: false
aligned: false
additive: true
behind: false
premultipliedAlpha: false
spriteMode: single
simple.png

