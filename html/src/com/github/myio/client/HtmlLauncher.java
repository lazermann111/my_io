package com.github.myio.client;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.backends.gwt.GwtApplication;
import com.badlogic.gdx.backends.gwt.GwtApplicationConfiguration;
import com.github.czyzby.websocket.GwtWebSockets;
import com.github.myio.GameClient;
import com.google.gwt.event.logical.shared.ResizeEvent;
import com.google.gwt.event.logical.shared.ResizeHandler;
import com.google.gwt.user.client.Window;

public class HtmlLauncher extends GwtApplication {
//    @Override
//    public GwtApplicationConfiguration getConfig() {
//
//        GwtApplicationConfiguration c = new GwtApplicationConfiguration(1920,1080);
//        c.preferFlash = true;
//        return c;
//
//    }
     private static final int PADDING = 0;
     private GwtApplicationConfiguration cfg;

     @Override
     public GwtApplicationConfiguration getConfig() {
         int w = Window.getClientWidth() - PADDING;
         int h = Window.getClientHeight() - PADDING;
         cfg = new GwtApplicationConfiguration(w, h);
         Window.enableScrolling(false);
         Window.setMargin("0");
         Window.addResizeHandler(new ResizeListener());
         cfg.preferFlash = false;
         return cfg;
     }

     class ResizeListener implements ResizeHandler {
         @Override
         public void onResize(ResizeEvent event) {
             int width = event.getWidth() - PADDING;
             int height = event.getHeight() - PADDING;
             getRootPanel().setWidth("" + width + "px");
             getRootPanel().setHeight("" + height + "px");
             getApplicationListener().resize(width, height);
             Gdx.graphics.setWindowedMode(width, height);
         }
     }
    @Override
    public ApplicationListener createApplicationListener() {
        // Initiating web sockets module:
        GwtWebSockets.initiate();
        return new GameClient();
    }
}