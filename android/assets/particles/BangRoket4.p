Untitled
- Delay -
active: false
- Duration - 
lowMin: 500.0
lowMax: 500.0
- Count - 
min: 0
max: 1000
- Emission - 
lowMin: 0.0
lowMax: 0.0
highMin: 500.0
highMax: 500.0
relative: false
scalingCount: 2
scaling0: 1.0
scaling1: 1.0
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Life - 
lowMin: 0.0
lowMax: 0.0
highMin: 1000.0
highMax: 1000.0
relative: false
scalingCount: 3
scaling0: 1.0
scaling1: 1.0
scaling2: 0.3
timelineCount: 3
timeline0: 0.0
timeline1: 0.66
timeline2: 1.0
- Life Offset - 
active: false
- X Offset - 
active: false
- Y Offset - 
active: false
- Spawn Shape - 
shape: point
- Spawn Width - 
lowMin: 0.0
lowMax: 0.0
highMin: 0.0
highMax: 0.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Spawn Height - 
lowMin: 0.0
lowMax: 0.0
highMin: 0.0
highMax: 0.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- X Scale - 
lowMin: 0.0
lowMax: 0.0
highMin: 300.0
highMax: 0.0
relative: false
scalingCount: 3
scaling0: 0.33333334
scaling1: 0.3529412
scaling2: 0.9607843
timelineCount: 3
timeline0: 0.0
timeline1: 0.25342464
timeline2: 0.9794521
- Velocity - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 400.0
highMax: 0.0
relative: false
scalingCount: 4
scaling0: 1.0
scaling1: 1.0
scaling2: 0.09803922
scaling3: 0.0
timelineCount: 4
timeline0: 0.0
timeline1: 0.5
timeline2: 0.5273973
timeline3: 0.9794521
- Angle - 
active: true
lowMin: 1.0
lowMax: 360.0
highMin: 45.0
highMax: 135.0
relative: false
scalingCount: 3
scaling0: 1.0
scaling1: 0.039215688
scaling2: 0.0
timelineCount: 3
timeline0: 0.0
timeline1: 0.01369863
timeline2: 1.0
- Rotation - 
active: false
- Wind - 
active: false
- Gravity - 
active: false
- Tint - 
colorsCount: 12
colors0: 1.0
colors1: 0.0
colors2: 0.0
colors3: 0.40392157
colors4: 0.1764706
colors5: 0.0
colors6: 0.19215687
colors7: 0.08235294
colors8: 0.0
colors9: 1.0
colors10: 1.0
colors11: 1.0
timelineCount: 4
timeline0: 0.0
timeline1: 0.17710944
timeline2: 0.5413534
timeline3: 1.0
- Transparency - 
lowMin: 0.0
lowMax: 0.0
highMin: 1.0
highMax: 1.0
relative: false
scalingCount: 4
scaling0: 0.0
scaling1: 1.0
scaling2: 0.75
scaling3: 0.0
timelineCount: 4
timeline0: 0.0
timeline1: 0.2
timeline2: 0.8
timeline3: 1.0
- Options - 
attached: true
continuous: false
aligned: false
additive: true
behind: false
premultipliedAlpha: false
spriteMode: single
simple.png
