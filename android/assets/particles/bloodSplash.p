inner
- Delay -
active: false
- Duration - 
lowMin: 500.0
lowMax: 500.0
- Count - 
min: 0
max: 100
- Emission - 
lowMin: 5000.0
lowMax: 5000.0
highMin: 10000.0
highMax: 10000.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Life - 
lowMin: 500.0
lowMax: 500.0
highMin: 500.0
highMax: 700.0
relative: false
scalingCount: 2
scaling0: 0.0
scaling1: 1.0
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Life Offset - 
active: false
- X Offset - 
active: false
- Y Offset - 
active: false
- Spawn Shape - 
shape: point
- Spawn Width - 
lowMin: 0.0
lowMax: 0.0
highMin: 0.0
highMax: 0.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Spawn Height - 
lowMin: 0.0
lowMax: 0.0
highMin: 0.0
highMax: 0.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- X Scale - 
lowMin: 16.0
lowMax: 16.0
highMin: 32.0
highMax: 32.0
relative: false
scalingCount: 2
scaling0: 1.0
scaling1: 0.26530612
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Velocity - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 120.0
highMax: 800.0
relative: false
scalingCount: 3
scaling0: 1.0
scaling1: 0.59183675
scaling2: 0.30612245
timelineCount: 3
timeline0: 0.0
timeline1: 0.30136988
timeline2: 0.739726
- Angle - 
active: true
lowMin: 1.0
lowMax: 360.0
highMin: -180.0
highMax: 180.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Rotation - 
active: false
- Wind - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: -90.0
highMax: 90.0
relative: false
scalingCount: 2
scaling0: 0.0
scaling1: 1.0
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Gravity - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 0.0
highMax: -700.0
relative: false
scalingCount: 2
scaling0: 0.0
scaling1: 1.0
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Tint - 
colorsCount: 6
colors0: 0.69411767
colors1: 0.0
colors2: 0.0
colors3: 1.0
colors4: 0.019607844
colors5: 0.0
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Transparency - 
lowMin: 0.0
lowMax: 0.0
highMin: 1.0
highMax: 1.0
relative: false
scalingCount: 4
scaling0: 0.0
scaling1: 1.0
scaling2: 0.6666667
scaling3: 0.0
timelineCount: 4
timeline0: 0.0
timeline1: 0.2
timeline2: 0.60958904
timeline3: 1.0
- Options - 
attached: false
continuous: false
aligned: false
additive: true
behind: false
premultipliedAlpha: false
spriteMode: single
simple.png

