package com.github.myio;

/*
* Class that holds all game servers and relation between websocket and gameserver that handles it
* */

import com.github.czyzby.websocket.serialization.impl.ManualSerializer;
import com.github.myio.dto.masterserver.GameServerDto;
import com.github.myio.entities.PlayerCell;
import com.github.myio.enums.GameServerState;
import com.github.myio.networking.Packet;
import com.github.myio.networking.packet.GameStateChangedPacket;
import com.github.myio.networking.packet.TeamLoginPacket;
import com.github.myio.packethandlers.ServerPacketHandler;
import com.github.myio.tools.IoLogger;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

import io.vertx.core.buffer.Buffer;
import io.vertx.core.http.ServerWebSocket;
import io.vertx.core.http.WebSocketFrame;

import static com.github.myio.GameConstants.DebugMode;
import static com.github.myio.GameConstants.MAX_PLAYERS_IN_INSTANCE;
import static com.github.myio.networking.PacketType.TEAM_LOGIN_PACKET;

public class LoadBalancer
{

    private List<GameWorld> gameWorlds;
    private ManualSerializer serializer;
    private Map<ServerWebSocket,GameWorld> socketGameWorldMap ;
    private Map<String,GameWorld> teamToWorldMap;
    ExecutorService executor ;


    public LoadBalancer(int serversAmount, ManualSerializer serializer)
    {
        this.serializer = serializer;
        gameWorlds = new CopyOnWriteArrayList<>();
        socketGameWorldMap = new ConcurrentHashMap<>(serversAmount*MAX_PLAYERS_IN_INSTANCE);
        teamToWorldMap = new ConcurrentHashMap<>(serversAmount*MAX_PLAYERS_IN_INSTANCE);


        for (int i =0; i< serversAmount; i++)
            gameWorlds.add(new GameWorld(serializer, i, this));

        executor = Executors.newFixedThreadPool(2);

        executor.execute(new Runnable() {
            @Override
            public void run() {
                while (true)
                    gameWorlds.forEach(GameWorld::serverFrame);

            }
        });

       /* new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                ping();
            }
        },1000,1000);*/
    }

    public void changeServersCount(int newCount)
    {

        if(newCount < 1 || newCount > 25) return; //sanity check
        IoLogger.log("Change server count to " + newCount);

        int currentCount = gameWorlds.size();
        if(currentCount < newCount )
        {
            for (int i = 0; i < (newCount - currentCount); i++)
            {
                gameWorlds.add(new GameWorld(serializer, currentCount+i, this));
            }
        }
        if(currentCount > newCount)
        {
            int toStop = currentCount - newCount;
            long emptyServers = gameWorlds.stream().filter(w -> w.getAlivePlayersMap().isEmpty()).count();

            gameWorlds.stream()
                        .filter(w -> w.getAlivePlayersMap().isEmpty())
                        .limit(toStop)
                        .forEach(w->
                        {
                           w.setShouldBeDisposed(true);
                           w.restart(null);
                        });

            if(emptyServers < toStop)
            {
                for (int i = 0; i < (toStop - emptyServers) ; i++)
                {
                    gameWorlds.get(i).setShouldBeDisposed(true);
                }
            }

        }
    }


    public void addNewClientSocket(ServerWebSocket socket)
    {
        if (DebugMode) IoLogger.log("addNewClientSocket " + socket.hashCode());
        boolean connected = false;
        for (GameWorld world : gameWorlds)
        {
            if(!world.isFull() && world.serverState != GameServerState.GAME_IN_PROGRESS  && world.serverState != GameServerState.LAUNCHING)
            {
                socketGameWorldMap.put(socket,world);
                //world.getPacketEmitter().clientSockets.add(socket);
                connected = true;

                //need to set it at very beginning
                socket.write(Buffer.buffer(ServerLauncher.serializer.serialize(
                        new GameStateChangedPacket(world.getServerState(), world.getGamemode().getGameType()))));

                break;
            }
        }
        if(!connected)
        {
            if(DebugMode) IoLogger.log("WEbSocketRejected, not enough player slots!");
            socket.reject();
        }
    }


    /**
     * We want to have all team at 1 game server, so we need to find for non-full
     * GameWorld and associate player socket with it. Next time when new team member with same
     * teamId will join server, we will add him right to this GameWorld
     *
    */
    public void assignTeamMemberToServer(ServerWebSocket teamMemberSocket, TeamLoginPacket packet)
    {
        if (DebugMode) IoLogger.log("assignTeamMemberToServer " + teamMemberSocket.hashCode());
        if(teamToWorldMap.containsKey(packet.teamId))
        {
            //we already have some of team members on this server, so just add this player to it
            GameWorld world = teamToWorldMap.get(packet.teamId);

            socketGameWorldMap.put(teamMemberSocket, world);
            teamMemberSocket.write(Buffer.buffer(ServerLauncher.serializer.serialize(
                    new GameStateChangedPacket(world.getServerState(), world.getGamemode().getGameType()))));
        }
        else
        {
            //need to find proper server
            boolean connected = false;
            for (GameWorld world : gameWorlds)
            {
                if(!world.isFull() && world.serverState != GameServerState.GAME_IN_PROGRESS && world.serverState != GameServerState.LAUNCHING)
                {
                    socketGameWorldMap.put(teamMemberSocket,world);
                    teamToWorldMap.put(packet.teamId, world);

                    connected = true;
                    //need to set it at very beginning
                    teamMemberSocket.write(Buffer.buffer(ServerLauncher.serializer.serialize(
                            new GameStateChangedPacket(world.getServerState(), world.getGamemode().getGameType()))));
                    break;
                }
            }
            if(!connected)
            {
                if(DebugMode) IoLogger.log("WEbSocketRejected, not enough player slots!");
                teamMemberSocket.reject();
            }
        }
    }

   /**
    *
    * Only place where we should handle all closed sockets
    */
    public void removeClientSocket(ServerWebSocket socket) {

        GameWorld playerWorld = socketGameWorldMap.get(socket);
        if (DebugMode) IoLogger.log("removeClientSocket " + socket.hashCode());
        socketGameWorldMap.remove(socket);
        if(playerWorld == null)
        {
            IoLogger.log("removeClientSocket null playerworld");
            return;
        }

        PlayerCell leftPlayer = playerWorld.getPacketEmitter().getSocketToPlayer().get(socket);
        if(leftPlayer != null)
        {
            if(DebugMode) IoLogger.log("removeClientSocket leftPlayer " + leftPlayer);
            if(DebugMode) IoLogger.log("removeClientSocket getPlayersToSocket " + playerWorld.getPacketEmitter().getPlayersToSocket().get(leftPlayer));
            if(DebugMode) IoLogger.log("removeClientSocket getAlivePlayersMap containsKey " + playerWorld.getAlivePlayersMap().containsKey (leftPlayer.getId()));
            playerWorld.getJustdisconnectedPlayers().add(leftPlayer.getId());
            playerWorld.getAlivePlayersMap().remove(leftPlayer.getId());
            playerWorld.getStatsManager().removePlayer(leftPlayer);
            if(playerWorld.getSquadManager() != null) playerWorld.getSquadManager().removePlayer(leftPlayer);
            playerWorld.getPacketEmitter().getPlayersToSocket().remove(leftPlayer);
        }
        else
        {
            IoLogger.log("removeClientSocket null leftPlayer "  + socket.hashCode());
        }
        playerWorld.getPacketEmitter().getSocketToPlayer().remove(socket);

    }

    private void ping()
    {
        socketGameWorldMap.keySet().forEach(s -> s.writeTextMessage("ping"));
    }

    public void pingHandler(String frame, ServerWebSocket clientSocket)
    {
        if(frame.equals("pong"))
        {
            clientSocket.writeTextMessage("pang");
        }
        else IoLogger.log("pingHandler unknown cmd: " + frame);
    }



    public void clientSocketHandle(WebSocketFrame frame, ServerWebSocket clientSocket)
    {
      //  if (DebugMode) IoLogger.log("clientSocketHandle " + clientSocket.hashCode());
        Packet packet = null;
        try {
            packet = (Packet) serializer.deserialize(frame.binaryData().getBytes());

            /**
             *  special case, we need to check if team member will have same server as another teammates,
             * and change his GameWorld if needed, then we will process same packet at proper GameWorld
             */

            if(packet.packetType == TEAM_LOGIN_PACKET)
                assignTeamMemberToServer(clientSocket,(TeamLoginPacket) packet);

        } catch (Exception e) {
            e.printStackTrace();
        }

        GameWorld playerWorld = socketGameWorldMap.get(clientSocket);

        ArrayList<ServerPacketHandler> currentPacketHandlers = new ArrayList<ServerPacketHandler>();
        currentPacketHandlers.addAll(playerWorld.getPacketHandlers());

        boolean handled = false;
        for (ServerPacketHandler packetHandler : currentPacketHandlers) {
            if (packetHandler.handlePacket(packet, clientSocket))
            {
                handled = true;
                break;
            }
        }

        if(!handled) IoLogger.log("Unhandled packets here: " + packet);


    }



    public List<GameServerDto> gameServerStatus()
    {
        List<GameServerDto> res = gameWorlds.stream().map(world -> new GameServerDto(true, world.getAlivePlayersMap().size(), MAX_PLAYERS_IN_INSTANCE, world.getWorldId(), world.getServerState())).collect(Collectors.toList());

        return res;
    }


    public ExecutorService getExecutor() {
        return executor;
    }


    public List<GameWorld> getGameWorlds() {
        return gameWorlds;
    }
}
