package com.github.myio.packethandlers;

import com.github.myio.GameWorld;
import com.github.myio.PacketEmitter;
import com.github.myio.networking.Packet;
import com.github.myio.networking.PacketType;

import io.vertx.core.http.ServerWebSocket;

/**
 * Created by taras on 21.03.2018.
 */

public class PvPInfoHandler extends AbstractPacketHandler {


    public PvPInfoHandler(GameWorld world, PacketEmitter p) {
        super(world, p);
    }

    @Override
    public boolean handlePacket(Packet packet, ServerWebSocket clientSocket) {

        if (packet.packetType == PacketType.PVP_INFO_PACKET){
            return true;
        }
        return false;
    }
}
