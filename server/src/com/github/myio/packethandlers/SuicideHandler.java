package com.github.myio.packethandlers;

import com.github.myio.GameWorld;
import com.github.myio.PacketEmitter;
import com.github.myio.entities.PlayerCell;
import com.github.myio.networking.Packet;
import com.github.myio.networking.PacketType;
import com.github.myio.networking.packet.SuicidePacket;

import io.vertx.core.http.ServerWebSocket;

/**
 * Created by taras on 13.02.2018.
 */
public class SuicideHandler extends AbstractPacketHandler {


    public SuicideHandler(GameWorld world, PacketEmitter p) {
        super(world, p);
    }

    @Override
    public boolean handlePacket(Packet packet, ServerWebSocket clientSocket) {


        if (packet.packetType == PacketType.SUICIDE){

            SuicidePacket p = (SuicidePacket) packet;

            PlayerCell playerCell = world.getAlivePlayersMap().get(p.playerId);
            if(playerCell != null)
            {
                world.RegisterKill(p.playerId,p.playerId);
            }


            return true;
        }

        return false;
    }
}
