package com.github.myio.packethandlers;

import com.github.myio.GameWorld;
import com.github.myio.PacketEmitter;
import com.github.myio.ServerLauncher;
import com.github.myio.entities.PlayerCell;
import com.github.myio.entities.map.TacticalShield;
import com.github.myio.networking.Packet;
import com.github.myio.networking.packet.HeartBeatDisablePacket;
import com.github.myio.networking.packet.HeartBeatPacket;
import com.github.myio.tools.IoLogger;

import io.vertx.core.buffer.Buffer;
import io.vertx.core.http.ServerWebSocket;

import static com.github.myio.networking.PacketType.HEARTBEAT;

/**
 * Created by Igor on 3/26/2018.
 */

public class HeartbeatHandler extends AbstractPacketHandler {
    public HeartbeatHandler(GameWorld world, PacketEmitter p) {
        super(world, p);
    }

    @Override
    public boolean handlePacket(Packet packet, ServerWebSocket clientSocket) {
        if(packet.packetType == HEARTBEAT)
        {
            HeartBeatPacket p = (HeartBeatPacket) packet;
            switch (p.type){
                case HeartBeatPacket.PLAYER:
                    if(!world.getAlivePlayersMap().containsKey(p.entityId))
                    {
                        IoLogger.log(world.getWorldIdTag() + " HEARTBEAT wrong player id: " + p.entityId);

                        sendErrorMessageToClient(clientSocket," HEARTBEAT wrong player id: " + p.entityId );

                        clientSocket.write(Buffer.buffer(ServerLauncher.serializer.serialize(new HeartBeatDisablePacket())));
                        //clientSocket.end();
                        //world.getLoadBalancer().removeClientSocket(clientSocket);
                        return true;
                    }
                    PlayerCell player = world.getAlivePlayersMap().get(p.entityId);
                    player.setX(p.x);
                    player.setY(p.y);
                    player.setRotationAngle( p.facingAngle);
                    break;
                case HeartBeatPacket.TACTICAL_SHIELD:
                    if(!world.getMapEntities().containsKey(p.entityId))
                    {
                        IoLogger.log("HEARTBEAT wrong entity id");
                        return true;
                    }
                    TacticalShield shield = (TacticalShield) world.getMapEntities().get(p.entityId);
                    shield.setX(p.x);
                    shield.setY(p.y);
                    shield.setRotationAngle( p.facingAngle);
                    world.getChangedEntitiesSet().add(shield);
                    break;
            }



            return true;
        }

        return false;
    }
}
