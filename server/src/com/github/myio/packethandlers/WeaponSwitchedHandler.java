package com.github.myio.packethandlers;

import com.github.myio.GameWorld;
import com.github.myio.PacketEmitter;
import com.github.myio.entities.PlayerCell;
import com.github.myio.entities.weapons.AbstractRangeWeapon;
import com.github.myio.networking.Packet;
import com.github.myio.networking.packet.WeaponSwitchedPacket;
import com.github.myio.tools.IoLogger;

import io.vertx.core.http.ServerWebSocket;

import static com.github.myio.networking.PacketType.WEAPON_SWITCHED;


public class WeaponSwitchedHandler extends AbstractPacketHandler {


    public WeaponSwitchedHandler(GameWorld world, PacketEmitter p) {
        super(world, p);
    }



    @Override
    public boolean handlePacket(Packet packet, ServerWebSocket clientSocket) {
        if(packet.packetType == WEAPON_SWITCHED)
        {
            WeaponSwitchedPacket p = (WeaponSwitchedPacket) packet;

            IoLogger.log("WeaponSwitchedPacket   "+"player ID = "+p.playerId+"| weaponIdx = "+p.weaponIdx+"| typeToSwitchTo = "+p.typeToSwitchTo );
            PlayerCell player = world.getAlivePlayersMap().get(p.playerId);

            if(player == null)
            {
                IoLogger.log("WeaponSwitchedHandler not alive player");

                sendErrorMessageToClient(clientSocket, "WeaponSwitchedHandler not alive player");
                return true;
            }

            if(!player.getInventoryManager().hasWeapon(p.typeToSwitchTo))
            {
                IoLogger.log("WeaponSwitchedHandler player has no such weapon");

                sendErrorMessageToClient(clientSocket, "WeaponSwitchedHandler player has no such weapon" );
                return true;
            }

            AbstractRangeWeapon newWeapon = player.getInventoryManager().getWeaponList().get(p.weaponIdx);
            if(newWeapon ==  null)
            {
                IoLogger.log("WeaponSwitchedHandler null weapon");

                sendErrorMessageToClient(clientSocket, "WeaponSwitchedHandler null weapon");
                return true;
            }

            if(!newWeapon.weaponType.equals(p.typeToSwitchTo))
            {
                IoLogger.log("WeaponSwitchedHandler weapon type mismatch");

                sendErrorMessageToClient(clientSocket,"WeaponSwitchedHandler weapon type mismatch" );
                return true;
            }
            player.getInventoryManager().switchWeaponTo(newWeapon);

            packetEmitter.emitToAllPlayers(p);
            return true;
        }
        return false;
    }




}
