package com.github.myio.packethandlers;

import com.badlogic.gdx.math.MathUtils;
import com.github.myio.EntityMapper;
import com.github.myio.GameWorld;
import com.github.myio.PacketEmitter;
import com.github.myio.StringConstants;
import com.github.myio.entities.PlayerCell;
import com.github.myio.entities.item.ItemCell;
import com.github.myio.entities.weapons.AbstractRangeWeapon;
import com.github.myio.enums.ItemType;
import com.github.myio.enums.WeaponType;
import com.github.myio.networking.Packet;
import com.github.myio.networking.PacketType;
import com.github.myio.networking.packet.ItemDroppedPacket;
import com.github.myio.networking.packet.ItemPickedPacket;
import com.github.myio.tools.IoLogger;

import io.vertx.core.http.ServerWebSocket;

import static com.github.myio.GameConstants.DebugMode;
import static com.github.myio.GameConstants.PLAYERS_MAX_ARMOR;


public class ItemHandler extends AbstractPacketHandler
{


    public ItemHandler(GameWorld world, PacketEmitter e)
    {
        super(world, e);
    }

    @Override
    public boolean handlePacket(Packet packet, ServerWebSocket clientSocket)
    {


        if (packet.packetType == PacketType.ITEM_PICKED)
        {

            ItemPickedPacket p = (ItemPickedPacket) packet;

            PlayerCell playerCell = world.getAlivePlayersMap().get(p.playerId);
            ItemCell itemCell = world.getWorldItems().get(p.itemId);
            if (itemCell == null)
            {
                IoLogger.log("ItemHandler ITEM_PICKED - null  item!");

                sendErrorMessageToClient(clientSocket, "ItemHandler ITEM_PICKED - null  item!");
                return true;
            }
            if (playerCell == null)
            {
                IoLogger.log("ItemHandler ITEM_PICKED - null player !");
                sendErrorMessageToClient(clientSocket, "ItemHandler ITEM_PICKED - null  player!");
                return true;
            }


            if (playerCell.getPosition().distance(itemCell.getPosition()) > itemCell.getCollisionRadius() * 1.5)
            {
                IoLogger.log("ItemHandler ITEM_PICKED - player is too far!");
                sendErrorMessageToClient(clientSocket, "ItemHandler ITEM_PICKED - player is too far!");
                return true;
            }

            IoLogger.log("ITEM_PICKED " + p.itemId);
            IoLogger.log("ITEM_PICKED " + world.getWorldItems().get(p.itemId));

            if (itemCell.getItemType() == ItemType.ARMOR)
            {
                if (playerCell.getCurrentArmor() < PLAYERS_MAX_ARMOR)
                {
                    playerCell.setCurrentArmor(PLAYERS_MAX_ARMOR);
                }
                else
                { return true; }

            }

            if (itemCell.getItemType().equals(ItemType.AMMUNITION))
            {
                if (itemCell.getItem_blueprint().equals(StringConstants.PATRON_BOX))
                {
                    int items = MathUtils.random(2,4);
                    world.dropOutPatrnobox(itemCell,items);
                }
                else
                {
                    int items = MathUtils.random(1,3);
                    world.dropOutAmmunition(itemCell,items);
                }

            }

            /*if(itemCell.getItem_blueprint().equals(StringConstants.FLASH_GRENADE) || itemCell.getItem_blueprint().equals(StringConstants.FRAG_GRENADE)) {
                if(playerHasThisGrenade(playerCell, itemCell))
                {
                    IoLogger.log("player already has this greande  " + p.itemId);
                    return true;
                }

            }*/
            if (itemCell.getItemType().equals(ItemType.PATRON))
            {
                if (playerCell.getInventoryManager().checkFullPatron(itemCell))
                {
                    playerCell.getInventoryManager().pickItem(itemCell);
                }else {
                    if (DebugMode)
                    IoLogger.err("FULL ITEM SLOT FOR "+itemCell.getItem_blueprint());
                    return true;
                }
            }else {
                playerCell.getInventoryManager().pickItem(itemCell);
            }

            ItemCell replacedWeapon = playerCell.getInventoryManager().getPreviousItem();
            if (replacedWeapon != null)
            {
                //ItemDroppedPacket itemDroppedPacket = new ItemDroppedPacket(replacedWeapon.getId(), replacedWeapon.getId(), replacedWeapon.totalAmmoAmount, replacedWeapon.clipAmmoAmount);
                IoLogger.log("WEAPON_DROPPED " + replacedWeapon.getItem_blueprint());

                replacedWeapon.x = playerCell.x;
                replacedWeapon.y = playerCell.y;

                world.getWorldItems().put(replacedWeapon.getId(), replacedWeapon);
                world.getDroppedItems().add(EntityMapper.mapFullEntity(replacedWeapon));
                playerCell.getInventoryManager().setPreviousItem(null);
            }


            world.getJustPickedItems().add(p.itemId);
            world.getWorldItems().remove(p.itemId);


            packetEmitter.emitToAllPlayers(packet);
            return true;
        }

        if (packet.packetType == PacketType.ITEM_DROPPED)
        {

            ItemDroppedPacket p = (ItemDroppedPacket) packet;

            PlayerCell playerCell = world.getAlivePlayersMap().get(p.playerId);
            if (playerCell == null)
            {
                IoLogger.log("ItemHandler ITEM_DROPPED - null player !");
                sendErrorMessageToClient(clientSocket, "ItemHandler ITEM_DROPPED - null player!");

                return true;
            }

            ItemCell itemCell = playerCell.getInventoryManager().getItems().get(p.itemId);
            if (itemCell == null)
            {
                IoLogger.log("ItemHandler ITEM_DROPPED - null  item!");
                sendErrorMessageToClient(clientSocket, "ItemHandler ITEM_DROPPED - null item!");
                return true;
            }

            if (!playerCell.getInventoryManager().getItems().containsValue(itemCell))
            {
                IoLogger.log("ItemHandler ITEM_DROPPED - player dropped wrong item!");
                sendErrorMessageToClient(clientSocket, "ItemHandler ITEM_DROPPED -  player dropped wrong item!");
                return true;
            }

            playerCell.getInventoryManager().getItems().remove(itemCell.getId());
            playerCell.getInventoryManager().dropItem(itemCell);


            itemCell.totalAmmoAmount = p.totalAmmoAmount;
            itemCell.clipAmmoAmount = p.clipAmmoAmount;

            itemCell.x = playerCell.x;
            itemCell.y = playerCell.y;

            world.getWorldItems().put(itemCell.getId(), itemCell);
            world.getDroppedItems().add(EntityMapper.mapFullEntity(itemCell));


            // packetEmitter.emitToAllPlayers(packet);
            return true;
        }


        return false;
    }

    private boolean playerHasThisGrenade(PlayerCell playerCell, ItemCell itemCell)
    {
        for (AbstractRangeWeapon weapon : playerCell.getInventoryManager().getWeaponList())
        {
            if (weapon != null && weapon.weaponType != null && weapon.weaponType == WeaponType.FLASHBANG && itemCell.getItem_blueprint().equals(StringConstants.FLASH_GRENADE) ||
                    weapon != null && weapon.weaponType != null && weapon.weaponType == WeaponType.FRAG_GRENADE && itemCell.getItem_blueprint().equals(StringConstants.FRAG_GRENADE))
            {
                return true;
            }
        }
        return false;
    }

}
