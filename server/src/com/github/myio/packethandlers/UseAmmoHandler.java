package com.github.myio.packethandlers;

/**
 * Created by alex on 28.06.18.
 */


import com.github.myio.GameWorld;
import com.github.myio.PacketEmitter;
import com.github.myio.entities.IoPoint;
import com.github.myio.entities.PlayerCell;
import com.github.myio.entities.item.ItemCell;
import com.github.myio.entities.weapons.AbstractRangeWeapon;
import com.github.myio.entities.weapons.ShotGun;
import com.github.myio.enums.WeaponType;
import com.github.myio.networking.Packet;
import com.github.myio.networking.packet.UseAmmoPacket;
import com.github.myio.tools.IoLogger;

import java.util.Iterator;

import io.vertx.core.http.ServerWebSocket;

import static com.github.myio.networking.PacketType.USE_AMMO;

public class UseAmmoHandler extends AbstractPacketHandler {


    public UseAmmoHandler(GameWorld world, PacketEmitter p) {
        super(world, p);
    }


    @Override
    public boolean handlePacket(Packet packet, ServerWebSocket clientSocket) {
        if (packet.packetType == USE_AMMO) {

            UseAmmoPacket p = (UseAmmoPacket) packet;
            PlayerCell player = world.getAlivePlayersMap().get(p.mPlayerId);
            if (player != null) {
                WeaponType weaponType = WeaponType.valueOf(p.mWeaponType);
                String ammoBlueprint =  player.getInventoryManager().getWeapon(weaponType).getWeaponAmmoBlueprint(weaponType);
                int ammo = player.getInventoryManager().getAmmo(ammoBlueprint);
                if (player.getInventoryManager().hasWeapon(WeaponType.valueOf(p.mWeaponType)) && ammo >= p.mPatron) {
                    IoLogger.log("UseAmmoHandler: chiki-piki");
                    if(player.getInventoryManager().getWeapon(weaponType) instanceof ShotGun) {
                        player.getInventoryManager().getWeapon(weaponType).reloadShotgun(player.getInventoryManager(), false, p.mPatron);
                    } else {
                    player.getInventoryManager().getWeapon(weaponType).reload(player.getInventoryManager(), false, p.mPatron);
                    packetEmitter.emitToNearestPlayers(p, new IoPoint(player.getX(), player.getY())); // todo replace with emit to single player
                    return true;}
                } else {
                    IoLogger.log("UseAmmoHandler: " + "on client = " + p.mPatron + "   on server = " + ammo);
                    packetEmitter.emitToNearestPlayers(new UseAmmoPacket(p.mPlayerId, p.mWeaponType, ammo), new IoPoint(player.getX(), player.getY())); // todo replace with emit to single player
                    return true;
                }
            }
        }
        return false;
    }
}

