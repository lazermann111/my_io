package com.github.myio.packethandlers;


import com.github.myio.GameWorld;
import com.github.myio.PacketEmitter;
import com.github.myio.ServerLauncher;
import com.github.myio.networking.packet.ServerErrorPacket;

import io.vertx.core.buffer.Buffer;
import io.vertx.core.http.ServerWebSocket;

public abstract class  AbstractPacketHandler implements ServerPacketHandler
{
    GameWorld world;

    PacketEmitter packetEmitter;
    public AbstractPacketHandler(GameWorld world, PacketEmitter p)
    {
        packetEmitter = p;
        this.world = world;
    }


    protected void sendErrorMessageToClient(ServerWebSocket socket, String message)
    {
        socket.write(Buffer.buffer(ServerLauncher.serializer
            .serialize(new ServerErrorPacket( "", message))));
    }
}
