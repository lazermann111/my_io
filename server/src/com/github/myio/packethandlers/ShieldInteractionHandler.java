package com.github.myio.packethandlers;

import com.github.myio.GameWorld;
import com.github.myio.PacketEmitter;
import com.github.myio.entities.PlayerCell;
import com.github.myio.entities.map.TacticalShield;
import com.github.myio.networking.Packet;
import com.github.myio.networking.packet.ShieldInteractionPacket;
import com.github.myio.tools.IoLogger;

import io.vertx.core.http.ServerWebSocket;

import static com.github.myio.networking.PacketType.SHIELD_INTERACTION;


public class ShieldInteractionHandler extends AbstractPacketHandler {

    public ShieldInteractionHandler(GameWorld world, PacketEmitter p) {
        super(world, p);
    }

    @Override
    public boolean handlePacket(Packet packet, ServerWebSocket clientSocket) {

        if(packet.packetType == SHIELD_INTERACTION)
        {

            ShieldInteractionPacket p = (ShieldInteractionPacket) packet;

            PlayerCell player = world.getAlivePlayersMap().get(p.playerId);
            TacticalShield shield = (TacticalShield) world.getMapEntities().get(p.shiledId);



            if(player != null && shield != null)
            {
                if(player.getPosition().distance(shield.getPosition()) > shield.getInteractionRadius()*1.5)
                {
                    IoLogger.log("ShieldInteractionHandler - player is too far!");

                    sendErrorMessageToClient(clientSocket,"ShieldInteractionHandler - player is too far!" );
                    return true;
                }

                if(shield.inUse)
                {
                    IoLogger.log("ShieldInteractionHandler - Already in use!");

                    sendErrorMessageToClient(clientSocket, "ShieldInteractionHandler - Already in use!");
                    return true;
                }
                if(p.equipped) player.shiled = shield;
                else  player.shiled = null;
            }
            else IoLogger.log("ShieldInteractionHandler - null player or shield " + player+shield);

            packetEmitter.emitToAllPlayers(p);

            return true;
        }


        return false;
    }
}
