
package com.github.myio.packethandlers;

import com.github.myio.GameWorld;
import com.github.myio.PacketEmitter;
import com.github.myio.entities.PlayerCell;
import com.github.myio.entities.item.ItemCell;
import com.github.myio.networking.Packet;
import com.github.myio.networking.packet.UseMedkitPacket;
import com.github.myio.tools.IoLogger;

import java.util.Iterator;

import io.vertx.core.http.ServerWebSocket;

import static com.github.myio.networking.PacketType.USE_MEDKIT;

/**
 * Created by alex on 04.04.18.
 */

public class UseMedKitHandler extends AbstractPacketHandler {


    public UseMedKitHandler(GameWorld world, PacketEmitter p) {
        super(world, p);
    }


    @Override
    public boolean handlePacket(Packet packet, ServerWebSocket clientSocket)
    {
        if (packet.packetType == USE_MEDKIT)
        {

            UseMedkitPacket p = (UseMedkitPacket) packet;
            PlayerCell player = world.getAlivePlayersMap().get(p.mPlayerId);
            if (player != null)
            {
                for (Iterator<ItemCell> playerItems = player.getInventoryManager().getItems().values().iterator(); playerItems.hasNext();)
                {
                    if (playerItems.next().getItem_blueprint().equals(p.medkitBlueprint))
                    {
                        playerItems.remove();
                        packetEmitter.emitToAllPlayers(p);
                        world.getAlivePlayersMap().get(p.mPlayerId).healByMedKit(p.medkitBlueprint);

                        IoLogger.log("UseMedKitHandler: " + "Player used: " + p.medkitBlueprint +
                                " Current health: " + player.getCurrentHealth() + "    Medkit size: " + player.getInventoryManager().getMedKits());
                        return true;
                    }
                }
                IoLogger.log("UseMedKitHandler: " + "Item " + p.medkitBlueprint + " not found");
                sendErrorMessageToClient(clientSocket,"UseMedKitHandler: " + "Item " + p.medkitBlueprint + " not found" );
            }
            else
            {
                IoLogger.log("UseMedKitHandler: " + "Player " + p.mPlayerId + " not found");
                sendErrorMessageToClient(clientSocket,"UseMedKitHandler: " + "Player " + p.mPlayerId + " not found" );
            }

            return true;

        }
        return false;
    }




}