package com.github.myio.packethandlers;

import com.github.myio.GameWorld;
import com.github.myio.PacketEmitter;
import com.github.myio.networking.Packet;
import com.github.myio.networking.PacketType;
import com.github.myio.networking.packet.MessageChatPacket;

import io.vertx.core.http.ServerWebSocket;

/**
 * Created by taras on 13.02.2018.
 */
public class MessageChatHandler extends AbstractPacketHandler {


    public MessageChatHandler(GameWorld world, PacketEmitter p) {
        super(world, p);
    }

    @Override
    public boolean handlePacket(Packet packet, ServerWebSocket clientSocket) {


        if (packet.packetType == PacketType.MESSAGE_CHAT_PACKET){

            MessageChatPacket p = (MessageChatPacket) packet;
            packetEmitter.emitToNearestPlayers(p,p.start);


            return true;
        }

        return false;
    }
}
