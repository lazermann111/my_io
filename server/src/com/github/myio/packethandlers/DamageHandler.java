package com.github.myio.packethandlers;

import com.badlogic.gdx.Gdx;
import com.github.myio.GameWorld;
import com.github.myio.PacketEmitter;
import com.github.myio.entities.PlayerCell;
import com.github.myio.networking.Packet;
import com.github.myio.networking.PacketType;
import com.github.myio.networking.packet.DealDamagePacket;

import com.github.myio.tools.IoLogger;
import io.vertx.core.http.ServerWebSocket;



//todo (Nikita) use for knife
public class DamageHandler extends AbstractPacketHandler {
    public DamageHandler(GameWorld world, PacketEmitter e) {
        super(world,e);
    }
    @Override
    public boolean handlePacket(Packet packet, ServerWebSocket clientSocket) {

        try {
            if(packet.packetType == PacketType.DEAL_DAMAGE)
            {
                DealDamagePacket p = (DealDamagePacket) packet;

                //IoLogger.log("Damaged");

                if(world.getAlivePlayersMap().containsKey(p.localPlayerId) && world.getAlivePlayersMap().containsKey(p.enemyId)) {

                    PlayerCell localPlayer = world.getAlivePlayersMap().get(p.localPlayerId);
                    PlayerCell enemy = world.getAlivePlayersMap().get(p.enemyId);
                    if(localPlayer.collides(enemy)) {
                        world.getAlivePlayersMap().put(localPlayer.getId(), localPlayer);

                        //IoLogger.log(localPlayer.isPredator());
                        //IoLogger.log(enemy.isPredator());
                        //if(!localPlayer.isPredator() && enemy.isPredator())
                         //   localPlayer.setCurrentHealth((int)((float)localPlayer.getCurrentHealth() - (float)DAMAGE));

                        //IoLogger.log("Local player HP: " + localPlayer.getCurrentHealth());
                    }
                }
                return true;
            }
        } catch (Exception e) {
            IoLogger.log("DamageHandler handlePacket", e.getMessage());
            e.printStackTrace();
        }
        return false;
    }
}
