package com.github.myio.packethandlers;

import com.github.myio.GameWorld;
import com.github.myio.PacketEmitter;
import com.github.myio.networking.Packet;
import com.github.myio.networking.PacketType;
import com.github.myio.networking.packet.EmotePacket;
import com.github.myio.networking.packet.VoiceCommandsMenuPacket;
import com.github.myio.tools.IoLogger;
import com.github.myio.ui.VoiceCommandsMenu;

import io.vertx.core.http.ServerWebSocket;



public class EmoteHandler extends AbstractPacketHandler {


    public EmoteHandler(GameWorld world, PacketEmitter p) {
        super(world, p);
    }



    @Override
    public boolean handlePacket(Packet packet, ServerWebSocket clientSocket) {
        if(packet.packetType == PacketType.EMOTE_SHOWN)
        {
            EmotePacket p = (EmotePacket) packet;
            packetEmitter.emitToNearestPlayers(p, p.getOrigin());
            IoLogger.log("EmoteHandler   handlePacket");
            IoLogger.log("EmoteHandler   " + p.getPlayerId());
            IoLogger.log("EmoteHandler  " + p.getEmoteName());
            return true;
        }

        if (packet.packetType == PacketType.VOICE_COMMAND_SHOW)
        {
            VoiceCommandsMenuPacket p = (VoiceCommandsMenuPacket) packet;
            packetEmitter.emitToAllPlayers(p);
            System.out.println(p.playerId+"|||"+p.pVoiceComandName+"||||"+p.playerId);
            return true;

        }

        return false;
    }




}
