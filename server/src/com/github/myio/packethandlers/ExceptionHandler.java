package com.github.myio.packethandlers;


import com.badlogic.gdx.Gdx;
import com.github.myio.GameWorld;
import com.github.myio.PacketEmitter;
import com.github.myio.networking.Packet;
import com.github.myio.networking.PacketType;
import com.github.myio.networking.packet.ExceptionPacket;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;

import com.github.myio.tools.IoLogger;
import io.vertx.core.http.ServerWebSocket;

public class ExceptionHandler extends AbstractPacketHandler {

    private PrintWriter pw;
    private File file;

    public ExceptionHandler(GameWorld world, PacketEmitter p) {
        super(world, p);
        file = new File("server/heroku_build/ExceptionFile.txt");
        try {
             pw = new PrintWriter(file);
        } catch (FileNotFoundException e) {
            IoLogger.log("ExceptionHandler FileNotFound",e.getMessage());
            e.printStackTrace();
        }
    }

    @Override
    public boolean handlePacket(Packet packet, ServerWebSocket clientSocket) {

        try {
            if (packet.packetType == PacketType.EXCEPTION_PACKET)
            {
             String exceptionPlace = ((ExceptionPacket) packet ).exceptionPlace;
             String getException = ((ExceptionPacket) packet ).getException;
             pw.println(exceptionPlace+" | "+getException);
            }
            return true;
        } catch (Exception e) {
            IoLogger.log("ExceptionHandler handlePacket",e.getMessage());
            e.printStackTrace();
        }
        return false;
    }
}
