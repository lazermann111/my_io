package com.github.myio.packethandlers;

import com.github.myio.GameConstants;
import com.github.myio.GameWorld;
import com.github.myio.PacketEmitter;
import com.github.myio.entities.PlayerCell;
import com.github.myio.networking.Packet;
import com.github.myio.networking.packet.PlayerRevivingRequest;

import io.vertx.core.http.ServerWebSocket;

import static com.github.myio.networking.PacketType.PLAYER_REVIVING_REQUEST;

/**
 * Created by dell on 27.04.2018.
 */

public class PlayerSuppressedHandler extends AbstractPacketHandler {
    public PlayerSuppressedHandler(GameWorld world, PacketEmitter p) {
        super(world, p);
    }

    @Override
    public boolean handlePacket(Packet packet, ServerWebSocket clientSocket)
    {
        if(packet.packetType == PLAYER_REVIVING_REQUEST)
        {
            PlayerRevivingRequest p = (PlayerRevivingRequest) packet;

            PlayerCell playerCell = world.getAlivePlayersMap().get(p.mPlayerId);
            if (playerCell != null)
            {
                playerCell.setSuppressed(p.isSuppressed);
                playerCell.setCurrentHealth(GameConstants.PLAYER_HEALTH_AFTER_HELP);

                packetEmitter.emitToAllPlayers(p);
                return true;
            }
        }
        return false;
    }
}
