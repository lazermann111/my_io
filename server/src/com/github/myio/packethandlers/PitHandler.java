package com.github.myio.packethandlers;

import com.github.myio.GameWorld;
import com.github.myio.PacketEmitter;
import com.github.myio.entities.PlayerCell;
import com.github.myio.entities.map.MapEntityCell;
import com.github.myio.networking.Packet;
import com.github.myio.networking.packet.PitInteractionPacker;
import com.github.myio.tools.IoLogger;

import io.vertx.core.http.ServerWebSocket;

import static com.github.myio.networking.PacketType.PIT_INTERACTION;

/**
 * Created by dell on 03.04.2018.
 */

public class PitHandler extends AbstractPacketHandler {
    public PitHandler(GameWorld world, PacketEmitter p) {
        super(world, p);
    }

    @Override
    public boolean handlePacket(Packet packet, ServerWebSocket clientSocket) {
        if(packet.packetType == PIT_INTERACTION) {

            PitInteractionPacker p = (PitInteractionPacker) packet;

            PlayerCell player = world.getAlivePlayersMap().get(p.playerId);
            MapEntityCell pit = world.getMapEntities().get(p.pitId);

            if(player.getPosition().distance(pit.getPosition()) > pit.getCollisionRadius()*1.5)
            {
                IoLogger.log("PitHandler - player is too far!");

                sendErrorMessageToClient(clientSocket, "PitHandler - player is too far!" );
                return true;
            }

            if(player != null && pit != null)
            {
                if(p.insidePit) {
                    pit.getPlayersId().add(p.playerId);
                    player.pit = p.pitId;
                    player.setInsidePit(true);
                 //   IoLogger.log("Player entered pit");
                }
                else {
                    pit.getPlayersId().remove(p.playerId);
                    player.pit = null;
                    player.setInsidePit(false);
                  //  IoLogger.log("Player left pit");
                }
            }

            packetEmitter.emitToAllPlayers(p);

            return true;
        }


        return false;
    }
}
