package com.github.myio.packethandlers;

import com.github.myio.GameWorld;
import com.github.myio.PacketEmitter;
import com.github.myio.entities.PlayerCell;
import com.github.myio.enums.MapEntityType;
import com.github.myio.networking.Packet;
import com.github.myio.networking.packet.VisibilityChangePacket;

import io.vertx.core.http.ServerWebSocket;

import static com.github.myio.networking.PacketType.VISIBILITY_CHANGE;

/**
 * Created by dell on 06.04.2018.
 */

public class VisibilityChangeHandler extends AbstractPacketHandler {
    public VisibilityChangeHandler(GameWorld world, PacketEmitter p) {
        super(world, p);
    }

    @Override
    public boolean handlePacket(Packet packet, ServerWebSocket clientSocket) {
        if(packet.packetType == VISIBILITY_CHANGE) {
            VisibilityChangePacket p = (VisibilityChangePacket) packet;

            PlayerCell playerCell = world.getAlivePlayersMap().get(p.playerId);
            if (playerCell != null) {
                if (p.type == MapEntityType.SPIDER_WEB || p.type == MapEntityType.SPIDER_WEB_BIG) {
                    if (p.insideEntity)
                    {
                        playerCell.setUnderWeb(true);
                    } else
                        playerCell.setUnderWeb(false);
                }

                packetEmitter.emitToAllPlayers(p);
                return true;
            }
        }
        return false;
    }
}
