package com.github.myio.packethandlers;


import com.github.myio.GameWorld;
import com.github.myio.PacketEmitter;
import com.github.myio.entities.PlayerCell;
import com.github.myio.entities.projectiles.AK47Bullet;
import com.github.myio.entities.projectiles.FlashbangAmmo;
import com.github.myio.entities.projectiles.GrenadeAmmo;
import com.github.myio.entities.projectiles.GrenadeLauncherAmmo;
import com.github.myio.entities.projectiles.Knife;
import com.github.myio.entities.projectiles.MachineGunBullet;
import com.github.myio.entities.projectiles.PistolBullet;
import com.github.myio.entities.projectiles.Rocket;
import com.github.myio.entities.projectiles.ShotgunAmmo;
import com.github.myio.entities.weapons.AbstractRangeWeapon;
import com.github.myio.networking.Packet;
import com.github.myio.networking.packet.ProjectileLaunchedPacket;
import com.github.myio.networking.packet.ShotgunShotPacket;
import com.github.myio.tools.IoLogger;

import io.vertx.core.http.ServerWebSocket;

import static com.github.myio.networking.PacketType.PROJECTILE_LAUNCHED;

public class ProjectileLaunchedHandler  extends  AbstractPacketHandler{


    public ProjectileLaunchedHandler(GameWorld world, PacketEmitter p) {
        super(world, p);
    }

    @Override
    public boolean handlePacket(Packet packet, ServerWebSocket clientSocket) {


        if(packet.packetType == PROJECTILE_LAUNCHED)
        {

            ProjectileLaunchedPacket p = (ProjectileLaunchedPacket) packet;

            PlayerCell player = world.getAlivePlayersMap().get(p.shooterId);

            if(player == null)
            {
                IoLogger.log("ProjectileLaunchedHandler not alive player");

                sendErrorMessageToClient(clientSocket, "ProjectileLaunchedHandler not alive player" );
                return true;
            }

            AbstractRangeWeapon playerWeapon = player.getCurrentWeapon();
            if(playerWeapon == null  )
            {
                IoLogger.err("ProjectileLaunchedHandler null current weapon");

                sendErrorMessageToClient(clientSocket,"ProjectileLaunchedHandler null current weapon" );
                return true;
            }

            if(playerWeapon.totalAmmoAmount <= 0)
            {
                IoLogger.log("ProjectileLaunchedHandler  current weapon has no ammo");

                sendErrorMessageToClient(clientSocket, "ProjectileLaunchedHandler  current weapon has no ammo");
                return true;
            }

            if(!playerWeapon.projectileBlueprint.projectileType.equals(p.type))
            {
                IoLogger.err("ProjectileLaunchedHandler wrong projectile type");

                sendErrorMessageToClient(clientSocket, "ProjectileLaunchedHandler wrong projectile type");
                return true;
            }

            if(packet instanceof ShotgunShotPacket)
            {
                ShotgunShotPacket shotPacket= (ShotgunShotPacket) packet;
                packetEmitter.emitToNearestPlayers(shotPacket, p.start);
            }
            else
            {
                packetEmitter.emitToNearestPlayers(p, p.start);
            }


            switch (p.type)
            {
                case SHOTGUN:
                    ShotgunShotPacket shotPacket  = (ShotgunShotPacket)p;
                    for (float a : shotPacket.scatterAngles)
                    {
                        p.scatter = a;
                        world.getLaunchedProjectiles().add(new ShotgunAmmo(null,null,null, p));
                    }
                    break;

                case MACHINEGUN_BULLET:
                    world.getLaunchedProjectiles().add(new MachineGunBullet(null,null,null, p) );
                    break;
                case PISTOL_BULLET:
                    world.getLaunchedProjectiles().add(new PistolBullet(null,null,null, p) );
                    break;
                case ROCKET:
                    world.getLaunchedProjectiles().add(new Rocket(null,null,null, p) );
                    break;
                case FRAG_GRENADE:
                    world.getLaunchedProjectiles().add(new GrenadeAmmo(null,null,null, p) );
                    break;
                case KNIFE:
                    world.getLaunchedProjectiles().add(new Knife(null,null,null, p) );
                    break;
                case FLASHBANG:
                    world.getLaunchedProjectiles().add(new FlashbangAmmo(null,null,null, p) );
                    break;
                case GRENADE_LAUNCHER_AMMO:
                    world.getLaunchedProjectiles().add(new GrenadeLauncherAmmo(null,null,null, p) );
                    break;
                case AK47_BULLET:
                    world.getLaunchedProjectiles().add(new AK47Bullet(null,null,null, p) );
                    break;
            }
            return true;
        }
        return false;
    }
}
