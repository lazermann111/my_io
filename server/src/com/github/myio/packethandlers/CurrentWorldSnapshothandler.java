package com.github.myio.packethandlers;

import com.github.myio.EntityMapper;
import com.github.myio.GameWorld;
import com.github.myio.PacketEmitter;
import com.github.myio.ServerLauncher;
import com.github.myio.dto.AnimalCellDto;
import com.github.myio.dto.ItemDto;
import com.github.myio.dto.MapEntityDto;
import com.github.myio.dto.PlayerCellDto;
import com.github.myio.networking.Packet;
import com.github.myio.networking.packet.CurrentWorldSnapshotPacket;
import com.github.myio.tools.IoLogger;

import io.vertx.core.buffer.Buffer;
import io.vertx.core.http.ServerWebSocket;

import static com.github.myio.GameConstants.DebugMode;
import static com.github.myio.networking.PacketType.CURRENT_WORLD_SNAPSHOT_REQUEST;



public class CurrentWorldSnapshothandler  extends AbstractPacketHandler{
    public CurrentWorldSnapshothandler(GameWorld world, PacketEmitter p) {
        super(world, p);
    }

    @Override
    public boolean handlePacket(Packet packet, ServerWebSocket clientSocket) {

        if (packet.packetType == CURRENT_WORLD_SNAPSHOT_REQUEST)
        {
            CurrentWorldSnapshotPacket initial = new CurrentWorldSnapshotPacket();

            initial.players = world.getAlivePlayersMap().values().stream().map(EntityMapper::mapFull).toArray(PlayerCellDto[]::new);
            initial.entities = world.getMapEntities().values().stream().map(EntityMapper::mapFullEntity).toArray(MapEntityDto[]::new);
            initial.items = world.getWorldItems().values().stream().map(EntityMapper::mapFullEntity).toArray(ItemDto[]::new);
            initial.animals = world.getAliveAnimals().values().stream().map(EntityMapper::mapFull).toArray(AnimalCellDto[]::new);

            if(DebugMode) IoLogger.log(world.getWorldIdTag()+"**sending CurrentWorldSnapshotPacket***" + initial.toString());
            clientSocket.write(Buffer.buffer(ServerLauncher.serializer.serialize(initial)));


            return true;
        }


        return false;
    }
}
