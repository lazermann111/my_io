package com.github.myio.packethandlers;

import com.github.myio.networking.Packet;

import io.vertx.core.http.ServerWebSocket;

/**
 * Can handle packet from client and respond immediately  using clientSocket
 */

public interface ServerPacketHandler {

    public abstract boolean handlePacket (Packet packet, ServerWebSocket clientSocket);
}
