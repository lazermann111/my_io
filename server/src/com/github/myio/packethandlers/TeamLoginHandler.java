package com.github.myio.packethandlers;

import com.github.myio.EntityMapper;
import com.github.myio.GameConstants;
import com.github.myio.GameWorld;
import com.github.myio.PacketEmitter;
import com.github.myio.ServerLauncher;
import com.github.myio.dto.AnimalCellDto;
import com.github.myio.dto.ItemDto;
import com.github.myio.dto.MapEntityDto;
import com.github.myio.dto.PlayerCellDto;
import com.github.myio.entities.IoPoint;
import com.github.myio.entities.PlayerCell;
import com.github.myio.enums.GameServerState;
import com.github.myio.networking.Packet;
import com.github.myio.networking.PacketType;
import com.github.myio.networking.packet.GameStateChangedPacket;
import com.github.myio.networking.packet.InitialWorldSnapshotPacket;
import com.github.myio.networking.packet.LoginResponsePacket;
import com.github.myio.networking.packet.PlayerListPacket;
import com.github.myio.networking.packet.TeamLoginPacket;
import com.github.myio.squads.Squad;
import com.github.myio.tools.IoLogger;

import io.vertx.core.buffer.Buffer;
import io.vertx.core.http.ServerWebSocket;

import static com.github.myio.GameConstants.DebugMode;
import static com.github.myio.GameConstants.MIN_PLAYERS_TO_START_GAME;
import static com.github.myio.GameConstants.PLAYER_DEFAULT_RADIUS;

/**
 * Handles team joining,
 * At this point we  assuming that we already in right GameWorld
 * (@see {@link com.github.myio.LoadBalancer.assignTeamMemberToServer(ServerWebSocket , TeamLoginPacket) }
 *
 *  So here we just need to add player to right squad and handle autofill squad function
 */

public class TeamLoginHandler extends AbstractPacketHandler {
    public TeamLoginHandler(GameWorld world, PacketEmitter p) {
        super(world, p);
    }

    @Override
    public boolean handlePacket(Packet packet, ServerWebSocket clientSocket) {

        try {
            if(packet.packetType == PacketType.TEAM_LOGIN_PACKET)
            {
                TeamLoginPacket p = (TeamLoginPacket) packet;
                String playerName = p.playerName;

                String newPlayerId = ++world.playersId+"";

                PlayerCell newPlayer = new PlayerCell(playerName, newPlayerId , 0, 0);
                newPlayer.setClientId(((TeamLoginPacket) packet ).clientId);
                newPlayer.setPlayerSkinColor(p.skinPlayerColor);
                packetEmitter.getPlayersToSocket().put(newPlayer, clientSocket); // associate player  and his socket
                packetEmitter.getSocketToPlayer().put(clientSocket, newPlayer); // and other way around :D

                // need to spawn near spawnpoint taking in cout players nearby
                IoPoint spawnPosition = world.spawnNear(world.spawnOnPoint(), PLAYER_DEFAULT_RADIUS,1000);

                    int squadId = world.getSquadManager().addTeamMember(newPlayer, clientSocket, (TeamLoginPacket) packet);
                    Squad squad = world.getSquadManager().getSquadMap().get(squadId);
                    IoLogger.log(" squad for " + playerName+" is " + squad);
                    if(squad.getSquadLeader().equals(newPlayer)) //leader  spawns on spawnpoint
                    {
                        newPlayer.x = spawnPosition.x;
                        newPlayer.y = spawnPosition.y;
                    }
                    else
                    {
                        IoPoint point = world.spawnNear(squad.getSquadLeader().getPosition(), PLAYER_DEFAULT_RADIUS,1000);
                        newPlayer.x = point.x;
                        newPlayer.y = point.y;
                    }


                world.getStatsManager().addNewPlayer(newPlayer);
                newPlayer.setDefaultInventory(false,false);


                IoLogger.log(world.getWorldIdTag()+" ADDING NEW PLAYER: [" + newPlayerId+"]");
                world.getAlivePlayersMap().put(newPlayerId, newPlayer);
                world.getJustJoinedPlayers().add(EntityMapper.mapFull(newPlayer));

                LoginResponsePacket initialResponse = new LoginResponsePacket
                        ( newPlayer.getId(), playerName, newPlayer.getX(), newPlayer.getY(),
                                1, GameConstants.PLAYERS_MAX_HEALTH,GameConstants.PLAYERS_MAX_HEALTH,world.getWorldId(),0,p.skinPlayerColor);
                clientSocket.write(Buffer.buffer(ServerLauncher.serializer.serialize(initialResponse)));




                PlayerListPacket playersList = new PlayerListPacket();
                playersList.names = world.getAlivePlayersMap().values().stream().map(PlayerCell::getUsername).toArray(String[]::new);
                clientSocket.write(Buffer.buffer(ServerLauncher.serializer.serialize(playersList)));


                InitialWorldSnapshotPacket initial = new InitialWorldSnapshotPacket();

                initial.players = world.getAlivePlayersMap().values().stream().map(EntityMapper::mapFull).toArray(PlayerCellDto[]::new);
                initial.entities = world.getMapEntities().values().stream().map(EntityMapper::mapFullEntity).toArray(MapEntityDto[]::new);
                initial.items = world.getWorldItems().values().stream().map(EntityMapper::mapFullEntity).toArray(ItemDto[]::new);
                initial.animals = world.getAliveAnimals().values().stream().map(EntityMapper::mapFull).toArray(AnimalCellDto[]::new);

                if(DebugMode) IoLogger.log(world.getWorldIdTag()+"sending WORLD_SNAPSHOT" + initial.toString());
                clientSocket.write(Buffer.buffer(ServerLauncher.serializer.serialize(initial)));



                if(DebugMode)  IoLogger.log(world.getWorldIdTag()+ "sending TILE_SNAPSHOT ");
                clientSocket.write(Buffer.buffer(world.tilePacket));

                if (world.getServerState().equals(GameServerState.WAITING_FOR_PLAYERS) && world.getAlivePlayersMap().size() >= MIN_PLAYERS_TO_START_GAME)
                {
                    world.getGamemode().startWarmup();
                }

                else
                {
                    clientSocket.write(Buffer.buffer(ServerLauncher.serializer.serialize(
                            new GameStateChangedPacket(world.getServerState(), world.getGamemode().getGameType()))));
                }

                return true;
            }
        } catch (Exception e) {
            IoLogger.log(world.getWorldIdTag()+" TeamLoginHandler handlePacket" + e.getMessage());
            e.printStackTrace();
            IoLogger.log("Error packet " + packet);
        }

        return false;
    }
}
