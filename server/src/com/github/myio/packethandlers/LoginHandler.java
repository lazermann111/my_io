package com.github.myio.packethandlers;


import com.github.myio.EntityMapper;
import com.github.myio.GameConstants;
import com.github.myio.GameWorld;
import com.github.myio.PacketEmitter;
import com.github.myio.ServerLauncher;
import com.github.myio.dto.AnimalCellDto;
import com.github.myio.dto.ItemDto;
import com.github.myio.dto.MapEntityDto;
import com.github.myio.dto.PlayerCellDto;
import com.github.myio.dto.masterserver.GameType;
import com.github.myio.entities.IoPoint;
import com.github.myio.entities.PlayerCell;
import com.github.myio.enums.GameServerState;
import com.github.myio.networking.Packet;
import com.github.myio.networking.PacketType;
import com.github.myio.networking.packet.GameStateChangedPacket;
import com.github.myio.networking.packet.InitialWorldSnapshotPacket;
import com.github.myio.networking.packet.LoginPacket;
import com.github.myio.networking.packet.LoginResponsePacket;
import com.github.myio.networking.packet.PlayerListPacket;
import com.github.myio.squads.Squad;
import com.github.myio.tools.IoLogger;

import java.util.Random;

import io.vertx.core.buffer.Buffer;
import io.vertx.core.http.ServerWebSocket;

import static com.github.myio.GameConstants.DebugMode;
import static com.github.myio.GameConstants.MIN_PLAYERS_TO_START_GAME;
import static com.github.myio.GameConstants.PLAYER_DEFAULT_RADIUS;

public class LoginHandler  extends AbstractPacketHandler
{
    Random r = new Random();
    private byte[] tiles;

    public LoginHandler(GameWorld world, PacketEmitter e) {
       super(world,e);
        //tiles = world.tiles;
    }

    @Override
    public boolean handlePacket(Packet packet, ServerWebSocket clientSocket) {

        try {
            if(packet.packetType == PacketType.LOGIN)
            {
                if (DebugMode) IoLogger.log("LoginHandler handlePacket for socket " + clientSocket.hashCode());
                String namePlayer = ((LoginPacket) packet ).namePlayer;
                String skinPlayer = ((LoginPacket) packet ).skinPlayerColor;
                //IoLogger.log(namePlayer+" HANDLE PACKET NAME");

                String newPlayerId = ++world.playersId+"";

                PlayerCell newPlayer = new PlayerCell(namePlayer, newPlayerId , 0, 0);
                newPlayer.setClientId(((LoginPacket) packet ).clientId);
                newPlayer.setPlayerSkinColor(skinPlayer);

                packetEmitter.getPlayersToSocket().put(newPlayer, clientSocket); // associate player  and his socket
                packetEmitter.getSocketToPlayer().put(clientSocket, newPlayer); // and other way around :D

                processGameModeSpecificLogic(clientSocket, newPlayer);

                world.getStatsManager().addNewPlayer(newPlayer);


                newPlayer.setDefaultInventory(false,false);

                IoLogger.log(world.getWorldIdTag()+" ADDING NEW PLAYER: [" + newPlayerId+"]"+"  NAME:["+newPlayer.getUsername()+"]");
                world.getAlivePlayersMap().put(newPlayerId, newPlayer);
                world.getJustJoinedPlayers().add(EntityMapper.mapFull(newPlayer));


                LoginResponsePacket initialResponse = new LoginResponsePacket( newPlayer.getId(), namePlayer, newPlayer.getX(), newPlayer.getY(),
                                1, GameConstants.PLAYERS_MAX_HEALTH,GameConstants.PLAYERS_MAX_HEALTH,world.getWorldId(),0,newPlayer.playerSkinColor);
                clientSocket.write(Buffer.buffer(ServerLauncher.serializer.serialize(initialResponse)));


                PlayerListPacket playersList = new PlayerListPacket();
                playersList.names = world.getAlivePlayersMap().values().stream().map(PlayerCell::getUsername).toArray(String[]::new);
                clientSocket.write(Buffer.buffer(ServerLauncher.serializer.serialize(playersList)));


                InitialWorldSnapshotPacket initial = new InitialWorldSnapshotPacket();

                initial.players = world.getAlivePlayersMap().values().stream().map(EntityMapper::mapFull).toArray(PlayerCellDto[]::new);
                initial.entities = world.getMapEntities().values().stream().map(EntityMapper::mapFullEntity).toArray(MapEntityDto[]::new);
                initial.items = world.getWorldItems().values().stream().map(EntityMapper::mapFullEntity).toArray(ItemDto[]::new);
                initial.animals = world.getAliveAnimals().values().stream().map(EntityMapper::mapFull).toArray(AnimalCellDto[]::new);

                if(DebugMode) IoLogger.log(world.getWorldIdTag()+"sending WORLD_SNAPSHOT" + initial.toString());
                clientSocket.write(Buffer.buffer(ServerLauncher.serializer.serialize(initial)));



                if(DebugMode)  IoLogger.log(world.getWorldIdTag()+ "sending TILE_SNAPSHOT ");
                clientSocket.write(Buffer.buffer(world.tilePacket));

                if (world.getServerState().equals(GameServerState.WAITING_FOR_PLAYERS) && world.getAlivePlayersMap().size() >= MIN_PLAYERS_TO_START_GAME)
                {
                    world.getGamemode().startWarmup();
                }
                else
                {
                    clientSocket.write(Buffer.buffer(ServerLauncher.serializer.serialize(
                            new GameStateChangedPacket(world.getServerState(), world.getGamemode().getGameType()))));
                }

                if(DebugMode)  IoLogger.log(world.getWorldIdTag()+ "Login sucessfull :"+newPlayer.getUsername());
                return true;
            }
        } catch (Exception e) {
            IoLogger.log(world.getWorldIdTag()+" LoginHandler handlePacket" + e.getMessage());
            e.printStackTrace();
        }

        return false;
    }


    private void processGameModeSpecificLogic(ServerWebSocket clientSocket, PlayerCell newPlayer)
    {
        // need to spawn near spawnpoint taking into account players nearby
        IoPoint spawnPosition = world.spawnNear(world.spawnOnPoint(), PLAYER_DEFAULT_RADIUS,1000);
        newPlayer.x = spawnPosition.x;
        newPlayer.y = spawnPosition.y;

        if(world.getGamemode().hasSquads())
        {
            if(world.getGamemode().getGameType().equals(GameType.TWO_TEAMS) || world.getGamemode().getGameType().equals(GameType.CONQUEST))
            {
                String playerColor = world.getTwoTeamsManager().addPlayerToRandomTeam(newPlayer, clientSocket);
                newPlayer.setPlayerSkinColor(playerColor);
            }
            else
            {
                int squadId = world.getSquadManager().addNewPlayerToRandomSquad(newPlayer, clientSocket);
                Squad squad = world.getSquadManager().getSquadMap().get(squadId);

                if(!squad.getSquadLeader().equals(newPlayer)) // squad member spawns near Squad leader
                {
                    IoPoint point = world.spawnNear(squad.getSquadLeader().getPosition(), PLAYER_DEFAULT_RADIUS,1000);
                    newPlayer.x = point.x;
                    newPlayer.y = point.y;
                }
            }

        }
    }
}
