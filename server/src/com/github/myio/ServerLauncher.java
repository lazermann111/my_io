package com.github.myio;

import com.github.czyzby.websocket.serialization.impl.ManualSerializer;
import com.github.myio.dto.masterserver.GameType;
import com.github.myio.networking.packet.CapturablePointStateChangedPacket;
import com.github.myio.networking.packet.CapturablePointsSnapshot;
import com.github.myio.networking.packet.CurrentWorldSnapshotPacket;
import com.github.myio.networking.packet.CurrentWorldSnapshotRequest;
import com.github.myio.networking.packet.DealDamagePacket;
import com.github.myio.networking.packet.EmotePacket;
import com.github.myio.networking.packet.EntityAddPacket;
import com.github.myio.networking.packet.EntityRemovePacket;
import com.github.myio.networking.packet.GameStateChangedPacket;
import com.github.myio.networking.packet.HeartBeatDisablePacket;
import com.github.myio.networking.packet.HeartBeatPacket;
import com.github.myio.networking.packet.InitialTileSnapshotPacket;
import com.github.myio.networking.packet.InitialWorldSnapshotPacket;
import com.github.myio.networking.packet.ItemDroppedPacket;
import com.github.myio.networking.packet.ItemPickedPacket;
import com.github.myio.networking.packet.LoginPacket;
import com.github.myio.networking.packet.LoginResponsePacket;
import com.github.myio.networking.packet.SuicidePacket;
import com.github.myio.networking.packet.PitInteractionPacker;
import com.github.myio.networking.packet.PlayerDiedPacket;
import com.github.myio.networking.packet.PlayerListPacket;
import com.github.myio.networking.packet.PlayerRevivingRequest;
import com.github.myio.networking.packet.PlayerStatsPacket;
import com.github.myio.networking.packet.ProjectileLaunchedPacket;
import com.github.myio.networking.packet.PvPInfoPacket;
import com.github.myio.networking.packet.ServerErrorPacket;
import com.github.myio.networking.packet.ShieldInteractionPacket;
import com.github.myio.networking.packet.ShotgunShotPacket;
import com.github.myio.networking.packet.SquadChangedPacket;
import com.github.myio.networking.packet.TeamLoginPacket;
import com.github.myio.networking.packet.TimerGamePacket;
import com.github.myio.networking.packet.UseAmmoPacket;
import com.github.myio.networking.packet.UseMedkitPacket;
import com.github.myio.networking.packet.VisibilityChangePacket;
import com.github.myio.networking.packet.VoiceCommandsMenuPacket;
import com.github.myio.networking.packet.WeaponSwitchedPacket;
import com.github.myio.networking.packet.WinPacket;
import com.github.myio.networking.packet.WorldUpdatePacket;
import com.github.myio.tools.IoLogger;
import com.github.myio.webserver.AbstractWebServer;
import com.github.myio.webserver.HerokuWebServer;
import com.github.myio.webserver.LocalWebServer;

import java.util.Random;

import static com.github.myio.GameConstants.GAME_WORLD_PER_SERVER;
import static com.github.myio.GameConstants.PRODUCTION_MODE;

//Note that server web socket implementation is not provided by gdx-websocket. This class uses an external library: Vert.x.
public class ServerLauncher {



    public static ManualSerializer serializer;
    Random r = new Random();

    private LoadBalancer loadBalancer;

    //todo find way how to change it
    private static GameType gameType = GameConstants.DEFAULT_GAME_TYPE; // one for all gameWorlds, to avoid mess


    public ServerLauncher() throws InterruptedException
    {





        serializer = new ManualSerializer();
        serializer.register(new HeartBeatPacket());
        serializer.register(new WorldUpdatePacket());
        serializer.register(new DealDamagePacket());
        serializer.register(new EmotePacket());
        serializer.register(new EntityAddPacket());
        serializer.register(new EntityRemovePacket());
        serializer.register(new ItemPickedPacket());
        serializer.register(new InitialTileSnapshotPacket());
        serializer.register(new InitialWorldSnapshotPacket());
        serializer.register(new LoginPacket());
        serializer.register(new LoginResponsePacket());
        serializer.register(new SuicidePacket());
        serializer.register(new PlayerListPacket());
        serializer.register(new ProjectileLaunchedPacket());
        serializer.register(new WeaponSwitchedPacket());
        serializer.register(new PvPInfoPacket());
        serializer.register(new TimerGamePacket());
        serializer.register(new ShieldInteractionPacket());
        serializer.register(new PitInteractionPacker());
        serializer.register(new VisibilityChangePacket());
        serializer.register(new WinPacket());
        serializer.register(new GameStateChangedPacket());
        serializer.register(new SquadChangedPacket());
      //  serializer.register(new GameStateChangedPacket());
        serializer.register(new UseMedkitPacket());
        serializer.register(new TeamLoginPacket());

        serializer.register(new CurrentWorldSnapshotRequest());
        serializer.register(new CurrentWorldSnapshotPacket());
        serializer.register(new ShotgunShotPacket());
        serializer.register(new HeartBeatDisablePacket());
        serializer.register(new PlayerDiedPacket());
        serializer.register(new PlayerStatsPacket());
        serializer.register(new PlayerRevivingRequest());
        serializer.register(new ItemDroppedPacket());

        serializer.register(new ServerErrorPacket());
        serializer.register(new CapturablePointStateChangedPacket());
        serializer.register(new CapturablePointsSnapshot());
        serializer.register(new VoiceCommandsMenuPacket());
        serializer.register(new UseAmmoPacket());

        //Thread.sleep(2000l);
        loadBalancer = new LoadBalancer(GAME_WORLD_PER_SERVER, serializer);



            //https://stackoverflow.com/questions/40412623/java-net-unknownhostexception-failed-to-resolve-inventory-microservice-excee
        System.setProperty("vertx.disableDnsResolver", "true");
    }

    public static void main(final String... args) throws Exception {
        GameConstants.isServer = true;

        IoLogger.log("Launching web socket server...");

        ServerLauncher gameServer = new ServerLauncher();

        AbstractWebServer httpServer  = PRODUCTION_MODE ? new HerokuWebServer(gameServer): new LocalWebServer(gameServer);
        httpServer.run();

    }


    /*
    * We are restarting only empty servers,
    * those which still have players, will restart with new gameType next time
    * */
    public void restartEmptyGameServers(GameType type)
    {
        loadBalancer.getGameWorlds().stream().filter(w -> w.getAlivePlayersMap().isEmpty()).forEach(w -> w.restart(type));
    }


    public void restartAllGameServers(GameType type)
    {
        loadBalancer.getGameWorlds().forEach(w -> w.restart(type));
    }

    public LoadBalancer getLoadBalancer() {
        return loadBalancer;
    }

    public ManualSerializer getSerializer() {
        return serializer;
    }


    public static GameType getGameType() {
        return gameType;
    }

    public static void setGameType(GameType g) {
        gameType = g;
    }
}
