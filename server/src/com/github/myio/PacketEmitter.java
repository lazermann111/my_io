package com.github.myio;

import com.badlogic.gdx.math.Circle;
import com.github.myio.entities.IoPoint;
import com.github.myio.entities.PlayerCell;
import com.github.myio.networking.Packet;
import com.github.myio.tools.IoLogger;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import io.vertx.core.buffer.Buffer;
import io.vertx.core.http.ServerWebSocket;

import static com.github.myio.GameConstants.PLAYER_SIGHT_RADIUS;

/**
 * Emits packet to all client sockets
 */

public class PacketEmitter
{


    private final Map<PlayerCell, ServerWebSocket> playersToSocket = new ConcurrentHashMap<>();
    private final Map<ServerWebSocket, PlayerCell> socketToPlayer = new ConcurrentHashMap<>();
    private final GameWorld world;

    public Circle collisionCircle = new Circle(0,0, PLAYER_SIGHT_RADIUS*2);

    private Set<ServerWebSocket> closedSockets = new HashSet<>();

    public PacketEmitter(GameWorld world) {
        this.world = world;
    }

    public void emitToAllPlayers(Packet packet)
    {


            byte[] dataTosend = ServerLauncher.serializer.serialize(packet);
            for (ServerWebSocket s: socketToPlayer.keySet())
            {
                try
                {
                     s.write(Buffer.buffer(dataTosend));
                }
                catch (IllegalStateException e)
                {

                    IoLogger.log(world.getWorldIdTag()+ " emitToAllPlayers Closed socket detected!");
                    closedSockets.add(s);

                }
            }

        closedSockets.forEach(s ->
        {
            world.getLoadBalancer().removeClientSocket(s);
            socketToPlayer.remove(s);
        });
        closedSockets.clear();
    }

    public void restart()
    {
        playersToSocket.clear();
        socketToPlayer.clear();
    }


    public void emitToNearestPlayers(Packet packet, IoPoint origin)
    {
        collisionCircle.setPosition(origin.getX(), origin.getY());
        byte[] dataTosend = ServerLauncher.serializer.serialize(packet);

        for (PlayerCell playerCell : playersToSocket.keySet())
            if(collisionCircle.contains(playerCell.getX(), playerCell.getY()))
            {

                try {
                    playersToSocket.get(playerCell).write(Buffer.buffer(dataTosend));
                } catch (IllegalStateException e) {
                    IoLogger.log(world.getWorldIdTag()+ " emitToNearestPlayers Closed socket detected!");
                    closedSockets.add( playersToSocket.get(playerCell));
                }

            }

        closedSockets.forEach(s -> {
            world.getLoadBalancer().removeClientSocket(s);
        });
        closedSockets.clear();

    }

    public Map<PlayerCell, ServerWebSocket> getPlayersToSocket() {
        return playersToSocket;
    }

    public Map<ServerWebSocket, PlayerCell> getSocketToPlayer() {
        return socketToPlayer;
    }
}
