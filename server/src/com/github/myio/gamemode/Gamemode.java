package com.github.myio.gamemode;


import com.github.myio.GameWorld;
import com.github.myio.dto.masterserver.GameType;
import com.github.myio.enums.GameServerState;
import com.github.myio.networking.packet.GameStateChangedPacket;
import com.github.myio.tools.IoLogger;

import java.util.Timer;
import java.util.TimerTask;

import static com.github.myio.GameConstants.WARMUP_PHASE_DURATION;

public abstract class Gamemode
{
    protected GameWorld gameWorld;

    public Gamemode(GameWorld gameWorld) {
        this.gameWorld = gameWorld;
    }

    public abstract void checkWinCondition();

    public  void processGamemodeLogic(float delta)
    {

    }

    public abstract boolean hasSquads();
    public abstract boolean isBattleRoyale();
    public abstract GameType getGameType();



    public void startWarmup()
    {
        IoLogger.log(gameWorld.getWorldIdTag() + " warmup started!");
        gameWorld.serverState = GameServerState.WARMUP;
        gameWorld.getPacketEmitter().emitToAllPlayers(new GameStateChangedPacket(GameServerState.WARMUP, getGameType()));


        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                startRound();
            }
        }, WARMUP_PHASE_DURATION);
    }

    public void startRound()
    {
        IoLogger.log(gameWorld.getWorldIdTag() + " round started!");
        gameWorld.setServerTime(1);
        gameWorld.serverState = GameServerState.GAME_IN_PROGRESS;
        gameWorld.getPacketEmitter().emitToAllPlayers(new GameStateChangedPacket(GameServerState.GAME_IN_PROGRESS, getGameType()));
    }


}
