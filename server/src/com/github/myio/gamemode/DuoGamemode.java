package com.github.myio.gamemode;

import com.github.myio.GameWorld;
import com.github.myio.ServerLauncher;
import com.github.myio.dto.masterserver.GameType;
import com.github.myio.entities.PlayerCell;
import com.github.myio.enums.GameServerState;
import com.github.myio.networking.packet.WinPacket;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by Igor on 4/3/2018.
 */

public class DuoGamemode extends Gamemode {
    public DuoGamemode(GameWorld gameWorld) {
        super(gameWorld);
    }

    @Override
    public void checkWinCondition() {

        if (gameWorld.getSquadManager().getSquadMap().size() > 1) return;
        if(!gameWorld.getServerState().equals(GameServerState.GAME_IN_PROGRESS)) return;

        if(!gameWorld.getAlivePlayersMap().isEmpty())
        {

            for (PlayerCell p : gameWorld.getAlivePlayersMap().values())
            {
                gameWorld.getStatsManager().sendWinStat(p);
            }
            for (PlayerCell p : gameWorld.getAlivePlayersMap().values())
            {
                gameWorld.getPacketEmitter().emitToAllPlayers(new WinPacket(p.getId()));
            }
        }

        gameWorld.serverState = GameServerState.LAUNCHING;

        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                gameWorld.restart(ServerLauncher.getGameType());
            }
        }, 5000);
    }

    @Override
    public boolean hasSquads() {
        return true;
    }

    @Override
    public boolean isBattleRoyale()
    {
        return true;
    }

    @Override
    public GameType getGameType() {
        return GameType.DUO;
    }


}
