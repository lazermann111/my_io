package com.github.myio.gamemode;

import com.github.myio.GameWorld;
import com.github.myio.ServerLauncher;
import com.github.myio.conquest.CapturablePointsManager;
import com.github.myio.dto.masterserver.GameType;
import com.github.myio.entities.PlayerCell;
import com.github.myio.enums.GameServerState;
import com.github.myio.spawnpoints.RespawnManager;

import java.util.Timer;
import java.util.TimerTask;

import static com.github.myio.GameConstants.CONQUEST_POINT_TO_WIN;


public class ConquestGamemode extends Gamemode
{

    CapturablePointsManager capturablePointsManager ;
    RespawnManager respawnManager;




    public ConquestGamemode(GameWorld gameWorld)
    {

        super(gameWorld);
        capturablePointsManager = new CapturablePointsManager(gameWorld);
        respawnManager = new RespawnManager();
    }

    @Override
    public void checkWinCondition()
    {
        if (gameWorld.getSquadManager().getSquadMap().size() > 1) return;
        if(!gameWorld.getServerState().equals(GameServerState.GAME_IN_PROGRESS)) return;

        if(capturablePointsManager.getRedTeamPoints() >= CONQUEST_POINT_TO_WIN)
        {

            for (PlayerCell p : gameWorld.getTwoTeamsManager().getRedTeam().getMembers())
            {
               //victory todo

            }

            for (PlayerCell p : gameWorld.getTwoTeamsManager().getBlueTeam().getMembers())
            {
                //fail todo

            }
        }

        else if(capturablePointsManager.getBlueTeamPoints() >= CONQUEST_POINT_TO_WIN)
        {

            for (PlayerCell p : gameWorld.getTwoTeamsManager().getBlueTeam().getMembers())
            {
                //victory todo

            }

            for (PlayerCell p : gameWorld.getTwoTeamsManager().getRedTeam().getMembers())
            {
                //fail todo

            }
        }
        else return;

        gameWorld.serverState = GameServerState.LAUNCHING;

        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                gameWorld.restart(ServerLauncher.getGameType());
            }
        }, 5000);
    }

    @Override
    public  void processGamemodeLogic(float delta)
    {
        capturablePointsManager.update(delta);
    }

    @Override
    public boolean hasSquads()
    {
        return true;
    }

    @Override
    public boolean isBattleRoyale()
    {
        return false;
    }

    @Override
    public GameType getGameType()
    {
        return GameType.CONQUEST;
    }


    public CapturablePointsManager getCapturablePointsManager()
    {
        return capturablePointsManager;
    }

    public RespawnManager getRespawnManager()
    {
        return respawnManager;
    }
}
