package com.github.myio.gamemode;

import com.github.myio.GameWorld;
import com.github.myio.ServerLauncher;
import com.github.myio.dto.masterserver.GameType;
import com.github.myio.enums.GameServerState;
import com.github.myio.networking.packet.WinPacket;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by Igor on 4/3/2018.
 */

public class SoloGamemode extends Gamemode {


    public SoloGamemode(GameWorld gameWorld) {
        super(gameWorld);
    }

    @Override
    public void checkWinCondition()
    {
        if (gameWorld.getAlivePlayersMap().size() > 1) return;
        if(!gameWorld.getServerState().equals(GameServerState.GAME_IN_PROGRESS)) return;

        if(!gameWorld.getAlivePlayersMap().isEmpty())
        {
            gameWorld.getPacketEmitter().emitToAllPlayers(new WinPacket(gameWorld.getAlivePlayersMap().values().iterator().next().getId()));
            gameWorld.getStatsManager().sendWinStat(gameWorld.getAlivePlayersMap().values().iterator().next());
            gameWorld.serverState = GameServerState.LAUNCHING;

        }

        gameWorld.serverState = GameServerState.LAUNCHING;

        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                gameWorld.restart(ServerLauncher.getGameType());
            }
        }, 5000);

    }

    @Override
    public boolean hasSquads() {
        return false;
    }

    @Override
    public GameType getGameType() {
        return GameType.SOLO;
    }


    @Override
    public boolean isBattleRoyale()
    {
        return true;
    }
}
