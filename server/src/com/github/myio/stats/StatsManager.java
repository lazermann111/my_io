package com.github.myio.stats;


import com.github.myio.GameWorld;
import com.github.myio.ServerLauncher;
import com.github.myio.dto.PlayerSessionStatsDto;
import com.github.myio.entities.PlayerCell;
import com.github.myio.networking.packet.PlayerStatsPacket;
import com.github.myio.tools.IoLogger;

import java.util.HashMap;
import java.util.Map;

import io.vertx.core.buffer.Buffer;

public class StatsManager
{


    private GameWorld gameWorld;
    private Map<String,PlayerSessionStatsDto> sessionStatsMap = new HashMap<>();

    public StatsManager(GameWorld gameWorld) {
        this.gameWorld = gameWorld;
    }

    public void addNewPlayer(PlayerCell player)
    {
        sessionStatsMap.put(player.getId(), new PlayerSessionStatsDto(player.getId(), player.getClientId()));
    }

    public void removePlayer(PlayerCell player)
    {
        sessionStatsMap.remove(player.getId());
    }

    public void handleDamageStats(String shooterId, String victimId, int damage)
    {
        PlayerCell shooter = gameWorld.getAlivePlayersMap().get(shooterId);
        PlayerCell victim = gameWorld.getAlivePlayersMap().get(victimId);

        if(shooter == null ){
            IoLogger.log("Stats manager null shooter!");
        }
        else
        {
            PlayerSessionStatsDto shooterStats = sessionStatsMap.get(shooter.getId());
            shooterStats.dealtDamage += damage;
        }

        if(victim == null ){
            IoLogger.log("Stats manager null victim!");
        }
        else
        {
            PlayerSessionStatsDto victimStats = sessionStatsMap.get(victim.getId());
            victimStats.takenDamage += damage;
        }
    }

    public void RegisterKill(PlayerCell killer, PlayerCell victim)
    {
        PlayerSessionStatsDto killerStats = sessionStatsMap.get(killer.getId());
        if(killerStats == null){
            IoLogger.log("RegisterKill null victimStats");
        }
        else{
            killerStats.killsNumber++;
        }


        //damaged should be handled before that in handleDamageStats

        PlayerSessionStatsDto victimStats = sessionStatsMap.get(victim.getId());
        if(victimStats == null){
            IoLogger.log("RegisterKill null victimStats");
            return;
        }


        victimStats.survivedFor = (int) ((System.currentTimeMillis() - victimStats.getGameStartTime())/1000);
        if(gameWorld.getGamemode().hasSquads() )
        {
            if(gameWorld.getSquadManager().getSquadMap().get(victim.getSquadId()) != null &&
                    gameWorld.getSquadManager().getSquadMap().get(victim.getSquadId()).getMembers() != null)
            {
                for (PlayerCell member : gameWorld.getSquadManager().getSquadMap().get(victim.getSquadId()).getMembers())
                {
                    if(member != null)
                    {
                        sendStatsToPlayer(victim, member);
                    }
                }
            }
            else
            {
                IoLogger.log(" RegisterKill No squad for player " + victim);
                IoLogger.log(" RegisterKill No squad for player, squad " + victim.getSquadId());
            }
        }



        else
        {
            sendStatsToPlayer(victim, victim);
        }
       // todo post to master server
    }

    public void sendWinStat(PlayerCell winner) {
        PlayerSessionStatsDto winnerStats = sessionStatsMap.get(winner.getId());
        if(winnerStats == null){
            IoLogger.log("sendWinStat null victimStats");
            return;
        }
        winnerStats.survivedFor = (int) ((System.currentTimeMillis() - winnerStats.getGameStartTime())/1000);
        if(gameWorld.getGamemode().hasSquads())

        {
                   if( gameWorld.getSquadManager().getSquadMap().get(winner.getSquadId()) != null && gameWorld.getSquadManager().getSquadMap().get(winner.getSquadId()).getMembers() != null)
                    {
                    for ( PlayerCell member : gameWorld.getSquadManager().getSquadMap().get(winner.getSquadId()).getMembers()) {
                        if(member != null)
                        {
                            //System.out.println("sendWinStat   winner   " + winner.getUsername() + "|  member  " + member.getUsername());
                            sendStatsToPlayer(winner, member);
                        }
                    }
                    }
                   else
                   {
                       IoLogger.log(" sendWinStat No squad for player " + winner);
                       IoLogger.log(" sendWinStat No squad for player, squad " + winner.getSquadId());
                   }

        }
        else
        {
            sendStatsToPlayer(winner, winner);
        }
    }


    /**
     *
     * @param player player
     * @param recipient player which will receive these stats, it can differ from @player if we have squads
     */
    public void sendStatsToPlayer(PlayerCell player, PlayerCell recipient)
    {
        PlayerSessionStatsDto playerStat = sessionStatsMap.get(player.getId());
        if(playerStat == null)
        {
            IoLogger.log("sendStatsToPlayer null victimStats");
        }
        else
        {

            try
            {
                gameWorld.getPacketEmitter().getPlayersToSocket().get(recipient)
                        .write(Buffer.buffer(ServerLauncher.serializer
                                .serialize(new PlayerStatsPacket(playerStat, player.getId()))));
            }
            catch (Exception e) //closed socket or null socket
            {
                e.printStackTrace();
                gameWorld.getLoadBalancer().removeClientSocket(gameWorld.getPacketEmitter().getPlayersToSocket().get(recipient));
            }
        }
    }
}
