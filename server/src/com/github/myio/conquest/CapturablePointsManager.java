package com.github.myio.conquest;


import com.github.myio.EntityMapper;
import com.github.myio.GameWorld;
import com.github.myio.dto.CapturablePointDto;
import com.github.myio.networking.packet.CapturablePointsSnapshot;
import com.github.myio.tools.IoLogger;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class CapturablePointsManager
{
    public List<CapturablePoint> capturablePoints = new ArrayList<>();
    private GameWorld gameWorld;

    private int redTeamPoints, blueTeamPoints;

    public static int UPDATE_FREQUENCY = 500;
    private long lastUpdate;


    public CapturablePointsManager(GameWorld gameWorld)
    {
        this.gameWorld = gameWorld;
    }

    public void update(float delta)
    {
        if(System.currentTimeMillis() - lastUpdate < UPDATE_FREQUENCY) return;

        lastUpdate = System.currentTimeMillis();

        for (CapturablePoint point : capturablePoints)
        {
            point.update(0);

            if(point.ownedByRedTeam())

            {
                redTeamPoints++;
                IoLogger.log("redTeamPoints " + redTeamPoints);
            }

            if(point.ownedByBlueTeam())
            {

                blueTeamPoints++;
                IoLogger.log("blueTeamPoints " + blueTeamPoints);
            }
        }

        sendCapturablePointsSnapshot();

    }


   private void sendCapturablePointsSnapshot()
   {
       ArrayList<CapturablePointDto> points = new ArrayList<>(capturablePoints.stream().map(EntityMapper::mapFull).collect(Collectors.toList()));
       CapturablePointDto[] dto = Arrays.copyOf(points.toArray(), points.size(), CapturablePointDto[].class);

       gameWorld.getPacketEmitter().emitToAllPlayers(new CapturablePointsSnapshot(dto, redTeamPoints,blueTeamPoints));
   }


    public List<CapturablePoint> getCapturablePoints()
    {
        return capturablePoints;
    }

    public int getRedTeamPoints()
    {
        return redTeamPoints;
    }

    public int getBlueTeamPoints()
    {
        return blueTeamPoints;
    }
}
