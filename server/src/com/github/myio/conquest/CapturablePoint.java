package com.github.myio.conquest;

import com.badlogic.gdx.math.Circle;
import com.github.myio.GameWorld;
import com.github.myio.entities.PlayerCell;
import com.github.myio.enums.CapturablePointState;
import com.github.myio.networking.packet.CapturablePointStateChangedPacket;
import com.github.myio.squads.Squad;
import com.github.myio.tools.IoLogger;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static com.github.myio.GameConstants.CONQUEST_CAPTURABLE_POINT_INTERACTION_RADIUS;

public class CapturablePoint
{
    private static int pointIdGenerator;

    private GameWorld  gameWorld;

    public String id;
    public String name;
    public float x,y;

    private List<PlayerCell>  nearbyPlayers = new ArrayList<>();
    private Circle pointCircle;

    public CapturablePoint(GameWorld gameWorld, float x, float y, String id)
    {
        this. id = id;
        this.gameWorld = gameWorld;
        this.x = x;
        this.y = y;
        pointsToCapture = 100;
        interactionRadius = CONQUEST_CAPTURABLE_POINT_INTERACTION_RADIUS;
        pointCircle = new Circle(x,y, interactionRadius);
    }

    // in Conquest mode 2 teams represented by 2 squads
    // owner is null by default - neutral point
    public Squad owner;

    //
    public int pointsToCapture;

    //
    public int capturingProgress;

    public int interactionRadius;

    public Squad capturingTeam;

    public boolean captureInProgress;



    public void update(float delta)
    {
        updateNearbyPlayers();

        List<PlayerCell> redTeamPlayers = nearbyPlayers.stream().filter(p -> p.getSquadId() ==
                gameWorld.getTwoTeamsManager().getRedTeam().getId()).collect(Collectors.toList());

        List<PlayerCell> blueTeamPlayers = nearbyPlayers.stream().filter(p -> p.getSquadId() ==
                gameWorld.getTwoTeamsManager().getBlueTeam().getId()).collect(Collectors.toList());

        //System.out.println(" redTeamPlayers " + redTeamPlayers.size());
        //System.out.println(" blueTeamPlayers " + blueTeamPlayers.size());

        //neutral point
        if(owner == null)
        {
          if(redTeamPlayers.isEmpty() && !blueTeamPlayers.isEmpty())
          {
              startPointCapture(gameWorld.getTwoTeamsManager().getBlueTeam());
          }
          if(!redTeamPlayers.isEmpty() && blueTeamPlayers.isEmpty())
          {
              startPointCapture(gameWorld.getTwoTeamsManager().getRedTeam());
          }
        }

        //point capture in progress
        if(capturingTeam != null)
        {
            //capturingTeam changed
            if(capturingTeam.getId() == gameWorld.getTwoTeamsManager().getBlueTeam().getId()
                    && blueTeamPlayers.isEmpty() && !redTeamPlayers.isEmpty())
            {
                startPointCapture(gameWorld.getTwoTeamsManager().getRedTeam());
            }

            //capturingTeam changed
            else if(capturingTeam.getId() == gameWorld.getTwoTeamsManager().getRedTeam().getId()
                    && redTeamPlayers.isEmpty() && !blueTeamPlayers.isEmpty())
            {
                startPointCapture(gameWorld.getTwoTeamsManager().getBlueTeam());
            }


            else capturingProgress += 10;
        }



        if(capturingProgress >= pointsToCapture)
        {
            pointCaptured(capturingTeam);
        }

        if(capturingTeam != null && redTeamPlayers.isEmpty() && blueTeamPlayers.isEmpty())
        {
            cancelPointCapture();
        }


    }


    public void startPointCapture(Squad team)
    {
        IoLogger.log("startPointCapture " + id);
        IoLogger.log("startPointCapture team " + team.getId());

        capturingProgress = 0;
        captureInProgress = true;
        capturingTeam = team;

        sendNewPointStatus(CapturablePointState.POINT_CAPTURING_STARTED);
    }

    public void cancelPointCapture()
    {
        IoLogger.log("cancelPointCapture " + id);

        capturingProgress = 0;
        captureInProgress = false;
        capturingTeam = null;

        sendNewPointStatus(CapturablePointState.POINT_CAPTURING_CANCELED);
    }

    public void pointCaptured(Squad team)
    {
        IoLogger.log("pointCaptured " + id);
        IoLogger.log("pointCaptured team " + team.getId());

        owner = team;
        capturingProgress = 0;
        capturingTeam = null;
        captureInProgress = false;

        sendNewPointStatus(CapturablePointState.POINT_CAPTURED);
    }

    private void sendNewPointStatus(CapturablePointState newState)
    {
            gameWorld.getPacketEmitter().emitToAllPlayers(new CapturablePointStateChangedPacket(newState,id));
    }


    private boolean isRedTeam(Squad team)
    {
        return team.getId() == gameWorld.getTwoTeamsManager().getRedTeam().getId();
    }

    private boolean isBlueTeam(Squad team)
    {
        return team.getId() == gameWorld.getTwoTeamsManager().getBlueTeam().getId();
    }

    public boolean ownedByRedTeam()
    {
        return owner != null && isRedTeam(owner);
    }

    public boolean ownedByBlueTeam()
    {
        return owner != null && isBlueTeam(owner);
    }


    public List<PlayerCell> getNearbyPlayers()
    {
        return nearbyPlayers;
    }

    private void updateNearbyPlayers()
    {
        nearbyPlayers.clear();

        for (PlayerCell c : gameWorld.getAlivePlayersMap().values())
        {
            if(pointCircle.contains(c.x, c.y))
                nearbyPlayers.add(c);
        }
    }


    public static int getPointIdGenerator()
    {
        return pointIdGenerator;
    }

    public GameWorld getGameWorld()
    {
        return gameWorld;
    }

    public String getId()
    {
        return id;
    }

    public String getName()
    {
        return name;
    }

    public float getX()
    {
        return x;
    }

    public float getY()
    {
        return y;
    }

    public Circle getPointCircle()
    {
        return pointCircle;
    }

    public Squad getOwner()
    {
        return owner;
    }

    public int getPointsToCapture()
    {
        return pointsToCapture;
    }

    public int getCapturingProgress()
    {
        return capturingProgress;
    }

    public int getInteractionRadius()
    {
        return interactionRadius;
    }

    public Squad getCapturingTeam()
    {
        return capturingTeam;
    }

    public boolean isCaptureInProgress()
    {
        return captureInProgress;
    }

    @Override
    public String toString()
    {
        return "CapturablePoint{" +
                "id=" + id +
                ", x=" + x +
                ", y=" + y +
                ", owner=" + owner +
                ", capturingProgress=" + capturingProgress +
                ", interactionRadius=" + interactionRadius +
                ", capturingTeam=" + capturingTeam +
                ", captureInProgress=" + captureInProgress +
                '}';
    }
}
