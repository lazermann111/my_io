package com.github.myio.squads;

import com.github.myio.GameWorld;
import com.github.myio.entities.PlayerCell;
import com.github.myio.enums.GameServerState;
import com.github.myio.tools.IoLogger;

import io.vertx.core.http.ServerWebSocket;

import static com.github.myio.GameConstants.CONQUEST_BLUE_TEAM_ID;
import static com.github.myio.GameConstants.CONQUEST_RED_TEAM_ID;
import static com.github.myio.StringConstants.SKIN_DARKBLUE_DEFAULT;
import static com.github.myio.StringConstants.SKIN_RED_DEFAULT;



public class TwoTeamsManager extends SquadManager {

    private Squad redTeam;
    private Squad blueTeam;

    private String redTeamColor = SKIN_RED_DEFAULT ;
    private String blueTeamColor = SKIN_DARKBLUE_DEFAULT ;

    public TwoTeamsManager(GameWorld gameWorld, int squadSizes) {
        super(gameWorld, squadSizes);

        redTeam = new Squad(50);
        blueTeam = new Squad(50);

        redTeam.setId(CONQUEST_RED_TEAM_ID);
        blueTeam.setId(CONQUEST_BLUE_TEAM_ID);

        squadMap.clear();
        squadMap.put(redTeam.getId(), redTeam);
        squadMap.put(blueTeam.getId(), blueTeam);
    }



    /**
     * Used only in TwoTeams gamemode
     * @return color of team
     */
    public String addPlayerToRandomTeam(PlayerCell playerCell, ServerWebSocket playerSocket)
    {
        Squad team = redTeam.getMembers().size() > blueTeam.getMembers().size() ? blueTeam : redTeam;

        team.getMembers().add(playerCell);
        playerCell.setSquadId(team.getId());
        if(team.getSquadSockets().containsKey(playerCell))
        {
            team.getSquadSockets().remove(playerCell);
        }
        team.getSquadSockets().putIfAbsent(playerCell, playerSocket);
        playerCellSquadMap.put(playerCell, team);


        IoLogger.log("addPlayerToRandomTeam = " + toString());
        return team.equals(redTeam) ? redTeamColor : blueTeamColor;
    }


    @Override
    public void removePlayer(PlayerCell playerCell)
    {
        Squad playerSquad = playerCellSquadMap.get(playerCell);
        IoLogger.log("removePlayer = " + playerCell.toString());
        if(playerSquad == null /*&& DebugMode*/)
        {
            IoLogger.log("remove player null squad!");
            IoLogger.log("playerCell = " + playerCell);
            IoLogger.log("playerCellSquadMap size = " + playerCellSquadMap.size());
            return;
        }

        playerSquad.getMembers().remove(playerCell);
        playerSquad.getSquadSockets().remove(playerCell);
        playerCellSquadMap.remove(playerCell);


        //  need to remove team only when game is in progress
        if(playerSquad.getMembers().isEmpty() && gameWorld.getServerState().equals(GameServerState.GAME_IN_PROGRESS))
        {
            squadMap.remove(playerSquad.getId());
        }

        IoLogger.log("removePlayer = " + toString());
    }


    public Squad getRedTeam()
    {
        return redTeam;
    }

    public Squad getBlueTeam()
    {
        return blueTeam;
    }

    @Override
    public String toString()
    {
        return "TwoTeamsManager{" +
                "redTeam=" + redTeam +
                ", blueTeam=" + blueTeam +
                "} " + super.toString();
    }
}
