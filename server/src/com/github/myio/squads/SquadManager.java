package com.github.myio.squads;

import com.github.myio.GameWorld;
import com.github.myio.ServerLauncher;
import com.github.myio.entities.PlayerCell;
import com.github.myio.networking.Packet;
import com.github.myio.networking.packet.SquadChangedPacket;
import com.github.myio.networking.packet.TeamLoginPacket;
import com.github.myio.tools.IoLogger;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import io.vertx.core.buffer.Buffer;
import io.vertx.core.http.ServerWebSocket;

import static com.github.myio.GameConstants.DebugMode;

/**
 * Server-side manager, used in all gamemodes
 */

public class SquadManager
{

    protected GameWorld gameWorld;

    protected Map<Integer,Squad> squadMap;
    // squad for teams (aka non-random squad). Key is team id. Same Squad should be also present in squadMap
    private Map<String,Squad> teamSquadMap;
    protected Map<PlayerCell,Squad> playerCellSquadMap;

    private Squad squadToFill; // temporary squad, will be copied on completion
    protected int squadSizes;

    protected Set<ServerWebSocket> closedSockets = new HashSet<>();

    public SquadManager(GameWorld gameWorld, int squadSizes)
    {
        this.gameWorld = gameWorld;
        this.squadSizes = squadSizes;
        squadMap = new ConcurrentHashMap<>();
        teamSquadMap = new ConcurrentHashMap<>();
        playerCellSquadMap = new ConcurrentHashMap<>();
        squadToFill = new Squad(squadSizes);
        squadMap.put(squadToFill.getId(),squadToFill);
    }

    public int addTeamMember(PlayerCell playerCell, ServerWebSocket playerSocket, TeamLoginPacket packet)
    {
        if(teamSquadMap.containsKey(packet.teamId))
        {
            Squad existingSquad = teamSquadMap.get(packet.teamId);
            existingSquad.getMembers().add(playerCell);
            existingSquad.getSquadSockets().put(playerCell, playerSocket);
            playerCell.setSquadId(existingSquad.getId());


            if(existingSquad.getMembers().size() > existingSquad.getExpectedTeamSize())
            {
                //for now just message
                IoLogger.log(" ERROR: Squad manager - squad has more players than expected!");
            }
            if(existingSquad.getMembers().size() == existingSquad.getExpectedTeamSize() && !existingSquad.isAutofill())
            {
                existingSquad.setFormed(true);
            }

            playerCellSquadMap.put(playerCell, existingSquad);
            squadMap.put(existingSquad.getId(), existingSquad);

            sendSquadUpdate(existingSquad);


            return existingSquad.getId();
        }
        else
        {
            Squad newTeamSquad = new Squad(squadSizes);
            newTeamSquad.getMembers().add(playerCell);
            /*if(packet.isTeamLeader)
            {
                newTeamSquad.setSquadLeader(playerCell);
            }*/
            //just make 1st guy teamleader
            IoLogger.log("addTeamMember new squadleader " + playerCell.getUsername() + " for squad " + newTeamSquad.getId());
            newTeamSquad.setSquadLeader(playerCell);

            newTeamSquad.setExpectedTeamSize(packet.teamSize);
            newTeamSquad.setAutofill(packet.autoFillEnabled);

            playerCellSquadMap.put(playerCell, newTeamSquad);
            sendSquadUpdate(newTeamSquad);

            teamSquadMap.put(packet.teamId, newTeamSquad);
            squadMap.put(newTeamSquad.getId(), newTeamSquad);

            return  newTeamSquad.getId();
        }
    }


    public int addNewPlayerToRandomSquad(PlayerCell playerCell, ServerWebSocket playerSocket)
    {

      // need to check first if there is team squad which is not full and has autofill enabled
      // in this case we nee to fill it with this random player
      for (Squad teamSquad : teamSquadMap.values())
      {
          if(!teamSquad.isAutofill() || teamSquad.isFormed()) continue;
          if(teamSquad.getExpectedTeamSize() != -1 &&
                  teamSquad.getMembers().size() == teamSquad.getExpectedTeamSize()
                  && teamSquad.getExpectedTeamSize() < squadSizes)
          {
              teamSquad.getMembers().add(playerCell);
              teamSquad.getSquadSockets().put(playerCell, playerSocket);
              playerCell.setSquadId(teamSquad.getId());

              if(teamSquad.getSquadLeader() == null)
              {
                  IoLogger.log("ERROR  addNewPlayerToRandomSquad squad with no leader!  ");
                //  teamSquad.setSquadLeader(playerCell);
              }
              if(teamSquad.getMembers().size() == squadSizes)
              {
                  teamSquad.setFormed(true);
              }

              sendSquadUpdate(teamSquad);
              return teamSquad.getId();
          }
      }


      if(!squadToFill.isFormed())
      {
          squadToFill.getMembers().add(playerCell);
          playerCell.setSquadId(squadToFill.getId());
          if(squadToFill.getSquadSockets().containsKey(playerCell))
          {
              squadToFill.getSquadSockets().remove(playerCell);
          }
          squadToFill.getSquadSockets().putIfAbsent(playerCell, playerSocket);
          if(squadToFill.getSquadLeader() == null)
          {
              IoLogger.log("addNewPlayerToRandomSquad new squadleader " + playerCell.getUsername() + " for squad " + squadToFill.getId());
              squadToFill.setSquadLeader(playerCell);
          }
          playerCellSquadMap.put(playerCell, squadToFill);



          if(squadToFill.getMembers().size() == squadSizes)
          {

              Squad toSend = squadToFill;
              squadToFill.setFormed(true);

              squadToFill = new Squad(squadSizes);
              //squadMap.put(squadToFill.getId(), squadToFill);

              IoLogger.log("Created new squad with id " +squadToFill.getId());
              sendSquadUpdate(toSend);
              return toSend.getId();
          }
          else
          {
              squadMap.put(squadToFill.getId(), squadToFill);
              sendSquadUpdate(squadToFill);
              return squadToFill.getId();
          }


      }
      else
      {
          IoLogger.log("SquadManager - It shouldn't happen!");
      }

      return -1;
    }

    private void sendSquadUpdate(Squad squad)
    {
        if(DebugMode)
        {
            IoLogger.log("sendSquadUpdate id " + squad.getId());
            IoLogger.log("sendSquadUpdate members " + squad.getMembers().size());
        }


        SquadChangedPacket packet = null;
        try {
            packet = new SquadChangedPacket(squad.getId(),
                    squad.getMembers().stream().map(PlayerCell::getId).toArray(String[]::new),
                    squad.getSquadLeader() != null ? squad.getSquadLeader().getId() : null,
                    squad.isFormed());
        } catch (Exception e) {
            IoLogger.log("sendSquadUpdate error " + squad);
            e.printStackTrace();

        }
        emitPacketToSquadMembers(packet, squad);
    }


    public void removePlayer(PlayerCell playerCell)
    {
        Squad playerSquad = playerCellSquadMap.get(playerCell);

        if(playerSquad == null /*&& DebugMode*/)
        {
            IoLogger.log("remove player null squad!");
            IoLogger.log("playerCell = " + playerCell);
            IoLogger.log("playerCellSquadMap size = " + playerCellSquadMap.size());
            return;
        }

        playerSquad.getMembers().remove(playerCell);
        playerSquad.getSquadSockets().remove(playerCell);
        playerCellSquadMap.remove(playerCell);

        if((playerSquad.getSquadLeader() == null || playerSquad.getSquadLeader().equals(playerCell)) && playerSquad.getMembers().size() > 0)
        {
            playerSquad.setSquadLeader(playerSquad.getMembers().get(0));
        }

        if(playerSquad.getMembers().isEmpty()) //squad destroyed
        {
            playerSquad.setSquadLeader(null);
            squadMap.remove(playerSquad.getId());
        }
        else
        {
            sendSquadUpdate(playerSquad);
        }




    }

    public void emitPacketToSquadMembers(Packet p, Squad squad)
    {
        byte[] dataToSend = ServerLauncher.serializer.serialize(p);

        for (ServerWebSocket socket : squad.getSquadSockets().values())

            try {
                socket.write(Buffer.buffer(dataToSend));
            } catch (IllegalStateException e) {
                closedSockets.add(socket);
            }

        closedSockets.forEach(s -> {
            gameWorld.getLoadBalancer().removeClientSocket(s);
        });
    }



    public Map<Integer, Squad> getSquadMap() {
        return squadMap;
    }

    public Squad getSquadToFill() {
        return squadToFill;
    }
}
