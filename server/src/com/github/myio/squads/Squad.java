package com.github.myio.squads;

import com.github.myio.entities.PlayerCell;
import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;

import java.util.ArrayList;
import java.util.List;

import io.vertx.core.http.ServerWebSocket;

/**
 * Group of players (2 or 4)
 */

public class Squad
{
    private static int squadIdGenerator;

    private int id;
    private int size;
    private boolean formed; // if not formed, new players can join it
    private List<PlayerCell> members;
    private PlayerCell squadLeader;
    private BiMap<PlayerCell, ServerWebSocket> squadSockets;


    // used for teams, shows how many members we are expecting
    private int expectedTeamSize = -1;

    // used for teams, if its true and  expectedTeamSize < size, then we need to complete
    // this squad with random guys
    private boolean autofill;

    public Squad(int size)
    {
        id = ++squadIdGenerator;
        this.size = size;
        members = new ArrayList<>(size);
        squadSockets =  HashBiMap.create();
    }

    private Squad(int id, int size, boolean formed, List<PlayerCell> members, PlayerCell squadLeader, BiMap<PlayerCell, ServerWebSocket> squadSockets) {
        this.id = id;
        this.size = size;
        this.formed = formed;
        this.members = members;
        this.squadLeader = squadLeader;
        this.squadSockets = squadSockets;
    }



    public int getId() {
        return id;
    }

    public boolean isFormed() {
        return formed;
    }

    public List<PlayerCell> getMembers() {
        return members;
    }

    public PlayerCell getSquadLeader() {
        return squadLeader;
    }

    public BiMap<PlayerCell, ServerWebSocket> getSquadSockets() {
        return squadSockets;
    }

    public void setFormed(boolean formed) {
        this.formed = formed;
    }

    public void setSquadLeader(PlayerCell squadLeader) {
        this.squadLeader = squadLeader;
    }

    public boolean isAutofill() {
        return autofill;
    }

    public void setAutofill(boolean autofill) {
        this.autofill = autofill;
    }

    public int getExpectedTeamSize() {
        return expectedTeamSize;
    }

    public void setExpectedTeamSize(int expectedTeamSize) {
        this.expectedTeamSize = expectedTeamSize;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Squad{" +
                "id=" + id +
                ", formed=" + formed +
                ", squadLeader=" + squadLeader +
                ", expectedTeamSize=" + expectedTeamSize +
                ", autofill=" + autofill +
                '}';
    }
}
