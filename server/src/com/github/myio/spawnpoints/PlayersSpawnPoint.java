package com.github.myio.spawnpoints;


import com.github.myio.entities.IoPoint;

public class PlayersSpawnPoint extends SpawnPoint {

    private int spawnedPlayersCount;

    public PlayersSpawnPoint(float x, float y) {
        super(x, y);
    }


    @Override
    public SpawnPointType spawnPointType() {
        return SpawnPointType.PLAYERS;
    }

    @Override
    public IoPoint spawn() {
        spawnedPlayersCount++;
        return new IoPoint(x,y);
    }

    public int getSpawnedPlayersCount() {
        return spawnedPlayersCount;
    }
}
