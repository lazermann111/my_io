package com.github.myio.spawnpoints;


import com.github.myio.entities.IoPoint;
import com.github.myio.enums.ItemType;

import java.util.Random;

public class ItemsSpawnPoint extends SpawnPoint {

    private float x;
    private float y;
    public ItemsSpawnPoint(float x, float y) {
        super(x, y);
        this.x=x;
        this.y=y;
    }

    public float getX() {return x;}

    public float getY() {return y;}

    @Override
    public SpawnPointType spawnPointType() {
        return SpawnPointType.ITEMS;
    }

    @Override
    public IoPoint spawn() {
        return null;
    }
    Random r = new Random();

    public ItemType getItemToSpawn()
    {
        return ItemType.values()[r.nextInt(ItemType.values().length-1)];
    }
}
