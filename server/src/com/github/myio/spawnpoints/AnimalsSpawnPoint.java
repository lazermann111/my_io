package com.github.myio.spawnpoints;

import com.github.myio.entities.IoPoint;
import com.github.myio.enums.AnimalType;
import com.github.myio.enums.ItemType;

import java.util.Random;

/**
 * Created by dell on 30.03.2018.
 */

public class AnimalsSpawnPoint extends SpawnPoint{
    private float x;
    private float y;
    public AnimalsSpawnPoint(float x, float y) {
        super(x, y);
        this.x=x;
        this.y=y;
    }

    public float getX() {return x;}

    public float getY() {return y;}

    @Override
    public SpawnPointType spawnPointType() {
        return SpawnPointType.ANIMALS;
    }

    @Override
    public IoPoint spawn() {
        return null;
    }
    Random r = new Random();

    public AnimalType getItemToSpawn()
    {
        return AnimalType.values()[r.nextInt(ItemType.values().length-1)];
    }
}
