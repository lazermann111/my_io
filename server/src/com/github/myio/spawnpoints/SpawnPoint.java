package com.github.myio.spawnpoints;


import com.badlogic.gdx.graphics.Color;
import com.github.myio.entities.IoPoint;
/*
* Server-side abstraction for spawning  (creating things at certain position) in game world.
* There are no spawn points on client as for now
* */


public abstract class SpawnPoint {

    public float x,y;

    public SpawnPoint(float x, float y) {
        this.x = x;
        this.y = y;
    }

    public enum SpawnPointType
    {
        ITEMS,
        PLAYERS,
        ANIMALS,
        TEAMS_SPAWN;

        static public SpawnPointType getSpawnByColor(Color c)
        {
            if(c.g >= 125/2555f && c.g <= 129/255f && c.b >= 37/255f && c.b <= 42/255f && c.r <= 255/255f) {
                return ANIMALS;
            }
            if(c.g > 0.6 && c.b < 0.3 && c.r < 0.3) return PLAYERS;
            if(c.b > 0.6 && c.g < 0.3 && c.r < 0.3) return ITEMS;
            //if(c.b > 0.6 && c.g < 0.3 && c.r < 0.3) return SPIDER_WEB;
            return null;
        }
    }




    public abstract SpawnPointType spawnPointType();

    public abstract IoPoint spawn();


}
