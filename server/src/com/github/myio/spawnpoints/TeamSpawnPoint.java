package com.github.myio.spawnpoints;

import com.github.myio.squads.Squad;


public class TeamSpawnPoint extends PlayersSpawnPoint
{

    private Squad owner;

    public TeamSpawnPoint(float x, float y, Squad owner)
    {
        super(x, y);

        this.owner = owner;
    }




}
