//package com.github.myio.visualizer;
//
//import com.badlogic.gdx.Application;
//import com.badlogic.gdx.Gdx;
//import com.badlogic.gdx.Preferences;
//import com.badlogic.gdx.graphics.OrthographicCamera;
//import com.badlogic.gdx.graphics.g2d.BitmapFont;
//import com.badlogic.gdx.graphics.g2d.SpriteBatch;
//import com.badlogic.gdx.graphics.g2d.TextureAtlas;
//import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
//import com.badlogic.gdx.graphics.profiling.GLProfiler;
//import com.badlogic.gdx.utils.viewport.ExtendViewport;
//import com.badlogic.gdx.utils.viewport.ScreenViewport;
//import com.badlogic.gdx.utils.viewport.Viewport;
//import com.github.myio.GameClient;
//import com.github.myio.GameConstants;
//import com.github.myio.GameWorld;
//import com.github.myio.dto.masterserver.GameType;
//import com.github.myio.entities.PlayerCell;
//import com.github.myio.entities.map.ZoneMapManager;
//import com.github.myio.tools.AssetManager;
//import com.github.myio.tools.SoundManager;
//import com.github.myio.ui.RenderManager;
//
//import java.util.Random;
//
//import static com.badlogic.gdx.Gdx.app;
//import static com.github.myio.GameConstants.DebugMode;
//import static com.github.myio.StringConstants.PREFERENCES_CLIENT_ID;
//
//
//public class ServerVizualizerClient extends GameClient
//{
//
//    private GameWorld world ;
//
//    public ServerVizualizerClient(GameWorld world)
//    {
//        this.world = world;
//    }
//
//    @Override
//    public void create() {
//        GameConstants.isServer = false;
//        game = this;
//        app.setLogLevel(Application.LOG_DEBUG);
//        if(DebugMode) GLProfiler.enable();
//        batch = new SpriteBatch(4096);
//        gameShapeRenderer = new ShapeRenderer();
//        uiShapeRenderer = new ShapeRenderer();
//     //   masterServer = new MasterServer();
//
//
//        camera = new OrthographicCamera();
//        camerUi = new OrthographicCamera();
//
//        gameViewPort = new ExtendViewport(Gdx.graphics.getWidth(), Gdx.graphics.getHeight(),camera);
//        uiViewport = new ScreenViewport(camerUi);
//        uiViewport.update(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
//        uiViewport.apply();
//        camerUi.position.set(camerUi.viewportWidth / 2, camerUi.viewportHeight / 2, 0);
//        camerUi.update();
//
//        gamePreferences =  Gdx.app.getPreferences("game_prefs");
//
//       /* if(gamePreferences.getLong(PREFERENCES_CLIENT_ID) != 0) todo find
//        {
//            clientId = gamePreferences.getString(PREFERENCES_CLIENT_ID);
//        }
//        else
//        {*/
//        clientId = System.currentTimeMillis()+""; //should be unique enough
//        gamePreferences.putString(PREFERENCES_CLIENT_ID, clientId);
//        gamePreferences.flush();
//        // }
//
//
//        zoneMapManager = new ZoneMapManager();
//     //   networkManager = new NetworkManager();
//        //renderManager = new RenderManager(this, camera);
//       // soundManager = new SoundManager();
//
//
//       // messagesPanel = new SystemMessagesPanel(this, camera,localPlayer);
//        // setScreen(new ParticleTest());
//        //setScreen(new SoundTest());
//        assetManager = new AssetManager();
//
//        //====================ATLAS=============================
//        playerSkin = new TextureAtlas(Gdx.files.internal("atlas/playerSkin/playerSkinAtlas.atlas"));
//        uiAtlas = new TextureAtlas(Gdx.files.internal("atlas/uiAtlas/uiAtlas.atlas"));
//        cellAtlas = new TextureAtlas(Gdx.files.internal("atlas/cellAtlas/cellAtlas.atlas"));
//        //=========================FONTS===================
//        font32White = new BitmapFont(Gdx.files.internal("fonts/font32White.fnt"));
//        font32Black = new BitmapFont(Gdx.files.internal("fonts/font32Black.fnt"));
//
//
//        //mainMenuScreen = new MainMenuScreen(this);
//        //setScreen(mainMenuScreen);
//        setScreen(new ServerVizualizerScreen(this,world));
//
//    }
//
//   // public Screen getMainMenuScreen() {return mainMenuScreen;}
//
//
//    @Override
//    public void render() {
//
//        super.render();
//   //     networkManager.update();
//    }
//
//    @Override
//    public void resize(int width, int height) {
//        super.resize(width, height);
//        uiViewport.update(width, height);
//        uiViewport.setScreenSize(width,height);
//        camerUi.position.set(width / 2, height / 2, 0);
//        camerUi.update();
//    }
//
//    @Override
//    public void dispose() {
//     //   networkManager.disconnect(); // Null-safe closing method that catches and logs any exceptions.
//        batch.dispose();
//        font32Black.dispose();
//        font32White.dispose();
//    }
//
//
//    public SpriteBatch getBatch() {
//        return batch;
//    }
//
//    public static BitmapFont getFont32White() {return font32White;}
//
//    public static BitmapFont getFont32Black() {return font32Black;}
//
//
//
//    public ShapeRenderer getGameShapeRenderer() {
//        return gameShapeRenderer;
//    }
//
//    public ShapeRenderer getUiShapeRenderer() {
//        return uiShapeRenderer;
//    }
//
//    public SoundManager getSoundManager() {
//        return soundManager;
//    }
//
//    public Preferences getGamePreferences() {
//        return gamePreferences;
//    }
//
//
//
//
//
//
//    public static GameClient getGame() {
//        return game;
//    }
//
//    public static Random random = new Random();
//
//    public static float nextFloat(float min, float max)
//    {
//        return min + random.nextFloat() * (max - min);
//    }
//
//    public RenderManager getRenderManager() {
//        return renderManager;
//    }
//
//    public  PlayerCell getLocalPlayer() {
//        return localPlayer;
//    }
//
//    public  void setLocalPlayer(PlayerCell localPlayer) {
//        this.localPlayer = localPlayer;
//    }
//
//    public OrthographicCamera getCamera() {
//        return camera;
//    }
//
//    public Viewport getGameViewPort() {
//        return gameViewPort;
//    }
//
//
//    public Viewport getUiViewport() {
//        return uiViewport;
//    }
//
//    public AssetManager getAssetManager() {
//        return assetManager;
//    }
//
//
//    public ZoneMapManager getZoneMapManager() {
//        return zoneMapManager;
//    }
//
//
//    public String getClientId() {
//        return clientId;
//    }
//
//    public String getClientRegion() {
//        return clientRegion;
//    }
//
//    public void setClientRegion(String clientRegion) {
//        this.clientRegion = clientRegion;
//    }
//
//    public GameType getGameType() {
//        return gameType;
//    }
//
//    public void setGameType(GameType gameType) {
//        this.gameType = gameType;
//    }
//
//
//}
