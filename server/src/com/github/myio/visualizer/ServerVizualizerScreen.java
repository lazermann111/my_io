//package com.github.myio.visualizer;
//
//import com.badlogic.gdx.Gdx;
//import com.badlogic.gdx.Input;
//import com.badlogic.gdx.InputMultiplexer;
//import com.badlogic.gdx.InputProcessor;
//import com.badlogic.gdx.Screen;
//import com.badlogic.gdx.graphics.Color;
//import com.badlogic.gdx.graphics.Cursor;
//import com.badlogic.gdx.graphics.GL20;
//import com.badlogic.gdx.graphics.OrthographicCamera;
//import com.badlogic.gdx.graphics.g2d.TextureAtlas;
//import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
//import com.badlogic.gdx.graphics.profiling.GLProfiler;
//import com.badlogic.gdx.math.Vector2;
//import com.badlogic.gdx.math.Vector3;
//import com.badlogic.gdx.scenes.scene2d.ui.Skin;
//import com.badlogic.gdx.utils.Timer;
//import com.badlogic.gdx.utils.viewport.Viewport;
//import com.github.myio.EntityManager;
//import com.github.myio.GameClient;
//import com.github.myio.GameConstants;
//import com.github.myio.GameWorld;
//import com.github.myio.dto.PlayerSessionStatsDto;
//import com.github.myio.dto.masterserver.GameType;
//import com.github.myio.entities.ClientSquad;
//import com.github.myio.entities.Collidable;
//import com.github.myio.entities.IoPoint;
//import com.github.myio.entities.PlayerCell;
//import com.github.myio.entities.map.ZoneMapManager;
//import com.github.myio.entities.projectiles.ParticlesPoolManager;
//import com.github.myio.entities.projectiles.ProjectileManager;
//import com.github.myio.entities.weapons.AbstractRangeWeapon;
//import com.github.myio.entities.weapons.Bazooka;
//import com.github.myio.entities.weapons.FlashBang;
//import com.github.myio.entities.weapons.Grenade;
//import com.github.myio.entities.weapons.Knife;
//import com.github.myio.entities.weapons.MachineGun;
//import com.github.myio.entities.weapons.Pistol;
//import com.github.myio.entities.weapons.ShotGun;
//import com.github.myio.enums.PlayerState;
//import com.github.myio.inventory.InventoryItemSlot;
//import com.github.myio.inventory.InventoryManager;
//import com.github.myio.inventory.InventoryPanel;
//import com.github.myio.networking.packet.SuicidePacket;
//import com.github.myio.networking.packet.ProjectileLaunchedPacket;
//import com.github.myio.screens.MainMenuScreen;
//import com.github.myio.tools.CustomInputProcessor;
//import com.github.myio.tools.IoLogger;
//import com.github.myio.tools.SoundManager;
//import com.github.myio.ui.AlivePlayersPanel;
//import com.github.myio.ui.AmmoPanel;
//import com.github.myio.ui.ArmorBar;
//import com.github.myio.ui.CountDown;
//import com.github.myio.ui.DebugPanel;
//import com.github.myio.ui.EmotesMenu;
//import com.github.myio.ui.ExitMenu;
//import com.github.myio.ui.FinalMessage;
//import com.github.myio.ui.GameChat;
//import com.github.myio.ui.HealthBar;
//import com.github.myio.ui.IDrawable;
//import com.github.myio.ui.ItemPopUp;
//import com.github.myio.ui.KillPanelPvP;
//import com.github.myio.ui.LockScreen;
//import com.github.myio.ui.RenderManager;
//import com.github.myio.ui.SquadInfo;
//import com.github.myio.ui.SystemMessagesPanel;
//import com.github.myio.ui.TimerZone;
//import com.github.myio.ui.ZonesCountdownBar;
//
//import java.util.ArrayList;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//import java.util.Random;
//
//import static com.github.myio.GameConstants.DebugMode;
//import static com.github.myio.GameConstants.PRODUCTION_MODE;
//import static com.github.myio.GameConstants.SINGLE_PLAYER_DEATH;
//import static com.github.myio.GameConstants.SINGLE_PLAYER_WIN;
//import static com.github.myio.GameConstants.SQUAD_DEATH;
//import static com.github.myio.GameConstants.SQUAD_WIN;
//
//
///**
// *  Screen for visualizing state of GameWorld.
// *  Key differences from GameScreen is that all entities for EntityManager and ProjectileManager are give from server (GameWorld.java),
// *  so they are representing current server state and may be useful for network debugging
// *
// */
//
//
//public class ServerVizualizerScreen implements Screen ,  InputProcessor{
//    private CustomInputProcessor customInputProcessor;
//
//
//    OrthographicCamera camera;
//    Viewport viewport;
//    Viewport UiViewport;
//    Skin skin;
//
//    public  PlayerCell localPlayer;
//    public  PlayerCell playerToSpectate;
//
//    private long gldraws ;
//    private long textureBindings1 ;
//    private long start;
//    //private boolean isInChatMode;
//
//    boolean zoomingInProgress = false;
//    float finalZoom;
//
//    private int minimumAccel = 2;
//    private ArrayList<IDrawable> UI;
//
//    private AlivePlayersPanel alivePlayersPanel;
//
//    private ZonesCountdownBar zonesCountdownBar;
//    private DebugPanel debugPanel;
//    private EmotesMenu emotesMenu;
//    private HealthBar healthBar;
//    private ArmorBar armorBar;
//    private ServerVizualizerClient game;
//    private EntityManager entityManager;
//    private RenderManager renderManager;
//    private ProjectileManager projectileManager;
//    private GameChat gameChat;
//    private ClientSquad clientSquad;
//    private InventoryPanel inventoryPanel;
//    private SystemMessagesPanel messagesPanel;
//    private InventoryManager inventoryManager;
//    private ParticlesPoolManager particlesPoolManager;
//    private ItemPopUp itemPopUp;
//    private KillPanelPvP killPanelPvP;
//    private TimerZone timerZone;
//    private ZoneMapManager zoneMapManager;
//    private AmmoPanel ammoPanel;
//    private CountDown countDown;
//    private SquadInfo squadInfo;
//    private FinalMessage finalMessage;
//    LockScreen lockScreen;
//    Map<String, PlayerSessionStatsDto> statMap;
//
//
//    private PlayerState localPlayerState;
//    private List<IDrawable> spawnedPlayerUi = new ArrayList<IDrawable>();
//
//    private Random rnd = new Random();
//    ExitMenu exit;
//    public int kickback = 0;
//
//    private ProjectileManager pool;
//    private Timer weaponReloadTimer;
//    InputMultiplexer inputMultiplexer;
//
//    public Viewport getViewport() {
//        return viewport;
//    }
//
//    GameWorld world;
//
//    public ServerVizualizerScreen(ServerVizualizerClient game, GameWorld world )
//    {
//        this.game = game;
//        if(DebugMode) GLProfiler.enable();
//        //camera = new OrthographicCamera(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
//        camera = game.getCamera();
//        viewport = game.getGameViewPort();
//        UiViewport = game.getUiViewport();
//        skin = new Skin(Gdx.files.internal("atlas/default/skin/uiskin.json"),new TextureAtlas("atlas/default/skin/uiskin.atlas"));
//
//       // localPlayer = world.getAlivePlayersMap().values().iterator().next();
//       // localPlayer = local;
//        localPlayerState  = PlayerState.SPECTATE;
//        //
//
//     //   GameClient.getGame().setLocalPlayer(local); // todo
//
//        inputMultiplexer = new InputMultiplexer();
//        camera.zoom = 2f;
//      //  emotesMenu = localPlayer.getEmotesMenu();
//        countDown = new CountDown();
//        renderManager = game.getRenderManager();
//        //messagesPanel = game.getMessagesPanel();
//      //  itemPopUp = new ItemPopUp(localPlayer,game,skin);
//        particlesPoolManager = new ParticlesPoolManager();
//        projectileManager = new ProjectileManager(game,particlesPoolManager);
//
//        //entityManager = new EntityManager(localPlayer, game,itemPopUp, projectileManager, inventoryManager);
//        //alivePlayersPanel = new AlivePlayersPanel( game,camera,entityManager.getAllPlayers(),entityManager,skin);
//
//
//       // localPlayer.setDefaultInventory(true,true);
//
//      //  inventoryManager.setNetworkManager(GameClient.getGame().getNetworkManager());
////		inventoryManager.switchWeaponTo(localPlayer.getCurrentWeapon());
//
//
//
//
//        zonesCountdownBar = new ZonesCountdownBar(game);
//        zonesCountdownBar.startZonesCoundown();
//
//
//        statMap = new HashMap<String, PlayerSessionStatsDto>();
//        finalMessage = new FinalMessage(game);
//        lockScreen = new LockScreen(game,localPlayerState);
//        //GameClient.getGame().getNetworkManager().setLockScreen(lockScreen);
//
//
//
//
//
//
//
//       // game.getNetworkManager().addPacketHandler(this);
//
//        //	game.getSoundManager().playSound(AMBIENT_1);
//
//       // IoLogger.log("","Game screen created");
//
////        messagesPanel.getLabelConnect().setText("WELCOME! "+localPlayer.getUsername()+" please wait for the players");
//
//    }
//
//
//    public void setLocalPlayer(PlayerCell localPlayer)
//    {
//        healthBar = localPlayer.getHealthBar();
//        armorBar = localPlayer.getmArmorBar();
//        armorBar.setLocalPlayer(localPlayer);
//
//        debugPanel = new DebugPanel(game, localPlayer, camera,entityManager,skin );
//        gameChat = new GameChat(game, localPlayer,camera);
//        killPanelPvP = new KillPanelPvP(localPlayer,game,skin);
//
//        exit = new ExitMenu(game,camera,skin);
//        ammoPanel = new AmmoPanel(game, localPlayer,skin);
//        zoneMapManager = new ZoneMapManager();
//        timerZone = new TimerZone(game,zoneMapManager,skin,localPlayer);
//
//        inventoryManager = localPlayer.getInventoryManager();
//        inventoryPanel = new InventoryPanel(inventoryManager, countDown);
//        inventoryManager.setInventoryPanel(inventoryPanel);
//        entityManager = new EntityManager(localPlayer, game,itemPopUp, projectileManager, inventoryManager, game.getNetworkManager());
//
//        alivePlayersPanel = new AlivePlayersPanel( game,camera,entityManager.getAllPlayers(),entityManager,skin);
//
//
//        //todo quick hack
//        if(!game.getGameType().equals(GameType.SOLO)) {
//            clientSquad = new ClientSquad(entityManager);
//            squadInfo = clientSquad.getSquadInfo();
//            clientSquad.setFinalMessage(finalMessage);
//            entityManager.setSquadClient(clientSquad);
//        }
//
//
//        	/* UI */
//        UI = new ArrayList<IDrawable>();
//
//        UI.add(debugPanel);
//        //UI.add(renderManager);
//        UI.add(messagesPanel);
//        UI.add(inventoryPanel);
//        UI.add(alivePlayersPanel);
//        UI.add(itemPopUp);
//        UI.add(exit);
//        UI.add(ammoPanel);
//        UI.add(emotesMenu);
//        UI.add(killPanelPvP);
//        UI.add(timerZone);
//        UI.add(healthBar);
//        UI.add(armorBar);
//        UI.add(countDown);
//        UI.add(zonesCountdownBar);
//        if(squadInfo != null) {
//            UI.add(squadInfo);
//        }
//        UI.add(finalMessage);
//        UI.add(lockScreen);
//
//
//
//        //spawnedPlayerUi.add(renderManager);
//        spawnedPlayerUi.add(inventoryPanel);
//        spawnedPlayerUi.add(alivePlayersPanel);
//        spawnedPlayerUi.add(ammoPanel);
//        spawnedPlayerUi.add(squadInfo);
//        spawnedPlayerUi.add(healthBar);
//        spawnedPlayerUi.add(itemPopUp);
//        spawnedPlayerUi.add(zonesCountdownBar);
//        //spawnedPlayerUi.add(hpBar);
//
//        customInputProcessor = new CustomInputProcessor(game, localPlayer, emotesMenu, countDown);
//        inputMultiplexer.addProcessor(inventoryPanel.getStage());
//        inputMultiplexer.addProcessor(customInputProcessor);
//        inputMultiplexer.addProcessor(emotesMenu.getStage());
//        inputMultiplexer.addProcessor(this);
//        Gdx.input.setInputProcessor(inputMultiplexer);
//    }
//
//    @Override
//    public void render(float delta) {
//        if(DebugMode)
//        {
//            gldraws = GLProfiler.drawCalls;
//            textureBindings1 = GLProfiler.textureBindings;
//            start = System.currentTimeMillis();
//        }
//
//        try {
//
//            // Tile color
//            Gdx.gl.glClearColor(0.43f,0.34f,0.28f, 1);
//            Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
//            Gdx.graphics.setSystemCursor(Cursor.SystemCursor.Crosshair);
//
//            processPlayerState();
//
//            game.getBatch().setProjectionMatrix(camera.combined);
//            game.getGameShapeRenderer().setProjectionMatrix(camera.combined);
//            game.getGameShapeRenderer().setAutoShapeType(true);
//            game.getGameShapeRenderer().begin(ShapeType.Line);
//            game.getGameShapeRenderer().setColor(1, 1, 0, 1);
//
//            game.getBatch().begin();
//            entityManager.update(delta);
//            if(renderManager.isRenderPlayer())
//            {
//
//                localPlayer.localRendererDraw(delta);
//
//            }
//            game.getBatch().end();
//
//
//            if(GameConstants.DebugMode) {
//                localPlayer.drawPointRadius(delta);
//                entityManager.updateDebug(delta);
//            }
//            game.getGameShapeRenderer().end();
//
//
//
//				/* drawing labels*/
//				/*game.getBatch().begin();
//				game.getFont().draw(game.getBatch(), camera.position.toString(), 10, Gdx.graphics.getHeight() - 10);
//				game.getBatch().end();*/
//				/*game.getBatch().begin();
//				if(renderManager.isRenderPlayer()) localPlayer.drawPlayerLabel();
//				game.getBatch().end();*/
//            getRenderManager().flashEffect(delta);
//				/* drawing ui*/
//
//
//            getRenderManager().userInput(); // todo remove from prod
//            if(getRenderManager().isRenderUI()) {
//                // game.getUiShapeRenderer().begin(ShapeType.Filled);
//                for (IDrawable drawable : UI) {
//                    drawable.draw(delta);
//                }
//            }
//            getRenderManager().flashEffect(delta);
//
//            if(localPlayer.isAim() && !localPlayer.hasShield() && !(localPlayer.getCurrentWeapon() instanceof Knife)) {
//                Vector3 touchVector = new Vector3();
//                touchVector.set(Gdx.input.getX(), Gdx.input.getY(), 0);
//                camera.unproject(touchVector);
//
//
//                if (localPlayer.getCurrentWeapon() != null)
//                {
//                    Vector2 forward = new Vector2(touchVector.x - localPlayer.getPosition().getX(), touchVector.y - localPlayer.getPosition().getY());
//                    forward.setLength(localPlayer.getRadius() * 2);
//                    Vector2 endVector = new Vector2(0, localPlayer.getCurrentWeapon().projectileBlueprint.maxDistance);
//                    endVector.rotate(localPlayer.getForward().angle() + 180);
//                    IoPoint end = new IoPoint(localPlayer.getFireCoords().add(forward).getX() + endVector.x, localPlayer.getFireCoords().add(forward).getY() + endVector.y);
//                    game.getGameShapeRenderer().begin(ShapeType.Line);
//                    game.getGameShapeRenderer().line(localPlayer.getFireCoords().add(forward).getX(), localPlayer.getFireCoords().add(forward).getY(), end.x, end.y, Color.WHITE, Color.RED);
//                    game.getGameShapeRenderer().end();
//                }
//            }
//        }
//        catch (Exception e) {
//
//           // GameClient.getGame().getMasterServer().postExceptionDetails(e, "Gamescreen::render");
//            IoLogger.log("render", e.getMessage());
//            e.printStackTrace();
//        }
//
//        // todo delete in prod
//        processZoom(delta);
//
//        if(DebugMode)
//        {
//            IoLogger.log ("Debug",  "gldraws :" + (GLProfiler.drawCalls - gldraws));
//            IoLogger.log ("Debug", "textureBindings :" +(GLProfiler.textureBindings  - textureBindings1));
//            IoLogger.log ("Debug", "main render takes :" +   (System.currentTimeMillis() - start) + " ms)");
//        }
//    }
//
//
//    private void die()
//    {
//        localPlayerState = PlayerState.DEAD;
//        messagesPanel.message("YOU DIE! Press S to spectate, M to go to main menu");
//        UI.removeAll(spawnedPlayerUi);
//        int code;
//        if(!game.getGameType().equals(GameType.SOLO) && clientSquad.getMembers().size() == 0)
//        {
//            code = SQUAD_DEATH;
//        }
//        else
//        {
//            code = SINGLE_PLAYER_DEATH;
//        }
//
//        finalMessage.showMessage(code, statMap);
//    }
//
//    private void win()
//    {
//        // todo add stats showing
//        // send stats to server
//        // add some visuals
//
//        localPlayerState = PlayerState.WINS;
//        messagesPanel.message("YOU WON! Press M to fo to main menu");
//        IoLogger.log(localPlayer.getUsername() + "win()");
//        UI.removeAll(spawnedPlayerUi);
//        int code;
//        if (!game.getGameType().equals(GameType.SOLO))
//        {
//            code = SQUAD_WIN;
//        }
//        else
//        {
//            code = SINGLE_PLAYER_WIN;
//        }
//        if (statMap != null)
//        {
//            finalMessage.showMessage(code, statMap);
//        }
//        else{ IoLogger.log("statMap is null"); }
//    }
//
//
//
//
//    private void processWinsMode()
//    {
//        if(Gdx.input.isKeyJustPressed(Input.Keys.M))
//        {
//
//            dispose();
//            game.setScreen(game.getMainMenuScreen());
//        }
//    }
//
//    private void spectate()
//    {
//        // todo add some spec panel - "Spectating %player_name%"
//        // and remove unnecessary gui
//
//        if(entityManager.getEnemyCells().isEmpty()) return;
//        localPlayerState = PlayerState.SPECTATE;
//        UI.removeAll(spawnedPlayerUi);
//
//        messagesPanel.message("Press Q to switch to previous player or E to switch to next player. Press R to respawn");
//    }
//
//    private void processDeadMode()
//    {
//        if(Gdx.input.isKeyJustPressed(Input.Keys.S))
//        {
//            spectate();
//        }
//
//        if(Gdx.input.isKeyJustPressed(Input.Keys.M))
//        {
//
//            dispose();
//            game.setScreen(new MainMenuScreen(game));
//        }
//    }
//
//
//
//    private short spectateIndex = 0;
//    private void processSpectatorMode()
//    {
//        if(playerToSpectate == null)
//        {
//            playerToSpectate = (PlayerCell) entityManager.getEnemyCells().values().toArray()[0];
//
//            entityManager.setLocalPlayer(playerToSpectate);
//        }
//        Vector3 position = camera.position;
//        position.x = camera.position.x + (playerToSpectate.getX() - camera.position.x) * 0.1f;
//        position.y = camera.position.y + (playerToSpectate.getY() - camera.position.y) * 0.1f;
//        camera.position.set(position);
//        camera.update();
//
//        if(Gdx.input.isKeyJustPressed(Input.Keys.R))
//        {
//            game.setScreen(new MainMenuScreen(game));
//        }
//
//        if(Gdx.input.isKeyJustPressed(Input.Keys.Y))
//        {
//            int pSize = entityManager.getEnemyCells().values().toArray().length;
//            playerToSpectate = (PlayerCell) entityManager.getEnemyCells().values().toArray()[rnd.nextInt(pSize)];
//            entityManager.setLocalPlayer(playerToSpectate);
//        }
//
//        if(Gdx.input.isKeyJustPressed(Input.Keys.Q))
//        {
//            spectateIndex--;
//            if(spectateIndex < 0)
//                spectateIndex = (short) (entityManager.getEnemyCells().size() - 1);
//            playerToSpectate = (PlayerCell) entityManager.getEnemyCells().values().toArray()[spectateIndex];
//            entityManager.setLocalPlayer(playerToSpectate);
//        }
//        if(Gdx.input.isKeyJustPressed(Input.Keys.E))
//        {
//            spectateIndex++;
//            if(spectateIndex > entityManager.getEnemyCells().size() - 1)
//                spectateIndex = 0;
//            playerToSpectate = (PlayerCell) entityManager.getEnemyCells().values().toArray()[spectateIndex];
//            entityManager.setLocalPlayer(playerToSpectate);
//        }
//
//    }
//
//
//    private void  switchToPlayingMode()
//    {
//        localPlayerState = PlayerState.PLAYING;
//        UI.addAll(spawnedPlayerUi);
//
//        messagesPanel.message("");
//    }
//
//    private void processPlayerState () throws InterruptedException
//    {
//        switch (localPlayerState) {
//           /* case PLAYING:
//
//                Vector3 position = camera.position;
//                position.x = camera.position.x + (localPlayer.getX() - camera.position.x) * 0.1f;
//                position.y = camera.position.y + (localPlayer.getY() - camera.position.y) * 0.1f;
//                //camera.position.set(localPlayer.getX(), localPlayer.getY(), 0);
//                camera.position.set(position);
//                camera.update();
//
//                handlePlayingMode();
//                //processChat();
//                localPlayer.sendHeartBeat(game.getNetworkManager());
//                break;*/
//            case DEAD:
//                processDeadMode();
//                break;
//            case WINS:
//                processWinsMode();
//                break;
//            case SPECTATE:
//                processSpectatorMode();
//                break;
//            case IN_LOBBY:
//
//                break;
//            case IN_CHAT:
//                //processChat();
//               // localPlayer.sendHeartBeat(game.getNetworkManager());
//                break;
//        }
//    }
//
//
//    private void processZoom(float delta){
//        try {
//            if(!zoomingInProgress)return;
//
//            //IoLogger.log("","camera.zoom =" + camera.zoom);
//            camera.zoom *= (1+delta/2);
//            if(camera.zoom >= finalZoom) zoomingInProgress = false;
//        } catch (Exception e) {
//            GameClient.getGame().getMasterServer().postExceptionDetails(e, "processZoom");
//            IoLogger.log("processZoom", e.getMessage());
//            e.printStackTrace();
//        }
//    }
//
//    private boolean playerLocked = false; // for testing purposes
//    PlayerCell nextStep = new PlayerCell();
//    private void acceleratePlayer() {
//        try {
//
//            float deltaX = 0;
//            float deltaY = 0;
//
//            if(localPlayer.getCurrentWeapon() != null) {
//                if (System.currentTimeMillis() - localPlayer.getCurrentWeapon().getLastShotTime() > localPlayer.getCurrentWeapon().getDecelerationTime())
//                    kickback = 0;
//                else
//                    kickback = localPlayer.getCurrentWeapon().getKickback();
//            }
//
//
//			/*if(localPlayer.getCurrentWeapon() instanceof Bazooka)
//			{
//				localPlayer.setAccelerationMultiplier(0.6f);
//			}*/
//
//            int playerSpeed = localPlayer.getSpeed();
//            if(localPlayer.isAim()) {
//                playerSpeed -= 3;
//            }
//            if((localPlayer.getCurrentWeapon() instanceof ShotGun) || (localPlayer.getCurrentWeapon() instanceof Bazooka)) {
//                if (Gdx.input.isKeyPressed(Input.Keys.W)) {
//                    deltaY = playerSpeed;
//                    localPlayer.setRotatedAfterStep(false);
//                }
//                if (Gdx.input.isKeyPressed(Input.Keys.S)) {
//                    deltaY = -playerSpeed;
//                    localPlayer.setRotatedAfterStep(false);
//                }
//                if (Gdx.input.isKeyPressed(Input.Keys.D)) {
//                    deltaX = playerSpeed;
//                    localPlayer.setRotatedAfterStep(false);
//                }
//                if (Gdx.input.isKeyPressed(Input.Keys.A)) {
//                    deltaX = -playerSpeed;
//                    localPlayer.setRotatedAfterStep(false);
//                }
//                //Vector2 kickbackVector = new Vector2(0, localPlayer.getCurrentWeapon().getKickback());
//				/*Vector2 kickbackVector = localPlayer.getForward();
// 				kickbackVector.setLength(localPlayer.getCurrentWeapon().getKickback());
// 				if(kickback != 0) {
// 					kickbackVector.rotate(localPlayer.getRotationAngle());
// 					deltaY += kickbackVector.y;
// 					deltaX += kickbackVector.x;
//+				}*/
//
//                Vector2 kickbackVector = new Vector2(0, localPlayer.getCurrentWeapon().getKickback());
//                if(kickback != 0) {
//                    kickbackVector.rotate(localPlayer.getRotationAngle());
//                    deltaY += kickbackVector.y;
//                    deltaX += kickbackVector.x;
//                }
//            }
//            else {
//
//                //WASD movement
//                if (Gdx.input.isKeyPressed(Input.Keys.W)) {
//                    //deltaY = playerSpeed - kickback;
//                    deltaY = playerSpeed;
//                    if(!localPlayer.isAim()) {
//                        deltaY -= kickback;
//                    }
//                    else
//                        deltaY -= kickback/2.0f;
//                    localPlayer.setRotatedAfterStep(false);
//                }
//                if (Gdx.input.isKeyPressed(Input.Keys.S)) {
//                    //deltaY = -playerSpeed + kickback;
//                    deltaY = -playerSpeed;
//                    if(!localPlayer.isAim()) {
//                        deltaY += kickback;
//                    }
//                    else
//                        deltaY += kickback/2.0f;
//                    localPlayer.setRotatedAfterStep(false);
//                }
//                if (Gdx.input.isKeyPressed(Input.Keys.D)) {
//                    //deltaX = playerSpeed - kickback;
//                    deltaX = playerSpeed;
//                    if(!localPlayer.isAim()) {
//                        deltaX -= kickback;
//                    }
//                    else
//                        deltaX -= kickback/2.0f;
//                    localPlayer.setRotatedAfterStep(false);
//                }
//                if (Gdx.input.isKeyPressed(Input.Keys.A)) {
//                    //deltaX = -playerSpeed + kickback;
//                    deltaX = -playerSpeed;
//                    if(!localPlayer.isAim()) {
//                        deltaX += kickback;
//                    }
//                    else
//                        deltaX += kickback/2.0f;
//                    localPlayer.setRotatedAfterStep(false);
//                }
//            }
//
//
//            Vector3 touchPos = new Vector3();
//            touchPos.set(Gdx.input.getX(), Gdx.input.getY(), 0);
//            camera.unproject(touchPos);
//
//            List<Collidable> collisions = entityManager.defineCollisions();
//
//            //Variant with computing next step
//
//
//            if(!collisions.isEmpty()) {
//                nextStep.setInsidePit(localPlayer.isInsidePit());
//                nextStep.setX(localPlayer.getX());
//                nextStep.setY(localPlayer.getY());
//
//                //deltaX*=1000;
//                //deltaY*=1000;
//                nextStep.setXPlus(deltaX*2);
//                nextStep.setYPlus(deltaY*2);
//				/*if(deltaX > 0) {
//					nextStep.setXMinus(Math.min(PLAYER_SPEED_LIMIT, deltaX / PLAYER_MOUSE_ACCELERATION_RATIO));
//				}
//				else if(deltaX < 0) {
//					nextStep.setXMinus(Math.max(-PLAYER_SPEED_LIMIT, deltaX/ PLAYER_MOUSE_ACCELERATION_RATIO));
//				}
//
//				if(deltaY > 0) {
//					nextStep.setYMinus(Math.min(PLAYER_SPEED_LIMIT, deltaY / PLAYER_MOUSE_ACCELERATION_RATIO));
//				}
//				else if(deltaY < 0) {
//					nextStep.setYMinus(Math.max(-PLAYER_SPEED_LIMIT, deltaY / PLAYER_MOUSE_ACCELERATION_RATIO));
//				}*/
//                boolean canMove = true;
//
//                for(Collidable cell: collisions) {
//                    if(cell.collides(nextStep)) {
//                        movementCollis(nextStep,deltaX,deltaY);
//                        canMove = false;
//
//                        //if (! GameClient.getGame().getSoundManager().check_names(SoundManager.PLAYER_WALL_HIT))
//                        //GameClient.getGame().getSoundManager().playSoundWithPanAndVolume(SoundManager.PLAYER_WALL_HIT, new IoPoint(camera.position.x, camera.position.y),new IoPoint(localPlayer.x,localPlayer.y));
//                    }
//                }
//
//                if(!canMove) {
//
//                    deltaX = 0;
//                    deltaY = 0;
//
//                }
//				/*else {
//					deltaX = (localPlayer.getX() - touchPos.x);
//					deltaY = (localPlayer.getY() - touchPos.y);
//				}*/
//
//            }
////            if (deltaX!=0 || deltaY!=0)
////            {
////
////				GameClient.getGame().getSoundManager().playSoundWithPanAndVolume(localPlayer.getPlayerGroundTypeSound(),
////					    new IoPoint(camera.position.x, camera.position.y),
////						new IoPoint (localPlayer.x,localPlayer.y),
////						1 / localPlayer.getAccelerationMultiplier());
////            }
//
//            if (localPlayer.isMoving() && !localPlayer.hasShield()){
//                GameClient.getGame().getSoundManager().playSoundWithPanAndVolume(localPlayer.getPlayerGroundTypeSound(),
//                        new IoPoint(camera.position.x, camera.position.y),
//                        new IoPoint (localPlayer.x,localPlayer.y),
//                        1 / localPlayer.getAccelerationMultiplier());
//            }else if (!localPlayer.isMoving() && !localPlayer.hasShield() ){
//                GameClient.getGame().getSoundManager().stopSound(localPlayer.getPlayerGroundTypeSound());
//            }
//
//            if (localPlayer.isMoving() && localPlayer.hasShield()){
//                GameClient.getGame().getSoundManager().playSoundWithPanAndVolume(SoundManager.MOVING_WITH_SHIELD,
//                        new IoPoint(camera.position.x, camera.position.y),
//                        new IoPoint (localPlayer.x,localPlayer.y),1 / localPlayer.getAccelerationMultiplier());
//            }else if (!localPlayer.isMoving() && localPlayer.hasShield()){
//                GameClient.getGame().getSoundManager().stopSound(SoundManager.MOVING_WITH_SHIELD);
//            }
//
//
//
//            if(playerLocked) return;
//
//            localPlayer.setXPlus(deltaX*localPlayer.getAccelerationMultiplier());
//            localPlayer.setYPlus(deltaY*localPlayer.getAccelerationMultiplier());
//
//
//        } catch (Exception e) {
//
//            GameClient.getGame().getMasterServer().postExceptionDetails(e, "acceleratePlayer");
//            IoLogger.log("acceleratePlayer", e.getMessage());
//            e.printStackTrace();
//        }
//    }
//
//    public void movementCollis(PlayerCell nextStep, float deltaX, float deltaY){
//        //System.out.println("nextStep = [" + nextStep.getX()%100+"|"+nextStep.getY()%100 + "], deltaX = [" + deltaX + "], deltaY = [" + deltaY + "]");
//
//
//
//    }
//
//    private boolean processChat() throws InterruptedException {
//
//        if (Gdx.input.isKeyJustPressed(Input.Keys.ENTER) && localPlayerState != PlayerState.IN_CHAT) {
//
//            localPlayerState = PlayerState.IN_CHAT;
//            UI.add(gameChat);
//            gameChat.openChat();
//
//            return true;
//        }
//        if (Gdx.input.isKeyJustPressed(Input.Keys.ENTER) &&  localPlayerState == PlayerState.IN_CHAT){
//
//            localPlayerState = PlayerState.PLAYING;
//            gameChat.closeChat();
//            Timer.schedule(new Timer.Task() {
//                @Override
//                public void run() {
//                    UI.remove(gameChat);
//                }
//            },2);
//
//            return false;
//        }
//        return true;
//    }
//
//
//    private void handlePlayingMode() {
//        try {
//
//            if(localPlayer.getCurrentHealth() <= 0)
//            {
//                die(); return;
//            }
//
//			/*if(Gdx.input.isKeyJustPressed(Input.Keys.U))
//			{
//				//spectate(); return;
//			}/*/
//
//            acceleratePlayer();
//            rotatePlayer();
//
//            if(localPlayer.getCurrentWeapon().totalAmmoAmount == 0 && localPlayer.getCurrentWeapon().canShoot()
//                    && (localPlayer.getCurrentWeapon() instanceof Grenade || localPlayer.getCurrentWeapon() instanceof FlashBang)) {
//                int slotIndex = localPlayer.getWeaponList().indexOf(localPlayer.getCurrentWeapon());
//                InventoryItemSlot slot = localPlayer.getInventoryManager().getSlots().get(slotIndex);
//                localPlayer.getInventoryManager().removeSlotFromWeapon(slot, localPlayer.getCurrentWeapon());
//                //game.getNetworkManager().sendPacket(new WeaponSwitchedPacket(localPlayer.getId(), localPlayer.getCurrentWeapon().weaponType));
//                //IoLogger.log("Removed");
//            }
//
//            if( Gdx.input.isKeyJustPressed(Input.Keys.ESCAPE)){
//
//                if(exit.isVisible())
//                {
//                    exit.hideDialog();
//                }
//                else
//                {
//                    exit.showDialog();
//                }
//            }
//
//            if (Gdx.input.isKeyPressed(Input.Keys.X)) {
//                camera.zoom += 0.02;
//            }
//            if (Gdx.input.isKeyPressed(Input.Keys.Z)) {
//                camera.zoom -= 0.02;
//            }
//
//            if (Gdx.input.isKeyJustPressed(Input.Keys.L))
//                playerLocked = !playerLocked;
//
//            if (Gdx.input.isKeyJustPressed(Input.Keys.M))
//            {
//                entityManager.addMarker(localPlayer.createMarker());
//            }
//
//            if (Gdx.input.isKeyJustPressed(Input.Keys.B)) // temp input for debugging
//            {
//                lockScreen.showLock();
//            }
//
//            if (Gdx.input.isKeyJustPressed(Input.Keys.T) && !PRODUCTION_MODE) {  // todo test function, remove in prod
//
//                localPlayer.setX(10000);
//                localPlayer.setY(8000);
//                //	localPlayer.getCurrentWeapon().clipAmmoAmount = 20;
//            }
//            if (Gdx.input.isKeyJustPressed(Input.Keys.O) && !PRODUCTION_MODE)
//            {  // todo test function, remove in prod
//
//                game.getNetworkManager().disconnect();
//                dispose();
//                game.setScreen(new MainMenuScreen(game));
//            }
//
//            if (Gdx.input.isKeyJustPressed(Input.Keys.U) && !PRODUCTION_MODE)
//            {  // todo test function, remove in prod
//
//                localPlayer.die(particlesPoolManager);
//            }
//
//            if(Gdx.input.isKeyJustPressed(Input.Keys.E))
//            {
//                stopAmmoReloading();
//
//                int curIndex = localPlayer.getWeaponList().indexOf(localPlayer.getCurrentWeapon());
//
//                do {
//                    curIndex++;
//                    if(curIndex > localPlayer.getWeaponList().size() - 1)
//                        curIndex = 0;
//                }while(localPlayer.getWeaponList().get(curIndex) == null);
//
//                if(curIndex >= localPlayer.getWeaponList().size())
//                {
//                    AbstractRangeWeapon w =localPlayer.getWeaponList().get(0);
//                    //localPlayer.setCurrentWeapon(w);
//                    inventoryManager.switchWeaponTo(w);
//                }
//                else
//                {
//                    AbstractRangeWeapon w =localPlayer.getWeaponList().get(curIndex);
//                    //localPlayer.setCurrentWeapon(w);
//                    inventoryManager.switchWeaponTo(w);
//                }
//
//
//                //	game.getNetworkManager().sendPacket(new WeaponSwitchedPacket(localPlayer.getId(), localPlayer.getCurrentWeapon().weaponType));
//            }
//
//            if(Gdx.input.isKeyJustPressed(Input.Keys.Q))
//            {
//                stopAmmoReloading();
//
//                int curIndex = localPlayer.getWeaponList().indexOf(localPlayer.getCurrentWeapon());
//
//                do {
//                    curIndex--;
//                    if(curIndex < 0)
//                        curIndex = localPlayer.getWeaponList().size() - 1;
//                }while(localPlayer.getWeaponList().get(curIndex) == null);
//
//                if (curIndex < 0) {
//                    AbstractRangeWeapon w = localPlayer.getWeaponList().get(localPlayer.getWeaponList().size() - 1);
//                    //localPlayer.setCurrentWeapon(w);
//                    inventoryManager.switchWeaponTo(w);
//                } else {
//                    AbstractRangeWeapon w = localPlayer.getWeaponList().get(curIndex);
//                    //localPlayer.setCurrentWeapon(w);
//                    inventoryManager.switchWeaponTo(w);
//                }
//
//
//                //	game.getNetworkManager().sendPacket(new WeaponSwitchedPacket(localPlayer.getId(), localPlayer.getCurrentWeapon().weaponType));
//            }
//
////			if(Gdx.input.isKeyJustPressed(Input.Keys.N))
////			{
////				localPlayer.nextTexture();
////			}
////			if(Gdx.input.isKeyJustPressed(Input.Keys.J)) {
////				if(!PRODUCTION_MODE) {
////					//todo remove
////					MachineGun machineGun = new MachineGun(game.getGameShapeRenderer(), localPlayer);
////					machineGun.totalAmmoAmount = 25;
////
////
////					ShotGun shotGun = new ShotGun(game.getGameShapeRenderer(), localPlayer);
////					shotGun.totalAmmoAmount = 5;
////
////					Pistol pistol = new Pistol(game.getGameShapeRenderer(), localPlayer);
////					pistol.totalAmmoAmount = 20;
////
////					Bazooka bazooka = new Bazooka(game.getGameShapeRenderer(), localPlayer);
////					bazooka.totalAmmoAmount =  1;
////
////					Grenade grenade = new Grenade(game.getGameShapeRenderer(), localPlayer);
////					grenade.totalAmmoAmount = 2;
////
////					FlashBang flashBang = new FlashBang(game.getGameShapeRenderer(), localPlayer);
////					flashBang.totalAmmoAmount = 2;
////
////					Knife knife = new Knife(game.getGameShapeRenderer(), localPlayer);
////
////					ArrayList abstractWeaponList = new ArrayList<AbstractRangeWeapon>();
////					abstractWeaponList.add(machineGun);
////					abstractWeaponList.add(shotGun);
////					abstractWeaponList.add(grenade);
////					abstractWeaponList.add(flashBang);
////					abstractWeaponList.add(knife);
////
////					localPlayer.setWeaponList(abstractWeaponList);
////					//todo end remove
////				}
////			}
//            if(Gdx.input.isKeyJustPressed(Input.Keys.K))
//            {
//                messagesPanel.message("Current zone is " + game.getZoneMapManager().getPlayerZone(localPlayer.getPosition()).name());
//            }
//            if(Gdx.input.isKeyJustPressed(Input.Keys.R) ) {
//                reloadWepon();
//            }
//
//
//
//
//
//
//            if(customInputProcessor.isFire()) {
//
//                if(Gdx.input.isButtonPressed(Input.Buttons.LEFT))
//                {
//
//                    if (localPlayer.getCurrentWeapon() != null && localPlayer.getCurrentWeapon().canShoot()) {
//
//                        Vector3 touchPos = new Vector3();
//                        touchPos.set(Gdx.input.getX(), Gdx.input.getY(), 0);
//                        camera.unproject(touchPos);
//
//                        if(touchPos.dst(localPlayer.getPositionVec()) > localPlayer.radius * 2 && localPlayer.getCurrentWeapon().clipAmmoAmount > 0)
//                        {
//                            if(countDown.isFinished()) {
////								if(!localPlayer.isUnderWeb()) {
//                                int slotIndex = localPlayer.getWeaponList().indexOf(localPlayer.getCurrentWeapon());
//                                InventoryItemSlot slot = localPlayer.getInventoryManager().getSlots().get(slotIndex);
//                                Vector2 forward = new Vector2(touchPos.x - localPlayer.getPosition().getX(), touchPos.y - localPlayer.getPosition().getY());
//                                forward.setLength(localPlayer.getRadius() * 2);
//                                ProjectileLaunchedPacket p = localPlayer.getCurrentWeapon().fire(localPlayer.getFireCoords().add(forward), new IoPoint(touchPos.x, touchPos.y), localPlayer.getRotationAngle(), localPlayer.getId(), slot);
//                                entityManager.addProjectiles(projectileManager.obtainProjectile(p));// todo experimental
//
//
//                                if (((localPlayer.getCurrentWeapon() instanceof Pistol || localPlayer.getCurrentWeapon() instanceof MachineGun || localPlayer.getCurrentWeapon() instanceof Knife))
//                                        && localPlayer.getCurrentWeapon().totalAmmoAmount > 0)
//                                    localPlayer.setRotatedAfterShot(false);
//                                    //}
//                                else {
//                                    //TODO: show label that you can't shoot in jug
//                                }
//                            }
//                            else {
//                                //TODO: show label about count down
//                            }
//                        }
//                        else if(localPlayer.getCurrentWeapon().clipAmmoAmount == 0 && localPlayer.getCurrentWeapon().canShoot()) {
//                            GameClient.getGame().getSoundManager().playSoundWithPanAndVolume(SoundManager.MISFIRE,
//                                    new IoPoint(camera.position.x, camera.position.y), localPlayer.getPosition() );
//                            reloadWepon();
//                            localPlayer.getCurrentWeapon().setCanShot(false);
//                            //TODO: play no ammo sound
//                        }
//                    }
//                }
//            }
//
//            if(!Gdx.input.isButtonPressed(Input.Buttons.LEFT)) {
//                if(localPlayer.getCurrentWeapon() != null)
//                    localPlayer.getCurrentWeapon().setCanShot(true);
//            }
////			if (Gdx.input.isKeyJustPressed(Input.Keys.TAB))
////			{
////				localPlayer.setCurrentHealth(1);
////			}
//            if (Gdx.input.isKeyJustPressed(Input.Keys.P))
//            {
//                game.getNetworkManager().sendPacket(new SuicidePacket());
//            }
//        } catch (Exception e) {
//            GameClient.getGame().getMasterServer().postExceptionDetails(e, "handlePlayingMode");
//            IoLogger.log("handlePlayingMode", e.getMessage());
//            e.printStackTrace();
//        }
//
//    }
//
//    private void reloadWepon(){
//        if(countDown.isFinished()) {
//            final AbstractRangeWeapon weapon = localPlayer.getCurrentWeapon();
//            if (weapon != null && weapon.canBeReload()) {
//                if (countDown.showCountDown(/*GameConstants.TIME_FOR_RECHARGE, */Color.WHITE, weapon, weapon.weaponType.name(), localPlayer.getCurrentWeapon().getReloadTime())) {
//                    weaponReloadTimer = new Timer();
//                    weaponReloadTimer.schedule(
//                            new Timer.Task() {
//                                @Override
//                                public void run() {
//                                    weapon.reload();
//                                    if (weapon.isCancelReloading()) {
//                                        weapon.resetClipAmount();
//                                        weapon.setCancelReloading(false);
//                                        cancel();
//                                    }
//                                }
//                            },
//                            GameConstants.TIME_FOR_RECHARGE);
//                    if (weapon instanceof MachineGun) {
//                        GameClient.getGame().getSoundManager().playSoundWithPanAndVolume(SoundManager.WEAPON_RELOAD, new IoPoint(camera.position.x, camera.position.y), new IoPoint(localPlayer.x, localPlayer.y));
//                    }
//                    if (weapon instanceof Pistol) {
//                        GameClient.getGame().getSoundManager().playSoundWithPanAndVolume(SoundManager.PISTOL_RELOAD, new IoPoint(camera.position.x, camera.position.y), new IoPoint(localPlayer.x, localPlayer.y));
//                    }
//                    if (weapon instanceof ShotGun) {
//                        GameClient.getGame().getSoundManager().playSoundWithPanAndVolume(SoundManager.SHOTGUN_RELOAD, new IoPoint(camera.position.x, camera.position.y), new IoPoint(localPlayer.x, localPlayer.y));
//                    }
//                }
//                //todo  sync reload and progress bar
//            }
//        }
//    }
//
//    private void stopAmmoReloading() { //TODO: Alex use this method to stop reloading after change weapon
//        if(localPlayer.getCurrentWeapon() != null) {
//            if (!countDown.isFinished()) {
//                localPlayer.getCurrentWeapon().setCancelReloading(true);
//                countDown.resetCountDown();
//                if (localPlayer.getCurrentWeapon() instanceof MachineGun)
//                {
//                    GameClient.getGame().getSoundManager().stopSound(SoundManager.WEAPON_RELOAD);
//                }
//                if (localPlayer.getCurrentWeapon() instanceof Pistol)
//                {
//                    GameClient.getGame().getSoundManager().stopSound(SoundManager.PISTOL_RELOAD);
//                }
//                if (localPlayer.getCurrentWeapon() instanceof ShotGun)
//                {
//                    GameClient.getGame().getSoundManager().stopSound(SoundManager.SHOTGUN_RELOAD);
//                }
//            }
//        }
//    }
//
//    private void rotatePlayer() {
//        try {
//		/*Vector3 touchPos = new Vector3();
//		touchPos.set(Gdx.input.getX(), Gdx.input.getY(), 0);
//		Vector2 touchVector = new Vector2(Gdx.input.getX() - localPlayer.getX(), Gdx.input.getY() - localPlayer.getY());
//
//		Vector2 startPos = new Vector2(localPlayer.getX() - localPlayer.getX(),localPlayer.getY() - 800 - localPlayer.getY());*/
//            //Vector3 touchPos = new Vector3(Gdx.input.getX(), Gdx.input.getY(),0);
//            Vector2 touchVector = new Vector2(Gdx.graphics.getWidth()/2 - Gdx.input.getX(),  Gdx.graphics.getHeight()/2 - Gdx.input.getY());
//
//            Vector2 startPos = new Vector2(0,-200);
//            float angle = touchVector.angle(startPos);
//            localPlayer.getForward().setAngle(angle);
//
//            if(!localPlayer.hasShield()) {
//                if (!localPlayer.isRotatedAfterShot()) {
//                    if (localPlayer.getCurrentWeapon() instanceof Pistol || localPlayer.getCurrentWeapon() instanceof MachineGun) {
//                        angle -= 9;
//                        localPlayer.setRotatedAfterShot(true);
//                    } else if (localPlayer.getCurrentWeapon() instanceof Knife) {
//                        angle += localPlayer.getKnifeKickAngle();
//                    }
//                }
//                if (!localPlayer.isRotatedAfterStep() && kickback == 0 && !localPlayer.isAim()) {
//                    angle += localPlayer.getAngleForStepRotation();
//                    localPlayer.setRotatedAfterStep(true);
//                }
//            }
//            localPlayer.setRotationAngle((int) angle);
//            if(localPlayer.hasShield()) {
//                localPlayer.shiled.setRotationAngle(((int) angle));
//            }
//
//            //	localPlayer.setRotationAngle((int) (angle * localPlayer.getRotationMultiplier())); // todo check if we want rotation inertia
//        } catch (Exception e) {
//            IoLogger.log("rotatePlayer", e.getMessage());
//            e.printStackTrace();
//        }
//    }
//
//    @Override
//    public void show() {
//
//
//    }
//
//    @Override
//    public void resize(int width, int height) {
//        try {
//            camera.viewportWidth = width;
//            camera.viewportHeight = height;
//			/*viewport.update(width, height);
//			viewport.setScreenSize(width,height);*/
//
//
//        } catch (Exception e) {
//            GameClient.getGame().getMasterServer().postExceptionDetails(e, "Gamescreen::resize");
//            IoLogger.log("resize", e.getMessage());
//            e.printStackTrace();
//        }
//
//    }
//
//    @Override
//    public void pause() {
//
//
//    }
//
//    @Override
//    public void resume() {
//
//
//    }
//
//    @Override
//    public void hide() {
//
//
//    }
//
//    @Override
//    public void dispose() {
//        try {
//    //        game.getNetworkManager().removePacketHandler(this);
//       //     game.getNetworkManager().disconnect();
//
//        } catch (Exception e) {
//            IoLogger.log("dispose", e.getMessage());
//        }
//
//    }
///*    @Override
//    public boolean handlePacket(Packet packet) {
//
//
//        try {
//            if (packet.packetType == PacketType.WORLD_SNAPSHOT) {
//                InitialWorldSnapshotPacket worldSnapshotPacket = (InitialWorldSnapshotPacket) packet;
//                //Read enemies from the packet
//                for (PlayerCellDto e : worldSnapshotPacket.players) {
//                    if (e == null)break;
//                    if (e.getId().equals(localPlayer.getId())) continue; // ourselves
//
//
//                    entityManager.addNewPlayer(e);
//
//                }
//
//                for (AnimalCellDto a : worldSnapshotPacket.animals) {
//                    if(a == null) break;
//
//                    AnimalCellDto animalCellDto = a;
//
//                    AnimalCell animalCell = new AnimalCell(game.getBatch(), game.getGameShapeRenderer(), animalCellDto.getX(), animalCellDto.getY(),
//                            20, animalCellDto.getId(), animalCellDto.getCurrentHealth());
//                    animalCell.setRotationAngle(animalCellDto.getRotationAngle());
//
//                    entityManager.addNewAnimal(animalCell);
//                }
//
//                if (worldSnapshotPacket.items != null && worldSnapshotPacket.entities != null ){
//                    entityManager.setItems(worldSnapshotPacket.items);
//                    entityManager.setEntities(worldSnapshotPacket.entities);
//                }
//
//
//                //Read item from packet
//                //todo seems like copypaste, check if its needed
//				for (ItemDto e : worldSnapshotPacket.items) {
//
//					ItemDto serverFood = e;
//					if (serverFood == null)break;
//					ItemCell food = new ItemCell(game.getBatch(), game.getGameShapeRenderer(),
//							serverFood.getX(), serverFood.getY(), serverFood.getId(), serverFood.getItemType(),serverFood.getItem_blueprint());
//
//					entityManager.addNewItem(food);
//				}
//
//                //	IoLogger.log("", "WORLD_SNAPSHOT client 3");
//                entityManager.defineNearestEntities();
//                //	IoLogger.log("", "WORLD_SNAPSHOT client 4");
//
//
//
//                return true;
//            }
//
//            if (packet.packetType == PacketType.WORLD_UPDATE) {
//
//                WorldUpdatePacket p = (WorldUpdatePacket) packet;
//                //if(DebugMode) IoLogger.log("", "WORLD_UPDATE client " + p.toString().length());
//                if (p.joinedPlayers != null)
//                {
//                    for (PlayerCellDto newPlayer : p.joinedPlayers)
//                    {
//                        if(newPlayer == null) break;
//                        if (newPlayer.getId().equals(localPlayer.getId())) continue;
//
//                        entityManager.addNewPlayer(newPlayer);
//                        IoLogger.log("", newPlayer.getUsername() + " just joined");
//
//                    }
//                }
//                entityManager.setPlayersData(p.playersData, localPlayerState == PlayerState.SPECTATE);
//                entityManager.setEntitiesData(p.changedEntitiesData);
//
//
//
//
//
//                if (p.pickedItems != null)
//                    for (String s : p.pickedItems)
//                    {
//                        if(s == null) break;
//                        entityManager.removeItem(s);
//                    }
//
//                if (p.droppedItems != null)
//                    for (ItemDto s : p.droppedItems)
//                    {   if(s == null) break;
//                        entityManager.addNewItem(s);
//                    }
//                if (p.leftPlayers != null) {
//                    for (String disconnected : p.leftPlayers) {
//                        if(disconnected == null) break;
//                        entityManager.removePlayer(disconnected);
//                    }
//                }
//                if(clientSquad != null && clientSquad.getSquadIdList() != null)
//                {
//                    Map<String, PlayerCell> allPlayers = entityManager.getAllPlayers();
//                    for (String id : clientSquad.getSquadIdList()) {
//                        if (id == null) break; // end of array
//                        if (allPlayers.get(id) != null) {
//                            PlayerCell member = allPlayers.get(id);
//                            if (member != null && !member.getId().equals(localPlayer.getId())) {
//                                squadInfo.setHealth(id, member.getHealthBar().getHealth().getValue());
//                                member.drawPlayerLabel();
//                            }
//                        }
//                    }
//                }
//
//
//                return true;
//            }
//
//            if (packet.packetType == PacketType.CURRENT_WORLD_SNAPSHOT)
//            {
//                entityManager.applyCurrentWorldSnapshot((CurrentWorldSnapshotPacket)packet);
//                return true;
//            }
//            if (packet.packetType == PacketType.HEARTBEAT_DISABLE)
//            {
//                localPlayer.setCanSendHeartBeats(false);
//                return true;
//            }
//
//            if (packet.packetType == PacketType.TIMER_GAME)
//            {
//
//                TimerGamePacket t = (TimerGamePacket)packet;
//                timerZone.setStartTime(t.timer);
//                timerZone.setZoneGreenDuration(t.zoneDuration[0]);;//green zone
//                timerZone.setZoneYellowDuration(t.zoneDuration[1]);;//yellow zone
//                timerZone.setZoneRedDuration(t.zoneDuration[2]);;//red zone
//                return true;
//            }
//            if (packet.packetType == PacketType.TILES_SNAPSHOT) {
//
//                InitialTileSnapshotPacket p = (InitialTileSnapshotPacket)packet;
//
//                entityManager.setTiles(p.tiles);
//                game.getZoneMapManager().initZones();
//                //	entityManager.setTestTiles(game.getBatch(), game.getShapeRenderer());
//                return true;
//            }
//
//            if (packet.packetType == PacketType.PROJECTILE_LAUNCHED) {
//
//                ProjectileLaunchedPacket p = (ProjectileLaunchedPacket) packet;
//                if(!p.shooterId.equals(localPlayer.getId()))
//                {
//                    entityManager.addProjectiles(projectileManager.obtainProjectile(p));
//                }
//
//                return true;
//            }
//
//            if(packet.packetType == WEAPON_SWITCHED)
//            {
//                WeaponSwitchedPacket p = (WeaponSwitchedPacket) packet;
//                if(!localPlayer.getId().equals(p.playerId))
//                {
//                    PlayerCell e = entityManager.getEnemyCells().get(p.playerId);
//                    e.setEnemyCurrentWeapon(p.typeToSwitchTo, e);
//                    //IoLogger.log("GameScreen", p.toString());
//                }
//                return true;
//            }
//            if (packet.packetType == PacketType.PVP_INFO_PACKET)
//            {
//
//                PvPInfoPacket p = (PvPInfoPacket) packet;
//                killPanelPvP.addLabel(p.killer,p.victim);
//
//                return true;
//            }
//            if(packet.packetType == EMOTE_SHOWN) {
//                EmotePacket p = (EmotePacket) packet;
//                IoLogger.log("GameScreen", p.toString());
//                if(!localPlayer.getId().equals(p.getPlayerId()))
//                {
//                    IoLogger.log("GameScreen   receive packet with emote", p.getEmoteName());
//                    entityManager.getEnemyCells().get(p.getPlayerId()).showMyEmote(p.getEmoteName());
//                }
//                return true;
//            }
//
//
//			if(packet.packetType == USE_MEDKIT)
//			{
//				UseMedkitPacket p = (UseMedkitPacket) packet;
//				if(!localPlayer.getId().equals(p.mPlayerId))
//				{
//					PlayerCell e = entityManager.getEnemyCells().get(p.mPlayerId);
//					e.healByMedKit(p.medkitBlueprint);
//					//e.setCurrentHealth(p.medkitBlueprint);
//					IoLogger.log("GameScreen", p.toString());
//				}
//				return true;
//			}
//
//            if(packet.packetType == PLAYER_STATS)
//            {
//                PlayerStatsPacket p = (PlayerStatsPacket) packet;
//                //todo (Alex) add stats to death/win screen
//                if(p.playerId.equals(localPlayer.getId()))
//                {
//                    game.getMessagesPanel().message(p.statsDto.toString());
//                    statMap.put(p.playerId, p.statsDto);
//                    IoLogger.log("Add own stat");
//                }
//                if(clientSquad != null && !p.playerId.equals(localPlayer.getId()) && clientSquad.getMembers().containsKey(p.playerId)) {
//                    statMap.put(p.playerId, p.statsDto);
//                    IoLogger.log("Add  stat of squadmember");
//                }
//
//                return true;
//            }
//
//            if(packet.packetType == ENTITY_REMOVE)
//            {
//                EntityRemovePacket p = (EntityRemovePacket) packet;
//                if(entityManager.getEntityWithEffect().contains(entityManager.getEntities().get(p.entityId))) {
//                    entityManager.removeSpecialEffectFromPlayer(entityManager.getEntities().get(p.entityId));
//                }
//                entityManager.removeEntity(p.entityId);
//
//                return true;
//            }
//
//            if(packet.packetType == PLAYER_DIED)
//            {
//                PlayerDiedPacket p = (PlayerDiedPacket) packet;
//                IoLogger.log("PlayerDiedPacket was received");
//
//                if (!localPlayer.getId().equals(p.playerId) )
//                {
//                    entityManager.getAllPlayers().get(p.playerId)
//                            .die(particlesPoolManager);
//                    return true;
//                }
//                if (localPlayer.getId().equals(p.playerId) && !localPlayerState.equals(PlayerState.DEAD))
//                {
//                    localPlayer.die(particlesPoolManager);
//                    die();
//                    return true;
//                }
//                if (clientSquad != null && squadInfo.hasInfo(p.playerId))
//                {
//                    squadInfo.setDead(p.playerId);
//                    IoLogger.log("setDead()");
//                }
//
//                if(entityManager.getAllPlayers().containsKey(p.playerId))
//                {
//                    entityManager.getAllPlayers().get(p.playerId)
//                            .die(particlesPoolManager); //visuals
//                }
//                System.out.println("remove player");
//                entityManager.removePlayer(p.playerId);
//
//                return true;
//            }
//
//
//
//            if(packet.packetType == ITEM_PICKED)
//            {
//                ItemPickedPacket p = (ItemPickedPacket) packet;
//                IoLogger.log("ITEM_PICKED ", "p.itemId " + p.itemId);
//                IoLogger.log("ITEM_PICKED ", "item " + entityManager.getItems().get(p.itemId));
//                if( entityManager.getItems().get(p.itemId) != null)
//                {
//                    if (p.playerId.equals(localPlayer.getId()))
//                    {
//                        inventoryManager.pickItem(entityManager.getItems().get(p.itemId));
//                    }
//                    entityManager.removeItem(p.itemId);
//                }
//                else
//                    Gdx.app.error("handlePacket()", "We got null item");
//
//                return true;
//            }
//
//            if(packet.packetType == SHIELD_INTERACTION)
//            {
//
//                ShieldInteractionPacket p = (ShieldInteractionPacket) packet;
//
//                PlayerCell player = entityManager.getAllPlayers().get(p.playerId);
//                TacticalShield shield = (TacticalShield) entityManager.getEntities().get(p.shiledId);
//
//                if(player != null && shield != null)
//                {
//                    if(p.equipped && localPlayer.shiled == null)
//                    {
//                        shield.startInteractionClient(player);
//                    }
//                    else
//                    {
//                        shield.finishInteractionClient(player);
//                    }
//                }
//
//                return true;
//            }
//
//            if(packet.packetType == PIT_INTERACTION) {
//                PitInteractionPacker p = (PitInteractionPacker) packet;
//
//                PlayerCell player = entityManager.getEnemyCells().get(p.playerId);
//                MapEntityCell pit = entityManager.getEntities().get(p.pitId);
//
//                if(player != null && pit != null) {
//                    if(p.insidePit) {
//                        pit.getPlayersId().add(p.playerId);
//                        player.setVisible(false);
//                        player.pit = p.pitId;
//                        player.setInsidePit(true);
//                        //IoLogger.log("Player entered pit");
//                    }
//                    else {
//                        pit.getPlayersId().remove(p.playerId);
//                        player.setVisible(true);
//                        player.pit = null;
//                        player.setInsidePit(false);
//                        //IoLogger.log("Player left pit");
//                    }
//                }
//
//                return true;
//            }
//            if(packet.packetType == SERVER_ERROR) {
//                ServerErrorPacket p = (ServerErrorPacket) packet;
//                System.out.println(p.error);
//
//                return true;
//            }
//            if(packet.packetType == VISIBILITY_CHANGE) {
//                VisibilityChangePacket p = (VisibilityChangePacket) packet;
//
//                PlayerCell playerCell = entityManager.getEnemyCells().get(p.playerId);
//
//                if(playerCell != null) {
//                    if (p.type == MapEntityType.SPIDER_WEB || p.type == MapEntityType.SPIDER_WEB_BIG) {
//                        if (p.insideEntity) {
//                            playerCell.setUnderWeb(true);
//                        } else {
//                            playerCell.setUnderWeb(false);
//                        }
//                    }
//                }
//
//                return true;
//            }
//            if(packet.packetType == GAME_WIN)
//            {
//                WinPacket p = (WinPacket) packet;
//                if(!p.winnerId.equals(localPlayer.getId()))
//                {
//                    IoLogger.log("handlePacket", "GAME_WIN packet wrong recepient");
//                    return true;
//                }
//
//                win();
//                return true;
//            }
//
//            if(packet.packetType == SERVER_STATE_INFO)
//            {
//                GameStateChangedPacket p = (GameStateChangedPacket) packet;
//                messagesPanel.messageForGameState(p.gameServerState);
//                GameClient.getGame().setGameType(p.gameType);
//                return true;
//            }
//
//            return false;
//        }
//        catch (Exception e)
//        {
//            GameClient.getGame().getMasterServer().postExceptionDetails(e, "Gamescreen::handlePacket " + packet.toString());
//            Gdx.app.error("handlePacket() GameScreen", packet.toString());
//            Gdx.app.error("handlePacket() GameScreen", e.getMessage());
//            e.printStackTrace();
//            return false;
//        }
//    }*/
//
//    public OrthographicCamera getCamera() {
//        return camera;
//    }
//
//    public ArrayList<IDrawable> getUI() {
//        return UI;
//    }
//
//
//
//    public DebugPanel getDebugPanel() {
//        return debugPanel;
//    }
//
//    public EntityManager getEntityManager() {
//        return entityManager;
//    }
//
//    public RenderManager getRenderManager() {
//        return renderManager;
//    }
//
//    @Override
//    public boolean keyDown(int keycode) {
//        return false;
//    }
//
//    @Override
//    public boolean keyUp(int keycode) {
//        return false;
//    }
//
//    @Override
//    public boolean keyTyped(char character) {
//        return false;
//    }
//
//    @Override
//    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
//
//		/*if (localPlayer.getCurrentWeapon() != null && localPlayer.getCurrentWeapon().canShoot()) {
//
//			Vector3 touchPos = new Vector3();
//			touchPos.set(Gdx.input.getX(), Gdx.input.getY(), 0);
//			camera.unproject(touchPos);
//
//			if(touchPos.dst(localPlayer.getPositionVec()) > localPlayer.radius * 2)
//			{
//				Vector2 forward = new Vector2(touchPos.x - localPlayer.getPosition().getX(), touchPos.y - localPlayer.getPosition().getY());
//				forward.setLength(localPlayer.getRadius() * 2);
//				AbstractProjectile p = localPlayer.getCurrentWeapon().fire(localPlayer.getFireCoords().add(forward), new IoPoint(touchPos.x, touchPos.y), localPlayer.getId());
//
//
//
//			}}*/
//        return false;
//    }
//
//    @Override
//    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
//        return false;
//    }
//
//    @Override
//    public boolean touchDragged(int screenX, int screenY, int pointer) {
//        return false;
//    }
//
//    @Override
//    public boolean mouseMoved(int screenX, int screenY) {
//        return false;
//    }
//
//    @Override
//    public boolean scrolled(int amount) {
//        return false;
//    }
//
//    public GameClient getGameClient() {
//        return game;
//    }
//}