//package com.github.myio.visualizer;
//
//
//import com.badlogic.gdx.Application;
//import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
//import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
//import com.github.myio.GameWorld;
//
//
//public class ServerVisualizerDesktopLauncher
//{
//
//    public static Application createApplication(GameWorld world)
//    {
//        final LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
//        config.width = 1200;
//        config.height = 900;
//        return new LwjglApplication(new ServerVizualizerClient(world), config);
//    }
//}
