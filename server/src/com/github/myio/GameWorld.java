package com.github.myio;


import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.MathUtils;
import com.github.czyzby.websocket.serialization.impl.ManualSerializer;
import com.github.myio.conquest.CapturablePoint;
import com.github.myio.dto.ItemDto;
import com.github.myio.dto.MapEntityBaseDto;
import com.github.myio.dto.PlayerCellDto;
import com.github.myio.dto.masterserver.GameType;
import com.github.myio.dto.masterserver.PlayerCellBaseDto;
import com.github.myio.entities.Collidable;
import com.github.myio.entities.IoPoint;
import com.github.myio.entities.PlayerCell;
import com.github.myio.entities.animals.AnimalCell;
import com.github.myio.entities.item.ItemCell;
import com.github.myio.entities.map.CapturablePointClient;
import com.github.myio.entities.map.MapEntityCell;
import com.github.myio.entities.map.TacticalShield;
import com.github.myio.entities.map.ZoneMapManager;
import com.github.myio.entities.projectiles.AbstractProjectile;
import com.github.myio.entities.projectiles.FlashbangAmmo;
import com.github.myio.entities.projectiles.GrenadeAmmo;
import com.github.myio.entities.projectiles.Knife;
import com.github.myio.entities.tile.TileDetails;
import com.github.myio.enums.GameServerState;
import com.github.myio.enums.ItemType;
import com.github.myio.enums.MapEntityType;
import com.github.myio.enums.TileType;
import com.github.myio.gamemode.ConquestGamemode;
import com.github.myio.gamemode.DuoGamemode;
import com.github.myio.gamemode.Gamemode;
import com.github.myio.gamemode.SoloGamemode;
import com.github.myio.gamemode.SquadGamemode;
import com.github.myio.gamemode.TwoTeamsGamemode;
import com.github.myio.networking.PacketType;
import com.github.myio.networking.packet.EntityRemovePacket;
import com.github.myio.networking.packet.GameStateChangedPacket;
import com.github.myio.networking.packet.InitialTileSnapshotPacket;
import com.github.myio.networking.packet.PlayerDiedPacket;
import com.github.myio.networking.packet.PlayerRevivingRequest;
import com.github.myio.networking.packet.PvPInfoPacket;
import com.github.myio.networking.packet.TimerGamePacket;
import com.github.myio.networking.packet.WorldUpdatePacket;
import com.github.myio.packethandlers.CurrentWorldSnapshothandler;
import com.github.myio.packethandlers.EmoteHandler;
import com.github.myio.packethandlers.HeartbeatHandler;
import com.github.myio.packethandlers.ItemHandler;
import com.github.myio.packethandlers.LoginHandler;
import com.github.myio.packethandlers.MessageChatHandler;
import com.github.myio.packethandlers.PitHandler;
import com.github.myio.packethandlers.PlayerSuppressedHandler;
import com.github.myio.packethandlers.ProjectileLaunchedHandler;
import com.github.myio.packethandlers.PvPInfoHandler;
import com.github.myio.packethandlers.ServerPacketHandler;
import com.github.myio.packethandlers.ShieldInteractionHandler;
import com.github.myio.packethandlers.SuicideHandler;
import com.github.myio.packethandlers.TeamLoginHandler;
import com.github.myio.packethandlers.UseAmmoHandler;
import com.github.myio.packethandlers.UseMedKitHandler;
import com.github.myio.packethandlers.VisibilityChangeHandler;
import com.github.myio.packethandlers.WeaponSwitchedHandler;
import com.github.myio.spawnpoints.AnimalsSpawnPoint;
import com.github.myio.spawnpoints.ItemsSpawnPoint;
import com.github.myio.spawnpoints.PlayersSpawnPoint;
import com.github.myio.spawnpoints.SpawnPoint;
import com.github.myio.squads.SquadManager;
import com.github.myio.squads.TwoTeamsManager;
import com.github.myio.stats.StatsManager;
import com.github.myio.tools.IoLogger;
import com.github.myio.tools.TileSerializer;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.stream.Collectors;

import javax.imageio.ImageIO;

import io.vertx.core.http.ServerWebSocket;
import io.vertx.core.impl.ConcurrentHashSet;

import static com.github.myio.GameConstants.CONQUEST_POINTS_NUMBER;
import static com.github.myio.GameConstants.DebugMode;
import static com.github.myio.GameConstants.ITEM_DETAILS;
import static com.github.myio.GameConstants.ITEM_DETAILS_FOR_DROP_AMMONITION;
import static com.github.myio.GameConstants.ITEM_DETAILS_FOR_DROP_PATRONBOX;
import static com.github.myio.GameConstants.MAPS_SWITH;
import static com.github.myio.GameConstants.MAX_PLAYERS_IN_INSTANCE;
import static com.github.myio.GameConstants.PLAYERS_MAX_HEALTH;
import static com.github.myio.GameConstants.PLAYERS_MIN_ARMOR;
import static com.github.myio.GameConstants.PRODUCTION_MODE;
import static com.github.myio.GameConstants.TILE_HEIGHT;
import static com.github.myio.GameConstants.TILE_WIDTH;
import static com.github.myio.GameConstants.UPDATE_TIME_FOR_SUPPRESSED_PLAYERS;
import static com.github.myio.GameConstants.USE_PITS;
import static com.github.myio.GameConstants.USE_SHIELDS;
import static com.github.myio.GameConstants.USE_SUPPRESSION;
import static com.github.myio.GameConstants.USE_WEBS;
import static com.github.myio.GameConstants.ZONE_GREEN_DURATION;
import static com.github.myio.GameConstants.ZONE_RED_DURATION;
import static com.github.myio.GameConstants.ZONE_YELLOW_DURATION;
import static com.github.myio.GameConstants.getArrayRandom;
import static com.github.myio.GameConstants.getArrayRandomForDropAmmunition;
import static com.github.myio.GameConstants.getArrayRandomForDropPatrnobox;
import static com.github.myio.entities.item.ItemCell.DEFAULT_AMMO_AMOUNT;
import static com.github.myio.entities.item.ItemCell.DEFAULT_CLIP_AMMO_AMOUNT;

public class GameWorld
{
    private TileType tiles[][];
    private int tileCountX;
    private int tileCountY;

    private Map<String, PlayerCell> alivePlayersMap = new ConcurrentHashMap<>();
    private Map<String, ItemCell> worldItems = new ConcurrentHashMap<>();
    private Map<String, MapEntityCell> mapEntities = new ConcurrentHashMap<>();
    private Set<MapEntityCell> changedEntitiesSet = new ConcurrentHashSet<>();
    private Map<String, AnimalCell> aliveAnimals = new ConcurrentHashMap<>();
    private List<String> justdisconnectedPlayers = new CopyOnWriteArrayList<>();
    private List<String> justDestroyedEntities = new CopyOnWriteArrayList<>();
    private List<PlayerCellDto> justJoinedPlayers = new CopyOnWriteArrayList<>();
    private List<ItemDto> droppedItems = new CopyOnWriteArrayList<>();
    private List<String> justPickedItems = new CopyOnWriteArrayList<>();
    private List<AbstractProjectile> launchedProjectiles = new CopyOnWriteArrayList<>();
    private List<PlayersSpawnPoint> playersSpawnPoints = new CopyOnWriteArrayList<>();
    private List<ItemsSpawnPoint> itemsSpawnPoints = new CopyOnWriteArrayList<>();
    private List<AnimalsSpawnPoint> animalsSpawnPoints = new CopyOnWriteArrayList<>();
    //private Map<String,MapEntityCell> mapEntities = new HashMap<>();

    private ZoneMapManager zoneMapManager = new ZoneMapManager();
    private GameZone currentGameZone = GameZone.GREEN;

    private PacketEmitter packetEmitter;
    private ArrayList<ServerPacketHandler> packetHandlers;


    public long playersId;
    public long itemId;
    public long animalId;
    public long entityId;
    public long tileId;
    private long lastFrameTime;
    private long lastUpdateTime;

    private ManualSerializer serializer;


    private float updateTimeForSuppressedPlayers = UPDATE_TIME_FOR_SUPPRESSED_PLAYERS;

    private int serverTime; // expose time in game
    private long timeUpdate;
    private boolean startTimer;

    private LoadBalancer loadBalancer;
    private Gamemode gamemode;
    private SquadManager squadManager;
    private StatsManager statsManager;

    public byte[] tilePacket = new byte[1];

    private long frameStart;
    private long projectileHandlingStart;
    private long updatesSendingStart;
    private long endingTimestamp;

    public GameServerState serverState = GameServerState.LAUNCHING;

    int worldId;
    String tag;


    private boolean shouldBeDisposed;

    public int getWorldId()
    {
        return worldId;
    }

    public String getWorldIdTag()
    {
        return tag;
    }

    public String mapName = "arena";
    public String mapNameProduction = "production";

    public GameWorld(ManualSerializer s, int id, LoadBalancer loadBalancer)
    {
        serializer = s;

        switch (ServerLauncher.getGameType())
        {

            case SOLO:
                gamemode = new SoloGamemode(this);
                break;
            case DUO:
                gamemode = new DuoGamemode(this);
                squadManager = new SquadManager(this, 2);
                break;
            case SQUAD:
                gamemode = new SquadGamemode(this);
                squadManager = new SquadManager(this, 4);
                break;
            case TWO_TEAMS:
                gamemode = new TwoTeamsGamemode(this);
                squadManager = new TwoTeamsManager(this, 50);
                break;
            case CONQUEST:
                gamemode = new ConquestGamemode(this);
                squadManager = new TwoTeamsManager(this, 50);
                break;
        }
        IoLogger.log(tag + "GameWorld: gamemode: " + gamemode.getGameType());


        spawnTiles(mapName, mapNameProduction);
        spawnEntities(mapName, mapNameProduction);
        spawnItems();
        worldId = id;
        tag = "World " + id + " ";
        this.loadBalancer = loadBalancer;
        statsManager = new StatsManager(this);

        IoLogger.log(tag + " : spawned entities: " + mapEntities.size());
        IoLogger.log(tag + ": alivePlayersMap: " + alivePlayersMap.size());
        IoLogger.log(tag + ": spawned items: " + worldItems.size());
        IoLogger.log(tag + ": itemsSpawnPoints: " + itemsSpawnPoints.size());
        IoLogger.log(tag + ": playersSpawnPoints: " + playersSpawnPoints.size());
        IoLogger.log(tag + "GameWorld: alive animals: " + aliveAnimals.size());
        IoLogger.log(tag + "GameWorld: animalsSpawnPoints: " + animalsSpawnPoints.size());

        serverState = GameServerState.WAITING_FOR_PLAYERS;
        packetEmitter = new PacketEmitter(this);
        packetHandlers = new ArrayList<>();


        AddPacketHandlers();

    }

    public ArrayList<ServerPacketHandler> getPacketHandlers()
    {
        return packetHandlers;
    }

    public PacketEmitter getPacketEmitter()
    {
        return packetEmitter;
    }

    private void AddPacketHandlers()
    {


        packetHandlers.add(new LoginHandler(this, this.packetEmitter));
        packetHandlers.add(new SuicideHandler(this, this.packetEmitter));
        packetHandlers.add(new TeamLoginHandler(this, this.packetEmitter));
        packetHandlers.add(new HeartbeatHandler(this, this.packetEmitter));
        packetHandlers.add(new ShieldInteractionHandler(this, this.packetEmitter));
        packetHandlers.add(new PitHandler(this, this.packetEmitter));
        packetHandlers.add(new VisibilityChangeHandler(this, this.packetEmitter));
        packetHandlers.add(new ItemHandler(this, this.packetEmitter));
        packetHandlers.add(new WeaponSwitchedHandler(this, this.packetEmitter));
        //   packetHandlers.add(new DamageHandler(this.world, this.packetEmitter));
        packetHandlers.add(new ProjectileLaunchedHandler(this, this.packetEmitter));
        //  packetHandlers.add(new ExceptionHandler(this.world, this.packetEmitter));
        packetHandlers.add(new MessageChatHandler(this, this.packetEmitter));
        packetHandlers.add(new EmoteHandler(this, this.packetEmitter));
        packetHandlers.add(new PvPInfoHandler(this, this.packetEmitter));
        packetHandlers.add(new UseMedKitHandler(this, this.packetEmitter));
        // packetHandlers.add(new TimerGameHandler(this, this.packetEmitter));
        packetHandlers.add(new CurrentWorldSnapshothandler(this, this.packetEmitter));
        packetHandlers.add(new PlayerSuppressedHandler(this, packetEmitter));
        packetHandlers.add(new UseAmmoHandler(this, this.packetEmitter));


    }


    public TileDetails getTile(float x, float y)
    {

        int i = (int) (y / TILE_HEIGHT);
        int j = (int) (x / TILE_WIDTH);

        if (i < 0 || j < 0)
        {
            return null;
        }
        if (i >= tileCountY || j >= tileCountX)
        {
            return null;
        }
        return GameConstants.TILE_DETAILS.get(tiles[i][j]);
    }

    public IoPoint getTileCenter(float x, float y)
    {

        int i = (int) (y / TILE_HEIGHT);
        int j = (int) (x / TILE_WIDTH);

        if (i < 0 || j < 0)
        {
            return null;
        }
        if (i >= tileCountY || j >= tileCountX)
        {
            return null;
        }
        return new IoPoint(i * TILE_HEIGHT + TILE_HEIGHT / 2, j * TILE_WIDTH + TILE_WIDTH / 2);
    }

    private List<TileDetails> defineTilesForCollision(float x, float y)
    {
        List<TileDetails> res = new ArrayList<>();

        int k = (int) (y / TILE_HEIGHT);
        int m = (int) (x / TILE_WIDTH);

        for (int i = k - 1;
             i < k + 2;
             i++)
        {
            for (int j = m - 1;
                 j < m + 2;
                 j++)
            {
                if (i < 0 || j < 0)
                {
                    continue;
                }
                if (i >= tileCountX || j >= tileCountY)
                {
                    continue;
                }
                res.add(GameConstants.TILE_DETAILS.get(tiles[i][j]));
            }
        }
        return res;
    }

    private void RegisterEntityDestroy(String entityId)
    {
        EntityRemovePacket p = new EntityRemovePacket();
        p.entityId = entityId;
        packetEmitter.emitToAllPlayers(p);

        mapEntities.remove(entityId);
    }


    public void RegisterKill(String killerId, String victimId)
    {
        if (DebugMode)
        {
            IoLogger.log("RegisterKill killerId = " + killerId);
        }
        if (DebugMode)
        {
            IoLogger.log("RegisterKill victimId = " + victimId);
        }
        PlayerCell killer = alivePlayersMap.get(killerId);
        //killer.setScore(killer.getScore()+1);
        //System.out.println("killerId = [" + killerId + "], victimId = [" + victimId + "]");

        PlayerCell victim = alivePlayersMap.get(victimId);

        statsManager.RegisterKill(killer, victim);
        dropDeadPlayerItems(victim);

        PvPInfoPacket p = new PvPInfoPacket(killer.getUsername(), victim.getUsername());
        packetEmitter.emitToAllPlayers(p);


        PlayerDiedPacket p2 = new PlayerDiedPacket(victim.getId());
        packetEmitter.emitToAllPlayers(p2);

        alivePlayersMap.remove(victimId);

        // justdisconnectedPlayers.add(victimId);


        if (gamemode.hasSquads())
        {
            squadManager.removePlayer(victim);
        }
    }

    public void dropOutPatrnobox(ItemCell itemCell, int amountToDrop)
    {
        float x = itemCell.getX();
        float y = itemCell.getY();
        String id;
        int counter = 0;

        int t = (int) Math.sqrt(amountToDrop);

        for (float i = x - (100 * t);
             i < x + 200;
             i += 80)
        {

            for (float j = y - (100 * t);
                 j < y + 200;
                 j += 80)
            {
                counter++;
                if (counter > amountToDrop)
                {
                    return;
                }
                IoPoint p = spawnNear(new IoPoint(i, j), (int) itemCell.getCollisionRadius(), 400);

                id = ++itemId + "";

                ItemCell f = new ItemCell(null, null, p.getX(), p.getY(), id, ItemType.PATRON, getArrayRandomForDropPatrnobox(ItemType.PATRON, ITEM_DETAILS_FOR_DROP_PATRONBOX.get(ItemType.PATRON)), DEFAULT_AMMO_AMOUNT, DEFAULT_CLIP_AMMO_AMOUNT);
                worldItems.put(id, f);
                getDroppedItems().add(EntityMapper.mapFullEntity(f));
                if (DebugMode)
                {
                    IoLogger.log("DROP FROM PATRON " + f.getItem_blueprint());
                }
            }
        }

    }

    public void dropOutAmmunition(ItemCell itemCell, int amountToDrop)
    {

        float x = itemCell.getX();
        float y = itemCell.getY();
        String id;
        int counter = 0;
        int t = (int) Math.sqrt(amountToDrop);

        for (float i = x - (100 * t);
             i < x + 200;
             i += 80)
        {

            for (float j = y - (100 * t);
                 j < y + 200;
                 j += 80)
            {
                counter++;
                if (counter > amountToDrop)
                {
                    return;
                }
                IoPoint p = spawnNear(new IoPoint(i, j), (int) itemCell.getCollisionRadius(), 400);

                id = ++itemId + "";

                int rndType = MathUtils.random(0, 100);
                ItemType itemType;
                if (rndType >= 0 && rndType < 50)
                {
                    itemType = ItemType.HP;
                }
                else if (rndType >= 50 && rndType < 80)
                {
                    itemType = ItemType.ARMOR;
                }
                else
                {
                    itemType = ItemType.WEAPON;
                }
                ItemCell f = new ItemCell(null, null, p.getX(), p.getY(), id, itemType, getArrayRandomForDropAmmunition(itemType, ITEM_DETAILS_FOR_DROP_AMMONITION.get(itemType)), DEFAULT_AMMO_AMOUNT, DEFAULT_CLIP_AMMO_AMOUNT);
                worldItems.put(id, f);
                getDroppedItems().add(EntityMapper.mapFullEntity(f));
                if (DebugMode)
                {
                    IoLogger.log("DROP FROM AMMUNITION " + f.getItem_blueprint());
                }
            }
        }


    }


    private void dropDeadPlayerItems(PlayerCell playerCell)
    {

        float x = playerCell.getX();
        float y = playerCell.getY();
        List<ItemCell> tmp = new ArrayList<>(playerCell.getInventoryManager().getItems().values());
        for (ItemCell item : tmp)
        {
            // int t = (int) Math.sqrt((double) tmp.size());

            //  for (float i = x - (100 * t); i < x + 200; i += 80)
            // {

            //     for (float j = y - (100 * t); j < y + 200; j += 80)
            /// {

            IoPoint p = spawnNear(new IoPoint(x, y), (int) item.getCollisionRadius(), 600);
            item.x = p.getX();
            item.y = p.getY();
            getWorldItems().put(item.getId(), item);
            getDroppedItems().add(EntityMapper.mapFullEntity(item));
            if (DebugMode)
            {
                IoLogger.log("DROP FROM PLAYER " + item.getItem_blueprint());
            }
            //  }
            // }
        }

        // }
    }


    private void registerAnimalKill(String victimId)
    {
        //AnimalCell victim = aliveAnimals.get(victimId);
        aliveAnimals.remove(victimId);

    }

    private Set<ServerWebSocket> closedSockets = new HashSet<>();
    private Set<PlayerCell> playerWithClosedSockets = new HashSet<>();


    // main game loop on server side
    public void serverFrame()
    {
        if (serverState.equals(GameServerState.LAUNCHING))
        {
            return;
        }
        if (DebugMode)
        {
            frameStart = System.currentTimeMillis();
        }
        try
        {
            if (lastFrameTime + GameConstants.SERVER_FRAME_RATE > System.currentTimeMillis())
            {
                return;
            }

            float delta = System.currentTimeMillis() - lastFrameTime;
            lastFrameTime = System.currentTimeMillis();
            // IoLogger.log("Game world tick!");
            timerZone();

            gamemode.processGamemodeLogic(delta);
            if (gamemode.isBattleRoyale() && USE_SUPPRESSION)
            {
                updateHpForSuppressedPlayers(delta);
            }

            List<AbstractProjectile> toRemove = new ArrayList<>();
            List<AbstractProjectile> listPatrons = new ArrayList<>(launchedProjectiles);
            if (DebugMode)
            {
                projectileHandlingStart = System.currentTimeMillis();
            }
            for (AbstractProjectile p : listPatrons)
            {
                p.update(delta / 1000);
            }

            for (AbstractProjectile projectile : listPatrons)
            {

                boolean projectileCollided = false;

                if (projectile.reachedDestination() && projectile.canBeDeleted() || tileCollidesWithProjectile(projectile))
                {
                    projectile.collide((Collidable) null);
                    if (projectile.hasSplashDamage())
                    {
                        ArrayList<PlayerCell> damagedPlayers;
                        damagedPlayers = projectile.applySplashDamage(alivePlayersMap.values(), mapEntities.values(), tiles);
                        if (damagedPlayers != null && !damagedPlayers.isEmpty())
                        {
                            if (DebugMode)
                            {
                                IoLogger.log(getWorldIdTag(), "Splash affects " + damagedPlayers.size() + " players");
                            }

                            for (PlayerCell playerCell : damagedPlayers)
                            {
                                if (gamemode.isBattleRoyale() && USE_SUPPRESSION)
                                {
                                    if (playerCell.getCurrentHealth() <= 0 && playerCell.isSuppressed())
                                    {
                                        RegisterKill(projectile.shooterId, playerCell.getId());
                                    }
                                    else if (playerCell.getCurrentHealth() <= 0 && !playerCell.isSuppressed() && USE_SUPPRESSION)
                                    {
                                        playerCell.setCurrentHealth(PLAYERS_MAX_HEALTH);
                                        playerCell.setSuppressed(true);
                                        playerCell.setKillerId(projectile.shooterId);
                                        PlayerRevivingRequest packet = new PlayerRevivingRequest(playerCell.getId(), true);
                                        packetEmitter.emitToAllPlayers(packet);
                                        //send packet to player
                                    }
                                }
                                else
                                {
                                    if (playerCell.getCurrentHealth() <= 0)
                                    {
                                        RegisterKill(projectile.shooterId, playerCell.getId());
                                    }
                                }
                            }
                        }
                    }
                    if (projectile.canBeRemoved())
                    {
                        toRemove.add(projectile);
                        //projectileCollided = true;
                    }
                }

                if (!projectileCollided)
                {
                    for (PlayerCell player : alivePlayersMap.values())
                    {

                        if (alivePlayersMap.get(projectile.shooterId) == null)
                        {
                            break;
                        }

                        if (canCollide(player, projectile))
                        {
                            if (player.collides(projectile))
                            {
                                if (projectile instanceof Knife && player.getCurrentArmor() > PLAYERS_MIN_ARMOR)
                                {//knife
                                    toRemove.add(projectile);
                                    projectileCollided = true;
                                    break;
                                }

                                if (!player.getId().equals(projectile.shooterId))
                                {
                                    // IoLogger.log(tag + "Proj collides player!");
                                    projectile.collide(player);
                                    statsManager.handleDamageStats(projectile.shooterId, player.getId(), projectile.damage);

                                    //KILL LOGIC
                                    if (gamemode.isBattleRoyale() && USE_SUPPRESSION)
                                    {
                                        if (player.getCurrentHealth() <= 0 && player.isSuppressed())
                                        {
                                            RegisterKill(projectile.shooterId, player.getId());
                                        }
                                        else if (player.getCurrentHealth() <= 0 && !player.isSuppressed())
                                        {
                                            player.setCurrentHealth(PLAYERS_MAX_HEALTH);
                                            player.setSuppressed(true);
                                            player.setKillerId(projectile.shooterId);
                                            PlayerRevivingRequest packet = new PlayerRevivingRequest(player.getId(), true);
                                            packetEmitter.emitToAllPlayers(packet);
                                            //send packet to player
                                        }
                                    }
                                    else
                                    {
                                        if (player.getCurrentHealth() <= 0)
                                        {
                                            RegisterKill(projectile.shooterId, player.getId());
                                        }
                                    }
//                                    if (projectile.hasSplashDamage())
//                                    {
//                                        projectile.applySplashDamage(alivePlayersMap.values(), mapEntities.values(), tiles); //we can hurt ourselvels with splash
//                                    }


                                    //DELETE PATRON
                                    toRemove.add(projectile);
                                    projectileCollided = true;
                                    break;
                                }
                            }
                        }
                    }
                }


                if (!projectileCollided)
                {
                    for (AnimalCell c : aliveAnimals.values())
                    {
                        if (c.collides(projectile))
                        {
                            if (projectile.hasSplashDamage())
                            {
                                //projectile.applySplashDamage(animalsDataMap.values());//TODO: splash damage for animals
                            }
                            // IoLogger.log("Proj collides with animal!");
                            projectile.collide(c);

                            if (c.getCurrentHealth() <= 0)
                            {
                                registerAnimalKill(c.getId());
                            }

                            toRemove.add(projectile);
                            projectileCollided = true;
                            break;
                        }
                    }
                }


                if (!projectileCollided)
                {
                    for (MapEntityCell entitis : mapEntities.values())
                    {

                        if (alivePlayersMap.get(projectile.shooterId) == null)
                        {
                            break;
                        }

                        if (isPlayerInsidePit(entitis, projectile))
                        {
                            if (entitis.collides(projectile))
                            {
                                //    IoLogger.log(tag + "Proj collides map entity!");
                                if (projectile instanceof GrenadeAmmo)
                                {
                                    projectile.applyDamageToPit(alivePlayersMap.values(), entitis); //to throw grenade into pit
                                }
                                projectile.collide(entitis);
                                if (entitis.getDurability() < 0)
                                {
                                    justDestroyedEntities.add(entitis.getId());
                                }
                                toRemove.add(projectile);
                                break;
                            }

                        }
                        else if (isCollidableEntity(entitis,projectile))
                        {
                            if (entitis.collides(projectile))
                            {
                                if (!DebugMode)
                                {
                                    IoLogger.log(tag + "Proj collides map entity: " + entitis.getType());
                                }
                                if (!(projectile instanceof GrenadeAmmo))
                                {
                                    projectile.collide(entitis);
                                }
                                if (projectile.hasSplashDamage())
                                {
                                    projectile.applySplashDamage(alivePlayersMap.values(), mapEntities.values(), tiles);
                                }
                                if (entitis.getDurability() < 0)
                                {
                                    justDestroyedEntities.add(entitis.getId());
                                }
                                toRemove.add(projectile);
                                break;
                            }
                        }
                    }
                }
            }


            justDestroyedEntities.forEach(this::RegisterEntityDestroy);

            launchedProjectiles.removeAll(toRemove);
            justDestroyedEntities.clear();


            if (lastUpdateTime + GameConstants.SERVER_SEND_UPDATES_RATE > System.currentTimeMillis())
            {
                return;
            }

            lastUpdateTime = System.currentTimeMillis();
            if (DebugMode)
            {
                updatesSendingStart = System.currentTimeMillis();
            }
            // send updates to all players
            WorldUpdatePacket worldUpdatePacket = new WorldUpdatePacket(PacketType.WORLD_UPDATE);


            worldUpdatePacket.leftPlayers = Arrays.copyOf(justdisconnectedPlayers.toArray(), justdisconnectedPlayers.size(), String[].class);
            justdisconnectedPlayers.clear();


            // List t = new CopyOnWriteArrayList(justJoinedPlayers);
            worldUpdatePacket.joinedPlayers = Arrays.copyOf(justJoinedPlayers.toArray(), justJoinedPlayers.size(), PlayerCellDto[].class);
            justJoinedPlayers.clear();


            worldUpdatePacket.droppedItems = Arrays.copyOf(droppedItems.toArray(), droppedItems.size(), ItemDto[].class);
            droppedItems.clear();


            worldUpdatePacket.pickedItems = Arrays.copyOf(justPickedItems.toArray(), justPickedItems.size(), String[].class);
            justPickedItems.clear();

            ArrayList<PlayerCellBaseDto> playersData = new ArrayList<>(alivePlayersMap.values().stream().map(EntityMapper::mapBase).collect(Collectors.toList()));
            worldUpdatePacket.playersData = Arrays.copyOf(playersData.toArray(), playersData.size(), PlayerCellBaseDto[].class); //todo old version

            if(changedEntitiesSet.isEmpty())
            {
                // add at least 1 entity
                //very experimental feature
                changedEntitiesSet.add(mapEntities.values().iterator().next());
            }
            ArrayList<MapEntityBaseDto> entities = new ArrayList<>(changedEntitiesSet.stream().map(EntityMapper::mapBase).collect(Collectors.toList()));
            worldUpdatePacket.changedEntitiesData = Arrays.copyOf(entities.toArray(), entities.size(), MapEntityBaseDto[].class);  // todo replace to proper updates with only nearest entities
            changedEntitiesSet.clear();

            packetEmitter.emitToAllPlayers(worldUpdatePacket);

              /*  for (Map.Entry<PlayerCell, ServerWebSocket> playerCellServerWebSocketEntry : packetEmitter.getPlayersToSocket().entrySet()) {
                    ArrayList<PlayerCellDto> tempPlayersData = new ArrayList<>();
                    for (PlayerCellDto player : playersData) {
                        if (distance(playerCellServerWebSocketEntry.getKey(), player) > PLAYER_SIGHT_RADIUS * 1.5)
                            continue;

                        tempPlayersData.add(player);
                    }

                    worldUpdatePacket.playersData = Arrays.copyOf(tempPlayersData.toArray(), tempPlayersData.size(), PlayerCellDto[].class);
                    byte[] dataToSend = serializer.serialize(worldUpdatePacket);


                    try {
                        playerCellServerWebSocketEntry.getValue().write(Buffer.buffer(dataToSend));
                       // IoLogger.log("WoRLD_UPDATE " + worldUpdatePacket+ " sent");
                    } catch (IllegalStateException e) {
                        IoLogger.log(getWorldIdTag()+ "serverFrame Closed socket detected!");
                        closedSockets.add(playerCellServerWebSocketEntry.getValue());
                        playerWithClosedSockets.add(playerCellServerWebSocketEntry.getKey());

                    }
                }*/


            if (DebugMode)
            {
                endingTimestamp = System.currentTimeMillis();
            }
            closedSockets.forEach(s ->
            {
                loadBalancer.removeClientSocket(s);
            });
           /* playerWithClosedSockets.forEach(p->  {
                alivePlayersMap.remove(p.getId());
            });*/
            closedSockets.clear();
            playerWithClosedSockets.clear();


            for (String disconnect : worldUpdatePacket.leftPlayers)
            {
                alivePlayersMap.remove(disconnect); //this line was originally in RegisterKill, but because of that victim didnt get final updatem that he was killed
            }
            gamemode.checkWinCondition();
            checkOutOfBoundPlayers();
            //packetEmitter.emitToAllPlayers(worldUpdatePacket);

        }

        catch (Exception e)
        {
            IoLogger.log(tag + "World fails! " + e.getMessage());
            e.printStackTrace();
        }
        finally
        {
            if (DebugMode && (System.currentTimeMillis() - frameStart > 16)) //lower than 60 fps
            {
                long now = System.currentTimeMillis();
                IoLogger.log(getWorldIdTag() + "Server frame takes " + (now - frameStart) + " ms");
                IoLogger.log(getWorldIdTag() + "Projectile handling takes " + (updatesSendingStart - projectileHandlingStart) + " ms");
                IoLogger.log(getWorldIdTag() + "Sending updates takes " + (endingTimestamp - updatesSendingStart) + " ms");
                IoLogger.log(getWorldIdTag() + "Everything else takes " + (now - endingTimestamp) + " ms");
            }
        }

    }

    private void updateHpForSuppressedPlayers(float delta)
    {
        updateTimeForSuppressedPlayers -= delta;
        if (updateTimeForSuppressedPlayers < 0)
        {
            updateTimeForSuppressedPlayers = UPDATE_TIME_FOR_SUPPRESSED_PLAYERS;
            for (PlayerCell playerCell : alivePlayersMap.values())
            {
                if (playerCell.isSuppressed())
                {
                    playerCell.setCurrentHealth(playerCell.getCurrentHealth() - 5);
                    if (playerCell.getCurrentHealth() <= 0)
                    {
                        if (playerCell.getCurrentHealth() <= 0 && playerCell.isSuppressed())
                        {
                            RegisterKill(playerCell.getKillerId(), playerCell.getId());
                        }
                    }
                }
            }
        }

    }

    public void timerZone()
    {
        //TIMER GAME

        if (!startTimer && serverState.equals(GameServerState.GAME_IN_PROGRESS))
        {
            this.startTimer = true;
            int zoneDuration[] = new int[3];
            zoneDuration[0] = ZONE_GREEN_DURATION;
            zoneDuration[1] = ZONE_YELLOW_DURATION;
            zoneDuration[2] = ZONE_RED_DURATION;
            TimerGamePacket timerGamePacket = new TimerGamePacket();
            timerGamePacket.timer = serverTime;
            timerGamePacket.zoneDuration = zoneDuration;
            packetEmitter.emitToAllPlayers(timerGamePacket);
        }
        if (startTimer && serverState.equals(GameServerState.GAME_IN_PROGRESS) && timeUpdate + GameConstants.TIMER_RATE < System.currentTimeMillis())
        {
            serverTime++;
            timeUpdate = System.currentTimeMillis();


            if (serverTime > ZONE_GREEN_DURATION + ZONE_YELLOW_DURATION)
            {
                currentGameZone = GameZone.RED;
            }

            if (serverTime > ZONE_GREEN_DURATION)
            {
                currentGameZone = GameZone.YELLOW;
            }
        }


    }

    private boolean isPlayerInsidePit(MapEntityCell c, AbstractProjectile p)
    {
        return alivePlayersMap.get(p.shooterId).isInsidePit() && c.getType() == MapEntityType.PIT;
    }

    private boolean isCollidableEntity(MapEntityCell c, AbstractProjectile patron)
    {

        if (patron instanceof GrenadeAmmo || patron instanceof FlashbangAmmo)return false;
        switch (c.getType())
        {
            case BUSH:
            case TACTICAL_SHIELD:
            case STONE_SMALL:
            case STONE_BIG:
            case STONE_GREEN:
                return true;

        }
        return false;
    }
    private boolean tileCollidesWithProjectile(AbstractProjectile projectile)
    {
        TileDetails t = getTile(projectile.getCurrentPosition().getX(), projectile.getCurrentPosition().getY());

        if (t != null && t.isObstacle())
        {

            // true for all excep grenADES
            return !(projectile instanceof GrenadeAmmo || projectile instanceof FlashbangAmmo);
        }

        return false;
    }

    private boolean canCollide(PlayerCell c, AbstractProjectile p)
    {
        return (c.isInsidePit() && alivePlayersMap.get(p.shooterId).isInsidePit() || //players should be on the same side: or both in pit or both outside pit
                !c.isInsidePit() && !alivePlayersMap.get(p.shooterId).isInsidePit()
                || p instanceof GrenadeAmmo); //grenade can collides with players in pit if its owner outside pit
              /*  && !c.isUnderWeb(); *///if player in jug proj don't collide with he
    }


    public IoPoint spawnNear(IoPoint point, int collisionRadius, int maxDistance)
    {

        IoLogger.log(getWorldIdTag(), "SpawnNear " + point);
        for (int i = 0;
             i < 50;
             i++)
        {

            IoPoint tmp = point.copy();
            tmp.x += GameClient.nextFloat(-maxDistance / 2, maxDistance / 2);
            tmp.y += GameClient.nextFloat(-maxDistance / 2, maxDistance / 2);

            Circle playerCircle = new Circle(tmp.x, tmp.y, collisionRadius);
            Circle entitiesCircle = new Circle(tmp.x, tmp.y, collisionRadius);
            boolean found = true;

            TileDetails t = getTile(tmp.x, tmp.y);
            if (t == null || t.isObstacle())
            {
                found = false;
            }
            else
            {
                //checking 8 nearest tiles + player tile
                for (TileDetails tile : defineTilesForCollision(tmp.x, tmp.y))
                {
                    if (tile != null && tile.isObstacle())
                    {
                        found = false;
                    }
                }
            }
            for (PlayerCell p : alivePlayersMap.values())
            {
                if (playerCircle.contains(p.x, p.y))
                {
                    found = false;
                }
            }
            for (AnimalCell a : aliveAnimals.values())
            {
                if (playerCircle.contains(a.x, a.y))
                {
                    found = false;
                }
            }

            for (ItemCell m : worldItems.values())
            {
                if (playerCircle.contains(m.x, m.y))
                {
                    found = false;
                }
            }

            for (MapEntityCell m : mapEntities.values())
            {
                entitiesCircle.setPosition(m.x, m.y);
                entitiesCircle.setRadius(m.getCollisionRadius());
                if (playerCircle.contains(entitiesCircle))
                {
                    found = false;
                }
            }

            if (found)
            {
                return tmp;
            }

        }

        IoLogger.log(getWorldIdTag() + "spawnNear cannot find point to spawn @: " + point);
        return point;

    }

   /*
    public IoPoint spawnNear(IoPoint point, int collisionRadius, int maxDistance)
    {

        final int offsetStep = 25;
        int offsetY = 0;
        while (offsetY*offsetStep < maxDistance)
        {
            IoPoint tmp = point.copy();

            tmp.y += offsetY * offsetStep;
            float offsetX = 0;
            while(offsetX * offsetStep < maxDistance) {
                tmp.x = point.copy().getX() + offsetX * offsetStep;

                PlayerCell playerCircle = new PlayerCell();
                playerCircle.setPosition(tmp);
                playerCircle.setCollisionRadius(collisionRadius);
                boolean found = true;

                TileDetails t = getTile(tmp.x, tmp.y);
                if (t == null || t.isObstacle()) {
                    found = false;
                } else {
                    //checking 8 nearest tiles + player tile
                    for (TileDetails tile : defineTilesForCollision(tmp.x, tmp.y)) {
                        if (tile != null && tile.isObstacle()) {
                            found = false;
                        }
                    }
                }
                for (PlayerCell p : alivePlayersMap.values()) {
                    if (playerCircle.collides(p)) {
                        found = false;
                    }
                }
                for (AnimalCell a : aliveAnimals.values()) {
                    if (playerCircle.collides(a)) {
                        found = false;
                    }
                }

                for (MapEntityCell m : mapEntities.values()) {
                    if (playerCircle.collides(m)) {
                        found = false;
                    }
                }

                for (ItemCell m : worldItems.values()) {
                    if (playerCircle.collides(m)) {
                        found = false;
                    }
                }

                if (found) {
                    return tmp;
                }
                offsetX *= -1;
                if(offsetX >= 0)
                    offsetX++;
            }
            offsetY *= -1;
            if(offsetY >= 0)
                offsetY++;

        }

        IoLogger.log(getWorldIdTag() + "spawnNear cannot find point to spawn @: " + point);
        return point;

    }
*/

    public void restart(GameType type)
    {
        //cleanup phase

        IoLogger.log(getWorldIdTag() + " server cleaning up!");
        packetEmitter.restart();
        aliveAnimals.clear();
        alivePlayersMap.clear();
        worldItems.clear();
        justdisconnectedPlayers.clear();
        justDestroyedEntities.clear();
        justJoinedPlayers.clear();
        droppedItems.clear();
        justPickedItems.clear();
        launchedProjectiles.clear();
        playersSpawnPoints.clear();
        animalsSpawnPoints.clear();
        itemsSpawnPoints.clear();
        mapEntities.clear();
        changedEntitiesSet.clear();

        if (shouldBeDisposed)
        {
            loadBalancer.getGameWorlds().remove(this);
            return;
        }

        //restart phase
        IoLogger.log(getWorldIdTag() + " server restarting!");
        serverState = GameServerState.LAUNCHING;

        switch (type)
        {

            case SOLO:
                gamemode = new SoloGamemode(this);
                break;
            case DUO:
                gamemode = new DuoGamemode(this);
                squadManager = new SquadManager(this, 2);
                break;
            case SQUAD:
                gamemode = new SquadGamemode(this);
                squadManager = new SquadManager(this, 4);
                break;
            case TWO_TEAMS:
                gamemode = new TwoTeamsGamemode(this);
                squadManager = new TwoTeamsManager(this, 50);
                break;
            case CONQUEST:

                gamemode = new ConquestGamemode(this);
                squadManager = new TwoTeamsManager(this, 50);
                break;
        }

        spawnTiles(mapName, mapNameProduction);
        spawnEntities(mapName, mapNameProduction);
        spawnItems();

        IoLogger.log(tag + " : spawned entities: " + mapEntities.size());
        IoLogger.log(tag + ": alivePlayersMap: " + alivePlayersMap.size());
        IoLogger.log(tag + ": spawned items: " + worldItems.size());
        IoLogger.log(tag + ": itemsSpawnPoints: " + itemsSpawnPoints.size());
        IoLogger.log(tag + ": playersSpawnPoints: " + playersSpawnPoints.size());
        IoLogger.log(tag + "GameWorld: alive animals: " + aliveAnimals.size());
        IoLogger.log(tag + "GameWorld: animalsSpawnPoints: " + animalsSpawnPoints.size());
        IoLogger.log(tag + "GameWorld: gamemode: " + gamemode.getGameType());

        serverState = GameServerState.WAITING_FOR_PLAYERS;
        packetEmitter.emitToAllPlayers(new GameStateChangedPacket(GameServerState.WAITING_FOR_PLAYERS, gamemode.getGameType()));

    }

    private static double distance(PlayerCell a, PlayerCellDto b)
    {
        return Math.hypot(a.getX() - b.getX(), a.getY() - b.getY());
    }

    private void spawnItems()
    {
        /*if(food.size() > MAX_ITEMS_AMOUNT_ON_SERVER) return;
        if(r.nextFloat() < 0.1) return;
        int i = 0;
        while (i < 30)
        {
            String id= ++itemId +"";
            ItemCell f = new ItemCell(null,null,
                    r.nextInt(GameConstants.WORLD_WIDTH),r.nextInt(GameConstants.WORLD_HEIGHT),id,
                    ItemType.values()[r.nextInt(ItemType.values().length-1)] );


            food.put(id, f);
            justSpawnedFood.add(EntityMapper.mapFullEntity(f));

            i++;
        }*/
        //armor 20%, hp 20%, weapon 40%, ammo 20%
        for (ItemsSpawnPoint i : itemsSpawnPoints)
        {
            String id = ++itemId + "";
            int value = MathUtils.random(0, 100);
            ItemType itemType;
            if (value < 15)
            {
                itemType = ItemType.ARMOR;
            }
            else if (value >= 15 && value < 35)
            {
                itemType = ItemType.HP;
            }
            else if (value >= 35 && value < 70)
            {
                itemType = ItemType.WEAPON;
            }
            else if (value >= 70 && value <= 80)
            {
                itemType = ItemType.AMMUNITION;
            }
            else
            {
                itemType = ItemType.PATRON;
            }
            //ItemType itemType = ItemType.values()[r.nextInt(ItemType.values().length)];
            ItemCell f = new ItemCell(null, null, i.getX(), i.getY(), id, itemType, getArrayRandom(itemType, ITEM_DETAILS.get(itemType)), DEFAULT_AMMO_AMOUNT, DEFAULT_CLIP_AMMO_AMOUNT);

            worldItems.put(id, f);
            EntityMapper.mapFullEntity(f);
        }

        for (AnimalsSpawnPoint i : animalsSpawnPoints)
        {
            String id = ++animalId + "";
            //AnimalType itemType = AnimalType.values()[r.nextInt(ItemType.values().length-1)];
            /*AnimalCell f = new AnimalCell(null,null,i.getX(),i.getY(), 50,id,100);
            aliveAnimals.put(id,f);
            EntityMapper.mapFull(f);*/
            //TODO: Uncomment to get animals
        }


        // IoLogger.log("GameWorld:  food on server: " + food.size());
    }

    private void spawnEntities(String mapName, String mapNameProduction)
    {
        Color[][] colors = new Color[0][];
        try
        {
            BufferedImage image;
            if(MAPS_SWITH == true) {
                image = PRODUCTION_MODE ?
                        ImageIO.read(new URL("https://image.ibb.co/ivHjG8/entity.png")) : //ImageIO.read(new URL("https://image.ibb.co/mLwjzn/entities.png")): ARENA
                        //ImageIO.read(new File("maps/Tile8.png"));
                        ImageIO.read(new File("maps/" + mapNameProduction + "/entities.png"));
                colors = new Color[image.getHeight()][image.getWidth()];
            }
            if(MAPS_SWITH == false) {
                image = PRODUCTION_MODE ?
                        ImageIO.read(new URL("https://image.ibb.co/cExMGK/entities.png")) : //ImageIO.read(new URL("https://image.ibb.co/mLwjzn/entities.png")): ARENA
                        //ImageIO.read(new File("maps/Tile8.png"));
                        ImageIO.read(new File("maps/" + mapName + "/entities.png"));
                colors = new Color[image.getHeight()][image.getWidth()];
            }


            for (int y = 0;
                 y < image.getHeight();
                 y++)
            {
                for (int x = 0;
                     x < image.getWidth();
                     x++)
                {

                    java.awt.Color c = new java.awt.Color(image.getRGB(x, y));
                    colors[y][x] = new Color(c.getRed() / 255f, c.getGreen() / 255f, c.getBlue() / 255f, 1);
                }
            }

        }
        catch (IOException e)
        {
            e.printStackTrace();
        }


        int tileYAmount = colors.length;
        int tileXAmount = colors[0].length;


        for (int i = 0;
             i < tileYAmount;
             i++)
        {
            for (int j = 0;
                 j < tileXAmount;
                 j++)
            {

                if (tileYAmount - 1 < i || tileXAmount - 1 < j)
                {
                    continue;
                }

                MapEntityType m = MapEntityType.getEntityByColor(colors[i][j]);
                /*if(m != null) {
                    if (MapEntityType.getEntityByColor(colors[i][j]).equals(MapEntityType.BUCH_WALL)) { //Deleting extra wall line in horizontal
                        if (j < tileCountX - 2) { //Removing extra horizontals
                            if (MapEntityType.getEntityByColor(colors[i][j + 1]) != null &&
                                    MapEntityType.getEntityByColor(colors[i][j + 1]).equals(MapEntityType.BUCH_WALL) &&
                                    MapEntityType.getEntityByColor(colors[i][j + 2]) != null &&
                                    !MapEntityType.getEntityByColor(colors[i][j + 2]).equals(MapEntityType.BUCH_WALL)) {
                                m = null;
                                j++;
                            }
                        }
                    }
                }*/


                if (m != null)
                {
                    String id = ++entityId + "";


                    MapEntityCell res = null;
                    switch (m)
                    {
                        case PIT:
                        case PIT_TRAP:
                        case PIT_TRAP2:
                            if (USE_PITS)
                            {
                                res = new MapEntityCell(null, null, j * TILE_WIDTH, i * TILE_HEIGHT, id, m);
                            }
                            break;

                        case SPIDER_WEB:
                        case SPIDER_WEB_BIG:

                            if (USE_WEBS)
                            {
                                res = new MapEntityCell(null, null, j * TILE_WIDTH, i * TILE_HEIGHT, id, m);
                            }
                            break;

                        case PLANT:
                        case STONE_SMALL:
                        case STONE_BIG:
                        case STONE_GREEN:
                        case BUSH:


                        case FLOOR_BUSH_GREEN:
                        case FLOOR_BUSH_GREEN_FLOWERS:
                        case GREEN_ZONE:
                        case YELLOW_ZONE:
                        case RED_ZONE:


                        case TRAP:
                        case MUD:
                            res = new MapEntityCell(null, null,
                                    j * TILE_WIDTH, i * TILE_HEIGHT, id, m);
                            break;
                        case TACTICAL_SHIELD:
                            if (USE_SHIELDS)
                            {
                                res = new TacticalShield(null, null, j * TILE_WIDTH, i * TILE_HEIGHT, id, m);
                            }

                            break;


                        case CAPTURABLE_POINT:
                            res = new CapturablePointClient(null, null,
                                    j * TILE_WIDTH, i * TILE_HEIGHT, id, m);

                            if (gamemode instanceof ConquestGamemode)
                            {
                                ConquestGamemode c = (ConquestGamemode) gamemode;
                                if (c.getCapturablePointsManager().getCapturablePoints().size() >= CONQUEST_POINTS_NUMBER)
                                {
                                    IoLogger.log(getWorldIdTag(), "We already have enough capturablePoints");
                                    break;
                                }
                                //Same id for CapturablePointClient and  CapturablePoint
                                CapturablePoint capturablePoint = new CapturablePoint(this, j * TILE_WIDTH, i * TILE_HEIGHT, id);
                                c.getCapturablePointsManager().capturablePoints.add(capturablePoint);
                            }

                            break;

                    }
                    if (res == null)
                    {
                        continue;
                    }
                    if (res.getType().equals(MapEntityType.GREEN_ZONE) || res.getType().equals(MapEntityType.YELLOW_ZONE) || res.getType().equals(MapEntityType.RED_ZONE))
                    {
                        zoneMapManager.addTile(res, res.getX(), res.getY());
                    }

                    mapEntities.put(id, res);


                }
            }
        }

        /*for (int i = 0; i < tileYAmount; i++) {
            for (int j = 0; j < tileXAmount; j++) {
                try {
                    if (i < tileCountY - 2) { //Removig extra verical
                        if (MapEntityType.getEntityByColor(colors[i][j]) != null &&
                                MapEntityType.getEntityByColor(colors[i][j]).equals(MapEntityType.BUCH_WALL) &&
                                MapEntityType.getEntityByColor(colors[i + 1][j]) != null &&
                                MapEntityType.getEntityByColor(colors[i + 1][j]).equals(MapEntityType.BUCH_WALL) *//*&&
                                MapEntityType.getEntityByColor(colors[i + 2][j]) != null &&
                                !MapEntityType.getEntityByColor(colors[i + 2][j]).equals(MapEntityType.BUCH_WALL)*//*) {
                            MapEntityCell mapEntityCellToRemove = null;
                            for (MapEntityCell mapEntityCell : mapEntities.values()) {
                                if (mapEntityCell.getX() == (j + 1) * TILE_WIDTH && mapEntityCell.getY() == (i * TILE_HEIGHT)) {
                                    mapEntityCellToRemove = mapEntityCell;
                                    break;
                                }
                            }
                            if(mapEntityCellToRemove != null) {
                                mapEntities.remove(mapEntityCellToRemove);
                                mapEntities.values().remove(mapEntityCellToRemove);
                            }
                        }
                    }
                } catch (Exception e) {
                    IoLogger.log(e.getMessage());
                }
            }
        }*/
        initSpawnPoints(colors);
        zoneMapManager.initZones();
    }

    private void initSpawnPoints(Color[][] mapColors)
    {
        int tileYAmount = mapColors.length;
        int tileXAmount = mapColors[0].length;

        for (int i = 0;
             i < tileYAmount;
             i++)
        {
            for (int j = 0;
                 j < tileXAmount;
                 j++)
            {

                if (tileYAmount - 1 < i || tileXAmount - 1 < j)
                {
                    continue;
                }
                else
                {
                    SpawnPoint.SpawnPointType t = SpawnPoint.SpawnPointType.getSpawnByColor(mapColors[i][j]);

                    if (t != null)
                    {
                        switch (t)
                        {
                            case ITEMS:
                                itemsSpawnPoints.add(new ItemsSpawnPoint(j * TILE_WIDTH, i * TILE_HEIGHT));
                                break;
                            case PLAYERS:
                                playersSpawnPoints.add(new PlayersSpawnPoint(j * TILE_WIDTH, i * TILE_HEIGHT));
                                break;
                            case ANIMALS:
                                animalsSpawnPoints.add(new AnimalsSpawnPoint(j * TILE_WIDTH, i * TILE_HEIGHT));
                                break;
                            case TEAMS_SPAWN:
                                //todo
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
        }
    }

    private void spawnTiles(String mapName, String mapNameProduction)
    {
        Color[][] colors = new Color[0][];
        try
        {
            BufferedImage image;
            if (MAPS_SWITH == true){
            image = PRODUCTION_MODE ?
                    ImageIO.read(new URL("https://image.ibb.co/nkRTb8/tilis.png")) :
                    ImageIO.read(new File("maps/" +mapNameProduction + "/tiles.png"));
            colors = new Color[image.getHeight()][image.getWidth()];
            }
            else {
                 image = PRODUCTION_MODE ?
                        ImageIO.read(new URL("https://image.ibb.co/kjUqAe/tiles.png")) :
                        ImageIO.read(new File("maps/" + mapName + "/tiles.png"));
                colors = new Color[image.getHeight()][image.getWidth()];
            }
            for (int y = 0;
                 y < image.getHeight();
                 y++)
            {
                for (int x = 0;
                     x < image.getWidth();
                     x++)
                {
                    java.awt.Color c = new java.awt.Color(image.getRGB(x, y));
                    colors[y][x] = new Color(c.getRed() / 255f, c.getGreen() / 255f, c.getBlue() / 255f, 1);
                }
            }

        }
        catch (IOException e)
        {
            e.printStackTrace();
        }

        tileCountY = colors.length; //(int) (WORLD_WIDTH/TILE_WIDTH);
        tileCountX = colors[0].length;  //(int) (WORLD_HEIGHT/TILE_HEIGHT);

        tiles = new TileType[tileCountY][tileCountX];

        for (int i = 0;
             i < tileCountY;
             i++)
        {
            for (int j = 0;
                 j < tileCountX;
                 j++)
            {
                if (tiles[i][j] == null)
                {
                    if (tileCountY - 1 < i || tileCountX - 1 < j)
                    {
                        tiles[i][j] = TileType.FLOOR_YELLOW;
                    }
                    else
                    {
                        tiles[i][j] = TileType.getTileByColor(colors[i][j]);

                    }
                }
            }
        }


        InitialTileSnapshotPacket tileSnapshotPacket = new InitialTileSnapshotPacket();
        tileSnapshotPacket.tiles = TileSerializer.serializeTiles(tiles, tileCountX, tileCountY);
        tilePacket = serializer.serialize(tileSnapshotPacket);


        //initSpawnPoints(colors);
    }


    private long lastTimePlayersChecked;

    private void checkOutOfBoundPlayers()
    {
        if (currentGameZone.equals(GameZone.GREEN))
        {
            return;
        }
        if (serverTime == lastTimePlayersChecked)
        {
            return;
        }

        lastTimePlayersChecked = serverTime;

        if (currentGameZone.equals(GameZone.YELLOW))
        {
            for (PlayerCell p : alivePlayersMap.values())
            {
                if (zoneMapManager.getPlayerZone(p.getPosition()).equals(MapEntityType.GREEN_ZONE))
                {
                    IoLogger.log("Player " + p + " in GREEN ZONE");
                    p.setCurrentHealth(p.getCurrentHealth() - 1);
                    RegisterKill(p.getId(), p.getId());
                }

            }
        }
        else if (currentGameZone.equals(GameZone.RED))
        {
            for (PlayerCell p : alivePlayersMap.values())
            {
                if (!zoneMapManager.getPlayerZone(p.getPosition()).equals(MapEntityType.RED_ZONE))
                {
                    IoLogger.log("Player " + p + " not in RED ZONE");
                    p.setCurrentHealth(p.getCurrentHealth() - 1);
                    RegisterKill(p.getId(), p.getId());
                }
            }
        }
    }


    public IoPoint spawnOnPoint()
    {
        int minSpawns = 10000;
        PlayersSpawnPoint spawn = null;
        for (PlayersSpawnPoint s : getPlayersSpawnPoints())
        {
            if (s.getSpawnedPlayersCount() == 0)
            {
                return s.spawn();
            }
            else if (s.getSpawnedPlayersCount() < minSpawns)
            {
                minSpawns = s.getSpawnedPlayersCount();
                spawn = s;
            }
        }

        return spawn.spawn();
    }

    public int getServerTime()
    {
        return serverTime;
    }

    public boolean isFull()
    {
        return alivePlayersMap.size() >= MAX_PLAYERS_IN_INSTANCE;
    }


    public Map<String, PlayerCell> getAlivePlayersMap()
    {
        return alivePlayersMap;
    }

    public Map<String, AnimalCell> getAliveAnimals()
    {
        return aliveAnimals;
    }

    public List<String> getJustdisconnectedPlayers()
    {
        return justdisconnectedPlayers;
    }


    public Map<String, ItemCell> getWorldItems()
    {
        return worldItems;
    }

    public TileType[][] getTiles()
    {
        return tiles;
    }

    public int getTileCountX()
    {
        return tileCountX;
    }

    public int getTileCountY()
    {
        return tileCountY;
    }

    public List<String> getJustDestroyedEntities()
    {
        return justDestroyedEntities;
    }

    public List<PlayerCellDto> getJustJoinedPlayers()
    {
        return justJoinedPlayers;
    }

    public List<ItemDto> getDroppedItems()
    {
        return droppedItems;
    }

    public List<String> getJustPickedItems()
    {
        return justPickedItems;
    }

    public List<AbstractProjectile> getLaunchedProjectiles()
    {
        return launchedProjectiles;
    }

    public List<PlayersSpawnPoint> getPlayersSpawnPoints()
    {
        return playersSpawnPoints;
    }

    public List<ItemsSpawnPoint> getItemsSpawnPoints()
    {
        return itemsSpawnPoints;
    }

    public Map<String, MapEntityCell> getMapEntities()
    {
        return mapEntities;
    }

    public GameServerState getServerState()
    {
        return serverState;
    }

    public void setServerTime(int serverTime)
    {
        this.serverTime = serverTime;
    }

    public void setServerState(GameServerState serverState)
    {
        this.serverState = serverState;
    }

    public Gamemode getGamemode()
    {
        return gamemode;
    }

    public SquadManager getSquadManager()
    {
        return squadManager;
    }

    public LoadBalancer getLoadBalancer()
    {
        return loadBalancer;
    }

    public boolean isShouldBeDisposed()
    {
        return shouldBeDisposed;
    }

    public void setShouldBeDisposed(boolean shouldBeDisposed)
    {
        this.shouldBeDisposed = shouldBeDisposed;
    }


    public Set<MapEntityCell> getChangedEntitiesSet()
    {
        return changedEntitiesSet;
    }

    public StatsManager getStatsManager()
    {
        return statsManager;
    }


    private enum GameZone
    {
        GREEN, YELLOW, RED
    }

    public TwoTeamsManager getTwoTeamsManager()
    {
        return (TwoTeamsManager) squadManager;
    }

}
