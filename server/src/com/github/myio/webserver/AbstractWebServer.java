package com.github.myio.webserver;


import com.github.myio.ServerLauncher;
import com.github.myio.dto.masterserver.GameType;
import com.github.myio.dto.masterserver.HttpServerDto;
import com.github.myio.dto.masterserver.Region;
import com.github.myio.tools.IoLogger;

import java.net.MalformedURLException;
import java.net.URL;


public abstract class AbstractWebServer
{
    protected ServerLauncher serverLauncher;

    protected Region region = null;
    protected String url = null;
    protected GameType gameType = null;

    public AbstractWebServer(ServerLauncher serverLauncher) {
        this.serverLauncher = serverLauncher;
    }

    public abstract void run();
    public abstract String getUrl();
    public abstract Region getRegion();


    protected void changeServerConfig(String url, String region, String gameType, String serversCount)
    {
        if(url != null )
        {
            try
            {
                URL u = new URL(url);
                 this.url = url;
            } catch (MalformedURLException e) {
                IoLogger.log("changeServerConfig wrong url " + url);
            }
        }
        if(region != null )
        {
              try
              {

                  this.region = Region.valueOf(region);
              }
                catch (IllegalArgumentException e)
                {
                    IoLogger.log("changeServerConfig wrong region " + region);
                }
        }

        if(serversCount != null )
        {


            try {
                int c = Integer.parseInt(serversCount);
                serverLauncher.getLoadBalancer().changeServersCount(c);

            } catch (NumberFormatException e) {
                e.printStackTrace();
            }

        }

        if(gameType != null )
        {
            try {

                this.gameType = GameType.valueOf(gameType);

                ServerLauncher.setGameType(this.gameType);
                serverLauncher.restartEmptyGameServers(this.gameType);

            }
            catch (IllegalArgumentException e)
            {
                IoLogger.log("changeServerConfig wrong gameType " + gameType);
            }
        }
    }

    protected HttpServerDto assembleHttpServerInfo()
    {
        HttpServerDto res = new HttpServerDto();
        res.setUrl(getUrl());
        res.setGameServers(serverLauncher.getLoadBalancer().gameServerStatus());
        res.setGameType(ServerLauncher.getGameType());
        res.setRegion(getRegion());

        return res;
    }
}
