package com.github.myio.webserver;

import com.github.myio.ServerLauncher;
import com.github.myio.dto.masterserver.HttpServerDto;
import com.github.myio.dto.masterserver.Region;
import com.github.myio.tools.IoLogger;

import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.http.HttpClient;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.http.HttpServerRequest;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.Json;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;

import static com.github.myio.GameConstants.MASTER_SERVER_DATA_POST_INTERVAL;
import static com.github.myio.GameConstants.MASTER_SERVER_URL;

/**
 * Production server
 */

public class HerokuWebServer extends AbstractWebServer {


    public HerokuWebServer(ServerLauncher gameServer) {
        super(gameServer);
    }


    @Override
    public void run() {



        IoLogger.log("serverPort " + System.getenv("http.port"));
        IoLogger.log("serverPort2  " + System.getenv("$PORT"));
        IoLogger.log("serverPort3  " + System.getenv("PORT"));


        final Vertx vertx = Vertx.vertx();
        final io.vertx.core.http.HttpServer httpServer = vertx.createHttpServer();


        // exposing API to get status of server from web
        Router router = Router.router(vertx);
        router.route(HttpMethod.GET, "/status/").handler(new Handler<RoutingContext>() {
            @Override
            public void handle(RoutingContext event) {
                HttpServerDto data = assembleHttpServerInfo();

                HttpServerResponse r = event.response();
                r.putHeader("content-type", "json");
                r.end(Json.encode(data));

            }
        });

        // exposing API to set URL/Region/GameType

        //todo remove after testing
        router.route(HttpMethod.GET, "/configure/").handler(new Handler<RoutingContext>() {
            @Override
            public void handle(RoutingContext event) {

                //  https://io-europe.herokuapp.com/configure?url=https://io-europe.herokuapp.com&region=EUROPE&gametype=SOLO&serversCount=8
                HttpServerRequest r = event.request();
                String url = r.getParam("url");
                String region = r.getParam("region");
                String gameType = r.getParam("gameType");
                String serversCount = r.getParam("serversCount");

                changeServerConfig(url, region, gameType, serversCount);

            }
        });

        int port = Integer.parseInt(System.getenv("PORT"));

        httpServer.websocketHandler(webSocket -> {
            serverLauncher.getLoadBalancer().addNewClientSocket(webSocket);
            webSocket.frameHandler(frame -> { serverLauncher.getLoadBalancer().clientSocketHandle(frame,webSocket); });
           // webSocket.textMessageHandler(frame -> { serverLauncher.getLoadBalancer().pingHandler(frame,webSocket); });
            webSocket.exceptionHandler(Throwable::printStackTrace);
            webSocket.closeHandler(event -> serverLauncher.getLoadBalancer().removeClientSocket(webSocket));
        })      .requestHandler(router::accept)

                .listen(
                port, System.getProperty("http.address", "0.0.0.0")
        );


        HttpClient httpClient = vertx.createHttpClient();
        long timerId = vertx.setPeriodic(MASTER_SERVER_DATA_POST_INTERVAL, id -> {
            HttpServerDto data = assembleHttpServerInfo();
            if(data.getUrl() == null | data.getRegion() == null)
            {
                IoLogger.log("null URL or region");
                return;
            }


            httpClient.postAbs(MASTER_SERVER_URL+ "/server/heartbeat", response -> {
                IoLogger.log("Received response from master with status  " + response.statusCode());
            }).putHeader("content-type", "application/json; charset=utf8").end(Json.encode(data));
        });

    }


    @Override
    public String getUrl() {


        return url;
    }

    @Override
    public Region getRegion() {

        return region;
    }
}
