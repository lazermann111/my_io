package com.github.myio;

import com.github.myio.conquest.CapturablePoint;
import com.github.myio.dto.AbstractWeaponDto;
import com.github.myio.dto.AnimalCellDto;
import com.github.myio.dto.CapturablePointBaseDto;
import com.github.myio.dto.CapturablePointDto;
import com.github.myio.dto.ItemDto;
import com.github.myio.dto.MapEntityBaseDto;
import com.github.myio.dto.MapEntityDto;
import com.github.myio.dto.PlayerCellDto;
import com.github.myio.dto.masterserver.PlayerCellBaseDto;
import com.github.myio.entities.PlayerCell;
import com.github.myio.entities.animals.AnimalCell;
import com.github.myio.entities.item.ItemCell;
import com.github.myio.entities.map.MapEntityCell;
import com.github.myio.entities.weapons.AbstractRangeWeapon;


public class EntityMapper {

    public static PlayerCellDto map(PlayerCell p)
    {
        PlayerCellDto res = new PlayerCellDto();
        res.setY(p.getY());
        res.setX(p.getX());
        res.setCurrentHealth(p.getCurrentHealth());
        res.setCurrentArmor(p.getCurrentArmor());
        res.setPlayerSkinColor(p.getPlayerSkinColor());
        res.setRotationAngle(p.getRotationAngle());
        res.setUsername(p.getUsername());
        res.setId(p.getId());
        res.setRadiusItem(p.getRadius());
        return res;
    }

    public static PlayerCellBaseDto mapBase(PlayerCell p)
    {
        return new PlayerCellBaseDto(p.getId(), p.getX(), p.getY(), p.getCurrentHealth(), p.getRotationAngle(),p.getCurrentArmor());

    }

    public static PlayerCellDto mapFull(PlayerCell p) // for initial packet
    {
        PlayerCellDto res = new PlayerCellDto();
        res.setY(p.getY());
        res.setX(p.getX());
        res.setCurrentHealth(p.getCurrentHealth());
        res.setCurrentArmor(p.getCurrentArmor());
        res.setPlayerSkinColor(p.getPlayerSkinColor());
        res.setRotationAngle(p.getRotationAngle());
        res.setUsername(p.getUsername());
        res.setId(p.getId());
        res.setRadiusItem(p.getRadius());
        res.setCurrentWeapon(mapFullEntity(p.getCurrentWeapon()));
        return res;
    }

    public static AnimalCellDto mapFull(AnimalCell p) {
        AnimalCellDto res = new AnimalCellDto();
        res.setY(p.getY());
        res.setX(p.getX());
        res.setCurrentHealth(p.getCurrentHealth());
        res.setRotationAngle(p.getRotationAngle());
        res.setId(p.getId());
        res.setRadiusItem(p.getRadius());
        return res;
    }

    public static CapturablePointDto mapFull(CapturablePoint p)
    {
        CapturablePointDto res = new CapturablePointDto(
                p.getId(),
                p.capturingProgress,
                p.captureInProgress,
                p.name,
                p.getX(),
                p.getY(),
                p.pointsToCapture,
                p.owner == null ? -1 : p.owner.getId(),
                p.capturingTeam == null ? -1 : p.capturingTeam.getId()  );


        return res;
    }

    public static CapturablePointBaseDto mapBase(CapturablePoint p)
    {
        CapturablePointBaseDto res = new CapturablePointBaseDto(p.getId(),p.capturingProgress,p.captureInProgress);

        return res;
    }

    public static AbstractWeaponDto mapFullEntity(AbstractRangeWeapon p)
    {
        return p == null? null : new AbstractWeaponDto(p.totalAmmoAmount, p.weaponType);
    }

    public static ItemDto mapFullEntity(ItemCell p)
    {
        ItemDto res = new ItemDto();
        res.setY(p.getY());
        res.setX(p.getX());
        res.setId(p.getId());
        res.setAmmoAmount(p.getTotalAmmoAmount());
        res.setClipAmmoAmount(p.getClipAmmoAmount());
        res.setRadiusItem(p.getRadius());
       // res.setXpGiven(p.getXpForHp());
        res.setItemType(p.getItemType());
        res.setItem_blueprint(p.getItem_blueprint());
        return res;
    }


    public static MapEntityDto mapFullEntity(MapEntityCell p)
    {
        MapEntityDto res = new MapEntityDto();
        res.setY(p.getY());
        res.setX(p.getX());
        res.setId(p.getId());
        res.setRadiusItem(p.getRadius());
        res.setCollisionRadius(p.getCollisionRadius());
        res.setEntityType(p.getType());
        return res;
    }

    public static MapEntityBaseDto mapBase(MapEntityCell p)
    {
        MapEntityBaseDto res = new MapEntityBaseDto();
        res.setId(p.getId());
        res.setY(p.getY());
        res.setX(p.getX());
        res.setRotationAngle(p.getRotationAngle());
        return res;
    }

}
