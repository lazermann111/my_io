package com.github.myio.dto;

import com.github.czyzby.websocket.serialization.SerializationException;
import com.github.czyzby.websocket.serialization.Transferable;
import com.github.czyzby.websocket.serialization.impl.Deserializer;
import com.github.czyzby.websocket.serialization.impl.Serializer;

/**
 * Used for frequent updates
 */

public class CapturablePointBaseDto implements Transferable<CapturablePointBaseDto>
{
    public String id;
    public int capturingProgress;
    public boolean captureInProgress;


    public CapturablePointBaseDto(String id, int capturingProgress, boolean captureInProgress)
    {
        this.id = id;
        this.capturingProgress = capturingProgress;
        this.captureInProgress = captureInProgress;
    }

    @Override
    public void serialize(Serializer serializer) throws SerializationException
    {

    }

    @Override
    public CapturablePointBaseDto deserialize(Deserializer deserializer) throws SerializationException
    {
        return null;
    }


    @Override
    public String toString()
    {
        return "CapturablePointBaseDto{" +
                "id=" + id +
                ", capturingProgress=" + capturingProgress +
                ", captureInProgress=" + captureInProgress +
                '}';
    }
}

