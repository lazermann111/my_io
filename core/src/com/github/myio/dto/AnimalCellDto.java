package com.github.myio.dto;

import com.github.czyzby.websocket.serialization.SerializationException;
import com.github.czyzby.websocket.serialization.Transferable;
import com.github.czyzby.websocket.serialization.impl.Deserializer;
import com.github.czyzby.websocket.serialization.impl.Serializer;

/**
 * Created by dell on 30.03.2018.
 */

public class AnimalCellDto extends CellDto implements Transferable<AnimalCellDto> {
    public long lastHeartbeat;
    public int currentHealth, rotationAngle;

    public AnimalCellDto() {
    }

    public AnimalCellDto(String id, float x, float y, int radius, int currentHealth, int rotationAngle) {
        super(id, x, y, radius);
        this.currentHealth = currentHealth;
        this.rotationAngle = rotationAngle;

    }

    public int getCurrentHealth() {
        return currentHealth;
    }

    public void setCurrentHealth(int currentHealth) {
        this.currentHealth = currentHealth;
    }

    public int getRotationAngle() {
        return rotationAngle;
    }

    public void setRotationAngle(int rotationAngle) {
        this.rotationAngle = rotationAngle;
    }

    @Override
    public void serialize(Serializer serializer) throws SerializationException {
        serializer.serializeString(id)
                .serializeFloat(x)
                .serializeFloat(y)
                .serializeInt(radius)
                .serializeInt(currentHealth)
                .serializeInt(rotationAngle);
    }

    @Override
    public AnimalCellDto deserialize(Deserializer deserializer) throws SerializationException {
        return new AnimalCellDto(
                deserializer.deserializeString(), //id
                deserializer.deserializeFloat(), //x
                deserializer.deserializeFloat(), //y
                deserializer.deserializeInt(), //radiusItem
                deserializer.deserializeInt(), //currentHealth
                deserializer.deserializeInt() //rotationAngle
        );
    }
}
