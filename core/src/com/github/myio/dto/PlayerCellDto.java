package com.github.myio.dto;


import com.github.czyzby.websocket.serialization.SerializationException;
import com.github.czyzby.websocket.serialization.Transferable;
import com.github.czyzby.websocket.serialization.impl.Deserializer;
import com.github.czyzby.websocket.serialization.impl.Serializer;

public class PlayerCellDto extends CellDto implements Transferable<PlayerCellDto> {

    public String username;
    public long lastHeartbeat;
    public AbstractWeaponDto currentWeapon = new AbstractWeaponDto();
    //public ArrayList<AbstractWeaponDto> weaponList; todo

    public int /*maxHealth,*/ currentHealth, rotationAngle;

    int currentArmor;

    public String playerSkinColor;

    public  PlayerCellDto(){}

    public PlayerCellDto(String id, float x, float y, /*int radius,*/ String username, int currentHealth, int rotationAngle,/* int currentScore,*/int currentArmor,String playerSkinColor, AbstractWeaponDto weaponDto) {
        super(id, x, y, 0);
        this.username = username;
     //   this.maxHealth = maxHealth;
        this.currentHealth = currentHealth;
        this.rotationAngle = rotationAngle;
        this.currentArmor = currentArmor;
        this.playerSkinColor = playerSkinColor;
        this.currentWeapon = weaponDto;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public long getLastHeartbeat() {
        return lastHeartbeat;
    }

    public void setLastHeartbeat(long lastHeartbeat) {
        this.lastHeartbeat = lastHeartbeat;
    }


    public int getCurrentHealth() {
        return currentHealth;
    }

    public void setCurrentHealth(int currentHealth) {
        this.currentHealth = currentHealth;
    }

    public int getRotationAngle() {
        return rotationAngle;
    }

    public void setRotationAngle(int rotationAngle) {
        this.rotationAngle = rotationAngle;
    }

    public int getCurrentArmor() {return currentArmor;}

    public void setCurrentArmor(int currentArmor) {this.currentArmor = currentArmor;}

   /* public int getCurrentScore() {
        return currentScore;
    }*

    public void setCurrentScore(int currentScore) {
        this.currentScore = currentScore;
    }*/

    public AbstractWeaponDto getCurrentWeapon() {
        return currentWeapon;
    }

    public void setCurrentWeapon(AbstractWeaponDto currentWeapon) {
        this.currentWeapon = currentWeapon;
    }

    public String getPlayerSkinColor() {return playerSkinColor;}

    public void setPlayerSkinColor(String playerSkinColor) {this.playerSkinColor = playerSkinColor;}

    @Override
    public void serialize(Serializer serializer) throws SerializationException {

        serializer
                .serializeTransferable(currentWeapon)
                .serializeString(id).
                serializeFloat(x).
                serializeFloat(y).
                serializeString(username)
                // serializeInt(maxHealth)
                .serializeInt(currentHealth)
                .serializeInt(rotationAngle).
                serializeInt(currentArmor).
                serializeString(playerSkinColor)
             ;
    }
    protected static AbstractWeaponDto _p = new AbstractWeaponDto();
    @Override
    public PlayerCellDto deserialize(Deserializer deserializer) throws SerializationException {

        AbstractWeaponDto w = deserializer.deserializeTransferable(_p);

        return new PlayerCellDto(
            deserializer.deserializeString(), //id
            deserializer.deserializeFloat(), //x
            deserializer.deserializeFloat(), //y
            deserializer.deserializeString(), //username
         //   deserializer.deserializeInt(), //maxHealth
            deserializer.deserializeInt(), //currentHealth
            deserializer.deserializeInt(), //rotationAngle
            deserializer.deserializeInt(),//currentArmor
                deserializer.deserializeString(),
                w
        );

    }

    /*public ArrayList<AbstractWeaponDto> getWeaponList() {
        return weaponList;
    }

    public void setWeaponList(List<AbstractWeaponDto> weaponList) {
        this.weaponList = new ArrayList<AbstractWeaponDto>(weaponList) ;
    }*/
}
