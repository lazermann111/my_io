package com.github.myio.dto;

import com.github.czyzby.websocket.serialization.SerializationException;
import com.github.czyzby.websocket.serialization.Transferable;
import com.github.czyzby.websocket.serialization.impl.Deserializer;
import com.github.czyzby.websocket.serialization.impl.Serializer;

/**
 * Used for initial world init
 */

public class CapturablePointDto implements Transferable<CapturablePointDto>
{

    public String id;
    public int capturingProgress;
    public boolean captureInProgress;

    public String name;
    public float x,y;
    public int pointsToCapture;


    public int owner = -1; //id of owner team, -1 for neutral
    public int capturingTeam = -1; //id of owner team, if its not captured right now

    public CapturablePointDto(){}
    public CapturablePointDto(String id, int capturingProgress, boolean captureInProgress, String name, float x, float y, int pointsToCapture, int owner, int capturingTeam)
    {
        this.id = id;
        this.capturingProgress = capturingProgress;
        this.captureInProgress = captureInProgress;
        this.name = name;
        this.x = x;
        this.y = y;
        this.pointsToCapture = pointsToCapture;
        this.owner = owner;
        this.capturingTeam = capturingTeam;
    }

    @Override
    public void serialize(Serializer serializer) throws SerializationException
    {
        serializer
                .serializeString(id)
                .serializeInt(capturingProgress)
                .serializeBoolean(captureInProgress)
                .serializeString(name)
                .serializeFloat(x)
                .serializeFloat(y)
                .serializeInt(pointsToCapture)
                .serializeInt(owner)
                .serializeInt(capturingTeam);
    }

    @Override
    public CapturablePointDto deserialize(Deserializer deserializer) throws SerializationException
    {
        return new CapturablePointDto(deserializer.deserializeString(), deserializer.deserializeInt(), deserializer.deserializeBoolean(),
                deserializer.deserializeString(), deserializer.deserializeFloat(),deserializer.deserializeFloat(),
                deserializer.deserializeInt(), deserializer.deserializeInt(),deserializer.deserializeInt());
    }

    @Override
    public String toString()
    {
        return "CapturablePointDto{" +
                "id=" + id +
                ", capturingProgress=" + capturingProgress +
                ", captureInProgress=" + captureInProgress +
                ", name='" + name + '\'' +
                ", x=" + x +
                ", y=" + y +
                ", pointsToCapture=" + pointsToCapture +
                ", owner=" + owner +
                ", capturingTeam=" + capturingTeam +
                '}';
    }

    public String toStringBase()
    {
        return "CapturablePointDto{" +
                "id=" + id +
                ", capturingProgress=" + capturingProgress +
                ", captureInProgress=" + captureInProgress +
                ", owner=" + owner +
                ", capturingTeam=" + capturingTeam +
                '}';
    }

}
