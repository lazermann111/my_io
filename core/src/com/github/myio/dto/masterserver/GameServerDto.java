package com.github.myio.dto.masterserver;


import com.github.myio.enums.GameServerState;

public class GameServerDto   {
    private long id;

    private boolean active;
    private int playersNumber;
    private int maxPlayersNumber;
    private int worldId;
    private GameServerState serverState;


    public int getWorldId() {
        return worldId;
    }

    public void setWorldId(int worldId) {
        this.worldId = worldId;
    }

    public GameServerDto() {
    }

    public GameServerDto(boolean active, int playersNumber, int maxPlayersNumber, int worldId,  GameServerState s) {
        this.active = active;
        this.playersNumber = playersNumber;
        this.maxPlayersNumber = maxPlayersNumber;
        this.worldId = worldId;
        serverState = s;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public int getPlayersNumber() {
        return playersNumber;
    }

    public void setPlayersNumber(int playersNumber) {
        this.playersNumber = playersNumber;
    }

    public int getMaxPlayersNumber() {
        return maxPlayersNumber;
    }

    public void setMaxPlayersNumber(int maxPlayersNumber) {
        this.maxPlayersNumber = maxPlayersNumber;
    }

    public GameServerState getServerState() {
        return serverState;
    }

    public void setServerState(GameServerState serverState) {
        this.serverState = serverState;
    }
}


