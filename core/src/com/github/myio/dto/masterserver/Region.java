package com.github.myio.dto.masterserver;

public enum Region {
    EUROPE, ASIA, USA, LOCALHOST
}
