package com.github.myio.dto.masterserver;


public enum GameType {

    SOLO, DUO, SQUAD, TWO_TEAMS, CONQUEST
}
