package com.github.myio.dto.masterserver;

import com.github.czyzby.websocket.serialization.SerializationException;
import com.github.czyzby.websocket.serialization.Transferable;
import com.github.czyzby.websocket.serialization.impl.Deserializer;
import com.github.czyzby.websocket.serialization.impl.Serializer;



public class PlayerCellBaseDto implements Transferable<PlayerCellBaseDto> {

    public String id;
    public float x;
    public float y;
    public int  currentHealth, rotationAngle, currentArmor;

    public PlayerCellBaseDto(){}

    public PlayerCellBaseDto(String id, float x, float y, int currentHealth, int rotationAngle, int currentArmor) {
        this.id = id;
        this.x = x;
        this.y = y;
        this.currentHealth = currentHealth;
        this.rotationAngle = rotationAngle;
        this.currentArmor = currentArmor;
    }

    @Override
    public void serialize(Serializer serializer) throws SerializationException {
        serializer
                .serializeString(id)
                .serializeFloat(x)
                .serializeFloat(y)
                .serializeInt(currentHealth)
                .serializeInt(rotationAngle)
                .serializeInt(currentArmor);

    }

    @Override
    public PlayerCellBaseDto deserialize(Deserializer deserializer) throws SerializationException {
        return new PlayerCellBaseDto(deserializer.deserializeString(),
                deserializer.deserializeFloat(),
                deserializer.deserializeFloat(),
                deserializer.deserializeInt(),
                deserializer.deserializeInt(),
                deserializer.deserializeInt());
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }

    public int getCurrentHealth() {
        return currentHealth;
    }

    public void setCurrentHealth(int currentHealth) {
        this.currentHealth = currentHealth;
    }

    public int getRotationAngle() {
        return rotationAngle;
    }

    public void setRotationAngle(int rotationAngle) {
        this.rotationAngle = rotationAngle;
    }

    public int getCurrentArmor() {
        return currentArmor;
    }

    public void setCurrentArmor(int currentArmor) {
        this.currentArmor = currentArmor;
    }
}
