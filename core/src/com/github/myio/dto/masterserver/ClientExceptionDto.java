package com.github.myio.dto.masterserver;


public class ClientExceptionDto {

    public String errorMessage;
    public String methodName;
    public String timestamp;
    public String clientId;
    public String region;

    public ClientExceptionDto(){}

    public ClientExceptionDto(String errorMessage, String methodName, String timestamp, String clientId, String region) {
        this.errorMessage = errorMessage;
        this.methodName = methodName;
        this.timestamp = timestamp;
        this.region = region;
        this.clientId = clientId;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ClientExceptionDto dto = (ClientExceptionDto) o;

        if (errorMessage != null ? !errorMessage.equals(dto.errorMessage) : dto.errorMessage != null)
            return false;
        return methodName != null ? methodName.equals(dto.methodName) : dto.methodName == null;

    }

    @Override
    public int hashCode() {
        int result = errorMessage != null ? errorMessage.hashCode() : 0;
        result = 31 * result + (methodName != null ? methodName.hashCode() : 0);
        return result;
    }
}
