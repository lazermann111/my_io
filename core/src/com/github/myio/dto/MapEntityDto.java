package com.github.myio.dto;


import com.github.czyzby.websocket.serialization.SerializationException;
import com.github.czyzby.websocket.serialization.Transferable;
import com.github.czyzby.websocket.serialization.impl.Deserializer;
import com.github.czyzby.websocket.serialization.impl.Serializer;
import com.github.myio.enums.MapEntityType;



public class MapEntityDto extends CellDto implements Transferable<MapEntityDto>
{
    public String spritePath;
    public float collisionRadius;
    public int rotationAngle;
    public MapEntityType entityType;


    public MapEntityDto() {
    }

    public MapEntityDto(String id, float x, float y, String spritePath, int radius, float collisionRadius, MapEntityType entityType) {
        super(id,x,y,radius);
        this.spritePath=spritePath;
        this.radius=radius;
        this.collisionRadius = collisionRadius;
        this.entityType=entityType;
    }

    public MapEntityDto(String id, float x, float y, String spritePath, int radius, float collisionRadius,  int rotationAngle, MapEntityType entityType) {
        super(id, x, y, radius);
        this.spritePath=spritePath;
        this.radius=radius;
        this.collisionRadius = collisionRadius;
        this.entityType=entityType;
        this.rotationAngle = rotationAngle;
    }

    public float getCollisionRadius() {
        return collisionRadius;
    }

    public void setCollisionRadius(float collisionRadius) {
        this.collisionRadius = collisionRadius;
    }

    public int getRotationAngle() {
        return rotationAngle;
    }

    public void setRotationAngle(int rotationAngle) {
        this.rotationAngle = rotationAngle;
    }

    public String getSpritePath() {
        return spritePath;
    }

    public void setSpritePath(String spritePath) {
        this.spritePath = spritePath;
    }

    public int getRadiusItem() {
        return radius;
    }

    public void setRadiusItem(int radiusItem) {
        this.radius = radiusItem;
    }

    public MapEntityType getEntityType() {
        return entityType;
    }

    public void setEntityType(MapEntityType entityType) {
        this.entityType = entityType;
    }

    @Override
    public void serialize(Serializer serializer) throws SerializationException {
        serializer.serializeString(id)
                .serializeFloat(x)
                .serializeFloat(y)
                .serializeString(spritePath)
                .serializeInt(radius)
                .serializeFloat(collisionRadius)
                .serializeInt(rotationAngle)
                .serializeEnum(entityType);

    }

    @Override
    public MapEntityDto deserialize(Deserializer deserializer) throws SerializationException {
        return new MapEntityDto(
                deserializer.deserializeString(),
                deserializer.deserializeFloat(),
                deserializer.deserializeFloat(),

                deserializer.deserializeString(), //spritePath
                deserializer.deserializeInt(),   //radius
                deserializer.deserializeFloat(), // collisionRadius
                deserializer.deserializeInt(),//rotationAngle
                deserializer.deserializeEnum(MapEntityType.values()));

    }
}
