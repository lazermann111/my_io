package com.github.myio.dto;

import com.github.czyzby.websocket.serialization.SerializationException;
import com.github.czyzby.websocket.serialization.Transferable;
import com.github.czyzby.websocket.serialization.impl.Deserializer;
import com.github.czyzby.websocket.serialization.impl.Serializer;
import com.github.myio.enums.ItemType;



public class ItemDto extends CellDto implements Transferable<ItemDto> {

    public String spritePath;
    public int xpGiven;
    public int radiusItem;
    public int ammoAmount;
    public int clipAmmoAmount;
    public ItemType itemType;
    public String item_blueprint;

    public ItemDto() {
    }

    public ItemDto(String id, Float x, Float y, String spritePath, int radius, int xpGiven, int radiusItem, ItemType itemType, String item_blueprint, int ammoAmount,int clipAmmoAmount) {
        super(id,x,y, radius);
        this.spritePath =spritePath;
        this.xpGiven =xpGiven;
        this.radiusItem = radiusItem;
        this.itemType =itemType;
        this.item_blueprint =item_blueprint;
        this.ammoAmount =ammoAmount;
        this.clipAmmoAmount = clipAmmoAmount;
    }


    public void setItem_blueprint(String item_blueprint) {
        this.item_blueprint = item_blueprint;
    }

    public String getItem_blueprint() {
        return item_blueprint;
    }

    public String getSpritePath() {
        return spritePath;
    }

    public void setSpritePath(String spritePath) {
        this.spritePath = spritePath;
    }

    public int getXpGiven() {
        return xpGiven;
    }

    public void setXpGiven(int xpGiven) {
        this.xpGiven = xpGiven;
    }

    public int getRadiusItem() {
        return radiusItem;
    }

    public void setRadiusItem(int radiusItem) {
        this.radiusItem = radiusItem;
    }

    public ItemType getItemType() {
        return itemType;
    }

    public void setItemType(ItemType itemType) {
        this.itemType = itemType;
    }

    public int getAmmoAmount()
    {
        return ammoAmount;
    }

    public void setAmmoAmount(int ammoAmount)
    {
        this.ammoAmount = ammoAmount;
    }

    public int getClipAmmoAmount() {return clipAmmoAmount;}

    public void setClipAmmoAmount(int clipAmmoAmount) {this.clipAmmoAmount = clipAmmoAmount;}

    @Override
    public void serialize(Serializer serializer) throws SerializationException {
        serializer.serializeString(id).
                serializeFloat(x).
                serializeFloat(y).
                serializeInt(radius).
                serializeString(spritePath)
                .serializeInt(xpGiven)
                .serializeInt(radiusItem)
                .serializeEnum(itemType)
                .serializeString(item_blueprint)
                .serializeInt(ammoAmount)
                .serializeInt(clipAmmoAmount);
    }

    @Override
    public ItemDto deserialize(Deserializer deserializer) throws SerializationException {

        return new ItemDto( deserializer.deserializeString(),
                deserializer.deserializeFloat(),
                deserializer.deserializeFloat(),
                deserializer.deserializeString(),
                deserializer.deserializeInt(),
                deserializer.deserializeInt(),
                deserializer.deserializeInt(),
                deserializer.deserializeEnum(ItemType.values()),
                deserializer.deserializeString(),
                deserializer.deserializeInt() ,
                deserializer.deserializeInt()  );
    }
}
