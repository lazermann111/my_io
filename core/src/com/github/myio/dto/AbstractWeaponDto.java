package com.github.myio.dto;


import com.github.czyzby.websocket.serialization.SerializationException;
import com.github.czyzby.websocket.serialization.Transferable;
import com.github.czyzby.websocket.serialization.impl.Deserializer;
import com.github.czyzby.websocket.serialization.impl.Serializer;
import com.github.myio.enums.WeaponType;

public class AbstractWeaponDto implements Transferable<AbstractWeaponDto> {
    public int ammoAmount;
    public WeaponType weaponType;

    public AbstractWeaponDto() {
    }

    public AbstractWeaponDto(int ammoAmount, WeaponType weaponType) {
        this.ammoAmount = ammoAmount;
        this.weaponType = weaponType;
    }

    @Override
    public void serialize(Serializer serializer) throws SerializationException {
        serializer.serializeInt(ammoAmount).serializeEnum(weaponType);
    }

    @Override
    public AbstractWeaponDto deserialize(Deserializer deserializer) throws SerializationException {
        return new AbstractWeaponDto(deserializer.deserializeInt(),deserializer.deserializeEnum(WeaponType.values()));
    }
}
