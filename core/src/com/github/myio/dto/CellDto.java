package com.github.myio.dto;

/**
 * Created by Igor on 12/20/2017.
 */

public class CellDto  {

    public String id;
    public float x;
    public float y;
    public int radius;

    public CellDto() {
    }

    public CellDto(String id, float x, float y, int radius) {
        this.id = id;
        this.x = x;
        this.y = y;
        this.radius = radius;
    }



    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }

    public int getRadiusItem() {
        return radius;
    }

    public void setRadiusItem(int radiusItem) {
        this.radius = radiusItem;
    }
}
