package com.github.myio.dto;

import com.github.czyzby.websocket.serialization.SerializationException;
import com.github.czyzby.websocket.serialization.Transferable;
import com.github.czyzby.websocket.serialization.impl.Deserializer;
import com.github.czyzby.websocket.serialization.impl.Serializer;


public class PlayerSessionStatsDto implements Transferable<PlayerSessionStatsDto>
{
    String playerId;
    String clientId; // basically its unique player username

    public int dealtDamage;
    public int takenDamage;
    public int survivedFor;  //seconds
    public int killsNumber;


    private long  gameStartTime;
    public PlayerSessionStatsDto(){}

    public PlayerSessionStatsDto(String playerId, String clientId) {
        gameStartTime = System.currentTimeMillis();
        this.playerId = playerId;
        this.clientId = clientId;
    }


    public PlayerSessionStatsDto(String playerId, String clientId, int dealtDamage, int takenDamage, int survivedFor, int killsNumber) {

        this.playerId = playerId;
        this.clientId = clientId;
        this.dealtDamage = dealtDamage;
        this.takenDamage = takenDamage;
        this.survivedFor = survivedFor;
        this.killsNumber = killsNumber;
    }

    public String getPlayerId() {
        return playerId;
    }

    public void setPlayerId(String playerId) {
        this.playerId = playerId;
    }

    public long getGameStartTime() {
        return gameStartTime;
    }

    @Override
    public void serialize(Serializer serializer) throws SerializationException {
        serializer.serializeString(playerId)
                .serializeString(clientId)
                .serializeInt(dealtDamage)
                .serializeInt(takenDamage)
                .serializeInt(survivedFor)
                .serializeInt(killsNumber);
    }

    @Override
    public PlayerSessionStatsDto deserialize(Deserializer deserializer) throws SerializationException {
        return new PlayerSessionStatsDto(deserializer.deserializeString(), deserializer.deserializeString(), deserializer.deserializeInt(),
                deserializer.deserializeInt(), deserializer.deserializeInt(), deserializer.deserializeInt());
    }

    @Override
    public String toString() {
        return "PlayerSessionStatsDto{" +
                "playerId='" + playerId + '\'' +
                ", dealtDamage=" + dealtDamage +
                ", takenDamage=" + takenDamage +
                ", survivedFor=" + survivedFor +
                ", killsNumber=" + killsNumber +
                '}';
    }
}
