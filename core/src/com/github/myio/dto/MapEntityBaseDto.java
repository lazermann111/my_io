package com.github.myio.dto;

import com.github.czyzby.websocket.serialization.SerializationException;
import com.github.czyzby.websocket.serialization.Transferable;
import com.github.czyzby.websocket.serialization.impl.Deserializer;
import com.github.czyzby.websocket.serialization.impl.Serializer;



public class MapEntityBaseDto extends CellDto implements Transferable<MapEntityBaseDto>

{
    public int rotationAngle;


    public MapEntityBaseDto() {
    }

    public MapEntityBaseDto(String id, float x, float y, int radius) {
        super(id,x,y,radius);

        this.radius=radius;

    }

    public MapEntityBaseDto(String id, float x, float y, int radius,int rotationAngle) {
        super(id, x, y, radius);
        this.rotationAngle = rotationAngle;
    }



    public int getRotationAngle() {
        return rotationAngle;
    }

    public void setRotationAngle(int rotationAngle) {
        this.rotationAngle = rotationAngle;
    }



    public int getRadiusItem() {
        return radius;
    }

    public void setRadiusItem(int radiusItem) {
        this.radius = radiusItem;
    }



    @Override
    public void serialize(Serializer serializer) throws SerializationException {
        serializer
                .serializeString(id)
                .serializeFloat(x)
                .serializeFloat(y)

                .serializeInt(radius)
                .serializeInt(rotationAngle);


    }

    @Override
    public MapEntityBaseDto deserialize(Deserializer deserializer) throws SerializationException {
        return new MapEntityBaseDto(
                deserializer.deserializeString(),

                deserializer.deserializeFloat(),
                deserializer.deserializeFloat(),

                deserializer.deserializeInt(),
                deserializer.deserializeInt());

    }
}
