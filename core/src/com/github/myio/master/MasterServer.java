package com.github.myio.master;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Net;
import com.badlogic.gdx.net.HttpRequestBuilder;
import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.JsonWriter;
import com.github.czyzby.websocket.net.ExtendedNet;
import com.github.myio.GameClient;
import com.github.myio.dto.masterserver.ClientExceptionDto;
import com.github.myio.tools.IoLogger;

import java.util.HashSet;
import java.util.Set;

import static com.github.myio.GameConstants.MASTER_SERVER_URL;



public class MasterServer {


    // common urls
    private String getServerToPlayPath = "/server/getServer";
    private String getAllServersPath = "/server/getServersForAllRegions";
    private String postExceptionPath = "/user/postException";

   // team match urls

    private String createTeamPath = "/team/create";
    private String joinTeamPath = "/team/join";
    private String getTeamUpdatesPath = "/team/getUpdate";
    private String leaveTeamPath = "/team/leave";
    private String startTeamMatchPath = "/team/start";

    String TAG = "MasterServer";

    public static int DEFAULT_TIMEOUT = 1;

    private Set<ClientExceptionDto> exceptions = new HashSet<ClientExceptionDto>();


    Json json = new Json(JsonWriter.OutputType.json);

    public void postExceptionDetails(Exception e, String methodName)
    {
        json.setOutputType(JsonWriter.OutputType.json);
        ClientExceptionDto dto = new ClientExceptionDto
                (e.toString(), methodName, System.currentTimeMillis()+"",
                        GameClient.getGame().getClientId(), GameClient.getGame().getClientRegion());

        if(exceptions.contains(dto)) return;

        exceptions.add(dto);
        HttpRequestBuilder requestBuilder = new HttpRequestBuilder();
        Net.HttpRequest httpRequest = requestBuilder.newRequest()
                .method(Net.HttpMethods.POST)
                .header("Content-Type", "application/json")
                .url(MASTER_SERVER_URL+postExceptionPath)
                .content(json.toJson(dto))
                .build();
        Gdx.net.sendHttpRequest(httpRequest, new Net.HttpResponseListener() {
            @Override
            public void handleHttpResponse(Net.HttpResponse httpResponse) {
                IoLogger.log("Master server", "Handled: " + httpResponse.getStatus());
            }

            @Override
            public void failed(Throwable t) {

              Gdx.app.error("Master server", "Failed " + t);
            }

            @Override
            public void cancelled() {
                Gdx.app.error("Master server", "Cancelled ");
            }
        });
    }



    public RequestResult getGameServerInRegion(String region)
    {

        IoLogger.log(TAG, "getGameServerInRegion " + region);
       final RequestResult res = new RequestResult();

        HttpRequestBuilder requestBuilder = new HttpRequestBuilder();
        Net.HttpRequest httpRequest = requestBuilder.newRequest()
                .method(Net.HttpMethods.GET)
                .url(MASTER_SERVER_URL+getServerToPlayPath).content("regionToPlay="+region).build();
        Gdx.net.sendHttpRequest(httpRequest, new Net.HttpResponseListener() {
            @Override
            public void handleHttpResponse(Net.HttpResponse httpResponse) {
                IoLogger.log(TAG, "getGameServerInRegion handleHttpResponse" + httpResponse);
                res.successfullresponse = httpResponse;
                res.response = httpResponse.getResultAsString();
            }

            @Override
            public void failed(Throwable t) {
                IoLogger.log(TAG, "getGameServerInRegion failed" );
                res.failedResponse = t;
            }

            @Override
            public void cancelled() {
                IoLogger.log(TAG, "getGameServerInRegion cancelled" );
                res.cancelled = true;
            }
        });


        return res;
    }

    public RequestResult getAllGameServersInfo( )
    {
        final RequestResult res = new RequestResult();

        HttpRequestBuilder requestBuilder = new HttpRequestBuilder();
        Net.HttpRequest httpRequest = requestBuilder.newRequest()
                .method(Net.HttpMethods.GET)
                .url(MASTER_SERVER_URL+getAllServersPath).build();
        ExtendedNet.getNet().sendHttpRequest(httpRequest, new Net.HttpResponseListener() {
            @Override
            public void handleHttpResponse(Net.HttpResponse httpResponse) {
                res.successfullresponse = httpResponse;
                res.response = httpResponse.getResultAsString();
            }

            @Override
            public void failed(Throwable t) {

                res.failedResponse = t;
            }

            @Override
            public void cancelled() {

                res.cancelled = true;
            }
        });


        return res;
    }

    public RequestResult getTeamUpdateInfo( String teamId)
    {
        final RequestResult res = new RequestResult();

        HttpRequestBuilder requestBuilder = new HttpRequestBuilder();
        Net.HttpRequest httpRequest = requestBuilder.newRequest()
                .method(Net.HttpMethods.GET)
                .url(MASTER_SERVER_URL+getTeamUpdatesPath)
                .content("teamId="+teamId)
                .build();
        ExtendedNet.getNet().sendHttpRequest(httpRequest, new Net.HttpResponseListener() {
            @Override
            public void handleHttpResponse(Net.HttpResponse httpResponse) {
                res.successfullresponse = httpResponse;
                res.response = httpResponse.getResultAsString();
            }

            @Override
            public void failed(Throwable t) {

                res.failedResponse = t;
            }

            @Override
            public void cancelled() {

                res.cancelled = true;
            }
        });


        return res;
    }

    public RequestResult joinTeam( String newTeamMember, String teamId)
    {
        final RequestResult res = new RequestResult();

        HttpRequestBuilder requestBuilder = new HttpRequestBuilder();
        Net.HttpRequest httpRequest = requestBuilder.newRequest()
                .method(Net.HttpMethods.GET)
                .url(MASTER_SERVER_URL+joinTeamPath)
                .content("newTeamMember="+newTeamMember+"&teamId="+teamId)
               // .content("teamId="+teamId)
                .build();
        ExtendedNet.getNet().sendHttpRequest(httpRequest, new Net.HttpResponseListener() {
            @Override
            public void handleHttpResponse(Net.HttpResponse httpResponse) {
                res.successfullresponse = httpResponse;
                res.response = httpResponse.getResultAsString();
            }

            @Override
            public void failed(Throwable t) {

                res.failedResponse = t;
            }

            @Override
            public void cancelled() {

                res.cancelled = true;
            }
        });


        return res;
    }
    public RequestResult leaveTeam( String leftTeamMember, String teamId)
    {
        final RequestResult res = new RequestResult();

        HttpRequestBuilder requestBuilder = new HttpRequestBuilder();
        Net.HttpRequest httpRequest = requestBuilder.newRequest()
                .method(Net.HttpMethods.GET)
                .url(MASTER_SERVER_URL+leaveTeamPath)
                .content("leftTeamMember="+leftTeamMember+"&teamId="+teamId)
               // .content("teamId="+teamId)
                .build();
        ExtendedNet.getNet().sendHttpRequest(httpRequest, new Net.HttpResponseListener() {
            @Override
            public void handleHttpResponse(Net.HttpResponse httpResponse) {
                res.successfullresponse = httpResponse;
                res.response = httpResponse.getResultAsString();
            }

            @Override
            public void failed(Throwable t) {

                res.failedResponse = t;
            }

            @Override
            public void cancelled() {

                res.cancelled = true;
            }
        });


        return res;
    }

    public RequestResult createTeam( String leaderName)
    {

        Gdx.app.debug(TAG, "createTeam " );
        final RequestResult res = new RequestResult();

        HttpRequestBuilder requestBuilder = new HttpRequestBuilder();
        Net.HttpRequest httpRequest = requestBuilder.newRequest()
                .method(Net.HttpMethods.GET)
                .url(MASTER_SERVER_URL+createTeamPath)
                .content("leaderName="+leaderName)
                .build();
        ExtendedNet.getNet().sendHttpRequest(httpRequest, new Net.HttpResponseListener() {
            @Override
            public void handleHttpResponse(Net.HttpResponse httpResponse) {
                Gdx.app.debug(TAG, "createTeam handleHttpResponse " + httpResponse.getStatus() );
                res.successfullresponse = httpResponse;
                res.response = httpResponse.getResultAsString();
            }

            @Override
            public void failed(Throwable t) {
                Gdx.app.debug(TAG, "createTeam failed" );
                res.failedResponse = t;
            }

            @Override
            public void cancelled() {
                Gdx.app.debug(TAG, "createTeam failed" );
                res.cancelled = true;
            }
        });


        return res;
    }

    //teamId=1&region=USA&gameType=SOLO&autofill=true
    public RequestResult startTeamMatch(final String region, String gameType, boolean autofill, String teamId)
    {
        final RequestResult res = new RequestResult();

        HttpRequestBuilder requestBuilder = new HttpRequestBuilder();
        Net.HttpRequest httpRequest = requestBuilder.newRequest()
                .method(Net.HttpMethods.GET)
                .url(MASTER_SERVER_URL+startTeamMatchPath)
                .content("region="+region+"&gameType="+gameType+"&autofill="+autofill+"&teamId="+teamId)
               // .content("gameType="+gameType)
             //   .content("autofill="+autofill)
               // .content("teamId="+teamId)
                .build();
        ExtendedNet.getNet().sendHttpRequest(httpRequest, new Net.HttpResponseListener() {
            @Override
            public void handleHttpResponse(Net.HttpResponse httpResponse) {
                Gdx.app.debug(TAG, "startTeamMatch handleHttpResponse" + res);
                res.successfullresponse = httpResponse;
                res.response = httpResponse.getResultAsString();
            }

            @Override
            public void failed(Throwable t) {
                Gdx.app.debug(TAG, " startTeamMatch failed " );
                res.failedResponse = t;
            }

            @Override
            public void cancelled() {
                Gdx.app.debug(TAG, "startTeamMatch cancelled" );
                res.cancelled = true;
            }
        });


        return res;
    }


    public class RequestResult
    {
        public Net.HttpResponse successfullresponse;
        public String response;
        public Throwable failedResponse;
        public boolean cancelled;



        public boolean isFailed()
        {
            return failedResponse != null;
        }
    }



}
