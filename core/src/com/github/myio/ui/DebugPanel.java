package com.github.myio.ui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.github.myio.EntityManager;
import com.github.myio.GameClient;
import com.github.myio.entities.PlayerCell;
import com.github.myio.entities.item.ItemCell;
import com.github.myio.entities.map.MapEntityCell;
import com.github.myio.tools.IoLogger;

import java.util.HashMap;
import java.util.Map;


public class DebugPanel implements IDrawable {
    //Label labelPlayer;
    //Label labelMap;
    Label labelFood;
    Label labelEnemies;
    Label labelTiles;
    Label labelEntities;
    Label labelRadiusPlus;
    Label labelRadiusMinus;

    EntityManager entityManager;
    private Map<String, PlayerCell> enemyCellsTmp;
    private Map<String, ItemCell> foodsTmp;
    private Map<String, MapEntityCell> mapEntitiesTmp;
    private Map<String, PlayerCell> enemyCells;
    private Map<String, ItemCell> foods;
    private Map<String, MapEntityCell> mapEntities;


   // Label labelXP;
    Label labelHP;
    Label labelAR;
    Label labelFPS;
    BitmapFont font;
    PlayerCell localPlayer;
    GameClient game;
   // OrthographicCamera camera;
    Stage stage;
    Viewport viewport;
    Table table;
    Actor btXP;
    Skin skin;


    public DebugPanel(GameClient game, PlayerCell localPlayer, OrthographicCamera camera, EntityManager entityManager, Skin skin) {


        this.font = GameClient.getFont32Black();
        this.skin = skin;
        this.game = game;
        this.localPlayer = localPlayer;
    //    this.camera = camera;

        viewport = game.getUiViewport();
        stage = new Stage(viewport, game.getBatch());
        Gdx.input.setInputProcessor(stage);
        table = new Table();
        table.top();
        table.setFillParent(true);

//        Label.LabelStyle labelStyle = new Label.LabelStyle();
//        labelStyle.font = font;


        labelEnemies = new Label("6 Enemies : ", skin);
        labelFood = new Label("7 Food : ", skin);
        //labelTiles = new Label("8 Tiles : " + localPlayer.getCurrentScore(), labelStyle);
        labelEntities = new Label("8 Entities : " , skin);

        labelRadiusPlus = new Label("9 Radius Plus : ", skin);
        labelRadiusMinus = new Label("0 Radius Minus : ", skin);
        //labelXP = new Label("XP : " + localPlayer.getScore(), skin);
        labelHP = new Label("HP: " + localPlayer.getCurrentHealth(), skin);
        labelFPS = new Label("FPS: ", skin);
        labelAR = new Label("ARMOR: ",skin);


        /*
        TextButton.TextButtonStyle textButtonStyle = new TextButton.TextButtonStyle();
        textButtonStyle.font = font;
        btXP = new TextButton("XP  Buuton:"+localPlayer.getCurrentScore(),textButtonStyle);
        btXP.setWidth(50);
        btXP.setHeight(50);
        btXP.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y)  {
                IoLogger.log("\n\n\n\nCLICK BTXP-----------+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n\n\n\n");

            }
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button)
            {
                IoLogger.log("\n\n\n\nTouchDown BTXP-----------+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n\n\n\n");
                return true;
            }
            @Override
            public void  	touchDragged(InputEvent event, float x, float y, int pointer)
            {
                IoLogger.log("\n\n\n\nTOUCHDRAGGERBTXP-----------+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n\n\n\n");

            }
        });
*/


        table.add(labelEnemies).expandX().padTop(10).align(Align.right).padRight(10);
        table.row();
        table.add(labelFood).expandX().padTop(10).align(Align.right).padRight(10);
        table.row();
        table.add(labelEntities).expandX().padTop(10).align(Align.right).padRight(10);
        table.row();
        table.add(labelRadiusPlus).expandX().padTop(10).align(Align.right).padRight(10);
        table.row();
        table.add(labelRadiusMinus).expandX().padTop(10).align(Align.right).padRight(10);
        table.row();

       // table.add(labelXP).expandX().padTop(10).align(Align.right).padRight(10);
        table.row();
        table.add(labelHP).expandX().padTop(10).align(Align.right).padRight(10);
        table.row();
        table.add(labelAR).expandX().padTop(10).align(Align.right).padRight(10);
        table.row();
        table.add(labelFPS).expandX().padTop(10).align(Align.right).padRight(10);
        stage.addActor(table);




        this.enemyCellsTmp=  new HashMap<String, PlayerCell>();
        this.foodsTmp=  new HashMap<String, ItemCell>();
        this.mapEntitiesTmp= new HashMap<String, MapEntityCell>();
        this.entityManager = entityManager;
        this.enemyCells=entityManager.getEnemyCells();
        this.foods=entityManager.getItems();
        this.mapEntities=entityManager.getEntities();
    }

    @Override
    public void draw(float delta) {


        userInput();


        //пока оставил размер свойств, но это лучше изменить
        labelEnemies.setText("6 Interpolation -  " + PlayerCell.interpolation_const);
        labelFood.setText("7 Interpolation + : " + PlayerCell.interpolation_const);
        labelEntities.setText("8 Entities : " + entityManager.getEntities().size());
        labelRadiusPlus.setText("9 Radius Plus : "+entityManager.getRadiusPlayerSight());
        labelRadiusMinus.setText("0 Radius Minus : ");

       // labelXP.setText("XP : " + localPlayer.getScore());
        labelHP.setText("HP: " + localPlayer.getCurrentHealth());
        labelAR.setText("ARMOR: " + localPlayer.getCurrentArmor());
        labelFPS.setText("FPS: " + (int) (1 / delta));
      //  game.getBatch().setProjectionMatrix(camera.combined);
        stage.act();
        stage.draw();
       // Gdx.input.setInputProcessor(stage);

    }

    private void userInput() {

        //      if(Gdx.input.isTouched())
        //        IoLogger.log("\n\n\n\n-----------++++++++++++++++++++++++++++++++++++++++++++++++++++++++++USER INPUT+\n\n\n\n");
        if (Gdx.input.isKeyJustPressed(Input.Keys.NUM_6)) PlayerCell.interpolation_const -=0.1f;
        if (Gdx.input.isKeyJustPressed(Input.Keys.NUM_7)) PlayerCell.interpolation_const +=0.1f;
        if (Gdx.input.isKeyJustPressed(Input.Keys.NUM_8)) changeListEntities();
        if (Gdx.input.isKeyJustPressed(Input.Keys.NUM_9)) increaseRadius();
        if (Gdx.input.isKeyJustPressed(Input.Keys.NUM_0)) reduceRadius();

    }


    private boolean addedTiles = true, addedEnemies = true, addedFoods = true, addedEntities = true;


    public void changeListEnemies() {

        if (addedEnemies) {
            enemyCellsTmp.putAll(enemyCells);
            enemyCells.clear();

            //IoLogger.log("-----------CLEAR                           ENEMIES-----");
        }
        else
            {
                enemyCells.putAll(enemyCellsTmp);
                //entityManager.getEnemyCells().putAll(enemyCellsTmp);
                enemyCellsTmp.clear();
                //IoLogger.log("-----------Add                           ENEMIES-----");
            }


        addedEnemies = !addedEnemies;

    }

    public void changeListFood() {
        if (addedFoods) {
            foodsTmp.putAll(foods);
            foods.clear();
            //IoLogger.log("-----------CLEAR                           FOODS-----");
        }
        else
        {
          foods.putAll(foodsTmp);
          foodsTmp.clear();
            //IoLogger.log("-----------Add                           FOODS-----");
        }

        addedFoods = !addedFoods;
        //IoLogger.log("-----------Food-----");
    }


//    public void changeListTiles() {
//        addedTiles = !addedTiles;
//        IoLogger.log("-----------TILES-----");
//    }

    public void changeListEntities() {
        if (addedEntities) {
             mapEntitiesTmp.putAll(mapEntities);
            mapEntities.clear();
            //IoLogger.log("-----------CLEAR                           Entities-----");
        }
        else
        {
            mapEntities.putAll(mapEntitiesTmp);
            mapEntitiesTmp.clear();
            //IoLogger.log("-----------Add                          Entities-----");
        }

        addedEntities = !addedEntities;
       // IoLogger.log("-----------Change                           Entities-----");
    }



    public void increaseRadius() {

        entityManager.radiusPlus();
        IoLogger.log("-----------increaseRadius-----");
    }

    public void reduceRadius() {
        entityManager.radiusMinus();
        IoLogger.log("-----------reduceRadius-----");
    }
}
