package com.github.myio.ui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.github.myio.GameClient;
import com.github.myio.StringConstants;

import java.util.ArrayList;
import java.util.List;

import static com.github.myio.StringConstants.PREFERENCES_SKIN;
import static com.github.myio.StringConstants.SKIN_DEFAULT;


/**
 * Created by taras on 29.03.2018.
 */

public class SkinManager implements IDrawable {


    private Stage mStage;
    private Table table;
    private TextButton btnCloseWindow;
     private String checkedSkinName;
    Table menu;
    Table rootTable;
    Viewport viewport;


    private String[] skinNames = {StringConstants.SKIN_YELLOW_DEFAULT,
            StringConstants.SKIN_BLACK_DEFAULT,
            StringConstants.SKIN_BROWN_DEFAULT,
            StringConstants.SKIN_DARKBLUE_DEFAULT,
            StringConstants.SKIN_GREY_DEFAULT,
            StringConstants.SKIN_KHAKI_DEFAULT,
            StringConstants.SKIN_ORANGE_DEFAULT,
            StringConstants.SKIN_RED_DEFAULT};

    private Skin skin;
    private GameClient game;
    public Image image;


    public ImageButton defaultImage;
    private List<ImageButton> listImageButton;


    public SkinManager(final Skin skin, GameClient game) {
        this.skin = skin;
        this.game = game;
        listImageButton = new ArrayList<ImageButton>();

        btnCloseWindow = new TextButton("OK",skin);
        btnCloseWindow.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {closeWindow();}});

//        rootTable = new Table();
//        rootTable.setFillParent(true);
        table = new Table(skin);


        Sprite bckMiddleSp = new Sprite(new Texture(Gdx.files.internal("mainMenu/background.png")));
        Image bckMiddle = new Image(bckMiddleSp);
        table.setBackground(bckMiddle.getDrawable());
       // table.setPosition(Gdx.graphics.getWidth(),Gdx.graphics.getHeight());
        table.setFillParent(true);
     //  table.center();
    //    table.debugAll();
        if (!game.getGamePreferences().getString(PREFERENCES_SKIN,"").equals(""))
        {
            defaultImage = createSkin(game.getGamePreferences().getString(PREFERENCES_SKIN));
        }else defaultImage = createSkin(SKIN_DEFAULT);

        table.add(defaultImage).colspan(3).center();

        table.row();
        addButtons(skinNames);
        table.row();
        table.add(btnCloseWindow).colspan(3).width(Gdx.graphics.getWidth()/6).height(Gdx.graphics.getHeight()/11);
        table.row();
       // table.add(btnCloseWindow).colspan(3).center().width(100);
        initImBt();
//        rootTable.add(table);
    }


    @Override
    public void  draw(float delta) {
        mStage.act();
        mStage.draw();

    }

    private ImageButton createSkin(String nameNormal){
        Sprite textureTransparent = GameClient.uiAtlas.createSprite(nameNormal);
        Sprite textureNormal = GameClient.uiAtlas.createSprite(nameNormal);
        ImageButton imageButton = new ImageButton(new Image(textureTransparent).getDrawable(), new Image(textureNormal).getDrawable());
        Button.ButtonStyle style = imageButton.getStyle();
        style.over = new Image(textureNormal).getDrawable();
        style.checkedOver = new Image(textureNormal).getDrawable();
        imageButton.setStyle(style);
        imageButton.setName(nameNormal);
        return imageButton;
    }

    private void addButtons(String[] skinNames) {
        for (int i = 0; i < skinNames.length; i++) {
            String n = skinNames[i];
            ImageButton imgBtn = createSkin(n);
            if((i+1)%3 == 0) {
                table.add(imgBtn).row();
            }else {
                table.add(imgBtn);
            }
            listImageButton.add(imgBtn);
        }
    }

    private void initImBt() {
        for (final ImageButton i : listImageButton) {
            i.addListener(new ClickListener(){
                @Override
                public void clicked(InputEvent event, float x, float y) {
                    ImageButton ib = createSkin(i.getName());
                    table.getCell(defaultImage).setActor(ib);
                    defaultImage = ib;
                    game.getGamePreferences().putString(PREFERENCES_SKIN,i.getName());
                    game.getGamePreferences().flush();
                }
            });
        }
    }

    public String getStringNameSkin (){return defaultImage.getName();}
    public void showWindow () {
        mStage.addActor(table);
        menu.setTouchable(Touchable.disabled);
    }

    public void closeWindow() {
       // table.remove();
        table.remove();
        menu.setTouchable(Touchable.enabled);
    }
    public void setmStage(Stage mStage) {this.mStage = mStage;}
    public void setMenu(Table mainMenu) {
        menu = mainMenu;
    }


}
