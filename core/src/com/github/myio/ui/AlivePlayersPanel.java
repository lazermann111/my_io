package com.github.myio.ui;


import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.github.myio.EntityManager;
import com.github.myio.GameClient;
import com.github.myio.GameConstants;
import com.github.myio.entities.PlayerCell;

import java.util.Map;

public class AlivePlayersPanel implements IDrawable {
    final  static int WIDTH=400;
    final static int HEIGTH=100;
    final static int POS_X=10;
    final static int POS_Y=10;

    int width,height,posX,posY;
    private Map<String, PlayerCell> allPlayers;

    Viewport viewport;
    GameClient game;
    Stage stage;
    Label numberAlive;
    Skin skin;
    EntityManager entityManager;
    private long lastUpdate;


    public AlivePlayersPanel(GameClient game, OrthographicCamera camera, Map<String,PlayerCell> list,EntityManager entityManager, Skin skin) {

        this.width= AlivePlayersPanel.WIDTH;
        this.height= AlivePlayersPanel.HEIGTH;
        this.posX= AlivePlayersPanel.POS_X;
        this.posY= AlivePlayersPanel.POS_Y;
        this.entityManager = entityManager;
        this.allPlayers = list;
        this.skin=skin;

        viewport = game.getUiViewport();
        this.stage= new Stage(viewport,game.getBatch());

        Table table = new Table();

        //float posX = Gdx.graphics.getWidth() * 5/100;
        //float posY = Gdx.graphics.getHeight() * 95/100;
        //table.setPosition(posX,posY);
        table.setBackground(new Image(GameClient.uiAtlas.createSprite("alive_background")).getDrawable());
        table.pack();

        Label.LabelStyle style = new Label.LabelStyle(GameClient.getFont32White(), Color.WHITE);
        numberAlive = new Label(String.valueOf(allPlayers.size()), style);
        numberAlive.setFontScale(2);

        Table root = new Table();
        root.setFillParent(true);
        root.add(table);
        root.right().top().padRight(10).padTop(10);
        root.setSize(150, 150);

        //root.debug();
        //table.debug();


        //table.defaults().width(60);
        table.add(numberAlive).pad(5, 10, 5, 10).center().row();
        table.add(new Label("alive", skin)).padBottom(3).center().row();

        stage.addActor(root);
    }


    @Override
    public void draw(float delta) {

        stage.act();
        stage.draw();
        if (lastUpdate + GameConstants.UPDATE_ALIVEPLAYER_RATE < System.currentTimeMillis()) {
            updateAlivePlayers();
            lastUpdate = System.currentTimeMillis();
        }

    }

    public void updateAlivePlayers (){
        allPlayers = entityManager.getAllPlayers();
        numberAlive.setText(String.valueOf(allPlayers.size()));


    }


}
