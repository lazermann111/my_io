package com.github.myio.ui;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.ProgressBar;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.github.myio.GameClient;
import com.github.myio.entities.PlayerCell;

import static com.github.myio.GameConstants.PLAYERS_MIN_ARMOR;


public class ArmorBar implements IDrawable {

    Stage stage;
    Armor armor;
    int maxArmor;
    GameClient game;
    PlayerCell localPlayer;

    private boolean showArmorBar = true;

    public ArmorBar(int maxArmor) {
        this.maxArmor = maxArmor;

        int maxWidth =(int) (maxArmor *3.8);

        stage= new Stage();
        armor = new Armor(maxWidth,15);
        armor.setPosition(10,40);
        //armor.debug();

        //stage.addActor(armor);

    }


    @Override
    public void draw(float delta) {


        if (localPlayer.getCurrentArmor() > PLAYERS_MIN_ARMOR )
        {
            stage.addActor(armor);
            stage.draw();
            stage.act();

        }
        if(localPlayer.getCurrentArmor() <= PLAYERS_MIN_ARMOR )
        {
            stage.clear();
        }

    }

    public void setLocalPlayer(PlayerCell localPlayer) {
        this.localPlayer = localPlayer;
    }

    public class Armor extends ProgressBar {
        public Armor(int width, int height) {
            super(0f, 1f, 0.01f, false, new ProgressBarStyle());
            getStyle().background = getColoredDrawable(width, height, Color.LIGHT_GRAY);
            getStyle().knob = getColoredDrawable(0, height, Color.GRAY);
            getStyle().knobBefore = getColoredDrawable(width, height, Color.GRAY);
            setWidth(width);
            setHeight(height);
            setAnimateDuration(0.0f);
            setValue(1f);
            setAnimateDuration(0.25f);
        }

        public Drawable getColoredDrawable(int width, int height, Color color) {
            int mWidth = MathUtils.nextPowerOfTwo(width);
            int mHeight = MathUtils.nextPowerOfTwo(height);
            Pixmap pixmap = new Pixmap(mWidth, mHeight, Pixmap.Format.RGBA8888);
            pixmap.setColor(color);
            pixmap.fill();
            Texture texture = new Texture(pixmap);
            TextureRegionDrawable drawable = new TextureRegionDrawable(new TextureRegion(texture));
            pixmap.dispose();
            return drawable;
        }
    }
    public void setArmor (float part) {
        armor.setValue(part);
    }
}
