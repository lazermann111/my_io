package com.github.myio.ui;


import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.github.myio.GameClient;
import com.github.myio.entities.PlayerCell;
import com.github.myio.entities.weapons.Knife;

public class AmmoPanel implements IDrawable {

    private Label ammoLabel;
    private GameClient game;
    private PlayerCell localPlayer;
    private Stage stage;
    private Viewport viewport;
    private BitmapFont font;
    Skin skin;


    public AmmoPanel(GameClient game, PlayerCell playerCell, Skin skin) {
        this.game = game;
        this.skin=skin;
        viewport = game.getUiViewport();
        stage = new Stage(viewport, game.getBatch());
        localPlayer = playerCell;

        Table rootTable = new Table();
        rootTable.setFillParent( true );
        Table table = new Table();

        BitmapFont font = game.getFont32White();
        Label.LabelStyle style = new Label.LabelStyle(font, Color.WHITE);
        font.getData().setScale(0.8f, 0.8f);


        ammoLabel = new Label(""  , style);
        table.add(ammoLabel);
        table.row();
        rootTable.add(table).expandY().bottom();
        rootTable.row();
        stage.addActor(rootTable);

    }

    @Override
    public void draw(float delta) {



        try {
            if ((localPlayer.getCurrentWeapon() instanceof Knife))
                ammoLabel.setText("");
            else if (localPlayer.getCurrentWeapon() != null)
                //ammoLabel.setText("Ammo: " + localPlayer.getCurrentWeapon().clipAmmoAmount + "/" + (localPlayer.getCurrentWeapon().totalAmmoAmount - localPlayer.getCurrentWeapon().clipAmmoAmount));

                ammoLabel.setText("Ammo: " + localPlayer.getCurrentWeapon().clipAmmoAmount);
        }
        catch (Exception e) {
            e.printStackTrace();
        }

        stage.act();
        stage.draw();
    }
}
