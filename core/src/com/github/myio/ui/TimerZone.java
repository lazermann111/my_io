package com.github.myio.ui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.TimeUtils;
import com.badlogic.gdx.utils.Timer;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.github.myio.GameClient;
import com.github.myio.GameConstants;
import com.github.myio.entities.PlayerCell;
import com.github.myio.entities.map.ZoneMapManager;
import com.github.myio.enums.MapEntityType;
import com.github.myio.networking.packet.TimerGamePacket;

import java.util.concurrent.TimeUnit;

/**
 * Created by taras on 23.03.2018.
 */

public class TimerZone implements IDrawable {


    private GameClient game;

    private Label.LabelStyle labelStyle;
    private Viewport viewport;
    private ZoneMapManager zoneMapManager;
    private PlayerCell localPlayer;

    private Stage stage;
    private Table table;
    private BitmapFont font;

    private Label time;
    private Label zoneGame;
    private Label zoneGamePlayer;

    private int timerGame; // expose time in game

    private int zoneGreenDuration;
    private int zoneYellowDuration;
    private int zoneRedDuration;


    private long timeUpdate;
    private long labelUpdate;
    Skin skin;

    private ZonesCountdown zonesCountdown;
    private int serverTime;
    ShapeRenderer shapeRenderer;


    public TimerZone(GameClient game, ZoneMapManager zoneMapManager, Skin skin, PlayerCell localPlayer) {
        this.game = game;
        this.skin = skin;
        this.zoneMapManager = zoneMapManager;
        this.localPlayer = localPlayer;
         shapeRenderer = new ShapeRenderer();

        viewport = game.getUiViewport();
        stage = new Stage(viewport, game.getBatch());
        Table rootTable = new Table();
        rootTable.setFillParent(true);
        table = new Table();
        table.setSize(100, 200);


        time = new Label("TIMER 0:00 ", skin);
        zoneGame = new Label("WAITING TO START GAME", skin);
        zoneGame.setColor(Color.GREEN);
        zoneGamePlayer = new Label("You ", skin);

        TimerGamePacket timerGamePacket = new TimerGamePacket();
        timerGamePacket.timer = serverTime;
        Viewport viewport = game.getUiViewport();
        stage = new Stage(viewport);

        zonesCountdown = new ZonesCountdown(300, 15, GameConstants.ZONE_GREEN_DURATION + GameConstants.ZONE_YELLOW_DURATION + GameConstants.ZONE_RED_DURATION);

        table.add(time).center();
        table.row();
        table.add(zoneGamePlayer).left();
        table.row();
        table.add(zoneGame).left();
        table.row();
        table.add(zonesCountdown);

        rootTable.add(table).left().top().expand();
        stage.addActor(rootTable);

    }

    @Override
    public void draw(float delta) {

        if (timeUpdate + GameConstants.TIMER_RATE < System.currentTimeMillis() && timerGame >= 1) {
            timerGame++;
            int minutes = timerGame / 60;
            int seconds = timerGame % 60;
            time.setText("TIMER " + minutes + ":" + seconds);

            //CountdownBar
            zonesCountdown.setValue(timerGame);

            if (timerGame == 0) {
                zonesCountdown.setColors(Color.FOREST, Color.DARK_GRAY);
            }
            if (timerGame == 1) {
                zonesCountdown.setColors(Color.GRAY, Color.DARK_GRAY);
            }
            if (timerGame == 2) {
                zonesCountdown.setColors(Color.FOREST, Color.DARK_GRAY);
            }
            if (timerGame == 3) {
                zonesCountdown.setColors(Color.GRAY, Color.DARK_GRAY);
            }
            if (timerGame >= 4) {
                zonesCountdown.setColors(Color.FOREST, Color.DARK_GRAY);
            }

            if (timerGame == GameConstants.ZONE_GREEN_DURATION) {
                zonesCountdown.setColors(Color.YELLOW, Color.DARK_GRAY);
            }
            if (timerGame == GameConstants.ZONE_GREEN_DURATION + 1) {
                zonesCountdown.setColors(Color.GREEN, Color.DARK_GRAY);
            }
            if (timerGame == GameConstants.ZONE_GREEN_DURATION + 2) {
                zonesCountdown.setColors(Color.YELLOW, Color.DARK_GRAY);
            }
            if (timerGame == GameConstants.ZONE_GREEN_DURATION + 3) {
                zonesCountdown.setColors(Color.GREEN, Color.DARK_GRAY);
            }
            if (timerGame >= GameConstants.ZONE_GREEN_DURATION + 4) {
                zonesCountdown.setColors(Color.YELLOW, Color.DARK_GRAY);
            }

            if (timerGame == GameConstants.ZONE_YELLOW_DURATION + GameConstants.ZONE_GREEN_DURATION) {
                zonesCountdown.setColors(Color.RED, Color.DARK_GRAY);
            }
            if (timerGame == GameConstants.ZONE_YELLOW_DURATION + GameConstants.ZONE_GREEN_DURATION + 1) {
                zonesCountdown.setColors(Color.YELLOW, Color.DARK_GRAY);
            }
            if (timerGame == GameConstants.ZONE_YELLOW_DURATION + GameConstants.ZONE_GREEN_DURATION + 2) {
                zonesCountdown.setColors(Color.RED, Color.DARK_GRAY);
            }
            if (timerGame == GameConstants.ZONE_YELLOW_DURATION + GameConstants.ZONE_GREEN_DURATION + 3) {
                zonesCountdown.setColors(Color.YELLOW, Color.DARK_GRAY);
            }
            if (timerGame >= GameConstants.ZONE_YELLOW_DURATION + GameConstants.ZONE_GREEN_DURATION + 4) {
                zonesCountdown.setColors(Color.RED, Color.DARK_GRAY);
            }


            if (timerGame == 2) {
                zoneGame.setText("PROCEED TO NEXT ZONE IN");
                zoneGame.setColor(Color.GREEN);
            } else if (timerGame == zoneGreenDuration) {
                zoneGame.setText("PROCEED TO NEXT ZONE IN");
                zoneGame.setColor(Color.YELLOW);
            } else if (timerGame == zoneGreenDuration + zoneYellowDuration) {
                zoneGame.setText("PROCEED TO NEXT ZONE IN");
                zoneGame.setColor(Color.RED);
            }

            timeUpdate = System.currentTimeMillis();
        }
        if (labelUpdate + GameConstants.TIMER_RATE < System.currentTimeMillis()) {
            if (game.getZoneMapManager().getPlayerZone(localPlayer.getPosition()).equals(MapEntityType.RED_ZONE)) {
                zoneGamePlayer.setText("  YOU ARE IN THE RED ZONE ");
                zoneGamePlayer.setColor(Color.RED);
            } else if (game.getZoneMapManager().getPlayerZone(localPlayer.getPosition()).equals(MapEntityType.YELLOW_ZONE)) {
                zoneGamePlayer.setText("YOU ARE IN THE YELLOW ZONE");
                zoneGamePlayer.setColor(Color.YELLOW);
            } else {
                zoneGamePlayer.setText("YOU ARE IN THE GREEN ZONE");
                zoneGamePlayer.setColor(Color.GREEN);
            }
            labelUpdate = System.currentTimeMillis();
        }

        stage.draw();
        stage.act(delta);

        if (timerGame >= GameConstants.ZONE_GREEN_DURATION - 9 && game.getZoneMapManager().getPlayerZone(localPlayer.getPosition()).equals(MapEntityType.GREEN_ZONE)){
            if(!game.getZoneMapManager().getPlayerZone(localPlayer.getPosition()).equals(MapEntityType.GREEN_ZONE)) {
                shapeRenderer.dispose();
                Gdx.gl.glDisable(GL20.GL_BLEND);
                return;
            }
            Gdx.gl.glEnable(GL20.GL_BLEND);
            Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
             if(timerGame == GameConstants.ZONE_GREEN_DURATION - 9){
                shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
                shapeRenderer.setColor(new Color(1, 0, 0, 0.1f));
                shapeRenderer.rect(0, 0, stage.getWidth(), stage.getHeight());
                shapeRenderer.end();
            }if(timerGame == GameConstants.ZONE_GREEN_DURATION - 8){
                shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
                shapeRenderer.setColor(new Color(1, 0, 0, 0.1f));
                shapeRenderer.rect(0, 0, stage.getWidth(), stage.getHeight());
                shapeRenderer.end();
            }if(timerGame == GameConstants.ZONE_GREEN_DURATION - 7){
                shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
                shapeRenderer.setColor(new Color(1, 0, 0, 0.1f));
                shapeRenderer.rect(0, 0, stage.getWidth(), stage.getHeight());
                shapeRenderer.end();
            }if(timerGame == GameConstants.ZONE_GREEN_DURATION - 6){
                shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
                shapeRenderer.setColor(new Color(1, 0, 0, 0.1f));
                shapeRenderer.rect(0, 0, stage.getWidth(), stage.getHeight());
                shapeRenderer.end();
            }if(timerGame == GameConstants.ZONE_GREEN_DURATION - 5){
                shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
                shapeRenderer.setColor(new Color(1, 0, 0, 0.1f));
                shapeRenderer.rect(0, 0, stage.getWidth(), stage.getHeight());
                shapeRenderer.end();
            }if(timerGame == GameConstants.ZONE_GREEN_DURATION - 4){
                shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
                shapeRenderer.setColor(new Color(1, 0, 0, 0.2f));
                shapeRenderer.rect(0, 0, stage.getWidth(), stage.getHeight());
                shapeRenderer.end();
            }if(timerGame == GameConstants.ZONE_GREEN_DURATION - 3){
                shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
                shapeRenderer.setColor(new Color(1, 0, 0, 0.2f));
                shapeRenderer.rect(0, 0, stage.getWidth(), stage.getHeight());
                shapeRenderer.end();
            }if(timerGame == GameConstants.ZONE_GREEN_DURATION - 2){
                shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
                shapeRenderer.setColor(new Color(1, 0, 0, 0.2f));
                shapeRenderer.rect(0, 0, stage.getWidth(), stage.getHeight());
                shapeRenderer.end();
            }if(timerGame == GameConstants.ZONE_GREEN_DURATION - 1){
                shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
                shapeRenderer.setColor(new Color(1, 0, 0, 0.2f));
                shapeRenderer.rect(0, 0, stage.getWidth(), stage.getHeight());
                shapeRenderer.end();
            }if(timerGame == GameConstants.ZONE_GREEN_DURATION ){
                shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
                shapeRenderer.setColor(new Color(1, 0, 0, 0.3f));
                shapeRenderer.rect(0, 0, stage.getWidth(), stage.getHeight());
                shapeRenderer.end();
            }
        }
        if (timerGame >= GameConstants.ZONE_YELLOW_DURATION + GameConstants.ZONE_GREEN_DURATION - 9 && game.getZoneMapManager().getPlayerZone(localPlayer.getPosition()).equals(MapEntityType.YELLOW_ZONE)){
            if(!game.getZoneMapManager().getPlayerZone(localPlayer.getPosition()).equals(MapEntityType.YELLOW_ZONE)) {
                shapeRenderer.dispose();
                Gdx.gl.glDisable(GL20.GL_BLEND);
                return;
            }
            Gdx.gl.glEnable(GL20.GL_BLEND);
            Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
             if(timerGame == GameConstants.ZONE_YELLOW_DURATION + GameConstants.ZONE_GREEN_DURATION - 9){
                shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
                shapeRenderer.setColor(new Color(1, 0, 0, 0.1f));
                shapeRenderer.rect(0, 0, stage.getWidth(), stage.getHeight());
                shapeRenderer.end();
            }if(timerGame == GameConstants.ZONE_YELLOW_DURATION + GameConstants.ZONE_GREEN_DURATION - 8){
                shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
                shapeRenderer.setColor(new Color(1, 0, 0, 0.1f));
                shapeRenderer.rect(0, 0, stage.getWidth(), stage.getHeight());
                shapeRenderer.end();
            }if(timerGame == GameConstants.ZONE_YELLOW_DURATION + GameConstants.ZONE_GREEN_DURATION - 7){
                shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
                shapeRenderer.setColor(new Color(1, 0, 0, 0.1f));
                shapeRenderer.rect(0, 0, stage.getWidth(), stage.getHeight());
                shapeRenderer.end();
            }if(timerGame == GameConstants.ZONE_YELLOW_DURATION + GameConstants.ZONE_GREEN_DURATION - 6){
                shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
                shapeRenderer.setColor(new Color(1, 0, 0, 0.1f));
                shapeRenderer.rect(0, 0, stage.getWidth(), stage.getHeight());
                shapeRenderer.end();
            }if(timerGame == GameConstants.ZONE_YELLOW_DURATION + GameConstants.ZONE_GREEN_DURATION - 5){
                shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
                shapeRenderer.setColor(new Color(1, 0, 0, 0.1f));
                shapeRenderer.rect(0, 0, stage.getWidth(), stage.getHeight());
                shapeRenderer.end();
            }if(timerGame == GameConstants.ZONE_YELLOW_DURATION + GameConstants.ZONE_GREEN_DURATION - 4){
                shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
                shapeRenderer.setColor(new Color(1, 0, 0, 0.2f));
                shapeRenderer.rect(0, 0, stage.getWidth(), stage.getHeight());
                shapeRenderer.end();
            }if(timerGame == GameConstants.ZONE_YELLOW_DURATION + GameConstants.ZONE_GREEN_DURATION - 3){
                shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
                shapeRenderer.setColor(new Color(1, 0, 0, 0.2f));
                shapeRenderer.rect(0, 0, stage.getWidth(), stage.getHeight());
                shapeRenderer.end();
            }if(timerGame == GameConstants.ZONE_YELLOW_DURATION + GameConstants.ZONE_GREEN_DURATION - 2){
                shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
                shapeRenderer.setColor(new Color(1, 0, 0, 0.2f));
                shapeRenderer.rect(0, 0, stage.getWidth(), stage.getHeight());
                shapeRenderer.end();
            }if(timerGame == GameConstants.ZONE_YELLOW_DURATION + GameConstants.ZONE_GREEN_DURATION - 1){
                shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
                shapeRenderer.setColor(new Color(1, 0, 0, 0.2f));
                shapeRenderer.rect(0, 0, stage.getWidth(), stage.getHeight());
                shapeRenderer.end();
            }if(timerGame == GameConstants.ZONE_YELLOW_DURATION + GameConstants.ZONE_GREEN_DURATION ){
                shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
                shapeRenderer.setColor(new Color(1, 0, 0, 0.3f));
                shapeRenderer.rect(0, 0, stage.getWidth(), stage.getHeight());
                shapeRenderer.end();
            }
        }
    }

    public void setStartTime(int timerGame) {
        this.timerGame = timerGame;
    }

    public void setZoneGreenDuration(int zoneGreenDuration) {
        this.zoneGreenDuration = zoneGreenDuration;
    }

    public void setZoneYellowDuration(int zoneYellowDuration) {
        this.zoneYellowDuration = zoneYellowDuration;
    }

    public void setZoneRedDuration(int zoneRedDuration) {
        this.zoneRedDuration = zoneRedDuration;
    }
}
