package com.github.myio.ui;


import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.ProgressBar;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;


public  class HealthBar implements IDrawable {
	Stage stage;
	Health health;

	public HealthBar(int maxHealth) {
		int maxWidth =(int) (maxHealth *3.8);
		stage = new Stage();
		health = new Health(maxWidth, 15);
		health.setPosition(10, 10);
		stage.addActor(health);

	}

		@Override
		public void draw(float delta) {
			stage.draw();
			stage.act();
		}

	public void setHealth (float part) {
		health.setValue(part);
	}


	public Health getHealth() {
		return health;
	}



	}

