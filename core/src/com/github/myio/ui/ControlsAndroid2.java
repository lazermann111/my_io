package com.github.myio.ui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.Touchpad;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.github.myio.GameClient;
import com.github.myio.entities.PlayerCell;
import com.github.myio.screens.GameScreen;
import com.github.myio.tools.SoundManager;

import static com.github.myio.GameConstants.MEDKIT_MOVEMENT_SLOWDOWN_COEFF;


public class ControlsAndroid2 implements IDrawable
{

    private OrthographicCamera camera;
    private Stage stage;
    private Touchpad touchPadMovement;
    private Touchpad touchpadRotate;
    private Touchpad.TouchpadStyle touchpadStyleMovement;
    private Touchpad.TouchpadStyle touchpadStyleRotate;
    private Skin skin;
    private Drawable touchBackground;
    private Drawable touchBackgroundRed;
    private Drawable touchKnobMovement;
    private Drawable touchKnobRotate;
    private GameClient game;
    private PlayerCell localPlayer;
    private Rectangle rectangleMovement;
    private Rectangle rectangleRotate;

    float knobPersentYmovement;
    float knobPersentXmovement;

    float knobPersentYrotate = -0.30f;
    float knobPersentXrotate;

    float knobPersentXrotateFire;
    float knobPersentYrotateFire;

    private float posX;
    private float posY;

    private boolean firstTouch = true;
    private boolean firstTouchRemove = false;
    private boolean secondTouch = true;
    TextButton textButton;
    boolean fireButton;
    boolean reloadButton;

    boolean isShowTouchPad;
    ImageButton reloadImageButton;
    ImageButton leftButton;
    ImageButton rightButton;
    ImageButton btnPick;
    ImageButton btnExit;
    ImageButton btnMute;
    ImageButton btnFire;

    private ImageButton.ImageButtonStyle reloadStyleButton;
    private ImageButton.ImageButtonStyle leftStyleButton;
    private ImageButton.ImageButtonStyle rightStyleButton;
    private ImageButton.ImageButtonStyle exitBtnStyle;
    private ImageButton.ImageButtonStyle muteBtnStyle;
    private ImageButton.ImageButtonStyle fireBtnStyle;
    GameScreen gameScreen;

    Circle circleMov;
    Circle circleRot;
    Table tableRotate;
    Table tableMov;
    Table tableMenu;
    ImageButton.ImageButtonStyle btnPickStyle;

    Viewport viewport;

    boolean chekBtnPick = true;
    Sprite pickSprAlf;
    Image pickImALf;
    Image pickIm;
    boolean pickItem;
    boolean isMute = true;
    boolean isShowMenu = true;
    ExitMenu exit;


    public ControlsAndroid2(final GameScreen gameScreen, final ExitMenu exit, OrthographicCamera camera, Skin skin, GameClient game, final PlayerCell localPlayer)
    {
        this.gameScreen = gameScreen;
        this.camera = camera;
        this.skin = skin;
        this.game = game;
        this.localPlayer = localPlayer;
        this.exit = exit;




        float DPIWidht =(float) Gdx.graphics.getWidth()/1080;
        float DPIHeight = ( float) Gdx.graphics.getHeight()/720;
        float DPI = ((DPIWidht +DPIHeight)/2) * 0.85f;

        System.out.println(DPIHeight +"|"+DPIWidht);
        viewport = GameClient.getGame().getUiViewport();

        stage = new Stage(viewport, GameClient.getGame().getBatch());
        //rectangleMovement = new Rectangle(0, 0, Gdx.graphics.getWidth() / 2, Gdx.graphics.getHeight());

        Table rootTable = new Table(skin);
        rootTable.setFillParent(true);
        tableRotate = new Table(skin);
        tableMov = new Table(skin);
        tableMenu = new Table(skin);


        //imageButton


        Sprite fireSprite = new Sprite(new Texture(Gdx.files.internal("btn/btnFire.png")));
        final Image fireImage = new Image(fireSprite);
        Sprite fireSpriteAfla = new Sprite(new Texture(Gdx.files.internal("btn/btnFireAfla.png")));
        final Image fireImageAlfa = new Image(fireSpriteAfla);
        fireBtnStyle = new ImageButton.ImageButtonStyle();
        fireBtnStyle.down = fireImageAlfa.getDrawable();
        fireBtnStyle.up = fireImage.getDrawable();


        Sprite exitSprite = new Sprite(new Texture(Gdx.files.internal("btn/settings.png")));
        final Image exitImage = new Image(exitSprite);
        Sprite exitSpriteAfla = new Sprite(new Texture(Gdx.files.internal("btn/settingsAlfa.png")));
        final Image exitImageAlfa = new Image(exitSpriteAfla);
        exitBtnStyle = new ImageButton.ImageButtonStyle();
        exitBtnStyle.down = exitImageAlfa.getDrawable();
        exitBtnStyle.up = exitImage.getDrawable();

        Sprite muteSprite = new Sprite(new Texture(Gdx.files.internal("btn/mute.png")));
        final Image muteImage = new Image(muteSprite);
        Sprite muteAlfSprite = new Sprite(new Texture(Gdx.files.internal("btn/muteAlfa.png")));
        final Image muteAlfImage = new Image(muteAlfSprite);
        muteBtnStyle = new ImageButton.ImageButtonStyle();
        muteBtnStyle.down = muteImage.getDrawable();
        muteBtnStyle.up = muteImage.getDrawable();
        muteBtnStyle.imageChecked = muteAlfImage.getDrawable();
        muteBtnStyle.imageCheckedOver = muteAlfImage.getDrawable();
        muteBtnStyle.disabled = muteAlfImage.getDrawable();
        muteBtnStyle.imageDisabled = muteAlfImage.getDrawable();


        Sprite pickSpr = new Sprite(new Texture(Gdx.files.internal("btn/pick.png")));
        pickIm = new Image(pickSpr);
        pickSprAlf = new Sprite(new Texture(Gdx.files.internal("btn/pickAlfa.png")));
        pickImALf = new Image(pickSprAlf);
        btnPickStyle = new ImageButton.ImageButtonStyle();
        btnPickStyle.down = pickImALf.getDrawable();
        btnPickStyle.up = pickImALf.getDrawable();
        btnPickStyle.imageDisabled = pickImALf.getDrawable();
        btnPickStyle.disabled = pickImALf.getDrawable();



        Sprite reloadSprite = new Sprite(new Texture(Gdx.files.internal("btn/reload.png")));
        final Image reloadImage = new Image(reloadSprite);
        Sprite reloadSpriteAlfa = new Sprite(new Texture(Gdx.files.internal("btn/reloadAlfa.png")));
        final Image reloadImageAlfa = new Image(reloadSpriteAlfa);
        reloadStyleButton = new ImageButton.ImageButtonStyle();
        reloadStyleButton.down = reloadImageAlfa.getDrawable();
        reloadStyleButton.up = reloadImage.getDrawable();

        Sprite leftSprite = new Sprite(new Texture(Gdx.files.internal("btn/left.png")));
        final Image leftImage = new Image(leftSprite);
        Sprite leftSpriteAfla = new Sprite(new Texture(Gdx.files.internal("btn/leftAlfa.png")));
        final Image leftImageAlfa = new Image(leftSpriteAfla);
        leftStyleButton = new ImageButton.ImageButtonStyle();
        leftStyleButton.down = leftImageAlfa.getDrawable();
        leftStyleButton.up = leftImage.getDrawable();

        Sprite rightSprite = new Sprite(new Texture(Gdx.files.internal("btn/right.png")));
        final Image rightImage = new Image(rightSprite);
        Sprite rightSpriteAlfa = new Sprite(new Texture(Gdx.files.internal("btn/rightAlfa.png")));
        final Image rightImageAlfa = new Image(rightSpriteAlfa);

        rightStyleButton = new ImageButton.ImageButtonStyle();
        rightStyleButton.down = rightImageAlfa.getDrawable();
        rightStyleButton.up = rightImage.getDrawable();


        float buttonSizeWidth = viewport.getScreenWidth() / 10;
        float buttonSizeHeight = viewport.getScreenHeight() / 10;

        float touchpadSizeWidth = viewport.getScreenWidth() / 6;
        float touchpadSizeHeight = viewport.getScreenHeight() / 6;


        btnFire = new ImageButton(fireBtnStyle);
        btnFire.setSize(buttonSizeWidth, buttonSizeHeight);

        btnFire.addListener(new ClickListener(){
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button)
            {
                if(localPlayer.getCurrentWeapon() != null)
                {

                    localPlayer.getCurrentWeapon().setCanShot(true);
                    fireTest = true;
                }


                return  true;
            }

            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button)
            {
                fireTest = false;
            }
        });


        btnExit = new ImageButton(exitBtnStyle);
        btnExit.setSize(buttonSizeWidth, buttonSizeHeight);
        btnExit.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y)
            {
                exit.showDialog();

//                if (isShowMenu)
//                {
//                    isShowMenu = false;
//                    exit.showDialog();
//                }else {
//                    isShowMenu = true;
//                    exit.hideDialog();
//                }
            }
        });

        btnMute = new ImageButton(muteBtnStyle);
        btnMute.setSize(buttonSizeWidth, buttonSizeHeight);
        btnMute.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y)
            {
                if (isMute)
                {
                    isMute = false;
                    btnMute.setChecked(true);
                    btnMute.setDisabled(true);
                    SoundManager.mute = true;
                    Gdx.app.log("android2","isMute111" + isMute);
                }else {
                    isMute = true;
                    btnMute.setChecked(false);
                    btnMute.setDisabled(false);
                    SoundManager.mute = false;
                    Gdx.app.log("android2","isMute222" + isMute);
                }


            }
        });


        btnPick = new ImageButton(btnPickStyle);
        btnPick.setSize(buttonSizeWidth, buttonSizeHeight);
        btnPick.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y)
            {
                pickItem = true;
                Gdx.app.log("android2","click btnClick"+pickItem);
            }});



        leftButton = new ImageButton(leftStyleButton);
        leftImage.setSize(buttonSizeWidth, buttonSizeHeight);
        leftButton.addListener(new ClickListener()
        {
            @Override
            public void clicked(InputEvent event, float x, float y)
            {
                gameScreen.backChangeWeapon();
            }
        });


        rightButton = new ImageButton(rightStyleButton);
        rightButton.setSize(buttonSizeWidth, buttonSizeHeight);
        rightButton.addListener(new ClickListener()
        {
            @Override
            public void clicked(InputEvent event, float x, float y)
            {
                gameScreen.nextChangeWeapon();
            }
        });

        reloadImageButton = new ImageButton(reloadStyleButton);
        reloadImageButton.setSize(buttonSizeWidth, buttonSizeHeight);
        reloadImageButton.addListener(new ClickListener()
        {
            @Override
            public void clicked(InputEvent event, float x, float y)
            {
                reloadButton = true;
            }
        });


        // MOVEMENT
        skin.add("touchBackgroundRed", new Texture("btn/touchbackgraundRed.png"));
        skin.add("touchBackground", new Texture("btn/touchBackgraund.png"));

        skin.add("touchKnobMovement", new Texture("btn/touchKnobMovement.png"));
        skin.add("touchKnobRotate", new Texture("btn/touchKnobRotate.png"));

        touchpadStyleMovement = new Touchpad.TouchpadStyle();

        touchBackground = skin.getDrawable("touchBackground");
        touchKnobMovement = skin.getDrawable("touchKnobMovement");
        touchpadStyleMovement.background = touchBackground;
        touchpadStyleMovement.knob = touchKnobMovement;
        touchPadMovement = new Touchpad(40, touchpadStyleMovement);
        touchPadMovement.setBounds(15, 15, touchpadSizeWidth, touchpadSizeHeight);


        //ROTATE

        touchKnobRotate = skin.getDrawable("touchKnobRotate");
        touchpadStyleRotate = new Touchpad.TouchpadStyle();
        touchpadStyleRotate.background = touchBackground;
        touchpadStyleRotate.knob = touchKnobRotate;
        touchpadRotate = new Touchpad(40, touchpadStyleRotate);
        touchpadRotate.setBounds(15, 15, touchpadSizeWidth*DPI, touchpadSizeHeight*DPI);

        System.out.println(DPI+"DPI========");
        tableMenu.add(btnExit).space(15,15,15,15).width(90*DPI).height(90*DPI);
        tableMenu.add(btnMute).space(15,15,15,15).width(90*DPI).height(90*DPI);



        tableRotate.add(leftButton).space(10,20,10,10).left().width(130*DPI).height(100*DPI);
        tableRotate.add(rightButton).space(10,10,10,20).right().width(130*DPI).height(100*DPI);
        tableRotate.row();
        tableRotate.add(btnFire).space(10,10,10,10).center().colspan(2).width(150*DPI).height(150*DPI);
        tableRotate.row();
        tableRotate.add(btnPick).space(10,10,10,10).left().width(100*DPI).height(100*DPI);
        tableRotate.add(reloadImageButton).space(10,10,10,10).right().width(100*DPI).height(100*DPI);


        tableMov.add(touchPadMovement).left().bottom().width(320*DPI).height(320*DPI);


        rootTable.add(tableMenu).expand().right().top().padTop(10).padRight(110).colspan(2);
        rootTable.row();
        rootTable.add(tableMov).expand().left().bottom().padBottom(105).padLeft(40);
        rootTable.add(tableRotate).expand().right().bottom().padBottom(100).padRight(110);
        rootTable.setTransform(true);

       // stage.setDebugAll(true);
        stage.addActor(rootTable);

    }

    private boolean posTouchFirst = true;
    private boolean posTouchSecond = true;
    private Touchpad first;
    private Touchpad second;

    boolean btnPickOneSet = true;
    boolean activateRotate;

    @Override
    public void draw(float delta)
    {

        if (touchPadMovement != null)
        {
            knobPersentYmovement = touchPadMovement.getKnobPercentY();
            knobPersentXmovement = touchPadMovement.getKnobPercentX();

//            if(activateRotate)
//            {
//                knobPersentYrotate = touchPadMovement.getKnobPercentY();
//                knobPersentXrotate = touchPadMovement.getKnobPercentX();
//            }
            knobPersentXrotateFire = touchPadMovement.getKnobPercentX();
            knobPersentYrotateFire = touchPadMovement.getKnobPercentY();

        }
        //Gdx.app.log("ANDROID22","knobPersentYrotate = "+knobPersentYrotate +"knobPersentXmovement =  " + knobPersentXrotate);

//        if (touchpadRotate != null)
//        {
//            knobPersentYrotate = touchpadRotate.getKnobPercentY();
//            knobPersentXrotate = touchpadRotate.getKnobPercentX();
//        }
        //System.out.println(chekBtnPick);
        if (chekBtnPick)
        {
            if (btnPickOneSet)
            {
                btnPick.setDisabled(true);
                //btnPick.setTouchable(Touchable.enabled);
                btnPickStyle.down = pickIm.getDrawable();
                btnPickStyle.up = pickIm.getDrawable();

            }


            btnPickOneSet = false;
        }else {

            if (!btnPickOneSet)
            {
                btnPick.setDisabled(false);
                // btnPick.setTouchable(Touchable.disabled);
                btnPickStyle.down = pickImALf.getDrawable();
                btnPickStyle.up = pickImALf.getDrawable();

            }


            btnPickOneSet = true;
        }

//           if (Gdx.input.isTouched(0))
//           {
//               first = getTouchpadTypeByCoords(Gdx.input.getX(0), camera.viewportHeight - Gdx.input.getY(0));
//               drawTouchPadForFirstClick(0);
//
//           }
//           else if (!Gdx.input.isTouched(0))
//           {
//               removActor(first);
//               first = null;
//               posTouchFirst = true;
//           }
//
//
//           if (Gdx.input.isTouched(1))
//           {
//               second = getTouchpadTypeByCoords(Gdx.input.getX(1), camera.viewportHeight - Gdx.input.getY(1));
//               drawTouchPadForSecondClick(1);
//
//           }
//           else if (!Gdx.input.isTouched(1))
//           {
//               removActor(second);
//               second = null;
//               posTouchSecond = true;
//
//           }


        stage.act();
        stage.draw();

        movementAcceleration();
        fire();
    }

    boolean showRedMov = true;

    public void setChekBtnPick(boolean chekBtnPick) {
        this.chekBtnPick = chekBtnPick;
    }

    public boolean movementAcceleration()
    {

        float posKnob = (float) Math.sqrt(touchPadMovement.getKnobPercentX() * touchPadMovement.getKnobPercentX() + touchPadMovement.getKnobPercentY() * touchPadMovement.getKnobPercentY());


        if (posKnob > 0.85)
        {
            if (showRedMov)
            {
                //touchPadMovement.getStyle().background = skin.getDrawable("touchBackgroundRed");
                showRedMov = false;
            }
            knobPersentYrotate = touchPadMovement.getKnobPercentY();
            knobPersentXrotate = touchPadMovement.getKnobPercentX();
            return true;
        }
        else
        {
           // activateRotate = false;
            if (!showRedMov)
            {
                //touchPadMovement.getStyle().background = skin.getDrawable("touchBackground");
                showRedMov = true;
            }

        }
        return false;
    }

    boolean showRedRot = true;
    boolean fire = true;
    boolean fireTest;

    public boolean fire()
    {

        float posKnob = (float) Math.sqrt(touchpadRotate.getKnobPercentX() * touchpadRotate.getKnobPercentX() + touchpadRotate.getKnobPercentY() * touchpadRotate.getKnobPercentY());

        if (posKnob > 0.85)
        {
            if (showRedRot)
            {
                touchpadRotate.getStyle().background = skin.getDrawable("touchBackgroundRed");
                showRedRot = false;
            }
            fire = true;
        }
        else
        {
            if (!showRedRot)
            {
                touchpadRotate.getStyle().background = skin.getDrawable("touchBackground");
                showRedRot = true;
            }
            fire = false;
        }

        if (fire)
        {

            if (!localPlayer.isSuppressed())
            {
                return true;
            }
            else
            {
                return false;
            }

        }
        else
        {
            if (localPlayer.getCurrentWeapon() != null)
            {
                localPlayer.getCurrentWeapon().setCanShot(true);
            }
            return false;
        }


    }

    private void drawTouchPadForFirstClick(int pointer)
    {
        if (posTouchFirst)
        {
            getTouchpadTypeByCoords(Gdx.input.getX(pointer), camera.viewportHeight - Gdx.input.getY(pointer)).setPosition(Gdx.input.getX(pointer) - 100, camera.viewportHeight - Gdx.input.getY(pointer) - 100);
            stage.addActor(getTouchpadTypeByCoords(Gdx.input.getX(pointer), camera.viewportHeight - Gdx.input.getY(pointer)));

            posTouchFirst = false;
        }


    }

    private void drawTouchPadForSecondClick(int pointer)
    {
        if (posTouchSecond)
        {
            getTouchpadTypeByCoords(Gdx.input.getX(pointer), camera.viewportHeight - Gdx.input.getY(pointer)).setPosition(Gdx.input.getX(pointer) - 100, camera.viewportHeight - Gdx.input.getY(pointer) - 100);
            stage.addActor(getTouchpadTypeByCoords(Gdx.input.getX(pointer), camera.viewportHeight - Gdx.input.getY(pointer)));
            posTouchSecond = false;
        }
    }


    private void removActor(Actor actor)
    {
        if (actor != null)
        {
            actor.remove();
        }

    }

    public Touchpad getTouchpadTypeByCoords(float x, float y)
    {
        if (rectangleMovement.contains(x, y))
        {
            return this.touchPadMovement;
        }
        else
        {
            return this.touchpadRotate;
        }
    }

    public String getTouchpadNameByCoords(float x, float y)
    {
        if (rectangleMovement.contains(x, y))
        {
            return "MOVEMENT";
        }
        else
        {
            return "ROTATE";
        }
    }


    public float getYTouchPadmovement()
    {
        return knobPersentYmovement;
    }

    public float getXTouchPadmovement()
    {
        return knobPersentXmovement;
    }

    public float getYTouchPadrotate()
    {
        return knobPersentYrotate;
    }

    public float getXTouchPadrotate()
    {
        return knobPersentXrotate;
    }


    public Stage getStage()
    {
        return stage;
    }

    public boolean isFireButton()
    {
        return fireButton;
    }

    public boolean isReloadButton()
    {
        return reloadButton;
    }

    public void setReloadButton(boolean reloadButton)
    {
        this.reloadButton = reloadButton;
    }

    public void setPickItem(boolean pickItem) {this.pickItem = pickItem;}

    public boolean isPickItem() {return pickItem;}

    public boolean isFireTest()
    {
        return fireTest;
    }

    public void setFireTest(boolean fireTest)
    {
        this.fireTest = fireTest;
    }

    public float getKnobPersentXrotateFire() {return knobPersentXrotateFire;}

    public float getKnobPersentYrotateFire() {return knobPersentYrotateFire;}
}