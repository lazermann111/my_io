package com.github.myio.ui;


import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.github.myio.GameClient;
import com.github.myio.entities.PlayerCell;

import java.util.ArrayList;
import java.util.Map;
@Deprecated
public class Scoreboard implements IDrawable {
    final  static int WIDTH=1200;
    final static int HEIGTH=1000;
    final static int POS_X=10;
    final static int POS_Y=10;

    int width,height,posX,posY;


    private Map<String, PlayerCell> allPlayers;

    private BitmapFont font;
    Viewport viewport;
    GameClient game;
    Stage stage;
    //OrthographicCamera camera;
    Label[] labels=new Label[SCOREBOARD_SIZE];
    Label.LabelStyle labelStyle;
    ShapeRenderer shapeRenderer;
    public static int SCOREBOARD_SIZE = 10;

    public Scoreboard(GameClient game, OrthographicCamera camera,Map<String,PlayerCell> list)
    {

        this.width=Scoreboard.WIDTH;
        this.height=Scoreboard.HEIGTH;
        this.posX=Scoreboard.POS_X;
        this.posY=Scoreboard.POS_Y;
        this.allPlayers =list;
       // this.camera = camera;
        this.game=game;
        font = GameClient.getFont32Black();

        shapeRenderer=game.getUiShapeRenderer();//new ShapeRenderer();

        viewport = game.getUiViewport();
        this.stage= new Stage(viewport,game.getBatch());



        Pixmap rect = new Pixmap(400,200,Pixmap.Format.RGBA8888);

        rect.setColor(0,0,0,200);
        rect.fillRectangle(3,2 , 65, 70);

        final Texture img =new Texture(rect);
        final Drawable splashDrawable = new TextureRegionDrawable(new TextureRegion(img));



        Table table = new Table();
        table.setWidth(width);
        table.setHeight(height);
        table.top();
        table.left();
        table.pad(10);

        table.setBackground(splashDrawable);
        labelStyle = new Label.LabelStyle();

        labelStyle.font = font;
        createList();


        table.add(new Label("Top Players",labelStyle)).expandX().padTop(10).align(Align.left).padLeft(20);
        table.row();
        //updateLabels();


        for(Label l:labels)
        {
            // IoLogger.log("-----------------Add Label ");
            table.add(l).expandX().padTop(10).align(Align.left).padLeft(10);
            table.row();
        }

        stage.addActor(table);
    }

    private long lastUpdate;

    @Override
    public void draw(float delta) {
       // if(lastUpdate + 500 > System.currentTimeMillis()) return;
        //lastUpdate = System.currentTimeMillis();

        updateLabels ();
        //game.getBatch().setProjectionMatrix(camera.combined);
        stage.act();
        stage.draw();
       // camera.update();
        //shapeRenderer.setProjectionMatrix(camera.combined);
      //  shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
       // shapeRenderer.setColor(250,250,250,255);
      //  shapeRenderer.rect(10,-10,100,100);
      //  shapeRenderer.end();
    }

    public void createList ()
    {

        for(int i=0;i<SCOREBOARD_SIZE;i++)
        {
            labels[i]=new Label("",labelStyle);
        }
    }
    public void updateLabels ()
    {
        ArrayList<PlayerCell> tmp = new ArrayList<PlayerCell>(allPlayers.values());
        int i =0;
        for (PlayerCell c : tmp)
        {
            if(i >= SCOREBOARD_SIZE) return;
            Label l = labels[i];
           // l.setText((i+1)+": "+(c.getUsername())+"  "+(c.getScore()));
            i++;
        }

        if(i < SCOREBOARD_SIZE -1)
        {
            for (int j = i; j < SCOREBOARD_SIZE; j++)
            {
                Label l = labels[j];
                l.setText("");
            }
        }
    }


}
