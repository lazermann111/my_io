package com.github.myio.ui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.utils.Timer;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.github.myio.GameClient;
import com.github.myio.dto.PlayerSessionStatsDto;
import com.github.myio.entities.PlayerCell;
import com.github.myio.screens.FadeScreen;
import com.github.myio.screens.GameScreen;
import com.github.myio.tools.IoLogger;

import java.util.Map;

import static com.github.myio.GameConstants.SINGLE_PLAYER_DEATH;
import static com.github.myio.GameConstants.SINGLE_PLAYER_WIN;
import static com.github.myio.GameConstants.SQUAD_DEATH;
import static com.github.myio.GameConstants.SQUAD_WIN;
import static com.github.myio.GameConstants.TEAM_LOOSE;
import static com.github.myio.GameConstants.TEAM_WIN;

/**
 * Created by alex on 18.04.18.
 */


public class FinalMessage implements IDrawable {

    PlayerCell mPlayer;
    Map<String, String> mSquadNames;
    Stage stage;
    Skin skin;
    Table mainTable;
    Table rootTable;
    TextButton newGameBtn;
    TextButton spectateButton;
    Drawable mainBackground;
    Drawable statBackground;
    Drawable playNewBackground;
    private GameClient mGame;
    //private Map<String, PlayerSessionStatsDto> mStatMap;
    InputProcessor originalInput;
    Viewport viewport;
    public FinalMessage(GameClient game, Viewport viewport) {
        mGame = game;
        stage = new Stage(game.getUiViewport());
        skin = new Skin(Gdx.files.internal("atlas/default/skin/uiskin.json"), new TextureAtlas("atlas/default/skin/uiskin.atlas"));
        this.viewport = viewport;
    }

    FadeScreen fadeScreen = new FadeScreen(GameClient.getGame());

        @Override
    public void draw(float delta) {
            stage.act();
            stage.draw();
           /* switch(code) {
                case SINGLE_PLAYER_DEATH:
                    if(localPlayer != null){
                        System.out.println("im here");
                        Texture texture;
                        texture = new Texture(Gdx.files.internal("images/black.jpg"));
                        SpriteBatch batch = new SpriteBatch();
                        batch.begin();
                        batch.draw(texture, 0, 0);
                        batch.end();
                    }
                    break;
            }
*/
    }

    String title;
    Map<String, PlayerSessionStatsDto> statMap1;
    Drawable f;
    public void showMessage(int code, Map<String, PlayerSessionStatsDto> statMap) {
        statMap1=statMap;
        mainTable = new Table();
        rootTable = new Table();
        originalInput = Gdx.input.getInputProcessor();
        Gdx.input.setInputProcessor(stage);
        mPlayer = mGame.getLocalPlayer();

        mainBackground = new Image(GameClient.uiAtlas.createSprite("main_table_bgrd_75")).getDrawable();
        statBackground = new Image(GameClient.uiAtlas.createSprite("player_stat_bgrd")).getDrawable();
           playNewBackground = new Image(GameClient.uiAtlas.createSprite("play_new_game_btn")).getDrawable();

        switch(code) {
            case SINGLE_PLAYER_WIN:
                if(mPlayer != null) {
                    title = mPlayer.getUsername() + " win the game!";
                    addToMainTable(title, getPlayerStat(mPlayer.getUsername(), statMap.get(mPlayer.getId())), false);
                }
                break;
            case SQUAD_WIN:
                if(mSquadNames !=null) {
                    int squadId = GameClient.getGame().getLocalPlayer().getSquadId();
                    title = "Squad #"  + squadId + " win the game!";
                    Table squadTable = new Table();
                    for (Map.Entry<String, PlayerSessionStatsDto> entry : statMap.entrySet()) {
                        if(entry != null) {
                            squadTable.add(getPlayerStat(mSquadNames.get(entry.getKey()), entry.getValue())).padRight(10);}
                    }
                    addToMainTable(title, squadTable, false);
                }
                break;
            case SINGLE_PLAYER_DEATH:
                if(mPlayer != null) {

                    Timer.schedule(new Timer.Task()
                    {
                        @Override
                        public void run()
                        {

                            title = mPlayer.getUsername() + " was killed!";
                            addToMainTable(title, getPlayerStat(mPlayer.getUsername(), statMap1.get(mPlayer.getId())),true);
                        }
                    }, 3f);

                }
                break;
            case SQUAD_DEATH:
                if(mSquadNames !=null) {
                    int squadId = GameClient.getGame().getLocalPlayer().getSquadId();
                    title = "Squad #"  + squadId + " was eliminated!";
                    Table squadTable = new Table();
                    for (Map.Entry<String, PlayerSessionStatsDto> entry : statMap.entrySet()) {
                        if(entry != null) {
                        squadTable.add(getPlayerStat(mSquadNames.get(entry.getKey()), entry.getValue())).padRight(10);}
                    }
                    addToMainTable(title, squadTable,true);
                }
                break;
            case TEAM_WIN:
                if(mPlayer != null) {

                    title = "Your team wins!";
                    addToMainTable(title, getPlayerStat(mPlayer.getUsername(), statMap.get(mPlayer.getId())),false);
                }
                break;

            case TEAM_LOOSE:
                if(mPlayer != null) {
                    title = "Your team loses!";
                    addToMainTable(title, getPlayerStat(mPlayer.getUsername(), statMap.get(mPlayer.getId())),false);
                }
                break;
        }


        IoLogger.log("SHOW_BANNER_HERE");

    }


    private Table getPlayerStat(String playerName, PlayerSessionStatsDto stat) {
        Table statTable = new Table(skin);
        Table rootTabel = new Table();
        rootTabel.add(statTable);
        Label label = new Label(playerName, skin);
        label.setColor(Color.WHITE);


        try
        {
            statTable.add(label).colspan(2).center().pad(5, 0, 8, 0).row();
            statTable.add("Kills").left().pad(0, 5, 5, 8);
            statTable.add(String.valueOf(stat.killsNumber)).pad(0, 0, 5, 8).row();    // add here kills num
            statTable.add("Damage dealt").left().pad(0, 5, 5, 8);
            statTable.add(String.valueOf(stat.dealtDamage)).pad(0, 0, 5, 8).row();    // add here Damage dealt
            statTable.add("Damage taken").left().pad(0, 5, 5, 8);
            statTable.add(String.valueOf(stat.takenDamage)).pad(0, 5, 5, 8).row();    // add here Damage taken
            statTable.add("Survived").left().pad(0, 5, 5, 8);
            statTable.add(String.valueOf(stat.survivedFor) + " s").pad(0, 5, 5, 8).row();    // add here Survived time
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }


        return statTable;
    }


    public void hide()
    {
        Gdx.input.setInputProcessor(originalInput);
        stage.dispose();


    }

    public void addToMainTable(String title, Table statInfo, boolean hasSpectateButton) {
        float DPIWidht = (float) Gdx.graphics.getWidth() / 1080;
        float DPIHeight = (float) Gdx.graphics.getHeight() / 720;
        float DPI = ((DPIWidht + DPIHeight) / 2) * 0.85f;

        Image bckMiddle = new Image(new Sprite(new Texture(Gdx.files.internal("mainMenu/background.png"))));

        Label.LabelStyle whiteStyle = new Label.LabelStyle(GameClient.getFont32White(), Color.WHITE);
        Label label = new Label(title, whiteStyle);
        label.setFontScale(2);
        mainTable.add(label).padBottom(30).padRight(25).padLeft(25).padTop(Gdx.graphics.getHeight()/40).row();
        mainTable.add(statInfo).padBottom(10).row();

        skin = new Skin(Gdx.files.internal("atlas/mainMenu/newSkin.json"), new TextureAtlas("atlas/mainMenu/newSkin.atlas"));
        skin.getFont("trebushet").getData().setScale(1.4f);

        newGameBtn = new TextButton("Play New Game", skin);

        newGameBtn.addListener(new ClickListener() {
            public void clicked (InputEvent event, float x, float y) {
                mGame.getScreen().dispose();
                mGame.setScreen(GameClient.getGame().getMainMenuScreen());
            }
        });
        mainTable.setBackground(bckMiddle.getDrawable());
        mainTable.add(newGameBtn).width(Gdx.graphics.getWidth()/5).height(Gdx.graphics.getHeight()/9).space(10 * DPI, 10 * DPI, 10 * DPI, 10 * DPI);
        mainTable.row();

        if(hasSpectateButton)
        {
            spectateButton = new TextButton("Spectate", skin);
            spectateButton.getLabelCell().pad(3, 3, 3, 3);
            spectateButton.addListener(new ClickListener()
            {
                public void clicked(InputEvent event, float x, float y)
                {
                    if (mGame.getScreen() instanceof GameScreen)
                    {
                        hide();
                        ((GameScreen) mGame.getScreen()).spectate();
                    }

                }
            });
            mainTable.add(spectateButton).width(Gdx.graphics.getWidth()/5).height(Gdx.graphics.getHeight()/9).padBottom(Gdx.graphics.getHeight()/40).space(10 * DPI, 10 * DPI, 10 * DPI, 10 * DPI);
        }   mainTable.row();

        rootTable.add(mainTable);
        rootTable.setFillParent(true);

        stage.addActor(rootTable);
      //  rootTable.debugAll();
    }

    public void setSquadNames(Map<String, String> names) {
        mSquadNames = names;
    }

    }
