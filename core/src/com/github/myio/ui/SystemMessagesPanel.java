package com.github.myio.ui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.github.myio.GameClient;
import com.github.myio.entities.PlayerCell;
import com.github.myio.enums.GameServerState;
import com.github.myio.tools.IoLogger;

import java.util.ArrayList;
import java.util.List;

import static com.github.myio.GameConstants.WARMUP_PHASE_DURATION;

/**
 * Debug tool for rendering specific game entities
 */

public class SystemMessagesPanel implements IDrawable
{


    int MSG_AMOUNT = 15;


    List<Label> labels = new ArrayList<Label>(5);
    List<String> messages = new ArrayList<String>(5);
    GameClient game;


    Stage stage;
    Viewport viewport;
    BitmapFont font;
    Skin skin;
    PlayerCell localPlayer;
    public Label labelConnect;
    boolean startWarmup;
    int countUpdateLabel = 1000;
    long updateWarmup;
    long warmupTime = WARMUP_PHASE_DURATION / 1000;

    long deleteMesage = 5000;
    long deleteUpdate;


    private final int MSG_SIZE = 2;
    private static long MSG_SHOW_DURATION = 50000;
    private MessageLabel[] messageLabels = new MessageLabel[MSG_SIZE];

    String s;
    Table table;


    public SystemMessagesPanel(GameClient game, OrthographicCamera camera, PlayerCell localPlayer)
    {
        this.game = game;
        this.localPlayer = localPlayer;

        font = GameClient.getFont32Black();
        Viewport screenViewport = game.getUiViewport();
        skin = new Skin(Gdx.files.internal("atlas/default/skin/uiskin.json"), new TextureAtlas("atlas/default/skin/uiskin.atlas"));

        viewport = game.getUiViewport();
        stage = new Stage(screenViewport, game.getBatch());
        table = new Table();
        table.top();
        table.setFillParent(true);

        labelConnect = new Label("", skin);
      //  table.add(labelConnect).top();
        table.row();

        createList();
        stage.addActor(table);

    }

    private int currentIdx;

    @Override
    public void draw(float delta)
    {


        updateLabes();
        stage.act();
        stage.draw();

        if (updateWarmup + countUpdateLabel < System.currentTimeMillis() && startWarmup)
        {
            warmupTime--;
            // labelConnect.setText("GAME START AFTER " + warmupTime + " second");
            if (warmupTime <= 0)
            {
                startWarmup = false;
            }
            updateWarmup = System.currentTimeMillis();
        }


//        stage.act();
//        stage.draw();
    }

    public void clear()
    {
        messages.clear();
    }

    public void message(String s)
    {
        addLabel(s);
    }

    public void messageForGameState(GameServerState s)
    {
        switch (s)
        {
            case LAUNCHING:
                labelConnect.setText("Welcome " + localPlayer.getUsername() + " game will end when more people join");
                IoLogger.log("LAUNCHING");
                break;
            case WAITING_FOR_PLAYERS:
                IoLogger.log("WAITING_FOR_PLAYERS");
                break;
            case WARMUP:
                startWarmup = true;
                IoLogger.log("WARMUP");
                break;
            case GAME_IN_PROGRESS:
                labelConnect.setText("");
                IoLogger.log("GAME_IN_PROGRESS");
                break;
        }

    }

    public void addLabel(String message)
    {
        for (MessageLabel messageLabel : messageLabels)
        {
            if (!messageLabel.isUsed)
            {
                messageLabel.setUsed(true);
                messageLabel.setMessage(message);
                messageLabel.setStartTime(System.currentTimeMillis());
                messageLabel.getTextLabel().setText(message);
                break;
            }
        }
    }

    private void updateLabes()
    {
        for (MessageLabel messageLabel : messageLabels)
        {
            if (messageLabel.isUsed)
            {
                if (messageLabel.getStartTime() + MSG_SHOW_DURATION < System.currentTimeMillis())
                {
                    messageLabel.setUsed(false);
                    messageLabel.getTextLabel().setText("");
                    break;
                }

            }
        }
    }

    private void createList()
    {

        for (int i = 0; i < MSG_SIZE; i++)
        {
            messageLabels[i] = new MessageLabel();
           // table.add(messageLabels[i].getTextLabel());
            table.row();

        }


    }

    public Label getLabelConnect() {return labelConnect;}


    private class MessageLabel
    {
        Label textLabel;
        String message;
        long startTime;
        boolean isUsed;

        public MessageLabel()
        {
            textLabel = new Label("", skin);
        }


        public void setTextLabel(Label textLabel)
        {
            this.textLabel = textLabel;
        }

        public void setMessage(String message)
        {
            this.message = message;
        }

        public void setStartTime(long startTime)
        {
            this.startTime = startTime;
        }

        public void setUsed(boolean used)
        {
            isUsed = used;
        }

        public Label getTextLabel()
        {
            return textLabel;
        }

        public String getMessage()
        {
            return message;
        }


        public long getStartTime()
        {
            return startTime;
        }

        public boolean isUsed()
        {
            return isUsed;
        }
    }
}

