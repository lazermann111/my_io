package com.github.myio.ui;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.ProgressBar;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.github.myio.screens.LoadingScreen;

/**
 * Created by Alexsandr on 09.07.2018.
 */

public class LoadingBar  implements IDrawable  {


    private Stage stage;
    private Loading loading;
    private LoadingScreen loadingScreen;

    private int maxLoading;
    public LoadingBar(int maxLoading)
    {
        this.maxLoading = maxLoading;
       int maxWidth =(int) (maxLoading *6.5);
        stage = new Stage();
        loading = new Loading(/*380*/maxWidth, 25);
        loading.setPosition(200, 20);
        stage.addActor(loading);
    }

    public void setMaxLoading(int maxLoading) {
        this.maxLoading = maxLoading;
    }

    @Override
    public void draw(float delta) {

        stage.draw();
        stage.act();
    }

 /*private Animation animation;
   private TextureRegion reg;
   private float stateTime;

    public LoadingBar(Animation animation) {
        this.animation = animation;
        reg = (TextureRegion) animation.getKeyFrame(0);
    }

    @Override
    public void act(float delta) {
        stateTime += delta;
        reg = (TextureRegion) animation.getKeyFrame(stateTime);
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        batch.draw(reg, getX(), getY());
    }*/
}