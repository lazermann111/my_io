package com.github.myio.ui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.github.myio.GameClient;
import com.github.myio.enums.PlayerState;


/**
 * Created by alex on 23.04.18.
 */

    public class LockScreen implements IDrawable{

    Stage stage;
    PlayerState mPlayerState;
    Skin skin;
    Viewport viewport;

    public LockScreen(GameClient game,PlayerState playerState) {
        Viewport viewport = game.getUiViewport();
        stage = new Stage(viewport);
        mPlayerState = playerState;
    }

    public void showLock() {
        mPlayerState = PlayerState.IN_LOBBY;
        Gdx.input.setInputProcessor(stage);
        Table rootTable = new Table();
        rootTable.setFillParent(true);
        Table mainTable = new Table();

        Image backLockScreen = new Image(new Sprite(new Texture("mainMenu/background.png")));

        skin = new Skin(Gdx.files.internal("atlas/mainMenu/newSkin.json"), new TextureAtlas("atlas/mainMenu/newSkin.atlas"));
        Label.LabelStyle style = new Label.LabelStyle(GameClient.getFont32White(), Color.WHITE);

        TextButton backBtn = new TextButton("Back to Main Menu", skin);
        backBtn.getLabelCell().pad(3, 3, 3, 3);
        backBtn.addListener(new ClickListener() {
            public void clicked (InputEvent event, float x, float y) {
                GameClient.getGame().setScreen(GameClient.getGame().getMainMenuScreen());
        }
        });

        Gdx.input.setInputProcessor(new InputAdapter() {
            public boolean touchDown(int screenX, int screenY, int pointer, int button) {
                if (button == Input.Buttons.LEFT) {
                    GameClient.getGame().setScreen(GameClient.getGame().getMainMenuScreen());
                }
                return true;
            }
        });

        mainTable.setBackground(backLockScreen.getDrawable());
        mainTable.pack();
        mainTable.setSize(Gdx.graphics.getWidth()/3, Gdx.graphics.getHeight()/3);
        mainTable.add(new Label("   CONNECTION TO SERVER HAS BEEN LOST :(   ", style)).pad(10, 0, 20, 0).row();
        mainTable.add(backBtn).height(Gdx.graphics.getHeight()/12).width(Gdx.graphics.getWidth()/6).padBottom(10).padBottom(10).row();
        mainTable.setPosition((Gdx.graphics.getWidth() / 2) - mainTable.getWidth() / 2, Gdx.graphics.getHeight() / 3 - mainTable.getHeight() / 3);
        mainTable.setSize(Gdx.graphics.getWidth() / 16 , Gdx.graphics.getHeight() / 5);
        rootTable.add(mainTable);
        stage.addActor(rootTable);
    }

    @Override
    public void draw(float delta) {
        stage.act();
        stage.draw();
    }
}
