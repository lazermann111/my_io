package com.github.myio.ui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.github.myio.GameClient;
import com.github.myio.entities.PlayerCell;
import com.github.myio.enums.ItemType;

/**
 * Created by taras on 26.02.2018.
 */

public class ItemPopUp implements IDrawable
{


    PlayerCell localPlayer;
    GameClient game;
    Viewport viewport;
    Stage stage;
    ImageButton imageButton;
    public boolean clickImageButton;
    public boolean showButton;

    Table table;
    Label labelF;
    Skin skin;
    OrthographicCamera camera;
    ImageButton.ImageButtonStyle pickArmor;
    ControlsAndroid controlsAndroid;


    public ItemPopUp(OrthographicCamera camera, ControlsAndroid controlsAndroid, PlayerCell localPlayer, GameClient game, Skin skin)
    {
        this.localPlayer = localPlayer;
        this.game = game;
        this.skin = skin;
        this.camera = camera;
        this.controlsAndroid = controlsAndroid;

        viewport = game.getUiViewport();
        stage = new Stage(viewport, game.getBatch());



        labelF = new Label("CLICK F", skin);
        labelF.setColor(Color.GREEN);
        labelF.setOrigin(labelF.getWidth() / 2, labelF.getHeight() / 2);
        labelF.setPosition(Gdx.graphics.getWidth()/2,Gdx.graphics.getHeight()/1.8f);

       stage.addActor(labelF);

        //stage.setDebugAll(true);

    }
    String helmet = "armor";
    String pistol = "pistol";
    String patronBox = "patron_box";
    String medKit = "hp_100";
    String shotgan = "shotgun";
    String flashGrenad = "flash_grenade";
    String fragGrenad = "frag_grenade";
    String machinegun = "machinegun";
    String bazooka = "bazooka";
    String ammunition = "ammunition";
    String tommyGun = "tommy_gun";


    public void itemInfoPickup(String nameCell)
    {
        if (GameClient.getGame().isAndroid())
        {


        }
        else
        {
            labelF.setText("CLICK F "+nameCell);
        }
        showButton = true;
    }


    public void itemInfoPickup(String item_blueprint, ItemType itemType)
    {
        labelF.setText(" "+item_blueprint);
        showButton = true;

        if(helmet.equals(item_blueprint)){
            labelF.setText(" "+ "Helmet");
            showButton = true;
        }
        if(pistol.equals(item_blueprint)){
            labelF.setText(" "+ "Beretta");
            showButton = true;
        }
        if(patronBox.equals(item_blueprint)){
            labelF.setText(" "+ "Patron box");
            showButton = true;
        }
        if(medKit.equals(item_blueprint)){
            labelF.setText(" "+ "Med kit");
            showButton = true;
        }
        if(shotgan.equals(item_blueprint)){
            labelF.setText(" "+ "Fabram martial");
            showButton = true;
        }
        if(flashGrenad.equals(item_blueprint)){
            labelF.setText(" "+ "Flash grenad");
            showButton = true;
        }
        if(fragGrenad.equals(item_blueprint)){
            labelF.setText(" "+ "Frag grenad");
            showButton = true;
        }
        if(machinegun.equals(item_blueprint)){
            labelF.setText(" "+ "Fame Saf 2");
            showButton = true;
        }
        if(bazooka.equals(item_blueprint)){
            labelF.setText(" "+ "RPG 7");
            showButton = true;
        }
        if(ammunition.equals(item_blueprint)){
            labelF.setText(" "+ "Ammunition");
            showButton = true;
        }
        if(tommyGun.equals(item_blueprint)){
            labelF.setText(" "+ "MGL");
            showButton = true;
        }

    }


    public void itemInfoPickupRemove()
    {
        showButton = false;
    }

    float scale = 1;
    boolean increase = true;

    @Override
    public void draw(float delta)
    {
        if (stage.getActors().size > 0)
        {
            animateLabel();
        }

        if (showButton)
        {
            stage.act();
            stage.draw();
        }

    }

    private void animateLabel()
    {
        if (increase)
        {
            scale += 0.02f;
        }
        else
        {
            scale -= 0.02f;
        }
        if (scale > 1.4)
        {
            increase = false;
        }
        else if (scale < 1)
        {
            increase = true;
        }
        if (GameClient.getGame().isAndroid())
        {
            labelF.setFontScale(scale);

        }
        else
        {
            labelF.setFontScale(scale);
        }


    }

    public boolean isClickImageButton()
    {
        return clickImageButton;
    }

    public void setClickImageButton(boolean clickImageButton)
    {
        this.clickImageButton = clickImageButton;
    }

    public Stage getStage()
    {
        return stage;
    }

}
