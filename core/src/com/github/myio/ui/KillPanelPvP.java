package com.github.myio.ui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.github.myio.GameClient;
import com.github.myio.entities.PlayerCell;

/**
 * Created by taras on 21.03.2018.
 */

public class KillPanelPvP implements IDrawable
{

    private PlayerCell playerCell;
    private GameClient game;

    private Viewport viewport;
    private Stage stage;
    private Table table;

    private BitmapFont font;

    private final int MSG_SIZE = 5;
    private static long MSG_SHOW_DURATION = 4000;
    private KillLabel[] labels = new KillLabel[MSG_SIZE];
    Skin skin;


    public KillPanelPvP(PlayerCell playerCell, GameClient game, Skin skin)
    {
        this.playerCell = playerCell;
        this.game = game;
        this.skin = skin;


        viewport = game.getUiViewport();
        stage = new Stage(viewport, game.getBatch());
        Table rootTable = new Table();
        rootTable.setFillParent(true);
        table = new Table();

        createList();
        rootTable.add(table).right().top().padTop(Gdx.graphics.getHeight()/6).expand();
        stage.addActor(rootTable);
    }

    @Override
    public void draw(float delta) {

        updateLabes();
        stage.act();
        stage.draw();
    }

    public void addLabel(String k, String v)
    {
        for (KillLabel killLabel : labels)
        {
            if (!killLabel.isUsed)
            {
                killLabel.setUsed(true);
                killLabel.setKiller(k);
                killLabel.setVictim(v);
                killLabel.setStartTime(System.currentTimeMillis());
                if (k.equals(v))
                {
                    killLabel.getTextLabel().setText(v + " COMMITTED SUICIDE");
                    killLabel.getTextLabel().setColor(Color.RED);
                }
                else
                {
                    killLabel.getTextLabel().setText(k + " KILLED " + v);
                    killLabel.getTextLabel().setColor(Color.WHITE);
                }

                break;
            }
        }
    }

    private void updateLabes()
    {
        for (KillLabel killLabel : labels)
        {
            if (killLabel.isUsed)
            {
                if (killLabel.getStartTime() + MSG_SHOW_DURATION < System.currentTimeMillis())
                {
                    killLabel.setUsed(false);
                    killLabel.getTextLabel().setText("");
                    break;
                }
            }
        }
    }


    private void createList()
    {

        for (int i = 0;
             i < MSG_SIZE;
             i++)
        {
            labels[i] = new KillLabel();
            table.add(labels[i].getTextLabel());
            table.row();

        }
    }


    private class KillLabel
    {
        Label textLabel;
        String killer, victim;
        long startTime;
        boolean isUsed;

        public KillLabel() {
            textLabel = new Label("", skin);
            textLabel.setFontScale(1,1);
        }

        public void setTextLabel(Label textLabel)
        {
            this.textLabel = textLabel;
        }

        public void setKiller(String killer)
        {
            this.killer = killer;
        }

        public void setVictim(String victim)
        {
            this.victim = victim;
        }

        public void setStartTime(long startTime)
        {
            this.startTime = startTime;
        }

        public void setUsed(boolean used)
        {
            isUsed = used;
        }

        public Label getTextLabel()
        {
            return textLabel;
        }

        public String getKiller()
        {
            return killer;
        }

        public String getVictim()
        {
            return victim;
        }

        public long getStartTime()
        {
            return startTime;
        }

        public boolean isUsed()
        {
            return isUsed;
        }
    }
}

