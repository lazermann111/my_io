package com.github.myio.ui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Dialog;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.github.myio.GameClient;

import static com.badlogic.gdx.scenes.scene2d.actions.Actions.hide;

/**
 * Created by Дмитрий on 28.02.2018.
 */

public class ExitMenu implements IDrawable {

    private Stage stage;
    private Skin skin;
    private Skin skin2;
    private boolean visible;
    GameClient game;
    InputProcessor originalInput;
    Table rootTable;
    Table exitMenuTable;
    TextButton btnQuiTheGame;
    TextButton btnResumeGame;
    TextButton btnBackToTheMainMenu;
    Label label;
    InputMultiplexer inputMultiplexer;

    Viewport viewport;

    public ExitMenu(final GameClient game, OrthographicCamera camera, Skin skin, InputMultiplexer inputMultiplexer) {
        this.game = game;
        // this.skin=skin;
        viewport = game.getUiViewport();
        this.inputMultiplexer = inputMultiplexer;
        stage = new Stage(game.getUiViewport());
        label = new Label("", skin);

        float DPIWidht = (float) Gdx.graphics.getWidth() / 1280;
        float DPIHeight = (float) Gdx.graphics.getHeight() / 720;
        float DPI = ((DPIWidht + DPIHeight) / 2) * 0.85f;

        skin = new Skin(Gdx.files.internal("atlas/mainMenu/newSkin.json"), new TextureAtlas("atlas/mainMenu/newSkin.atlas"));
        skin.getFont("trebushet").getData().setScale(1.4f);

        btnQuiTheGame = new TextButton("Quit the game", skin);
        btnQuiTheGame.setSize(viewport.getScreenWidth() / 9, viewport.getScreenHeight() / 9);
        btnResumeGame = new TextButton("Resume game", skin);
        btnResumeGame.setSize(viewport.getScreenWidth() / 7, viewport.getScreenHeight() / 11);
        btnBackToTheMainMenu = new TextButton("Main menu", skin);
        btnQuiTheGame.setSize(viewport.getScreenWidth() / 7, viewport.getScreenHeight() / 11);

        btnQuiTheGame.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                game.getNetworkManager().disconnect();
                Gdx.app.exit();
            }
        });

        btnResumeGame.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                //   hide();
                hideDialog();
            }
        });

        btnBackToTheMainMenu.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                game.getNetworkManager().disconnect();
                game.getScreen().dispose();
                game.setScreen(game.getMainMenuScreen());
            }
        });

        exitMenuTable = new Table(skin);
        rootTable = new Table(skin);

        Sprite bckMiddleSp = new Sprite(new Texture(Gdx.files.internal("mainMenu/background.png")));
        Image bckMiddle = new Image(bckMiddleSp);

        exitMenuTable.setBackground(bckMiddle.getDrawable());
        exitMenuTable.add(label);
        exitMenuTable.row();
        exitMenuTable.add(btnQuiTheGame).width(viewport.getScreenWidth() / 5).height(viewport.getScreenHeight() / 9).space(10 * DPI, 10 * DPI, 10 * DPI, 10 * DPI);
        exitMenuTable.row();
        exitMenuTable.add(btnResumeGame).width(viewport.getScreenWidth() / 5).height(viewport.getScreenHeight() / 9).space(10 * DPI, 10 * DPI, 10 * DPI, 10 * DPI);
        exitMenuTable.row();
        exitMenuTable.add(btnBackToTheMainMenu).width(viewport.getScreenWidth() / 5).height(viewport.getScreenHeight() / 9).space(10 * DPI, 10 * DPI, 10 * DPI, 10 * DPI);
        exitMenuTable.row();
        exitMenuTable.add(label);
        exitMenuTable.setSize(viewport.getScreenWidth() / 16 * DPI, viewport.getScreenHeight() / 5 * DPI);

        rootTable.add(exitMenuTable);
        rootTable.setFillParent(true);
//        exitMenuTable.debugAll();
    }

    public void showDialog() {
        originalInput = Gdx.input.getInputProcessor();
        Gdx.input.setInputProcessor(stage);
        stage.addActor(rootTable);
        visible = true;

    }

    public void hideDialog() {
        Gdx.input.setInputProcessor(originalInput);
        stage.clear();
        visible = false;

    }

    public boolean isVisible() {
        return visible;
    }

    public Stage getStage() {
        return stage;
    }

    @Override
    public void draw(float delta) {
        if (stage != null) {
            stage.act(delta);
            stage.draw();
        }


    }
}
