package com.github.myio.ui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.DragAndDrop;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.github.myio.GameClient;
import com.github.myio.StringConstants;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by alex on 15.03.18.
 */

public class EmoteManager implements IDrawable{

    private static final String TAG = "EmoteManager";

    private String[] emotesNames = {"angel", "happy", "sad", "laugh", "pumpkin", "thumb_down", "thumb_up", "tongue", "argentina", "china", "england", "germany", "india", "ireland", "japan", "south_korea", "turkey", "ukraine", "usa"};

    float emotesPad = 10;
    private float tableWidth;
    private float tableHeight;
    float tableX;
    float tableY;
    Stage mStage;
    Table mTable;
    Table rootTabel;
    Skin skin;
    GameClient mGame;
    Table listTable;
    Pixmap labelColor;
    Pixmap listColor;
    Pixmap grayColor;
    List<ImageButton> emotesForDrag;
    List<ImageButton> emotesTarget;
    DragAndDrop dnd;
    Table menu;
    Rectangle rectangle;
    Viewport viewport;
    /*public EmoteManager(GameClient game, boolean dummy)
    {

    }*/
    public EmoteManager(GameClient game, boolean dummy , Viewport viewport) {
        mGame = game;
        skin = new Skin(Gdx.files.internal("atlas/mainMenu/newSkin.json"), new TextureAtlas("atlas/mainMenu/newSkin.atlas"));
        mTable = new Table();
        rootTabel = new Table();
        //rootTabel.debugAll();
        tableWidth = (float) (Gdx.graphics.getWidth() * 0.7);
        tableHeight = (float) (Gdx.graphics.getHeight() * 2);
        tableX = (float)(Gdx.graphics.getWidth() * 0.2);
        tableY = (float)(Gdx.graphics.getHeight() * 0.2);
        mTable.setWidth(tableWidth);
        mTable.setHeight(tableHeight);
        mTable.setBounds(0, 0, tableWidth, tableHeight);
        rectangle = new Rectangle(tableX, tableY, tableWidth, tableHeight);
        Image backEmoteManager = new Image(new Sprite(new Texture("mainMenu/background.png")));


        //mTable.setFillParent(true);
        emotesForDrag = new ArrayList<ImageButton>();
        emotesTarget = new ArrayList<ImageButton>();
        dnd = new DragAndDrop();

        //prepare colors
        labelColor = new Pixmap(1, 1, Pixmap.Format.RGB888);
        labelColor.setColor(Color.FOREST);
        labelColor.fill();
        listColor = new Pixmap(1, 1, Pixmap.Format.RGB888);
        listColor.setColor(Color.OLIVE);
        listColor.fill();
        grayColor = new Pixmap(1, 1, Pixmap.Format.RGB888);
        grayColor.setColor(Color.GRAY);
        grayColor.fill();

        Texture greenTexture = new Texture(labelColor);
        Texture oliveTexture = new Texture(listColor);
        Texture grayTexture = new Texture(grayColor);
        greenTexture.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Nearest);
        oliveTexture.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Nearest);

        Drawable green = new Image(greenTexture).getDrawable();
        Drawable olive = new Image(oliveTexture).getDrawable();
        Drawable gray = new Image(grayTexture).getDrawable();

        mTable.setBackground(backEmoteManager.getDrawable());

        //elements of main table
        Label.LabelStyle style = new Label.LabelStyle(GameClient.getFont32White(), Color.WHITE);
        Label label = new Label ("Make your own emotes set",style);
        label.setAlignment(Align.center);
        TextButton closeButton = new TextButton("CLOSE", skin);
        closeButton.addCaptureListener(new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                menu.setTouchable(Touchable.enabled);
                clearEM();
            }
        });

        listTable=new Table();
        listTable.background(backEmoteManager.getDrawable());
        addButtons(emotesNames);
        //listTable.debug();
        //ImageButton winEmote = createEmote(emotesNames[0]);
        //ImageButton deadEmote = createEmote(emotesNames[5]);
        //emotesTarget.add(winEmote);
        //emotesTarget.add(deadEmote);

        Table resultTable= new Table();
        //resultTable.debug();
        resultTable.background(backEmoteManager.getDrawable());
        //resultTable.add(new Label("Win", skin)).expandX().right().top().row();
        //resultTable.add(winEmote).right().top().row();
        resultTable.add(createDefaultMenu()).padTop(15).center().row();
        //resultTable.add(new Label("Dead", skin)).expandX().right().top().row();
        //resultTable.add(deadEmote).padTop(15).right();
        //greyColor.fillCircle((int) deadEmote.getX(), (int) deadEmote.getY(), (int) deadEmote.getHeight());

        mTable.add(label).colspan(4).fill().pad(5, 0, 5, 0).expandX().row();
        ScrollPane scroll = new ScrollPane(listTable);
        mTable.add(scroll).expand().fill();
        mTable.add().width(25).expandY();
        mTable.add(resultTable).expand().colspan(2).fill().row();
        mTable.add(closeButton).right().height(Gdx.graphics.getHeight()/12).width(Gdx.graphics.getWidth()/6).padBottom(10);
        initDnD();

        labelColor.dispose();
        listColor.dispose();
     //   mTable.debugAll();
        rootTabel.setFillParent(true);
        rootTabel.setSize(viewport.getWorldWidth()/2,viewport.getWorldHeight()/2);

        rootTabel.add(mTable);
    }

    @Override
    public void draw(float delta) {
        mStage.act();
        mStage.draw();

    }

    public ImageButton createEmote(String nameNormal) {

        ImageButton imageButton = new ImageButton(new Image(GameClient.uiAtlas.createSprite(nameNormal + "_tr")).getDrawable(), new Image(GameClient.uiAtlas.createSprite(nameNormal)).getDrawable());
        Button.ButtonStyle style = imageButton.getStyle();
        style.over = new Image(GameClient.uiAtlas.createSprite(nameNormal)).getDrawable();
        style.checkedOver = new Image(GameClient.uiAtlas.createSprite(nameNormal)).getDrawable();
        imageButton.setStyle(style);
        imageButton.setName(nameNormal);
        return imageButton;
    }

    public void addButtons(String[] emoteNames) {
        for (int i = 0; i < emoteNames.length; i++) {
            String n = emoteNames[i];
            ImageButton imgBtn = createEmote(n);
            if((i+1)%4 == 0) {
                listTable.add(imgBtn).pad(emotesPad,emotesPad,emotesPad,emotesPad).row();
            }else {
                listTable.add(imgBtn).pad(emotesPad,emotesPad,emotesPad,emotesPad);
            }
            emotesForDrag.add(imgBtn);
        }
    }

    public void showEM() {
        menu.setTouchable(Touchable.disabled);
        mTable.setPosition((float)(Gdx.graphics.getWidth() * 0.2), (float)(Gdx.graphics.getHeight() * 0.2));
        mStage.addActor(rootTabel);
    }
public Table createDefaultMenu() {
    Table emotesTable = new Table();
    ImageButton emote1 = createEmote(emotesNames[0]);
    ImageButton emote2 = createEmote(emotesNames[1]);
    ImageButton emote3 = createEmote(emotesNames[2]);
    ImageButton emote4 = createEmote(emotesNames[3]);

    emotesTable.add(emote1).colspan(3).align(Align.bottom).row();
    emotesTable.add(emote2);
    emotesTable.add(createEmote("close"));
    emotesTable.add(emote3).row();
    emotesTable.add(emote4).colspan(3).align(Align.top).row();
    emotesTarget.add(emote1);
    emotesTarget.add(emote2);
    emotesTarget.add(emote3);
    emotesTarget.add(emote4);
        return emotesTable;
}

public void initDnD() {
    for (final ImageButton e : emotesForDrag) {
        dnd.addSource(new DragAndDrop.Source(e) {
            @Override
            public DragAndDrop.Payload dragStart(InputEvent event, float x, float y, int pointer) {
                DragAndDrop.Payload payload = new DragAndDrop.Payload();
                if (event != null) {
                    payload.setObject("ooooooo!");
                    ImageButton ibForDrag = createEmote(e.getName());
                    payload.setDragActor(ibForDrag);
                    payload.setValidDragActor(ibForDrag);
                }
                return payload;
            }
        });
    }



    for (ImageButton i : emotesTarget) {
        DragAndDrop.Target target = new DragAndDrop.Target(i) {
            @Override
            public boolean drag(DragAndDrop.Source source, DragAndDrop.Payload payload, float x, float y, int pointer) {
                return true;
            }

            @Override
            public void drop(DragAndDrop.Source source, DragAndDrop.Payload payload, float x, float y, int pointer) {

                ImageButton replaced = (ImageButton) getActor();
                ImageButton addNew = (ImageButton) payload.getDragActor();
                Table table = (Table) replaced.getParent();
                table.getCell(replaced).setActor(addNew);
                updateTargets(addNew);
                emotesTarget.set(emotesTarget.indexOf(replaced), addNew);
            }
        };
        dnd.addTarget(target);
    }
}

    public void updateTargets(ImageButton ib) {
        dnd.addTarget(new DragAndDrop.Target(ib) {
            @Override
            public boolean drag(DragAndDrop.Source source, DragAndDrop.Payload payload, float x, float y, int pointer) {
                return true;
            }

            @Override
            public void drop(DragAndDrop.Source source, DragAndDrop.Payload payload, float x, float y, int pointer) {
                ImageButton replaced = (ImageButton) getActor();
                ImageButton addNew = (ImageButton) payload.getDragActor();
                Table table = (Table) replaced.getParent();
                table.getCell(replaced).setActor(addNew);
                updateTargets(addNew);
                emotesTarget.set(emotesTarget.indexOf(replaced), addNew);
            }
        });

    }

    public ArrayList<String> getEmoteList() {
        ArrayList<String> customEmoteList = new ArrayList<String>();
        for (ImageButton e : emotesTarget) {
            customEmoteList.add(e.getName());
        }
       return customEmoteList;
    }

    public Stage getStage() {
        return mStage;
    }

    public void setStage(final Stage stage) {
        mStage = stage;
        mStage.addCaptureListener(new ClickListener() {
            public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
                //Vector2 vec = stage.screenToStageCoordinates(new Vector2(x, y));
                if (!rectangle.contains(x, y)) {
                    menu.setTouchable(Touchable.enabled);
                    clearEM();
                    return true;
                }
                return false;
            }        });
    }

    public void clearEM() {
        mTable.remove();
    }

    public void setMenu(Table mainMenu) {
        menu = mainMenu;
    }

}
