package com.github.myio.ui;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.ui.ProgressBar;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

public class Health extends ProgressBar {
    int mWidth, mHeight;



    public Health(int width, int height) {
        super(0f, 1f, 0.01f, false, new ProgressBarStyle());
        mWidth = width;
        mHeight = height;
        getStyle().background = getColoredDrawable(width, height, Color.RED);
        getStyle().knob = getColoredDrawable(0, height, Color.GREEN);
        getStyle().knobBefore = getColoredDrawable(width, height, Color.GREEN);
        setWidth(width);
        setHeight(height);
        setAnimateDuration(0.0f);
        setValue(1f);
        setAnimateDuration(0.25f);
    }

    public Drawable getColoredDrawable(int width, int height, Color color) {
        int mWidth = MathUtils.nextPowerOfTwo(width);
        int mHeight = MathUtils.nextPowerOfTwo(height);
        Pixmap pixmap = new Pixmap(mWidth, mHeight, Pixmap.Format.RGBA8888);
        pixmap.setColor(color);
        pixmap.fill();
        Texture texture = new Texture(pixmap);
        texture.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
        TextureRegionDrawable drawable = new TextureRegionDrawable(new TextureRegion(texture));
        pixmap.dispose();
        return drawable;
    }

    public void setColors(Color background, Color knob) {
        getStyle().background = getColoredDrawable(mWidth, mHeight, background);
        getStyle().knob = getColoredDrawable(0, mHeight, knob);
        getStyle().knobBefore = getColoredDrawable(mWidth, mHeight, knob);
    }

}