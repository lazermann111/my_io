package com.github.myio.ui;


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Timer;
import com.github.myio.GameClient;
import com.github.myio.tools.IoLogger;

import java.util.ArrayList;

import static com.github.myio.GameConstants.EMOTE_PRESENTATION_TIME;


public class EmotesMenu implements IDrawable {

    final static String TAG = "EmotesMenu";
    private int tableWidth = 400;
    private int tableHeight = 400;

    Stage mStage;
    Table emotesTable;
    private String checkedEmoteName;
    private String emoteForBroadcast;
    Image emote;




    public EmotesMenu(ArrayList<String> emotesNames)  {

//        IoLogger.log(TAG, emotesNames.toString());

        mStage = new Stage(GameClient.getGame().getUiViewport());

        //Texture backgroundTable = GameClient.getGame().getAssetManager().getTexture("background_table");
        //final Drawable tableBackground = new TextureRegionDrawable(new TextureRegion(backgroundTable));
        //IoLogger.log(TAG, "" + tableBackground.getLeftWidth() + "   "+tableBackground.getBottomHeight());



        // create 4 emotes and close button
       ImageButton emote1 = createEmote(emotesNames.get(0));
        ImageButton emote2 = createEmote(emotesNames.get(1));
        ImageButton emote3 = createEmote(emotesNames.get(2));
        ImageButton emote4 = createEmote(emotesNames.get(3));
        ImageButton emoteClose = createEmote("close");
        //ImageButton emoteWin = createEmote(emotesNames.get(0));
        //ImageButton emoteDead = createEmote(emotesNames.get(1));

        //set Listeners for emotes
        EmoteListener e1 = new EmoteListener(emote1);
        EmoteListener e2 = new EmoteListener(emote2);
        EmoteListener e3 = new EmoteListener(emote3);
        EmoteListener e4 = new EmoteListener(emote4);
        emote1.addListener(e1);
        emote2.addListener(e2);
        emote3.addListener(e3);
        emote4.addListener(e4);




        // create table with emotes
        emotesTable = new Table();
        emotesTable.setWidth(tableWidth);
        emotesTable.setHeight(tableHeight);
        //emotesTable.setBackground(tableBackground);

        emotesTable.add(emote1).colspan(3).align(Align.bottom).row();
        emotesTable.add(emote2);
        emotesTable.add(emoteClose);
        emotesTable.add(emote3);
        emotesTable.row();
        emotesTable.add(emote4).colspan(3).align(Align.top).row();

    }

    public Stage getStage() {
        return mStage;
    }

    public class EmoteListener extends ClickListener {
        ImageButton mButton;

        public EmoteListener(ImageButton imageButton) {
            mButton = imageButton;
        }

        public void enter(InputEvent event, float x, float y, int pointer, Actor fromActor) {
            if(event != null) {
                checkedEmoteName = mButton.getName();
            }


        }

        public void exit(InputEvent event, float x, float y, int pointer, Actor toActor) {
            checkedEmoteName = null;
        }
       }

    @Override
    public void draw(float delta) {
        mStage.act();
        mStage.draw();
    }



    public void showEmotesTable(int positionX, int positionY) {
        emotesTable.setPosition(positionX - emotesTable.getWidth() / 2, (Gdx.graphics.getHeight() - positionY) - emotesTable.getHeight() / 2);
        mStage.addActor(emotesTable);

    }

    public void hide() {
        emotesTable.remove();
        if (checkedEmoteName != null) {
            mStage.clear();
            emoteForBroadcast = checkedEmoteName;
            showSingleEmote(checkedEmoteName);
            }
        }

        public void showSingleEmote(String name) {
            emote = new Image(GameClient.uiAtlas.createSprite(checkedEmoteName));
            emote.setPosition(Gdx.graphics.getWidth() / 2, Gdx.graphics.getHeight() / 2 + emote.getHeight() / 2);
            mStage.addActor(emote);
            if(deleteEmote(emote)) {
                checkedEmoteName = null;
            }
        }


    public boolean deleteEmote(final Image checkedEmote) {
        try {
        Timer.schedule(
                new Timer.Task() {
                    @Override
                    public void run() {
                        checkedEmote.remove();
                    }
                },
                EMOTE_PRESENTATION_TIME);}
            catch (Exception e) {
                e.printStackTrace();
                return false;
            }
        return  true;
        }



    public ImageButton createEmote(String nameNormal) {
        ImageButton imageButton = new ImageButton(new Image(GameClient.uiAtlas.createSprite(nameNormal + "_tr")).getDrawable(), new Image(GameClient.uiAtlas.createSprite(nameNormal)).getDrawable());
        Button.ButtonStyle style = imageButton.getStyle();
        style.over = new Image(GameClient.uiAtlas.createSprite(nameNormal)).getDrawable();
        style.checkedOver = new Image(GameClient.uiAtlas.createSprite(nameNormal)).getDrawable();
        imageButton.setStyle(style);
        imageButton.setName(nameNormal);
        return imageButton;
    }





    public String getEmoteForBroadcast() {
        return emoteForBroadcast;
    }

    public void clearEmoteForBroadcast() {
        emoteForBroadcast = null;
        IoLogger.log(TAG, "" + emoteForBroadcast);

    }



}
