package com.github.myio.ui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.github.myio.GameClient;

/**
 * Debug tool for rendering specific game entities
 */

public class RenderManager implements IDrawable
{

    Label labelPlayer;
    Label labelEnemies;
    Label labelTiles;
    Label labelMap;
    Label labelFood;

    GameClient game;
   // OrthographicCamera camera;
    Stage stage;
    Viewport viewport;
    BitmapFont font;

    public RenderManager(GameClient game, OrthographicCamera camera) {
        this.game = game;
        //this.camera = camera;
        font = GameClient.getFont32Black();


        viewport = game.getUiViewport();
        stage = new Stage(viewport, game.getBatch());
        Table table = new Table();

       // table.bottom();
        table.top();
        table.setFillParent(true);

      //  table.setDebug( true );
       // table.right().bottom();

        Label.LabelStyle labelStyle = new Label.LabelStyle();
        labelStyle.font = font;


       /* labelPlayer = new Label("1 render player : " + renderPlayer , labelStyle);
        labelEnemies = new Label("2 render enemies  " + renderEnemies , labelStyle);
        labelFood = new Label("3 render item : " + renderFood , labelStyle);
        labelMap = new Label("4 render map entities : " + renderMapEntities , labelStyle);
        labelTiles = new Label("5 render tiles : " + renderTiles , labelStyle);


        table.add(labelPlayer).expandX().padTop(10).padLeft(10);
        table.row();

        table.add(labelEnemies).expandX().padTop(10).padLeft(10);
        table.row();

        table.add(labelFood).expandX().padTop(10).padLeft(10);
        table.row();

        table.add(labelMap).expandX().padTop(10).padLeft(10);
        table.row();

        table.add(labelTiles).expandX().padTop(10).padLeft(10);
        table.row();

        stage.addActor(table);*/


    }




    private boolean renderPlayer = true, renderTiles= true, renderEnemies= true, renderUI= true, renderFood= true, renderMapEntities= true, renderAnimals = true;



    public boolean isRenderPlayer() {
        return renderPlayer;
    }

    public boolean isRenderTiles() {
        return renderTiles;
    }

    public boolean isRenderEnemies() {
        return renderEnemies;
    }

    public boolean isRenderItems() {
        return renderFood;
    }

    public boolean isRenderMapEntities() {
        return renderMapEntities;
    }

    public boolean isRenderAnimals() {
        return renderAnimals;
    }

    public boolean isRenderUI() {
        return renderUI;
    }

    public void switchRenderPlayer()
    {
        renderPlayer = !renderPlayer;
    }

    public void switchrenderEnemies()
    {
        renderEnemies = !renderEnemies;
    }

    public void switchrenderFood()
    {
        renderFood = !renderFood;
    }

    public void switchrenderMapEntities()
    {
        renderMapEntities = !renderMapEntities;
    }

    public void switchrenderTiles()
    {
        renderTiles = !renderTiles;
    }


    @Override
    public void draw(float delta) {
        userInput();

       /* labelPlayer.setText("1 render player : " + renderPlayer );
        labelEnemies.setText("2 render enemies  " + renderEnemies );
        labelFood .setText("3 render item : " + renderFood );
        labelMap .setText("4 render map entities : " + renderMapEntities );
        labelTiles.setText("5 render tiles : " + renderTiles );



        stage.act();
        stage.draw();*/
    }

    public void userInput()
    {
        if(Gdx.input.isKeyJustPressed(Input.Keys.NUM_1)) {
            switchRenderPlayer();
            switchrenderEnemies();
        }
        if(Gdx.input.isKeyJustPressed(Input.Keys.NUM_2)) switchRenderUI();
        if(Gdx.input.isKeyJustPressed(Input.Keys.NUM_3)) switchrenderFood();
        if(Gdx.input.isKeyJustPressed(Input.Keys.NUM_4)) switchrenderMapEntities();
        if(Gdx.input.isKeyJustPressed(Input.Keys.NUM_5)) switchrenderTiles();

    }


    private float flashTimer;
    private  boolean flashing  ;
    public static final int FLASH_TIME = 3;


    public void flash()
    {
        flashing = true;
        flashTimer = 0;
    }

    private void switchRenderUI()
    {
        renderUI = !renderUI;
    }

    public void flashEffect(float deltaTime )
    {
        if(!flashing) return;
        flashTimer += deltaTime;
        float alpha = flashTimer/FLASH_TIME;

        /*Gdx.gl.glEnable(GL20.GL_BLEND);
        Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
        GameClient.getGame().getGameShapeRenderer().begin(ShapeRenderer.ShapeType.Filled);
        GameClient.getGame().getGameShapeRenderer().setColor(new Color(0, 0, 0,alpha));
        GameClient.getGame().getGameShapeRenderer().rect(0, 0, 1000, 1000);
        GameClient.getGame().getGameShapeRenderer().end();

        Gdx.gl.glDisable(GL20.GL_BLEND);

*/       Gdx.gl.glEnable(GL20.GL_BLEND);
         Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);

      //  IoLogger.log("RM", alpha+"");
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        Gdx.gl.glClearColor(1,1,1,alpha);
        Gdx.gl.glDisable(GL20.GL_BLEND);
        if(flashTimer >= FLASH_TIME)
            flashing = false;
    }


}
