package com.github.myio.ui;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.github.myio.GameClient;
import com.github.myio.entities.PlayerCell;
import com.github.myio.screens.GameScreen;


/**
 * Created by alex on 23.05.18.
 */

public class Compass implements IDrawable {
    private Stage stage;
    Image compass;
    float centerX, centerY, playerX, playerY;
    PlayerCell player;
    Vector2 center;
    Camera mCamera;





    private Table main;
    private float updateTime = 0.5f;
    int index = 0;
    Image[] arrows;

    public Compass(PlayerCell playerCell) {
        stage = new Stage();
        player = playerCell;
        main = new Table();
        /*Texture textureRed = new Texture(Gdx.files.internal("arrow_red.png"));
        Texture textureYellow = new Texture(Gdx.files.internal("arrow_yellow.png"));
        Texture textureBlue = new Texture(Gdx.files.internal("arrow_blue.png"));
        Texture textureViolet = new Texture(Gdx.files.internal("arrow_violet.png"));
        Texture textureWhite = new Texture(Gdx.files.internal("arrow_white.png"));*/
        //Texture textureCompass = new Texture(Gdx.files.internal("compass.png"));

       // arrowWhite = new Image(textureWhite);
       // arrowRed = new Image(textureRed);
        //compass = new Image(textureCompass);

    }


    public void showCompass() {
        //compass.setPosition(Gdx.graphics.getWidth() / 2 + 50, Gdx.graphics.getHeight() / 2);
        //stage.addActor(compass);







        /*arrowWhite.setRotation(0);
        System.out.println("getRotation  " + arrowWhite.getRotation());
        arrowWhite.setPosition(400, 400);
        arrowWhite.setRotation(getAngle(center));
        getAngle2();
        stage.addActor(arrowWhite);
        stage.addActor(arrowRed);*/
        /*isShown = true;
        while (isShown) {
            changeArrowAlpha();


        }*/
    }

    public void hideCompass() {

    }

    /*private void changeArrowAlpha() {
        arrows[index].addAction(Actions.alpha(1));
        if(index > 0) {
            arrows[index - 1].addAction(Actions.alpha(0.5f));
        }
        index++;
        if(index > 3) {
            index = 0;
        }
    }*/


    @Override
    public void draw(float delta) {
        stage.act();
        stage.draw();
    }

   /*private float getAngle(Vector2 aim) {
        playerX = player.getX();
        playerY = player.getY();
        double distanceToAim = Math.sqrt(Math.pow((aim.x - playerX), 2) + Math.pow((aim.y - playerY), 2));
        double cathetus = Math.abs(aim.y - playerY);
        double cos = Math.cos(cathetus / distanceToAim);
        float angle = (float) Math.toDegrees(cos);

        if(aim.x <= playerX  && aim.y <= playerY) {   //
            angle = 360 - angle;
        }
        if(aim.x < playerX  && aim.y > playerY) {
            angle = 180 + angle;
        }
        if(aim.x >= playerX  && aim.y >= playerY) {
            angle = 180 - angle;
        }
        System.out.println("getAngle(): angle="+angle + " centerX="+aim.x + " centerY="+aim.y + " playerX="+playerX +" playerY="+playerY + "   distanceToAim = "+ distanceToAim + " cathetus=" + cathetus);
        return angle;
    }

    public float getAngle2() {
        playerX = player.getX();
        playerY = player.getY();
        float angle = (new Vector2(playerX, playerY)).sub(center).angle();
        System.out.println("angle2 =" + angle);
        return angle;
    }


    public Vector2 getPosition() {
        float rx, x0, x, ry, y0, y, x1, y1;
        double c, s;
        x = player.getX();
        y = player.getY();
        x0 = x;
        y0 = 
        rx = x0 - x;
        ry = y0 - y;
        c = Math.cos(player.getRotationAngle());
        s = Math.sin(player.getRotationAngle());
        x1 = (float) (x + rx * c - ry * s);
        y1 = (float) (y + rx * s + ry * c);
        System.out.println("x=" + x + "   y="+y + "   compass.x=" + x1 + "     compass.y="+y1);
        return new Vector2(x1, y1);
    }*/

    public Vector3 getCompassPosition() {
        return new Vector3(player.getX(), player.getY(), 0);

    }





}
