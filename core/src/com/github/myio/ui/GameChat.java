package com.github.myio.ui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Dialog;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.github.myio.GameClient;
import com.github.myio.entities.IoPoint;
import com.github.myio.entities.PlayerCell;
import com.github.myio.networking.packet.MessageChatPacket;

import static com.badlogic.gdx.graphics.Color.BLACK;

/**
 * Created by taras on 12.02.2018.
 */
public class GameChat implements IDrawable {

    private GameClient game;
   // private OrthographicCamera camera;
    private Viewport viewport;
    private PlayerCell playerCell;
    private TextField textField;
    private Table table;
    private Stage stage;
    private Label label;
    private Skin skin;
    private String massage;
    private Dialog dialog;
    private IoPoint start;


    public GameChat(GameClient game, PlayerCell playerCell,OrthographicCamera camera) {
        this.game = game;
        this.playerCell = playerCell;
       // this.camera = camera;
        this.start = playerCell.getPosition();

        viewport = game.getUiViewport();
        stage = new Stage();
        Gdx.input.setInputProcessor(stage);

        //skin = new Skin(Gdx.files.internal("MainScreen/ui/menuSkin.json"),new TextureAtlas("MainScreen/ui/atlas.pack"));
        //textField = new TextField(null,skin);

        table = new Table();
        table.center().padBottom(200);
        table.setFillParent(true);
//        table.setSize(200,200);


    }

    public void openChat(){

        textField.getOnscreenKeyboard().show(true);
        Gdx.input.setOnscreenKeyboardVisible(true);
        table.add(textField);
        table.row();
        stage.setKeyboardFocus(textField);
        stage.addActor(table);

    }
    public void closeChat()  {
        massage = textField.getText();
        label = new Label(massage,skin);
        game.getNetworkManager().sendPacket(new MessageChatPacket(start,massage));
        label.setColor(BLACK);
        table.clear();
        table.add(label);
        table.row();
        stage.addActor(table);

    }

    @Override
    public void draw(float delta)
    {
       // game.getBatch().setProjectionMatrix(camera.combined);
      //  camera.update();

       // game.getBatch().begin();
        stage.act();
        stage.draw();
      //  game.getBatch().end();
    }
}
