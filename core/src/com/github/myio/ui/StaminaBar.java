package com.github.myio.ui;

import com.badlogic.gdx.scenes.scene2d.Stage;
import com.github.myio.GameClient;
import com.github.myio.screens.GameScreen;

/**
 * Created by Aleksandr on 26.05.2018.
 */

public class StaminaBar implements IDrawable
{
    Stage stage;
    Stamina stamina;



    public StaminaBar(float maxStamina) {
        float maxWidth = (float) (maxStamina *3.8);
        stage = new Stage();
        stamina = new Stamina(/*380*/(int) maxWidth, 5);
        stamina.setPosition(10, 70);
        stage.addActor(stamina);



        if( GameClient.getGame().isAndroid()){
              maxWidth = (float) (maxStamina *3.0);
            stage = new Stage();
            stamina = new Stamina(/*380*/(int) maxWidth, 25);
            stamina.setPosition(820, 45);
            stage.addActor(stamina);
        }

    }

    @Override
    public void draw(float delta) {
        stage.draw();
        stage.act();

    }

    public void setStamina (float part) {
        stamina.setValue(part);
    }


    public Stamina getStamina() {
        return stamina;
    }

}

