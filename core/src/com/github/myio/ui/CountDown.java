package com.github.myio.ui;

/**
 * Created by alex on 26.03.18.
 */

        import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.github.myio.GameClient;
import com.github.myio.entities.PlayerCell;
import com.github.myio.entities.weapons.AbstractRangeWeapon;
import com.github.myio.networking.packet.UseMedkitPacket;


public  class CountDown implements IDrawable
{
    Stage stage;
    CircleProgressBar circleProgressBar = null;
    private long lastUpdate = 0L;
    private float remainingPercentage = 1.0f;
    private long updateTime = 50L;
    private boolean isFinished = true;
    private float actionTime;//in milliseconds
    private float remainingActionTime;//in milliseconds
    Object target;
    String mTag;

    public CountDown()
    {
        stage = new Stage(GameClient.getGame().getUiViewport());

    }

    public boolean isFinished() {
        return isFinished;
    }

    @Override
    public void draw(float delta) {
        if(circleProgressBar != null) {
           // if (System.currentTimeMillis() - lastUpdate > updateTime) {
                circleProgressBar.update(remainingPercentage);


                //remainingPercentage -= 0.01f;
                remainingActionTime -= (System.currentTimeMillis() - lastUpdate);
                remainingPercentage = remainingActionTime / actionTime;
                lastUpdate = System.currentTimeMillis();


                //if (remainingPercentage <= 0.0f) {
                if (remainingActionTime <= 0.0f)
                {
                    resetCountDown();
                    finalAction(target, mTag);
                }
          //  }
       }

        stage.draw();
        stage.act();
    }

    public void resetCountDown() {
        circleProgressBar.remove();
        remainingPercentage = 1.0f;
        circleProgressBar = null;
        isFinished = true;
    }

    public  boolean showCountDown(Color color, Object object, String tag, float reloadTime) {
        target = object;
        mTag = tag;
        if (target instanceof PlayerCell) {
            ((PlayerCell) target).isUsingInventory = true;
        }
        try {
            isFinished = false;
            //updateTime = (long) seconds * 10;
            this.remainingActionTime = reloadTime;
            this.actionTime = reloadTime;
            circleProgressBar = new CircleProgressBar(true, 5);
            circleProgressBar.setPosition(Gdx.graphics.getWidth() / 2 + 40, Gdx.graphics.getHeight() / 2 + 10);
            circleProgressBar.setSize(50, 50);
            circleProgressBar.setColor(color);
            stage.addActor(circleProgressBar);
            lastUpdate = System.currentTimeMillis();
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public void finalAction(Object object, String tag)
    {
        if (object instanceof AbstractRangeWeapon) {
            //((AbstractRangeWeapon) object).reload(GameClient.getGame().getLocalPlayer().getInventoryManager()); // TODO maybe BUG
        }
        else if (object instanceof PlayerCell)
        {
            PlayerCell playerCell = (PlayerCell) object;
            playerCell.healByMedKit(tag);
            GameClient.getGame().getNetworkManager().sendPacket(new UseMedkitPacket(playerCell.getId(), tag));
            playerCell.isUsingInventory = false;
            playerCell.setAccelerationMultiplier(1);

        }

    }



}