package com.github.myio.ui;

/*
*  Any object that can be drawn on screen
*/
public interface IDrawable
{
     void draw(float delta);

}
