package com.github.myio.ui;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.github.myio.GameClient;
import com.github.myio.GameConstants;
import com.github.myio.entities.IoPoint;
import com.github.myio.entities.PlayerCell;
import com.github.myio.networking.NetworkManager;
import com.github.myio.networking.packet.EmotePacket;
import com.github.myio.networking.packet.VoiceCommandsMenuPacket;
import com.github.myio.screens.SoundTest;
import com.github.myio.tools.SoundManager;

/**
 * Created by Yevhen on 11.06.2018.
 */
public class VoiceCommandsMenu implements IDrawable {


    final static String TAG = "VoiceCommandsMenu";
    private int tableWidth = 800;
    private int tableHeight = 400;
    Game game;
    PlayerCell localPlayer;
    Table table;
    Stage stage;
    Skin skin;
    OrthographicCamera camera;
    IoPoint currentPosition;
    private final String num1 = "num1";
    private final String num2 = "num2";
    private long labelUpdate;

    public VoiceCommandsMenu(GameClient game, PlayerCell localPlayer, Skin skin, OrthographicCamera camera) {
        this.game = game;
        this.localPlayer = localPlayer;
        this.skin = skin;
        stage = new Stage(GameClient.getGame().getUiViewport());
        camera = game.getCamera();
        this.camera = camera;


        Label label = new Label("VOICE COMMANDS\n1 Go-Go-Go\n2 Ok Let's Go",skin);
        label.setFontScale(1.2f);

        table = new Table();
        table.addActor(label);
        table.setSize(tableWidth,tableHeight);
        table.setPosition(10,120);
        //table.setDebug(true);
    }

    public void showVoiceTable() {

        stage.addActor(table);

    }

    public void closeVoiceTable() {
        if (table != null)
        {
            stage.clear();
        }

    }
    public void inputPlayerKeysVoiceCom(NetworkManager networkManager,PlayerCell localPlayer) {

        if (Gdx.input.isKeyPressed(Input.Keys.C)) {
            showVoiceTable();
        }
        else {
            closeVoiceTable();
        }

        if (labelUpdate + GameConstants.EMOTE_PRESENTATION_TIME < System.currentTimeMillis()){

            if (Gdx.input.isKeyJustPressed(Input.Keys.NUM_1)) {
                GameClient.getGame().getSoundManager().playSoundWithPanAndVolume(SoundManager.GO_GO_GO,
                        new IoPoint(camera.position.x, camera.position.y), new IoPoint(localPlayer.x, localPlayer.y), 4);
                networkManager.sendPacket(new VoiceCommandsMenuPacket(localPlayer.getId(), num1, localPlayer.getPosition()));

            } else if (Gdx.input.isKeyJustPressed(Input.Keys.NUM_2)) {
                GameClient.getGame().getSoundManager().playSoundWithPanAndVolume(SoundManager.OKEY_LETS_GO,
                        new IoPoint(camera.position.x, camera.position.y), new IoPoint(localPlayer.x, localPlayer.y), 4);
                networkManager.sendPacket(new VoiceCommandsMenuPacket(localPlayer.getId(), num2, localPlayer.getPosition()));
            }

         labelUpdate = System.currentTimeMillis();
        }

    }




        @Override
    public void draw(float delta) {
     stage.draw();
    }
}