package com.github.myio.ui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.github.myio.GameClient;
import com.github.myio.entities.PlayerCell;
import com.github.myio.tools.IoLogger;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by alex on 10.04.18.
 */

public class SquadInfo implements IDrawable {
    Stage stage;
    private final int MEMBER_HEALTHBAR_WIDTH = 60;
    private final int MEMBER_HEALTHBAR_HEIGHT = 8;

    private final int MEMBER_STAMINABAR_WIDTH = 160;
    private final int MENBER_STAMINABAR_HEIGHT = 10;

    private int offset = 0;
    Table allSquad;
    Skin skin;
    Drawable lightGray;
    List<Image> colorMarks;
    List<Color> colorLines;
    Image scull;
    Color backColor;
    Map<PlayerCell, Color> squadColors;



    public SquadInfo() {

        stage = new Stage();
        skin = new Skin(Gdx.files.internal("atlas/default/skin/uiskin.json"), new TextureAtlas("atlas/default/skin/uiskin.atlas"));

        Image redCircle = new Image(GameClient.uiAtlas.createSprite("circle_red"));
        Image blueCircle = new Image(GameClient.uiAtlas.createSprite("circle_blue"));
        Image yellowCircle = new Image(GameClient.uiAtlas.createSprite("circle_yellow"));
        Image violetCircle = new Image(GameClient.uiAtlas.createSprite("circle_violet"));
        colorMarks = new ArrayList<Image>();
        colorMarks.add(redCircle);
        colorMarks.add(blueCircle);
        colorMarks.add(yellowCircle);
        colorMarks.add(violetCircle);

        colorLines = new ArrayList<Color>();
        colorLines.add(Color.RED);
        colorLines.add(Color.BLUE);
        colorLines.add(Color.YELLOW);
        colorLines.add(Color.VIOLET);

        squadColors = new HashMap();

        backColor = new Color(255,255,255,0);


        allSquad = new Table();
        lightGray = new Image(GameClient.uiAtlas.createSprite("squad_info_background")).getDrawable();
    }


    @Override
    public void draw(float delta) {
        stage.draw();
        stage.act();
    }

    public void setSquadMembers(String[] list, Map<String, PlayerCell> members) {
        for (String id : list) {
            if(id ==  null) break; // end of array
            if (!id.equals(GameClient.getGame().getLocalPlayer().getId()) && allSquad.findActor(id) == null){
                Table memberInfo = new Table();
                memberInfo.setHeight(30);
                int i = Arrays.asList(list).indexOf(id);
                memberInfo.setBackground(lightGray);
                memberInfo.add(new Label(members.get(id).getUsername(), skin)).left().pad(3, 6, 3, 6);
                memberInfo.add(colorMarks.get(i)).right().pad(3, 6, 3, 6).row();
                members.get(id).setSquadColor(colorLines.get(i));
                Health health = new Health(MEMBER_HEALTHBAR_WIDTH, MEMBER_HEALTHBAR_HEIGHT);

                Stamina stamina = new Stamina(MEMBER_STAMINABAR_WIDTH, MENBER_STAMINABAR_HEIGHT);

                backColor = new Color(255,255,255,0);
                health.setColors(backColor, Color.OLIVE);
                health.setName(id);

                stamina.setColors(backColor, Color.LIGHT_GRAY);
                //stamina.setName(id);

                memberInfo.add(health).center().pad(3, 6, 3, 6).colspan(2);

                //memberInfo.add(stamina).center().pad(3, 6,3, 6).colspan(2);
                //memberInfo.add(stamina).center().pad(3,6,3,6).colspan(2);

                allSquad.add(memberInfo).padBottom(10).padRight(10);
                /*scull = new Image(new Texture(Gdx.files.internal("squad_info/scull.png")));
                scull.setName(id+"scull");
                scull.setVisible(false);
                allSquad.add(scull).padBottom(10).row();*/

                offset += (int) memberInfo.getHeight();
        }
    }
       // }
      // allSquad.top().left();
       //allSquad.setFillParent(true);
        float width = (float) (Gdx.graphics.getWidth() * 0.07);
        float height = (float) (Gdx.graphics.getHeight() * 0.9);
        allSquad.setPosition(width, height);
        stage.addActor(allSquad);


    }


    public void setHealth(String id, float part) {
        Health h = allSquad.findActor(id);
        if(h != null) {
            h.setValue(part);
            if(part < 0.3) h.setColors(backColor, Color.RED);
            if(part < 0.6 && part > 0.3) h.setColors(backColor, Color.YELLOW);
            if(part > 0.6) h.setColors(backColor, Color.OLIVE);
        }else {
            System.out.println("health is null");
        }


    }

    public void setStamina(String id, float part)
    {
        Stamina s = allSquad.findActor(id);
        s.setValue(part);
        if(part < 0.3) s.setColors(backColor, Color.BLUE);
        if(part < 0.6 && part > 0.3) s.setColors(backColor, Color.CYAN);
        if(part > 0.6) s.setColors(backColor, Color.MAGENTA);
    }

    public void setDead(String id) {
        Health health = allSquad.findActor(id);
        health.setValue(0);
        health.setColors(backColor, backColor);
        /*Table member = (Table)health.getParent();
        Label.LabelStyle labelStyle = new Label.LabelStyle(new BitmapFont(), Color.RED);
        member.add(new Label("R.I.P.", labelStyle));*/
        /*Image scull = allSquad.findActor(id+"scull");
        scull.setVisible(true);*/
        IoLogger.log("setDead  " + id);

    }

    public void clear() {
        allSquad.clearChildren();
    }

    public Boolean hasInfo(String id) {
        return allSquad.findActor(id) != null;
    }


}