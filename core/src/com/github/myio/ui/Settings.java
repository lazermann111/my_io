package com.github.myio.ui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Array;
import com.github.myio.GameClient;
import com.github.myio.StringConstants;
import com.github.myio.dto.masterserver.GameType;
import com.github.myio.screens.MainMenuScreen;

import java.util.ArrayList;
import java.util.List;

import static com.github.myio.StringConstants.PREFERENCES_SKIN;
import static com.github.myio.StringConstants.SKIN_DEFAULT;

/**
 * Created by alex on 11.05.18.
 */


public class Settings implements IDrawable {

    public static Slider volumeSlider;
    private Stage mStage;
    private Table table;
    private TextButton btnClose;
    Table menu;


    private Skin skin;
    private GameClient game;
    public Image image;




    public Settings(final Skin skin, GameClient game) {
        this.skin = skin;
        this.game = game;

        btnClose = new TextButton("X",skin);
        btnClose.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                table.remove();
                menu.setTouchable(Touchable.enabled);}});

        SelectBox<String> languages = new SelectBox<String>(skin);
        languages.setItems(StringConstants.languagesList);

         volumeSlider = new Slider(0f,1f,0.1f,false,skin);
			volumeSlider.setPosition(30,30);
			volumeSlider.setValue(1f);
			volumeSlider.addListener(new ChangeListener() {
				@Override
				public void changed(ChangeEvent event, Actor actor) {
                    MainMenuScreen.musicMainMenu.setVolume(volumeSlider.getValue());
					GameClient.getGame().getSoundManager().setVolume(volumeSlider.getValue());
				}
			});

       /* Pixmap titleColor = new Pixmap(1, 1, Pixmap.Format.RGB888);
        titleColor.setColor(Color.GRAY);
        titleColor.fill();
        Texture t = new Texture(titleColor);
        t.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
        Drawable titleBackground = new Image(t).getDrawable();

        Label title = new Label("SETTINGS", skin);
        title.getStyle().background = titleBackground;*/

        table = new Table(skin);

       // table.setDebug(true);
        table.top();
        table.setBackground(skin.getDrawable("default-pane-noborder"));
        table.setPosition(Gdx.graphics.getWidth() * 0.7f, Gdx.graphics.getHeight() * 0.05f);
        table.setSize(256, 200);
        table.add("SETTINGS").expandX().padLeft(5).left();
        table.add(btnClose).expandX().right().row();
        table.add(getLine()).colspan(2).pad(2, 0, 7, 0).row();
        table.add("Change language").padBottom(5).padLeft(5).left().row();
        table.add(languages).colspan(2).center().row();
        table.add(getLine()).colspan(2).pad(2, 0, 7, 0).row();
        table.add("Set volume").padBottom(5).padLeft(5).left().row();
        table.add(volumeSlider).row();



    }


    @Override
    public void draw(float delta) {
        mStage.act();
        mStage.draw();

    }

    public void showSettings() {
        menu.setTouchable(Touchable.disabled);
        mStage.addActor(table);
    }




    public void setStage(Stage stage) {mStage = stage;}
    public void setMenu(Table mainMenu) {
        menu = mainMenu;
    }

    public Image getLine() {
        Pixmap pixmap = new Pixmap((int) table.getWidth(), 1, Pixmap.Format.RGBA8888);
        pixmap.setColor(Color.WHITE);
        pixmap.fill();
        Texture texture = new Texture(pixmap);
        texture.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
        Image line = new Image(texture);
        pixmap.dispose();
        return line;
    }


}