package com.github.myio.networking;



public interface ClientPacketHandler {
	
	public abstract boolean handlePacket (Packet packet);
	
}
