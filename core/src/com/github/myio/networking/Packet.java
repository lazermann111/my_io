package com.github.myio.networking;

import com.github.myio.GameClient;

public abstract class Packet   {
	
	public int packetType;
	
	public Packet (int type) {
		this.packetType = type;
	}

    public Packet() {

    }


    public void send () {

		GameClient.getGame().getNetworkManager().sendPacket(this);
	}

}
