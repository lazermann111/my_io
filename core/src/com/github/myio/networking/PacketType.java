package com.github.myio.networking;

@SuppressWarnings({ "unchecked" })
public class PacketType {

    public static final int HEARTBEAT = 0;
    public static final int LOGIN = 1;
    public static final int SUICIDE = 2;
    public static final int WORLD_SNAPSHOT = 3;
    public static final int ENTITY_ADD = 4;
    public static final int ENTITY_REMOVE = 5;
    public static final int ITEM_PICKED = 6;
    public static final int WORLD_UPDATE = 7;

    public static final int POPUP_TEXT = 8;
    public static final int CHAT_MESSAGE = 9;
    public static final int ITEM_DROPPED = 10;
  //  public static final int PLAYER_PICK = 11;
    public static final int PLAYER_LIST = 12;
    public static final int LOGIN_RESPONSE = 13;
    public static final int PROJECTILE_LAUNCHED = 14;
   // public static final int NPC_DIALOG = 14;
   // public static final int NPC_DIALOG_REQUEST = 15;
    /*public static final int FLAGS_ADD = 16;
    public static final int FORM_INTERACT = 17;
    public static final int FORM_REQUEST = 18;
    public static final int FORM_DATA = 19;
    public static final int POSITION_CORRECTION = 20;
    public static final int PLAYER_STATS = 21;*/
    public static final int DEAL_DAMAGE = 22;
    public static final int TILES_SNAPSHOT = 23;

    public static final int EXCEPTION_PACKET = 24;
    public static final int MESSAGE_CHAT_PACKET = 25;
    public static final int WEAPON_SWITCHED = 26;
    public static final int EMOTE_SHOWN = 27;
    public static final int PVP_INFO_PACKET = 28;
    public static final int TIMER_GAME = 29;
    public static final int SHIELD_INTERACTION = 30;
    public static final int GAME_WIN = 31;
    public static final int SERVER_STATE_INFO = 32;
    public static final int PIT_INTERACTION = 33;
    public static final int SQUAD_CHANGED = 34;
    public static final int USE_MEDKIT = 35;
    public static final int VISIBILITY_CHANGE = 36;
    public static final int TEAM_LOGIN_PACKET = 37;

    public static final int CURRENT_WORLD_SNAPSHOT = 38;
    public static final int CURRENT_WORLD_SNAPSHOT_REQUEST = 39;
    public static final int SKIN_PLAYER = 40;
    public static final int HEARTBEAT_DISABLE = 41;
    public static final int PLAYER_DIED = 42;
    public static final int PLAYER_STATS = 43;
    public static final int PLAYER_REVIVING_REQUEST = 44;

    public static final int SERVER_ERROR = 47;    // todo check 2 equal codes
    public static final int CAPTURABLE_POINT_STATE_CHANGED = 45;
    public static final int CAPTURABLE_POINTS_SNAPSHOT = 46;
    public static final int VOICE_COMMAND_SHOW = 47;
    public static final int USE_AMMO = 48;

}
