package com.github.myio.networking.packet;

import com.github.czyzby.websocket.serialization.SerializationException;
import com.github.czyzby.websocket.serialization.Transferable;
import com.github.czyzby.websocket.serialization.impl.Deserializer;
import com.github.czyzby.websocket.serialization.impl.Serializer;
import com.github.myio.networking.Packet;

import static com.github.myio.networking.PacketType.GAME_WIN;

/**
 * Created by Igor on 3/29/2018.
 */

public class WinPacket extends Packet implements Transferable<WinPacket> {


    public String winnerId;

    public WinPacket() {
        super(GAME_WIN);
    }

    public WinPacket(String winnerId) {
        super(GAME_WIN);
        this.winnerId = winnerId;
    }

    @Override
    public void serialize(Serializer serializer) throws SerializationException {
            serializer.serializeString(winnerId);
    }

    @Override
    public WinPacket deserialize(Deserializer deserializer) throws SerializationException {
        return new WinPacket(deserializer.deserializeString());
    }
}
