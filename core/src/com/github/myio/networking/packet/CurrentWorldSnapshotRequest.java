package com.github.myio.networking.packet;

import com.github.czyzby.websocket.serialization.SerializationException;
import com.github.czyzby.websocket.serialization.Transferable;
import com.github.czyzby.websocket.serialization.impl.Deserializer;
import com.github.czyzby.websocket.serialization.impl.Serializer;
import com.github.myio.networking.Packet;

import static com.github.myio.networking.PacketType.CURRENT_WORLD_SNAPSHOT_REQUEST;

/**
 * Used to request server for current world snapshot, normally shouldn't be used :D
 */

public class CurrentWorldSnapshotRequest extends Packet implements Transferable<CurrentWorldSnapshotRequest>{

        public CurrentWorldSnapshotRequest()
        {
            super(CURRENT_WORLD_SNAPSHOT_REQUEST);
        }

    @Override
    public void serialize(Serializer serializer) throws SerializationException {

    }

    @Override
    public CurrentWorldSnapshotRequest deserialize(Deserializer deserializer) throws SerializationException {
        return new CurrentWorldSnapshotRequest();
    }
}
