package com.github.myio.networking.packet;


import com.github.czyzby.websocket.serialization.SerializationException;
import com.github.czyzby.websocket.serialization.Transferable;
import com.github.czyzby.websocket.serialization.impl.Deserializer;
import com.github.czyzby.websocket.serialization.impl.Serializer;
import com.github.myio.networking.*;

public class EntityRemovePacket extends Packet implements Transferable<EntityRemovePacket> {
	
	public String entityId;
	
	public EntityRemovePacket () {
		super(PacketType.ENTITY_REMOVE);
	}

	public EntityRemovePacket(String entityId) {
		super(PacketType.ENTITY_REMOVE);
		this.entityId=entityId;
	}

	@Override
	public void serialize(Serializer serializer) throws SerializationException {
	serializer.serializeString(entityId);
	}

	@Override
	public EntityRemovePacket deserialize(Deserializer deserializer) throws SerializationException {
		return new EntityRemovePacket(deserializer.deserializeString());
	}
}
