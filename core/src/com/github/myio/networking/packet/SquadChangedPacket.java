package com.github.myio.networking.packet;

import com.github.czyzby.websocket.serialization.SerializationException;
import com.github.czyzby.websocket.serialization.Transferable;
import com.github.czyzby.websocket.serialization.impl.Deserializer;
import com.github.czyzby.websocket.serialization.impl.Serializer;
import com.github.myio.networking.Packet;

import static com.github.myio.networking.PacketType.SQUAD_CHANGED;



public class SquadChangedPacket extends Packet implements Transferable<SquadChangedPacket> {

    public int squadId;
    public  String [] membersId;
    public String leaderId;
    public boolean formed;

    public SquadChangedPacket(int squadId, String [] membersId, String leaderId, boolean formed) {
        super(SQUAD_CHANGED);
        this.squadId = squadId;
        this.membersId = membersId;
        this.leaderId = leaderId;
        this.formed = formed;
    }

    public SquadChangedPacket() {
    }



    @Override
    public void serialize(Serializer serializer) throws SerializationException {
        serializer.serializeInt(squadId).
        serializeStringArray(membersId).
        serializeString(leaderId).
        serializeBoolean(formed);
    }

    @Override
    public SquadChangedPacket deserialize(Deserializer deserializer) throws SerializationException {

        int id = deserializer.deserializeInt();

        deserializer.deserializeStringArray(_members);
        return new SquadChangedPacket(id,
                _members  , deserializer.deserializeString(), deserializer.deserializeBoolean());
    }

    private static String[] _members = new String[10];
}
