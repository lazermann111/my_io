package com.github.myio.networking.packet;

import com.github.czyzby.websocket.serialization.SerializationException;
import com.github.czyzby.websocket.serialization.Transferable;
import com.github.czyzby.websocket.serialization.impl.Deserializer;
import com.github.czyzby.websocket.serialization.impl.Serializer;
import com.github.myio.networking.Packet;

import static com.github.myio.networking.PacketType.USE_MEDKIT;

/**
 * Created by alex on 04.04.18.
 */



public class UseMedkitPacket extends Packet implements Transferable<UseMedkitPacket> {

    public String mPlayerId;
    public String medkitBlueprint;

    public UseMedkitPacket() {
        super(USE_MEDKIT);
    }


    public UseMedkitPacket(String playerId, String currentHealth) {
        super(USE_MEDKIT);
        mPlayerId = playerId;
        medkitBlueprint = currentHealth;
    }

    @Override
    public void serialize(Serializer serializer) throws SerializationException {
        serializer.serializeString(mPlayerId).serializeString(medkitBlueprint);
    }

    @Override
    public UseMedkitPacket deserialize(Deserializer deserializer) throws SerializationException {
        return new UseMedkitPacket(deserializer.deserializeString(),deserializer.deserializeString());
    }
}
