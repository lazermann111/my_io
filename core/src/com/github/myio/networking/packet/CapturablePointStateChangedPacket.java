package com.github.myio.networking.packet;

import com.github.czyzby.websocket.serialization.SerializationException;
import com.github.czyzby.websocket.serialization.Transferable;
import com.github.czyzby.websocket.serialization.impl.Deserializer;
import com.github.czyzby.websocket.serialization.impl.Serializer;
import com.github.myio.enums.CapturablePointState;
import com.github.myio.networking.Packet;

import static com.github.myio.networking.PacketType.CAPTURABLE_POINT_STATE_CHANGED;


public class CapturablePointStateChangedPacket extends Packet implements Transferable<CapturablePointStateChangedPacket>
{
    public String pointId;
    public CapturablePointState newPointState;



    public CapturablePointStateChangedPacket()
    {
        super(CAPTURABLE_POINT_STATE_CHANGED);
    }

    public CapturablePointStateChangedPacket(CapturablePointState newPointState, String pointId)
    {
        super(CAPTURABLE_POINT_STATE_CHANGED);
        this.pointId = pointId;
        this.newPointState = newPointState;

    }

    @Override
    public void serialize(Serializer serializer) throws SerializationException
    {
        serializer.serializeEnum(newPointState).serializeString(pointId);
    }

    @Override
    public CapturablePointStateChangedPacket deserialize(Deserializer deserializer) throws SerializationException
    {
        return new CapturablePointStateChangedPacket(deserializer.deserializeEnum(CapturablePointState.values()), deserializer.deserializeString());
    }
}
