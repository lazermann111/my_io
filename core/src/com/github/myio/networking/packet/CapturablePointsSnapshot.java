package com.github.myio.networking.packet;

import com.github.czyzby.websocket.serialization.SerializationException;
import com.github.czyzby.websocket.serialization.Transferable;
import com.github.czyzby.websocket.serialization.impl.Deserializer;
import com.github.czyzby.websocket.serialization.impl.Serializer;
import com.github.myio.dto.CapturablePointDto;
import com.github.myio.networking.Packet;

import static com.github.myio.GameConstants.CONQUEST_POINTS_NUMBER;
import static com.github.myio.networking.PacketType.CAPTURABLE_POINTS_SNAPSHOT;


public class CapturablePointsSnapshot extends Packet implements Transferable<CapturablePointsSnapshot>
{


    public CapturablePointDto[] points  = new CapturablePointDto[CONQUEST_POINTS_NUMBER];
    public int redTeamPoints, blueTeamPoints;

    public CapturablePointsSnapshot()
    {
        super(CAPTURABLE_POINTS_SNAPSHOT);

    }


    public CapturablePointsSnapshot(CapturablePointDto[] points, int redTeamPoints, int blueTeamPoints)
    {
        super(CAPTURABLE_POINTS_SNAPSHOT);
        this.points = points;
        this.redTeamPoints = redTeamPoints;
        this.blueTeamPoints = blueTeamPoints;
    }

    @Override
    public void serialize(Serializer serializer) throws SerializationException
    {
            serializer
                    .serializeTransferableArrayWithPossibleNulls(points)
                    .serializeInt(redTeamPoints)
                    .serializeInt(blueTeamPoints);
    }

    @Override
    public CapturablePointsSnapshot deserialize(Deserializer deserializer) throws SerializationException
    {
        deserializer.deserializeTransferableArrayWithPossibleNulls(points, _p);

        return new CapturablePointsSnapshot(points, deserializer.deserializeInt(),deserializer.deserializeInt());
    }


    private static CapturablePointDto _p = new CapturablePointDto();
}
