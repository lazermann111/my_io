package com.github.myio.networking.packet;


import com.github.czyzby.websocket.serialization.SerializationException;
import com.github.czyzby.websocket.serialization.Transferable;
import com.github.czyzby.websocket.serialization.impl.Deserializer;
import com.github.czyzby.websocket.serialization.impl.Serializer;
import com.github.myio.networking.*;

public class PlayerListPacket extends Packet implements Transferable<PlayerListPacket> {
	
	public static final int RESULT_SUCCESSFUL = 0;
	public static final int RESULT_INVALID_LOGIN = 1;

	public String[] names;

	public String email;
	public int result = 0;
	
	public PlayerListPacket () {
		super(PacketType.PLAYER_LIST);
	}
	
	public PlayerListPacket (String email) {
		this();
		this.email = email;
	}

	public PlayerListPacket( String[] names, String email, int result) {
		super(PacketType.PLAYER_LIST);
		this.names=names;
		this.email=email;
		this.result=result;
	}

	@Override
	public void serialize(Serializer serializer) throws SerializationException {
		serializer.serializeStringArray(names)
				.serializeString(email)
				.serializeInt(result);
	}

	@Override
	public PlayerListPacket deserialize(Deserializer deserializer) throws SerializationException {
		return new PlayerListPacket(deserializer.deserializeStringArray(),
				deserializer.deserializeString(),
				deserializer.deserializeInt());
	}
}
