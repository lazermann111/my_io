package com.github.myio.networking.packet;


import com.github.czyzby.websocket.serialization.SerializationException;
import com.github.czyzby.websocket.serialization.Transferable;
import com.github.czyzby.websocket.serialization.impl.Deserializer;
import com.github.czyzby.websocket.serialization.impl.Serializer;
import com.github.myio.dto.AnimalCellDto;
import com.github.myio.dto.ItemDto;
import com.github.myio.dto.MapEntityDto;
import com.github.myio.dto.PlayerCellDto;
import com.github.myio.networking.Packet;
import com.github.myio.networking.PacketType;

public class InitialWorldSnapshotPacket extends Packet implements Transferable<InitialWorldSnapshotPacket> {

	public PlayerCellDto[] players;
	public ItemDto[] items;
	public MapEntityDto[] entities;
	public AnimalCellDto[] animals;

	static PlayerCellDto psDto = new PlayerCellDto();
	static ItemDto itemDto = new ItemDto();
	static MapEntityDto meDto = new MapEntityDto();
    static AnimalCellDto animal = new AnimalCellDto();


    public InitialWorldSnapshotPacket() {
		super(PacketType.WORLD_SNAPSHOT);
		players = new PlayerCellDto[1000];
		items = new ItemDto[10000];
		entities = new MapEntityDto[10000];
		animals = new AnimalCellDto[10000];
	}

	public InitialWorldSnapshotPacket(PlayerCellDto[] players,ItemDto[] items,MapEntityDto[] entities, AnimalCellDto[] animals) {
		super(PacketType.WORLD_SNAPSHOT);
		this.players = players;
		this.items = items;
		this.entities = entities;
		this.animals = animals;
	}


	@Override
	public String toString() {
		return "InitialWorldSnapshotPacket{" +
				"players=" + players.length +
				", item=" + items.length +
				", entities=" + entities.length +
				", animals=" + animals.length +
				"} ";
	}

	@Override
	public void serialize(Serializer serializer) throws SerializationException {
	serializer.serializeTransferableArrayWithPossibleNulls(players).
			serializeTransferableArrayWithPossibleNulls(items).
			serializeTransferableArrayWithPossibleNulls(entities).
			serializeTransferableArrayWithPossibleNulls(animals);
			//serializeInt(timerGame);
	}

	@Override
	public InitialWorldSnapshotPacket deserialize(Deserializer deserializer) throws SerializationException {

		deserializer.deserializeTransferableArrayWithPossibleNulls(players,psDto);
		deserializer.deserializeTransferableArrayWithPossibleNulls(items,itemDto);
		deserializer.deserializeTransferableArrayWithPossibleNulls(entities,meDto);
		deserializer.deserializeTransferableArrayWithPossibleNulls(animals,animal);

		return new InitialWorldSnapshotPacket(players, items, entities, animals);
	}
}
