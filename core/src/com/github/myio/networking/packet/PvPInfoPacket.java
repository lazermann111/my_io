package com.github.myio.networking.packet;

import com.github.czyzby.websocket.serialization.SerializationException;
import com.github.czyzby.websocket.serialization.Transferable;
import com.github.czyzby.websocket.serialization.impl.Deserializer;
import com.github.czyzby.websocket.serialization.impl.Serializer;
import com.github.myio.networking.Packet;
import com.github.myio.networking.PacketType;

/**
 * Created by taras on 21.03.2018.
 */

public class PvPInfoPacket extends Packet implements Transferable<PvPInfoPacket> {

    public String killer;
    public String victim;

    public PvPInfoPacket() {
        super(PacketType.PVP_INFO_PACKET);
    }

    public PvPInfoPacket(String killer, String victim) {
        super(PacketType.PVP_INFO_PACKET);
        this.killer = killer;
        this.victim = victim;
    }


    @Override
    public void serialize(Serializer serializer) throws SerializationException {
        serializer.serializeString(killer).serializeString(victim);
    }

    @Override
    public PvPInfoPacket deserialize(Deserializer deserializer) throws SerializationException {
        return new PvPInfoPacket(deserializer.deserializeString(),deserializer.deserializeString());
    }
}
