package com.github.myio.networking.packet;

import com.github.czyzby.websocket.serialization.SerializationException;
import com.github.czyzby.websocket.serialization.Transferable;
import com.github.czyzby.websocket.serialization.impl.Deserializer;
import com.github.czyzby.websocket.serialization.impl.Serializer;
import com.github.myio.networking.Packet;
import com.github.myio.networking.PacketType;


public class ItemDroppedPacket extends Packet implements Transferable<ItemDroppedPacket>
{

    public String itemId, playerId;

    //todo fix it
    public int totalAmmoAmount, clipAmmoAmount;

    public ItemDroppedPacket(String playerID, String itemId, int   totalAmmoAmount ,int clipAmmoAmount )
    {
        super(PacketType.ITEM_DROPPED);
        this.itemId = itemId;
        this.playerId = playerID;
        this.totalAmmoAmount = totalAmmoAmount;
        this.clipAmmoAmount = clipAmmoAmount;
    }

    public ItemDroppedPacket()
    {
        super(PacketType.ITEM_DROPPED);
    }


    @Override
    public void serialize(Serializer serializer) throws SerializationException
    {
        serializer.serializeString(playerId).serializeString(itemId).serializeInt(totalAmmoAmount).serializeInt(clipAmmoAmount);
    }

    @Override
    public ItemDroppedPacket deserialize(Deserializer deserializer) throws SerializationException
    {
        return new ItemDroppedPacket(deserializer.deserializeString(), deserializer.deserializeString(), deserializer.deserializeInt(),deserializer.deserializeInt());
    }
}


