package com.github.myio.networking.packet;

import com.github.czyzby.websocket.serialization.SerializationException;
import com.github.czyzby.websocket.serialization.Transferable;
import com.github.czyzby.websocket.serialization.impl.Deserializer;
import com.github.czyzby.websocket.serialization.impl.Serializer;
import com.github.myio.networking.Packet;

import static com.github.myio.networking.PacketType.HEARTBEAT_DISABLE;


public class HeartBeatDisablePacket extends Packet implements Transferable<HeartBeatDisablePacket> {

    public HeartBeatDisablePacket() {
         super(HEARTBEAT_DISABLE);
    }

    @Override
    public void serialize(Serializer serializer) throws SerializationException {

    }

    @Override
    public HeartBeatDisablePacket deserialize(Deserializer deserializer) throws SerializationException {
        return new HeartBeatDisablePacket();
    }
}
