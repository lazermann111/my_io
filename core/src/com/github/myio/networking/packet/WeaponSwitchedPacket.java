package com.github.myio.networking.packet;

import com.github.czyzby.websocket.serialization.SerializationException;
import com.github.czyzby.websocket.serialization.Transferable;
import com.github.czyzby.websocket.serialization.impl.Deserializer;
import com.github.czyzby.websocket.serialization.impl.Serializer;
import com.github.myio.enums.WeaponType;
import com.github.myio.networking.Packet;

import static com.github.myio.networking.PacketType.WEAPON_SWITCHED;


public class WeaponSwitchedPacket extends Packet implements Transferable<WeaponSwitchedPacket> {

    public String playerId;
    public int weaponIdx; //index of weapon in player weaponList, to disctint weapons in case of similar ones
    public WeaponType typeToSwitchTo;

    public WeaponSwitchedPacket() {
        super(WEAPON_SWITCHED);
    }


    public WeaponSwitchedPacket(String playerId, WeaponType typeToSwitchTo, int weaponIdx) {
        super(WEAPON_SWITCHED);
        this.playerId = playerId;
        this.typeToSwitchTo = typeToSwitchTo;
        this.weaponIdx = weaponIdx;
    }

    @Override
    public void serialize(Serializer serializer) throws SerializationException {
        serializer.serializeString(playerId).serializeEnum(typeToSwitchTo).serializeInt(weaponIdx);
    }

    @Override
    public WeaponSwitchedPacket deserialize(Deserializer deserializer) throws SerializationException {
        return new WeaponSwitchedPacket(deserializer.deserializeString(),deserializer.deserializeEnum(WeaponType.values()), deserializer.deserializeInt());
    }
}
