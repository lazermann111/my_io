package com.github.myio.networking.packet;

import com.github.czyzby.websocket.serialization.SerializationException;
import com.github.czyzby.websocket.serialization.Transferable;
import com.github.czyzby.websocket.serialization.impl.Deserializer;
import com.github.czyzby.websocket.serialization.impl.Serializer;
import com.github.myio.networking.Packet;
import com.github.myio.networking.PacketType;

/**
 * Created by dell on 03.04.2018.
 */

public class PitInteractionPacker extends Packet implements Transferable<PitInteractionPacker> {
    public String playerId;
    public boolean insidePit;
    public String pitId;

    public PitInteractionPacker() {
        super(PacketType.PIT_INTERACTION);
    }

    public PitInteractionPacker(String playerId, boolean insidePit, String pitId) {
        super(PacketType.PIT_INTERACTION);
        this.playerId = playerId;
        this.insidePit = insidePit;
        this.pitId = pitId;
    }

    @Override
    public void serialize(Serializer serializer) throws SerializationException {
        serializer
                .serializeString(playerId)
                .serializeBoolean(insidePit)
                .serializeString(pitId);
    }

    @Override
    public PitInteractionPacker deserialize(Deserializer deserializer) throws SerializationException {
        return new PitInteractionPacker(deserializer.deserializeString(), deserializer.deserializeBoolean(), deserializer.deserializeString());
    }
}
