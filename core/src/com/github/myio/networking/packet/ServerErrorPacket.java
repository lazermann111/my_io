package com.github.myio.networking.packet;

import com.github.czyzby.websocket.serialization.SerializationException;
import com.github.czyzby.websocket.serialization.Transferable;
import com.github.czyzby.websocket.serialization.impl.Deserializer;
import com.github.czyzby.websocket.serialization.impl.Serializer;
import com.github.myio.networking.Packet;
import com.github.myio.networking.PacketType;

/**
 * Created by dell on 16.05.2018.
 */

public class ServerErrorPacket extends Packet  implements Transferable<ServerErrorPacket> {
    public String playerId;
    public String error;

    public ServerErrorPacket() {
        super(PacketType.SERVER_ERROR);
    }

    public ServerErrorPacket(String playerId, String error) {
        super(PacketType.SERVER_ERROR);
        this.playerId = playerId;
        this.error = error;
    }

    @Override
    public void serialize(Serializer serializer) throws SerializationException {
        serializer.serializeString(playerId).serializeString(error);
    }

    @Override
    public ServerErrorPacket deserialize(Deserializer deserializer) throws SerializationException {
        return new ServerErrorPacket(deserializer.deserializeString(), deserializer.deserializeString());
    }
}
