package com.github.myio.networking.packet;

import com.github.czyzby.websocket.serialization.SerializationException;
import com.github.czyzby.websocket.serialization.Transferable;
import com.github.czyzby.websocket.serialization.impl.Deserializer;
import com.github.czyzby.websocket.serialization.impl.Serializer;
import com.github.myio.networking.Packet;
import com.github.myio.networking.PacketType;

/**
 * Sent player his emptyItem and initial position, maybe something additional
 */

public class LoginResponsePacket extends Packet implements Transferable<LoginResponsePacket>
{

    public String playerId ;
    public String name;
    public String playerSkinCollor;
    public float x,y ;
    public int maxHealth, currentHealth, level;
    public int currentXp;
    public int worldId;

    public LoginResponsePacket() {
        super(PacketType.LOGIN_RESPONSE);
    }


    public LoginResponsePacket(String playerId, String name, float x, float y, int maxHealth, int currentHealth, int level, int worldId, int currentXp,String playerSkinCollor) {
        super(PacketType.LOGIN_RESPONSE);
        this.playerId = playerId;
        this.name = name;
        this.playerSkinCollor = playerSkinCollor;
        this.x = x;
        this.y = y;
        this.maxHealth = maxHealth;
        this.currentHealth = currentHealth;
        this.level = level;
        this.worldId = worldId;
        this.currentXp = currentXp;
    }

    @Override
    public String toString() {
        return "LoginResponsePacket{" +
                "entityId='" + playerId + '\'' +
                ", name='" + name + '\'' +
                ", x=" + x +
                ", y=" + y +
                "} " + super.toString();
    }

    @Override
    public void serialize(Serializer serializer) throws SerializationException {
        serializer.serializeString(playerId)
                .serializeString(name)
                .serializeFloat(x)
                .serializeFloat(y)
                .serializeInt(maxHealth)
                .serializeInt(currentHealth)
                .serializeInt(level)
                .serializeInt(worldId)
                .serializeInt(currentXp)
                .serializeString(playerSkinCollor);
    }

    @Override
    public LoginResponsePacket deserialize(Deserializer deserializer) throws SerializationException {
        return new LoginResponsePacket(

                deserializer.deserializeString(),
                deserializer.deserializeString(),
                deserializer.deserializeFloat(),
                deserializer.deserializeFloat(),
                deserializer.deserializeInt(),
                deserializer.deserializeInt(),
                deserializer.deserializeInt(),
                deserializer.deserializeInt(),
                deserializer.deserializeInt(),
                deserializer.deserializeString());
    }
}
