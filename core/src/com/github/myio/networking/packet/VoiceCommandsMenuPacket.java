package com.github.myio.networking.packet;

/**
 * Created by Yevhen on 20.06.2018.
 */

import com.github.czyzby.websocket.serialization.SerializationException;
import com.github.czyzby.websocket.serialization.Transferable;
import com.github.czyzby.websocket.serialization.impl.Deserializer;
import com.github.czyzby.websocket.serialization.impl.Serializer;
import com.github.myio.entities.Cell;
import com.github.myio.entities.IoPoint;
import com.github.myio.networking.Packet;
import com.github.myio.networking.PacketType;
public class VoiceCommandsMenuPacket extends Packet implements Transferable<VoiceCommandsMenuPacket> {

    public IoPoint mOrigin;//WAT IN HIR PLACE?
    public String pVoiceComandName;
    public String playerId;

    public VoiceCommandsMenuPacket(String playerId, String voiceComandName, IoPoint origin) {
        super(PacketType.VOICE_COMMAND_SHOW);
        this.playerId = playerId;
        this.mOrigin = origin;
        this.pVoiceComandName = voiceComandName;
    }

    public VoiceCommandsMenuPacket() {
        super(PacketType.EMOTE_SHOWN);
    }

    @Override
    public void serialize(Serializer serializer) throws SerializationException {
        serializer.serializeString(playerId).serializeString(pVoiceComandName).serializeTransferable(mOrigin);
    }

    @Override
    public VoiceCommandsMenuPacket deserialize(Deserializer deserializer) throws SerializationException {
        return new VoiceCommandsMenuPacket(deserializer.deserializeString(),deserializer.deserializeString(),deserializer.deserializeTransferable(_p));
    }
    protected static IoPoint _p = new IoPoint();


}

