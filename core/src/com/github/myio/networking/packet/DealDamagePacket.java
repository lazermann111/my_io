package com.github.myio.networking.packet;

import com.github.czyzby.websocket.serialization.SerializationException;
import com.github.czyzby.websocket.serialization.Transferable;
import com.github.czyzby.websocket.serialization.impl.Deserializer;
import com.github.czyzby.websocket.serialization.impl.Serializer;
import com.github.myio.networking.Packet;
import com.github.myio.networking.PacketType;

/**
 * Created by dell on 20.12.2017.
 */

public class DealDamagePacket extends Packet implements Transferable<DealDamagePacket> {
    public String localPlayerId, enemyId;

    public DealDamagePacket( String localPlayerId, String enemyId) {
        super(PacketType.DEAL_DAMAGE);
        this.localPlayerId = localPlayerId;
        this.enemyId = enemyId;
    }

    public DealDamagePacket() {
        super(PacketType.DEAL_DAMAGE);
    }

    @Override
    public void serialize(Serializer serializer) throws SerializationException {
        serializer.serializeString(localPlayerId).serializeString(enemyId);
    }

    @Override
    public DealDamagePacket deserialize(Deserializer deserializer) throws SerializationException {
        return new DealDamagePacket(deserializer.deserializeString(),deserializer.deserializeString());
    }
}
