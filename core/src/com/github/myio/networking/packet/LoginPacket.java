package com.github.myio.networking.packet;


import com.github.czyzby.websocket.serialization.SerializationException;
import com.github.czyzby.websocket.serialization.Transferable;
import com.github.czyzby.websocket.serialization.impl.Deserializer;
import com.github.czyzby.websocket.serialization.impl.Serializer;
import com.github.myio.networking.Packet;
import com.github.myio.networking.PacketType;

public class LoginPacket extends Packet implements Transferable<LoginPacket> {

    public String namePlayer; // maybe we will have db with players meanwhile
    public String skinPlayerColor;
    public String clientId;


    public LoginPacket() {
        super(PacketType.LOGIN);
    }

    public LoginPacket (String namePlayer, String skinPlayerColor, String clientId) {

        super(PacketType.LOGIN);
        this.namePlayer = namePlayer;
        this.skinPlayerColor = skinPlayerColor;
        this.clientId = clientId;

    }

    @Override
    public void serialize(Serializer serializer) throws SerializationException {
        serializer
                .serializeString(namePlayer)
                .serializeString(skinPlayerColor)
                .serializeString(clientId);
    }

    @Override
    public LoginPacket deserialize(Deserializer deserializer) throws SerializationException {
        return new LoginPacket(deserializer.deserializeString(), deserializer.deserializeString(),
                deserializer.deserializeString());
    }
}
