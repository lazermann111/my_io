package com.github.myio.networking.packet;

/**
 * Created by alex on 28.06.18.
 */

import com.github.czyzby.websocket.serialization.SerializationException;
import com.github.czyzby.websocket.serialization.Transferable;
import com.github.czyzby.websocket.serialization.impl.Deserializer;
import com.github.czyzby.websocket.serialization.impl.Serializer;
import com.github.myio.networking.Packet;

import static com.github.myio.networking.PacketType.USE_AMMO;





public class UseAmmoPacket extends Packet implements Transferable<UseAmmoPacket> {

    public String mPlayerId;
    public int mPatron;
    public String mWeaponType;

    public UseAmmoPacket() {
        super(USE_AMMO);
    }


    public UseAmmoPacket(String playerId, String weaponType, int patron) {
        super(USE_AMMO);
        mPlayerId = playerId;
        mPatron = patron;
        mWeaponType = weaponType;
    }

    @Override
    public void serialize(Serializer serializer) throws SerializationException {
        serializer.serializeString(mPlayerId).serializeString(mWeaponType).serializeInt(mPatron);
    }

    @Override
    public UseAmmoPacket deserialize(Deserializer deserializer) throws SerializationException {
        return new UseAmmoPacket(deserializer.deserializeString(),deserializer.deserializeString(),deserializer.deserializeInt());
    }
}

