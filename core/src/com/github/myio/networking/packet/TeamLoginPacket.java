package com.github.myio.networking.packet;

import com.github.czyzby.websocket.serialization.SerializationException;
import com.github.czyzby.websocket.serialization.Transferable;
import com.github.czyzby.websocket.serialization.impl.Deserializer;
import com.github.czyzby.websocket.serialization.impl.Serializer;
import com.github.myio.dto.masterserver.GameType;
import com.github.myio.networking.Packet;

import static com.github.myio.networking.PacketType.TEAM_LOGIN_PACKET;

/**
 * Used specifically for team matchmaking, contains info that helps LoadBalancer to get whole team in 1 squad at 1 server
 */

public class TeamLoginPacket extends Packet implements Transferable<TeamLoginPacket> {

    public boolean isTeamLeader;
    public GameType gameType;
    public int teamSize;
    public String teamId;
    public String skinPlayerColor;
    public boolean autoFillEnabled;

    public String playerName;
    public String clientId;

    public TeamLoginPacket() {
        super(TEAM_LOGIN_PACKET);
    }


    public TeamLoginPacket(boolean isTeamLeader, GameType gameType, int teamSize, String teamId, boolean autoFillEnabled, String playerName,String clientId ,String skinPlayerColor) {
        super(TEAM_LOGIN_PACKET);
        this.isTeamLeader = isTeamLeader;
        this.gameType = gameType;
        this.teamSize = teamSize;
        this.skinPlayerColor = skinPlayerColor;
        this.teamId = teamId;
        this.autoFillEnabled = autoFillEnabled;
        this.playerName = playerName;
        this.clientId = clientId;
    }

    @Override
    public void serialize(Serializer serializer) throws SerializationException {
      serializer
              .serializeBoolean(isTeamLeader)
              .serializeEnum(gameType)
              .serializeInt(teamSize)
              .serializeString(teamId)
              .serializeBoolean(autoFillEnabled)
              .serializeString(playerName)
              .serializeString(clientId)
              .serializeString(skinPlayerColor);

    }

    @Override
    public TeamLoginPacket deserialize(Deserializer deserializer) throws SerializationException {
        return new TeamLoginPacket(
                deserializer.deserializeBoolean(),
                deserializer.deserializeEnum(GameType.values()),
                deserializer.deserializeInt(),
                deserializer.deserializeString(),
                deserializer.deserializeBoolean(),
                deserializer.deserializeString(),
                deserializer.deserializeString(),
                deserializer.deserializeString()

        );
    }
}
