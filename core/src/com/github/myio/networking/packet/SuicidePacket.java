package com.github.myio.networking.packet;


import com.github.czyzby.websocket.serialization.SerializationException;
import com.github.czyzby.websocket.serialization.Transferable;
import com.github.czyzby.websocket.serialization.impl.Deserializer;
import com.github.czyzby.websocket.serialization.impl.Serializer;
import com.github.myio.networking.Packet;
import com.github.myio.networking.PacketType;

public class SuicidePacket extends Packet implements Transferable<SuicidePacket>
{


	
	public String playerId ;

	public SuicidePacket()
	{
		super(PacketType.SUICIDE);
	}

	public SuicidePacket( String plId) {
		super(PacketType.SUICIDE);
		playerId = plId;
	}

	public SuicidePacket(int reason, int i) {
	}

	@Override
	public void serialize(Serializer serializer) throws SerializationException {
		serializer.serializeString(playerId);
	}

	@Override
	public SuicidePacket deserialize(Deserializer deserializer) throws SerializationException {
		return new SuicidePacket(deserializer.deserializeString());
	}
}
