package com.github.myio.networking.packet;

import com.github.czyzby.websocket.serialization.SerializationException;
import com.github.czyzby.websocket.serialization.Transferable;
import com.github.czyzby.websocket.serialization.impl.Deserializer;
import com.github.czyzby.websocket.serialization.impl.Serializer;
import com.github.myio.networking.Packet;

import static com.github.myio.networking.PacketType.PLAYER_REVIVING_REQUEST;


/**
 * Created by dell on 20.04.2018.
 */

public class PlayerRevivingRequest extends Packet implements Transferable<PlayerRevivingRequest> {
    public String mPlayerId;
    public boolean isSuppressed;

    public PlayerRevivingRequest()
    {
        super(PLAYER_REVIVING_REQUEST);
    }

    public PlayerRevivingRequest(String mPlayerId, boolean isSuppressed)
    {
        super(PLAYER_REVIVING_REQUEST);
        this.mPlayerId = mPlayerId;
        this.isSuppressed = isSuppressed;
    }

    @Override
    public void serialize(Serializer serializer) throws SerializationException
    {
        serializer
                .serializeString(mPlayerId)
                .serializeBoolean(isSuppressed);

    }

    @Override
    public PlayerRevivingRequest deserialize(Deserializer deserializer) throws SerializationException {
        return new PlayerRevivingRequest(deserializer.deserializeString(), deserializer.deserializeBoolean());
    }
}
