package com.github.myio.networking.packet;


import com.github.czyzby.websocket.serialization.SerializationException;
import com.github.czyzby.websocket.serialization.Transferable;
import com.github.czyzby.websocket.serialization.impl.Deserializer;
import com.github.czyzby.websocket.serialization.impl.Serializer;
import com.github.myio.networking.Packet;
import com.github.myio.networking.PacketType;

public class ExceptionPacket extends Packet implements Transferable<ExceptionPacket> {

    public String exceptionPlace,getException;

    public ExceptionPacket(int type) {
        super(PacketType.EXCEPTION_PACKET);
    }

    public ExceptionPacket (String exceptionPlace,String getException){
        super(PacketType.EXCEPTION_PACKET);

        this.exceptionPlace = exceptionPlace;
        this.getException = getException;
    }

    @Override
    public void serialize(Serializer serializer) throws SerializationException {
        serializer.serializeString(exceptionPlace).serializeString(getException);
    }

    @Override
    public ExceptionPacket deserialize(Deserializer deserializer) throws SerializationException {
        return new ExceptionPacket(deserializer.deserializeString(),deserializer.deserializeString());
    }
}
