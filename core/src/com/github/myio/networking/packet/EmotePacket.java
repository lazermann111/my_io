package com.github.myio.networking.packet;

import com.github.czyzby.websocket.serialization.SerializationException;
import com.github.czyzby.websocket.serialization.Transferable;
import com.github.czyzby.websocket.serialization.impl.Deserializer;
import com.github.czyzby.websocket.serialization.impl.Serializer;
import com.github.myio.entities.IoPoint;
import com.github.myio.networking.Packet;
import com.github.myio.networking.PacketType;

/**
 * Created by alex on 10.03.18.
 */

public class EmotePacket extends Packet implements Transferable<EmotePacket> {
    String mPlayerId;
    String mEmoteName;
    IoPoint mOrigin;

    public EmotePacket(String playerId, String emoteName, IoPoint origin) {
        super(PacketType.EMOTE_SHOWN);
        mPlayerId = playerId;
        mEmoteName = emoteName;
        mOrigin = origin;
    }

    public EmotePacket() {
        super(PacketType.EMOTE_SHOWN);
    }

    public String getPlayerId() {
        return mPlayerId;
    }

    public String getEmoteName() {
        return mEmoteName;
    }

    public IoPoint getOrigin() {
        return mOrigin;
    }

    @Override
    public void serialize(Serializer serializer) throws SerializationException {
        serializer.serializeString(mPlayerId).serializeString(mEmoteName).serializeTransferable(mOrigin);
    }

    @Override
    public EmotePacket deserialize(Deserializer deserializer) throws SerializationException {
        return new EmotePacket(deserializer.deserializeString()
                ,deserializer.deserializeString()
                ,deserializer.deserializeTransferable(_p));
    }


    protected static IoPoint _p = new IoPoint();

}
