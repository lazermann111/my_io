package com.github.myio.networking.packet;

import com.github.czyzby.websocket.serialization.SerializationException;
import com.github.czyzby.websocket.serialization.Transferable;
import com.github.czyzby.websocket.serialization.impl.Deserializer;
import com.github.czyzby.websocket.serialization.impl.Serializer;
import com.github.myio.entities.IoPoint;
import com.github.myio.enums.ProjectileType;
import com.github.myio.networking.Packet;

import static com.github.myio.networking.PacketType.PROJECTILE_LAUNCHED;


public class ProjectileLaunchedPacket extends Packet implements Transferable<ProjectileLaunchedPacket> {

    public float launchedTime; // milliseconds from server start, maybe we need it for sync

    // All this stuff can be changed in Projectile by abilities/debaffs
    public String shooterId;
    public IoPoint start;
    public IoPoint end;
    public float speed;
    public float scatter;
    public int damage;
  //  public int weaponIdx;
    public ProjectileType type;
    public int maxDistance;
    public float rotation;

    protected static IoPoint _p = new IoPoint();

    public ProjectileLaunchedPacket() {
        super(PROJECTILE_LAUNCHED);
    }


    public ProjectileLaunchedPacket(IoPoint start, IoPoint end, float speed, int damage, int dist, ProjectileType t, String shooterId, float scatter) {
        super(PROJECTILE_LAUNCHED);
        this.shooterId = shooterId;
        this.start = start;
        this.end = end;
        this.speed = speed;
        this.damage = damage;
        this.scatter = scatter;
       // this.weaponIdx = weaponIdx;
        maxDistance = dist;
        type =t;
    }

    public String toString(){
        String s ="type="+type.name()+ "|shooterID="+shooterId+"|start="+start+"|end="+end+"|speed="+speed+"|damag="+damage+"|scatter="+scatter+"|maxDistance="+maxDistance;

        return s;

    }

    @Override
    public void serialize(Serializer serializer) throws SerializationException {
    serializer.
        serializeTransferable(start).
        serializeTransferable(end).
        serializeFloat(speed).
        serializeInt(damage).
        serializeInt(maxDistance).
        serializeEnum(type).
        serializeString(shooterId).
        serializeFloat(scatter);
      //  serializeInt(weaponIdx);
    }

    @Override
    public ProjectileLaunchedPacket deserialize(Deserializer deserializer) throws SerializationException {
        return new ProjectileLaunchedPacket(
                deserializer.deserializeTransferable(_p),
                deserializer.deserializeTransferable(_p),
                deserializer.deserializeFloat(),
                deserializer.deserializeInt(),
                deserializer.deserializeInt(),
                deserializer.deserializeEnum(ProjectileType.values()),
                deserializer.deserializeString(),
                deserializer.deserializeFloat()
               // deserializer.deserializeInt()
                );

    }



}
