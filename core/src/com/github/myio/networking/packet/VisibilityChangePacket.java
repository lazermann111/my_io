package com.github.myio.networking.packet;

import com.github.czyzby.websocket.serialization.SerializationException;
import com.github.czyzby.websocket.serialization.Transferable;
import com.github.czyzby.websocket.serialization.impl.Deserializer;
import com.github.czyzby.websocket.serialization.impl.Serializer;
import com.github.myio.enums.MapEntityType;
import com.github.myio.networking.Packet;
import com.github.myio.networking.PacketType;

/**
 * Created by dell on 06.04.2018.
 */

public class VisibilityChangePacket extends Packet implements Transferable<VisibilityChangePacket> {
    public MapEntityType type;
    public String playerId;
    public boolean insideEntity;

    public VisibilityChangePacket() {
        super(PacketType.VISIBILITY_CHANGE);
    }

    public VisibilityChangePacket(MapEntityType type, String playerId, boolean insideEntity) {
        super(PacketType.VISIBILITY_CHANGE);
        this.playerId = playerId;
        this.type = type;
        this.insideEntity = insideEntity;
    }
    @Override
    public void serialize(Serializer serializer) throws SerializationException {
        serializer
                .serializeEnum(type)
                .serializeString(playerId)
                .serializeBoolean(insideEntity);
    }

    @Override
    public VisibilityChangePacket deserialize(Deserializer deserializer) throws SerializationException {
        return new VisibilityChangePacket(deserializer.deserializeEnum(MapEntityType.values()), deserializer.deserializeString(), deserializer.deserializeBoolean());
    }
}
