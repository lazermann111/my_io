package com.github.myio.networking.packet;


import com.github.czyzby.websocket.serialization.SerializationException;
import com.github.czyzby.websocket.serialization.Transferable;
import com.github.czyzby.websocket.serialization.impl.Deserializer;
import com.github.czyzby.websocket.serialization.impl.Serializer;
import com.github.myio.networking.*;

public class EntityAddPacket extends Packet implements Transferable<EntityAddPacket> {

	//public EntitySnapshot snapshot;
	
	public EntityAddPacket () {
		super(PacketType.ENTITY_ADD);
	}

	@Override
	public void serialize(Serializer serializer) throws SerializationException {

	}

	@Override
	public EntityAddPacket deserialize(Deserializer deserializer) throws SerializationException {
		return null;
	}
}
