package com.github.myio.networking.packet;

import com.github.czyzby.websocket.serialization.SerializationException;
import com.github.czyzby.websocket.serialization.impl.Deserializer;
import com.github.czyzby.websocket.serialization.impl.Serializer;
import com.github.myio.entities.IoPoint;
import com.github.myio.enums.ProjectileType;

/**
 * Encapsulates many shots with different scatter angles
 */

public class ShotgunShotPacket extends ProjectileLaunchedPacket {

    public float[] scatterAngles;

    public ShotgunShotPacket(IoPoint start, IoPoint end, float speed, int damage, int dist, ProjectileType t, String shooterId,  float[] scatter) {
        super(start, end, speed, damage, dist, t, shooterId, 0);
        scatterAngles = scatter;
    }

    public ShotgunShotPacket() {
    }

    @Override
    public void serialize(Serializer serializer) throws SerializationException {
        serializer.
                serializeTransferable(start).
                serializeTransferable(end).
                serializeFloat(speed).
                serializeInt(damage).
                serializeInt(maxDistance).
                serializeEnum(type).
                serializeString(shooterId).

        serializeFloatArray(scatterAngles);
    }

    @Override
    public ShotgunShotPacket deserialize(Deserializer deserializer) throws SerializationException {
        return new ShotgunShotPacket(
                deserializer.deserializeTransferable(_p),
                deserializer.deserializeTransferable(_p),
                deserializer.deserializeFloat(),
                deserializer.deserializeInt(),
                deserializer.deserializeInt(),
                deserializer.deserializeEnum(ProjectileType.values()),
                deserializer.deserializeString(),
                deserializer.deserializeFloatArray()
        );
    }
}
