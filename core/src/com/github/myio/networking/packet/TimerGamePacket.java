package com.github.myio.networking.packet;

import com.github.czyzby.websocket.serialization.SerializationException;
import com.github.czyzby.websocket.serialization.Transferable;
import com.github.czyzby.websocket.serialization.impl.Deserializer;
import com.github.czyzby.websocket.serialization.impl.Serializer;
import com.github.myio.networking.Packet;
import com.github.myio.networking.PacketType;

/**
 * Created by taras on 23.03.2018.
 */

public class TimerGamePacket extends Packet implements Transferable<TimerGamePacket> {

    public int timer;
    public int zoneDuration[];

    public TimerGamePacket() {
        super(PacketType.TIMER_GAME);
    }

    public TimerGamePacket(int timer, int zoneDuration[] ) {
        super(PacketType.TIMER_GAME);
        this.timer = timer;
        this.zoneDuration = zoneDuration;
    }

    @Override
    public void serialize(Serializer serializer) throws SerializationException {
        serializer.serializeInt(timer).serializeIntArray(zoneDuration);
    }

    @Override
    public TimerGamePacket deserialize(Deserializer deserializer) throws SerializationException {
        return new TimerGamePacket(deserializer.deserializeInt(),deserializer.deserializeIntArray());
    }
}
