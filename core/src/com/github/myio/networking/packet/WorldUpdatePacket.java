package com.github.myio.networking.packet;


import com.github.czyzby.websocket.serialization.SerializationException;
import com.github.czyzby.websocket.serialization.Transferable;
import com.github.czyzby.websocket.serialization.impl.Deserializer;
import com.github.czyzby.websocket.serialization.impl.Serializer;
import com.github.myio.dto.ItemDto;
import com.github.myio.dto.MapEntityBaseDto;
import com.github.myio.dto.PlayerCellDto;
import com.github.myio.dto.masterserver.PlayerCellBaseDto;
import com.github.myio.networking.Packet;
import com.github.myio.networking.PacketType;

import static com.github.myio.GameConstants.MAX_PLAYERS_IN_INSTANCE;
import static com.github.myio.networking.PacketType.WORLD_UPDATE;


public class WorldUpdatePacket extends Packet implements Transferable<WorldUpdatePacket> {

    public PlayerCellBaseDto[] playersData;
    public ItemDto[] droppedItems;
    public String[] pickedItems;
    public PlayerCellDto[] joinedPlayers;
    public String[] leftPlayers;
    public MapEntityBaseDto[] changedEntitiesData;

    static PlayerCellBaseDto emptyBasePlayer = new PlayerCellBaseDto();
    static PlayerCellDto emptyPlayer = new PlayerCellDto();
    static MapEntityBaseDto emptyEntity  = new MapEntityBaseDto();
    static ItemDto emptyItem = new ItemDto();
//    static PlayerCellDto joinPl = new PlayerCellDto();

    public WorldUpdatePacket(int type) {
        super(type);
    }
    public WorldUpdatePacket() {
        super(PacketType.WORLD_UPDATE);
    }




    public WorldUpdatePacket(PlayerCellBaseDto[] playersData, ItemDto[] droppedItems, String[] pickedItems, PlayerCellDto[] joinedPlayers, String[] leftPlayers, MapEntityBaseDto[] changedEntitiesData) {
        super(WORLD_UPDATE);
        this.playersData = playersData;
        this.droppedItems = droppedItems;
        this.pickedItems = pickedItems;
        this.joinedPlayers = joinedPlayers;
        this.leftPlayers = leftPlayers;
        this.changedEntitiesData = changedEntitiesData;
    }

    @Override
    public String toString() {
        return "WorldUpdatePacket{" +
                "playersData=" + playersData.length +
                ", droppedItems=" + droppedItems.length +
                ", pickedItems=" + pickedItems.length +
                ", joinedPlayers=" + joinedPlayers.length +
                ", leftPlayers=" + leftPlayers.length +
                "} ";
    }

    @Override
    public void serialize(Serializer serializer) throws SerializationException {
        serializer.serializeTransferableArrayWithPossibleNulls(playersData).
                  serializeTransferableArrayWithPossibleNulls(droppedItems)
                .serializeStringArray(pickedItems)
                .serializeTransferableArrayWithPossibleNulls(joinedPlayers)
                .serializeStringArray(leftPlayers)
                .serializeTransferableArrayWithPossibleNulls(changedEntitiesData);
    }

    @Override
    public WorldUpdatePacket deserialize(Deserializer deserializer) throws SerializationException {

        deserializer.deserializeTransferableArrayWithPossibleNulls(_playersData, emptyBasePlayer);
        deserializer.deserializeTransferableArrayWithPossibleNulls(_spawnedItems, emptyItem);
        deserializer.deserializeStringArray(_pickedItems);
        deserializer.deserializeTransferableArrayWithPossibleNulls(_joinedPlayers, emptyPlayer);
        deserializer.deserializeStringArray(_leftPlayers);
        deserializer.deserializeTransferableArrayWithPossibleNulls(_changedEntities, emptyEntity);
        return new WorldUpdatePacket(_playersData,_spawnedItems,_pickedItems,_joinedPlayers,_leftPlayers, _changedEntities);
    }

    private static PlayerCellBaseDto[] _playersData = new PlayerCellBaseDto[MAX_PLAYERS_IN_INSTANCE];
    private static MapEntityBaseDto[] _changedEntities = new MapEntityBaseDto[1000];
    private static ItemDto[] _spawnedItems =new  ItemDto[1000];
    private static String[] _pickedItems = new String[100];
    private static PlayerCellDto[] _joinedPlayers = new PlayerCellDto[MAX_PLAYERS_IN_INSTANCE];
    private static String[] _leftPlayers = new String[MAX_PLAYERS_IN_INSTANCE];

}
