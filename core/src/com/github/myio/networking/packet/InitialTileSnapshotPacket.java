package com.github.myio.networking.packet;

import com.github.czyzby.websocket.serialization.SerializationException;
import com.github.czyzby.websocket.serialization.Transferable;
import com.github.czyzby.websocket.serialization.impl.Deserializer;
import com.github.czyzby.websocket.serialization.impl.Serializer;
import com.github.myio.networking.Packet;

import static com.github.myio.networking.PacketType.TILES_SNAPSHOT;


public class InitialTileSnapshotPacket extends Packet implements Transferable<InitialTileSnapshotPacket> {
    public String tiles = "";

    public InitialTileSnapshotPacket()
    {
        super(TILES_SNAPSHOT);
    }

    public InitialTileSnapshotPacket(String s) {
        super(TILES_SNAPSHOT);
        tiles = s;
    }



    @Override
    public String toString() {
        return "InitialTileSnapshotPacket{" +
                "} " + super.toString();
    }

    @Override
    public void serialize(Serializer serializer) throws SerializationException {
        serializer.serializeString(tiles);
    }

    @Override
    public InitialTileSnapshotPacket deserialize(Deserializer deserializer) throws SerializationException {
        return new InitialTileSnapshotPacket(deserializer.deserializeString());
    }
}
