package com.github.myio.networking.packet;

import com.github.czyzby.websocket.serialization.SerializationException;
import com.github.czyzby.websocket.serialization.Transferable;
import com.github.czyzby.websocket.serialization.impl.Deserializer;
import com.github.czyzby.websocket.serialization.impl.Serializer;
import com.github.myio.networking.Packet;
import com.github.myio.networking.PacketType;

/**
 * Created by Igor on 12/18/2017.
 */

public class  ItemPickedPacket extends Packet implements Transferable<ItemPickedPacket> {

    public String itemId, playerId;

    public ItemPickedPacket(String itemId, String playerID) {
        super(PacketType.ITEM_PICKED);
        this.itemId = itemId;
        this.playerId = playerID;
    }

    public ItemPickedPacket() {
        super(PacketType.ITEM_PICKED);
    }


    @Override
    public void serialize(Serializer serializer) throws SerializationException {
        serializer.serializeString(itemId).serializeString(playerId);
    }

    @Override
    public ItemPickedPacket deserialize(Deserializer deserializer) throws SerializationException {
        return new ItemPickedPacket(deserializer.deserializeString(),deserializer.deserializeString());
    }
}
