package com.github.myio.networking.packet;

import com.github.czyzby.websocket.serialization.SerializationException;
import com.github.czyzby.websocket.serialization.Transferable;
import com.github.czyzby.websocket.serialization.impl.Deserializer;
import com.github.czyzby.websocket.serialization.impl.Serializer;
import com.github.myio.dto.PlayerSessionStatsDto;
import com.github.myio.networking.Packet;

import static com.github.myio.networking.PacketType.PLAYER_STATS;


public class PlayerStatsPacket extends Packet implements Transferable<PlayerStatsPacket> {

    public String playerId;
    public PlayerSessionStatsDto statsDto;
    private static PlayerSessionStatsDto _p = new PlayerSessionStatsDto();

    public PlayerStatsPacket(PlayerSessionStatsDto statsDto ,String playerId) {
        super(PLAYER_STATS);
        this.statsDto = statsDto;
        this.playerId = playerId;
    }

    public PlayerStatsPacket(){}

    @Override
    public void serialize(Serializer serializer) throws SerializationException {
        serializer.serializeTransferable(statsDto).serializeString(playerId);
    }

    @Override
    public PlayerStatsPacket deserialize(Deserializer deserializer) throws SerializationException {
        return new PlayerStatsPacket(deserializer.deserializeTransferable(_p), deserializer.deserializeString());
    }
}
