package com.github.myio.networking.packet;


import com.github.czyzby.websocket.serialization.SerializationException;
import com.github.czyzby.websocket.serialization.Transferable;
import com.github.czyzby.websocket.serialization.impl.Deserializer;
import com.github.czyzby.websocket.serialization.impl.Serializer;
import com.github.myio.networking.Packet;
import com.github.myio.networking.PacketType;

public class PlayerDiedPacket extends Packet implements Transferable<PlayerDiedPacket> {

	public String playerId;

	public PlayerDiedPacket() {
		super(PacketType.PLAYER_DIED);
	}

	public PlayerDiedPacket(String playerId) {
		super(PacketType.PLAYER_DIED);
		this.playerId = playerId;
	}

	@Override
	public void serialize(Serializer serializer) throws SerializationException {
	serializer.serializeString(playerId);
	}

	@Override
	public PlayerDiedPacket deserialize(Deserializer deserializer) throws SerializationException {
		return new PlayerDiedPacket(deserializer.deserializeString());
	}
}
