package com.github.myio.networking.packet;

import com.github.czyzby.websocket.serialization.SerializationException;
import com.github.czyzby.websocket.serialization.Transferable;
import com.github.czyzby.websocket.serialization.impl.Deserializer;
import com.github.czyzby.websocket.serialization.impl.Serializer;
import com.github.myio.dto.masterserver.GameType;
import com.github.myio.enums.GameServerState;
import com.github.myio.networking.Packet;

import static com.github.myio.networking.PacketType.SERVER_STATE_INFO;

/**
 * Created by Igor and Tarasik on 3/29/2018.
 */

public class GameStateChangedPacket extends Packet implements Transferable<GameStateChangedPacket> {


    public GameServerState gameServerState;
    public GameType gameType;

    public GameStateChangedPacket() {

        super(SERVER_STATE_INFO);
    }

    public GameStateChangedPacket(GameServerState gameServerState, GameType gameType) {
        super(SERVER_STATE_INFO);
        this.gameServerState = gameServerState;
        this.gameType = gameType;
    }

    @Override
    public void serialize(Serializer serializer) throws SerializationException {
            serializer.serializeEnum(gameServerState);
            serializer.serializeEnum(gameType);
    }

    @Override
    public GameStateChangedPacket deserialize(Deserializer deserializer) throws SerializationException {
        return new GameStateChangedPacket(deserializer.deserializeEnum(GameServerState.values()), deserializer.deserializeEnum(GameType.values()));
    }
}
