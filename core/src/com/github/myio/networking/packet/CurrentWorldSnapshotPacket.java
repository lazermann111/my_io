package com.github.myio.networking.packet;

import com.github.czyzby.websocket.serialization.SerializationException;
import com.github.czyzby.websocket.serialization.Transferable;
import com.github.czyzby.websocket.serialization.impl.Deserializer;
import com.github.czyzby.websocket.serialization.impl.Serializer;
import com.github.myio.dto.AnimalCellDto;
import com.github.myio.dto.ItemDto;
import com.github.myio.dto.MapEntityDto;
import com.github.myio.dto.PlayerCellDto;
import com.github.myio.networking.Packet;

import static com.github.myio.networking.PacketType.CURRENT_WORLD_SNAPSHOT;

/**
 * Used for requesting actual state of players, entities, items for player
 * Need to request server for it if there are some desync between client and server detected,
 * i.e. server sent us update on player which we dont have in EntityManager
 *
 * Has exactly same fields as {@link InitialWorldSnapshotPacket}, the difference is that
 * we requesting it on demand
 *
 */

public class CurrentWorldSnapshotPacket extends Packet implements Transferable<CurrentWorldSnapshotPacket> {

    public PlayerCellDto[] players;
    public ItemDto[] items;
    public MapEntityDto[] entities;
    public AnimalCellDto[] animals;

    static PlayerCellDto psDto = new PlayerCellDto();
    static ItemDto itemDto = new ItemDto();
    static MapEntityDto meDto = new MapEntityDto();
    static AnimalCellDto animal = new AnimalCellDto();

    public CurrentWorldSnapshotPacket() {
        super(CURRENT_WORLD_SNAPSHOT);
        players = new PlayerCellDto[1000];
        items = new ItemDto[10000];
        entities = new MapEntityDto[10000];
        animals = new AnimalCellDto[500];
    }


    public CurrentWorldSnapshotPacket(PlayerCellDto[] players, ItemDto[] items, MapEntityDto[] entities, AnimalCellDto[] animals) {
        super(CURRENT_WORLD_SNAPSHOT);
        this.players = players;
        this.items = items;
        this.entities = entities;
        this.animals = animals;

    }

    @Override
    public void serialize(Serializer serializer) throws SerializationException {
        serializer.serializeTransferableArrayWithPossibleNulls(players).
                serializeTransferableArrayWithPossibleNulls(items).
                serializeTransferableArrayWithPossibleNulls(entities).
                serializeTransferableArrayWithPossibleNulls(animals);
        //serializeInt(timerGame);
    }

    @Override
    public CurrentWorldSnapshotPacket deserialize(Deserializer deserializer) throws SerializationException {

        deserializer.deserializeTransferableArrayWithPossibleNulls(players,psDto);
        deserializer.deserializeTransferableArrayWithPossibleNulls(items,itemDto);
        deserializer.deserializeTransferableArrayWithPossibleNulls(entities,meDto);
        deserializer.deserializeTransferableArrayWithPossibleNulls(animals,animal);

        return new CurrentWorldSnapshotPacket(players, items, entities, animals);
    }


    @Override
    public String toString() {
        return "CurrentWorldSnapshotPacket{" +
                "players=" + players.length +
                ", item=" + items.length +
                ", entities=" + entities.length +
                ", animals=" + animals.length +
                "} ";
    }
}
