package com.github.myio.networking.packet;


import com.github.czyzby.websocket.serialization.SerializationException;
import com.github.czyzby.websocket.serialization.Transferable;
import com.github.czyzby.websocket.serialization.impl.Deserializer;
import com.github.czyzby.websocket.serialization.impl.Serializer;
import com.github.myio.networking.Packet;
import com.github.myio.networking.PacketType;

   /**
    * Sent from client to server for updating entity position on server
    */

public class HeartBeatPacket  extends Packet implements Transferable<HeartBeatPacket>
{
    public static final int PLAYER =0;
    public static final int TACTICAL_SHIELD =1;

    public int type;
    public String entityId;
    public float x,y;
    public int facingAngle;

    public HeartBeatPacket() {
        super(PacketType.HEARTBEAT);
    }


    public HeartBeatPacket(String entityId, float x, float y, int rotationAngle, int type) {
        super(PacketType.HEARTBEAT);
        this.entityId = entityId;
        this.x = x;
        this.y = y;
        this.facingAngle = rotationAngle;
        this.type = type;
    }

    @Override
    public void serialize(Serializer serializer) throws SerializationException {
        serializer.serializeString(entityId);
        serializer.serializeFloat(x);
        serializer.serializeFloat(y);
        serializer.serializeInt(facingAngle);
        serializer.serializeInt(type);
    }

    @Override
    public HeartBeatPacket deserialize(Deserializer deserializer) throws SerializationException {
        return new HeartBeatPacket(deserializer.deserializeString(),
                deserializer.deserializeFloat(),
                deserializer.deserializeFloat(),
                deserializer.deserializeInt(),
                deserializer.deserializeInt());
    }
}
