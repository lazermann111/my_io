package com.github.myio.networking.packet;

import com.github.czyzby.websocket.serialization.SerializationException;
import com.github.czyzby.websocket.serialization.Transferable;
import com.github.czyzby.websocket.serialization.impl.Deserializer;
import com.github.czyzby.websocket.serialization.impl.Serializer;
import com.github.myio.networking.Packet;
import com.github.myio.networking.PacketType;


public class ShieldInteractionPacket extends Packet  implements Transferable<ShieldInteractionPacket> {

    public String playerId;
    public String shiledId;
    public boolean equipped;

    public ShieldInteractionPacket() {
        super(PacketType.SHIELD_INTERACTION);
    }

    public ShieldInteractionPacket(String playerId, String shiledId, boolean equipped) {

        super(PacketType.SHIELD_INTERACTION);
        this.playerId = playerId;
        this.shiledId = shiledId;
        this.equipped = equipped;
    }

    @Override
    public void serialize(Serializer serializer) throws SerializationException {
        serializer
                .serializeString(playerId)
                .serializeString(shiledId)
                .serializeBoolean(equipped);
    }

    @Override
    public ShieldInteractionPacket deserialize(Deserializer deserializer) throws SerializationException {
        return new ShieldInteractionPacket(deserializer.deserializeString(), deserializer.deserializeString(), deserializer.deserializeBoolean());
    }
}
