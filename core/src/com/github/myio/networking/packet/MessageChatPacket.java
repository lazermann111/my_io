package com.github.myio.networking.packet;

import com.github.myio.entities.IoPoint;
import com.github.myio.networking.Packet;
import com.github.myio.networking.PacketType;


public class MessageChatPacket extends Packet {

    public String message;
    public IoPoint start;


    public MessageChatPacket() {
        super(PacketType.MESSAGE_CHAT_PACKET);
    }

    public MessageChatPacket(IoPoint start,String message) {
        super(PacketType.MESSAGE_CHAT_PACKET);
        this.message = message;
        this.start = start;
    }
}
