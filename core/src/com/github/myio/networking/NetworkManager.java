package com.github.myio.networking;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.github.czyzby.websocket.WebSocket;
import com.github.czyzby.websocket.WebSocketAdapter;
import com.github.czyzby.websocket.data.WebSocketCloseCode;
import com.github.czyzby.websocket.net.ExtendedNet;
import com.github.czyzby.websocket.serialization.impl.ManualSerializer;
import com.github.myio.GameClient;
import com.github.myio.networking.packet.*;
import com.github.myio.screens.GameScreen;
import com.github.myio.screens.MainMenuScreen;
import com.github.myio.tools.IoLogger;
import com.github.myio.ui.LockScreen;

import java.util.ArrayList;
import java.util.HashSet;

import static com.github.myio.GameConstants.DebugMode;
import static com.github.myio.GameConstants.isBotSimulator;

public class NetworkManager {
	
	private static final int PACKET_LIFESPAN = 5000;//ms
	private static final java.lang.String TAG ="NetworkManager" ;

	private HashSet<ClientPacketHandler> packetHandlers;
	private ArrayList<PacketWrapper> packets;


	private ManualSerializer serializer;
	
	private WebSocket socket;

	private LockScreen lockScreen;

	private volatile boolean isConnected = false;
	
	private long lastPingSendTime = System.currentTimeMillis();
	private int ping = 0;

	public NetworkManager () {
		packetHandlers = new HashSet<ClientPacketHandler>();
		packets = new ArrayList<PacketWrapper>();

	}
	
	public void update () {
		try {
			ArrayList<PacketWrapper> currentPackets = new ArrayList<PacketWrapper>();
			//packets.removeAll(Collections.singleton(null));
			currentPackets.addAll(packets);
			//IoLogger.log("" , "NetworkManager update " + currentPackets.size());
			ArrayList<PacketWrapper> packetsToRemove = new ArrayList<PacketWrapper>();
			for (PacketWrapper packetWrapper : currentPackets) {
               if(packetWrapper == null)
               {
				   packetsToRemove.add(packetWrapper);
				   continue;
			   }

               	Packet packet = packetWrapper.packet;
                ArrayList<ClientPacketHandler> currentPacketHandlers = new ArrayList<ClientPacketHandler>();
                currentPacketHandlers.addAll(packetHandlers);

                boolean handled = false;
                for (ClientPacketHandler packetHandler : currentPacketHandlers) {
                    if (packetHandler.handlePacket(packet))
                        handled = true;
                }

                if (handled)
				{
					packetsToRemove.add(packetWrapper);
				}
                	if (System.currentTimeMillis() - packetWrapper.time > PACKET_LIFESPAN )
					{
						if(DebugMode && !isBotSimulator)
                        {
                            IoLogger.log("NetworkManager.class","update()");
                        }
						packetsToRemove.add(packetWrapper);
					}
            }

			removeAllPackets(packetsToRemove);
		} catch (Exception e) {

			IoLogger.log("NetworkManager update", e.getMessage());
			e.printStackTrace();
			if(GameClient.getGame()!= null && GameClient.getGame().getMasterServer() !=null){
				GameClient.getGame().getMasterServer().postExceptionDetails(e, "NetworkManager::update");
			}



		}
	}
	
	private synchronized void removeAllPackets (ArrayList<PacketWrapper> packetsToRemove) {
		this.packets.removeAll(packetsToRemove);
	}
	
	private synchronized void removeAllPacketHandlers ()
	{
		IoLogger.log(TAG,"removeAllPacketHandlers");
		packetHandlers.clear();
	}
	
	private synchronized void addPacket (PacketWrapper packet) {
		packets.add(packet);
	}
	
	public void connect (String address, int port) throws Exception {

            isConnected = false;
            //socket = ExtendedNet.getNet().newSecureWebSocket(address, port);
            socket = ExtendedNet.getNet().newWebSocket(address, port);



		serializer = new ManualSerializer();
		serializer.register(new HeartBeatPacket());
		serializer.register(new WorldUpdatePacket());
		serializer.register(new DealDamagePacket());
		serializer.register(new EmotePacket());
		serializer.register(new EntityAddPacket());
		serializer.register(new EntityRemovePacket());
		serializer.register(new ItemPickedPacket());
		serializer.register(new InitialTileSnapshotPacket());
		serializer.register(new InitialWorldSnapshotPacket());
		serializer.register(new LoginPacket());
		serializer.register(new LoginResponsePacket());
		serializer.register(new SuicidePacket());
		serializer.register(new PlayerListPacket());
		serializer.register(new ProjectileLaunchedPacket());
		serializer.register(new WeaponSwitchedPacket());
		serializer.register(new PvPInfoPacket());
		serializer.register(new TimerGamePacket());
		serializer.register(new ShieldInteractionPacket());
		serializer.register(new PitInteractionPacker());
		serializer.register(new VisibilityChangePacket());
		serializer.register(new WinPacket());
		serializer.register(new GameStateChangedPacket());
		serializer.register(new SquadChangedPacket());
	//	serializer.register(new GameStateChangedPacket());
		serializer.register(new UseMedkitPacket());
		serializer.register(new TeamLoginPacket());
		serializer.register(new CurrentWorldSnapshotRequest());
		serializer.register(new CurrentWorldSnapshotPacket());
		serializer.register(new ShotgunShotPacket());
		serializer.register(new HeartBeatDisablePacket());
		serializer.register(new PlayerDiedPacket());
		serializer.register(new PlayerStatsPacket());
		serializer.register(new PlayerRevivingRequest());
		serializer.register(new ItemDroppedPacket());

		serializer.register(new ServerErrorPacket());
		serializer.register(new CapturablePointStateChangedPacket());
		serializer.register(new CapturablePointsSnapshot());
		serializer.register(new VoiceCommandsMenuPacket());
		serializer.register(new UseAmmoPacket());

		socket.setSerializer(serializer);

		socket.setSendGracefully(true);

		socket.addListener(getWebSocketListener());
        socket.connect();

		IoLogger.log(TAG, "connect()");

	}
	
	public void disconnect () {
		if (socket != null)
		{
			socket.close();
	 		IoLogger.log(TAG, "disconnect()");
		}
	}
	
	public void sendPacket (Packet packet) {

			if (socket != null) {
                byte[] packetData = serializer.serialize(packet);
                socket.send(packetData);
		}
	}



	
	public void sendPacketData (byte[] packetData) {
		if (socket != null)
			socket.send(packetData);
	}
	

	public synchronized void addPacketHandler (ClientPacketHandler packetHandler) {
		IoLogger.log(TAG,"Added PH " +packetHandler);
		packetHandlers.add(packetHandler);
	}
	
	public synchronized void removePacketHandler (ClientPacketHandler packetHandler) {
		IoLogger.log(TAG,"Removed PH " +packetHandler);
		packetHandlers.remove(packetHandler);
	}
	
	public boolean isConnected () {
		if (socket == null)
			return false;
		return isConnected;
	}
	
	private WebSocketAdapter getWebSocketListener() {
        return new WebSocketAdapter() {
            @Override
            public boolean onOpen(final WebSocket webSocket) {
				if(Gdx.app !=null) {
					IoLogger.log("WS", "Connected!");
					GameClient.getGame().getMessagesPanel().message("Connected!");
				}

                isConnected = true;
                return FULLY_HANDLED;
            }

            @Override
            public boolean onClose(final WebSocket webSocket, final WebSocketCloseCode code, final String reason) {
            	isConnected = false;
				if(Gdx.app !=null) IoLogger.log("WS", "Disconnected - status: " + code + ", reason: " + reason);
                removeAllPacketHandlers();

				GameClient.getGame().getMessagesPanel().message("Disconnected - status: " + code + ", reason: " + reason);
				if(lockScreen != null && GameClient.getGame().getScreen() instanceof GameScreen)
				{

					Gdx.app.postRunnable(new Runnable() {
						public void run() {
							lockScreen.showLock();
						}
					});
				}
				else if(GameClient.getGame().getScreen() instanceof MainMenuScreen)
				{
					((MainMenuScreen)GameClient.getGame().getScreen()).getConnectionStatus().setText("DISCONNECTED");
					((MainMenuScreen)GameClient.getGame().getScreen()).getConnectionStatus().setColor(Color.RED);
				}
				else
				{
					IoLogger.log("lockScreen is null");
				}

				return FULLY_HANDLED;
            }

            @Override
            public boolean onMessage(WebSocket webSocket, byte[] packetData) {
            	try
				{
					Packet	packet = (Packet) serializer.deserialize(packetData);

        			addPacket(new PacketWrapper(packet));
					if(DebugMode && Gdx.app !=null)	IoLogger.log("","Client on message packet " + packet);
        		} catch (Exception e) {
					if(Gdx.app !=null)IoLogger.log("Exception", e.getMessage());
					e.printStackTrace();
        			return NOT_HANDLED;
        		}
            	return FULLY_HANDLED;
            }
            
			@Override
            public boolean onMessage(final WebSocket webSocket, final String packetString) {
            	if (packetString.equals("ping")) {
            		socket.send("pong");
            		lastPingSendTime = System.currentTimeMillis();
            		return FULLY_HANDLED;
            	}
            	
            	if (packetString.equals("pang")) {
            		ping = (int) (System.currentTimeMillis() - lastPingSendTime);
					if(Gdx.app !=null)
					{
						GameClient.getGame().getMessagesPanel().message("Ping: " + ping);
					}
					else
					{
						IoLogger.log("Ping: " + ping);
					}
					return FULLY_HANDLED;
            	}
        		return NOT_HANDLED;
            }
            
            @Override
            public boolean onError(WebSocket webSocket, Throwable error)
			{
				if(Gdx.app !=null)
				{
					IoLogger.log("WS", "Error: " + error.getMessage());
					GameClient.getGame().getMessagesPanel().message("Error: " + error.getMessage());
					if(lockScreen != null && GameClient.getGame().getScreen() instanceof GameScreen)
					{

						Gdx.app.postRunnable(new Runnable() {
							public void run() {
								lockScreen.showLock();
							}
						});
					}
					else if(GameClient.getGame().getScreen() instanceof MainMenuScreen)
					{
						((MainMenuScreen)GameClient.getGame().getScreen()).getConnectionStatus().setText("DISCONNECTED");
						((MainMenuScreen)GameClient.getGame().getScreen()).getConnectionStatus().setColor(Color.RED);
					}
					else
					{
						IoLogger.log("lockScreen is null");
					}
				}

				return FULLY_HANDLED;
            }
            
        };
    }
	
	public int getPing() {
		return ping;
	}

	public void setLockScreen(LockScreen lock)  {
		lockScreen = lock;
	}
	
}
