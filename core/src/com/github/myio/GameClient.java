package com.github.myio;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.profiling.GLProfiler;
import com.badlogic.gdx.utils.Scaling;
import com.badlogic.gdx.utils.viewport.ExtendViewport;
import com.badlogic.gdx.utils.viewport.ScalingViewport;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.github.myio.dto.masterserver.GameType;
import com.github.myio.entities.PlayerCell;
import com.github.myio.entities.map.ZoneMapManager;
import com.github.myio.master.MasterServer;
import com.github.myio.networking.NetworkManager;
import com.github.myio.screens.LoadingScreen;
import com.github.myio.screens.MainMenuScreen;
import com.github.myio.tools.AssetManager;
import com.github.myio.tools.LoadAsset;
import com.github.myio.tools.SoundManager;
import com.github.myio.ui.RenderManager;
import com.github.myio.ui.SystemMessagesPanel;

import java.util.Random;

import static com.badlogic.gdx.Gdx.app;
import static com.github.myio.GameConstants.DebugMode;
import static com.github.myio.StringConstants.PREFERENCES_CLIENT_ID;


public class GameClient extends Game
{
    protected SpriteBatch batch;
    protected NetworkManager networkManager;
    protected MasterServer masterServer;
    protected ShapeRenderer gameShapeRenderer;
    protected ShapeRenderer uiShapeRenderer;
    protected AssetManager assetManager;
    protected OrthographicCamera camera;
    protected Viewport gameViewPort;
    protected Viewport uiViewport;
    protected Viewport scalingViewport;
    protected OrthographicCamera camerUi;
    protected SoundManager soundManager;
    protected Preferences gamePreferences;
    protected RenderManager renderManager;
    protected ZoneMapManager zoneMapManager;

    protected static GameClient game;
    protected SystemMessagesPanel messagesPanel;
    protected PlayerCell localPlayer;
    protected String clientId;
    protected String clientRegion;
    protected GameType gameType = null; //we should receive it from server



    public static TextureAtlas playerSkin;
    public static TextureAtlas uiAtlas;
    public static TextureAtlas mainMenu;
    public static TextureAtlas cellAtlas;

    public static BitmapFont font32White;
    public static BitmapFont font32Black;
    public static BitmapFont font32Trebushet;

    public Screen mainMenuScreen;

    boolean isAndroid;
    OrthographicCamera cameraSca;


    private LoadingScreen loadingScreen;
    public LoadAsset loadAsset = new LoadAsset();
    public static Music playingSong;
    public final static int MENU = 0;
    public final static int PREFERENCES = 1;
    public final static int APPLICATION = 2;
    public final static int ENDGAME = 3;
    @Override
    public void create() {
        GameConstants.isServer = false;
        game = this;
        app.setLogLevel(Application.LOG_DEBUG);
        if(DebugMode) GLProfiler.enable();
        batch = new SpriteBatch(4096);
        gameShapeRenderer = new ShapeRenderer();
        uiShapeRenderer = new ShapeRenderer();
        masterServer = new MasterServer();

        if (Gdx.app.getType().equals(Application.ApplicationType.Android))
        {
        isAndroid = true;
        }else {
         isAndroid = false;
        }




        camera = new OrthographicCamera();
        camerUi = new OrthographicCamera();

        gameViewPort = new ExtendViewport(Gdx.graphics.getWidth(), Gdx.graphics.getHeight(),camera);
        uiViewport = new ScreenViewport(camerUi);
        uiViewport.update(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        uiViewport.apply();
        camerUi.position.set(camerUi.viewportWidth / 2, camerUi.viewportHeight / 2, 0);
        camerUi.update();

        scalingViewport = new ScalingViewport(Scaling.stretch,Gdx.graphics.getWidth(),Gdx.graphics.getHeight(),camerUi);


        gamePreferences =  Gdx.app.getPreferences("game_prefs");

       /* if(gamePreferences.getLong(PREFERENCES_CLIENT_ID) != 0) todo find
        {
            clientId = gamePreferences.getString(PREFERENCES_CLIENT_ID);
        }
        else
        {*/
        clientId = System.currentTimeMillis()+""; //should be unique enough
        gamePreferences.putString(PREFERENCES_CLIENT_ID, clientId);
        gamePreferences.flush();
        // }


        zoneMapManager = new ZoneMapManager();
        networkManager = new NetworkManager();
        renderManager = new RenderManager(this, camera);
        soundManager = new SoundManager();


        messagesPanel = new SystemMessagesPanel(this, camera,localPlayer);
        assetManager = new AssetManager();

        //====================ATLAS=============================
        playerSkin = new TextureAtlas(Gdx.files.internal("atlas/playerSkin/playerSkin.atlas"));
        uiAtlas = new TextureAtlas(Gdx.files.internal("atlas/uiAtlas/uiAtlas.atlas"));
        mainMenu = new TextureAtlas(Gdx.files.internal("atlas/mainMenu/mainMenu.atlas"));
        cellAtlas = new TextureAtlas(Gdx.files.internal("atlas/cellAtlas/cellAtlas.atlas"));
        cellAtlas = new TextureAtlas(Gdx.files.internal("atlas/cellAtlas/cellAtlas.atlas"));
        //=========================FONTS===================
        font32White = new BitmapFont(Gdx.files.internal("fonts/font32White.fnt"));
        font32Black = new BitmapFont(Gdx.files.internal("fonts/font32Black.fnt"));
        font32Trebushet = new BitmapFont(Gdx.files.internal("fonts/trebushet2.fnt"));
        mainMenuScreen = new MainMenuScreen(this);

        //loadingScreen = new LoadingScreen(this);
        // loadAsset.queueAddMusic();
       // loadAsset.manager.finishLoading();

       mainMenuScreen = new MainMenuScreen(this);
        setScreen(mainMenuScreen);
    }

    public void changeScreen(int screen){
        switch(screen){

            case APPLICATION:
                if(mainMenuScreen == null) mainMenuScreen = new MainMenuScreen(this);
                this.setScreen(mainMenuScreen);
                break;
        }
    }

    public Screen getMainMenuScreen() {return mainMenuScreen;}


    @Override
    public void render() {

        super.render();
        if(networkManager != null) networkManager.update();
    }

    @Override
    public void resize(int width, int height) {
        super.resize(width, height);
        uiViewport.update(width, height);
        uiViewport.setScreenSize(width,height);
        float aspectRatio = (float) width / (float) height;
        camerUi.position.set(width / 2, height / 2, 0);
        camerUi.update();
    }

    @Override
    public void dispose() {
        networkManager.disconnect(); // Null-safe closing method that catches and logs any exceptions.
        batch.dispose();
        font32Black.dispose();
        font32White.dispose();
       // loadAsset.manager.dispose();
    }


    public SpriteBatch getBatch() {
        return batch;
    }

    public static BitmapFont getFont32White() {return font32White;}

    public static BitmapFont getFont32Black() {return font32Black;}

    public NetworkManager getNetworkManager() {
        return networkManager;
    }

    public ShapeRenderer getGameShapeRenderer() {
        return gameShapeRenderer;
    }

    public ShapeRenderer getUiShapeRenderer() {
        return uiShapeRenderer;
    }

    public SoundManager getSoundManager() {
        return soundManager;
    }

    public Preferences getGamePreferences() {
        return gamePreferences;
    }




    public SystemMessagesPanel getMessagesPanel() {
        return messagesPanel;
    }

    public static GameClient getGame() {
        return game;
    }

    public static Random random = new Random();

    public static float nextFloat(float min, float max)
    {
        return min + random.nextFloat() * (max - min);
    }

    public RenderManager getRenderManager() {
        return renderManager;
    }

    public  PlayerCell getLocalPlayer() {
        return localPlayer;
    }

    public  void setLocalPlayer(PlayerCell localPlayer) {
        this.localPlayer = localPlayer;
    }

    public OrthographicCamera getCamera() {
        return camera;
    }

    public Viewport getGameViewPort() {
        return gameViewPort;
    }


    public Viewport getUiViewport() {
        return uiViewport;
    }

    public AssetManager getAssetManager() {
        return assetManager;
    }


    public ZoneMapManager getZoneMapManager() {
        return zoneMapManager;
    }

    public MasterServer getMasterServer() {
        return masterServer;
    }

    public String getClientId() {
        return clientId;
    }

    public String getClientRegion() {
        return clientRegion;
    }

    public void setClientRegion(String clientRegion) {
        this.clientRegion = clientRegion;
    }

    public GameType getGameType() {
        return gameType;
    }

    public void setGameType(GameType gameType) {
        this.gameType = gameType;
    }

    public boolean isAndroid() {return isAndroid;}

    public Viewport getScalingViewport() {return scalingViewport;}
}
