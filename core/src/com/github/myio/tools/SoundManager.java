package com.github.myio.tools;


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.github.myio.entities.IoPoint;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import static com.github.myio.GameConstants.DebugMode;

public class SoundManager
{
    public static String ROCKET = "r_launch";
    public static String MACHINEGUN = "machinegun";
    public static String AK47 = "ak47";
    public static String PLASMA = "plasmagun";
    public static String RAILSLUG = "railgun";
    public static String SHOTGUN_AMMO = "shotgun";
    public static String PISTOL = "pistol_shot";
    public static String SHELL = "shell";


    public static String EXPLOSION1 = "expl1";
    public static String EXPLOSION2 = "expl2";

    public static String RELOAD = "reaload1";

    public static String MARKER = "MARKER";
    public static String WEAPON_SWITCH = "WEAPON_SWITCH";
    public static String ITEM_PICKED = "ITEM_PICKED";

    public static String FOOTSTEPS_MUD = "footsteps";
    public static String FOOTSTEPS_STONE = "FOOTSTEPS_STONE";
    public static String FOOTSTEPS_SAND = "FOOTSTEPS_SAND";
    public static String FOOTSTEPS_ENEMIES = "footsteps_e";
    public static String WALLHIT = "wallhit";
    public static String WALL_BROWN = "wall_brown";
    public static String WALL_YELLOW = "wall_yellow";
    public static String BUSH = "bush";
    public static String SPIDER_WEB = "spider_web";
    public static String STONE_SMALL = "hit_stoun";
    public static String STONE_BIG = "hit_stoun";
    public static String STONE_GREEN = "hit_stoun";
    public static String MISFIRE = "misfire";

    public static String PLAYER_WALL_HIT = "playerwallhit";
    public static String WEAPON_RELOAD = "weapon_reload";
    public static String ROCKET_2 = "rocket_2";
    public static String GRENADE_THROW = "grenade_throw";
    public static String GRENADE_LAUNCH = "g_launch";
    public static String KNIFE_WHOOSH = "knife_whoosh";
    public static String DEATH = "death";
    public static String HIT = "hit";

    public static String AMBIENT_1 = "AMBIENT_1";

    public static String FOOTSTEPS_WET = "water_footsteps";
    public static String BUSH_HIT = "bush_hit";
    public static String SHIELD_HIT = "shield_hit";
    public static String MOVING_WITH_SHIELD = "moving_with_shield";
    public static String PISTOL_RELOAD = "pistol_reload";
    public static String SHOTGUN_RELOAD = "shotgun_reload";
    public static String SHOTGUN_RELOAD_1_PATRON = "shotgun_reload_1_patron";
    public static String SHOTGUN_RELOAD_FINISH = "shotgun_reload_finish";
    public static String FLASHBANG = "flashbang";
    public static String HP = "hp";
    public static String WEAPON = "weapon";
    public static String ARMOR = "armor";
    public static String AMMUNITION = "ammunition";
    public static String PATRON = "patron";
    public static String MAIN_MENU_OFFICIALLY = "main_menu_officially";
    public static String GO_GO_GO = "go go go";
    public static String OKEY_LETS_GO = "okeyLetsGo";


    public static int MAX_PAN_DISTANCE = 1000;
    public static int MIN_VOL_DISTANCE = 3000;

    String[] check_sound_names={"","",""};

    public static boolean mute;
    public float vol=1;
    public float gameVol=1;


    private  Map<String, SoundInfo> soundMap = new HashMap<String, SoundInfo>();
    {

        Sound[] machineGunVariations = new Sound[]
        {
                Gdx.audio.newSound(Gdx.files.internal("sounds/machinegun.mp3")),
        };

        Sound[] shellVariation = new Sound[]
                {
                        Gdx.audio.newSound(Gdx.files.internal("sounds/shell.mp3")),
                        Gdx.audio.newSound(Gdx.files.internal("sounds/Shell_2.mp3")),
                        Gdx.audio.newSound(Gdx.files.internal("sounds/shell_3.mp3")),

                };

        Sound[] footstepsSandVariationVariation = new Sound[]
                {
                        Gdx.audio.newSound(Gdx.files.internal("sounds/footsteps_sand.mp3")),
                        Gdx.audio.newSound(Gdx.files.internal("sounds/footsteps_sand_2.mp3")),


                };
        Sound[] wallhitVariation = new Sound[]
                {
                        Gdx.audio.newSound(Gdx.files.internal("sounds/nomusic.mp3")),
                        Gdx.audio.newSound(Gdx.files.internal("sounds/nomusic.mp3")),
                        Gdx.audio.newSound(Gdx.files.internal("sounds/nomusic.mp3")),
                        Gdx.audio.newSound(Gdx.files.internal("sounds/nomusic.mp3")),
                        Gdx.audio.newSound(Gdx.files.internal("sounds/wallhit_4.mp3")),


                };
        Sound[] wallBrownVariation = new Sound[]
                {
                        Gdx.audio.newSound(Gdx.files.internal("sounds/nomusic.mp3")),
                        Gdx.audio.newSound(Gdx.files.internal("sounds/wallhit_4.mp3")),
                        Gdx.audio.newSound(Gdx.files.internal("sounds/nomusic.mp3")),
                        Gdx.audio.newSound(Gdx.files.internal("sounds/nomusic.mp3")),
                        Gdx.audio.newSound(Gdx.files.internal("sounds/nomusic.mp3")),

                };
        Sound[] wallYellowVariation = new Sound[]
                {
                        Gdx.audio.newSound(Gdx.files.internal("sounds/wall_yellow.mp3")),


                };
        Sound[] BushVariation = new Sound[]
                {
                        Gdx.audio.newSound(Gdx.files.internal("sounds/bush.mp3")),


                };
        Sound[] spiderWebVariation = new Sound[]
                {
                        Gdx.audio.newSound(Gdx.files.internal("sounds/spider_web.mp3")),


                };
        Sound[] hitSmallStounVariation = new Sound[]
                {
                        Gdx.audio.newSound(Gdx.files.internal("sounds/nomusic.mp3")),
                        Gdx.audio.newSound(Gdx.files.internal("sounds/nomusic.mp3")),
                        Gdx.audio.newSound(Gdx.files.internal("sounds/nomusic.mp3")),
                        Gdx.audio.newSound(Gdx.files.internal("sounds/hit_stoun_3.mp3")),
                        Gdx.audio.newSound(Gdx.files.internal("sounds/nomusic.mp3")),




                };
        Sound[] hitStounBigVariation = new Sound[]
                {
                        Gdx.audio.newSound(Gdx.files.internal("sounds/nomusic.mp3")),
                        Gdx.audio.newSound(Gdx.files.internal("sounds/nomusic.mp3")),
                        Gdx.audio.newSound(Gdx.files.internal("sounds/nomusic.mp3")),
                        Gdx.audio.newSound(Gdx.files.internal("sounds/nomusic.mp3")),
                        Gdx.audio.newSound(Gdx.files.internal("sounds/hit_stoun_3.mp3")),


                };
        Sound[] hitStounBigGreenVariation = new Sound[]
                {
                        Gdx.audio.newSound(Gdx.files.internal("sounds/nomusic.mp3")),
                        Gdx.audio.newSound(Gdx.files.internal("sounds/hit_stoun_3.mp3")),
                        Gdx.audio.newSound(Gdx.files.internal("sounds/nomusic.mp3")),
                        Gdx.audio.newSound(Gdx.files.internal("sounds/nomusic.mp3")),
                        Gdx.audio.newSound(Gdx.files.internal("sounds/nomusic.mp3")),


                };

        Sound[] deathVariation = new Sound[]
                {
                        Gdx.audio.newSound(Gdx.files.internal("sounds/death.mp3")),
                        Gdx.audio.newSound(Gdx.files.internal("sounds/death_1.mp3")),
                        Gdx.audio.newSound(Gdx.files.internal("sounds/death_2.mp3")),

                };
        Sound[] hitVariation = new Sound[]
                {

                        Gdx.audio.newSound(Gdx.files.internal("sounds/cry_3.mp3")),
                        Gdx.audio.newSound(Gdx.files.internal("sounds/player_hit.mp3")),
                        Gdx.audio.newSound(Gdx.files.internal("sounds/player_hit.mp3")),
                        Gdx.audio.newSound(Gdx.files.internal("sounds/player_hit.mp3")),
                        Gdx.audio.newSound(Gdx.files.internal("sounds/player_hit.mp3")),
                        Gdx.audio.newSound(Gdx.files.internal("sounds/player_hit.mp3")),
                        Gdx.audio.newSound(Gdx.files.internal("sounds/player_hit.mp3")),
                        Gdx.audio.newSound(Gdx.files.internal("sounds/player_hit.mp3")),
                        Gdx.audio.newSound(Gdx.files.internal("sounds/player_hit.mp3")),
                        Gdx.audio.newSound(Gdx.files.internal("sounds/player_hit.mp3")),
                        Gdx.audio.newSound(Gdx.files.internal("sounds/player_hit.mp3")),



                };
        Sound[] footstepsStoneVariation = new Sound[]
                {
                        Gdx.audio.newSound(Gdx.files.internal("sounds/footstepsStoun_2.mp3")),

                };
        Sound[] footstepsMudVariation = new Sound[]
                {

                        Gdx.audio.newSound(Gdx.files.internal("sounds/footsteps_mud_3.mp3")),
                        Gdx.audio.newSound(Gdx.files.internal("sounds/footsteps_mud_4.mp3")),

                };
        Sound[] footstepsWetVariation = new Sound[]
                {
                        Gdx.audio.newSound(Gdx.files.internal("sounds/wet_1.mp3")),

                };
        Sound[] shieldHitVariation = new Sound[]
                {
                        Gdx.audio.newSound(Gdx.files.internal("sounds/shield_hit.mp3")),
                        Gdx.audio.newSound(Gdx.files.internal("sounds/shield_hit_1.mp3")),
                        Gdx.audio.newSound(Gdx.files.internal("sounds/shield_hit_2.mp3")),

                };
        Sound[] markerVariation = new Sound[]
                {
                        Gdx.audio.newSound(Gdx.files.internal("sounds/marker.mp3")),

                };

        Sound[] weaponSwitchVariation = new Sound[]
            {
                    Gdx.audio.newSound(Gdx.files.internal("sounds/weapon_switch.mp3")),

            };
        Sound[] itemPickedVariation = new Sound[]
            {
                    Gdx.audio.newSound(Gdx.files.internal("sounds/item_picked.mp3")),

            };
       Sound[] playerwallhitVariation = new Sound[]
           {
                   Gdx.audio.newSound(Gdx.files.internal("sounds/player_wall_touch.mp3")),

           };
         Sound[] moving_with_shield = new Sound[]
           {
                   Gdx.audio.newSound(Gdx.files.internal("sounds/moving_with_shield_1.mp3")),

             };




        soundMap.put(EXPLOSION1, new SoundInfo(Gdx.audio.newSound(Gdx.files.internal("sounds/grenade_explosion.mp3")), 1000, 0.2f ));
        soundMap.put(EXPLOSION2, new SoundInfo(Gdx.audio.newSound(Gdx.files.internal("sounds/grenade_explosion.mp3")), 1000, 0.2f ));

        soundMap.put(RELOAD, new SoundInfo(Gdx.audio.newSound(Gdx.files.internal("sounds/shot_gun_reload_1_cartridge.mp3")),100,1,1.0f));


        soundMap.put(ROCKET, new SoundInfo(Gdx.audio.newSound(Gdx.files.internal("sounds/rpg_flight_1.mp3")),100,1,1.0f));
        soundMap.put(MACHINEGUN, new SoundInfo(machineGunVariations,80,1));
        soundMap.put(SHOTGUN_AMMO, new SoundInfo(Gdx.audio.newSound(Gdx.files.internal("sounds/shotgun_shot.mp3")),400, 1,1.0f));
        soundMap.put(PISTOL, new SoundInfo(Gdx.audio.newSound(Gdx.files.internal("sounds/pistol_shot_1.mp3")), 100,1,1f));
        soundMap.put(SHELL, new SoundInfo(shellVariation,200, 1));


        soundMap.put(FOOTSTEPS_MUD, new SoundInfo(footstepsMudVariation,400, 3));
        soundMap.put(FOOTSTEPS_STONE, new SoundInfo(footstepsStoneVariation,400, 3));
        soundMap.put(FOOTSTEPS_SAND, new SoundInfo(footstepsSandVariationVariation,400, 3));
        soundMap.put(FOOTSTEPS_WET, new SoundInfo(footstepsWetVariation,400, 3));


        soundMap.put(FOOTSTEPS_ENEMIES, new SoundInfo(Gdx.audio.newSound(Gdx.files.internal("sounds/footsteps.mp3")),400, 3));


        soundMap.put(WALLHIT, new SoundInfo(wallhitVariation,100, 1));
        soundMap.put(WALL_BROWN, new SoundInfo(wallBrownVariation,100, 1));
        soundMap.put(WALL_YELLOW, new SoundInfo(wallYellowVariation,100, 1));
        soundMap.put(BUSH, new SoundInfo(BushVariation,100, 1));
        soundMap.put(SPIDER_WEB, new SoundInfo(spiderWebVariation,100, 1));
        soundMap.put(STONE_SMALL, new SoundInfo(hitSmallStounVariation,100, 1));
        soundMap.put(STONE_BIG, new SoundInfo(hitStounBigVariation,100, 1));
        soundMap.put(STONE_GREEN, new SoundInfo(hitStounBigGreenVariation,100, 1));
        soundMap.put(MISFIRE, new SoundInfo(Gdx.audio.newSound(Gdx.files.internal("sounds/misfire_1.mp3")),100,1,0.3f));
        soundMap.put(PLAYER_WALL_HIT, new SoundInfo(playerwallhitVariation,1000, 1));
        soundMap.put(WEAPON_RELOAD, new SoundInfo(Gdx.audio.newSound(Gdx.files.internal("sounds/weapon_reload.mp3")),1000,1,1.0f));
        soundMap.put(ROCKET_2, new SoundInfo(Gdx.audio.newSound(Gdx.files.internal("sounds/rocket_launch2.mp3")),100,1,1.0f));
        soundMap.put(GRENADE_THROW, new SoundInfo(Gdx.audio.newSound(Gdx.files.internal("sounds/grenade_thpow.mp3")),100,1,1.0f));
        soundMap.put(KNIFE_WHOOSH, new SoundInfo(Gdx.audio.newSound(Gdx.files.internal("sounds/knife_whoosh.mp3")),100,1,1.0f));
        soundMap.put(DEATH, new SoundInfo(deathVariation,10000,1));
        soundMap.put(HIT, new SoundInfo(hitVariation,400,1));
        soundMap.put(GRENADE_LAUNCH,new SoundInfo(Gdx.audio.newSound(Gdx.files.internal("sounds/grenade_launch.mp3")),100,1,1.0f));
        soundMap.put(AK47,new SoundInfo(Gdx.audio.newSound(Gdx.files.internal("sounds/ak47.mp3")),100,2,1.0f));

        soundMap.put(BUSH_HIT, new SoundInfo(Gdx.audio.newSound(Gdx.files.internal("sounds/bush_hit.mp3")),100,1,1.0f));
        soundMap.put(SHIELD_HIT, new SoundInfo(shieldHitVariation,400,1));
        soundMap.put(MOVING_WITH_SHIELD, new SoundInfo(moving_with_shield,500,100));
        soundMap.put(PISTOL_RELOAD, new SoundInfo(Gdx.audio.newSound(Gdx.files.internal("sounds/pistol_reload.mp3")),100,1,1.0f));
        soundMap.put(SHOTGUN_RELOAD, new SoundInfo(Gdx.audio.newSound(Gdx.files.internal("sounds/shotgun_reload_2.mp3")),100,1,1.0f));
        soundMap.put(SHOTGUN_RELOAD_1_PATRON, new SoundInfo(Gdx.audio.newSound(Gdx.files.internal("sounds/shotgun_reload_1_patron.mp3")),100,1,1.0f));
        soundMap.put(SHOTGUN_RELOAD_FINISH, new SoundInfo(Gdx.audio.newSound(Gdx.files.internal("sounds/shotgun_reload_finish.mp3")),100,1,1.0f));
        soundMap.put(FLASHBANG, new SoundInfo(Gdx.audio.newSound(Gdx.files.internal("sounds/flashbang_1.mp3")),1000,1,0.5f));
        soundMap.put(HP, new SoundInfo(Gdx.audio.newSound(Gdx.files.internal("sounds/hp.mp3")),1000,1,0.5f));
        soundMap.put(WEAPON, new SoundInfo(Gdx.audio.newSound(Gdx.files.internal("sounds/weapon.mp3")),1000,1,0.5f));
        soundMap.put(ARMOR, new SoundInfo(Gdx.audio.newSound(Gdx.files.internal("sounds/armor.mp3")),1000,1,0.5f));
        soundMap.put(AMMUNITION, new SoundInfo(Gdx.audio.newSound(Gdx.files.internal("sounds/ammunision.mp3")),1000,1,0.5f));
        soundMap.put(PATRON, new SoundInfo(Gdx.audio.newSound(Gdx.files.internal("sounds/patron.mp3")),1000,1,0.5f));
        soundMap.put(MAIN_MENU_OFFICIALLY, new SoundInfo(Gdx.audio.newSound(Gdx.files.internal("sounds/main_menu_officially.mp3")),1000,1,0.5f));
//        soundMap.put(GO_GO_GO, new SoundInfo(Gdx.audio.newSound(Gdx.files.internal("sounds/gogogo.mp3")),1000,1,0.5f));
//        soundMap.put(GO_GO_GO, new SoundInfo(Gdx.audio.newSound(Gdx.files.internal("sounds/go go go.mp3")),1000,1,0.5f));
        //soundMap.put(OKEY_LETS_GO, new SoundInfo(Gdx.audio.newSound(Gdx.files.internal("sounds/okeyLets'Go.mp3")),1000,1,0.5f));



        soundMap.put(MARKER, new SoundInfo(markerVariation,400,100));
        soundMap.put(WEAPON_SWITCH, new SoundInfo(weaponSwitchVariation,400,100));
        soundMap.put(ITEM_PICKED, new SoundInfo(itemPickedVariation,400,100));


    }

    private  Map<String, Long> lastPlayedSound = new HashMap<String, Long>();

    public SoundManager()
    {
        for (SoundInfo  s : soundMap.values()) // atlasAll for preloading all sounds for web version
           for (Sound sound : s.getSounds()) {
               sound.play(); //todo find better way for preloading
           }
    }


    public void playSound(String soundname)
    {
        if(!soundMap.containsKey(soundname))
        {
            if(DebugMode) IoLogger.log("SoundManager", " Unable to obtain sound! " + soundname);
            return;
        }

        soundMap.get(soundname).getRandomSound().play();
    }
   // public void playSoundWithPanAndVolume(String soundname)
   // {
    //    soundname =SoundInfo(soundMainMenuSckreenVariation,400,100));
   // }

    public void playSoundWithPanAndVolume(String soundname,  IoPoint listenerPosition, IoPoint soundPosition)
    {
        playSoundWithPanAndVolume(soundname, "", listenerPosition, soundPosition, 1);
    }

    public void playSoundWithPanAndVolume(String soundname,  IoPoint listenerPosition, IoPoint soundPosition, float intervalMultiplier)
    {
        playSoundWithPanAndVolume(soundname, "", listenerPosition, soundPosition, intervalMultiplier);
    }
    public void playSoundWithPanAndVolume(String soundname, String emitterId, IoPoint listenerPosition, IoPoint soundPosition  )
    {
        playSoundWithPanAndVolume(soundname, emitterId, listenerPosition, soundPosition, 1);
    }
    public void stopSound(String soundName) {
        soundMap.get(soundName).getRandomSound().stop();
    }
    public void setVolume(float volume){
       this.vol=volume;

    }
    /**
     *
     * @param soundname
     * @param emitterId  indicates sound source, like you, enemy player, or even some entity on map
     * @param listenerPosition
     * @param soundPosition
     * @param intervalMultiplier sometimes we need to have custom intervals between 2 sounds, like when we have speedMultiplier 0.4 (moving with shield), then we need to have 2.5 more interval between footsteps
     */
    public void playSoundWithPanAndVolume(String soundname, String emitterId, IoPoint listenerPosition, IoPoint soundPosition, float intervalMultiplier)
    {
        if(!soundMap.containsKey(soundname))
        {
            if(DebugMode) IoLogger.log("SoundManager", " Unable to obtain sound! " + soundname);
            return;
        }

        if(listenerPosition.distance(soundPosition) > MIN_VOL_DISTANCE) return;

        SoundInfo info = soundMap.get(soundname);
        float pan = (listenerPosition.x - soundPosition.x)/MAX_PAN_DISTANCE;

        if (!mute) {
            gameVol = (float) (1 - listenerPosition.distance(soundPosition)/MIN_VOL_DISTANCE * info.getFallOffMultyplier()) * info.a *(vol);  ;
        } else if(mute){
            gameVol=0;
        }

        if(gameVol < 0) return; // no sense to play it :D


        if(info.getInterval() != 0)
        {
            String sound = soundname+emitterId;// here we make
            long current = System.currentTimeMillis();
            if(!lastPlayedSound.containsKey(sound)  // play sound first time
             || (current - lastPlayedSound.get(sound) > info.getInterval() * intervalMultiplier)) // sound was played long ago
            {
                info.getRandomSound().play(gameVol, 1, -pan);
                lastPlayedSound.put(sound, current);
            }

        }
        else
        {
            info.getRandomSound().play(gameVol, 1, -pan);
        }


        //soundDelay(soundname);
    }

    public boolean check_names(String name)
    {
        for (int i=0;i<3;i++) {
            if (check_sound_names[i].equals(name))
                return true;
        }
        return false;
    }


    public  class SoundInfo
    {
        //defines how fast sound will fall off from emitting point
        private float fallOffMultiplier = 1;
        public float a = 1;
        //sound instance
        private List<Sound> sounds = new ArrayList();

        //how often we can play same sound (in ms). 0 means no restriction on play frequency
        private long  interval;

        public SoundInfo(Sound sound) {
            sounds.add(sound);

        }

        public SoundInfo(Sound sound, long interval, float fallOffMultyplier) {
            this.fallOffMultiplier = fallOffMultyplier;
            sounds.add(sound);
            this.interval = interval;
        }

        public SoundInfo(Sound sound, long interval, float fallOffMultyplier, float a) {
            this.fallOffMultiplier = fallOffMultyplier;
            sounds.add(sound);
            this.interval = interval;
            this.a = a;
        }

        public SoundInfo(Sound[] sounds, long interval, float fallOffMultyplier) {
            this.fallOffMultiplier = fallOffMultyplier;
            this.interval = interval;

            for (Sound s : sounds)
                this.sounds.add(s);
        }

        public float getFallOffMultyplier() {
            return fallOffMultiplier;
        }

        public void setFallOffMultyplier(float fallOffMultiplier) {
            this.fallOffMultiplier = fallOffMultiplier;
        }



        public Sound getRandomSound()
        {
            return sounds.get(r.nextInt(sounds.size()));
        }


        public long getInterval() {
            return interval;
        }

        public void setInterval(long interval) {
            this.interval = interval;
        }

        public List<Sound> getSounds() {
            return sounds;
        }
    }


    private Random r = new Random();


}

