package com.github.myio.tools;


import com.badlogic.gdx.Gdx;

import java.util.logging.Level;
import java.util.logging.Logger;

public class IoLogger
{

    static Logger fileLogger;

 /*   static
    {

//        fileLogger = Logger.getLogger("MyLog");
//        FileHandler fh;
//        try
//        {
//            System.setProperty("java.util.logging.SimpleFormatter.format","%1$tF %1$tT %4$s %2$s %5$s%6$s%n");
//
//            DateFormat df = new SimpleDateFormat("dd_MM_yyyy_hh_mm_ss");
//            String requiredDate = df.format(new Date());
//            if (isServer)
//            {
//                fh = new FileHandler("./logs/"+ requiredDate +"_serverLog.txt", true);
//            }
//            else if(isBotSimulator)
//            {
//                fh = new FileHandler("./logs/ "+ requiredDate +"_botSimLog.txt");
//            }
//            else
//            {
//                fh = new FileHandler("./clientLog/"+requiredDate +"_clientLog.txt"); // core/assets
//            }
//            fileLogger.addHandler(fh);
//            SimpleFormatter formatter = new SimpleFormatter();
//            fh.setFormatter(formatter);
//
//           //  the following statement is used to log any messages
//             fileLogger.info("My first log");
//
//        }
//        catch (Exception e)
//        {
//            e.printStackTrace();
//        }

    }*/

    public static void log(String tag, String message)
    {
        if (Gdx.app != null)
        { Gdx.app.log(tag, message); }

        else
        {
            System.out.println(tag + ": " + message);
        }


        if (fileLogger != null)
        { fileLogger.log(Level.INFO, tag + ": " + message); }
    }

    public static void log(String message)
    {
        if (Gdx.app != null)
        { Gdx.app.log("", message); }

        else
        {
            System.out.println(message);
        }


        if (fileLogger != null)
        { fileLogger.log(Level.INFO, message); }
    }

    public static void err(String tag, String message)
    {
        if (Gdx.app != null)
        { Gdx.app.log(tag, message); }

        else
        {
            System.out.println(tag + ": " + message);
        }


        if (fileLogger != null)
        { fileLogger.log(Level.SEVERE, tag + ": " + message); }
    }

    public static void err(String message)
    {
        if (Gdx.app != null)
        { Gdx.app.log("", message); }

        else
        {
            System.out.println(message);
        }


        if (fileLogger != null)
        { fileLogger.log(Level.SEVERE, message); }
    }


}
