package com.github.myio.tools;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.github.myio.GameClient;
import com.github.myio.entities.PlayerCell;
import com.github.myio.networking.packet.EmotePacket;
import com.github.myio.ui.CountDown;
import com.github.myio.ui.EmotesMenu;
import com.github.myio.ui.ControlsAndroid;

/**
 * Created by alex on 08.03.18.
 */

public class CustomInputProcessor implements InputProcessor
{

    EmotesMenu mEmotesMenu;
    GameClient mGame;
    public PlayerCell mLocalPlayer;
    CountDown mCountDown;
    private boolean isFire = false;
    ControlsAndroid touchPadTest;
    int pointer;

    final String TAG = "CustomInputProcessor";

    public CustomInputProcessor(GameClient game, PlayerCell localPlayer, EmotesMenu emotesMenu, CountDown countDown)
    {
        mLocalPlayer = localPlayer;
        mEmotesMenu = emotesMenu;
        mGame = game;
        mCountDown = countDown;

    }


    @Override
    public boolean keyDown(int keycode)
    {
        if (keycode == Input.Keys.V)
        {
            if (!mLocalPlayer.isSuppressed())
            {
                mLocalPlayer.setAim(true);
            }
            return true;
        }
        return false;
    }

    @Override
    public boolean keyUp(int keycode)
    {
        if (keycode == Input.Keys.V)
        {
            if (!mLocalPlayer.isSuppressed())
            {
                mLocalPlayer.setAim(false);
            }
            return true;
        }
        return false;
    }


    @Override
    public boolean keyTyped(char character)
    {
        return false;
    }

    @Override
    public boolean touchDown(int x, int y, int pointer, int button)
    {
        this.pointer = pointer;
        if (!GameClient.getGame().isAndroid())
        {
            if (button == Input.Buttons.LEFT)
            {
                if (!mLocalPlayer.isSuppressed())
                {
                    isFire = true;
                }
                else
                {
                    isFire = false;
                }
                return true;
            }

            if (button == Input.Buttons.RIGHT)
            {
                mEmotesMenu.showEmotesTable(Gdx.input.getX(), Gdx.input.getY());
                return true;
            }
        }
        return false;

    }


    public boolean isFire()
    {
        return isFire;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button)
    {
        if (!GameClient.getGame().isAndroid())
        {
            if (button == Input.Buttons.LEFT)
            {
                if (mLocalPlayer.getCurrentWeapon() != null)
                {
                    mLocalPlayer.getCurrentWeapon().setCanShot(true);
                }
                isFire = false;
            }
            if (button == Input.Buttons.RIGHT)
            {
                mEmotesMenu.hide();
                if (mEmotesMenu.getEmoteForBroadcast() != null)
                {
                    mGame.getNetworkManager().sendPacket(new EmotePacket(mLocalPlayer.getId(), mEmotesMenu.getEmoteForBroadcast(), mLocalPlayer.getPosition()));
                    IoLogger.log(TAG, "send   " + mEmotesMenu.getEmoteForBroadcast());
                    mEmotesMenu.clearEmoteForBroadcast();
                }
                return true;
            }
        }

        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer)
    {
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY)
    {
        return false;
    }

    @Override
    public boolean scrolled(int amount)
    {


        return false;
    }

    public void setTouchPadTest(ControlsAndroid controlsAndroid)
    {
        this.touchPadTest = controlsAndroid;
    }

    public int getPointer()
    {
        return pointer;
    }

    public void setPointer(int pointer)
    {
        this.pointer = pointer;
    }
}

