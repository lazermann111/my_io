package com.github.myio.tools;

import com.badlogic.gdx.Gdx;
import com.github.myio.EntityManager;
import com.github.myio.GameClient;
import com.github.myio.entities.PlayerCell;
import com.github.myio.networking.ClientPacketHandler;
import com.github.myio.networking.NetworkManager;
import com.github.myio.networking.Packet;
import com.github.myio.networking.packet.SquadChangedPacket;
import com.github.myio.ui.FinalMessage;
import com.github.myio.ui.SquadInfo;

import java.util.HashMap;
import java.util.Map;

import static com.github.myio.networking.PacketType.SQUAD_CHANGED;

/**
 * Created by Igor on 4/4/2018.
 */

public class ClientSquad implements ClientPacketHandler
{

    private int squadId;
    private boolean formed;
    private Map<String,PlayerCell> members = new HashMap<String, PlayerCell>();
    private PlayerCell squadLeader;
    SquadInfo squadInfo;
    String[] squadIdList;
    Map<String, String> squadNames;
    FinalMessage mFinalMessage;


    private EntityManager entityManager;

    public ClientSquad(EntityManager entityManager)  {
        this.entityManager = entityManager;
        GameClient.getGame().getNetworkManager().addPacketHandler(this);
        squadInfo = new SquadInfo();
        squadNames = new HashMap<String, String>();
    }

    public ClientSquad(EntityManager entityManager, NetworkManager networkManager)  {
        this.entityManager = entityManager;
        networkManager.addPacketHandler(this);
        squadNames = new HashMap<String, String>();
    }

    public boolean assignFromPacket(SquadChangedPacket packet)
    {
        boolean localPlayerFound = false;
        members.clear();
        squadIdList = packet.membersId;
        for (String s : packet.membersId)
        {
            IoLogger.log("membersId   " + s);
            if(s ==  null) break; // end of array
            PlayerCell member = entityManager.getAllPlayers().get(s);
            if(member == null)
            {
                Gdx.app.error("ClientSquad", "Wrong member id");
                return false;
            }

            if(member.equals(GameClient.getGame().getLocalPlayer()))
            {
                localPlayerFound = true;
            }
            members.put(s,member);
        }

        if(!localPlayerFound)
        {
            Gdx.app.error("ClientSquad", "Local player not found in squad members!!");
            return false;
        }

        formed = packet.formed;
        squadLeader = entityManager.getAllPlayers().get(packet.leaderId);
        squadId = packet.squadId;
        GameClient.getGame().getLocalPlayer().setSquadId(squadId);
        IoLogger.log("ClientSquad    " + entityManager.getLocalPlayer().getId());
        if(members.size() <= 1 && squadInfo != null) {
            //squadInfo.clear();
        } else {
            squadInfo.setSquadMembers(squadIdList, members);
        }
        GameClient.getGame().getMessagesPanel().message("Squad changed, new size " + members.size() +"" +
                " new leader: " + squadLeader.getUsername() + ", formed - " +formed);
        if(formed) {
            for (Map.Entry<String, PlayerCell> entry : members.entrySet()) {
                squadNames.put(entry.getKey(), entry.getValue().getUsername());
            }
        } else {
            for (Map.Entry<String, PlayerCell> entry : members.entrySet()) {
                if(!squadNames.containsKey(entry.getKey()))
                    squadNames.put(entry.getKey(), entry.getValue().getUsername());
            }
        }
        mFinalMessage.setSquadNames(squadNames);
        return true;
    }


    public boolean isFormed() {
        return formed;
    }

    public void setFormed(boolean formed) {
        this.formed = formed;
    }

    public Map<String, PlayerCell> getMembers() {
        return members;
    }

    public void setMembers(Map<String, PlayerCell> members) {
        this.members = members;
    }

    public PlayerCell getSquadLeader() {
        return squadLeader;
    }

    public void setSquadLeader(PlayerCell squadLeader) {
        this.squadLeader = squadLeader;
    }


    public int getSquadId() {
        return squadId;
    }

    public void setSquadId(int squadId) {
        this.squadId = squadId;
    }

    public SquadInfo getSquadInfo() {
        return squadInfo;
    }

    @Override
    public boolean handlePacket(Packet packet) {


    try{

        if(packet.packetType == SQUAD_CHANGED)
        {
            SquadChangedPacket p = (SquadChangedPacket) packet;
            return assignFromPacket(p);

        }
        return false;
    }catch (Exception e)
    {
        GameClient.getGame().getMasterServer().postExceptionDetails(e, "Gamescreen::handlePacket " + packet.toString());
        Gdx.app.error("handlePacket", packet.toString());
        Gdx.app.error("handlePacket", e.getMessage());
        return false;
    }

    }

    public String[] getSquadIdList() {
        return squadIdList;
    }

    
    public void setFinalMessage(FinalMessage finalMessage) {
        mFinalMessage = finalMessage;
    }
}
