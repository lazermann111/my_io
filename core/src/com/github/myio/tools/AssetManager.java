package com.github.myio.tools;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.Pool;

import java.util.HashMap;

public class AssetManager implements Pool.Poolable {
	
	public static final TextureFilter TEXTURE_FILTER = TextureFilter.Linear;
	
	private HashMap<String, Texture> textureMap;
	private HashMap<String, TextureRegion[][]> textureRegionMap;
	private HashMap<String, Animation<TextureRegion>> animationMap;


	public AssetManager() {
		textureMap = new HashMap<String, Texture>();
		textureRegionMap = new HashMap<String, TextureRegion[][]>();
		animationMap = new HashMap<String, Animation<TextureRegion>>();
	}
	
	/**
	 * Adds a texture without a filter
	 * @param key
	 * @param fileLocation
	 */
	public void putTexture (String key, String fileLocation) {
		putTexture(key, fileLocation, false);
	}
	
	/**
	 * Adds a texture with possibility of a filter
	 * @param key
	 * @param fileLocation
	 * @param applyFilter
	 */
	public void putTexture (String key, String fileLocation, boolean applyFilter) {
		Texture texture = new Texture(fileLocation);
		if (applyFilter)
			applyFilter(texture);
		textureMap.put(key, texture);
	}
	
	public Texture getTexture (String key) {
		return textureMap.get(key);
	}
	
	/**
	 * Adds a texture map without a filter
	 * @param key
	 * @param fileLocation
	 * @param tileWidth
	 * @param tileHeight
	 */
	public void putTextureMap (String key, String fileLocation, int tileWidth, int tileHeight) {
		putTextureMap(key, fileLocation, tileWidth, tileHeight, false);
	}
	
	/**
	 * Adds a texture map with possibility of filter
	 * @param key
	 * @param fileLocation
	 * @param tileWidth
	 * @param tileHeight
	 * @param applyFilter
	 */
	public void putTextureMap (String key, String fileLocation, int tileWidth, int tileHeight, boolean applyFilter) {
		TextureRegion[][] textureMap = TextureRegion.split(new Texture(fileLocation), tileWidth, tileHeight);
		for (TextureRegion[] trArray : textureMap) {
			for (TextureRegion tr : trArray) {
				if (applyFilter)
					applyFilter(tr);
			}
		}
		textureRegionMap.put(key, textureMap);
	}
	
	/**
	 * Returns entire texture map
	 * @param key
	 * @return
	 */
	public TextureRegion[][] getTextureMap (String key) {
		return textureRegionMap.get(key);
	}
	
	/**
	 * Adds an animation without a filter
	 * @param key
	 * @param animation
	 */
	public void putAnimation (String key, Animation animation) {
		putAnimation(key, animation, false);
	}
	
	/**
	 * Adds animation with possibility of filter
	 * @param key
	 * @param animation
	 * @param filter
	 */
	public void putAnimation (String key, Animation<TextureRegion> animation, boolean filter) {
		for (TextureRegion tr : animation.getKeyFrames()) {
			if (filter)
				applyFilter(tr);
		}
		animationMap.put(key, animation);
	}
	
	/**
	 * Get specific from of an animation with no looping
	 * @param key
	 * @param stateTime
	 * @return
	 */
	public TextureRegion getAnimationKeyFrame (String key, float stateTime) {
		return getAnimationKeyFrame(key, stateTime, false);
	}
	
	/**
	 * Get specific from of an animation with possibility of looping
	 * @param key
	 * @param stateTime
	 * @param looping
	 * @return
	 */
	public TextureRegion getAnimationKeyFrame (String key, float stateTime, boolean looping) {
		return animationMap.get(key).getKeyFrame(stateTime, looping);
	}
	
	/**
	 * Applies a linear filter to this texture region
	 * @param tr
	 */
	private void applyFilter (TextureRegion tr) {
		applyFilter(tr.getTexture());
	}
	
	/**
	 * Applies a linear filter to this texture
	 * @param texture
	 */
	private void applyFilter (Texture texture) {
		texture.setFilter(TEXTURE_FILTER, TEXTURE_FILTER);
	}


	@Override
	public void reset() {

	}
}
