package com.github.myio.tools;

import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.github.myio.ui.LoadingBar;

/**
 * Created by Alexsandr on 10.07.2018.
 */

public class LoadingBarPart extends Actor
{
    private TextureAtlas.AtlasRegion image;

    private Animation flameAnimation;
    private LoadingBar loadingBar;


    public LoadingBarPart(TextureAtlas.AtlasRegion ar, Animation an) {
        super();
        image = ar;
      flameAnimation = an;
       // this.loadingBar = loadingBar;
        this.setWidth(400);
        this.setHeight(400);
        this.setVisible(false);
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
    super.draw(batch, parentAlpha);
    batch.draw(image, getX(),getY(), 1080, 400);
    batch.setBlendFunction(GL20.GL_SRC_ALPHA, GL20.GL_ONE);
    batch.draw(currentFrame, getX()-5,getY(), 40, 40);
    batch.setBlendFunction(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
    }

    private float stateTime;
   public TextureRegion currentFrame = new TextureRegion();
    @Override
    public void act(float delta) {
        super.act(delta);
        stateTime += delta; // Accumulate elapsed animation time
       currentFrame = (TextureRegion) flameAnimation.getKeyFrame(stateTime, true);
    }



}
