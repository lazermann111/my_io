package com.github.myio.tools;


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.assets.loaders.SkinLoader;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;

/**
 * Created by Alexsandr on 09.07.2018.
 */

public class LoadAsset {
    public final AssetManager manager = new AssetManager();
    private Skin menuSkin;

    // Sounds

    // Music
    public   String playingSong = "sounds/main_menu_officially.mp3";

    // Skin
    private    String skin = "./skins/uiskin.json";

    // Textures
    public   String gameImages = "atlas/mainMenu/mainMenu.atlas";
    public   String loadingImages = "atlas/mainMenu/newSkin.atlas";
    public   String buttonsPack = "buttons.pack";

   /* public final String playerImages = "atlas/playerSkin/playerSkin.atlas";
    public final String uiImages = "atlas/uiAtlas/uiAtlas.atlas";
    public final String  cellImages = "atlas/cellAtlas/cellAtlas.atlas";*/

    //Fonts
    public   String font32W  = "fonts/font32White.fnt";
    public   String font32B = "fonts/font32Black.fnt";
    public   String  font32T = "fonts/trebushet2.fnt";
  //  public  final String attractive = "fonts/Attractive-Regular.ttf";

    public void queueAddFonts(){
         FileHandle[] fileArray =  Gdx.files.internal("fonts/").list();
        for(int i=0; i<fileArray.length; i++){
            //if it is not a directory
            if(!(fileArray[i].isDirectory())){
                String stringPath = fileArray[i].path();
                //load file
                manager.load(stringPath, BitmapFont.class);
            }
        }
        IoLogger.log("Loading fonts.....");
     /*   manager.load(font32W, BitmapFont.class);
        manager.finishLoading();
        manager.load(font32B, BitmapFont.class);
        manager.finishLoading();
        manager.load(font32T, BitmapFont.class);
        manager.finishLoading();
     //   manager.load(attractive, BitmapFont.class);*/

    }

    public void queueAddParticleEffects(){
      FileHandle[] fileArray =  Gdx.files.internal("/particles/").list();
        for(int i=0; i<fileArray.length; i++){
            //if it is not a directory
            if(!(fileArray[i].isDirectory())){
                String stringPath = fileArray[i].path();
                //load file
                IoLogger.log("Loading Particles......");

                manager.load(stringPath, ParticleEffect.class);
            }
        }
    }
    private Image loadingFrame;
    private Image loadingBarHidden;
    private Image screenBg;
    private Image loadingBg;

    public void queueAddImages(){

        manager.load(gameImages, TextureAtlas.class);
        manager.finishLoading();

    }

    // a small set of images used by the loading screen
    public void queueAddLoadingImages(){
        manager.load(loadingImages, TextureAtlas.class);

        System.out.println("Loading buttonsPack...");
        manager.load(buttonsPack, TextureAtlas.class);
        System.out.println(" Finish Loading buttonsPack...");
        FileHandle[] btnArray =  Gdx.files.internal("btn/").list();
        for(int i=0; i<btnArray.length; i++){
            //if it is not a directory
            if(!(btnArray[i].isDirectory())){
                String stringPath = btnArray[i].path();
                //load file
                IoLogger.log("Load btn....");

                manager.load(stringPath, Texture.class);
                manager.finishLoading();
            }
        }
      /*  FileHandle[] fontArray =  Gdx.files.internal("fonts/").list();
        for(int i=0; i<fontArray.length; i++){
            //if it is not a directory
            if(!(fontArray[i].isDirectory())){
                String stringPath = fontArray[i].path();
                //load file
                IoLogger.log("Load fonts png....");
                manager.load(stringPath, Texture.class);
            }
        }*/

        FileHandle[] fileArray =  Gdx.files.internal("atlas/playerSkin/").list("atlas");
        for(int i=0; i<fileArray.length; i++){
            //if it is not a directory
            if(!(fileArray[i].isDirectory())){
                String stringPath = fileArray[i].path();
                //load file
                IoLogger.log("Loading player skins");
                manager.load(stringPath, TextureAtlas.class);
                manager.finishLoading();
            }
        }
        FileHandle[] patronArray =  Gdx.files.internal("atlas/uiAtlas/").list("atlas");
        for(int i=0; i<patronArray.length; i++){
            //if it is not a directory
            if(!(patronArray[i].isDirectory())){
                String stringPath = patronArray[i].path();
                //load file
                IoLogger.log("Loading  uiatala");
                manager.load(stringPath, TextureAtlas.class);
                manager.finishLoading();
            }
        }
        FileHandle[] mainMenuArray =  Gdx.files.internal("/atlas/mainMenu/").list();
        for(int i=0; i<mainMenuArray.length; i++){
            //if it is not a directory
            if(!(mainMenuArray[i].isDirectory())){
                String stringPath = mainMenuArray[i].path();
                //load file
                IoLogger.log("Loading atlas mainMenu");
                manager.load(stringPath, TextureAtlas.class);
            }
        }

        FileHandle[] cellArray =  Gdx.files.internal("atlas/cellAtlas/").list();
        for(int i=0; i<patronArray.length; i++){
            //if it is not a directory
            if(!(cellArray[i].isDirectory())){
                String stringPath = cellArray[i].path();
                //load file
                IoLogger.log("Loading cellAtlas......");

                manager.load(stringPath, TextureAtlas.class);
                manager.finishLoading();
            }
        }

        FileHandle[] patronsArray =  Gdx.files.internal("atlas/patrons/").list();
        for(int i=0; i<patronsArray.length; i++){
            //if it is not a directory
            if(!(patronsArray[i].isDirectory())){
                String stringPath = patronsArray[i].path();
                //load file
                IoLogger.log("Loading patrons png......");

                manager.load(stringPath, Texture.class);
                manager.finishLoading();
            }
        }

        FileHandle[] skinArray =  Gdx.files.internal("/skins/").list();
        for(int i=0; i<skinArray.length; i++){
            //if it is not a directory
            if(!(skinArray[i].isDirectory())){
                String stringPath = skinArray[i].path();
                //load file
                IoLogger.log("Loading png skins from LoadAsset");
                manager.load(stringPath, Texture.class);
            }
        }
       /* manager.load(playerImages, TextureAtlas.class);
        manager.load(uiImages, TextureAtlas.class);
        manager.load(cellImages, TextureAtlas.class);*/
    }

    public void queueAddSkin(){
        //skin = new Skin(Gdx.files.internal("atlas/default/skin/uiskin.json"), new TextureAtlas("atlas/default/skin/uiskin.atlas"));
        manager.load("atlas/default/skin/uiskin.json", Skin.class, new SkinLoader.SkinParameter("atlas/default/skin/uiskin.atlas"));
        IoLogger.log("Load skins default");
        manager.finishLoading();
      /*  if (menuSkin == null)
        menuSkin = new Skin(Gdx.files.internal("/skins/uiskin.json"),
                manager.get("skins/uiskin.atlas", TextureAtlas.class));*/

       /* manager.load("atlas/default/skin/uiskin.json", Skin.class, new SkinLoader.SkinParameter("atlas/default/skin/uiskin.atlas"));
        IoLogger.log("Load skins default");
        manager.finishLoading();*/
     /* SkinParameter params = new SkinParameter("skins/uiskin.atlas");
        manager.load(skin, Skin.class, params);*/
      /* FileHandle[] fileArray =  Gdx.files.internal("/skins/").list();
        for(int i=0; i<fileArray.length; i++){
            //if it is not a directory
            if(!(fileArray[i].isDirectory())){
                String stringPath = fileArray[i].path();
                System.out.println("Loading skins png.....");
                //load file
                manager.load(stringPath, Texture.class);
                manager.finishLoading();
            }
        }*/
    }

    public void queueAddMusic(){
        manager.load(playingSong, Music.class);
    }

    public void queueAddSounds(){
        FileHandle[] fileArray =  Gdx.files.internal("sounds/").list();
        for(int i=0; i<fileArray.length; i++){
            //if it is not a directory
            if(!(fileArray[i].isDirectory())){
                String stringPath = fileArray[i].path();
                //load file
                manager.load(stringPath, Music.class);
//                manager.finishLoading();
            }
        }
       // manager.load(boingSound, Sound.class);
        //manager.load(pingSound, Sound.class);
    }


}
