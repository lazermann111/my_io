package com.github.myio.tools;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.github.myio.GameClient;
import com.github.myio.dto.CapturablePointDto;
import com.github.myio.entities.map.CapturablePointClient;
import com.github.myio.networking.ClientPacketHandler;
import com.github.myio.networking.Packet;
import com.github.myio.networking.packet.CapturablePointStateChangedPacket;
import com.github.myio.networking.packet.CapturablePointsSnapshot;
import com.github.myio.ui.CountDown;
import com.github.myio.ui.IDrawable;

import java.util.HashMap;
import java.util.Map;

import static com.github.myio.GameConstants.CONQUEST_POINTS_NUMBER;
import static com.github.myio.GameConstants.CONQUEST_RED_TEAM_ID;
import static com.github.myio.networking.PacketType.CAPTURABLE_POINTS_SNAPSHOT;
import static com.github.myio.networking.PacketType.CAPTURABLE_POINT_STATE_CHANGED;

/**
 * Created by Igor on 5/30/2018.
 */

public class ClientCapturablePointsManager  implements ClientPacketHandler, IDrawable
{

    private static final String TAG = "ClientCapturablePointsManager";
    Map<String, CapturablePointClient> capturablePoints = new HashMap<String, CapturablePointClient>();


    //todo (Alex/Taras) remove debug UI, add point list UI
    private  Label label1;
    private  Label label2;
    private  Label label3;
    private  Label label4;
    private  Label label5;

    private  Label[] labels = new Label[CONQUEST_POINTS_NUMBER];

    private Label redPoints;
    private Label bluePoints;

    private Stage stage;
    private Viewport viewport;
    private Table table;

    private Skin skin;

    private int redTeamPoints, blueTeamPoints;

    private CountDown captureProgress = new CountDown();

    private CapturablePointClient lastInteractedPoint;
    private long latInteractionTime;

    public ClientCapturablePointsManager (boolean debug, Skin skin)
    {
        GameClient.getGame().getNetworkManager().addPacketHandler(this);
        this.skin = skin;

        if(debug)
        {
            GameClient game = GameClient.getGame();


            viewport = game.getUiViewport();
            stage = new Stage(viewport, game.getBatch());
            Gdx.input.setInputProcessor(stage);
            table = new Table();
            table.top();
            table.setFillParent(true);

            label1 = new Label("", skin);
            label2 = new Label("", skin);
            label3 = new Label("", skin);
            label4 = new Label("", skin);
            label5 = new Label("", skin);

            labels[0]=label1;
            labels[1]=label2;
            labels[2]=label3;
            labels[3]=label4;
            labels[4]=label5;

            redPoints = new Label("", skin);
            bluePoints = new Label("", skin);

            table.add(label1).expandX().padTop(10).align(Align.right).padRight(10);
            table.row();
            table.add(label2).expandX().padTop(10).align(Align.right).padRight(10);
            table.row();
            table.add(label3).expandX().padTop(10).align(Align.right).padRight(10);
            table.row();
            table.add(label4).expandX().padTop(10).align(Align.right).padRight(10);
            table.row();
            table.add(label5).expandX().padTop(10).align(Align.right).padRight(10);
            table.row();


            table.add(redPoints).expandX().padTop(10).align(Align.right).padRight(10);
            table.row();

            table.add(bluePoints).expandX().padTop(10).align(Align.right).padRight(10);
            table.row();

            stage.addActor(table);
        }
    }

    public void startPointInteraction(CapturablePointClient pointClient)
    {
        latInteractionTime = System.currentTimeMillis();
       // IoLogger.log(TAG, "startPointInteraction ");
        if(lastInteractedPoint != pointClient)
        {
            lastInteractedPoint = pointClient;

            if(pointClient.pointDto == null)
            {
                IoLogger.log(TAG, "startPointInteraction with null pointDto! " + pointClient);
                return;
            }
            Color teamColor = pointClient.pointDto.capturingTeam == CONQUEST_RED_TEAM_ID ? Color.RED : Color.BLUE;

            //todo change for manual updated bar
            captureProgress.showCountDown(teamColor,null,null,10000);
            interactingWithPoint = true;
        }



    }

    public void finishPointInteraction(CapturablePointClient pointClient)
    {
        IoLogger.log(TAG, "finishPointInteraction ");
        lastInteractedPoint = null;
        captureProgress.resetCountDown();
    }

    public void drawCustom(float delta)
    {
        for (CapturablePointClient c : capturablePoints.values())
        {
            c.drawPointRadius(delta);
        }
    }

    @Override
    public boolean handlePacket(Packet packet)
    {
        if(packet.packetType == CAPTURABLE_POINT_STATE_CHANGED)
        {
            CapturablePointStateChangedPacket p = (CapturablePointStateChangedPacket)packet;

            switch (p.newPointState)
            {

                case POINT_CAPTURED:
                    GameClient.getGame().getMessagesPanel().message("POINT_CAPTURED " + p.pointId);
                    break;
                case POINT_CAPTURING_STARTED:
                    GameClient.getGame().getMessagesPanel().message("POINT_CAPTURING_STARTED " + p.pointId);
                    break;
                case POINT_CAPTURING_CANCELED:
                    GameClient.getGame().getMessagesPanel().message("POINT_CAPTURING_CANCELED " + p.pointId);
                    break;
            }

            return true;
        }

        if(packet.packetType == CAPTURABLE_POINTS_SNAPSHOT)
        {
            CapturablePointsSnapshot p = (CapturablePointsSnapshot)packet;

            redTeamPoints = p.redTeamPoints;
            blueTeamPoints = p.blueTeamPoints;

            for (CapturablePointDto pointDto : p.points)
            {
                if (pointDto == null)
                {
                    break;
                }
                if (capturablePoints.containsKey(pointDto.id)) //
                {
                    capturablePoints.get(pointDto.id).pointDto = pointDto;
                }
                else IoLogger.log(TAG, "Update for capturablePoint with unknown id: " + pointDto.id);
            }

            return true;
        }

        return false;
    }


    public void addCapturablePoint(CapturablePointClient pointClient)
    {
        capturablePoints.put(pointClient.getId(), pointClient);
    }

    private boolean interactingWithPoint;

    @Override
    public void draw(float delta)
    {
        if(capturablePoints.isEmpty()) return;


         if(latInteractionTime + 200 < System.currentTimeMillis() && interactingWithPoint)
         {

             finishPointInteraction(lastInteractedPoint);
             interactingWithPoint = false;
         }
        int i =0;
        for (CapturablePointClient pointClient : capturablePoints.values())
        {
            labels[i++].setText(pointClient.toString());
        }


        redPoints.setText("Red : " + redTeamPoints);
        bluePoints.setText("Blue : " + blueTeamPoints);

        stage.act();
        stage.draw();
    }
}
