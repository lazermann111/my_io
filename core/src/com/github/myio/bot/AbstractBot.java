package com.github.myio.bot;


import com.github.myio.EntityManager;
import com.github.myio.GameClient;
import com.github.myio.GameConstants;
import com.github.myio.entities.IoPoint;
import com.github.myio.entities.PlayerCell;
import com.github.myio.networking.ClientPacketHandler;
import com.github.myio.networking.NetworkManager;
import com.github.myio.networking.Packet;
import com.github.myio.networking.PacketType;
import com.github.myio.networking.packet.LoginPacket;
import com.github.myio.networking.packet.LoginResponsePacket;
import com.github.myio.networking.packet.PlayerDiedPacket;
import com.github.myio.tools.IoLogger;

import java.util.Random;

import static com.github.myio.GameConstants.LOCAL_SERVER_PORT;
import static com.github.myio.GameConstants.LOCAL_SERVER_URL;
import static com.github.myio.GameConstants.PLAYERS_MAX_HEALTH;
import static com.github.myio.GameConstants.PLAYERS_MAX_STAMINA;
import static com.github.myio.GameConstants.PRODUCTION_MODE;
import static com.github.myio.GameConstants.PRODUCTION_SERVER_URL_SOLO;
import static com.github.myio.StringConstants.SKIN_DEFAULT;
import static com.github.myio.networking.PacketType.PLAYER_DIED;

public abstract class AbstractBot implements ClientPacketHandler
{
    protected PlayerCell bot;
    protected Random r = new Random();

    protected boolean loggedIn;
    protected boolean requestSent;
    protected boolean died;
    protected long lastHeartbeat;

    protected int id;
    protected String botServerId; // differs from local id, useful for searching in logs
    protected NetworkManager networkManager;
    protected IoPoint initialBotSpawn;

    protected EntityManager entityManager;

    public AbstractBot(int id)
    {
      this.id = id;
      connect();
    }


    protected void connect()
    {
        networkManager = new NetworkManager();
        try {
            if(PRODUCTION_MODE)
            {
                networkManager.connect(PRODUCTION_SERVER_URL_SOLO, 80);
            }
            else
            {
                networkManager.connect(LOCAL_SERVER_URL, LOCAL_SERVER_PORT);
            }
        } catch (java.lang.Exception e) {
            e.printStackTrace();
        }
        networkManager.addPacketHandler(this);
        IoLogger.log(botTag() +" sending login request ");
        networkManager.sendPacket(new LoginPacket(botTag(),SKIN_DEFAULT,""));
    }


    // boolean means if we need to process custom update logic in child classes
    //if method returns false here, we have to stop processing update
    public boolean update()
    {
        networkManager.update();

        if(died) return false;
        if(!loggedIn) return false;


        if(bot.getCurrentHealth() <= 0)
        {
            IoLogger.log(bot.getUsername()+ " Died ");
            networkManager.disconnect();
            died = true;
            return false;
        }
        if(lastHeartbeat + GameConstants.HEARTBEAT_RATE > System.currentTimeMillis()) return true;
        lastHeartbeat = System.currentTimeMillis();
        bot.sendHeartBeat(networkManager);
        return true;

    }


    /**
    * Basic functions - handle login response from server and check if we not dead
    * */
    @Override
    public boolean handlePacket(Packet packet)
    {
        if(packet.packetType == PacketType.LOGIN_RESPONSE)
        {

            LoginResponsePacket p = (LoginResponsePacket) packet;

            bot =  new PlayerCell(null, null, null, p.name, p.x,p.y, 100, p.playerId,  PLAYERS_MAX_HEALTH, PLAYERS_MAX_HEALTH,PLAYERS_MAX_STAMINA, PLAYERS_MAX_STAMINA,p.currentXp,SKIN_DEFAULT);
            initialBotSpawn = new IoPoint(p.x,p.y);


            bot.setDefaultInventory(false,true);
            bot.getInventoryManager().setNetworkManager(networkManager);
            loggedIn = true;


            entityManager = new EntityManager(null,bot,new GameClient(),null,null,null, networkManager);
            IoLogger.log(botTag() +" logged in! with playerID " + p.playerId + " in world " + p.worldId);
            botServerId =  p.playerId;
            return true;
        }

        if(packet.packetType == PLAYER_DIED)
        {
            PlayerDiedPacket p = (PlayerDiedPacket) packet;
            if(bot.getId().equals(p.playerId))
            {
                died = true;
            }
            else  entityManager.getAllPlayers().remove(p.playerId)  ;

        }

        if (packet.packetType == PacketType.HEARTBEAT_DISABLE)
        {
            bot.setCanSendHeartBeats(false);
            return true;
        }

        return false;
    }


    protected void teleport(IoPoint point)
    {
        bot.setPosition(point);
        bot.forceHeartBeat(networkManager);
    }



    protected abstract String botTag();
}

