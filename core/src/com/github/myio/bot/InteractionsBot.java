package com.github.myio.bot;


import com.github.myio.dto.ItemDto;
import com.github.myio.dto.PlayerCellDto;
import com.github.myio.entities.IoPoint;
import com.github.myio.entities.PlayerCell;
import com.github.myio.entities.item.ItemCell;
import com.github.myio.entities.map.MapEntityCell;
import com.github.myio.entities.map.TacticalShield;
import com.github.myio.entities.weapons.AbstractRangeWeapon;
import com.github.myio.enums.ItemType;
import com.github.myio.enums.MapEntityType;
import com.github.myio.enums.WeaponType;
import com.github.myio.networking.Packet;
import com.github.myio.networking.PacketType;
import com.github.myio.networking.packet.CurrentWorldSnapshotPacket;
import com.github.myio.networking.packet.InitialWorldSnapshotPacket;
import com.github.myio.networking.packet.ItemPickedPacket;
import com.github.myio.networking.packet.PitInteractionPacker;
import com.github.myio.networking.packet.PlayerDiedPacket;
import com.github.myio.networking.packet.ShieldInteractionPacket;
import com.github.myio.networking.packet.UseMedkitPacket;
import com.github.myio.networking.packet.WeaponSwitchedPacket;
import com.github.myio.networking.packet.WorldUpdatePacket;
import com.github.myio.tools.IoLogger;

import static com.github.myio.GameConstants.PLAYERS_MAX_HEALTH;
import static com.github.myio.GameConstants.PLAYERS_MAX_STAMINA;
import static com.github.myio.StringConstants.SKIN_DEFAULT;
import static com.github.myio.networking.PacketType.ENTITY_REMOVE;
import static com.github.myio.networking.PacketType.ITEM_PICKED;
import static com.github.myio.networking.PacketType.PIT_INTERACTION;
import static com.github.myio.networking.PacketType.SHIELD_INTERACTION;
import static com.github.myio.networking.PacketType.USE_MEDKIT;
import static com.github.myio.networking.PacketType.WEAPON_SWITCHED;

/**
 * Covers different client/server interactions:
 * using medkit/shield/pit
 * picking items
 *
 * Uses EntityManager only for holding info off all entities/players/items in gameworld
 * todo add all new interactions to this bot
 */

public class InteractionsBot extends AbstractBot {

    private MapEntityCell playerPit;
    private boolean insidePit;
    private long lastInteractionTime;

    public InteractionsBot(int id)
    {
        super(id);

    }


    @Override
    public boolean update() {
        if(!super.update()) return false;

        randomInteraction();
        return true;

    }

    protected long lastInteraction;

    private void randomInteraction()
    {
        if(died) return ;
        if(!loggedIn) return ;

        if(lastInteraction + 500 > System.currentTimeMillis()) return ;
        lastInteraction = System.currentTimeMillis();

        if(r.nextBoolean())
        {
            if(r.nextBoolean())
            {
               pickRandomItem();
            }
            else
            {
                useMedkit();
            }
        }
        else if(r.nextBoolean())
        {
            if(r.nextBoolean())
            {
                shoot();
            }
            else if(r.nextBoolean())
            {
                shoot();
            }
            else
            {
                dropRandomItem();
            }
        }
        else
        if(r.nextBoolean())
        {
            switchWeapon();
        }
        else
        {
            shoot();
        }

    }

    private void pickRandomItem()
    {
        IoLogger.log(botTag()+"pickRandomItem - "+entityManager.getItems().size());
        if(entityManager.getItems().isEmpty())
        {
           IoLogger.log(botTag()+"pickRandomItem no items");
            return;
        }


        ItemCell item = entityManager.getItems().values().iterator().next();
        teleport(item.getPosition());

        bot.consumeItem(networkManager, item);
    }

    private void dropRandomItem()
    {
        IoLogger.log(botTag()+"dropRandomItem - ");

        if(bot.getInventoryManager().getItems().isEmpty())
        {
            IoLogger.log(botTag()+"dropRandomItem no items");
            return;
        }


        bot.getInventoryManager().dropItem(bot.getInventoryManager().getItems().values().iterator().next());



    }

    private void useMedkit()
    {
        IoLogger.log(botTag()+"useMedkit");

        for(ItemCell i : bot.getInventoryManager().getItems().values())
        {
            if (i.getItemType().equals(ItemType.HP))
            {
                bot.healByMedKit(i.getItem_blueprint());
                break;
            }

        }
    }

    //todo add random pit picking , not just first in list
    private void interactWithPit()
    {
       IoLogger.log(botTag()+"interactWithPit");
        if(playerPit !=null)
        {
            teleport(playerPit.getPosition());
            networkManager.sendPacket(new PitInteractionPacker(bot.getId(), insidePit, playerPit.getId()));
            return;
        }
        for (MapEntityCell e : entityManager.getEntities().values())
        {
           if(e.getType().equals(MapEntityType.PIT)||e.getType().equals(MapEntityType.PIT_TRAP2))
           {

               playerPit = e;
               teleport(playerPit.getPosition());
               networkManager.sendPacket(new PitInteractionPacker(bot.getId(), insidePit, playerPit.getId()));
               return;
           }
        }

        insidePit = !insidePit;
    }


    //todo add random shiled picking , not just first in list
    private void interactWithShield()
    {
       IoLogger.log(botTag()+"interactWithShield");
        if(bot.shiled != null)
        {
            bot.shiled.interact(bot, networkManager);
        }
        else
        {
            for (MapEntityCell e : entityManager.getEntities().values())
            {
                if(e instanceof TacticalShield)
                {
                    TacticalShield shield = (TacticalShield) e;
                    if(shield.inUse) continue;

                    teleport(e.getPosition());
                    shield.interact(bot,networkManager);
                    bot.shiled = shield;
                    return;
                }
            }
        }
    }


    private void switchWeapon()
    {

        bot.getInventoryManager().switchToFirstAvailableWeapon();
        networkManager.sendPacket(new WeaponSwitchedPacket(bot.getId(), bot.getCurrentWeapon().weaponType, bot.getWeaponList().indexOf(bot.getCurrentWeapon())));
    }

    private void shoot()
    {
        if(bot.getCurrentWeapon().weaponType.equals(WeaponType.FRAG_GRENADE) || bot.getCurrentWeapon().weaponType.equals(WeaponType.ROCKET)) return; //not testing splash damage
        bot.getCurrentWeapon().setCanShot(true);
        bot.getCurrentWeapon().fire(bot.getPosition(), new IoPoint(), bot.getRotationAngle(), bot.getId(), networkManager);
    }


    @Override
    public boolean handlePacket(Packet packet) {

        try {

            if (packet.packetType == PacketType.WORLD_SNAPSHOT) {
                ///	IoLogger.log("", "WORLD_SNAPSHOT client");
                InitialWorldSnapshotPacket worldSnapshotPacket = (InitialWorldSnapshotPacket) packet;

                for (PlayerCellDto e : worldSnapshotPacket.players) {
                    if (e == null)break;
                    if (e.getId().equals(bot.getId())) continue; // ourselves

                    PlayerCellDto newPlayer = e;

                    PlayerCell enemy = new PlayerCell(null, null, null,
                            newPlayer.getUsername(), newPlayer.getX(), newPlayer.getY(),
                            100, e.getId(),PLAYERS_MAX_HEALTH,(int)PLAYERS_MAX_STAMINA, PLAYERS_MAX_STAMINA,newPlayer.getCurrentHealth(), 0,SKIN_DEFAULT);
                    enemy.setRotationAngle(newPlayer.getRotationAngle());
                    if(newPlayer.getCurrentWeapon() != null)
                        enemy.setCurrentWeapon(AbstractRangeWeapon.Factory(newPlayer.getCurrentWeapon().weaponType, null, enemy) );
                    entityManager.addNewPlayer(enemy);

                }
           //    IoLogger.log(botTag() + " received " + entityManager.getEnemyCells().size() + " enemies");


              //  if (worldSnapshotPacket.items != null && worldSnapshotPacket.entities != null ){
                    entityManager.setItems(worldSnapshotPacket.items);
                    entityManager.setEntities(worldSnapshotPacket.entities);
              //  }

               IoLogger.log(botTag() + " received " + entityManager.getItems().size() + " items");
               IoLogger.log(botTag() + " received " + entityManager.getEntities().size() + " entities");
                return true;
            }


            if (packet.packetType == PacketType.WORLD_UPDATE) {

                WorldUpdatePacket p = (WorldUpdatePacket) packet;
                //IoLogger.log(botTag() +"WORLD_UPDATE " + packet.toString());
                if (p.joinedPlayers != null) {
                    for (PlayerCellDto newPlayer : p.joinedPlayers) {
                        if (newPlayer == null || newPlayer.getId().equals(bot.getId())) continue;
                        PlayerCell enemy = new PlayerCell(null, null,null, newPlayer.getUsername(), newPlayer.getX(), newPlayer.getY(),
                                100, newPlayer.getId(),PLAYERS_MAX_HEALTH,(int)PLAYERS_MAX_STAMINA,PLAYERS_MAX_STAMINA, newPlayer.getCurrentHealth(),  0,SKIN_DEFAULT);
                        enemy.setRotationAngle(newPlayer.getRotationAngle());

                        entityManager.addNewPlayer(enemy);
                        //       IoLogger.log(botTag() + " added " + entityManager.getEnemyCells().size() + " enemies");
                    }
                }


                entityManager.setPlayersData(p.playersData, false);

                entityManager.setEntitiesData(p.changedEntitiesData);

                if (p.pickedItems != null)
                    for (String s : p.pickedItems)
                    {
                        if(s == null) break;
                        entityManager.removeItem(s);
                    }

                if (p.droppedItems != null)
                    for (ItemDto s : p.droppedItems)
                    {   if(s == null) break;
                        entityManager.addNewItem(s);
                    }
                if (p.leftPlayers != null) {
                    for (String disconnected : p.leftPlayers) {
                        if(disconnected == null) break;
                        entityManager.removePlayer(disconnected);
                    }
                }

                return true;
            }


            if(packet.packetType == USE_MEDKIT)
            {
                UseMedkitPacket p = (UseMedkitPacket) packet;
                if(!bot.getId().equals(p.mPlayerId))
                {
                    PlayerCell e = entityManager.getEnemyCells().get(p.mPlayerId);
                    if(e == null)
                    {
                        bot.requestWorldSnapshot(networkManager);
                    }
                    else
                    {
                        e.healByMedKit(p.medkitBlueprint);
                    }

                    //e.setCurrentHealth(p.medkitBlueprint);

                }
                return true;
            }

            if (packet.packetType == PacketType.CURRENT_WORLD_SNAPSHOT)
            {
                entityManager.applyCurrentWorldSnapshot((CurrentWorldSnapshotPacket)packet);
                return true;
            }

            if(packet.packetType == ENTITY_REMOVE)
            {
                PlayerDiedPacket p = (PlayerDiedPacket) packet;
                if(entityManager.getEntityWithEffect().contains(entityManager.getEntities().get(p.playerId))) {
                    entityManager.removeSpecialEffectFromPlayer(entityManager.getEntities().get(p.playerId));
                }
                entityManager.removeEntity(p.playerId);

                return true;
            }

            if(packet.packetType == ITEM_PICKED)
            {
                ItemPickedPacket p = (ItemPickedPacket) packet;
               // IoLogger.log("ITEM_PICKED ", "p.itemId " + p.itemId);
               // IoLogger.log("ITEM_PICKED ", "item " + entityManager.getItems().get(p.itemId));
                if( entityManager.getItems().get(p.itemId) != null) {
                    if (p.playerId.equals(bot.getId()))
                    {
                       bot.getInventoryManager().pickItem(entityManager.getItems().get(p.itemId));
                    }
                    entityManager.removeItem(p.itemId);
                }
                else
                   IoLogger.log("handlePacket()", "We got null item");

                return true;
            }

            if(packet.packetType == WEAPON_SWITCHED)
            {
                WeaponSwitchedPacket p = (WeaponSwitchedPacket) packet;
                if(!bot.getId().equals(p.playerId))
                {
                    PlayerCell e = entityManager.getEnemyCells().get(p.playerId);
                    if(e == null)
                    {
                        bot.requestWorldSnapshot(networkManager);
                    }
                    else
                    {
                        e.setEnemyCurrentWeapon(p.typeToSwitchTo, e);
                    }

                    //IoLogger.log("GameScreen", p.toString());
                }
                return true;
            }


            if(packet.packetType == SHIELD_INTERACTION)
            {

                ShieldInteractionPacket p = (ShieldInteractionPacket) packet;

                PlayerCell player = entityManager.getAllPlayers().get(p.playerId);
                TacticalShield shield = (TacticalShield) entityManager.getEntities().get(p.shiledId);

                if(player != null && shield != null)
                {
                    if(p.equipped)
                    {
                        shield.startInteractionClient(player);
                    }
                    else
                    {
                        shield.finishInteractionClient(player);
                    }
                }

                return true;
            }

            if(packet.packetType == PIT_INTERACTION) {
                PitInteractionPacker p = (PitInteractionPacker) packet;

                PlayerCell player = entityManager.getEnemyCells().get(p.playerId);
                MapEntityCell pit = entityManager.getEntities().get(p.pitId);

                if(player != null && pit != null) {
                    if(p.insidePit) {
                        pit.getPlayersId().add(p.playerId);
                        player.setVisible(false);
                        player.pit = p.pitId;
                        player.setInsidePit(true);
                  //     IoLogger.log("Player entered pit");
                    }
                    else {
                        pit.getPlayersId().remove(p.playerId);
                        player.setVisible(true);
                        player.pit = null;
                        player.setInsidePit(false);
                  //     IoLogger.log("Player left pit");
                    }
                }

                return true;
            }
            return super.handlePacket(packet);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return false;
        }


    }

    @Override
    protected String botTag() {
         return "InteractionsBot bot " + id +" / " + botServerId;
    }
}

