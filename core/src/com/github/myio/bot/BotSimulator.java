//package com.github.myio.bot;
//
////import com.github.czyzby.websocket.CommonWebSockets;
//
//import com.github.czyzby.websocket.CommonWebSockets;
//import com.github.myio.dto.masterserver.GameType;
//import com.github.myio.tools.IoLogger;
//
//import java.util.List;
//import java.util.concurrent.CopyOnWriteArrayList;
//import java.util.concurrent.ExecutorService;
//import java.util.concurrent.Executors;
//
//import static com.github.myio.GameConstants.isBotSimulator;
//
///**
// * Populates server with bots
// */
//
//public class BotSimulator
//{
//    private int botAmount;
//    private ExecutorService executor ;
//    private List<AbstractBot> botList;
//
//    public BotSimulator(int botAmount) {
//        this.botAmount = botAmount;
//    }
//
//
//    public void run()
//    {
//        executor = Executors.newFixedThreadPool(2);
//
//        botList = new CopyOnWriteArrayList<AbstractBot>();
//
//       executor.execute(new Runnable() {
//            @Override
//            public void run() {
//                int i = 0;
//
//                //  i += teamBotBasicTest();
//                //  teamBotDiffServersTest();
//                while (i < botAmount) {
//                   // botList.add(new MovingBot(i++));
//                  //  botList.add(new DisconnecterBot(i++));
//
//                        botList.add(new InteractionsBot(i++));
//                    try {
//                        Thread.sleep(500);
//                    } catch (InterruptedException e) {
//                        IoLogger.log("BotSimulator " + e.getMessage());
//                    }
//                }}});
//
//
//        while (true) {
//            for (AbstractBot bot : botList) bot.update();
//        }
//    }
//
//
//    /**
//     *
//     *
//     *  Set these const as follows before this test:
//     *   MIN_PLAYERS_TO_START_GAME = 2;
//     MAX_PLAYERS_IN_INSTANCE = 5;
//     GAME_WORLD_PER_SERVER = 3;
//     *
//     *
//     */
//
//    private void teamBotDiffServersTest()
//    {
//
//        try {
//            botList.add(new TeamBot(1, true,  GameType.SQUAD,3, "1", true));
//            botList.add(new TeamBot(2, false, GameType.SQUAD,3, "1", true));
//            botList.add(new MovingBot(111));
//            botList.add(new MovingBot(111));
//            botList.add(new MovingBot(111));
//            //1st server is full now
//
//            botList.add(new MovingBot(111));
//            //  Thread.sleep(100);
//            botList.add(new TeamBot(3, false, GameType.SQUAD,3, "2", true)); //he should connect to world 0, squad is formed now
//            botList.add(new TeamBot(4, true, GameType.SQUAD,2, "2", true));
//            //   Thread.sleep(100);
//            botList.add(new MovingBot(222));
//            botList.add(new MovingBot(333));
//
//
//            botList.add(new MovingBot(444));
//            botList.add(new MovingBot(555));
//            //  Thread.sleep(100);
//            botList.add(new TeamBot(5, false, GameType.SQUAD,2, "2", true)); //he should connect to world 1
//            botList.add(new MovingBot(666)); // they shouldnt team 2, since team 2 is @ 2nd server
//            botList.add(new MovingBot(777));
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//
//    private int teamBotBasicTest()
//    {
//        // squad autofill scenario
//        botList.add(new TeamBot(1, true,  GameType.SQUAD,3, "1", true));
//        botList.add(new TeamBot(2, false, GameType.SQUAD,3, "1", true));
//        botList.add(new MovingBot(4)); // should move to another squad
//        botList.add(new TeamBot(5, false, GameType.SQUAD,3, "1", true));
//        botList.add(new MovingBot(6)); // should join this squad, since autofill = true and 3 members already joined
//
//        // 2 teamleads scenario
//        botList.add(new TeamBot(7, true,  GameType.SQUAD, 3, "2", true));
//        botList.add(new TeamBot(8, true,  GameType.SQUAD, 3, "2", true)); //shouldnt became new teamlead
//        botList.add(new TeamBot(9, false, GameType.SQUAD, 3, "2", true));
//
//        // no teamleads scenario
//        botList.add(new TeamBot(11, false,  GameType.SQUAD, 3, "21", true));
//        botList.add(new TeamBot(12, false,  GameType.SQUAD, 3, "21", true));
//        botList.add(new TeamBot(13, false,  GameType.SQUAD, 3, "21", true));
//
//
//        // teamlead joins last scenario
//        botList.add(new TeamBot(14, false,  GameType.SQUAD, 3, "2", true));
//        botList.add(new TeamBot(15, false,  GameType.SQUAD, 3, "22", true));
//        botList.add(new TeamBot(16, false,  GameType.SQUAD, 3, "22", true));
//
//        // too much team members scenario
//        botList.add(new TeamBot(17, true,  GameType.SQUAD, 3,  "3", true));
//        botList.add(new TeamBot(18, false, GameType.SQUAD, 3,  "3", true));
//        botList.add(new TeamBot(19, false, GameType.SQUAD, 3,  "3", true));
//        botList.add(new TeamBot(20, false, GameType.SQUAD, 3,  "3", true));
//        botList.add(new TeamBot(221, false, GameType.SQUAD, 3,  "3", true));
//
//        // too much team members scenario 2
//        botList.add(new TeamBot(21, true,  GameType.SQUAD, 5,  "31", true));
//        botList.add(new TeamBot(22, false, GameType.SQUAD, 5,  "31", true));
//        botList.add(new TeamBot(23, false, GameType.SQUAD, 5,  "31", true));
//        botList.add(new TeamBot(24, false, GameType.SQUAD, 5,  "31", true));
//        botList.add(new TeamBot(25, false, GameType.SQUAD, 5,  "31", true));
//
//
//        // no autofill scenario
//        botList.add(new TeamBot(31, true,  GameType.SQUAD, 3,  "3", false));
//        botList.add(new TeamBot(32, false, GameType.SQUAD, 3,  "3", false));
//        botList.add(new TeamBot(33, false, GameType.SQUAD, 3,  "3", false));
//        botList.add(new MovingBot(222)); // shouldnt join this squad
//
//        // waiting for buddies scenario :D
//        botList.add(new TeamBot(41, true,  GameType.SQUAD, 4,  "4", false));
//        botList.add(new TeamBot(42, false, GameType.SQUAD, 4,  "4", false));
//        botList.add(new MovingBot(444)); // shouldnt join this squad
//        botList.add(new MovingBot(555)); // shouldnt join this squad
//
//
//
//
//        return botList.size();
//    }
//
//
//    public static void main(String[] args) {
//
//        int botNumber =50;
//        if(args.length > 0)
//        {
//            botNumber= Integer.parseInt(args[0]);
//        }
//        isBotSimulator = true;
//        CommonWebSockets.initiate(); // todo comment it if GWT fails
//        new BotSimulator(botNumber).run();
//    }
//}
