package com.github.myio.bot;

import com.github.myio.dto.masterserver.GameType;
import com.github.myio.tools.ClientSquad;
import com.github.myio.networking.NetworkManager;
import com.github.myio.networking.Packet;
import com.github.myio.networking.packet.TeamLoginPacket;
import com.github.myio.tools.IoLogger;

import static com.github.myio.GameConstants.LOCAL_SERVER_PORT;
import static com.github.myio.GameConstants.LOCAL_SERVER_URL;
import static com.github.myio.GameConstants.PRODUCTION_MODE;

import static com.github.myio.GameConstants.PRODUCTION_SERVER_URL_DUO;
import static com.github.myio.StringConstants.SKIN_DEFAULT;

/**
 *  Bot for testing team squads and ClientSquad functionality
 */

public class TeamBot extends AbstractBot {

   private ClientSquad clientSquad;


    public boolean isTeamLeader;
    public GameType gameType;
    public int teamSize;
    public String teamId;
    public boolean autoFillEnabled;

    public TeamBot(int id) {
        super(id);

        clientSquad = new ClientSquad(entityManager, networkManager);
    }

    public TeamBot(int id, boolean isTeamLeader, GameType gameType, int teamSize, String teamId, boolean autoFillEnabled) {
        super(id);
        this.isTeamLeader = isTeamLeader;
        this.gameType = gameType;
        this.teamSize = teamSize;
        this.teamId = teamId;
        this.autoFillEnabled = autoFillEnabled;

        clientSquad = new ClientSquad(entityManager, networkManager);
        connect2();

    }


    private void connect2()
    {
        networkManager = new NetworkManager();
        try {
            if(PRODUCTION_MODE)
            {
                networkManager.connect(PRODUCTION_SERVER_URL_DUO, 80);
            }
            else
            {
                networkManager.connect(LOCAL_SERVER_URL, LOCAL_SERVER_PORT);
            }
        } catch (java.lang.Exception e) {
            e.printStackTrace();
        }
        networkManager.addPacketHandler(this);
        IoLogger.log("Bot " + id +" sending login request ");
        networkManager.sendPacket(new TeamLoginPacket(isTeamLeader, gameType,teamSize,teamId,autoFillEnabled, botTag(), botTag(),SKIN_DEFAULT));
    }

    @Override
    protected void connect()
    {
        networkManager = new NetworkManager();
    }

    @Override
    public boolean update() {
        return super.update();


    }

    @Override
    public boolean handlePacket(Packet packet) {

        return super.handlePacket(packet);
    }

    @Override
    public String toString() {
        return botTag() + super.toString();
    }

    @Override
    protected String botTag() {
        return "TeamBot " + id+" / " + botServerId;
    }
}
