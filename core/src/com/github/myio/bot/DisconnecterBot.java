package com.github.myio.bot;


public class DisconnecterBot extends AbstractBot   {



    public DisconnecterBot(int id)
    {
        super(id);
    }
    private boolean connected;



    private void disconnect(){
        networkManager.disconnect();
        loggedIn= false;
        connected = false;
    }


    @Override
    public boolean update()
    {
        if(!super.update()) return false;
        if(connected)
        {
            disconnect();
            connect();
        }

        return true;
    }

    @Override
    protected String botTag() {
        return "Disconnecter bot " + id +" /" + botServerId;
    }

}
