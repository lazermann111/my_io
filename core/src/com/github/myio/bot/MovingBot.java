package com.github.myio.bot;


import com.badlogic.gdx.math.Vector2;
import com.github.myio.GameClient;
import com.github.myio.entities.IoPoint;
import com.github.myio.networking.Packet;

import static com.github.myio.GameConstants.PLAYER_INITIAL_SPEED;



/*
* Moving in horizontal or vertical axis with constant speed, used for delay/interpolation tests
* */
public class MovingBot extends AbstractBot {


    public MovingBot(int id)
    {
        super(id);
    }

    private boolean moveHorizontally = r.nextBoolean();
    private IoPoint pointToMove;

    @Override
    public boolean update()
    {
        if(!super.update()) return false;

            /*if (moveHorizontally)
                moveHorizontally();
            else moveVertically();*/

            moveToPoint();

        return true;
    }


    private long lastMoveTime;
    private void moveToPoint()
    {
        if(lastMoveTime + 16 > System.currentTimeMillis()) return ; //60 fps
        lastMoveTime = System.currentTimeMillis();
        if(pointToMove == null) {
            pointToMove = initialBotSpawn.add(new Vector2(GameClient.nextFloat(0,5000), GameClient.nextFloat(0,5000)));
        }

        if(bot.getPosition().distance(pointToMove) < 5){
            pointToMove = new IoPoint(GameClient.nextFloat(0,5000), GameClient.nextFloat(0,5000));
            bot.setRotationAngle((int) (bot.getRotationAngle() + GameClient.nextFloat(20,120)));
        }



        Vector2 move =  pointToMove.toVector2().sub(bot.getPosition().toVector2()).setLength(PLAYER_INITIAL_SPEED);


        bot.x += move.x;
        bot.y += move.y;
    }
    /*
        private void moveHorizontally()
        {
            if(Math.abs(initialBotSpawn.x - bot.x) > 1000)
            bot.setXMinus(PLAYER_INITIAL_SPEED);
            else if(Math.abs(initialBotSpawn.x - bot.x) < 100)
            bot.setXPlus(PLAYER_INITIAL_SPEED);
        }

        private void moveVertically()
        {
            if(Math.abs(initialBotSpawn.y - bot.y)  > 4000)
                bot.setYMinus(PLAYER_INITIAL_SPEED);
            else if(Math.abs(initialBotSpawn.y - bot.y) < 200)
                bot.setYPlus(PLAYER_INITIAL_SPEED);
        }
    */
    @Override
    public boolean handlePacket(Packet packet) {

        return super.handlePacket(packet);
    }


    @Override
    protected String botTag() {
        return "MovingBot bot " + id +"/" + botServerId;
    }
}
