//package com.github.myio.bot;
//
//
//import com.github.myio.entities.IoPoint;
//import com.github.myio.entities.weapons.AbstractRangeWeapon;
//import com.github.myio.enums.WeaponType;
//import com.github.myio.networking.Packet;
//import com.github.myio.networking.packet.WeaponSwitchedPacket;
//
//
///*
//* Aiming , shooting and switching weapons
//* */
//
//public class ShootingBot extends AbstractBot {
//
//
//    public ShootingBot(int id)
//    {
//        super(id);
//
//    }
//
//
//     private long lastShotTime;
//    @Override
//    public boolean update()
//    {
//        if(!super.update()) return false;
//
//        if(lastShotTime + 500 > System.currentTimeMillis()) return true;
//        lastShotTime = System.currentTimeMillis();
//        aim();
//        shoot();
//
//        switchWeapon();
//
//        return true;
//    }
//
//    //todo
//    private void aim()
//    {
//
//    }
//
//
//    private void switchWeapon()
//    {
//        int curIndex = bot.getWeaponList().indexOf(bot.getCurrentWeapon());
//        curIndex--;
//        if(curIndex <0 )
//        {
//            AbstractRangeWeapon w =  bot.getWeaponList().get(bot.getWeaponList().size()-1);
//            bot.setCurrentWeapon(w);
//        }
//        else
//        {
//            AbstractRangeWeapon w =bot.getWeaponList().get(curIndex);
//            bot.setCurrentWeapon(w);
//
//        }
//       networkManager.sendPacket(new WeaponSwitchedPacket(bot.getId(), bot.getCurrentWeapon().weaponType, bot.getWeaponList().indexOf(bot.getCurrentWeapon())));
//    }
//
//    private void shoot()
//    {
//        if(bot.getCurrentWeapon().weaponType.equals(WeaponType.FRAG_GRENADE) || bot.getCurrentWeapon().weaponType.equals(WeaponType.ROCKET)) return; //not testing splash damage
//        bot.getCurrentWeapon().setCanShot(true);
//        bot.getCurrentWeapon().fire(bot.getPosition(), new IoPoint(), bot.getRotationAngle(), bot.getId(), networkManager);
//    }
//
//    @Override
//    public boolean handlePacket(Packet packet) {
//
//        return super.handlePacket(packet);
//    }
//
//    @Override
//    protected String botTag() {
//        return "ShootingBot bot " + id +" /" + botServerId;
//    }
//}
