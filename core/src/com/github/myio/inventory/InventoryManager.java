package com.github.myio.inventory;


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Camera;
import com.github.myio.GameClient;
import com.github.myio.StringConstants;
import com.github.myio.entities.IoPoint;
import com.github.myio.entities.PlayerCell;
import com.github.myio.entities.item.ItemCell;
import com.github.myio.entities.weapons.AbstractRangeWeapon;
import com.github.myio.entities.weapons.FlashBang;
import com.github.myio.entities.weapons.Grenade;
import com.github.myio.enums.ItemType;
import com.github.myio.enums.WeaponType;
import com.github.myio.networking.NetworkManager;
import com.github.myio.networking.packet.ItemDroppedPacket;
import com.github.myio.networking.packet.WeaponSwitchedPacket;
import com.github.myio.tools.IoLogger;
import com.github.myio.tools.SoundManager;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.github.myio.GameConstants.MAX_QUANTITY_HARDPATRON;
import static com.github.myio.GameConstants.MAX_QUANTITY_MEDIUMPATRON;
import static com.github.myio.GameConstants.MAX_QUANTITY_MINIHARDPATRON;
import static com.github.myio.GameConstants.MAX_QUANTITY_ROCKET;
import static com.github.myio.GameConstants.MAX_QUANTITY_SMALLPATRON;
import static com.github.myio.StringConstants.HARD_PATRON;
import static com.github.myio.StringConstants.HP_100;
import static com.github.myio.StringConstants.HP_30;
import static com.github.myio.StringConstants.HP_60;
import static com.github.myio.StringConstants.MEDIUM_PATRON;
import static com.github.myio.StringConstants.MINI_HARD_PATRON;
import static com.github.myio.StringConstants.ROCKET_PATRON;
import static com.github.myio.StringConstants.SMALL_PATRON;
import static com.github.myio.entities.item.ItemCell.DEFAULT_AMMO_AMOUNT;
import static com.github.myio.enums.ItemType.HP;

public class InventoryManager
{

    private static final java.lang.String TAG = "InventoryManager";
    private InventoryPanel mInventoryPanel;
    private NetworkManager networkManager;
    private InventoryItemSlot active;
    public static final int MAX_NUMBER_OF_MEDKITS = 1;

    private PlayerCell player;

    private InventoryItemSlot primaryWeaponSlot;
    private InventoryItemSlot secondaryWeaponSlot;

    private int medKits;

    private InventoryItemSlot knifeSlot;
    private InventoryItemSlot medKitSlot;
    private InventoryItemSlot fragGrenadeSlot;
    private InventoryItemSlot flashbangSlot;

    private InventoryItemSlot smallPatronSlot;
    private InventoryItemSlot mediumPatronSlot;
    private InventoryItemSlot rocketAmmoSlot;
    private InventoryItemSlot hardPatronSlot;
    private InventoryItemSlot miniHardPatronSlot;

    private List<InventoryItemSlot> weaponSlots;
    private List<InventoryItemSlot> ammoSlots;

    //private ArrayList<ItemCell> items = new ArrayList<ItemCell>();

    private boolean hasVisuals;
    private boolean isClient;

    protected List<AbstractRangeWeapon> weaponList = new ArrayList<AbstractRangeWeapon>();
    protected AbstractRangeWeapon currentWeapon;

    private Map<String, ItemCell> items = new HashMap<String, ItemCell>(); // id <--> item

    private ArrayList<Integer> patron = new ArrayList<Integer>(90);//patron in backpack
    private ItemCell previousItem = null;

    private int smallPatron = 0;    // AK-47               count 90
    private int mediumPatron = 0;   // pistol, machinegun  count 90
    private int hardPatron = 0;     // shotgun             count 25
    private int miniHardPatron = 0; // rocketLauncher          count 24
    private int rocketAmmo = 0;     // bazooka             count 2


    public InventoryManager(PlayerCell localPlayer, boolean hasVisuals, boolean isClient, List<AbstractRangeWeapon> weapons)
    {
        this.hasVisuals = hasVisuals;
        this.isClient = isClient;
        player = localPlayer;
        weaponSlots = new ArrayList<InventoryItemSlot>();
        ammoSlots = new ArrayList<InventoryItemSlot>();
        medKits = 0;
        weaponList = weapons;
        initSLots();
    }

    public void initSLots()
    {
        primaryWeaponSlot = new InventoryItemSlot(hasVisuals);
        secondaryWeaponSlot = new InventoryItemSlot(hasVisuals);
        knifeSlot = new InventoryItemSlot(hasVisuals);
        fragGrenadeSlot = new InventoryItemSlot(hasVisuals);
        flashbangSlot = new InventoryItemSlot(hasVisuals);
        medKitSlot = new InventoryItemSlot(hasVisuals);


        weaponSlots.add(primaryWeaponSlot);
        weaponSlots.add(secondaryWeaponSlot);
        weaponSlots.add(knifeSlot);
        weaponSlots.add(fragGrenadeSlot);
        weaponSlots.add(flashbangSlot);
        weaponSlots.add(medKitSlot);

        fragGrenadeSlot.setGrenade(true);
        flashbangSlot.setGrenade(true);

        setSlotFromWeapon(knifeSlot, weaponList.get(2), null);
        active = knifeSlot;

        smallPatronSlot = new InventoryItemSlot(hasVisuals);
        mediumPatronSlot = new InventoryItemSlot(hasVisuals);
        hardPatronSlot = new InventoryItemSlot(hasVisuals);
        miniHardPatronSlot = new InventoryItemSlot(hasVisuals);
        rocketAmmoSlot = new InventoryItemSlot(hasVisuals);
        ammoSlots.add(smallPatronSlot);
        ammoSlots.add(mediumPatronSlot);
        ammoSlots.add(hardPatronSlot);
        ammoSlots.add(miniHardPatronSlot);
        ammoSlots.add(rocketAmmoSlot);
    }

    public List<InventoryItemSlot> getWeaponSlots()
    {
        return weaponSlots;
    }

    public List<InventoryItemSlot> getAmmoSlots()
    {
        return ammoSlots;
    }

    public void removeItem(ItemCell item)
    {

    }

    public void pickItem(ItemCell item)
    {
        if (!item.getItemType().equals(ItemType.AMMUNITION) && !item.getItemType().equals(ItemType.ARMOR) ){
            items.put(item.getId(), item);
        }
        switch (item.getItemType())
        {
            case HP:
                medKits++;
                setSlotFromGoods(medKitSlot, item);
                break;
            case PATRON:
                updateAmmo(item.getItem_blueprint(), item.getPatron());
                setSlotFromAmmo(getAmmoSlot(item.getItem_blueprint()) , item);
                break;
            case WEAPON:
                processWeaponPicking(item);
                break;
        }
    }

    public void dropItem(ItemCell item)
    {
        //i.e. knife, we cant drop it
        if (item == null)
        { return; }

        items.remove(item.getId());
        switch (item.getItemType())
        {
            case HP:
                medKits--;
               // if (medKits != 0)
               // { medKitSlot.amount--; } todo add medkits amount
                //else
                { medKitSlot.clear(); }

                break;
            case PATRON:
                InventoryItemSlot ammoSlotToClear = null;
                for (InventoryItemSlot s : ammoSlots)
                {
                    if (item.equals(s.item))
                    {
                        item.totalAmmoAmount = s.amount;
                        item.clipAmmoAmount = s.amount;
                        ammoSlotToClear = s;
                        break;
                    }
                }

                if (ammoSlotToClear != null){ammoSlotToClear.clear(); }
                break;
            case WEAPON:
                InventoryItemSlot slotToClear = null;
                for (InventoryItemSlot s : weaponSlots)
                {
                    if (item.equals(s.item))
                    {
                        item.totalAmmoAmount = s.weapon.totalAmmoAmount;
                        item.clipAmmoAmount = s.weapon.clipAmmoAmount;
                        slotToClear = s;
                        break;
                    }
                }

                if (slotToClear != null){ removeSlotFromWeapon(slotToClear, slotToClear.weapon); }
                break;
            case ARMOR:
                // todo
                break;
        }

        if (isClient)
        {
            networkManager.sendPacket(new ItemDroppedPacket(player.getId(), item.getId(), item.totalAmmoAmount, item.clipAmmoAmount));
        }

    }

public boolean checkFullPatron(ItemCell item)
{
    if(item.getItem_blueprint().equals(StringConstants.SMALL_PATRON))
    {
        if (smallPatron >= MAX_QUANTITY_SMALLPATRON){return false;}
    }else if (item.getItem_blueprint().equals(StringConstants.MEDIUM_PATRON))
    {
        if (mediumPatron >= MAX_QUANTITY_MEDIUMPATRON){return false;}
    }else if (item.getItem_blueprint().equals(StringConstants.HARD_PATRON))
    {
        if (hardPatron >= MAX_QUANTITY_HARDPATRON){return false;}
    }else if (item.getItem_blueprint().equals(StringConstants.MINI_HARD_PATRON))
    {
        if (miniHardPatron >= MAX_QUANTITY_MINIHARDPATRON){return false;}
    }else if (item.getItem_blueprint().equals(StringConstants.ROCKET_PATRON))
    {
        if (rocketAmmo >= MAX_QUANTITY_ROCKET){return false;}
    }
    return true;
}

    public void updateAmmo(String blueprint, int patron)
    {
        if(blueprint.equals(StringConstants.SMALL_PATRON))
        {
            smallPatron = Math.min(smallPatron + patron, MAX_QUANTITY_SMALLPATRON);
        }else if (blueprint.equals(StringConstants.MEDIUM_PATRON))
        {
            mediumPatron = Math.min(mediumPatron + patron, MAX_QUANTITY_MEDIUMPATRON);
        }else if (blueprint.equals(StringConstants.HARD_PATRON))
        {
            hardPatron = Math.min(hardPatron + patron, MAX_QUANTITY_HARDPATRON);
        }else if (blueprint.equals(StringConstants.MINI_HARD_PATRON))
        {
            miniHardPatron = Math.min(miniHardPatron + patron, MAX_QUANTITY_MINIHARDPATRON);
        }else if (blueprint.equals(StringConstants.ROCKET_PATRON))
        {
           rocketAmmo = Math.min(rocketAmmo + patron, MAX_QUANTITY_ROCKET);
        }





//        if (primaryWeaponSlot.weapon != null)
//        {
//            primaryWeaponSlot.weapon.totalAmmoAmount += primaryWeaponSlot.weapon.getClipMaxAmmoAmount();
//            if (primaryWeaponSlot.weapon.totalAmmoAmount > primaryWeaponSlot.weapon.getMaxAmmoAmount())
//            {
//                primaryWeaponSlot.weapon.totalAmmoAmount = primaryWeaponSlot.weapon.getMaxAmmoAmount();
//                //primaryWeaponSlot.setAmount(primaryWeaponSlot.weapon.clipAmmoAmount);
//            }
//        }
//        if (secondaryWeaponSlot.weapon != null)
//        {
//            secondaryWeaponSlot.weapon.totalAmmoAmount += secondaryWeaponSlot.weapon.getClipMaxAmmoAmount();
//            if (secondaryWeaponSlot.weapon.totalAmmoAmount > secondaryWeaponSlot.weapon.getMaxAmmoAmount())
//            {
//                secondaryWeaponSlot.weapon.totalAmmoAmount = secondaryWeaponSlot.weapon.getMaxAmmoAmount();
////                secondaryWeaponSlot.setAmount(secondaryWeaponSlot.weapon.totalAmmoAmount);
//            }
//        }
    }


    public void processWeaponPicking(ItemCell itemCell)
    {
        AbstractRangeWeapon weaponInstance = AbstractRangeWeapon.Factory(
                itemCell.getItem_blueprint(),
                hasVisuals ? GameClient.getGame().getGameShapeRenderer() : null,
                player);
        if (weaponInstance == null)
        {
            Gdx.app.error("InventoryManager", "Wrong Item_blueprint");
            return;
        }
        switchWeaponTo(weaponInstance);

        if (itemCell.totalAmmoAmount != DEFAULT_AMMO_AMOUNT)
        {
            weaponInstance.totalAmmoAmount = itemCell.totalAmmoAmount;
            weaponInstance.clipAmmoAmount = itemCell.clipAmmoAmount;
        }

        boolean changeWeapon = false;//for example if you have a bazooka and you picked machine gun we have to replace itemCell
        AbstractRangeWeapon weaponToChange = null;

        if (weaponInstance instanceof Grenade)
        {
            if (fragGrenadeSlot.weapon != null && fragGrenadeSlot.weapon.totalAmmoAmount > 0)
            {
                previousItem = fragGrenadeSlot.item;
                weaponList.set(weaponList.indexOf(fragGrenadeSlot.weapon), weaponInstance);
                if (active.equals(fragGrenadeSlot))
                {
                    weaponToChange = weaponInstance;
                    changeWeapon = true;
                }
            }
            else
            { weaponList.set(3, weaponInstance); }
            setSlotFromWeapon(fragGrenadeSlot, weaponInstance, itemCell);
        }
        else if (weaponInstance instanceof FlashBang)
        {
            if (flashbangSlot.weapon != null && flashbangSlot.weapon.totalAmmoAmount > 0)
            {
                previousItem = flashbangSlot.item;
                weaponList.set(weaponList.indexOf(flashbangSlot.weapon), weaponInstance);
                if (active.equals(flashbangSlot))
                {
                    weaponToChange = weaponInstance;
                    changeWeapon = true;
                }
            }
            else
            { weaponList.set(4, weaponInstance); }
            setSlotFromWeapon(flashbangSlot, weaponInstance, itemCell);
        }
        else
        {
            if (primaryWeaponSlot.weapon == null)
            {
                weaponList.set(0, weaponInstance);
                setSlotFromWeapon(primaryWeaponSlot, weaponInstance, itemCell);
                switchWeaponTo(weaponInstance);
                active = primaryWeaponSlot;
            }
            else if (secondaryWeaponSlot.weapon == null)
            {
                weaponList.set(1, weaponInstance);
                setSlotFromWeapon(secondaryWeaponSlot, weaponInstance, itemCell);
               // active = secondaryWeaponSlot;
            }
            else if (active.equals(primaryWeaponSlot))
            {
                if (!isClient)
                {
                    previousItem = primaryWeaponSlot.item;
                }
                weaponList.set(0, weaponInstance);
                weaponToChange = weaponInstance;
                changeWeapon = true;
                setSlotFromWeapon(primaryWeaponSlot, weaponInstance, itemCell);
                active = primaryWeaponSlot;
            }
            else if (active.equals(secondaryWeaponSlot))
            {
                if (!isClient)
                {
                    previousItem = secondaryWeaponSlot.item;
                }
                weaponList.set(1, weaponInstance);
                weaponToChange = weaponInstance;
                changeWeapon = true;
                setSlotFromWeapon(secondaryWeaponSlot, weaponInstance, itemCell);
                active = secondaryWeaponSlot;
            }
            else
            {
                if (!isClient)
                {
                    previousItem = primaryWeaponSlot.item;
                }
                weaponList.set(weaponList.indexOf(primaryWeaponSlot.weapon), weaponInstance);
                weaponToChange = weaponInstance;
                changeWeapon = true;
                setSlotFromWeapon(primaryWeaponSlot, weaponInstance, itemCell);
                active = primaryWeaponSlot;
            }
        }
        if (changeWeapon)
        {
            setCurrentWeapon(weaponToChange);
            if (hasVisuals)
            {
                switchWeaponTo(weaponToChange);
            }
        }

        // printWeaponListInfo();
    }

    private void printWeaponListInfo()
    {
        IoLogger.log("******WEAPONS:*********");
        for (AbstractRangeWeapon w : weaponList)
        {

            if (w == null){ IoLogger.log("Null"); }
            else{ IoLogger.log(w.toString()); }
        }
    }



    public void switchWeaponTo(AbstractRangeWeapon weapon)
    {

        if (weapon.equals(primaryWeaponSlot.weapon))
        {
            switchSlot(primaryWeaponSlot);
        }

        else if (weapon.equals(secondaryWeaponSlot.weapon))
        {
            switchSlot(secondaryWeaponSlot);
        }

        else if (weapon.equals(knifeSlot.weapon))
        {
            switchSlot(knifeSlot);
        }

        else if (weapon.equals(fragGrenadeSlot.weapon))
        {
            switchSlot(fragGrenadeSlot);
        }
        else if (weapon.equals(flashbangSlot.weapon))
        {
            switchSlot(flashbangSlot);
        }
        else
        {
            IoLogger.log("InventoryManager ", "switchWeaponTo unknown weapon slot!");
        }
       // printWeaponListInfo();
        if (!hasVisuals){ return; }
        //todo add more specific sounds for different weapon types
        Camera gameCam = GameClient.getGame().getCamera();
        IoPoint pos = new IoPoint(gameCam.position.x, gameCam.position.y);
        GameClient.getGame().getSoundManager().playSoundWithPanAndVolume(SoundManager.ITEM_PICKED, pos, pos);

    }


    public void switchSlot(InventoryItemSlot slot)
    {
        slot.setActive(true);
        if (active != slot)
        {
            clearActive();
        }
        active = slot;
        player.setCurrentWeapon(slot.weapon); // important to use  player.setCurrentWeapon here, since we are changing player skins in this method

        if(!isClient|| !hasVisuals) return;
        mInventoryPanel.setActiveSlot(active);
        networkManager.sendPacket(new WeaponSwitchedPacket(player.getId(), slot.weapon.weaponType, weaponList.indexOf(slot.weapon)));

    }

    public void setSlotFromWeapon(InventoryItemSlot slot, AbstractRangeWeapon weapon, ItemCell itemCell)
    {
        slot.setItemName(weapon.weaponType.name());
        //slot.setAmount(weapon.totalAmmoAmount);
        slot.setActive(weapon.equals(currentWeapon));

        slot.weapon = weapon;
        slot.item = itemCell;
    }

    public void removeSlotFromWeapon(InventoryItemSlot slot, AbstractRangeWeapon weapon)
    {
        int idx = player.getWeaponList().indexOf(weapon);
        if(idx == -1)
        {
           IoLogger.log(TAG, "removeSlotFromWeapon -1 idx for slot " + slot);
        }
        else
        {
            player.getWeaponList().set(idx, null);
        }
        slot.clear();
        //todo: add removing weapon from slot
        switchToFirstAvailableWeapon();

    }



    public void switchToFirstAvailableWeapon()
    {
        //set first not null weapon
        for (short i = 0;
             i < player.getWeaponList().size();
             i++)
        {
            if (player.getWeaponList().get(i) != null && player.getWeaponList().get(i).totalAmmoAmount > 0)
            {
                player.setCurrentWeapon(player.getWeaponList().get(i));
                switchWeaponTo(player.getWeaponList().get(i));
                break;
            }
        }
    }

    public void setSlotFromGoods(InventoryItemSlot slot, ItemCell item)
    {
        slot.setItemName(item.getItem_blueprint());
        if (!slot.equals(knifeSlot))
        {
            //slot.setAmount(getAmount(item.getItem_blueprint()));
        }

        slot.item = item;
    }

    public void setSlotFromAmmo(InventoryItemSlot slot, ItemCell item)
    {
        slot.setItemName(item.getItem_blueprint());
        slot.item = item;
        slot.setAmount(getAmmo(item.getItem_blueprint()));
    }

   public int getAmount(String itemBluePrint)
    {
        int amount = 0;
        if (itemBluePrint.equals(HP_30) || itemBluePrint.equals(HP_60) || itemBluePrint.equals(HP_100))
        {
            amount = medKits;
        }
        return amount;
    }


    public void clearActive()
    {
        if (active != null)
        { active.setActive(false); }
    }

    public InventoryItemSlot getActive()
    {
        return active;
    }


    public PlayerCell getPlayer()
    {
        return player;
    }


    public Boolean canPickItem(ItemType type)
    {
        if (type == HP)
        {
            return MAX_NUMBER_OF_MEDKITS > medKits;
        }
        else
        { return true; }
    }

    public void useItem(String itemBluePrint)
    {
        if (itemBluePrint.equals(HP_100) || itemBluePrint.equals(HP_60) || itemBluePrint.equals(HP_30))
        {
            medKits--;
            for (ItemCell itemCell : items.values())
            {
                if (itemCell.getItem_blueprint().equals(itemBluePrint))
                {
                    items.remove(itemCell);
                    break;
                }
            }
            if (medKits == 0)
            {
                medKitSlot.clear();
            }
        }

    }


    public void setInventoryPanel(InventoryPanel inventoryPanel)
    {
        mInventoryPanel = inventoryPanel;
    }


    public void setWeaponList(List<AbstractRangeWeapon> weaponList)
    {
        this.weaponList = weaponList;
    }

    public boolean hasWeapon(WeaponType t)
    {
        for (AbstractRangeWeapon w : weaponList)
        {
            if (w != null && w.weaponType.equals(t))
            { return true; }
        }
        return false;
    }


    public AbstractRangeWeapon getWeapon(WeaponType t)
    {
        for (AbstractRangeWeapon w : weaponList)
        {
            if (w != null && w.weaponType.equals(t))
            { return w; }
        }
        return null;
    }

    public List<AbstractRangeWeapon> getWeaponList()
    {
        return weaponList;
    }

   /* public AbstractRangeWeapon getCurrentWeapon()
    {
        return currentWeapon;
    }
*/
    public void setCurrentWeapon(AbstractRangeWeapon currentWeapon)
    {
        //  IoLogger.log("IM", "setCurrentWeapon " + currentWeapon);
        this.currentWeapon = currentWeapon;
    }

    public Map<String, ItemCell> getItems()
    {
        return items;
    }


    public void setNetworkManager(NetworkManager networkManager)
    {
        this.networkManager = networkManager;
    }

    public ItemCell getPreviousItem()
    {
        return previousItem;
    }

    public void setPreviousItem(ItemCell previousItem)
    {
        this.previousItem = previousItem;
    }

    public int getMedKits()
    {
        return medKits;
    }

    public int getAmmo(String blueprint) {
        int ammo = 0;
        if(blueprint == null) {
            IoLogger.log("getAmmo()", "blueprint = null");
            return ammo;
        }
        if(blueprint.equals(HARD_PATRON)) {
            ammo = hardPatron;
        }
        if(blueprint.equals(MEDIUM_PATRON)) {
            ammo = mediumPatron;
        }
        if(blueprint.equals(MINI_HARD_PATRON)) {
            ammo = miniHardPatron;
        }
        if(blueprint.equals(ROCKET_PATRON)) {
            ammo = rocketAmmo;
        }
        if(blueprint.equals(SMALL_PATRON)) {
            ammo = smallPatron;
        }

        return ammo;
    }
    public InventoryItemSlot getAmmoSlot(String blueprint) {
        InventoryItemSlot slot = null;
        if(blueprint.equals(HARD_PATRON)) {
            slot = hardPatronSlot;
        }
        if(blueprint.equals(MEDIUM_PATRON)) {
            slot = mediumPatronSlot;
        }
        if(blueprint.equals(MINI_HARD_PATRON)) {
            slot = miniHardPatronSlot;
        }
        if(blueprint.equals(ROCKET_PATRON)) {
            slot = rocketAmmoSlot;
        }
        if(blueprint.equals(SMALL_PATRON)) {
            slot = smallPatronSlot;
        }
        return slot;
    }

    public void takeAmmoFromSlot(AbstractRangeWeapon weapon, int ammoTaken, String ammoBlueprint) {
        if(ammoBlueprint.equals(StringConstants.SMALL_PATRON)) {
            smallPatron = smallPatron - ammoTaken;
            getAmmoSlot(ammoBlueprint).setAmount(smallPatron);
            //getWeaponSlot(weapon).setAmount(ammoTaken);
            }
        if(ammoBlueprint.equals(StringConstants.MEDIUM_PATRON)) {
            mediumPatron = mediumPatron - ammoTaken;
            getAmmoSlot(ammoBlueprint).setAmount(mediumPatron);
            //getWeaponSlot(weapon).setAmount(ammoTaken);
        }
        if(ammoBlueprint.equals(StringConstants.ROCKET_PATRON)) {
            rocketAmmo = rocketAmmo - ammoTaken;
            getAmmoSlot(ammoBlueprint).setAmount(rocketAmmo);
            //getWeaponSlot(weapon).setAmount(ammoTaken);
        }
        if(ammoBlueprint.equals(StringConstants.HARD_PATRON)) {
            hardPatron = hardPatron - ammoTaken;
            getAmmoSlot(ammoBlueprint).setAmount(hardPatron);
            //getWeaponSlot(weapon).setAmount(ammoTaken);
        }
        if(ammoBlueprint.equals(StringConstants.MINI_HARD_PATRON)) {
            miniHardPatron = miniHardPatron - ammoTaken;
            getAmmoSlot(ammoBlueprint).setAmount(miniHardPatron);
            //getWeaponSlot(weapon).setAmount(ammoTaken);
        }
    }

    public InventoryItemSlot getWeaponSlot(AbstractRangeWeapon weapon) {
        InventoryItemSlot slot = null;
        for (InventoryItemSlot s : weaponSlots) {
            if(s.weapon == weapon) {
                slot = s;
            }
        }
        return slot;
    }

    public InventoryItemSlot getFragGrenadeSlot() {
        return fragGrenadeSlot;
    }

    public InventoryItemSlot getFlashbangSlot() {
        return flashbangSlot;
    }


}

