package com.github.myio.inventory;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.github.myio.GameClient;
import com.github.myio.GameConstants;
import com.github.myio.entities.PlayerCell;
import com.github.myio.entities.weapons.FlashBang;
import com.github.myio.entities.weapons.Grenade;
import com.github.myio.ui.CountDown;
import com.github.myio.ui.IDrawable;

import static com.github.myio.GameConstants.MEDKIT_MOVEMENT_SLOWDOWN_COEFF;
import static com.github.myio.StringConstants.HP_100;
import static com.github.myio.StringConstants.HP_30;
import static com.github.myio.StringConstants.HP_60;


public class InventoryPanel implements IDrawable {

    private int weaponSlotWidth = 90;
    private int weaponSlotHeight = 50;
    private int grenadeSlotWidth = 38;  // width of each grenade button in the slot
    private int grenadeSlotHeight = 40; // height of each grenade button in the slot
    private int ammoSlotWidth = 80;
    private int ammoSlotHeight = 35;


    Drawable background;
    Drawable background_green;
    Drawable background_blue;

    Label.LabelStyle labelStyle;
    Viewport viewport;
    Stage stage;
    Table panelTable;
    Table ammoTable;
    BitmapFont font;
    InventoryManager mInventoryManager;
    Table activeTable;
    CountDown mCountDown;

    public InventoryPanel(InventoryManager inventoryManager, CountDown countDown) {

        mCountDown = countDown;
        mInventoryManager = inventoryManager;
        activeTable = mInventoryManager.getActive().getItemSlotButton();

        background = new Image(GameClient.uiAtlas.createSprite("slot_background")).getDrawable();
        background_green = new Image(GameClient.uiAtlas.createSprite("slot_background_green")).getDrawable();
        background_blue = new Image(GameClient.uiAtlas.createSprite("slot_background_blue")).getDrawable();

        font = GameClient.getFont32Black();
        viewport = GameClient.getGame().getUiViewport();
        stage = new Stage(viewport, GameClient.getGame().getBatch());
        labelStyle = new Label.LabelStyle();
        labelStyle.font = font;

        panelTable = new Table();
        panelTable.right().bottom().padRight(5);
        panelTable.setFillParent(true);
        panelTable.defaults().size(weaponSlotWidth, weaponSlotHeight);
        Table grenadeTable = new Table();
        grenadeTable.setName("grenadeTable");
        grenadeTable.setBackground(background);

        //panelTable.setPosition((float) (Gdx.graphics.getWidth() * 0.95), (float) (Gdx.graphics.getHeight() * 0.22));
        for (InventoryItemSlot s : inventoryManager.getWeaponSlots()) {
            s.getItemSlotButton().addCaptureListener(new ItemListener(s));
            if(s.isGrenade()) {
                s.getItemSlotButton().pad(1);
                grenadeTable.add(s.getItemSlotButton()).size(grenadeSlotWidth, grenadeSlotHeight);
                if(panelTable.findActor("grenadeTable") == null) {
                    panelTable.add(grenadeTable).padBottom(10).padRight(5).row();
                }
            }else {
            Table tb = new Table();
            tb.setBackground(background);
            //tb.debugAll();
            tb.add(s.getItemSlotButton()).size(weaponSlotWidth, weaponSlotHeight).padRight(5);
            panelTable.add(tb).padBottom(10).row();

            }
        }

        ammoTable = new Table();
        ammoTable.right().bottom().padBottom(300).padRight(5);
        ammoTable.setFillParent(true);
        ammoTable.defaults().size(ammoSlotWidth, ammoSlotHeight);

        for (InventoryItemSlot s : inventoryManager.getAmmoSlots()) {
            Table t = new Table();
            t.setBackground(background);
            t.add(s.getItemSlotButton()).size(ammoSlotWidth, ammoSlotHeight).padRight(5);
            ammoTable.add(t).padBottom(10).row();

        }

        stage.addActor(panelTable);
        stage.addActor(ammoTable);



    }


    @Override
    public void draw(float delta) {

        stage.act();
        stage.draw();

    }


    public class ItemListener extends ClickListener {
        InventoryItemSlot mButton;
        Table table;

        public ItemListener(InventoryItemSlot button) {
            mButton = button;
        }


        public void enter(InputEvent event, float x, float y, int pointer, Actor fromActor) {
                if (event != null) {
                table = (Table) mButton.getItemSlotButton().getParent();
                table.setBackground(background_green);
            }


        }

        public void exit(InputEvent event, float x, float y, int pointer, Actor toActor) {
            if (!mButton.isActive()) {
                if(isAnotherGrenadeActive(mButton)) {
                    table.setBackground(background_blue);
                }else {
                table.setBackground(background);}
            } else {
                table.setBackground(background_blue);
            }
        }

        @Override
        public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
            return true;
        }

        public void touchUp (InputEvent event, float x, float y, int pointer, int button) {
            PlayerCell player = mInventoryManager.getPlayer();
            if(mButton.getSlotName() == null)
                return;

            if (button == Input.Buttons.LEFT)
            {
                if (    !mButton.getSlotName().equals(HP_30) &&
                        !mButton.getSlotName().equals(HP_60) &&
                        !mButton.getSlotName().equals(HP_100))
                {
                    mInventoryManager.switchWeaponTo(mButton.weapon);
                    table.setBackground(background_blue);
                    if (activeTable != table && activeTable != null)
                    {
                        activeTable.setBackground(background);
                        activeTable = table;
                        player.setCurrentWeapon(mButton.weapon);
                    }
                }
                else {
                    if (player.getCurrentHealth() < player.getMaxHealth() /*&& mButton.getAmount() > 0)*/)
                    {
                        if(mCountDown.isFinished()) {
                            mCountDown.showCountDown(Color.GREEN, player, mButton.getSlotName(), GameConstants.TIME_FOR_HEAL_BY_MEDKIT);
                            player.setAccelerationMultiplier(MEDKIT_MOVEMENT_SLOWDOWN_COEFF);
                        }
                    }
                }
            }

            else if(button == Input.Buttons.RIGHT)
            {
                //item dropping
              mInventoryManager.dropItem(mButton.item);
            }


        }
    }
        public Stage getStage() {
            return stage;
        }

        public void setActiveSlot(InventoryItemSlot activeSlot) {
            Table t = (Table) panelTable.findActor(activeSlot.getSlotName()).getParent();
            t.setBackground(background_blue);
            if (activeTable != t && activeTable != null) {
                activeTable.setBackground(background);
                activeTable = t;
            }
        }

        public boolean isAnotherGrenadeActive(InventoryItemSlot button) {
            if(button.weapon instanceof Grenade) {
            return mInventoryManager.getFlashbangSlot().isActive();
            }
            if(button.weapon instanceof FlashBang) {
                return mInventoryManager.getFragGrenadeSlot().isActive();
            }
            return  false;
        }



}

