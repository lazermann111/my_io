package com.github.myio.inventory;




import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ImageTextButton;
import com.badlogic.gdx.utils.Scaling;
import com.github.myio.GameClient;
import com.github.myio.entities.item.ItemCell;
import com.github.myio.entities.weapons.AbstractRangeWeapon;

/**
 *   Class that shares view and logic representation of items between
 *   InventoryManager and InventoryPanel
 *   Can be added to Scene as Actor
 */
public class InventoryItemSlot  {
   // visual part
    Integer amount; // important!! should be reference value
    Vector2 mPosition;

    InventoryItemSlotButton itemSlotButton;


   // logic part
   String item_blueprint;
   boolean isActive;
   boolean isEmpty = true;
   boolean isGrenade = false;

   boolean hasVisuals;

   public AbstractRangeWeapon weapon;
   public ItemCell item;

    public InventoryItemSlot(boolean hasVisuals)
    {
        if(hasVisuals)
        {
            itemSlotButton = new InventoryItemSlotButton();
            itemSlotButton.pad(5);
        }
        this.hasVisuals = hasVisuals;
    }



   public String getSlotName() {
      return this.itemSlotButton.getName();
   }



    public void setItemName(String itemName)
    {
        if(!hasVisuals) return;

        this.itemSlotButton.setName(itemName);
        String itemNameTransparent = itemName + "_tr";
        System.out.println(itemName);
        Image up = new Image(GameClient.uiAtlas.createSprite(itemNameTransparent));
        Image down = new Image(GameClient.uiAtlas.createSprite(itemName));
        up.setScaling(Scaling.fill);

        ImageTextButton.ImageTextButtonStyle style = this.itemSlotButton.getStyle();
        style.imageUp = up.getDrawable();
        style.imageOver = down.getDrawable();
        style.imageCheckedOver = down.getDrawable();
        style.fontColor = Color.WHITE;
        style.overFontColor = Color.BLACK;
        style.checkedOverFontColor = Color.BLACK;
        this.itemSlotButton.setStyle(style);
        isEmpty = false;
    }

    public Integer getAmount() {
      return amount;
   }

    public void setAmount(Integer amount)
    {
       this.amount = amount;
       if(!hasVisuals) return;
       this.itemSlotButton.setText(String.valueOf(amount));
    }

   public boolean isActive() {
      return isActive;
   }

   public void setActive(boolean active) {
      isActive = active;
   }

   public boolean isEmpty() {
      return isEmpty;
   }

   public Vector2 getPosition() {
      return mPosition;
   }

   public void setPosition(Vector2 position) {
      mPosition = position;
   }


   public void clear() {
       this.weapon = null;
       this.item = null;
        if(!hasVisuals) return;

       ImageTextButton.ImageTextButtonStyle style = this.itemSlotButton.getStyle();
       style.imageUp = null;
       style.imageOver = null;
       style.imageCheckedOver = null;
       this.itemSlotButton.setStyle(style);
       this.itemSlotButton.setText("");
       this.itemSlotButton.setName(null);
   }

    public InventoryItemSlotButton getItemSlotButton()
    {
        return itemSlotButton;
    }

    public class InventoryItemSlotButton extends ImageTextButton
   {
       public InventoryItemSlotButton() {
           super("", new ImageTextButtonStyle(null, null, null, new BitmapFont()));
       }



       @Override
       public void draw(Batch batch, float parentAlpha) {
           super.draw(batch, parentAlpha);
       }

       @Override
       public void act(float delta) {
           super.act(delta);
       }
   }


   @Override
   public String toString() {
      return "InventoryItemSlot{" +
              "slotName='" + getSlotName() + '\'' +
              ", amount=" + amount +
              ", isActive=" + isActive +
              ", isEmpty=" + isEmpty +
              ", weapon=" + weapon +
              '}';
   }

    public boolean isGrenade() {
        return isGrenade;
    }

    public void setGrenade(boolean grenade) {
        isGrenade = grenade;
    }

}