package com.github.myio.enums;

/**
 * Created by Igor on 5/30/2018.
 */

public enum  CapturablePointState
{
   POINT_CAPTURED, POINT_CAPTURING_STARTED, POINT_CAPTURING_CANCELED
}
