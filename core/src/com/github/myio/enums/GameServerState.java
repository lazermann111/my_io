package com.github.myio.enums;



public enum GameServerState
{
    LAUNCHING, WAITING_FOR_PLAYERS, WARMUP, GAME_IN_PROGRESS
}