package com.github.myio.enums;



public enum PlayerState {
    PLAYING, DEAD, WINS, SPECTATE, IN_LOBBY, IN_CHAT
}
