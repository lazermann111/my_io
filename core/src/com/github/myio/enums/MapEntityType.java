package com.github.myio.enums;


import com.badlogic.gdx.graphics.Color;

public enum MapEntityType {

    BUSH,
    SPIDER_WEB,
    SPIDER_WEB_BIG,
    MUD,
    PIT,
    PIT_TRAP,
    PIT_TRAP2,
    TRAP,
    TACTICAL_SHIELD,
    PLANT,
    FLOOR_BUSH_GREEN,
    FLOOR_BUSH_GREEN_FLOWERS,

    STONE_SMALL,
    STONE_BIG,
    STONE_GREEN,

    CAPTURABLE_POINT,

    GREEN_ZONE ,
    YELLOW_ZONE,
    RED_ZONE;




    static public MapEntityType getEntityByColor(Color c)
    {
        if(c.g > 0.9 && c.b > 0.9 && c.r > 0.9)
            return null;
        if(c.g < 0.1 && c.b < 0.1 && c.r < 0.1)
            return null;

        if(c.r == 162/255f && c.g == 73/255f && c.b == 165/255f) return SPIDER_WEB;
        if(c.r == 239/255f && c.g == 228/255f && c.b == 176/255f) return SPIDER_WEB_BIG;

        if(c.r == 128/255f && c.g == 64/255f && c.b == 0/255f) {
            return BUSH;}
        if(c.r == 128/255f && c.g == 128/255f && c.b == 0/255f){
            return MUD;}
        if(c.r == 127/255f && c.g == 127/255f && c.b == 127/255f){

            return PIT;}
        if(c.r == 0/255f && c.g == 128/255f && c.b == 128/255f) return TACTICAL_SHIELD;

        if(c.r == 120/255f && c.g == 115/255f && c.b == 140/255f)  {
               return PIT_TRAP;
        }
        if(c.r == 88/255f && c.g == 84/255f && c.b == 103/255f) return PIT_TRAP2;
        if(c.r == 78/255f && c.g == 78/255f && c.b == 78/255f) return TRAP;

        if(c.r == 208/255f && c.g == 239/255f && c.b == 114/255f) return PLANT;
        if(c.r == 210/255f && c.g == 210/255f && c.b == 0/255f) return FLOOR_BUSH_GREEN_FLOWERS;

        if(c.r == 162/255f && c.g == 163/255f && c.b == 202/255f) return STONE_SMALL;
        if(c.r == 118/255f && c.g == 119/255f && c.b == 177/255f) return STONE_BIG;
        if(c.r == 79/255f && c.g == 80/255f && c.b == 138/255f) return STONE_GREEN;



        if(c.r == 180/255f && c.g == 230/255f && c.b == 0) return CAPTURABLE_POINT;

        if(c.r == 128/255f && c.g == 255/255f && c.b == 128/255f){
            return GREEN_ZONE;

        }
        if(c.r == 255/255f && c.g == 201/255f && c.b == 14/255f){
            return YELLOW_ZONE;
        }
        if(c.r == 237/255f && c.g == 28/255f && c.b == 36/255f){
            return RED_ZONE;
        }


        return null;
    }

}
