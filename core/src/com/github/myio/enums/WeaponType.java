package com.github.myio.enums;


public enum WeaponType  {

    ROCKET(1), MACHINE_GUN(2),SHOTGUN(3),PISTOL(4), FRAG_GRENADE(5) , FLASHBANG(6), KNIFE(7),GRENADE_LAUNCHER(8),AK47(9);


    private Integer intValue;

    WeaponType(final Integer hierarchy) {
        this.intValue = hierarchy;
    }

    public Integer getIntValue() {
        return intValue;
    }


}
