package com.github.myio.enums;


import com.badlogic.gdx.graphics.Color;

public enum TileType {


    WALL_BROWN(1),
    WALL_BUSH_BROWN(2),
    FLOOR_BROWN(3),
    FLOOR_GREEN(4),
    FLOOR_YELLOW (5),
    WALL_YELLOW(6);

    //WALL_BUSH_GREEN(7),
    private Integer intValue;

    TileType(final Integer hierarchy) {
        this.intValue = hierarchy;
    }

    public Integer getIntValue() {
        return intValue;
    }


    static public TileType getTileByColor(Color c)
    {

        if(c.r == 88/255f && c.g == 55/255f && c.b == 37/255) return FLOOR_BROWN;
        if(c.r == 255/255f && c.g == 242/255f && c.b == 0/255f) return FLOOR_YELLOW;
        if(c.r == 21/255f && c.g == 111/255f && c.b == 48/255f) return FLOOR_GREEN;

        if(c.r == 179/255f && c.g == 89/255f && c.b == 0/255f) return WALL_BUSH_BROWN;
        //if(c.r == 14/255f && c.g == 71/255f && c.b == 31/255f) return WALL_BUSH_GREEN;

        if(c.r == 202/255f && c.g == 192/255f && c.b == 0/255f) return WALL_YELLOW;
        if(c.r == 0/255f && c.g == 0/255f && c.b == 0/255) return WALL_BROWN;

        return FLOOR_BROWN;
    }
}
