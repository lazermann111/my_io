package com.github.myio;



public class StringConstants
{

    public static final String GREEN_ZONE = "GREEN ZONE";
    public static final String YELLOW_ZONE = "YELLOW ZONE";
    public static final String RED_ZONE = "RED ZONE";

    public static final String PREFERENCES_USERNAME = "pref_username";
    public static final String PREFERENCES_SKIN_COLOR = "pref_skin_color";
    public static final String PREFERENCES_REGION = "pref_region";
    public static final String PREFERENCES_MODE = "pref_mode";
    public static final String PREFERENCES_CLIENT_ID = "pref_client_id";
    public static final String PREFERENCES_SKIN = "pref_skin";

    public static final String MARKER = "marker";

    //====================TILES=======================

    public static final String WALL_BROWN = "wall_brown";
    public static final String WALL_YELLOW = "wall_yellow";
    public static final String WALL_BUSH_BROWN = "wall_bush_brown";
    public static final String WALL_BUSH_GREEN = "wall_bush_green";

    public static final String FLOOR_KIND1 = "floor_kind_1";
    public static final String FLOOR_KIND2 = "floor_kind_2";
    public static final String FLOOR_KIND3 = "floor_kind_3";
    public static final String FLOOR_KIND4 = "floor_kind_4";

    public static final String FLOOR_YELLOW1 = "floor_yellow_1";
    public static final String FLOOR_YELLOW2 = "floor_yellow_2";
    public static final String FLOOR_YELLOW3 = "floor_yellow_3";
    public static final String FLOOR_YELLOW4 = "floor_yellow_4";

    public static final String FLOOR_GREEN1 = "floor_green_1";
    public static final String FLOOR_GREEN2 = "floor_green_2";
    public static final String FLOOR_GREEN3 = "floor_green_3";
    public static final String FLOOR_GREEN4 = "floor_green_4";



    //====================ITEMS=======================

    public static final String AK47 = "AK47";
    public static final String SHOTGUN = "shotgun";
    public static final String MACHINEGUN = "machinegun";
    public static final String BAZOOKA = "bazooka";
    public static final String PISTOL = "pistol";
    public static final String KNIFE = "knife";

    public static final String FLASH_GRENADE = "flash_grenade";
    public static final String FRAG_GRENADE = "frag_grenade";
    public static final String GRENADE_LAUNCHER = "tommy_gun";

    public static final String HP_30 = "hp_30";
    public static final String HP_60 = "hp_60";
    public static final String HP_100 = "hp_100";

    public static final String ARMOR = "armor";
    public static final String ARMOR_HELMET = "helmet";
//
    public static final String PATRON_BOX = "patron_box";

    public static final String SMALL_PATRON = "small_patron";
    public static final String MEDIUM_PATRON = "medium_patron";
    public static final String HARD_PATRON = "hard_patron";
    public static final String MINI_HARD_PATRON = "miniHard_patron";
    public static final String ROCKET_PATRON = "rocket_ammo";


    public static final String AMMUNITION = "ammunition";

    //===================ENTITIES=================


    public static final String TACTICAL_SHIELD = "shield";
    public static final String BUSH = "bush";
    public static final String LION = "lion";
    // public static final String SPIDER_WEB1 = "web1";
    public static final String SPIDER_WEB1 = "web_1";
    public static final String SPIDER_WEB2 = "web_2";
    // public static final String SPIDER_WEB3 = "web3";
    // public static final String SPIDER_WEB4 = "web4";
    public static final String SPIDER_WEB_BIG = "webBig";
    public static final String PIT = "pit";

    public static final String PIT_TRAP = "pit_trap";
    public static final String PIT_TRAP2 = "pit_trap2";
    public static final String TRAP = "trap";
    public static final String CAPTURABLE_POINT = "trap"; //todo change
    public static final String FLOOR_BUSH_GREEN = "floor_bush_green";
    public static final String FLOOR_BUSH_GREEN_FLOWERS = "floor_bush_with_flowers";

    public static final String STONE_SMALL1 = "stoneSmall_1";
    public static final String STONE_SMALL2 = "stoneSmall_2";
    public static final String STONE_SMALL3 = "stoneSmall_3";
    public static final String STONE_BIG1 = "stoneBig_1";
    public static final String STONE_BIG2 = "stoneBig_2";

    public static final String STONE_GREEN1 = "stoneGreen_1";
    public static final String STONE_GREEN2 = "stoneGreen_2";
    public static final String STONE_GREEN3 = "stoneGreen_3";


    public static final String PLANT1 = "plant_1";
    public static final String PLANT2 = "plant_2";
    public static final String PLANT3 = "plant_3";
    public static final String PLANT4 = "plant_4";
    public static final String PLANT5 = "plant_5";
    public static final String PLANT6 = "plant_6";





    public static final String MUD_BLACK = "mud_black";
    public static final String MUD_YELLOW = "mud_yellow";



//==================EMOJI=================

    public static final String ANGEL = "angel";
    public static final String ANGEL_TR = "angel_tr";
    public static final String LAUGH = "laugh";
    public static final String LAUGH_TR = "laugh_tr";
    public static final String PUMPKIN = "pumpkin";
    public static final String PUMPKIN_TR = "pumpkin_tr";
    public static final String HAPPY_TR = "happy_tr";
    public static final String HAPPY = "happy";
    public static final String SAD = "sad";
    public static final String SAD_TR = "sad_tr";
    public static final String THUMB_DOWN = "thumb_down";
    public static final String THUMB_DOWN_TR = "thumb_down_tr";
    public static final String THUMB_UP = "thumb_up";
    public static final String THUMB_UP_TR = "thumb_up_tr";
    public static final String TONGUE = "tongue";
    public static final String TONGUE_TR = "tongue_tr";
    public static final String ARGENTINA = "argentina";
    public static final String ARGENTINA_TR = "argentina_tr";
    public static final String CLOSE_TR = "close_tr";
    public static final String CLOSE = "close";

    public static final String CHINA = "china";
    public static final String CHINA_TR = "china_tr";
    public static final String ENGLAND = "england";
    public static final String ENGLAND_TR = "england_tr";
    public static final String GERMANY = "germany";
    public static final String GERMANY_TR = "germany_tr";
    public static final String INDIA = "india";
    public static final String INDIA_TR = "india_tr";
    public static final String IRELAND = "ireland";
    public static final String IRELAND_TR = "ireland_tr";
    public static final String JAPAN = "japan";
    public static final String JAPAN_TR = "japan_tr";
    public static final String SOUTH_COREA = "south_korea";
    public static final String SOUTH_COREA_TR = "south_korea_tr";
    public static final String TURKEY = "turkey";
    public static final String TURKEY_TR = "turkey_tr";
    public static final String UKRAINE = "ukraine";
    public static final String UKRAINE_TR = "ukraine_tr";
    public static final String USA = "usa";
    public static final String USA_TR = "usa_tr";
    public static final String BACKGROUND_TABLE = "background_table";
    public static final String YOUTUBE = "youtube";
    public static final String FACEBOOK = "facebook";

    //=======================================
    //============SKIN PLAYER================
    //=======================================
    public static final String SKIN_DEFAULT = "darkBlue";

    public static final String SKIN_YELLOW_DEFAULT = "yellow";
    public static final String SKIN_GREY_DEFAULT = "grey";
    //public static final String SKIN_GREEN_DEFAULT = "green";
    public static final String SKIN_RED_DEFAULT = "red";
    public static final String SKIN_BROWN_DEFAULT = "brown";
    public static final String SKIN_ORANGE_DEFAULT = "orange";
    public static final String SKIN_KHAKI_DEFAULT = "khaki";
    public static final String SKIN_BLACK_DEFAULT = "black";
    public static final String SKIN_DARKBLUE_DEFAULT = "darkBlue";

    public static final String SKIN_MACHINEGUN = "_with_machinegun";
    public static final String SKIN_KNIFE = "_with_knife";
    public static final String SKIN_PISTOL = "_with_pistol";
    public static final String SKIN_BAZOOKA = "_with_bazooka";
    public static final String SKIN_SHOTGUN = "_with_shotgun";
    public static final String SKIN_FRAG_GRENADE = "_with_grenade";
    public static final String SKIN_FLASHBANG = "_with_flashbang";
    public static final String SKIN_GRENADE_LANCHER = "_tommy_gun";
    public static final String SKIN_AK47 = "_AK47";

    //==================ICON SCIN ==================


    public static final String ICON_ARMOR = "armor_icon";
    public static final String ICON_BAZOOKA = "bazooka_icon";
    public static final String ICON_GRENADE = "grenade_icon";
    public static final String ICON_GRENADE_LAUNCHER = "grenadeLauncher_icon";
    public static final String ICON_HP = "hp_icon";
    public static final String ICON_KNIFE = "knife_icon";
    public static final String ICON_MACHINEGUN = "machinegun_icon";
    public static final String ICON_NOISE_GRENADE = "noiseGrenade_icon";
    public static final String ICON_PISTOL = "pistol_icon";
    public static final String ICON_RPK = "rpk_icon";
    public static final String ICON_SHOTGUN = "shotgun_icon";
    public static final String ICON_SMOOKE_GRENADE = "smookeGrenade_icon";



    public static final int ROCKET_EFFECT = 1;
    public static final int BULLET_EFFECT = 2;
    public static final int PISTOL_EFFECT = 3;
    public static final int SHOTGUNAMMO_EFFECT = 4;
    public static final int ROCKET_EXPLOSION_EFFECT = 5;
    public static final int GRENADE_EXPLOSION_EFFECT = 6;
    public static final int KNIFE_EFFECT = 7;
    public static final int DEATH_EFFECT = 8;
    public static final int GRENADE_LAUNCHER_EFFECT = 9;
    public static final int GRENADE_LAUNCHER_EXPLOSION_EFFECT = 10;


    //==================SETTING ==================
    public static final String[] languagesList = {"English"};


    //==================SETTING ==================
    public static final String GOGOGO = "num1";
    public static final String OKLETSGO = "num2";


}
