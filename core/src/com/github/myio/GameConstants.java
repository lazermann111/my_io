package com.github.myio;


import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.MathUtils;
import com.github.myio.dto.masterserver.GameType;
import com.github.myio.entities.map.MapEntityDetails;
import com.github.myio.entities.tile.TileDetails;
import com.github.myio.enums.AnimalType;
import com.github.myio.enums.ItemType;
import com.github.myio.enums.MapEntityType;
import com.github.myio.enums.TileType;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import static com.github.myio.StringConstants.AK47;
import static com.github.myio.StringConstants.AMMUNITION;
import static com.github.myio.StringConstants.ARMOR;
import static com.github.myio.StringConstants.BAZOOKA;
import static com.github.myio.StringConstants.BUSH;
import static com.github.myio.StringConstants.CAPTURABLE_POINT;
import static com.github.myio.StringConstants.FLASH_GRENADE;
import static com.github.myio.StringConstants.FLOOR_GREEN1;
import static com.github.myio.StringConstants.FLOOR_GREEN2;
import static com.github.myio.StringConstants.FLOOR_GREEN3;
import static com.github.myio.StringConstants.FLOOR_GREEN4;
import static com.github.myio.StringConstants.FLOOR_KIND1;
import static com.github.myio.StringConstants.FLOOR_KIND2;
import static com.github.myio.StringConstants.FLOOR_KIND3;
import static com.github.myio.StringConstants.FLOOR_KIND4;
import static com.github.myio.StringConstants.FLOOR_YELLOW1;
import static com.github.myio.StringConstants.FLOOR_YELLOW2;
import static com.github.myio.StringConstants.FLOOR_YELLOW3;
import static com.github.myio.StringConstants.FLOOR_YELLOW4;
import static com.github.myio.StringConstants.FRAG_GRENADE;
import static com.github.myio.StringConstants.GRENADE_LAUNCHER;
import static com.github.myio.StringConstants.HARD_PATRON;
import static com.github.myio.StringConstants.HP_100;
import static com.github.myio.StringConstants.MACHINEGUN;
import static com.github.myio.StringConstants.MEDIUM_PATRON;
import static com.github.myio.StringConstants.MINI_HARD_PATRON;
import static com.github.myio.StringConstants.MUD_BLACK;
import static com.github.myio.StringConstants.MUD_YELLOW;
import static com.github.myio.StringConstants.PATRON_BOX;
import static com.github.myio.StringConstants.PISTOL;
import static com.github.myio.StringConstants.PIT;
import static com.github.myio.StringConstants.PIT_TRAP2;
import static com.github.myio.StringConstants.PLANT1;
import static com.github.myio.StringConstants.PLANT2;
import static com.github.myio.StringConstants.PLANT3;
import static com.github.myio.StringConstants.PLANT4;
import static com.github.myio.StringConstants.PLANT5;
import static com.github.myio.StringConstants.PLANT6;
import static com.github.myio.StringConstants.ROCKET_PATRON;
import static com.github.myio.StringConstants.SHOTGUN;
import static com.github.myio.StringConstants.SMALL_PATRON;
import static com.github.myio.StringConstants.SPIDER_WEB1;
import static com.github.myio.StringConstants.SPIDER_WEB2;
import static com.github.myio.StringConstants.SPIDER_WEB_BIG;
import static com.github.myio.StringConstants.STONE_BIG1;
import static com.github.myio.StringConstants.STONE_BIG2;
import static com.github.myio.StringConstants.STONE_GREEN1;
import static com.github.myio.StringConstants.STONE_GREEN2;
import static com.github.myio.StringConstants.STONE_GREEN3;
import static com.github.myio.StringConstants.STONE_SMALL1;
import static com.github.myio.StringConstants.STONE_SMALL2;
import static com.github.myio.StringConstants.STONE_SMALL3;
import static com.github.myio.StringConstants.TACTICAL_SHIELD;
import static com.github.myio.StringConstants.TRAP;

public class GameConstants
{
    public static Map<TileType, TileDetails> TILE_DETAILS = new HashMap<TileType, TileDetails>();
    public static Map<MapEntityType, MapEntityDetails> MAP_ENTITIES = new HashMap<MapEntityType, MapEntityDetails>();
    public static ArrayList<String> FLOOR_BROWN_TEXTURES = new ArrayList<String>();
    public static ArrayList<String> FLOOR_YELLOW_TEXTURE = new ArrayList<String>();
    public static ArrayList<String> FLOOR_GREEN_TEXTURE = new ArrayList<String>();

    public static Map<ItemType, String[]> ITEM_DETAILS = new HashMap<ItemType, String[]>();
    public static Map<ItemType, String[]> ITEM_DETAILS_FOR_DROP_AMMONITION = new HashMap<ItemType, String[]>();
    public static Map<ItemType, String[]> ITEM_DETAILS_FOR_DROP_PATRONBOX = new HashMap<ItemType, String[]>();
    public static Map<AnimalType, String[]> ANIMALS_DETAILS = new HashMap<AnimalType, String[]>();

    public static final int CONQUEST_CAPTURABLE_POINT_INTERACTION_RADIUS = 400;

    public static final float TILE_WIDTH = 200;
    public static final float TILE_HEIGHT = 200;

    static
    {


        String[] weapon = {MACHINEGUN, SHOTGUN, PISTOL, BAZOOKA, FLASH_GRENADE, FRAG_GRENADE, GRENADE_LAUNCHER,AK47};
        String[] patron = {SMALL_PATRON, MEDIUM_PATRON, HARD_PATRON, MINI_HARD_PATRON, ROCKET_PATRON};
        String[] ammunition = {AMMUNITION, PATRON_BOX};
        String[] armor = {ARMOR};
        String[] hp = {/*HP_30,HP_60*/HP_100};

        String[] weaponDropAmmunition = {FLASH_GRENADE, FRAG_GRENADE};
        String[] patronForDropBox = {SMALL_PATRON, MEDIUM_PATRON, HARD_PATRON, MINI_HARD_PATRON, ROCKET_PATRON};

        ITEM_DETAILS_FOR_DROP_PATRONBOX.put(ItemType.PATRON, patronForDropBox);

        ITEM_DETAILS_FOR_DROP_AMMONITION.put(ItemType.WEAPON, weaponDropAmmunition);
        ITEM_DETAILS_FOR_DROP_AMMONITION.put(ItemType.ARMOR, armor);
        ITEM_DETAILS_FOR_DROP_AMMONITION.put(ItemType.HP, hp);


        ITEM_DETAILS.put(ItemType.WEAPON, weapon);
        ITEM_DETAILS.put(ItemType.PATRON, patron);
        ITEM_DETAILS.put(ItemType.ARMOR, armor);
        ITEM_DETAILS.put(ItemType.HP, hp);
        ITEM_DETAILS.put(ItemType.AMMUNITION, ammunition);


        TILE_DETAILS.put(TileType.FLOOR_BROWN, new TileDetails(Color.BROWN, TILE_WIDTH, TILE_HEIGHT, false));
        TILE_DETAILS.put(TileType.FLOOR_YELLOW, new TileDetails(Color.YELLOW, TILE_WIDTH, TILE_HEIGHT, false));
        TILE_DETAILS.put(TileType.FLOOR_GREEN, new TileDetails(Color.GREEN, TILE_WIDTH, TILE_HEIGHT, false));

        TILE_DETAILS.put(TileType.WALL_BROWN, new TileDetails(Color.GOLD, TILE_WIDTH, TILE_HEIGHT, true));
        TILE_DETAILS.put(TileType.WALL_BUSH_BROWN, new TileDetails(Color.DARK_GRAY, TILE_WIDTH, TILE_HEIGHT, true));
        //TILE_DETAILS.put(TileType.WALL_BUSH_GREEN, new TileDetails(Color.CHARTREUSE, TILE_WIDTH, TILE_HEIGHT, true));
        TILE_DETAILS.put(TileType.WALL_YELLOW, new TileDetails(Color.BLUE, TILE_WIDTH, TILE_HEIGHT, true));


        MAP_ENTITIES.put(MapEntityType.BUSH, new MapEntityDetails(new String[]{BUSH}, (int) TILE_HEIGHT / 2, true, true, false));
        MAP_ENTITIES.put(MapEntityType.SPIDER_WEB, new MapEntityDetails(new String[]{SPIDER_WEB1, SPIDER_WEB2}, 50, false, true, true));
        MAP_ENTITIES.put(MapEntityType.SPIDER_WEB_BIG, new MapEntityDetails(new String[]{SPIDER_WEB_BIG}, 150, false, true, true));
        MAP_ENTITIES.put(MapEntityType.TACTICAL_SHIELD, new MapEntityDetails(new String[]{TACTICAL_SHIELD}, 110, true, false, true));
        MAP_ENTITIES.put(MapEntityType.MUD, new MapEntityDetails(new String[]{MUD_BLACK, MUD_YELLOW}, 110, false, false, true));
        MAP_ENTITIES.put(MapEntityType.PLANT, new MapEntityDetails(new String[]{PLANT1, PLANT2, PLANT3, PLANT4, PLANT5, PLANT6}, 110, false, false, true));
        // MAP_ENTITIES.put(MapEntityType.FLOOR_BUSH_GREEN_FLOWERS, new MapEntityDetails( new String[] {FLOOR_BUSH_GREEN_FLOWERS},110, false,false,true));

        MAP_ENTITIES.put(MapEntityType.STONE_BIG, new MapEntityDetails(new String[]{STONE_BIG1, STONE_BIG2}, 80, true, false, false));
        MAP_ENTITIES.put(MapEntityType.STONE_SMALL, new MapEntityDetails(new String[]{STONE_SMALL1, STONE_SMALL2, STONE_SMALL3}, 80, true, false, false));
        MAP_ENTITIES.put(MapEntityType.STONE_GREEN, new MapEntityDetails(new String[]{STONE_GREEN1, STONE_GREEN2, STONE_GREEN3}, 80, true, false, false));

        MAP_ENTITIES.put(MapEntityType.GREEN_ZONE, new MapEntityDetails(new String[]{BUSH}, 1, false, false, false));
        MAP_ENTITIES.put(MapEntityType.YELLOW_ZONE, new MapEntityDetails(new String[]{BUSH}, 1, false, false, false));
        MAP_ENTITIES.put(MapEntityType.RED_ZONE, new MapEntityDetails(new String[]{BUSH}, 1, false, false, false));

        MAP_ENTITIES.put(MapEntityType.PIT, new MapEntityDetails(new String[]{PIT}, 250, true, false, true));
        // MAP_ENTITIES.put(MapEntityType.PIT_TRAP, new MapEntityDetails( new String[] {PIT_TRAP},250, true,false,true));
        MAP_ENTITIES.put(MapEntityType.PIT_TRAP2, new MapEntityDetails(new String[]{PIT_TRAP2}, 250, true, false, true));
        MAP_ENTITIES.put(MapEntityType.TRAP, new MapEntityDetails(new String[]{TRAP}, 100, false, false, true));
        MAP_ENTITIES.put(MapEntityType.CAPTURABLE_POINT, new MapEntityDetails(new String[]{CAPTURABLE_POINT}, CONQUEST_CAPTURABLE_POINT_INTERACTION_RADIUS, false, false, true));


        FLOOR_BROWN_TEXTURES.add(FLOOR_KIND1);
        FLOOR_BROWN_TEXTURES.add(FLOOR_KIND2);
        FLOOR_BROWN_TEXTURES.add(FLOOR_KIND3);
        FLOOR_BROWN_TEXTURES.add(FLOOR_KIND4);

        FLOOR_YELLOW_TEXTURE.add(FLOOR_YELLOW1);
        FLOOR_YELLOW_TEXTURE.add(FLOOR_YELLOW2);
        FLOOR_YELLOW_TEXTURE.add(FLOOR_YELLOW3);
        FLOOR_YELLOW_TEXTURE.add(FLOOR_YELLOW4);

        FLOOR_GREEN_TEXTURE.add(FLOOR_GREEN1);
        FLOOR_GREEN_TEXTURE.add(FLOOR_GREEN2);
        FLOOR_GREEN_TEXTURE.add(FLOOR_GREEN3);
        FLOOR_GREEN_TEXTURE.add(FLOOR_GREEN4);

    }

    public static String getArrayRandomForDropPatrnobox(ItemType itemType, String[] array)
    {
        int i;
        switch (itemType)
        {
            case PATRON:
                i = MathUtils.random(0, array.length - 1);
                return array[i];
        }
        return null;
    }

    public static String getArrayRandomForDropAmmunition(ItemType itemType, String[] array)
    {
        int i;
        switch (itemType)
        {
            case HP:
                i = MathUtils.random(0/*,2*/);
                return array[i];
            case WEAPON:
                i = MathUtils.random(0, 1);
                return array[i];
            case ARMOR:
                return array[0];
        }
        return null;
    }

    public static String getArrayRandom(ItemType itemType, String[] array)
    {
        switch (itemType)
        {
            case WEAPON:
                int rnd = new Random().nextInt(100);
                if (rnd >= 0 && rnd < 9)
                { //9%
                    return array[0];
                }
                else if (rnd >= 9 && rnd < 18)
                { //9%
                    return array[7];
                }
                else if (rnd >= 18 && rnd < 36)
                {//18%
                    return array[1];
                }
                else if (rnd >= 36 && rnd < 57)
                { //21%
                    return array[2];
                }
                else if (rnd >= 57 && rnd < 64)
                {//7%
                    return array[3];
                }
                else if (rnd >= 64 && rnd < 82)
                {//18%
                    return array[4];
                }
                else if (rnd >= 82 && rnd < 91)
                {//9%
                    return array[5];
                }
                else
                {
                    return array[6];
                }

            case HP:
//                rnd = new Random().nextInt(100);
//                if(rnd >= 0 && rnd < 50) {
                return array[0];
//                }
//                else if(rnd >= 50 && rnd < 80) {
//                    return array[1];
//                }
//                else
//                    return array[2];

            case ARMOR:
                return array[0];

            case PATRON:
                rnd = MathUtils.random(0, 4);
                return array[rnd];
            case AMMUNITION:
                rnd = MathUtils.random(0, 1);
                return array[rnd];
        }
        return null;
    }

    /*public static String getArrayRandom (String[] array){
        int rnd = new Random().nextInt(array.length);
        return array[rnd];

    }*/


    public static boolean isServer = false; // some of logic depends on environment type : client/server
    //public static boolean isDedicatedServer = false; // non-dedicated server means that player can host server and have some gui to vizualize its state
    public static boolean isBotSimulator = false;
    public static boolean DebugMode = false;
    public static final boolean PRODUCTION_MODE = true; // one variable to switch between local and production awenv
    public static final boolean MAPS_SWITH = true; // true is ProductionMap false testMap

    public static final int ANIMAL_INITIAL_SPEED = 10;
    public static final int ANIMAL_MAX_HEALTH = 10;

    public static final int PLAYERS_MAX_HEALTH = 100;

    public static final float PLAYERS_MAX_STAMINA = 100f;


    public static final int MAX_QUANTITY_SMALLPATRON = 90;     // AK47
    public static final int MAX_QUANTITY_MEDIUMPATRON = 90;    //pistol,machinegun
    public static final int MAX_QUANTITY_HARDPATRON = 25;      //shotgun
    public static final int MAX_QUANTITY_MINIHARDPATRON = 24;  //rocketLauncher
    public static final int MAX_QUANTITY_ROCKET = 2;          //bazooka

    public static final int PLAYERS_MAX_ARMOR = 100;
    public static final int PLAYERS_MIN_ARMOR = 1;
    public static final float DAMAG_WITH_ARMOR = 1.5f;

    public static final int PLAYER_INITIAL_SPEED = 13;
    public static final int PLAYER_SIGHT_RADIUS = 2000;
    public static final int PLAYER_DEFAULT_RADIUS = 75;
    public static final int PLAYER_DEFAULT_SPEED = 200;
    public static final int PLAYER_HEALTH_AFTER_HELP = 30;
    public static final int EXIT_FROM_PIT_CHECK_ANGLE = 14;

    //connection
    public static final String PRODUCTION_SERVER_URL_SOLO = "mazersolo.herokuapp.com";  //use only for bot test
    public static final String PRODUCTION_SERVER_URL_SOLO2 = "mazersolo2.herokuapp.com";  //use only for bot test
    public static final String PRODUCTION_SERVER_URL_SOLO3 = "mazersolo3.herokuapp.com";  //use only for bot test
    public static final String PRODUCTION_SERVER_URL_SOLO4 = "mazersolo4.herokuapp.com";  //use only for bot test


    public static final String PRODUCTION_SERVER_URL_DUO = "io-europe-duo.herokuapp.com";  //use only for bot test
    public static final String PRODUCTION_SERVER_URL_SQUAD = "io-europe-squad.herokuapp.com";  //use only for bot test
    public static final String PRODUCTION_SERVER_URL_2TEAMS = "io-europe-2teams.herokuapp.com";  //use only for bot test

    public static final int PRODUCTION_SERVER_PORT = 80;
    public static final String LOCAL_MASTER_SERVER_URL = "http://localhost:8081";
    //  public static final String MASTER_SERVER_URL = "http://localhost:8081";
    public static final String MASTER_SERVER_URL = "https://coffee-shop-test.herokuapp.com/";
    public static final String LOCAL_SERVER_URL = "localhost";
    public static final int LOCAL_SERVER_PORT = 8000;

    //game server settings
    public static final int UPDATE_TIME_FOR_SUPPRESSED_PLAYERS = 1000; //ms
    public static final int GAME_DURATION = 600; //s
    public static final int ZONE_GREEN_DURATION = 300; //s
    public static final int ZONE_YELLOW_DURATION = 180; //s
    public static final int ZONE_RED_DURATION = 120; //s

    public static final int WARMUP_PHASE_DURATION = 20000; //ms
    public static final int MASTER_SERVER_DATA_POST_INTERVAL = 5000; //ms
    public static final int MIN_PLAYERS_TO_START_GAME = 10; //todo should be 30-100 in production
    public static final int MAX_PLAYERS_IN_INSTANCE = 20;
    public static final int GAME_WORLD_PER_SERVER = 2;
    public static final GameType DEFAULT_GAME_TYPE = GameType.SOLO;


    //networking
    public static final int HEARTBEAT_RATE = 16; // in milliseconds
    public static final int SERVER_FRAME_RATE = 16; // in milliseconds
    public static final int SERVER_SEND_UPDATES_RATE = 16; // in milliseconds


    public static final int EMOTE_PRESENTATION_TIME = 3; // in seconds
    public static final float TIME_FOR_RECHARGE = 2; // in seconds
    public static final float TIME_FOR_HEAL_BY_MEDKIT = 2000; // in seconds

    public static final float TIME_TO_GRENADE_EXPLODE = 2000; // in milliseconds
    public static final int UPDATE_ALIVEPLAYER_RATE = 300; // in milliseconds
    public static final int PLAYER_UPDATESCREEN_INTERVAL = 300;
    public static final int TIMER_RATE = 1000; // in milliseconds(server)
    public static final int TIMEOUT_AUTOPICK_PATRON = 500; // in milliseconds(client)


    public static final int SHOTGUN_PELLETS_AMMOUNT = 5;
    public static final float MEDKIT_MOVEMENT_SLOWDOWN_COEFF = 0.5f;

    public static final int SINGLE_PLAYER_WIN = 0;
    public static final int TEAM_WIN = 4;
    public static final int TEAM_LOOSE = 5;
    public static final int SQUAD_WIN = 1;
    public static final int SINGLE_PLAYER_DEATH = 2;
    public static final int SQUAD_DEATH = 3;

    public static final int CONQUEST_POINT_TO_WIN = 300;
    public static final int CONQUEST_POINTS_NUMBER = 5;


    public static final int CONQUEST_RED_TEAM_ID = 1;
    public static final int CONQUEST_BLUE_TEAM_ID = 2;


    public static final boolean USE_SHIELDS = false;
    public static final boolean USE_PITS = false;
    public static final boolean USE_WEBS = false;
    public static final boolean USE_SUPPRESSION = false;


}
