package com.github.myio;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.github.myio.dto.AnimalCellDto;
import com.github.myio.dto.ItemDto;
import com.github.myio.dto.MapEntityBaseDto;
import com.github.myio.dto.MapEntityDto;
import com.github.myio.dto.PlayerCellDto;
import com.github.myio.dto.masterserver.PlayerCellBaseDto;
import com.github.myio.entities.Collidable;
import com.github.myio.entities.Grave;
import com.github.myio.entities.IoPoint;
import com.github.myio.entities.Marker;
import com.github.myio.entities.PlayerCell;
import com.github.myio.entities.animals.AnimalCell;
import com.github.myio.entities.item.ItemCell;
import com.github.myio.entities.map.CapturablePointClient;
import com.github.myio.entities.map.MapEntityCell;
import com.github.myio.entities.map.TacticalShield;
import com.github.myio.entities.projectiles.AbstractProjectile;
import com.github.myio.entities.projectiles.FlashbangAmmo;
import com.github.myio.entities.projectiles.GrenadeAmmo;
import com.github.myio.entities.projectiles.ParticlesPoolManager;
import com.github.myio.entities.projectiles.ProjectileManager;
import com.github.myio.entities.tile.Tile;
import com.github.myio.entities.weapons.AbstractRangeWeapon;
import com.github.myio.enums.ItemType;
import com.github.myio.enums.MapEntityType;
import com.github.myio.enums.TileType;
import com.github.myio.inventory.InventoryManager;
import com.github.myio.networking.NetworkManager;
import com.github.myio.networking.packet.CurrentWorldSnapshotPacket;
import com.github.myio.networking.packet.PitInteractionPacker;
import com.github.myio.networking.packet.PlayerRevivingRequest;
import com.github.myio.networking.packet.VisibilityChangePacket;
import com.github.myio.tools.ClientCapturablePointsManager;
import com.github.myio.tools.ClientSquad;
import com.github.myio.tools.IoLogger;
import com.github.myio.ui.ControlsAndroid2;
import com.github.myio.ui.ItemPopUp;
import com.github.myio.ui.RenderManager;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import static com.github.myio.GameConstants.DebugMode;
import static com.github.myio.GameConstants.EXIT_FROM_PIT_CHECK_ANGLE;
import static com.github.myio.GameConstants.PLAYERS_MAX_HEALTH;
import static com.github.myio.GameConstants.PLAYERS_MAX_STAMINA;
import static com.github.myio.GameConstants.PLAYER_UPDATESCREEN_INTERVAL;
import static com.github.myio.GameConstants.TILE_HEIGHT;
import static com.github.myio.GameConstants.TILE_WIDTH;
import static com.github.myio.GameConstants.isBotSimulator;
import static com.github.myio.StringConstants.TACTICAL_SHIELD;

/**
 * Responsibe for creation/updating/deletion and rendering a entities
 */

public class EntityManager
{
    private static final java.lang.String TAG = "EntityManager";
    private Map<String, PlayerCell> enemyCells;
    private Map<String, PlayerCell> allPlayers;
    private Map<String, ItemCell> items;
    private Map<String, MapEntityCell> mapEntities;
    private Map<String, AnimalCell> animals;

    //client-side only
    private Map<String, Marker> markers;
    private Map<String, Grave> graves;

    private int radiusPlayerSight;

    private int tileCountX; //= (int) ( WORLD_WIDTH/TILE_WIDTH);
    private int tileCountY;//= (int) ( WORLD_HEIGHT/TILE_HEIGHT);

    private Tile tiles[][] = new Tile[tileCountX][tileCountY];

    private List<PlayerCell> nearestPlayers = new ArrayList<PlayerCell>();
    private List<AnimalCell> nearestAnimals = new ArrayList<AnimalCell>();
    private List<ItemCell> nearestItems = new ArrayList<ItemCell>();
    private List<MapEntityCell> nearestEntities = new ArrayList<MapEntityCell>();
    private List<Tile> nearestTiles = new ArrayList<Tile>();
    private List<Tile> nearestCollidableTiles = new ArrayList<Tile>();
    private List<Marker> nearestMarkers = new ArrayList<Marker>();
    private List<Grave> nearestGraves = new ArrayList<Grave>();
    private List<AbstractProjectile> nearestProjectiles = new ArrayList<AbstractProjectile>();
    private PlayerCell localPlayer;
    private GameClient game;
    private RenderManager renderManager;
    private ProjectileManager projectileManager;
    private ParticlesPoolManager particlesPoolManager;
    private ItemPopUp itemPopUp;
    private InventoryManager mInventoryManager;
    private NetworkManager networkManager;
    private ClientCapturablePointsManager clientCapturablePointsManager;
    private ControlsAndroid2 controlsAndroid;

    private HashSet<MapEntityCell> entityWithEffect = new HashSet();//When you collide with entity that give some effect we put it here and remove when you don't collide with entity
    private List<AbstractProjectile> projectilesToRemove = new ArrayList<AbstractProjectile>();


    private long lastTimeNearestUpdated;
    private ClientSquad squadClient;
    long debugTimer;
    long updateLoopTimer;
    long updateAutopick;

    Texture texture;
    SpriteBatch batch;

    public void setLocalPlayer(PlayerCell localPlayer)
    {
        this.localPlayer = localPlayer;
    }


    public EntityManager(ControlsAndroid2 controlsAndroid, PlayerCell localPlayer, GameClient game, ItemPopUp itemPopUp, ProjectileManager projectileManager, InventoryManager inventoryManager, NetworkManager networkManager)
    {
        this.radiusPlayerSight = GameConstants.PLAYER_SIGHT_RADIUS;
        this.projectileManager = projectileManager;
        this.localPlayer = localPlayer;
        this.game = game;
        this.networkManager = networkManager;
        this.itemPopUp = itemPopUp;
        this.controlsAndroid = controlsAndroid;
        mInventoryManager = inventoryManager;

        animals = new HashMap<String, AnimalCell>(200);
        enemyCells = new HashMap<String, PlayerCell>(200);
        allPlayers = new HashMap<String, PlayerCell>(200);
        allPlayers.put(localPlayer.getId(), localPlayer);
        items = new HashMap<String, ItemCell>(10000); // estimated item cell count on map
        mapEntities = new HashMap<String, MapEntityCell>(1000);
        markers = new HashMap<String, Marker>(30);
        graves = new HashMap<String, Grave>(30);


        if (isBotSimulator)
        {
            return; // all visuals are not processed by bots
        }
        renderManager = game.getRenderManager();
        particlesPoolManager = new ParticlesPoolManager();

    }

    /* private Map<String, PlayerCell> enemyCells;
     private Map<String, PlayerCell> allPlayers;
     private Map<String, ItemCell> items;
     private Map<String, MapEntityCell> mapEntities;
     private Map<String, AnimalCell> animals;*/
    public EntityManager(PlayerCell localPlayer, GameClient game, ItemPopUp itemPopUp, ProjectileManager projectileManager, InventoryManager inventoryManager, NetworkManager networkManager,

                         Map<String, PlayerCell> enemyCells, Map<String, PlayerCell> allPlayers, Map<String, ItemCell> items, Map<String, MapEntityCell> mapEntities, Map<String, AnimalCell> animals
    )
    {
        this.enemyCells = enemyCells;
        this.allPlayers = allPlayers;
        this.items = items;
        this.mapEntities = mapEntities;
        this.animals = animals;


        this.radiusPlayerSight = GameConstants.PLAYER_SIGHT_RADIUS;
        this.projectileManager = projectileManager;
        this.localPlayer = localPlayer;
        this.game = game;
        this.networkManager = networkManager;
        this.itemPopUp = itemPopUp;
        mInventoryManager = inventoryManager;

        animals = new HashMap<String, AnimalCell>(200);
        enemyCells = new HashMap<String, PlayerCell>(200);
        allPlayers = new HashMap<String, PlayerCell>(200);
        allPlayers.put(localPlayer.getId(), localPlayer);
        items = new HashMap<String, ItemCell>(10000); // estimated item cell count on map
        mapEntities = new HashMap<String, MapEntityCell>(1000);
        markers = new HashMap<String, Marker>(30);
        graves = new HashMap<String, Grave>(30);


        if (isBotSimulator)
        {
            return; // all visuals are not processed by bots
        }
        renderManager = game.getRenderManager();
        particlesPoolManager = new ParticlesPoolManager();

    }

    public void update(float delta)
    {

        if (DebugMode)
        {
            updateLoopTimer = System.currentTimeMillis();
        }

        defineNearestEntities();
        //defineCollisions();

        if (nearestTiles.isEmpty() && nearestPlayers.isEmpty() && nearestItems.isEmpty() && nearestAnimals.isEmpty())
        {
            return; //should happen only at start, we dont need to start batch in this case
        }

        //   game.getBatch().begin();
        if (renderManager.isRenderTiles())
        {
            for (Tile tile : nearestTiles)
            {
                tile.draw();
            }
        }
        // game.getBatch().end();


        // game.getBatch().begin();
        if (renderManager.isRenderItems())
        {
            for (ItemCell f : nearestItems)
            {
                if (!localPlayer.isInsidePit())
                {
                    f.rendererDraw(delta);
                }
            }
        }

        if (renderManager.isRenderMapEntities())
        {
            for (MapEntityCell f : nearestEntities)
            {
                f.rendererDraw(delta);

            }
        }
        if (DebugMode)
        {
            IoLogger.log("Debug", "entityManager  update() tiles+items+entities takes :" + (System.currentTimeMillis() - updateLoopTimer) + " ms)");
        }
        if (renderManager.isRenderMapEntities())
        {
            for (Marker f : nearestMarkers)
            {
                f.rendererDraw(delta);
            }
            for (Grave f : nearestGraves)
            {
                f.rendererDraw(delta);
            }

        }
        // game.getBatch().end();


        //game.getBatch().begin();
        if (renderManager.isRenderAnimals())
        {
            for (AnimalCell animalCell : nearestAnimals)
            {
                animalCell.rendererDraw(delta);
                if (animalCell.isMoving())
                {
                    //TODO: Add sound for animals
                }
            }

        }
        //game.getBatch().end();

        // game.getBatch().begin();
        if (renderManager.isRenderEnemies())
        {
            for (PlayerCell enemyCell : nearestPlayers)
            {
                if (localPlayer.equals(enemyCell))
                {
                    continue;
                }
                if (localPlayer.isInsidePit() && !enemyCell.isInsidePit()
                        || !localPlayer.isInsidePit() && enemyCell.isInsidePit()
                        || enemyCell.isUnderWeb())
                { //don't draw enemies if player inside jug
                    if (enemyCell.isMoving())
                    {
                        GameClient.getGame().getSoundManager()
                                .playSoundWithPanAndVolume(enemyCell.getPlayerGroundTypeSound(), enemyCell.getId(),
                                        new IoPoint(localPlayer.x, localPlayer.y), new IoPoint(enemyCell.x, enemyCell.y));
                    }
                    continue;
                }
                enemyCell.rendererDraw(delta);
                if (enemyCell.isMoving())
                {
                    GameClient.getGame().getSoundManager()
                            .playSoundWithPanAndVolume(enemyCell.getPlayerGroundTypeSound(), enemyCell.getId(),
                                    new IoPoint(localPlayer.x, localPlayer.y), new IoPoint(enemyCell.x, enemyCell.y));
                }
            }
        }
        //game.getBatch().end();
        if (DebugMode)
        {
            IoLogger.log("Debug", "entityManager  update() tiles+items+entities+enemies takes :" + (System.currentTimeMillis() - updateLoopTimer) + " ms)");
        }
        projectilesToRemove.clear();

        //  game.getBatch().begin();
//        game.getGameShapeRenderer().begin();
        for (AbstractProjectile projectile : nearestProjectiles)
        {
            projectile.update(delta);
            // if(!localPlayer.isUnderWeb()) {
            projectile.render(delta);
            projectile.drawTrail();
            // }


            if (projectile.reachedDestination() && projectile.canBeDeleted() || tileCollidesWithProjectile(projectile) )
            {
                if (projectile instanceof FlashbangAmmo)
                {
                    projectile.collide(nearestEntities, nearestTiles);
                }
                else
                { projectile.collide((Collidable) null); }
                if (projectile.canBeRemoved())
                {
                    projectilesToRemove.add(projectile);
                }
            }
            else
            {
                boolean collided = false;
                if (tileCollidesWithProjectile(projectile))
                {
                    for (Tile tile : nearestCollidableTiles)
                    {
                        if (tile.collides(projectile))
                        {
                            projectile.collide(tile);
                            if (projectile.canBeRemoved())
                            {
                                projectilesToRemove.add(projectile);
                                freeProjectile(projectile);
                            }
                            collided = true;
                            break;
                        }
                    }
                }


                for (PlayerCell enemyCell : nearestPlayers)
                {
                    if ((!enemyCell.isInsidePit() && !localPlayer.isInsidePit() || enemyCell.isInsidePit() && localPlayer.isInsidePit()))
                    {
                        if (enemyCell.collides(projectile))
                        {
                            projectile.collide(enemyCell);
                            if (projectile.canBeRemoved())
                            {
                                projectilesToRemove.add(projectile);
                                freeProjectile(projectile);
                                collided = true;
                                break;
                            }
                        }
                    }

                }

                if (!collided)
                {
                    for (AnimalCell animalCell : nearestAnimals)
                    {
                        if (animalCell.canBeKilled)
                        {
                            if (animalCell.collides(projectile))
                            {
                                projectile.collide(animalCell);
                                if (projectile.canBeRemoved())
                                {
                                    projectilesToRemove.add(projectile);
                                    freeProjectile(projectile);
                                    collided = true;
                                    break;
                                }
                            }
                        }
                    }
                }

                if (!collided)
                {
                    for (MapEntityCell entitis : nearestEntities)
                    {
                        if (localPlayer.isInsidePit())
                        {
                            if (entitis.getType() == MapEntityType.PIT && !(projectile instanceof FlashbangAmmo))
                            {
                                if (entitis.collides(projectile))
                                {
                                    projectile.collide(entitis);
                                    if (projectile.canBeRemoved())
                                    {
                                        projectilesToRemove.add(projectile);
                                    }
                                    break;
                                }
                            }
                        }
                        else if (isCollidableEntity(entitis, projectile))
                        {
                            if (entitis.collides(projectile))
                            {
                                projectile.collide(entitis);

                                if (projectile.canBeRemoved())
                                {  //shouldnt delete projectile on collision with web
                                    projectilesToRemove.add(projectile);
                                    freeProjectile(projectile);
                                }
                                break;
                            }
                        }
                    }
                }
            }

        }

        nearestProjectiles.removeAll(projectilesToRemove);
        //  game.getBatch().end();
        if (DebugMode)
        {
            IoLogger.log("Debug", "entityManager whole update() takes :" + (System.currentTimeMillis() - updateLoopTimer) + " ms)");
        }
    }

    private boolean isCollidableEntity(MapEntityCell c, AbstractProjectile patron)
    {

        if (patron instanceof GrenadeAmmo || patron instanceof FlashbangAmmo)return false;
        switch (c.getType())
        {
            case BUSH:
            case TACTICAL_SHIELD:
            case STONE_SMALL:
            case STONE_BIG:
            case STONE_GREEN:
                return true;

        }
        return false;
    }

    private boolean tileCollidesWithProjectile(AbstractProjectile projectile)
    {
        Tile t = getTile(projectile.getCurrentPosition().x, projectile.getCurrentPosition().y);

        if (t != null && t.isObstacle)
        {

            // true for all excep grenADES
            return !(projectile instanceof GrenadeAmmo || projectile instanceof FlashbangAmmo);
        }

        return false;
    }


    private void freeProjectile(AbstractProjectile p)
    {
        projectileManager.freeProjectile(p);
        particlesPoolManager.freeParticleEffect(p.getTrailEffect());
        particlesPoolManager.freeParticleEffect(p.getExplosionEffect());
    }


    public void updateDebug(float delta)
    {
        if (nearestTiles.isEmpty() && nearestPlayers.isEmpty() && nearestItems.isEmpty())
        {
            return; //should happen only at start, we dont need to start batch in this case
        }

        // game.getBatch().begin();
        game.getGameShapeRenderer().setColor(Color.YELLOW);
        game.getGameShapeRenderer().set(ShapeRenderer.ShapeType.Line);
        // game.getGameShapeRenderer().begin(ShapeRenderer.ShapeType.Filled);
        if (renderManager.isRenderTiles())
        {
            for (Tile tile : nearestTiles)
            {
                //if (!tile.getTileType().equals(TileType.FLOOR_BROWN)) //
                tile.drawDebug(); //TODO disable draw Circle about(around) tile
            }
        }


        if (renderManager.isRenderEnemies())
        {
            for (PlayerCell enemyCell : nearestPlayers)
            {
                enemyCell.rendererDrawDebug(delta);
            }
        }
        //game.getGameShapeRenderer().end();

        //game.getGameShapeRenderer().begin(ShapeRenderer.ShapeType.Filled);
        if (renderManager.isRenderItems())
        {
            for (ItemCell f : nearestItems)
            {
                f.rendererDrawDebug(delta);
            }
        }

        if (renderManager.isRenderMapEntities())
        {
            for (MapEntityCell f : nearestEntities)
            {
                f.rendererDrawDebug(delta);
            }
        }


        for (Marker f : nearestMarkers)
        {
            f.rendererDrawDebug(delta);
        }
        for (Grave f : nearestGraves)
        {
            f.rendererDrawDebug(delta);
        }

        //game.getGameShapeRenderer().end();

        //   game.getBatch().end();
    }


    /*
    *  Returns tile which player currently stands on
    * */
    private Tile getTile(float x, float y)
    {

        int j = (int) (x / TILE_WIDTH);
        int i = (int) (y / TILE_HEIGHT);

        if (i < 0 || j < 0)
        {
            return null;
        }
        if (j >= tileCountX || i >= tileCountY)
        {
            return null;
        }
        return tiles[i][j];
    }


    public void addMarker(Marker m)
    {
        if (m != null)
        {
            markers.put(m.getId(), m);
        }
    }
    public void addGrave(Grave m)
    {
        if (m != null)
        {
            graves.put(m.getId(), m);
        }
    }

    public void removeMarker(Marker m)
    {
        markers.remove(m.getId());
    }


    public void defineNearestEntities()
    {
        if (lastTimeNearestUpdated + PLAYER_UPDATESCREEN_INTERVAL > System.currentTimeMillis())
        {
            return;
        }

        // IoLogger.log("defineNearestEntities");
        nearestItems.clear();
        nearestEntities.clear();
        nearestPlayers.clear();
        nearestAnimals.clear();
        nearestTiles.clear();
        nearestCollidableTiles.clear();
        nearestMarkers.clear();
        nearestGraves.clear();


        // Circle r = new Circle(localPlayer.getX() , localPlayer.getY() , PLAYER_SIGHT_RADIUS);
        Circle r = new Circle(localPlayer.getX(), localPlayer.getY(), this.radiusPlayerSight);


        for (PlayerCell enemyCell : enemyCells.values())
        {
            if (r.contains(enemyCell.getX(), enemyCell.getY()))
            {
                nearestPlayers.add(enemyCell);
                Tile playerTile = getTile(enemyCell.x, enemyCell.y);
                if (playerTile != null)
                {

                    //todo
                    if (playerTile.getTileType().equals(TileType.FLOOR_GREEN))
                    {
                        enemyCell.setPlayerGroundType(PlayerCell.GroundType.MUD);
                    }
                    if (playerTile.getTileType().equals(TileType.FLOOR_BROWN))
                    {
                        enemyCell.setPlayerGroundType(PlayerCell.GroundType.STONE);
                    }
                    if (playerTile.getTileType().equals(TileType.FLOOR_YELLOW))
                    {
                        enemyCell.setPlayerGroundType(PlayerCell.GroundType.SAND);
                    }
                }

            }
        }
        nearestPlayers.add(localPlayer);

        for (AnimalCell animalCell : animals.values())
        {
            if (r.contains(animalCell.getX(), animalCell.getY()))
            {
                nearestAnimals.add(animalCell);
            }
        }


        for (ItemCell f : items.values())
        {
            if (r.contains(f.getX(), f.getY()))
            {
                nearestItems.add(f);

            }
        }

        for (MapEntityCell f : mapEntities.values())
        {
            if (r.contains(f.getX(), f.getY()))
            {
                nearestEntities.add(f);
            }
        }

        for (Marker f : markers.values())
        {
            if (r.contains(f.getX(), f.getY()))
            {
                nearestMarkers.add(f);
            }
        }

        for (Grave f : graves.values())
        {
            if (r.contains(f.getX(), f.getY()))
            {
                nearestGraves.add(f);
            }
        }

        for (AbstractProjectile p : nearestProjectiles)
        {
            if (!r.contains(p.getCurrentPosition().x, p.getCurrentPosition().y))
            {
                projectilesToRemove.add(p);
            }
        }

        nearestProjectiles.removeAll(projectilesToRemove);


        Tile playerTile = getTile(localPlayer.x, localPlayer.y);
        if (playerTile != null)
        {

            //todo
            if (playerTile.getTileType().equals(TileType.FLOOR_GREEN))
            {
                localPlayer.setPlayerGroundType(PlayerCell.GroundType.MUD);
            }
            if (playerTile.getTileType().equals(TileType.FLOOR_BROWN))
            {
                localPlayer.setPlayerGroundType(PlayerCell.GroundType.STONE);
            }
            if (playerTile.getTileType().equals(TileType.FLOOR_YELLOW))
            {
                localPlayer.setPlayerGroundType(PlayerCell.GroundType.SAND);
            }

            int tilesCount = (int) (this.radiusPlayerSight / TILE_HEIGHT);

            for (int i = playerTile.getI() - tilesCount;
                 i < playerTile.getI() + tilesCount;
                 i++)
            {
                for (int j = playerTile.getJ() - tilesCount;
                     j < playerTile.getJ() + tilesCount;
                     j++)
                {
                    if (i < 0 || j < 0)
                    {
                        continue;
                    }
                    if (i >= tileCountX || j >= tileCountY)
                    {
                        continue;
                    }
                    Tile tile = tiles[i][j];

                    if (tiles[i][j].getTileType().equals(TileType.WALL_BROWN) || tiles[i][j].getTileType().equals(TileType.WALL_YELLOW) || tiles[i][j].getTileType().equals(TileType.WALL_BUSH_BROWN))
                    {
                        if (i > 1)
                        {
                            try
                            {
                                if (tiles[i - 1][j].getTileType().equals(TileType.WALL_BROWN) || tiles[i - 1][j].getTileType().equals(TileType.WALL_YELLOW) || tiles[i - 1][j].getTileType().equals(TileType.WALL_BUSH_BROWN))
                                {
                                    tile.setRotation(90);
                                }
                            }
                            catch (Exception e)
                            {
                                e.printStackTrace();
                            }
                        }
                        if (i < tileCountY - 1)
                        {
                            try
                            {
                                if (tiles[i + 1][j].getTileType().equals(TileType.WALL_BROWN) || tiles[i + 1][j].getTileType().equals(TileType.WALL_YELLOW) || tiles[i + 1][j].getTileType().equals(TileType.WALL_BUSH_BROWN))
                                {
                                    tile.setRotation(90);
                                }
                            }
                            catch (Exception e)
                            {
                                e.printStackTrace();
                            }
                        }
                        if (j > 1)
                        {
                            try
                            {
                                if (tiles[i][j - 1].getTileType().equals(TileType.WALL_BROWN) || tiles[i][j - 1].getTileType().equals(TileType.WALL_YELLOW) || tiles[i][j - 1].getTileType().equals(TileType.WALL_BUSH_BROWN))
                                {
                                    tile.setRotation(0);
                                }
                            }
                            catch (Exception e)
                            {
                                e.printStackTrace();
                            }
                        }
                        if (j < tileCountX - 1)
                        {
                            try
                            {
                                if (tiles[i][j + 1].getTileType().equals(TileType.WALL_BROWN) || tiles[i][j + 1].getTileType().equals(TileType.WALL_YELLOW) || tiles[i][j + 1].getTileType().equals(TileType.WALL_BUSH_BROWN))
                                {
                                    tile.setRotation(0);
                                }
                            }
                            catch (Exception e)
                            {
                                e.printStackTrace();
                            }
                        }
                        tile.initWallSprite(tiles[i][j].getTileType());
                    }
                    else if (tiles[i][j].getTileType().equals(TileType.FLOOR_BROWN))
                    {
                        tile.initFloorSprite(TileType.FLOOR_BROWN);
                    }
                    else if (tiles[i][j].getTileType().equals(TileType.FLOOR_YELLOW))
                    {
                        tile.initFloorSprite(TileType.FLOOR_YELLOW);
                    }
                    else if (tiles[i][j].getTileType().equals(TileType.FLOOR_GREEN))
                    {
                        tile.initFloorSprite(TileType.FLOOR_GREEN);
                    }
                    nearestTiles.add(tile);

                    if (tile.isObstacle)
                    {
                        nearestCollidableTiles.add(tile);
                    }

                }
            }

            nearestTiles.removeAll(Collections.singleton(null)); // removing all nulls
            nearestCollidableTiles.removeAll(Collections.singleton(null)); // removing all nulls
        }
        lastTimeNearestUpdated = System.currentTimeMillis();

    }

    private List<Tile> tilesForCollisions = new ArrayList<Tile>();

    private List<Collidable> resultCells = new ArrayList<Collidable>();

    public List<IoPoint> defineCollisions()
    {
        if (DebugMode)
        {
            debugTimer = System.currentTimeMillis();
        }

        resultCells = new ArrayList<Collidable>();
        List<IoPoint> resultPoints = new ArrayList<IoPoint>();

        itemPopUp.itemInfoPickupRemove();
        controlsAndroid.setChekBtnPick(false);
        for (PlayerCell enemyCell : nearestPlayers)
        {
            if (localPlayer.equals(enemyCell))
            {
                continue;
            }

            if (localPlayer.collides(enemyCell))
            {
                //  localPlayer.dealDamage(game.getNetworkManager(), localPlayer.getId(), enemyCell.getId());
                if (enemyCell.isSuppressed())
                {
                    for (String memberId : squadClient.getSquadIdList())
                    {
                        if (enemyCell.getId().equals(memberId))
                        {
                            itemPopUp.itemInfoPickup("Help");

                            if (Gdx.input.isKeyJustPressed(Input.Keys.F))
                            {
                                // itemPopUp.itemInfoPickupRemove();
                                PlayerRevivingRequest packet = new PlayerRevivingRequest(enemyCell.getId(), false);
                                networkManager.sendPacket(packet);

                            }
                        }
                    }
                }
                resultPoints.add(enemyCell.getPosition());
                resultCells.add(enemyCell);
            }
        }
        for (AnimalCell animalCell : nearestAnimals)
        {
            if (localPlayer.collides(animalCell))
            {
                resultPoints.add(animalCell.getPosition());
                resultCells.add(animalCell);
            }
        }


        ItemCell consumed = null;


        boolean isPickedUp = false;//to player could pick only one item or shield
        boolean collidesWithItem = false;//to pick only nearest weapon or item
        ItemCell nearestItem = null;
        for (ItemCell itemCell : nearestItems)
        {
            if (localPlayer.collides(itemCell))
            {
                if (!collidesWithItem)
                {
                    nearestItem = itemCell;
                }
                else if (localPlayer.getPosition().distance(nearestItem.getPosition()) > localPlayer.getPosition().distance(itemCell.getPosition()))
                {
                    nearestItem = itemCell;
                }
                collidesWithItem = true;
                //itemsCollides.add(itemCell);

            }
        }
        if (collidesWithItem)
        {
            if (!nearestItem.getItemType().equals(ItemType.PATRON))
            itemPopUp.itemInfoPickup(nearestItem.getItem_blueprint(),nearestItem.getItemType());
            controlsAndroid.setChekBtnPick(true);
            if (Gdx.input.isKeyJustPressed(Input.Keys.F) && !isPickedUp && !nearestItem.getItemType().equals(ItemType.PATRON) || controlsAndroid.isPickItem() && !isPickedUp && !nearestItem.getItemType().equals(ItemType.PATRON))
            {
                controlsAndroid.setPickItem(false);
                if (mInventoryManager.canPickItem(nearestItem.getItemType()))
                {
                    itemPopUp.itemInfoPickupRemove();
                    controlsAndroid.setChekBtnPick(false);
                    localPlayer.consumeItem(networkManager, nearestItem);
                    isPickedUp = true;
                    itemPopUp.setClickImageButton(false);
                }

                //AUTOPICK PATRON
            }
            else if (updateAutopick + GameConstants.TIMEOUT_AUTOPICK_PATRON < System.currentTimeMillis())
            {
                if (nearestItem.getItemType().equals(ItemType.PATRON))
                {
                    if (mInventoryManager.canPickItem(nearestItem.getItemType()))
                    {
                        itemPopUp.itemInfoPickupRemove();
                        controlsAndroid.setChekBtnPick(false);
                        localPlayer.consumeItem(networkManager, nearestItem);
                        updateAutopick = System.currentTimeMillis();
                    }
                }
            }
        }
        if (DebugMode)
        {
            IoLogger.log("Debug", "entityManager defineCollisions players+items takes :" + (System.currentTimeMillis() - debugTimer) + " ms)");
        }

        for (MapEntityCell mapEntityCell : nearestEntities)
        {
            if (localPlayer.collides(mapEntityCell))
            {
                if (mapEntityCell.isObstacle())
                {
                    resultPoints.add(mapEntityCell.getPosition());
                    resultCells.add(mapEntityCell);
                }
                if (mapEntityCell.hasSpecialEffect() /*&& !entityWithEffect.contains(mapEntityCell)*/)
                {
                    applySpecialEffectToPlayer(mapEntityCell);

                }
                if (mapEntityCell instanceof TacticalShield)
                {
                    TacticalShield shield = (TacticalShield) mapEntityCell;
                    if (!shield.inUse)
                    {
                        itemPopUp.itemInfoPickup(TACTICAL_SHIELD);
                    }

                    if (Gdx.input.isKeyJustPressed(Input.Keys.F) && !isPickedUp)
                    {
                        // itemPopUp.itemInfoPickupRemove();
                        shield.interact(localPlayer);
                        localPlayer.setInterectedTacticalShild(shield);
                        isPickedUp = true;

                    }
                    if (localPlayer.equals(shield.getOwner()))
                    {
                        resultPoints.remove(shield.getPosition());
                        resultCells.remove(shield);
                    }
                    if (localPlayer.getInterectedTacticalShild() != null)
                    {
                        if (localPlayer.getInterectedTacticalShild().equals(shield))
                        {
                            resultPoints.remove(shield.getPosition());
                            resultCells.remove(shield);
                        }
                    }
                }
                if (mapEntityCell instanceof CapturablePointClient && clientCapturablePointsManager != null)
                {
                    clientCapturablePointsManager.startPointInteraction((CapturablePointClient) mapEntityCell);
                }

            }
            else if (entityWithEffect.contains(mapEntityCell))
            {
                removeSpecialEffectFromPlayer(mapEntityCell);
                /*localPlayer.removePlayerAlpha();*/

            }
        }

        if (DebugMode)
        {
            IoLogger.log("Debug", "entityManager defineCollisions players+items+entities takes :" + (System.currentTimeMillis() - debugTimer) + " ms)");
        }
        defineTilesForCollision();
        for (Tile t : tilesForCollisions)
        {
            if (t.isObstacle)
            {
                resultPoints.add(new IoPoint(t.getX(), t.getY()));
                resultCells.add(t);
            }
        }
        if (DebugMode)
        {
            IoLogger.log("Debug", "entityManager defineCollisions players+items+entities+tiles takes :" + (System.currentTimeMillis() - debugTimer) + " ms)");
        }
        return resultPoints;
    }

    public List<Collidable> getCollidableCells()
    {
        return resultCells;
    }

    private void defineTilesForCollision()
    {

        if (DebugMode)
        {
            debugTimer = System.currentTimeMillis();
        }
        tilesForCollisions.clear();

        Tile t = getTile(localPlayer.x, localPlayer.y);
        //int i = t.getI();
        //int j = t.getJ();
        if (t == null)
        {
            IoLogger.log("DEBUG", "defineTilesForCollision null tile");
            return;
        }
        for (int i = t.getI() - 1;
             i < t.getI() + 2;
             i++)
        {
            for (int j = t.getJ() - 1;
                 j < t.getJ() + 2;
                 j++)
            {
                if (i < 0 || j < 0)
                {
                    continue;
                }
                if (i >= tileCountX || j >= tileCountY)
                {
                    continue;
                }
                tilesForCollisions.add(tiles[i][j]);
            }
        }
        if (DebugMode)
        {
            IoLogger.log("Debug", "entityManager defineTilesForCollision takes :" + (System.currentTimeMillis() - debugTimer) + " ms)");
        }

    }


    private void applySpecialEffectToPlayer(MapEntityCell entityCell)
    {

        if (DebugMode)
        {
            debugTimer = System.currentTimeMillis();
        }
        if (!localPlayer.isUnderWeb())
        {
            if (entityCell.getType().equals(MapEntityType.SPIDER_WEB) || entityCell.getType().equals(MapEntityType.SPIDER_WEB_BIG))
            {
                localPlayer.setVisible(false);
                localPlayer.setPlayerAlpha(450);
                entityWithEffect.add(entityCell);
                localPlayer.setUnderWeb(true);
                networkManager.sendPacket(new VisibilityChangePacket(entityCell.getType(), localPlayer.getId(), true));
            }
            else if (entityCell.getType().equals(MapEntityType.PIT) || entityCell.getType().equals(MapEntityType.PIT_TRAP2))
            {
                if (!localPlayer.isInsidePit())
                {
                    itemPopUp.itemInfoPickup("Pit");
                    if (Gdx.input.isKeyJustPressed(Input.Keys.F))
                    {
                        if (entityCell.getPlayersId().size() <= 1)
                        {
                            //TODO: Add timer on 3 seconds
                            localPlayer.setPositionBeforeInteraction(localPlayer.getPosition());
                            localPlayer.setInsidePit(true);
                            localPlayer.pit = entityCell.getId();
                            itemPopUp.itemInfoPickupRemove();
                            if (entityCell.getPlayersId().size() == 0)
                            {
                                localPlayer.setX(entityCell.getX());
                                localPlayer.setY(entityCell.getY());
                            }
                            else
                            {
                                float x = entityCell.getX() - entityCell.getCollisionRadius() + localPlayer.getCollisionRadius();
                                float y = entityCell.getY();
                                PlayerCell enemy = enemyCells.get(entityCell.getPlayersId().get(0));
                                while (true)
                                {
                                    if (x > entityCell.getX() + entityCell.getCollisionRadius())
                                    {
                                        x = entityCell.getX() - entityCell.getCollisionRadius() + localPlayer.getCollisionRadius();
                                    }
                                    PlayerCell nextStep = new PlayerCell();
                                    nextStep.x = x;
                                    nextStep.y = y;
                                    if (!enemy.collides(nextStep))
                                    {
                                        localPlayer.setX(x);
                                        localPlayer.setY(y);
                                        break;
                                    }
                                    x++;
                                }
                            }
                            entityCell.getPlayersId().add(localPlayer.getId());
                            networkManager.sendPacket(new PitInteractionPacker(localPlayer.getId(), true, entityCell.getId()));
                        }
                        else
                        {
                            //TODO: show text pit full
                        }

                    }
                }
                else
                {
                    itemPopUp.itemInfoPickup("Exit");
                    if (Gdx.input.isKeyJustPressed(Input.Keys.F))
                    {
                        //TODO: Add timer on 3 seconds
                        Vector2 exitVector = new Vector2(localPlayer.getX() - entityCell.getX(), localPlayer.getY() - entityCell.getY());
                        exitVector.setLength(entityCell.getCollisionRadius() + localPlayer.getCollisionRadius() + localPlayer.getCollisionRadius() / 10);

                        float startAngle = exitVector.angle();
                        PlayerCell nextStep = new PlayerCell();
                        boolean collide = false;
                        int i = 0;
                        do
                        {
                            exitVector.setAngle(exitVector.angle() + 1);
                            if (exitVector.angle() >= startAngle + EXIT_FROM_PIT_CHECK_ANGLE)
                            {
                                exitVector.setAngle(startAngle - EXIT_FROM_PIT_CHECK_ANGLE);
                            }
                            nextStep.setX(entityCell.getX() + exitVector.x);
                            nextStep.setY(entityCell.getY() + exitVector.y);


                            for (MapEntityCell entityCell1 : nearestEntities)
                            {
                                if (entityCell1.isObstacle() && entityCell1.collides(nextStep))
                                {
                                    collide = true;
                                    break;
                                }
                            }

                            if (!collide)
                            {
                                for (Tile tile : nearestTiles)
                                {
                                    if (tile.isObstacle && tile.collides(nextStep))
                                    {
                                        collide = true;
                                        break;
                                    }
                                }
                            }

                            if (!collide)
                            {
                                for (PlayerCell playerCell : nearestPlayers)
                                {
                                    if (playerCell.collides(nextStep))
                                    {
                                        collide = true;
                                        break;
                                    }
                                }
                            }

                            if (!collide)
                            {
                                break;
                            }
                        }
                        while (!(exitVector.angle() > startAngle - 0.9 && exitVector.angle() < startAngle + 0.9));


                        if (!collide)
                        {
                            localPlayer.setInsidePit(false);
                            localPlayer.pit = null;
                            entityCell.getPlayersId().remove(localPlayer.getId());
                            itemPopUp.itemInfoPickupRemove();

                            localPlayer.setPosition(new IoPoint(nextStep.getX(), nextStep.getY()));
                            localPlayer.setPositionBeforeInteraction(null);
                            networkManager.sendPacket(new PitInteractionPacker(localPlayer.getId(), false, entityCell.getId()));
                        }
                        else
                        {
                            System.out.println("THERE IS OBSTACLE");
                            //TODO: Add label that there is obstacle
                        }
                    }
                }
            }
        }

        if (DebugMode)
        {
            IoLogger.log("Debug", "entityManager applySpecialEffectToPlayer takes :" + (System.currentTimeMillis() - debugTimer) + " ms)");
        }
    }

    public IoPoint spawnNear(IoPoint point, int collisionRadius, int maxDistance)
    {

        final int offsetStep = 25;
        int offsetY = 0;
        while (offsetY * offsetStep < maxDistance)
        {
            IoPoint tmp = point.copy();

            tmp.y += offsetY * offsetStep;
            float offsetX = 0;
            while (offsetX * offsetStep < maxDistance)
            {
                tmp.x = point.copy().getX() + offsetX * offsetStep;

                //Circle playerCircle = new Circle(tmp.x, tmp.y, collisionRadius);
                PlayerCell playerCell = new PlayerCell();
                playerCell.setPosition(tmp);
                playerCell.setCollisionRadius(collisionRadius);
                Circle entitiesCircle = new Circle(tmp.x, tmp.y, collisionRadius);
                boolean found = true;

                Tile t = getTile(tmp.x, tmp.y);
                if (t == null || t.isObstacle)
                {
                    found = false;
                }
                else
                {
                    //checking 8 nearest tiles + player tile
                    for (Tile tile : nearestTiles)
                    {
                        if (tile != null && tile.isObstacle)
                        {
                            if (tile.collides(localPlayer))
                            {
                                found = false;
                            }
                        }
                    }
                }
                for (PlayerCell p : nearestPlayers)
                {
                    if (playerCell.collides(p))
                    {
                        found = false;
                    }
                }
                for (AnimalCell a : nearestAnimals)
                {
                    if (playerCell.collides(a))
                    {
                        found = false;
                    }
                }

                for (MapEntityCell m : nearestEntities)
                {
                    entitiesCircle.setPosition(m.x, m.y);
                    entitiesCircle.setRadius(m.getCollisionRadius());
                    if (playerCell.collides(m))
                    {
                        found = false;
                    }
                }

                if (found)
                {
                    return tmp;
                }
                offsetX *= -1;
                if (offsetX >= 0)
                {
                    offsetX++;
                }
            }
            offsetY *= -1;
            if (offsetY >= 0)
            {
                offsetY++;
            }

        }

        IoLogger.log(TAG + "spawnNear cannot find point to spawn @: " + point);
        return point;

    }


    public void removeSpecialEffectFromPlayer(MapEntityCell entityCell)
    {
        if (entityCell.getType().equals(MapEntityType.SPIDER_WEB) || entityCell.getType().equals(MapEntityType.SPIDER_WEB_BIG))
        {
            localPlayer.removePlayerAlpha();
            entityWithEffect.remove(entityCell);
            networkManager.sendPacket(new VisibilityChangePacket(entityCell.getType(), localPlayer.getId(), false));
            localPlayer.setUnderWeb(false);
        }
        else if (entityCell.getType().equals(MapEntityType.PIT) || entityCell.getType().equals(MapEntityType.PIT_TRAP2))
        {
            //localPlayer.removePlayerAlpha();
        }
    }

    public HashSet<MapEntityCell> getEntityWithEffect()
    {
        return entityWithEffect;
    }


    public void addNewItem(ItemDto e)
    {
        ItemCell res = new ItemCell(game.getBatch(), game.getGameShapeRenderer(),
                e.getX(), e.getY(), e.getId(), e.getItemType(), e.getItem_blueprint(), e.getAmmoAmount(), e.getClipAmmoAmount());

        items.put(e.getId(), res);
    }

    public void addNewPlayer(PlayerCell e)
    {
        enemyCells.put(e.getId(), e);
        allPlayers.put(e.getId(), e);
    }

    public void addNewPlayer(PlayerCellDto e)
    {
        PlayerCellDto newPlayer = e;

        PlayerCell enemy = new PlayerCell(game.getBatch(), game.getGameShapeRenderer(), GameClient.getFont32Black(),
                newPlayer.getUsername(), newPlayer.getX(), newPlayer.getY(),
                100, e.getId(), PLAYERS_MAX_HEALTH, (int) PLAYERS_MAX_STAMINA, PLAYERS_MAX_STAMINA, newPlayer.getCurrentHealth(), 0, newPlayer.getPlayerSkinColor());
        enemy.setRotationAngle(newPlayer.getRotationAngle());
        //IoLogger.log("assNewPlayer() = [" + newPlayer.getPlayerSkinColor() + "]");
        if (newPlayer.getCurrentWeapon() != null)
        {
            enemy.setCurrentWeapon(AbstractRangeWeapon.Factory(newPlayer.getCurrentWeapon().weaponType, game.getGameShapeRenderer(), enemy));
        }

        addNewPlayer(enemy);
    }

    public void addNewAnimal(AnimalCell animalCell)
    {
        animals.put(animalCell.getId(), animalCell);
    }


    public Map<String, PlayerCell> getAllPlayers()
    {
        return allPlayers;
    }

    public void addNewItem(ItemCell e)
    {
        items.put(e.getId(), e);
    }

    public void removePlayer(String id)
    {
        PlayerCell enemy = enemyCells.get(id);
        enemyCells.remove(id);
        IoLogger.err("PLAYER DELETE");
        allPlayers.remove(id);
        nearestPlayers.remove(enemy);

        if (enemy == null)
        {
            IoLogger.log(TAG, "removePlayer null player: " + id);
        }
    }

    public void removeAnimal(String id)
    {
        AnimalCell animalCell = animals.get(id);
        animals.remove(id);
        nearestAnimals.remove(animalCell);
    }

    public void removeItem(String id)
    {
        ItemCell f = items.get(id);
        nearestItems.remove(f);
        items.remove(id);
    }

    public void removeEntity(String id)
    {
        MapEntityCell f = mapEntities.get(id);

        nearestEntities.remove(f);
        mapEntities.remove(id);

        if (f == null)
        {
            IoLogger.log("removeEntity null entity: id: " + id);
        }


        if (f != null && (f.getType().equals(MapEntityType.SPIDER_WEB_BIG) || f.getType().equals(MapEntityType.SPIDER_WEB)))
        {
            //TODO: Add sound of broken spider
        }
    }


    public List<PlayerCell> getNearestPlayers()
    {
        return nearestPlayers;
    }

    public List<AnimalCell> getNearestAnimals()
    {
        return nearestAnimals;
    }

    public void setItems(ItemDto[] f)
    {
        for (ItemDto kvp : f)
        {
            if (kvp == null)
            {
                break;
            }
            ItemDto e = kvp;
            ItemCell res = new ItemCell(game.getBatch(), game.getGameShapeRenderer(),
                    e.getX(), e.getY(), e.getId(), e.getItemType(), e.getItem_blueprint(), e.getAmmoAmount(), e.getClipAmmoAmount());

            items.put(e.getId(), res);
        }
    }

    public void setEntities(MapEntityDto[] f)
    {
        for (MapEntityDto kvp : f)
        {
            if (kvp == null)
            {
                break;
            }
            MapEntityDto e = kvp;
            MapEntityCell res = null;
            switch (e.getEntityType())
            {

                case PIT_TRAP:
                case PLANT:
                case STONE_SMALL:
                case STONE_BIG:
                case STONE_GREEN:
                case BUSH:
                case PIT:
                case SPIDER_WEB:
                case SPIDER_WEB_BIG:
                case GREEN_ZONE:
                case YELLOW_ZONE:
                case RED_ZONE:
                case PIT_TRAP2:
                case TRAP:
                case MUD:
                case FLOOR_BUSH_GREEN:
                case FLOOR_BUSH_GREEN_FLOWERS:
                    res = new MapEntityCell(game.getBatch(), game.getGameShapeRenderer(),
                            e.getX(), e.getY(), e.getId(), e.getEntityType());
                    break;

                case TACTICAL_SHIELD:
                    res = new TacticalShield(game.getBatch(), game.getGameShapeRenderer(),
                            e.getX(), e.getY(), e.getId(), e.getEntityType());
                    break;

                case CAPTURABLE_POINT:
                    res = new CapturablePointClient(game.getBatch(), game.getGameShapeRenderer(),
                            e.getX(), e.getY(), e.getId(), e.getEntityType());

                    if (clientCapturablePointsManager != null)
                    {
                        clientCapturablePointsManager.addCapturablePoint((CapturablePointClient) res);
                    }
                    break;


            }
            mapEntities.put(e.getId(), res);
        }
    }

    public void addNewEntity(MapEntityDto entityDto)
    {


        MapEntityCell res = null;
        switch (entityDto.getEntityType())
        {

            case PIT_TRAP:

            case PLANT:
            case STONE_SMALL:
            case STONE_BIG:
            case STONE_GREEN:
            case BUSH:
            case PIT:
            case SPIDER_WEB:
            case SPIDER_WEB_BIG:
            case GREEN_ZONE:
            case YELLOW_ZONE:
            case RED_ZONE:
            case PIT_TRAP2:
            case TRAP:
            case MUD:
            case FLOOR_BUSH_GREEN:
            case FLOOR_BUSH_GREEN_FLOWERS:
                res = new MapEntityCell(game.getBatch(), game.getGameShapeRenderer(),
                        entityDto.getX(), entityDto.getY(), entityDto.getId(), entityDto.getEntityType());
                break;

            case TACTICAL_SHIELD:
                res = new TacticalShield(game.getBatch(), game.getGameShapeRenderer(),
                        entityDto.getX(), entityDto.getY(), entityDto.getId(), entityDto.getEntityType());
                break;

            case CAPTURABLE_POINT:
                res = new CapturablePointClient(game.getBatch(), game.getGameShapeRenderer(),
                        entityDto.getX(), entityDto.getY(), entityDto.getId(), entityDto.getEntityType());
        }
        mapEntities.put(entityDto.getId(), res);
    }

    public List<ItemCell> getNearestItems()
    {
        return nearestItems;
    }


    public void applyCurrentWorldSnapshot(CurrentWorldSnapshotPacket packet)
    {
        localPlayer.waitingForCurrentWorldSnapshot = false;

        List<String> serverPlayerIds = new ArrayList<String>();
        for (PlayerCellDto playerCell : packet.players)
        {
            if (playerCell == null)
            {
                break;
            }
            serverPlayerIds.add(playerCell.getId());
            if (allPlayers.containsKey(playerCell.getId()))
            {
                continue; //should only add  players that missing on client
            }
            addNewPlayer(playerCell);
        }

        Map<String, PlayerCell> tmp = new HashMap<String, PlayerCell>(enemyCells);
        for (PlayerCell p : tmp.values())
        {
            if (!serverPlayerIds.contains(p.getId())) // we missed moment when player disconnects
            {
                removePlayer(p.getId());
            }
        }


        List<String> itemIds = new ArrayList<String>();
        for (ItemDto item : packet.items)
        {
            if (item == null)
            {
                break;
            }
            itemIds.add(item.getId());
            if (items.containsKey(item.getId()))
            {
                continue; //should only add items that missing on client
            }
            addNewItem(item);
        }

        Map<String, ItemCell> tmp2 = new HashMap<String, ItemCell>(items);
        for (ItemCell p : tmp2.values())
        {
            if (!itemIds.contains(p.getId())) // we missed moment when item was picked
            {
                removeItem(p.getId());
            }
        }

        List<String> entitiesIds = new ArrayList<String>();
        for (MapEntityDto entityDto : packet.entities)
        {
            if (entityDto == null)
            {
                break;
            }
            entitiesIds.add(entityDto.getId());
            if (mapEntities.containsKey(entityDto.getId()))
            {
                continue; //should only add entities that missing on client
            }
            addNewEntity(entityDto);
        }

        Map<String, MapEntityCell> tmp3 = new HashMap<String, MapEntityCell>(mapEntities);
        for (MapEntityCell p : tmp3.values())
        {
            if (!entitiesIds.contains(p.getId())) // we missed moment when entity was destroyed
            {
                removeEntity(p.getId());
            }
        }


        //todo do we need it for  animals?

    }

    public void setEntitiesData(MapEntityBaseDto[] entitiesData)
    {
        for (MapEntityBaseDto entityDto : entitiesData)
        {
            if (entityDto == null)
            {
                break; // we can break here, since we have consecutive array of players, so if we have null player, then all followiung playesr also would be nulls
            }
            if (mapEntities.containsKey(entityDto.getId()))
            {
                MapEntityCell e = mapEntities.get(entityDto.getId());
                if (e == null)
                {
                    break;
                }
                if (localPlayer.shiled != null)
                {
                    if (e.getType() == MapEntityType.TACTICAL_SHIELD && e.getId().equals(localPlayer.shiled.getId()))
                    {

                        continue;//don't interpolate shield pos
                    }
                }
                e.setX(entityDto.getX());
                e.setY(entityDto.getY());
                e.setRotationAngle(entityDto.getRotationAngle());
            }
            else
            {
                localPlayer.requestWorldSnapshot(networkManager);
                IoLogger.log("Update for entity who is not in entity map! " + entityDto.getId());
            }
        }
    }

    public void setPlayersData(PlayerCellBaseDto[] playersData, boolean interpolateLocalPosition)
    {
        for (PlayerCellBaseDto playerCell : playersData)
        {
            if (playerCell == null)
            {
                break; // we can break here, since we have consecutive array of players, so if we have null player, then all following players also would be nulls
            }
            if (enemyCells.containsKey(playerCell.getId()))
            {
                PlayerCell enemy = enemyCells.get(playerCell.getId());
                enemy.setX(playerCell.getX());
                enemy.setY(playerCell.getY());
                //  enemy.setRadiusItem(playerCell.getRadiusItem());
                enemy.setRotationAngle(playerCell.getRotationAngle());
                enemy.setCurrentHealth(playerCell.getCurrentHealth());
                enemy.setCurrentArmor(playerCell.getCurrentArmor());
                // enemy.setCurrentWeapon(AbstractRangeWeapon.Factory(playerCell.getCurrentWeapon().weaponType, game.getShapeRenderer()));  // just for drawing
                // enemy.setWeaponList(AbstractRangeWeapon.Factory2(playerCell.getWeaponList(), game.getShapeRenderer()));  // just for drawing
                //  enemy.setScore(playerCell.getCurrentScore());
                //  enemy.setCurrentLevel(playerCell.getCurrentLevel());
                enemy.setId(playerCell.getId());

            }
            else if (playerCell.getId().equals(localPlayer.getId()))
            {
                localPlayer.setCurrentHealth(playerCell.getCurrentHealth());
                localPlayer.setCurrentArmor(playerCell.getCurrentArmor());
                //localPlayer.setScore(playerCell.getCurrentScore());

                if (interpolateLocalPosition)
                {
                    localPlayer.setX(playerCell.getX());
                    localPlayer.setY(playerCell.getY());
                }
                //  localPlayer.setRadiusItem(playerCell.getRadiusItem());

            }
            else
            {

                if (Gdx.app != null)
                {
                    Gdx.app.error("ENTITY MANAGER", "Update for player who is not in enemyCells map!" + playerCell.getId());
                }
                else
                {
                    IoLogger.log("Update for player who is not in enemyCells map! " + playerCell.getId());
                }

                localPlayer.requestWorldSnapshot(networkManager);

            }
        }
    }

    public void setAnimalsData(AnimalCellDto[] animalsData)
    {
        for (AnimalCellDto animalCellDto : animalsData)
        {
            if (animalCellDto == null)
            {
                break;
            }
            if (animals.containsKey(animalCellDto.getId()))
            {
                AnimalCell animalCell = animals.get(animalCellDto.getId());
                animalCell.setX(animalCellDto.getX());
                animalCell.setY(animalCellDto.getY());
                animalCell.setRotationAngle(animalCellDto.getRotationAngle());
                animalCell.setCurrentHealth(animalCellDto.getCurrentHealth());
                animalCell.setId(animalCellDto.getId());
            }
        }
    }

    public Tile[][] getTiles()
    {
        return tiles;
    }

    public void setTiles(String tileList)
    {
        int x = (int) Math.sqrt(tileList.length()); //(int) (WORLD_WIDTH/TILE_WIDTH);
        int y = (int) Math.sqrt(tileList.length()); //(int) (WORLD_HEIGHT/TILE_HEIGHT);
        tileCountX = x;
        tileCountY = y;

        tiles = new Tile[y][x];
        for (int i = 0;
             i < y;
             i++)
        {
            for (int j = 0;
                 j < x;
                 j++)
            {

                int tileIdx = i * x + j;
                //IoLogger.log("tileIdx "+ tileIdx);
                int tileType = Integer.parseInt(tileList.charAt(tileIdx) + "");
                tiles[i][j] = new Tile(game.getBatch(), game.getGameShapeRenderer(), "0",
                        TILE_WIDTH * j, TILE_HEIGHT * i, TileType.values()[tileType - 1], i, j);

            }
        }

        //tiles = TileSerializer.deserializeTiles(tileList, tileXAmount ,tileYAmount);
    }


    public Map<String, PlayerCell> getEnemyCells()
    {
        return this.enemyCells;
    }

    public Map<String, AnimalCell> getAnimalCells()
    {
        return this.animals;
    }

    // public void setEnemyCells(Map<String, PlayerCell> enemyCells){ this.enemyCells=enemyCells;}
    public Map<String, ItemCell> getItems()
    {
        return this.items;
    }

    public Map<String, MapEntityCell> getEntities()
    {
        return this.mapEntities;
    }

    public void radiusPlus()
    {
        this.radiusPlayerSight += 100;
    }

    public void radiusMinus()
    {
        if (this.radiusPlayerSight >= 100)
        {
            this.radiusPlayerSight -= 100;
        }
    }

    public int getRadiusPlayerSight()
    {
        return this.radiusPlayerSight;
    }

    public void addProjectile(AbstractProjectile p)
    {
        nearestProjectiles.add(p);
    }

    public void addProjectiles(List<AbstractProjectile> p)
    {
        nearestProjectiles.addAll(p);
    }

    public List<AbstractProjectile> getNearestProjectiles()
    {
        return nearestProjectiles;
    }

    public PlayerCell getLocalPlayer()
    {
        return localPlayer;
    }

    public void setSquadClient(ClientSquad squadClient)
    {
        this.squadClient = squadClient;
    }

    public Vector3 getWorldCenter()
    {
        Vector3 worldCenter = new Vector3(tileCountX * 200 / 2, tileCountY * 200 / 2, 0);
        return worldCenter;
    }


    public ClientCapturablePointsManager getClientCapturablePointsManager()
    {
        return clientCapturablePointsManager;
    }

    public void setClientCapturablePointsManager(ClientCapturablePointsManager clientCapturablePointsManager)
    {
        this.clientCapturablePointsManager = clientCapturablePointsManager;
    }
}
