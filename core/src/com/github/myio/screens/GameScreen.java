package com.github.myio.screens;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Cursor;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.graphics.profiling.GLProfiler;
import com.badlogic.gdx.input.GestureDetector;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.utils.Timer;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.github.myio.EntityManager;
import com.github.myio.GameClient;
import com.github.myio.GameConstants;
import com.github.myio.dto.AnimalCellDto;
import com.github.myio.dto.ItemDto;
import com.github.myio.dto.PlayerCellDto;
import com.github.myio.dto.PlayerSessionStatsDto;
import com.github.myio.dto.masterserver.GameType;
import com.github.myio.entities.Collidable;
import com.github.myio.entities.IoPoint;
import com.github.myio.entities.PlayerCell;
import com.github.myio.entities.animals.AnimalCell;
import com.github.myio.entities.map.MapEntityCell;
import com.github.myio.entities.map.TacticalShield;
import com.github.myio.entities.map.ZoneMapManager;
import com.github.myio.entities.projectiles.ParticlesPoolManager;
import com.github.myio.entities.projectiles.ProjectileManager;
import com.github.myio.entities.weapons.AK47;
import com.github.myio.entities.weapons.AbstractRangeWeapon;
import com.github.myio.entities.weapons.Bazooka;
import com.github.myio.entities.weapons.FlashBang;
import com.github.myio.entities.weapons.Grenade;
import com.github.myio.entities.weapons.Knife;
import com.github.myio.entities.weapons.MachineGun;
import com.github.myio.entities.weapons.Pistol;
import com.github.myio.entities.weapons.ShotGun;
import com.github.myio.enums.GameServerState;
import com.github.myio.enums.MapEntityType;
import com.github.myio.enums.PlayerState;
import com.github.myio.enums.WeaponType;
import com.github.myio.inventory.InventoryItemSlot;
import com.github.myio.inventory.InventoryManager;
import com.github.myio.inventory.InventoryPanel;
import com.github.myio.networking.ClientPacketHandler;
import com.github.myio.networking.Packet;
import com.github.myio.networking.PacketType;
import com.github.myio.networking.packet.CurrentWorldSnapshotPacket;
import com.github.myio.networking.packet.EmotePacket;
import com.github.myio.networking.packet.EntityRemovePacket;
import com.github.myio.networking.packet.GameStateChangedPacket;
import com.github.myio.networking.packet.InitialTileSnapshotPacket;
import com.github.myio.networking.packet.InitialWorldSnapshotPacket;
import com.github.myio.networking.packet.ItemPickedPacket;
import com.github.myio.networking.packet.PitInteractionPacker;
import com.github.myio.networking.packet.PlayerDiedPacket;
import com.github.myio.networking.packet.PlayerStatsPacket;
import com.github.myio.networking.packet.ProjectileLaunchedPacket;
import com.github.myio.networking.packet.PvPInfoPacket;
import com.github.myio.networking.packet.ServerErrorPacket;
import com.github.myio.networking.packet.ShieldInteractionPacket;
import com.github.myio.networking.packet.SuicidePacket;
import com.github.myio.networking.packet.TimerGamePacket;
import com.github.myio.networking.packet.UseAmmoPacket;
import com.github.myio.networking.packet.VisibilityChangePacket;
import com.github.myio.networking.packet.WeaponSwitchedPacket;
import com.github.myio.networking.packet.WinPacket;
import com.github.myio.networking.packet.WorldUpdatePacket;
import com.github.myio.tools.ClientCapturablePointsManager;
import com.github.myio.tools.ClientSquad;
import com.github.myio.tools.CustomInputProcessor;
import com.github.myio.tools.IoLogger;
import com.github.myio.tools.SoundManager;
import com.github.myio.ui.AlivePlayersPanel;
import com.github.myio.ui.AmmoPanel;
import com.github.myio.ui.ArmorBar;
import com.github.myio.ui.Compass;
import com.github.myio.ui.ControlsAndroid;
import com.github.myio.ui.ControlsAndroid2;
import com.github.myio.ui.CountDown;
import com.github.myio.ui.DebugPanel;
import com.github.myio.ui.EmotesMenu;
import com.github.myio.ui.ExitMenu;
import com.github.myio.ui.FinalMessage;
import com.github.myio.ui.GameChat;
import com.github.myio.ui.HealthBar;
import com.github.myio.ui.IDrawable;
import com.github.myio.ui.ItemPopUp;
import com.github.myio.ui.KillPanelPvP;
import com.github.myio.ui.LockScreen;
import com.github.myio.ui.RenderManager;
import com.github.myio.ui.SquadInfo;
import com.github.myio.ui.StaminaBar;
import com.github.myio.ui.SystemMessagesPanel;
import com.github.myio.ui.TimerZone;
import com.github.myio.ui.VoiceCommandsMenu;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import static com.github.myio.GameConstants.DebugMode;
import static com.github.myio.GameConstants.MEDKIT_MOVEMENT_SLOWDOWN_COEFF;
import static com.github.myio.GameConstants.PRODUCTION_MODE;
import static com.github.myio.GameConstants.SINGLE_PLAYER_DEATH;
import static com.github.myio.GameConstants.SINGLE_PLAYER_WIN;
import static com.github.myio.GameConstants.SQUAD_DEATH;
import static com.github.myio.GameConstants.SQUAD_WIN;
import static com.github.myio.GameConstants.TEAM_LOOSE;
import static com.github.myio.GameConstants.TEAM_WIN;
import static com.github.myio.networking.PacketType.EMOTE_SHOWN;
import static com.github.myio.networking.PacketType.ENTITY_REMOVE;
import static com.github.myio.networking.PacketType.GAME_WIN;
import static com.github.myio.networking.PacketType.ITEM_PICKED;
import static com.github.myio.networking.PacketType.PIT_INTERACTION;
import static com.github.myio.networking.PacketType.PLAYER_DIED;
import static com.github.myio.networking.PacketType.PLAYER_STATS;
import static com.github.myio.networking.PacketType.SERVER_ERROR;
import static com.github.myio.networking.PacketType.SERVER_STATE_INFO;
import static com.github.myio.networking.PacketType.SHIELD_INTERACTION;
import static com.github.myio.networking.PacketType.VISIBILITY_CHANGE;
import static com.github.myio.networking.PacketType.WEAPON_SWITCHED;


public class GameScreen   implements Screen, ClientPacketHandler, InputProcessor
{
    private CustomInputProcessor customInputProcessor;


    private float staminaMax;// = localPlayer.getMaxStamina();
    private float staminaCurrent;// = localPlayer.getCurrentStamina();
    OrthographicCamera camera;
    ArrayList<IoPoint> queue = new ArrayList<IoPoint>();
    Viewport viewport;
    Viewport UiViewport;
    Skin skin;

    public PlayerCell localPlayer;
    public PlayerCell playerToSpectate;

    private long gldraws;
    private long textureBindings1;
    private long start;
    //private boolean isInChatMode;
    private boolean disconetPlayer;
    private long disconectPlayerPause = 1000;
    private long disconectPlayerCall;
    private int second = 0;
    private int durationPause = 30;
    private boolean isNavigationVisible = false;
    private boolean isSquadMembersVisible = false;
    private boolean isFinisherReloadShotgun = true;
    private int countReloadShotgun;

    boolean zoomingInProgress = false;
    float finalZoom;

    private int minimumAccel = 2;
    private ArrayList<IDrawable> UI;

    private AlivePlayersPanel alivePlayersPanel;

    private DebugPanel debugPanel;
    private EmotesMenu emotesMenu;
    private HealthBar healthBar;

    private StaminaBar staminaBar;

    private ArmorBar armorBar;
    private GameClient game;
    private EntityManager entityManager;
    private RenderManager renderManager;
    private ProjectileManager projectileManager;
    private GameChat gameChat;
    private ClientSquad clientSquad;
    private InventoryPanel inventoryPanel;
    private SystemMessagesPanel messagesPanel;
    private InventoryManager inventoryManager;
    private ParticlesPoolManager particlesPoolManager;
    private ItemPopUp itemPopUp;
    private KillPanelPvP killPanelPvP;
    private TimerZone timerZone;
    private ZoneMapManager zoneMapManager;
    private AmmoPanel ammoPanel;
    private CountDown countDown;
    private SquadInfo squadInfo;
    private FinalMessage finalMessage;
    private VoiceCommandsMenu voiceCommandsMenu;
    private ControlsAndroid controlsAndroid;
    private ControlsAndroid2 controlsAndroid2;
    private ClientCapturablePointsManager clientCapturablePointsManager;
    LockScreen lockScreen;
    Map<String, PlayerSessionStatsDto> statMap;
    private Compass compass;


    private PlayerState localPlayerState;
    private List<IDrawable> spawnedPlayerUi = new ArrayList<IDrawable>();

    private Random rnd = new Random();
    ExitMenu exit;
    public int kickback = 0;

    private ProjectileManager pool;
    private Timer weaponReloadTimer;
    InputMultiplexer inputMultiplexer;
    public boolean isAndroid;

    int serverSeconds;
    public Texture localGrave;


    float touchFireCoordsX ;
    float touchFireCoordsY ;

    float coordsTouchX ;
    float coordsTouchY;

    IoPoint startPoint;//temp var for start spawn point

    public Viewport getViewport()
    {
        return viewport;
    }
    private float speedUp;
    private float walkingPlayerSpeed;

    public GameScreen(GameClient game, PlayerCell local)
    {
        this.game = game;
        if (DebugMode)
        {
            GLProfiler.enable();
        }
        //camera = new OrthographicCamera(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        camera = game.getCamera();
        viewport = game.getGameViewPort();
        UiViewport = game.getUiViewport();
        skin = new Skin(Gdx.files.internal("atlas/default/skin/uiskin.json"), new TextureAtlas("atlas/default/skin/uiskin.atlas"));

        localGrave = new Texture(Gdx.files.internal("atlas/RIP.png"));


        localPlayer = local;
        startPoint = localPlayer.getPosition();
        localPlayerState = PlayerState.PLAYING;
        //

        GameClient.getGame().setLocalPlayer(local); // todo

        inputMultiplexer = new InputMultiplexer();
        camera.zoom = 2f;
        emotesMenu = localPlayer.getEmotesMenu();
        countDown = new CountDown();
        renderManager = game.getRenderManager();
        messagesPanel = game.getMessagesPanel();

        particlesPoolManager = new ParticlesPoolManager();
        projectileManager = new ProjectileManager(game, particlesPoolManager);

        //entityManager = new EntityManager(localPlayer, game,itemPopUp, projectileManager, inventoryManager);
        //alivePlayersPanel = new AlivePlayersPanel( game,camera,entityManager.getAllPlayers(),entityManager,skin);


        localPlayer.setDefaultInventory(true, true);
        inventoryManager = local.getInventoryManager();
        inventoryManager.setNetworkManager(GameClient.getGame().getNetworkManager());

        inventoryPanel = new InventoryPanel(inventoryManager, countDown);
        inventoryManager.setInventoryPanel(inventoryPanel);
        inventoryManager.switchWeaponTo(localPlayer.getWeaponList().get(2));

        exit = new ExitMenu(game, camera, skin,inputMultiplexer);
        controlsAndroid = new ControlsAndroid(this,exit,camera,skin,game,localPlayer);
        controlsAndroid2 = new ControlsAndroid2(this,exit,camera,skin,game,localPlayer);
        itemPopUp = new ItemPopUp(camera,controlsAndroid,localPlayer, game, skin);
        entityManager = new EntityManager(controlsAndroid2,localPlayer, game, itemPopUp, projectileManager, inventoryManager, game.getNetworkManager());
        alivePlayersPanel = new AlivePlayersPanel(game, camera, entityManager.getAllPlayers(), entityManager, skin);

        healthBar = localPlayer.getHealthBar();

        staminaBar = localPlayer.getStaminaBar();

        armorBar = localPlayer.getmArmorBar();
        armorBar.setLocalPlayer(localPlayer);

        debugPanel = new DebugPanel(game, localPlayer, camera, entityManager, skin);
        gameChat = new GameChat(game, localPlayer, camera);
        killPanelPvP = new KillPanelPvP(localPlayer, game, skin);


        ammoPanel = new AmmoPanel(game, local, skin);
        zoneMapManager = new ZoneMapManager();
        timerZone = new TimerZone(game, zoneMapManager, skin, localPlayer);
        statMap = new HashMap<String, PlayerSessionStatsDto>();
        finalMessage = new FinalMessage(game,viewport);
        lockScreen = new LockScreen(game, localPlayerState);
        voiceCommandsMenu = new VoiceCommandsMenu(game,localPlayer,skin,camera);
        GameClient.getGame().getNetworkManager().setLockScreen(lockScreen);


        //compass = new Compass(localPlayer, isCenterLine);
        compass = new Compass(localPlayer);


        UI = new ArrayList<IDrawable>();

        //todo quick hack FIX THIS SQUAD
//        if (!game.getGameType().equals(GameType.SOLO))
//        {
//            clientSquad = new ClientSquad(entityManager);
//            squadInfo = clientSquad.getSquadInfo();
//            clientSquad.setFinalMessage(finalMessage);
//            entityManager.setSquadClient(clientSquad);
//            if(squadInfo != null) {UI.add(squadInfo);}
//        }

        if (game.getGameType().equals(GameType.CONQUEST))
        {
            clientCapturablePointsManager = new ClientCapturablePointsManager(true, skin);
            UI.add(clientCapturablePointsManager);
            entityManager.setClientCapturablePointsManager(clientCapturablePointsManager);
        }

		/* UI */
        //isAndroid = true;
        if (Gdx.app.getType().equals(Application.ApplicationType.Android))
        {

            isAndroid = true;
            //UI.add(renderManager);
            UI.add(messagesPanel);
            UI.add(inventoryPanel);
            UI.add(alivePlayersPanel);
            UI.add(itemPopUp);
            UI.add(exit);
            UI.add(ammoPanel);
            UI.add(emotesMenu);
            UI.add(killPanelPvP);
            UI.add(timerZone);
            UI.add(healthBar);

            /*if(localPlayer.isMoving()){
            UI.add(staminaBar);}*/


            UI.add(armorBar);
            UI.add(countDown);
            UI.add(finalMessage);
            UI.add(lockScreen);
            UI.add(voiceCommandsMenu);
            UI.add(compass);


            //======= Only use android=========
            UI.add(controlsAndroid2);


        }else {
            isAndroid = false;
            UI.add(messagesPanel);
            UI.add(inventoryPanel);
            UI.add(alivePlayersPanel);
            UI.add(itemPopUp);
            UI.add(exit);
            UI.add(ammoPanel);
            UI.add(emotesMenu);
            UI.add(killPanelPvP);
            UI.add(timerZone);
            UI.add(healthBar);
            UI.add(staminaBar);
            UI.add(armorBar);
            UI.add(countDown);
            UI.add(finalMessage);
            UI.add(lockScreen);
            UI.add(voiceCommandsMenu);
            UI.add(compass);

            //UI.add(controlsAndroid2);
        }

//        UI.add(debugPanel);
//        if (!PRODUCTION_MODE)
//        {
//            UI.add(debugPanel);
//        }


        //spawnedPlayerUi.add(renderManager);
        spawnedPlayerUi.add(inventoryPanel);
        spawnedPlayerUi.add(controlsAndroid2);
        //spawnedPlayerUi.add(alivePlayersPanel);
        spawnedPlayerUi.add(ammoPanel);
        //   spawnedPlayerUi.add(squadInfo);
        spawnedPlayerUi.add(healthBar);
        spawnedPlayerUi.add(itemPopUp);
        spawnedPlayerUi.add(staminaBar);
        // spawnedPlayerUi.add(zonesCountdownBar);
        //spawnedPlayerUi.add(hpBar);

        customInputProcessor = new CustomInputProcessor(game, localPlayer, emotesMenu, countDown);


        inputMultiplexer.addProcessor(itemPopUp.getStage());
        if (isAndroid)
        {
            inputMultiplexer.addProcessor(controlsAndroid2.getStage());
        }

        inputMultiplexer.addProcessor(inventoryPanel.getStage());
        inputMultiplexer.addProcessor(customInputProcessor);
        inputMultiplexer.addProcessor(emotesMenu.getStage());
        inputMultiplexer.addProcessor(this);


        Gdx.input.setInputProcessor(inputMultiplexer);


        game.getNetworkManager().addPacketHandler(this);

        IoLogger.log("", "Game screen created");

        messagesPanel.getLabelConnect().setText("WELCOME! " + localPlayer.getUsername() + " please wait for the players");
        weaponReloadTimer = new Timer();
         speedUp= localPlayer.getAccelerationMultiplier() * 1.3f;
         walkingPlayerSpeed = localPlayer.getAccelerationMultiplier() ;
    }

    boolean deadP = false;
    int code1;
    private float lastTime;
    private  float FRAME_DURATION = 30;
    @Override
    public void render(float delta)
    {
        disconectPlayerInPauseMode();

//
//        int fps =(int) (1 / delta);
//
//        if (lastTime + FRAME_DURATION > System.currentTimeMillis())
//        {
//            return;
//        }
//        lastTime =  System.currentTimeMillis();




        if (DebugMode)
        {
            gldraws = GLProfiler.drawCalls;
            textureBindings1 = GLProfiler.textureBindings;
            start = System.currentTimeMillis();
        }

        try
        {

            voiceCommandsMenu.inputPlayerKeysVoiceCom(GameClient.getGame().getNetworkManager(),localPlayer);
            // Tile color
            Gdx.gl.glClearColor(0.43f, 0.34f, 0.28f, 1);
            Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
            Gdx.graphics.setSystemCursor(Cursor.SystemCursor.Crosshair);

            processPlayerState(delta);
            if (DebugMode)
            {
                IoLogger.log("Debug", "processPlayerState   takes :" + (System.currentTimeMillis() - start) + " ms)");
            }

            game.getBatch().setProjectionMatrix(camera.combined);
            game.getGameShapeRenderer().setProjectionMatrix(camera.combined);
            game.getGameShapeRenderer().setAutoShapeType(true);
            game.getGameShapeRenderer().begin(ShapeType.Line);
            game.getGameShapeRenderer().setColor(1, 1, 0, 1);



            game.getBatch().begin();
            entityManager.update(delta);

            if(deadP) {
                game.getBatch().draw(localGrave, localPlayer.getX() - localGrave.getWidth() / 4, localPlayer.getY() - localGrave.getHeight() / 4, localGrave.getWidth() / 2, localGrave.getHeight() / 2);
            }

            if (DebugMode)
            {
                IoLogger.log("Debug", "entityManager update + processPlayerState   takes :" + (System.currentTimeMillis() - start) + " ms)");
            }
            if (renderManager.isRenderPlayer())
            {
                if (localPlayerState.equals(PlayerState.SPECTATE) && playerToSpectate != null)
                {
                    playerToSpectate.rendererDraw(delta);
                }
                else if(!deadP)
                {
                    localPlayer.localRendererDraw(delta);

                }
                //localPlayer.drawGrid();
            }
            game.getBatch().end();




            if (GameConstants.DebugMode)
            {
                localPlayer.rendererDrawDebug(delta);
                entityManager.updateDebug(delta);
            }
            if (clientCapturablePointsManager != null)
            {
                clientCapturablePointsManager.drawCustom(delta);
            }
            game.getGameShapeRenderer().end();



				/* drawing labels*/
                /*game.getBatch().begin();
                game.getFont().draw(game.getBatch(), camera.position.toString(), 10, Gdx.graphics.getHeight() - 10);
				game.getBatch().end();*/
                /*game.getBatch().begin();
                if(renderManager.isRenderPlayer()) localPlayer.drawPlayerLabel();
				game.getBatch().end();*/
            getRenderManager().flashEffect(delta);
                /* drawing ui*/


            if (DebugMode)
            {
                getRenderManager().userInput(); // todo remove from prod
            }
            if (getRenderManager().isRenderUI())
            {
                // game.getUiShapeRenderer().begin(ShapeType.Filled);
                for (IDrawable drawable : UI)
                {
                    drawable.draw(delta);
                }
            }
            getRenderManager().flashEffect(delta);

            if (DebugMode)
            {
                IoLogger.log("Debug", "entityManager + UI  takes :" + (System.currentTimeMillis() - start) + " ms)");
            }
            if (isNavigationVisible)
            {
                Label label = new Label("center", skin);
                label.setFontScale(2f);
                Vector3 playerPos = compass.getCompassPosition();
                Vector3 center = entityManager.getWorldCenter();
                float distanceToLabel = 400f;
                float distance = center.dst(playerPos);

                drawDashedLine(Color.WHITE, game.getGameShapeRenderer(), 10, 10, playerPos, center, 2);
                float lambda = distanceToLabel / (distance - distanceToLabel);

                float labelX = ((playerPos.x + (lambda * center.x)) / (1 + lambda)) - (label.getWidth() / 2);
                float labelY = (playerPos.y + (lambda * center.y)) / (1 + lambda);


                if(distance < distanceToLabel) {
                    label.setPosition(center.x, center.y);
                } else {
                    label.setPosition(labelX, labelY);
                }
                Batch batch = game.getBatch();
                batch.setProjectionMatrix(camera.combined);
                batch.begin();
                label.draw(batch, 1f);
                batch.end();
            }
            if (isSquadMembersVisible) {
                Map<String, PlayerCell> members = clientSquad.getMembers();
                for (Map.Entry<String, PlayerCell> entry : members.entrySet()) {
                    PlayerCell member = entry.getValue();
                    if(!member.getId().equals(localPlayer.getId()) && member != null) {
                        Color memberColor = member.getSquadColor();
                        Vector3 memberPosition = new Vector3(member.getX(), member.getY(), 0);
                        drawDashedLine(memberColor, game.getGameShapeRenderer(), 10, 10, compass.getCompassPosition(), memberPosition, 2);
                        Label label = new Label(member.getUsername(), skin);
                        label.setColor(memberColor);
                        label.setFontScale(2f);
                        Vector3 playerPos = compass.getCompassPosition();
                        Vector3 center = entityManager.getWorldCenter();
                        float distanceToLabel = 200f;
                        float distance = memberPosition.dst(playerPos);

                        float lambda = distanceToLabel / (distance - distanceToLabel);

                        float labelX = ((playerPos.x + (lambda * memberPosition.x)) / (1 + lambda)) - (label.getWidth() / 2);
                        float labelY = (playerPos.y + (lambda * memberPosition.y)) / (1 + lambda);


                        if(distance < distanceToLabel) {
                            label.setPosition(memberPosition.x, memberPosition.y);
                        } else {
                            label.setPosition(labelX, labelY);
                        }
                        Batch batch = game.getBatch();
                        batch.setProjectionMatrix(camera.combined);
                        batch.begin();
                        label.draw(batch, 1f);
                        batch.end();
                    }}}


//            if (localPlayer.isAim() && !localPlayer.hasShield() && !(localPlayer.getCurrentWeapon() instanceof Knife))
//            {
//                Vector3 touchVector = new Vector3();
//                touchVector.set(Gdx.input.getX(), Gdx.input.getY(), 0);
//                camera.unproject(touchVector);
//
//
//                if (localPlayer.getCurrentWeapon() != null)
//                {
//                    Vector2 forward = new Vector2(touchVector.x - localPlayer.getPosition().getX(), touchVector.y - localPlayer.getPosition().getY());
//                    forward.setLength(localPlayer.getRadius() * 2);
//                    Vector2 endVector = new Vector2(0, localPlayer.getCurrentWeapon().projectileBlueprint.maxDistance);
//                    endVector.rotate(localPlayer.getForward().angle() + 180);
//                    IoPoint end = new IoPoint(localPlayer.getFireCoords().add(forward).getX() + endVector.x, localPlayer.getFireCoords().add(forward).getY() + endVector.y);
//                    game.getGameShapeRenderer().begin(ShapeType.Line);
//                    game.getGameShapeRenderer().line(localPlayer.getFireCoords().add(forward).getX(), localPlayer.getFireCoords().add(forward).getY(), end.x, end.y, Color.WHITE, Color.RED);
//                    game.getGameShapeRenderer().end();
//                }
//
//
//            }
        }
        catch (Exception e)
        {

            GameClient.getGame().getMasterServer().postExceptionDetails(e, "Gamescreen::render");
            IoLogger.log("render", e.getMessage());
            e.printStackTrace();
        }

        // todo delete in prod
        processZoom(delta);

        if (DebugMode)
        {
            IoLogger.log("Debug", "gldraws :" + (GLProfiler.drawCalls - gldraws));
            IoLogger.log("Debug", "textureBindings :" + (GLProfiler.textureBindings - textureBindings1));
            IoLogger.log("Debug", "main render takes :" + (System.currentTimeMillis() - start) + " ms)");
        }
    }

    int code;
    SpriteBatch batch;


    private void die()
    {
        deadP = true;

        localPlayerState = PlayerState.DEAD;
        messagesPanel.addLabel("YOU DIE! Press S to spectate, M to go to main menu");
        UI.removeAll(spawnedPlayerUi);




        if (game.getGameType().equals(GameType.CONQUEST) || game.getGameType().equals(GameType.TWO_TEAMS))
        {
            code = TEAM_LOOSE;
        }
        else if (!game.getGameType().equals(GameType.SOLO))
        {
            code = SQUAD_DEATH;
        }
        else
        {

            code = SINGLE_PLAYER_DEATH;

        }
        // Timer.schedule(new Timer.Task()
        // {
        //   @Override
        //   public void run()
        //  {

        finalMessage.showMessage(code, statMap);
        //  }
        // }, 3f);
    }

    private void win()
    {
        // todo add stats showing
        // send stats to server
        // add some visuals

        localPlayerState = PlayerState.WINS;
        messagesPanel.message("YOU WON! Press M to fo to main menu");
        IoLogger.log(localPlayer.getUsername() + "win()");
        UI.removeAll(spawnedPlayerUi);
        int code;

        if (game.getGameType().equals(GameType.CONQUEST) || game.getGameType().equals(GameType.TWO_TEAMS))
        {
            code = TEAM_WIN;
        }
        else if (!game.getGameType().equals(GameType.SOLO))
        {
            code = SQUAD_WIN;
        }
        else
        {
            code = SINGLE_PLAYER_WIN;
        }
        if (statMap != null)
        {
            finalMessage.showMessage(code, statMap);
        }
        else
        {
            IoLogger.log("statMap is null");
        }
    }


    private void processWinsMode()
    {
        if (Gdx.input.isKeyJustPressed(Input.Keys.M))
        {

            game.getNetworkManager().disconnect();
            game.getScreen().dispose();
            game.setScreen(game.getMainMenuScreen());
        }
    }

    public void spectate()
    {
        // todo add some spec panel - "Spectating %player_name%"
        // and remove unnecessary gui

        if (entityManager.getEnemyCells().isEmpty())
        {
            return;
        }
        localPlayerState = PlayerState.SPECTATE;
        UI.removeAll(spawnedPlayerUi);

        messagesPanel.message("Press Q to switch to previous player or E to switch to next player. Press R to respawn");
    }

    private void processDeadMode()
    {
        if (Gdx.input.isKeyJustPressed(Input.Keys.S))
        {
            spectate();
        }

        if (Gdx.input.isKeyJustPressed(Input.Keys.M))
        {

            game.getScreen().dispose();
            game.setScreen(game.getMainMenuScreen());
        }
    }


    private short spectateIndex = 0;

    private void processSpectatorMode()
    {
		/*if(playerToSpectate == null)
		{
			playerToSpectate = entityManager.getEnemyCells().values().iterator().next();

			entityManager.setLocalPlayer(playerToSpectate);
		}

		if(queue.size() == 0)
			queue.add(new IoPoint(playerToSpectate.getX(), playerToSpectate.getY()));
		else if(!queue.get(queue.size() - 1).equals(new IoPoint(playerToSpectate.getX(), playerToSpectate.getY())))
			queue.add(new IoPoint(playerToSpectate.getX(), playerToSpectate.getY()));
*/
		/*if(queue.size() > 5) {
			//camera.position.set(playerToSpectate.getX(), playerToSpectate.getY(), 0);
			camera.position.set(queue.get(0).getX(), queue.get(0).getY(), 0);
			camera.update();
			//queue.add(1, new IoPoint((camera.position.x + queue.get(0).getX())/2, (camera.position.y + queue.get(0).getY())/2));
			queue.remove(0);
		}*/

		/*if(Gdx.input.isKeyJustPressed(Input.Keys.U))
		{
			switchToPlayingMode();
		}*/

        if (playerToSpectate == null)
        {
            playerToSpectate = (PlayerCell) entityManager.getEnemyCells().values().toArray()[0];

            entityManager.setLocalPlayer(playerToSpectate);
            setPlayerUI(playerToSpectate);
        }
        Vector3 position = camera.position;
        position.x = camera.position.x + (playerToSpectate.getX() - camera.position.x) * 0.1f;
        position.y = camera.position.y + (playerToSpectate.getY() - camera.position.y) * 0.1f;
        camera.position.set(position);
        camera.update();

        if (Gdx.input.isKeyJustPressed(Input.Keys.R))
        {
            game.setScreen(game.getMainMenuScreen());
        }

        if (Gdx.input.isKeyJustPressed(Input.Keys.Y))
        {
            int pSize = entityManager.getEnemyCells().values().toArray().length;
            playerToSpectate = (PlayerCell) entityManager.getEnemyCells().values().toArray()[rnd.nextInt(pSize)];
            entityManager.setLocalPlayer(playerToSpectate);
            setPlayerUI(playerToSpectate);
        }

        if (Gdx.input.isKeyJustPressed(Input.Keys.Q))
        {
            spectateIndex--;
            if (spectateIndex < 0)
            {
                spectateIndex = (short) (entityManager.getEnemyCells().size() - 1);
            }
            playerToSpectate = (PlayerCell) entityManager.getEnemyCells().values().toArray()[spectateIndex];
            entityManager.setLocalPlayer(playerToSpectate);
            setPlayerUI(playerToSpectate);
        }
        if (Gdx.input.isKeyJustPressed(Input.Keys.E))
        {
            spectateIndex++;
            if (spectateIndex > entityManager.getEnemyCells().size() - 1)
            {
                spectateIndex = 0;
            }
            playerToSpectate = (PlayerCell) entityManager.getEnemyCells().values().toArray()[spectateIndex];
            entityManager.setLocalPlayer(playerToSpectate);
            setPlayerUI(playerToSpectate);
        }

    }

    private void setPlayerUI(PlayerCell localPlayer)
    {
        // this.localPlayer = localPlayer;

        healthBar = localPlayer.getHealthBar();

        //  staminaBar = localPlayer.getStaminaBar();

        armorBar = localPlayer.getmArmorBar();
        armorBar.setLocalPlayer(localPlayer);
        //inventoryPanel.s(localPlayer);

        messagesPanel.message("Spectating " + localPlayer.getUsername());
    }


    private void switchToPlayingMode()
    {
        localPlayerState = PlayerState.PLAYING;
        UI.addAll(spawnedPlayerUi);

        messagesPanel.message("");
    }

    private void processPlayerState(float delta) throws InterruptedException
    {
        switch (localPlayerState)
        {
            case PLAYING:

                Vector3 position = camera.position;
                position.x = camera.position.x + (localPlayer.getX() - camera.position.x) * 0.1f;
                position.y = camera.position.y + (localPlayer.getY() - camera.position.y) * 0.1f;
                //camera.position.set(localPlayer.getX(), localPlayer.getY(), 0);
                camera.position.set(position);
                camera.update();

                handlePlayingMode(delta);
                //processChat();
                localPlayer.sendHeartBeat(game.getNetworkManager());
                break;
            case DEAD:
                processDeadMode();
                break;
            case WINS:
                processWinsMode();
                break;
            case SPECTATE:
                processSpectatorMode();
                break;
            case IN_LOBBY:

                break;
            case IN_CHAT:
                //processChat();
                localPlayer.sendHeartBeat(game.getNetworkManager());
                break;
        }
    }


    private void processZoom(float delta)
    {
        try
        {
            if (!zoomingInProgress)
            {
                return;
            }

            //IoLogger.log("","camera.zoom =" + camera.zoom);
            camera.zoom *= (1 + delta / 2);
            if (camera.zoom >= finalZoom)
            {
                zoomingInProgress = false;
            }
        }
        catch (Exception e)
        {
            GameClient.getGame().getMasterServer().postExceptionDetails(e, "processZoom");
            IoLogger.log("processZoom", e.getMessage());
            e.printStackTrace();
        }
    }

    private boolean playerLocked = false; // for testing purposes
    PlayerCell nextStep = new PlayerCell();

    private void acceleratePlayer(float delta)
    {
        try
        {

            float diff = delta*805;
            //int playerSpeed = localPlayer.getSpeed();
            float playerSpeed = diff;


            float deltaX = 0;
            float deltaY = 0;
            if(isAndroid) {
                if (localPlayer.getAccelerationMultiplier() > 1f ) {
                    UI.add(staminaBar);
                } else {
                    UI.remove(staminaBar);
                }
            }
            if (isAndroid)
            {
                deltaX = controlsAndroid2.getXTouchPadmovement()*playerSpeed;
                deltaY = controlsAndroid2.getYTouchPadmovement()*playerSpeed;
            }


            if (localPlayer.getCurrentWeapon() != null)
            {
                if (System.currentTimeMillis() - localPlayer.getCurrentWeapon().getLastShotTime() > localPlayer.getCurrentWeapon().getDecelerationTime())
                {
                    kickback = 0;
                }
                else
                {
                    kickback = localPlayer.getCurrentWeapon().getKickback();
                }
            }


			/*if(localPlayer.getCurrentWeapon() instanceof Bazooka)
			{
				localPlayer.setAccelerationMultiplier(0.6f);
			}*/


            if (localPlayer.isAim())
            {
                playerSpeed -= 3;
            }
            if ((localPlayer.getCurrentWeapon() instanceof ShotGun) || (localPlayer.getCurrentWeapon() instanceof Bazooka))
            {
                if (Gdx.input.isKeyPressed(Input.Keys.W))
                {
                    deltaY = playerSpeed;
                    localPlayer.setRotatedAfterStep(false);
                }
                if (Gdx.input.isKeyPressed(Input.Keys.S))
                {
                    deltaY = -playerSpeed;
                    localPlayer.setRotatedAfterStep(false);
                }
                if (Gdx.input.isKeyPressed(Input.Keys.D))
                {
                    deltaX = playerSpeed;
                    localPlayer.setRotatedAfterStep(false);
                }
                if (Gdx.input.isKeyPressed(Input.Keys.A))
                {
                    deltaX = -playerSpeed;
                    localPlayer.setRotatedAfterStep(false);
                }
                //Vector2 kickbackVector = new Vector2(0, localPlayer.getCurrentWeapon().getKickback());
				/*Vector2 kickbackVector = localPlayer.getForward();
 				kickbackVector.setLength(localPlayer.getCurrentWeapon().getKickback());
 				if(kickback != 0) {
 					kickbackVector.rotate(localPlayer.getRotationAngle());
 					deltaY += kickbackVector.y;
 					deltaX += kickbackVector.x;
+				}*/

                Vector2 kickbackVector = new Vector2(0, localPlayer.getCurrentWeapon().getKickback());
                if (kickback != 0)
                {
                    kickbackVector.rotate(localPlayer.getRotationAngle());
                    deltaY += kickbackVector.y;
                    deltaX += kickbackVector.x;
                }
            }
            else
            {

                //WASD movement
                if (Gdx.input.isKeyPressed(Input.Keys.W))
                {
                    //deltaY = playerSpeed - kickback;
                    deltaY = playerSpeed;
                    if (!localPlayer.isAim())
                    {
                        deltaY -= kickback;
                    }
                    else
                    {
                        deltaY -= kickback / 2.0f;
                    }
                    localPlayer.setRotatedAfterStep(false);
                }
                if (Gdx.input.isKeyPressed(Input.Keys.S))
                {
                    //deltaY = -playerSpeed + kickback;
                    deltaY = -playerSpeed;
                    if (!localPlayer.isAim())
                    {
                        deltaY += kickback;
                    }
                    else
                    {
                        deltaY += kickback / 2.0f;
                    }
                    localPlayer.setRotatedAfterStep(false);
                }
                if (Gdx.input.isKeyPressed(Input.Keys.D))
                {
                    //deltaX = playerSpeed - kickback;
                    deltaX = playerSpeed;
                    if (!localPlayer.isAim())
                    {
                        deltaX -= kickback;
                    }
                    else
                    {
                        deltaX -= kickback / 2.0f;
                    }
                    localPlayer.setRotatedAfterStep(false);
                }
                if (Gdx.input.isKeyPressed(Input.Keys.A))
                {
                    //deltaX = -playerSpeed + kickback;
                    deltaX = -playerSpeed;
                    if (!localPlayer.isAim())
                    {
                        deltaX += kickback;
                    }
                    else
                    {
                        deltaX += kickback / 2.0f;
                    }
                    localPlayer.setRotatedAfterStep(false);
                }
            }


            Vector3 touchPos = new Vector3();
            touchPos.set(Gdx.input.getX(), Gdx.input.getY(), 0);
            camera.unproject(touchPos);

            List<IoPoint> collisionsPoints = entityManager.defineCollisions();
            List<Collidable> collisionsCells = entityManager.getCollidableCells();

            //Variant with computing next step


            if (!collisionsPoints.isEmpty())
            {
                nextStep.setInsidePit(localPlayer.isInsidePit());
                nextStep.setX(localPlayer.getX());
                nextStep.setY(localPlayer.getY());


                nextStep.setXPlus(deltaX * 4);
                nextStep.setYPlus(deltaY * 4);
				/*if(deltaX > 0) {
					nextStep.setXMinus(Math.min(PLAYER_SPEED_LIMIT, deltaX / PLAYER_MOUSE_ACCELERATION_RATIO));
				}
				else if(deltaX < 0) {
					nextStep.setXMinus(Math.max(-PLAYER_SPEED_LIMIT, deltaX/ PLAYER_MOUSE_ACCELERATION_RATIO));
				}

				if(deltaY > 0) {
					nextStep.setYMinus(Math.min(PLAYER_SPEED_LIMIT, deltaY / PLAYER_MOUSE_ACCELERATION_RATIO));
				}
				else if(deltaY < 0) {
					nextStep.setYMinus(Math.max(-PLAYER_SPEED_LIMIT, deltaY / PLAYER_MOUSE_ACCELERATION_RATIO));
				}*/
                boolean canMove = true;

                Collidable nearestCellForCollision = null;
                IoPoint nearestCellPoints = new IoPoint(0, 0);
                for (int i = 0;
                     i < collisionsCells.size();
                     i++)
                {
                    Collidable cell = collisionsCells.get(i);
                    if (cell.collides(nextStep))
                    {
                        IoPoint currentCellPoints = collisionsPoints.get(i);
                        if (nextStep.getPosition().distance(currentCellPoints) < nextStep.getPosition().distance(nearestCellPoints))
                        {
                            nearestCellForCollision = cell;
                            nearestCellPoints = currentCellPoints;
                        }
                        movementCollis(nextStep, deltaX, deltaY);
                    }
                }

                if (nearestCellForCollision != null)
                {

                    Vector2 movementVector = new Vector2(nextStep.getX() - localPlayer.getX(), nextStep.getY() - localPlayer.getY());
                    Vector2 ortVector = new Vector2(0, 10);
                    Vector2 vectorToCell = new Vector2(nearestCellPoints.getX() - localPlayer.getX(), nearestCellPoints.getY() - localPlayer.getY());

                    float ortMovementAngle = ortVector.angle(movementVector);
                    float cellMovementAngle = vectorToCell.angle(movementVector);

                    if (ortMovementAngle > 0 && ortMovementAngle <= 45 && cellMovementAngle > 0 /*&& cellMovementAngle <= 80*/)
                    {
                        deltaX = -10;
                        deltaY = 0;
                    }
                    else if (ortMovementAngle >= 45 && ortMovementAngle < 90 && cellMovementAngle < 0 /*&& cellMovementAngle > -80*/)
                    {
                        deltaX = 0;
                        deltaY = 10;

                    }
                    else if (ortMovementAngle > 90 && ortMovementAngle <= 135 && cellMovementAngle > 0 /*&& cellMovementAngle <= 80*/)
                    {
                        deltaX = 0;
                        deltaY = -10;

                    }
                    else if (ortMovementAngle >= 135 && ortMovementAngle < 180 && cellMovementAngle < 0 /*&& cellMovementAngle <= 80*/)
                    {
                        deltaX = -10;
                        deltaY = 0;

                    }
                    else if (ortMovementAngle < 0 && ortMovementAngle >= -45 && cellMovementAngle < 0 /*&& cellMovementAngle >= -80*/)
                    {
                        deltaX = 10;
                        deltaY = 0;

                    }
                    else if (ortMovementAngle <= -45 && ortMovementAngle > -90 && cellMovementAngle > 0 /*&& cellMovementAngle <= 80*/)
                    {
                        deltaX = 0;
                        deltaY = 10;

                    }
                    else if (ortMovementAngle < -90 && ortMovementAngle >= -135 && cellMovementAngle < 0 /*&& cellMovementAngle >= -80*/)
                    {
                        deltaX = 0;
                        deltaY = -10;

                    }
                    else if (ortMovementAngle <= -135 && ortMovementAngle > -180 && cellMovementAngle > 0 /*&& cellMovementAngle >= -80*/)
                    {
                        deltaX = 10;
                        deltaY = 0;

                    } /*else if(cellMovementAngle > 30 *//*&& cellMovementAngle <= 90*//* && ortMovementAngle == 90) {// <-
                        deltaX = -10;
                        deltaY = 0;
                    } else if(cellMovementAngle < -30 *//*&& cellMovementAngle >= -90*//* && ortMovementAngle == -90) {//->
                        deltaX = 10;
                        deltaY = 0;
                    } else if(cellMovementAngle > 0 *//*&& cellMovementAngle <= 90*//* && ortMovementAngle == 0 ){//up
                        deltaX = 0;
                        deltaY = 10;
                    } else if(cellMovementAngle < 0 *//*&& cellMovementAngle >=-90 *//*&& ortMovementAngle == -180) {
                        deltaX = 0;
                        deltaY = -10;
                    }*/
                    else
                    {
                        deltaX = 0;
                        deltaY = 0;
                        canMove = false;
                    }

                    //System.out.println("OrtMovementAngle: " + ortMovementAngle);
                    // System.out.println("cellMovementAngle" + cellMovementAngle);

                    if (canMove)
                    {
                        if (!isCanMove(deltaX, deltaY, collisionsCells))
                        {
                            deltaX = 0;
                            deltaY = 0;
                        }
                    }

                }

            }
//

            if (localPlayer.isMoving() && !localPlayer.hasShield())
            {
                GameClient.getGame().getSoundManager().playSoundWithPanAndVolume(localPlayer.getPlayerGroundTypeSound(),
                        new IoPoint(camera.position.x, camera.position.y),
                        new IoPoint(localPlayer.x, localPlayer.y),
                        1 / localPlayer.getAccelerationMultiplier());
            }
            else if (!localPlayer.isMoving() && !localPlayer.hasShield())
            {
                GameClient.getGame().getSoundManager().stopSound(localPlayer.getPlayerGroundTypeSound());
            }

            if (localPlayer.isMoving() && localPlayer.hasShield())
            {
                GameClient.getGame().getSoundManager().playSoundWithPanAndVolume(SoundManager.MOVING_WITH_SHIELD,
                        new IoPoint(camera.position.x, camera.position.y),
                        new IoPoint(localPlayer.x, localPlayer.y), 1 / localPlayer.getAccelerationMultiplier());
            }
            else if (!localPlayer.isMoving() && localPlayer.hasShield())
            {
                GameClient.getGame().getSoundManager().stopSound(SoundManager.MOVING_WITH_SHIELD);
            }


            if (playerLocked)
            {
                return;
            }

            localPlayer.setXPlus(deltaX * localPlayer.getAccelerationMultiplier());
            localPlayer.setYPlus(deltaY * localPlayer.getAccelerationMultiplier());


            if (deltaX == 0 && deltaY == 0)
            {
                stopPressedShift();
            }
           staminaCurrentForSprint(deltaX, deltaY);
            if (Gdx.input.isKeyPressed(Input.Keys.SHIFT_LEFT))
            {


                if (localPlayer.isUsingInventory )
                {
                    localPlayer.setAccelerationMultiplier(MEDKIT_MOVEMENT_SLOWDOWN_COEFF);
                }else {
                    if (localPlayer.isPlayerHaveStamina())
                    {
                        localPlayer.setAccelerationMultiplier(speedUp);

                    }else {
                        localPlayer.setAccelerationMultiplier(walkingPlayerSpeed);
                    }
                }

            }else {
                if (localPlayer.isUsingInventory ) {
                    localPlayer.setAccelerationMultiplier(MEDKIT_MOVEMENT_SLOWDOWN_COEFF);
                }else {
                    localPlayer.setAccelerationMultiplier(walkingPlayerSpeed);
                }
            }

            //ANDROID
           /* if(isAndroid)
            {
                if (Gdx.input.isTouched(1) && !controlsAndroid2.fire())
                {
                    IoLogger.log("I work--------------------------------------------!!!!!!!!!!!!!!!!!");

                    if (localPlayer.isUsingInventory )
                    {
                        localPlayer.setAccelerationMultiplier(MEDKIT_MOVEMENT_SLOWDOWN_COEFF);
                    }else {
                        if (localPlayer.isPlayerHaveStamina())
                        {
                            localPlayer.setAccelerationMultiplier(speedUp);

                        }else {
                            localPlayer.setAccelerationMultiplier(walkingPlayerSpeed);
                        }
                    }

                }else {
                    if (localPlayer.isUsingInventory ) {
                        localPlayer.setAccelerationMultiplier(MEDKIT_MOVEMENT_SLOWDOWN_COEFF);
                    }else {
                        localPlayer.setAccelerationMultiplier(walkingPlayerSpeed);
                    }
                }

            }*/


        }
        catch (Exception e)
        {

            GameClient.getGame().getMasterServer().postExceptionDetails(e, "acceleratePlayer");
            IoLogger.log("acceleratePlayer", e.getMessage());
            e.printStackTrace();
        }
    }


    private boolean isCanMove(float deltaX, float deltaY, List<Collidable> collisionsCells)
    {
        nextStep.setX(localPlayer.getX());
        nextStep.setY(localPlayer.getY());
        nextStep.setXPlus(deltaX);
        nextStep.setYPlus(deltaY);
        GameClient.getGame().getGameShapeRenderer().begin(ShapeType.Line);
        GameClient.getGame().getGameShapeRenderer().circle(nextStep.getX(), nextStep.getY(), nextStep.radius);
        GameClient.getGame().getGameShapeRenderer().end();
        for (int i = 0;
             i < collisionsCells.size();
             i++)
        {
            Collidable cell = collisionsCells.get(i);
            if (cell.collides(nextStep))
            {
                return false;
            }
        }
        return true;
    }

    public void movementCollis(PlayerCell nextStep, float deltaX, float deltaY)
    {
        //System.out.println("nextStep = [" + nextStep.getX()%100+"|"+nextStep.getY()%100 + "], deltaX = [" + deltaX + "], deltaY = [" + deltaY + "]");


    }

    private boolean processChat() throws InterruptedException
    {

        if (Gdx.input.isKeyJustPressed(Input.Keys.ENTER) && localPlayerState != PlayerState.IN_CHAT)
        {

            localPlayerState = PlayerState.IN_CHAT;
            UI.add(gameChat);
            gameChat.openChat();

            return true;
        }
        if (Gdx.input.isKeyJustPressed(Input.Keys.ENTER) && localPlayerState == PlayerState.IN_CHAT)
        {

            localPlayerState = PlayerState.PLAYING;
            gameChat.closeChat();
            Timer.schedule(new Timer.Task()
            {
                @Override
                public void run()
                {
                    UI.remove(gameChat);
                }
            }, 2);

            return false;
        }
        return true;
    }
    private void handlePlayingMode(float delta)
    {
        try
        {
            if (localPlayer.getCurrentHealth() <= 0)
            {
                die();
                return;
            }

			/*if(Gdx.input.isKeyJustPressed(Input.Keys.U))
			{
				//spectate(); return;
			}/*/

            acceleratePlayer(delta);
            rotatePlayer();

            if (localPlayer.getCurrentWeapon().totalAmmoAmount == 0 && localPlayer.getCurrentWeapon().canShoot()
                    && (localPlayer.getCurrentWeapon() instanceof Grenade || localPlayer.getCurrentWeapon() instanceof FlashBang))
            {
                int slotIndex = localPlayer.getWeaponList().indexOf(localPlayer.getCurrentWeapon());
                // InventoryItemSlot slot = localPlayer.getInventoryManager().getAmmoSlots().get(slotIndex);
                InventoryItemSlot slot = localPlayer.getInventoryManager().getWeaponSlots().get(slotIndex);
                localPlayer.getInventoryManager().removeSlotFromWeapon(slot, localPlayer.getCurrentWeapon());
                //game.getNetworkManager().sendPacket(new WeaponSwitchedPacket(localPlayer.getId(), localPlayer.getCurrentWeapon().weaponType));
                //IoLogger.log("Removed");
            }

            if (Gdx.input.isKeyJustPressed(Input.Keys.ESCAPE))
            {

                if (exit.isVisible())
                {
                    exit.hideDialog();
                }
                else
                {
                    exit.showDialog();
                }
            }

            if (Gdx.input.isKeyPressed(Input.Keys.X))
            {
                camera.zoom += 0.02;
            }
            if (Gdx.input.isKeyPressed(Input.Keys.Z))
            {
                camera.zoom -= 0.02;
            }

            if (Gdx.input.isKeyJustPressed(Input.Keys.L))
            {
                playerLocked = !playerLocked;
            }

            if (Gdx.input.isKeyJustPressed(Input.Keys.M))
            {
                entityManager.addMarker(localPlayer.createMarker());
            }

            if (Gdx.input.isKeyJustPressed(Input.Keys.B)) { // temp input for debugging
                //compass.showCompass();
                isNavigationVisible = !isNavigationVisible;
                isSquadMembersVisible = !isSquadMembersVisible;
            }

            if (Gdx.input.isKeyJustPressed(Input.Keys.T) && !PRODUCTION_MODE)
            {  // todo test function, remove in prod

                IoPoint newPoint = entityManager.spawnNear(startPoint, (int) localPlayer.getCollisionRadius(), 1000);
                localPlayer.setPosition(newPoint);
                /*localPlayer.setX(10000);
                localPlayer.setY(8000);*/
                //	localPlayer.getCurrentWeapon().clipAmmoAmount = 20;
            }
            if (Gdx.input.isKeyJustPressed(Input.Keys.O) && !PRODUCTION_MODE)
            {  // todo test function, remove in prod

                game.getNetworkManager().disconnect();
                dispose();
                game.setScreen(game.getMainMenuScreen());
            }

            if (Gdx.input.isKeyJustPressed(Input.Keys.U) && !PRODUCTION_MODE)
            {  // todo test function, remove in prod

                localPlayer.die(particlesPoolManager);
            }

            if (Gdx.input.isKeyJustPressed(Input.Keys.E))
            {
                nextChangeWeapon();
            }


            if (Gdx.input.isKeyJustPressed(Input.Keys.Q))
            {
                backChangeWeapon();
            }

            if (Gdx.input.isKeyJustPressed(Input.Keys.K))
            {
                messagesPanel.message("Current zone is " + game.getZoneMapManager().getPlayerZone(localPlayer.getPosition()).name());
            }



            if (Gdx.input.isKeyJustPressed(Input.Keys.R) || controlsAndroid.isReloadButton()|| controlsAndroid2.isReloadButton() )
            {
                controlsAndroid.setReloadButton(false);
                controlsAndroid2.setReloadButton(false);
                if (localPlayer.getCurrentWeapon().weaponType.equals(WeaponType.SHOTGUN))
                {
                    if (countDown.isFinished() && isFinisherReloadShotgun)
                        if (localPlayer.getCurrentWeapon().checkAmmo(inventoryManager))
                        {
                            for (int i = 0; i < localPlayer.getCurrentWeapon().quantityAmmoForAlonReload(inventoryManager); i++)
                            {
                                isFinisherReloadShotgun = false;
                                int count = i;
                                weaponReloadTimer.schedule(
                                        new Timer.Task()
                                        {
                                            @Override
                                            public void run() {
                                                reloadWeapon();
                                                System.out.println(" I AM HERE");
                                                ++countReloadShotgun;
                                            }
                                        }, count);

                                if (localPlayer.getCurrentWeapon().quantityAmmoForAlonReload(inventoryManager) <= i + 1)
                                {
                                    isFinisherReloadShotgun = true;

                                }

                            }
                        }

                }
                else
                {
                    reloadWeapon();
                }
            }

            shootingPlayer();
//            if (customInputProcessor.isFire() || controlsAndroid.fire() || controlsAndroid2.isFireTest() )
//            {
//               // if (Gdx.input.isButtonPressed(Input.Buttons.LEFT)  || controlsAndroid2.isFireTest())
//               // {
//                    if (localPlayer.getCurrentWeapon() instanceof ShotGun)
//                    {
//                        stopAmmoReloading();
//                    }
//
//                    if (localPlayer.getCurrentWeapon() != null && localPlayer.getCurrentWeapon().canShoot())
//                    {
//                        Vector3 touchPos = new Vector3();
//
//                        float centerScreenX = Gdx.graphics.getWidth()/2;
//                        float centerScreenY = Gdx.graphics.getHeight()/2;
//
//                        if (isAndroid)
//                        {
//
//
//                                coordsTouchX = -controlsAndroid2.getXTouchPadrotate() * centerScreenX;
//                                coordsTouchY = controlsAndroid2.getYTouchPadrotate() * centerScreenY;
//
//
//                                touchFireCoordsX = centerScreenX - coordsTouchX;
//                                touchFireCoordsY = centerScreenY - coordsTouchY;
//
//                            //touchFireCoordsX = centerScreenX - coordsTouchX;
//                           // touchFireCoordsY = centerScreenY - coordsTouchY;
//
//                            Gdx.app.log("ANDROID2","coordsTouchX = "+coordsTouchX +" touchFireCoordsX = "+touchFireCoordsX+" centerScreenX = "+centerScreenX);
//                            Gdx.app.log("ANDROID2","coordsTouchY = "+coordsTouchY +" touchFireCoordsY = "+touchFireCoordsY+" centerScreenY = "+centerScreenY);
//                            Gdx.app.log("ANDROID2","getXTouchPadmovement = "+controlsAndroid2.getXTouchPadmovement());
//
//                            touchPos.set( touchFireCoordsX,touchFireCoordsY,0 );
//
//
//                            camera.unproject(touchPos);
//                        }else {
//                            touchPos.set(Gdx.input.getX(), Gdx.input.getY(), 0);
//                            camera.unproject(touchPos);
//                        }
//
//
//                        if (touchPos.dst(localPlayer.getPositionVec()) > localPlayer.radius * 2 && localPlayer.getCurrentWeapon().clipAmmoAmount > 0)
//                        {
//                            if (countDown.isFinished())
//                            {
////								if(!localPlayer.isUnderWeb()) {
//                                int slotIndex = localPlayer.getWeaponList().indexOf(localPlayer.getCurrentWeapon());
//                                InventoryItemSlot slot = localPlayer.getInventoryManager().getWeaponSlots().get(slotIndex);
//                                Vector2 forward = new Vector2(touchPos.x - localPlayer.getPosition().getX(), touchPos.y - localPlayer.getPosition().getY());
//                                forward.setLength(localPlayer.getRadius() * 2);
//                                ProjectileLaunchedPacket p = localPlayer.getCurrentWeapon().fire(localPlayer.getFireCoords().add(forward), new IoPoint(touchPos.x, touchPos.y), localPlayer.getRotationAngle(), localPlayer.getId(), slot);
//                                entityManager.addProjectiles(projectileManager.obtainProjectile(p));// todo experimental
//
//
//                                if (((localPlayer.getCurrentWeapon() instanceof Pistol || localPlayer.getCurrentWeapon() instanceof MachineGun || localPlayer.getCurrentWeapon() instanceof AK47|| localPlayer.getCurrentWeapon() instanceof Knife))
//                                        && localPlayer.getCurrentWeapon().totalAmmoAmount > 0)
//                                {
//                                    localPlayer.setRotatedAfterShot(false);
//                                }
//                                //}
//                                else
//                                {
//                                    //TODO: show label that you can't shoot in jug
//                                }
//                            }
//                            else
//                            {
//                                //TODO: show label about count down
//                            }
//                        }
//                        else if (localPlayer.getCurrentWeapon().clipAmmoAmount == 0 && localPlayer.getCurrentWeapon().canShoot())
//                        {
//                            GameClient.getGame().getSoundManager().playSoundWithPanAndVolume(SoundManager.MISFIRE,
//                                    new IoPoint(camera.position.x, camera.position.y), localPlayer.getPosition());
//                            if(!localPlayer.getCurrentWeapon().getReloading()) reloadWeapon();
//                            localPlayer.getCurrentWeapon().setCanShot(false);
//                        }
//                    }
//                //}
//            }
//
//            if (!Gdx.input.isButtonPressed(Input.Buttons.LEFT) || !controlsAndroid.fire() ||  !controlsAndroid2.isFireTest())
//            {
//                if (localPlayer.getCurrentWeapon() != null)
//                {
//                    localPlayer.getCurrentWeapon().setCanShot(true);
//                }
//            }
            if (Gdx.input.isKeyJustPressed(Input.Keys.P))
            {
                game.getNetworkManager().sendPacket(new SuicidePacket(localPlayer.getId()));
            }
        }
        catch (Exception e)
        {
            GameClient.getGame().getMasterServer().postExceptionDetails(e, "handlePlayingMode");
            IoLogger.log("handlePlayingMode", e.getMessage());
            e.printStackTrace();
        }

    }


    private long timeUpdate;
    private float timerGame = 100f;
    boolean stopSprint = true;
    boolean isSprinting = true;

    private void shootingPlayer()
    {
        if (customInputProcessor.isFire()  || controlsAndroid2.isFireTest() )
        {
            // if (Gdx.input.isButtonPressed(Input.Buttons.LEFT)  || controlsAndroid2.isFireTest())
            // {
            if (localPlayer.getCurrentWeapon() instanceof ShotGun)
            {
                stopAmmoReloading();
            }

            if (localPlayer.getCurrentWeapon() != null && localPlayer.getCurrentWeapon().canShoot())
            {
                Vector3 touchPos = new Vector3();

                float centerScreenX = Gdx.graphics.getWidth()/2;
                float centerScreenY = Gdx.graphics.getHeight()/2;

                if (isAndroid)
                {

                    coordsTouchX = -controlsAndroid2.getXTouchPadrotate() * centerScreenX;
                    coordsTouchY = controlsAndroid2.getYTouchPadrotate() * centerScreenY;

                    touchFireCoordsX = centerScreenX - coordsTouchX;
                    touchFireCoordsY = centerScreenY - coordsTouchY;

                    touchPos.set( touchFireCoordsX,touchFireCoordsY,0 );


                    camera.unproject(touchPos);
                }else {
                    touchPos.set(Gdx.input.getX(), Gdx.input.getY(), 0);
                    camera.unproject(touchPos);
                }


                if (touchPos.dst(localPlayer.getPositionVec()) > localPlayer.radius * 2 && localPlayer.getCurrentWeapon().clipAmmoAmount > 0)
                {
                    if (countDown.isFinished())
                    {
//								if(!localPlayer.isUnderWeb()) {
                        int slotIndex = localPlayer.getWeaponList().indexOf(localPlayer.getCurrentWeapon());
                        InventoryItemSlot slot = localPlayer.getInventoryManager().getWeaponSlots().get(slotIndex);
                        Vector2 forward = new Vector2(touchPos.x - localPlayer.getPosition().getX(), touchPos.y - localPlayer.getPosition().getY());
                        forward.setLength(localPlayer.getRadius() * 2);
                        ProjectileLaunchedPacket p = localPlayer.getCurrentWeapon().fire(localPlayer.getFireCoords().add(forward), new IoPoint(touchPos.x, touchPos.y), localPlayer.getRotationAngle(), localPlayer.getId(), slot);
                        entityManager.addProjectiles(projectileManager.obtainProjectile(p));// todo experimental


                        if (((localPlayer.getCurrentWeapon() instanceof Pistol || localPlayer.getCurrentWeapon() instanceof MachineGun || localPlayer.getCurrentWeapon() instanceof AK47|| localPlayer.getCurrentWeapon() instanceof Knife))
                                && localPlayer.getCurrentWeapon().totalAmmoAmount > 0)
                        {
                            localPlayer.setRotatedAfterShot(false);
                        }
                        //}
                        else
                        {
                            //TODO: show label that you can't shoot in jug
                        }
                    }
                    else
                    {
                        //TODO: show label about count down
                    }
                }
                else if (localPlayer.getCurrentWeapon().clipAmmoAmount == 0 && localPlayer.getCurrentWeapon().canShoot())
                {
                    GameClient.getGame().getSoundManager().playSoundWithPanAndVolume(SoundManager.MISFIRE,
                            new IoPoint(camera.position.x, camera.position.y), localPlayer.getPosition());
                    if(!localPlayer.getCurrentWeapon().getReloading()) reloadWeapon();
                    localPlayer.getCurrentWeapon().setCanShot(false);
                }
            }
            //}
        }

        if (!Gdx.input.isButtonPressed(Input.Buttons.LEFT) ||  !controlsAndroid2.isFireTest())
        {
            if (localPlayer.getCurrentWeapon() != null)
            {
                localPlayer.getCurrentWeapon().setCanShot(true);
            }
        }
    }
    private boolean isSprint(float delaX, float deltaY)
    {
        return delaX != 0 && localPlayer.getAccelerationMultiplier() > 1f || deltaY != 0 && localPlayer.getAccelerationMultiplier() > 1f;
    }


    public void reloadWeapon()
    {
        final AbstractRangeWeapon weapon = localPlayer.getCurrentWeapon();
        if (countDown.isFinished())
        {
            if (weapon != null && weapon.canBeReload(localPlayer.getInventoryManager(), weapon))
            {
                weapon.setReloading(true);
                if (countDown.showCountDown(Color.WHITE, weapon, weapon.weaponType.name(), localPlayer.getCurrentWeapon().getReloadTime()))
                {
                    // weaponReloadTimer = new Timer();
                    weaponReloadTimer.schedule(
                            new Timer.Task()
                            {
                                @Override
                                public void run()
                                {
                                    if (localPlayer.getCurrentWeapon() instanceof ShotGun)
                                    {
                                        weapon.reloadShotgun(localPlayer.getInventoryManager(), true, 0);
                                    }
                                    else
                                    {
                                        weapon.reload(localPlayer.getInventoryManager(), true, 0);
                                    }

                                    if (weapon.isCancelReloading())
                                    {
                                        weapon.resetClipAmount();
                                        weapon.setCancelReloading(false);
                                        cancel();
                                    }
                                }
                            },
                            weapon.getTimerForReacheng());
                    if (weapon instanceof MachineGun || weapon instanceof AK47)
                    {
                        GameClient.getGame().getSoundManager().playSoundWithPanAndVolume(SoundManager.WEAPON_RELOAD, new IoPoint(camera.position.x, camera.position.y), new IoPoint(localPlayer.x, localPlayer.y));
                    }
                    if (weapon instanceof Pistol)
                    {
                        GameClient.getGame().getSoundManager().playSoundWithPanAndVolume(SoundManager.PISTOL_RELOAD, new IoPoint(camera.position.x, camera.position.y), new IoPoint(localPlayer.x, localPlayer.y));
                    }
                    if (weapon instanceof ShotGun)
                    {
                        if (countReloadShotgun < localPlayer.getCurrentWeapon().quantityAmmoForAlonReload(inventoryManager))
                        {
                            GameClient.getGame().getSoundManager().playSoundWithPanAndVolume(SoundManager.SHOTGUN_RELOAD_1_PATRON, new IoPoint(camera.position.x, camera.position.y), new IoPoint(localPlayer.x, localPlayer.y), localPlayer.getCurrentWeapon().getReloadTime() / 5.0f / 1000.0f);

                        }else {
                            countReloadShotgun = 0;
                            GameClient.getGame().getSoundManager().playSoundWithPanAndVolume(SoundManager.SHOTGUN_RELOAD_1_PATRON, new IoPoint(camera.position.x, camera.position.y), new IoPoint(localPlayer.x, localPlayer.y), localPlayer.getCurrentWeapon().getReloadTime() / 5.0f / 1000.0f);
                            weaponReloadTimer.schedule(
                                    new Timer.Task()
                                    {
                                        @Override
                                        public void run()
                                        {
                                            GameClient.getGame().getSoundManager().playSoundWithPanAndVolume(SoundManager.SHOTGUN_RELOAD_FINISH, new IoPoint(camera.position.x, camera.position.y), new IoPoint(localPlayer.x, localPlayer.y), localPlayer.getCurrentWeapon().getReloadTime() / 5.0f / 1000.0f);

                                        }}, 0.7f);
                        }

                    }
                }
            }
        }
    }

    public void backChangeWeapon()
    {
        stopAmmoReloading();

        int curIndex = localPlayer.getWeaponList().indexOf(localPlayer.getCurrentWeapon());

        do
        {
            curIndex--;
            if (curIndex < 0)
            {
                curIndex = localPlayer.getWeaponList().size() - 1;
            }
        } while (localPlayer.getWeaponList().get(curIndex) == null);

        if (curIndex < 0)
        {
            AbstractRangeWeapon w = localPlayer.getWeaponList().get(localPlayer.getWeaponList().size() - 1);
            //localPlayer.setCurrentWeapon(w);
            inventoryManager.switchWeaponTo(w);
        }
        else
        {
            AbstractRangeWeapon w = localPlayer.getWeaponList().get(curIndex);
            //localPlayer.setCurrentWeapon(w);
            inventoryManager.switchWeaponTo(w);
        }
    }

    public void nextChangeWeapon()
    {
        stopAmmoReloading();

        int curIndex = localPlayer.getWeaponList().indexOf(localPlayer.getCurrentWeapon());

        do
        {
            curIndex++;
            if (curIndex > localPlayer.getWeaponList().size() - 1)
            {
                curIndex = 0;
            }
        } while (localPlayer.getWeaponList().get(curIndex) == null);

        if (curIndex >= localPlayer.getWeaponList().size())
        {
            AbstractRangeWeapon w = localPlayer.getWeaponList().get(0);
            //localPlayer.setCurrentWeapon(w);
            inventoryManager.switchWeaponTo(w);
        }
        else
        {
            AbstractRangeWeapon w = localPlayer.getWeaponList().get(curIndex);
            //localPlayer.setCurrentWeapon(w);
            inventoryManager.switchWeaponTo(w);
        }
    }

    private void stopAmmoReloading()
    { //TODO: Alex use this method to stop reloading after change weapon
        if (localPlayer.getCurrentWeapon() != null)
        {
            if (!countDown.isFinished())
            {
                localPlayer.getCurrentWeapon().setCancelReloading(true);
                countDown.resetCountDown();
                if (localPlayer.getCurrentWeapon() instanceof MachineGun || localPlayer.getCurrentWeapon() instanceof AK47)
                {
                    GameClient.getGame().getSoundManager().stopSound(SoundManager.WEAPON_RELOAD);
                }
                if (localPlayer.getCurrentWeapon() instanceof Pistol)
                {
                    GameClient.getGame().getSoundManager().stopSound(SoundManager.PISTOL_RELOAD);
                }
                if (localPlayer.getCurrentWeapon() instanceof ShotGun)
                {
                    GameClient.getGame().getSoundManager().stopSound(SoundManager.SHOTGUN_RELOAD);
                }
            }
        }
    }
    float angle = 0;
    float angle2;
    private void rotatePlayer()
    {
        try
        {

            if(isAndroid)
            {

                if(controlsAndroid2.getXTouchPadrotate()!=0 && controlsAndroid2.getYTouchPadrotate()!=0)
                {
                    float radAngle = MathUtils.atan2(controlsAndroid2.getYTouchPadrotate(), controlsAndroid2.getXTouchPadrotate());
                    angle2 = MathUtils.radiansToDegrees * radAngle;
                    angle2 += 90;
                }
                angle = angle2;

            }else {
                Vector2 touchVector = new Vector2(Gdx.graphics.getWidth() / 2 - Gdx.input.getX(), Gdx.graphics.getHeight() / 2 - Gdx.input.getY());
                Vector2 startPos = new Vector2(0, -200);
                angle = touchVector.angle(startPos);
            }


            Vector2 v = new Vector2(controlsAndroid2.getXTouchPadrotate(), controlsAndroid2.getYTouchPadrotate());
            float angle2 = v.angle();

            // Gdx.app.log("ANGLE",angle + " ANGLE");
            // Gdx.app.log("ANGLE2",angle2 + " ANGLE2");

            localPlayer.getForward().setAngle(angle);

            if (!localPlayer.hasShield())
            {
                if (!localPlayer.isRotatedAfterShot())
                {
                    if (localPlayer.getCurrentWeapon() instanceof Pistol || localPlayer.getCurrentWeapon() instanceof MachineGun || localPlayer.getCurrentWeapon() instanceof AK47)
                    {
                        angle -= 9;
                        localPlayer.setRotatedAfterShot(true);
                    }
                    else if (localPlayer.getCurrentWeapon() instanceof Knife)
                    {
                        angle += localPlayer.getKnifeKickAngle();
                    }
                }
                if (!localPlayer.isRotatedAfterStep() && kickback == 0 && !localPlayer.isAim())
                {
                    angle += localPlayer.getAngleForStepRotation();
                    localPlayer.setRotatedAfterStep(true);
                }
            }
            localPlayer.setRotationAngle((int) angle);
            if (localPlayer.hasShield())
            {
                localPlayer.shiled.setRotationAngle(((int) angle));
            }

            //	localPlayer.setRotationAngle((int) (angle * localPlayer.getRotationMultiplier())); // todo check if we want rotation inertia
        }
        catch (Exception e)
        {
            IoLogger.log("rotatePlayer", e.getMessage());
            e.printStackTrace();
        }
    }

    @Override
    public void show()
    {


    }

    @Override
    public void resize(int width, int height)
    {
        try
        {
            camera.viewportWidth = width;
            camera.viewportHeight = height;
			/*viewport.update(width, height);
			viewport.setScreenSize(width,height);*/


        }
        catch (Exception e)
        {
            GameClient.getGame().getMasterServer().postExceptionDetails(e, "Gamescreen::resize");
            IoLogger.log("resize", e.getMessage());
            e.printStackTrace();
        }

    }

    private void disconectPlayerInPauseMode()
    {
//        if (disconetPlayer)
//        {
//            if (disconectPlayerCall + disconectPlayerPause < System.currentTimeMillis())
//            {
//                ++second;
//                if (second == durationPause)
//                {
//                    game.getNetworkManager().removePacketHandler(this);
//                    game.getNetworkManager().disconnect();
//                }
//                disconectPlayerCall = System.currentTimeMillis();
//            }
//        }
    }

    @Override
    public void pause()
    {
        disconetPlayer = true;


    }

    @Override
    public void resume()
    {
        disconetPlayer = false;
    }

    @Override
    public void hide()
    {
        second = 0;

    }

    @Override
    public void dispose()
    {
        try
        {
            game.getNetworkManager().removePacketHandler(this);
            game.getNetworkManager().disconnect();
        }
        catch (Exception e)
        {
            IoLogger.log("dispose", e.getMessage());
        }
    }

    @Override
    public boolean handlePacket(Packet packet)
    {


        try
        {
            if (packet.packetType == PacketType.WORLD_SNAPSHOT)
            {
                InitialWorldSnapshotPacket worldSnapshotPacket = (InitialWorldSnapshotPacket) packet;
                //Read enemies from the packet
                for (PlayerCellDto e : worldSnapshotPacket.players)
                {
                    if (e == null)
                    {
                        break;
                    }
                    if (e.getId().equals(localPlayer.getId()))
                    {
                        continue; // ourselves
                    }


                    entityManager.addNewPlayer(e);

                }

                for (AnimalCellDto a : worldSnapshotPacket.animals)
                {
                    if (a == null)
                    {
                        break;
                    }

                    AnimalCellDto animalCellDto = a;

                    AnimalCell animalCell = new AnimalCell(game.getBatch(), game.getGameShapeRenderer(), animalCellDto.getX(), animalCellDto.getY(),
                            20, animalCellDto.getId(), animalCellDto.getCurrentHealth());
                    animalCell.setRotationAngle(animalCellDto.getRotationAngle());

                    entityManager.addNewAnimal(animalCell);
                }

                if (worldSnapshotPacket.items != null && worldSnapshotPacket.entities != null)
                {
                    entityManager.setItems(worldSnapshotPacket.items);
                    entityManager.setEntities(worldSnapshotPacket.entities);
                }


                //Read item from packet
                //todo seems like copypaste, check if its needed
				/*for (ItemDto e : worldSnapshotPacket.items) {

					ItemDto serverFood = e;
					if (serverFood == null)break;
					ItemCell food = new ItemCell(game.getBatch(), game.getGameShapeRenderer(),
							serverFood.getX(), serverFood.getY(), serverFood.getId(), serverFood.getItemType(),serverFood.getItem_blueprint());

					entityManager.addNewItem(food);
				}*/

                //	IoLogger.log("", "WORLD_SNAPSHOT client 3");
                entityManager.defineNearestEntities();
                //	IoLogger.log("", "WORLD_SNAPSHOT client 4");


                return true;
            }

            if (packet.packetType == PacketType.WORLD_UPDATE)
            {

                WorldUpdatePacket p = (WorldUpdatePacket) packet;
                //if(DebugMode) IoLogger.log("", "WORLD_UPDATE client " + p.toString().length());
                if (p.joinedPlayers != null)
                {
                    for (PlayerCellDto newPlayer : p.joinedPlayers)
                    {
                        if (newPlayer == null)
                        {
                            break;
                        }
                        if (newPlayer.getId().equals(localPlayer.getId()))
                        {
                            continue;
                        }

                        entityManager.addNewPlayer(newPlayer);
                        IoLogger.log("", newPlayer.getUsername() + " just joined");

                    }
                }
                entityManager.setPlayersData(p.playersData, localPlayerState == PlayerState.SPECTATE);
                entityManager.setEntitiesData(p.changedEntitiesData);


                if (p.pickedItems != null)
                {
                    for (String s : p.pickedItems)
                    {
                        if (s == null)
                        {
                            break;
                        }
                        // entityManager.removeItem(s); //TODO we have ITEM PICKED packet, which the delete item
                    }
                }

                if (p.droppedItems != null)
                {
                    for (ItemDto s : p.droppedItems)
                    {
                        if (s == null)
                        {
                            break;
                        }
                        entityManager.addNewItem(s);
                    }
                }
                if (p.leftPlayers != null)
                {
                    for (String disconnected : p.leftPlayers)
                    {
                        if (disconnected == null)
                        {
                            break;
                        }
                        entityManager.removePlayer(disconnected);
                    }
                }
                if (clientSquad != null && clientSquad.getSquadIdList() != null)
                {
                    Map<String, PlayerCell> allPlayers = entityManager.getAllPlayers();
                    for (String id : clientSquad.getSquadIdList())
                    {
                        if (id == null)
                        {
                            break; // end of array
                        }
                        if (allPlayers.get(id) != null)
                        {
                            PlayerCell member = allPlayers.get(id);
                            if (member != null && !member.getId().equals(localPlayer.getId()))
                            {
                                squadInfo.setHealth(id, member.getHealthBar().getHealth().getValue());

                                //squadInfo.setStamina(id, member.getStaminaBar().getStamina().getValue());
                                member.drawPlayerLabel();
                            }
                        }
                    }
                }


                return true;
            }

            if (packet.packetType == PacketType.CURRENT_WORLD_SNAPSHOT)
            {
                entityManager.applyCurrentWorldSnapshot((CurrentWorldSnapshotPacket) packet);
                return true;
            }
            if (packet.packetType == PacketType.HEARTBEAT_DISABLE)
            {
                localPlayer.setCanSendHeartBeats(false);
                return true;
            }

            if (packet.packetType == PacketType.TIMER_GAME)
            {

                TimerGamePacket t = (TimerGamePacket) packet;
                System.out.println("T TIMER ====" + t.timer);
                timerZone.setStartTime(t.timer);
                timerZone.setZoneGreenDuration(t.zoneDuration[0]);
                ;//green zone
                timerZone.setZoneYellowDuration(t.zoneDuration[1]);
                ;//yellow zone
                timerZone.setZoneRedDuration(t.zoneDuration[2]);
                ;//red zone
                return true;
            }
            if (packet.packetType == PacketType.TILES_SNAPSHOT)
            {

                InitialTileSnapshotPacket p = (InitialTileSnapshotPacket) packet;
                entityManager.setTiles(p.tiles);
                game.getZoneMapManager().initZones();
                // compass.setCenter(entityManager.getWorldCenter());
                //	entityManager.setTestTiles(game.getBatch(), game.getShapeRenderer());
                return true;
            }

            if (packet.packetType == PacketType.PROJECTILE_LAUNCHED)
            {

                ProjectileLaunchedPacket p = (ProjectileLaunchedPacket) packet;
                if (!p.shooterId.equals(localPlayer.getId()))
                {
                    entityManager.addProjectiles(projectileManager.obtainProjectile(p));
                }

                return true;
            }

            if (packet.packetType == WEAPON_SWITCHED)
            {
                WeaponSwitchedPacket p = (WeaponSwitchedPacket) packet;
                if (!localPlayer.getId().equals(p.playerId))
                {
                    PlayerCell e = entityManager.getEnemyCells().get(p.playerId);
                    e.setEnemyCurrentWeapon(p.typeToSwitchTo, e);
                    IoLogger.log("GameScreen    packet.packetType == WEAPON_SWITCHED", p.toString());
                }
                return true;
            }
            if (packet.packetType == PacketType.PVP_INFO_PACKET)
            {

                PvPInfoPacket p = (PvPInfoPacket) packet;
                killPanelPvP.addLabel(p.killer, p.victim);

                return true;
            }
            if (packet.packetType == EMOTE_SHOWN)
            {
                EmotePacket p = (EmotePacket) packet;
                IoLogger.log("GameScreen", p.toString());
                if (!localPlayer.getId().equals(p.getPlayerId()))
                {
                    IoLogger.log("GameScreen   receive packet with emote", p.getEmoteName());
                    entityManager.getEnemyCells().get(p.getPlayerId()).showMyEmote(p.getEmoteName());
                }
                return true;
            }

            //TODO server error
//            if (packet.packetType == VOICE_COMMAND_SHOW)
//            {
//                VoiceCommandsMenuPacket p = (VoiceCommandsMenuPacket) packet;
//                IoLogger.log("GameScreen", p.toString());
//                if (!localPlayer.getId().equals(p.playerId))
//                {
//                    IoLogger.log("GameScreen   receive packet with voiceCommands", p.pVoiceComandName);
//                    entityManager.getEnemyCells().get(p.playerId).voiceCommandOteherPlayers(p.pVoiceComandName);
//                }
//                return true;
//            }


			/*if(packet.packetType == USE_MEDKIT)
			{
				UseMedkitPacket p = (UseMedkitPacket) packet;
				if(!localPlayer.getId().equals(p.mPlayerId))
				{
					PlayerCell e = entityManager.getEnemyCells().get(p.mPlayerId);
					e.healByMedKit(p.medkitBlueprint);
					//e.setCurrentHealth(p.medkitBlueprint);
					IoLogger.log("GameScreen", p.toString());
				}
				return true;
			}*/

            if (packet.packetType == PLAYER_STATS)
            {
                PlayerStatsPacket p = (PlayerStatsPacket) packet;

                if (p.playerId.equals(localPlayer.getId()))
                {
                    game.getMessagesPanel().message(p.statsDto.toString());
                    statMap.put(p.playerId, p.statsDto);
                    IoLogger.log("Add own stat");
                }
                if (clientSquad != null && !p.playerId.equals(localPlayer.getId()) && clientSquad.getMembers().containsKey(p.playerId))
                {
                    statMap.put(p.playerId, p.statsDto);
                    IoLogger.log("Add  stat of squadmember");
                }

                return true;
            }

            if (packet.packetType == ENTITY_REMOVE)
            {
                EntityRemovePacket p = (EntityRemovePacket) packet;
                if (entityManager.getEntityWithEffect().contains(entityManager.getEntities().get(p.entityId)))
                {
                    entityManager.removeSpecialEffectFromPlayer(entityManager.getEntities().get(p.entityId));
                }
                entityManager.removeEntity(p.entityId);

                return true;
            }

            if (packet.packetType == PLAYER_DIED)
            {
                final PlayerDiedPacket p = (PlayerDiedPacket) packet;
                IoLogger.log("PlayerDiedPacket was received");

                if (!localPlayer.getId().equals(p.playerId))
                {
                   final PlayerCell enemy = entityManager.getAllPlayers().get(p.playerId);
                    if (enemy != null)
                    {
                        enemy.die(particlesPoolManager);

                        // todo very hacky way to remove died player
                        Timer.schedule(new Timer.Task()
                        {
                            @Override
                            public void run()
                            {
                                entityManager.removePlayer(p.playerId);
                                entityManager.addGrave(enemy.createGrave());

                            }
                        }, 1.5f);

                    }
                    else
                    {
                        IoLogger.log("PlayerDiedPacket null player!");
                    }


                    return true;
                }
                if (localPlayer.getId().equals(p.playerId) && !localPlayerState.equals(PlayerState.DEAD))
                {
                    localPlayer.die(particlesPoolManager);
                    die();
                    return true;
                }
                if (clientSquad != null && squadInfo.hasInfo(p.playerId))
                {
                    squadInfo.setDead(p.playerId);
                    IoLogger.log("setDead()");
                }

                if (entityManager.getAllPlayers().containsKey(p.playerId))
                {
                    entityManager.getAllPlayers().get(p.playerId)
                            .die(particlesPoolManager); //visuals
                }
                entityManager.removePlayer(p.playerId);

                return true;
            }


            if (packet.packetType == ITEM_PICKED)
            {

                ItemPickedPacket p = (ItemPickedPacket) packet;
                IoLogger.log("ITEM_PICKED ", "p.itemId " + p.itemId);
                IoLogger.log("ITEM_PICKED ", "item " + entityManager.getItems().get(p.itemId));
                if (entityManager.getItems().get(p.itemId) != null)
                {
                    if (p.playerId.equals(localPlayer.getId()))
                    {
                        inventoryManager.pickItem(entityManager.getItems().get(p.itemId));
                    }
                    entityManager.removeItem(p.itemId);
                }
                else
                {
                    Gdx.app.error("handlePacket()", "We got null item");
                }

                return true;
            }

            if (packet.packetType == SHIELD_INTERACTION)
            {

                ShieldInteractionPacket p = (ShieldInteractionPacket) packet;

                PlayerCell player = entityManager.getAllPlayers().get(p.playerId);
                TacticalShield shield = (TacticalShield) entityManager.getEntities().get(p.shiledId);

                if (player != null && shield != null)
                {
                    if (p.equipped && localPlayer.shiled == null)
                    {
                        shield.startInteractionClient(player);
                    }
                    else
                    {
                        shield.finishInteractionClient(player);
                    }
                }

                return true;
            }

            if (packet.packetType == PIT_INTERACTION)
            {
                PitInteractionPacker p = (PitInteractionPacker) packet;

                PlayerCell player = entityManager.getEnemyCells().get(p.playerId);
                MapEntityCell pit = entityManager.getEntities().get(p.pitId);

                if (player != null && pit != null)
                {
                    if (p.insidePit)
                    {
                        pit.getPlayersId().add(p.playerId);
                        player.setVisible(false);
                        player.pit = p.pitId;
                        player.setInsidePit(true);
                        //IoLogger.log("Player entered pit");
                    }
                    else
                    {
                        pit.getPlayersId().remove(p.playerId);
                        player.setVisible(true);
                        player.pit = null;
                        player.setInsidePit(false);
                        //IoLogger.log("Player left pit");
                    }
                }

                return true;
            }
            if (packet.packetType == SERVER_ERROR)
            {
                ServerErrorPacket p = (ServerErrorPacket) packet;


                IoLogger.log("ServerErrorPacket ", p.error);

                return true;
            }
            if (packet.packetType == VISIBILITY_CHANGE)
            {
                VisibilityChangePacket p = (VisibilityChangePacket) packet;

                PlayerCell playerCell = entityManager.getEnemyCells().get(p.playerId);

                if (playerCell != null)
                {
                    if (p.type == MapEntityType.SPIDER_WEB || p.type == MapEntityType.SPIDER_WEB_BIG)
                    {
                        if (p.insideEntity)
                        {
                            playerCell.setUnderWeb(true);
                        }
                        else
                        {
                            playerCell.setUnderWeb(false);
                        }
                    }
                }

                return true;
            }
            if (packet.packetType == GAME_WIN)
            {
                WinPacket p = (WinPacket) packet;
                if (!p.winnerId.equals(localPlayer.getId()))
                {
                    IoLogger.log("handlePacket", "GAME_WIN packet wrong recepient");
                    return true;
                }

                win();
                return true;
            }
            if (packet.packetType == PacketType.USE_AMMO) // todo check shotgun reload and clip update
            {
                UseAmmoPacket p = (UseAmmoPacket) packet;
                if (p.mPlayerId.equals(localPlayer.getId()))
                {
                    IoLogger.log("handlePacket", "USE_AMMO");
                    AbstractRangeWeapon weapon = localPlayer.getInventoryManager().getWeapon(WeaponType.valueOf(p.mWeaponType));
                    int clip = weapon.clipAmmoAmount;
                    clip = Math.min((clip + p.mPatron), weapon.getClipMaxAmmoAmount());
                    weapon.setClipAmmoAmount(clip);
                    return true;
                }

            }

            if (packet.packetType == SERVER_STATE_INFO)
            {
                GameStateChangedPacket p = (GameStateChangedPacket) packet;
                serverState = p.gameServerState;
                messagesPanel.messageForGameState(p.gameServerState);
                GameClient.getGame().setGameType(p.gameType);
                return true;
            }

            return false;
        }
        catch (Exception e)
        {
            GameClient.getGame().getMasterServer().postExceptionDetails(e, "Gamescreen::handlePacket " + packet.toString());
            Gdx.app.error("handlePacket() GameScreen", packet.toString());
            Gdx.app.error("handlePacket() GameScreen", e.getMessage());
            e.printStackTrace();
            return false;
        }
    }

    GameServerState serverState;

    public OrthographicCamera getCamera()
    {
        return camera;
    }

    public ArrayList<IDrawable> getUI()
    {
        return UI;
    }


    public DebugPanel getDebugPanel()
    {
        return debugPanel;
    }

    public EntityManager getEntityManager()
    {
        return entityManager;
    }

    public RenderManager getRenderManager()
    {
        return renderManager;
    }
    private FadeScreen fadeScreen;

    public FadeScreen getFadeScreen()
    {
        return  fadeScreen;
    }



    @Override
    public boolean keyDown(int keycode)
    {
        return false;
    }
    @Override
    public boolean keyUp(int keycode)
    {
       return false;
    }
    private void staminaCurrentForSprint(float deltaX, float deltaY)
    {
        staminaMax = localPlayer.getMaxStamina();
        staminaCurrent = staminaMax;

            if (isSprint(deltaX, deltaY)) {
                if (timeUpdate + GameConstants.TIMER_RATE < System.currentTimeMillis() && timerGame >= 0 && staminaCurrent > 0) {
                   timerGame -= 0.6f;
                    staminaCurrent  =timerGame;
                    localPlayer.setCurrentStamina(staminaCurrent);
                }
            } else {
                if (timeUpdate + GameConstants.TIMER_RATE < System.currentTimeMillis() && timerGame <= staminaMax && staminaCurrent <= staminaMax) {
                    timerGame += 0.05f;
                    staminaCurrent = timerGame;
                    localPlayer.setCurrentStamina(staminaCurrent);
                }
            }
     }

    @Override
    public boolean keyTyped(char character)
    {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button)
    {

		/*if (localPlayer.getCurrentWeapon() != null && localPlayer.getCurrentWeapon().canShoot()) {

			Vector3 touchPos = new Vector3();
			touchPos.set(Gdx.input.getX(), Gdx.input.getY(), 0);
			camera.unproject(touchPos);

			if(touchPos.dst(localPlayer.getPositionVec()) > localPlayer.radius * 2)
			{
				Vector2 forward = new Vector2(touchPos.x - localPlayer.getPosition().getX(), touchPos.y - localPlayer.getPosition().getY());
				forward.setLength(localPlayer.getRadius() * 2);
				AbstractProjectile p = localPlayer.getCurrentWeapon().fire(localPlayer.getFireCoords().add(forward), new IoPoint(touchPos.x, touchPos.y), localPlayer.getId());

			}}*/
        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button)
    {
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer)
    {
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY)
    {
        return false;
    }

    @Override
    public boolean scrolled(int amount)
    {
        if (amount == 1)
        {
            nextChangeWeapon();
        }
        else
        {
            backChangeWeapon();
        }
        return false;
    }

    public GameClient getGameClient()
    {
        return game;
    }

    private boolean stopPressedShift()
    {
        if (Gdx.input.isKeyPressed(Input.Keys.SHIFT_LEFT))
        {
            return false;
        }
        return false;
    }

    public void drawDashedLine(Color color, ShapeRenderer renderer, int dashLenth, int dashSpaceBetween, Vector3 start, Vector3 end, float width)
    {
        if (dashLenth == 0)
        {
            return;
        }
        float dirX = end.x - start.x;
        float dirY = end.y - start.y;

        float length = Vector2.len(dirX, dirY);
        dirX /= length;
        dirY /= length;

        float curLen = 0;
        float curX = 0;
        float curY = 0;

        renderer.begin(ShapeType.Filled);
        renderer.setColor(color);

        while (curLen <= length)
        {
            curX = (start.x + dirX * curLen);
            curY = (start.y + dirY * curLen);
            renderer.rectLine(curX, curY, curX + dirX * dashLenth, curY + dirY * dashLenth, width);
            curLen += (dashLenth + dashSpaceBetween);

        }
        renderer.end();
    }



}