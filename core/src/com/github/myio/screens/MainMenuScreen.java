package com.github.myio.screens;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.SelectBox;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.JsonReader;
import com.badlogic.gdx.utils.JsonValue;
import com.badlogic.gdx.utils.Timer;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.github.myio.GameClient;
import com.github.myio.dto.masterserver.HttpServerBaseDto;
import com.github.myio.dto.masterserver.Region;
import com.github.myio.entities.PlayerCell;
import com.github.myio.master.MasterServer;
import com.github.myio.networking.ClientPacketHandler;
import com.github.myio.networking.Packet;
import com.github.myio.networking.PacketType;
import com.github.myio.networking.packet.GameStateChangedPacket;
import com.github.myio.networking.packet.LoginPacket;
import com.github.myio.networking.packet.LoginResponsePacket;
import com.github.myio.tools.IoLogger;
import com.github.myio.tools.SoundManager;
import com.github.myio.ui.EmoteManager;
import com.github.myio.ui.Settings;
import com.github.myio.ui.SkinManager;

import java.util.ArrayList;
import java.util.List;

import static com.badlogic.gdx.net.HttpStatus.SC_OK;
import static com.github.myio.GameClient.getGame;
import static com.github.myio.GameConstants.LOCAL_SERVER_PORT;
import static com.github.myio.GameConstants.LOCAL_SERVER_URL;
import static com.github.myio.GameConstants.PLAYERS_MAX_HEALTH;
import static com.github.myio.GameConstants.PLAYERS_MAX_STAMINA;
import static com.github.myio.GameConstants.PRODUCTION_MODE;
import static com.github.myio.GameConstants.PRODUCTION_SERVER_PORT;
import static com.github.myio.GameConstants.PRODUCTION_SERVER_URL_SOLO;
import static com.github.myio.GameConstants.PRODUCTION_SERVER_URL_SOLO2;
import static com.github.myio.GameConstants.PRODUCTION_SERVER_URL_SOLO3;
import static com.github.myio.GameConstants.PRODUCTION_SERVER_URL_SOLO4;
import static com.github.myio.StringConstants.PREFERENCES_MODE;
import static com.github.myio.StringConstants.PREFERENCES_REGION;
import static com.github.myio.StringConstants.PREFERENCES_USERNAME;
import static com.github.myio.master.MasterServer.DEFAULT_TIMEOUT;
import static com.github.myio.networking.PacketType.SERVER_STATE_INFO;
import static com.github.myio.tools.SoundManager.MAIN_MENU_OFFICIALLY;

public class MainMenuScreen implements Screen, ClientPacketHandler
{

    final String TAG = "MainMenuScreen";
    //OrthographicCamera camera;
    private GameClient game;
    private boolean loginReqSent;

    private Stage stage;
    private Skin skin;
    private Skin newSkin;
    private Table menuTable;
    private Table teamTable;
    private Table tableInfoBoard;
    private TextButton btnPlay;
    private TextButton btnCreateTeam;
    private TextButton btnJoinTeam;
    private TextButton btnHowtoplay;
    private TextButton btnSettings;
    private TextButton btnEmoteManager;
    private TextButton btnSkinManager;
    private TextButton btnSound;
    private Texture myTexture;
    private TextureRegion myTexRegion;
    private TextureRegionDrawable myTextureRegionDrawable;
    private ImageButton button;


    private Label label;
    private Label name;
    private Label connectionStatus;
    private Label matchMakingStatus;
    private Label errorsLabel;
    private Label infoBoard;
    private TextField userNameInput;
    private TextField teamIdInput;
    private BitmapFont font32White;
    private BitmapFont font32Black;
    private Label.LabelStyle labelStyle;
    private EmoteManager emoteManager;
    private SkinManager skinManager;
    private SelectBox<String> regionDropdown;
    private SelectBox<String> gameModeDropdown;
    private Settings settings;
    private Table chengeSkin;

    private List<HttpServerBaseDto> serversInfo = new ArrayList<HttpServerBaseDto>();
    private JsonReader jsonReader = new JsonReader();
    private Json json = new Json();
    Timer.Task firstConnectionTask;
    Timer.Task updateserversInfoTask;

    Viewport viewport;
    boolean isAndroid;

    private boolean connectingToServer;

    public static Music musicMainMenu;
    long startTime ;


    public MainMenuScreen(final GameClient game)
    {
        this.game = game;

        try
        {
            font32White = GameClient.getFont32White();
            font32Black = GameClient.getFont32Black();
            viewport = game.getUiViewport();

            stage = new Stage(viewport);
            Gdx.input.setInputProcessor(stage);
            skin = new Skin(Gdx.files.internal("atlas/mainMenu/newSkin.json"), new TextureAtlas("atlas/mainMenu/newSkin.atlas"));
            skin.getFont("trebushet").getData().setScale(1.1f);
            if (Gdx.app.getType().equals(Application.ApplicationType.Android)) {
                skin.getFont("trebushet").getData().setScale(1.6f);
                isAndroid = true;
            }else {
                isAndroid = false;
            }
            emoteManager = new EmoteManager(game, false , viewport);
            skinManager = new SkinManager(skin, game);
            settings = new Settings(skin, game);


            emoteManager.setStage(stage);
            skinManager.setmStage(stage);
            settings.setStage(stage);


            final String username = game.getGamePreferences().getString(PREFERENCES_USERNAME);
            userNameInput = new TextField(username, skin);
            userNameInput.setMessageText("Enter name");
            userNameInput.setMaxLength(14);
            teamIdInput = new TextField("", skin);
            teamIdInput.setTextFieldFilter(new TextField.TextFieldFilter.DigitsOnlyFilter());
            teamIdInput.setMessageText("Enter team id");

            btnPlay = new TextButton("PLAY", skin);

            btnCreateTeam = new TextButton("CREATE TEAM", skin);
            btnJoinTeam = new TextButton("JOIN TEAM", skin);
            btnHowtoplay = new TextButton("HOW TO PLAY", skin);
            btnSettings = new TextButton("SETTINGS", skin);
            btnEmoteManager = new TextButton("EMOTE MANAGER", skin);
            btnSkinManager = new TextButton("CHANGE SKIN", skin);
            btnSound = new TextButton("TURN OFF SOUND", skin);


            btnSound.addListener(new ClickListener()
            {
                @Override
                public void clicked(InputEvent event, float x, float y)
                {
                    if (!SoundManager.mute)
                    {
                        SoundManager.mute = true;
                        btnSound.setText("TURN ON SOUND");
                        dispose();
                    }
                    else
                    {
                        SoundManager.mute = false;
                        btnSound.setText("TURN OFF SOUND");
                    }
                }
            });

            btnSkinManager.addListener(new ClickListener()
            {
                @Override
                public void clicked(InputEvent event, float x, float y)
                {
                    skinManager.showWindow();
                }
            });
            btnPlay.addListener(new ClickListener()
            {
                @Override
                public void clicked(InputEvent event, float x, float y)
                {
                    Gdx.input.setOnscreenKeyboardVisible(false);

                    if (!connectingToServer)
                    {


                        if (userNameInput.getText().isEmpty())
                        {
                            userNameInput.setColor(Color.RED);
                            stage.setKeyboardFocus(userNameInput);
                            return;
                        }

                            loginToGameServer();
                            btnPlay.setText("WAITING...");
                            btnPlay.setTouchable(Touchable.disabled);
                            connectingToServer = true;

                            Timer.schedule(new Timer.Task()
                        {
                            @Override
                            public void run()
                            {
                                btnPlay.setTouchable(Touchable.enabled);
//                                connectionToServersWithSquadsAndGameTypes();
                                btnPlay.setText("PLAY");
                                connectingToServer = false;
                            }
                        },3);
                    }

                    else
                    {
                        btnPlay.setTouchable(Touchable.enabled);
                        connectionToServersWithSquadsAndGameTypes();
                        btnPlay.setText("PLAY");
                        connectingToServer = false;
                    }


                }
            });

//            btnCreateTeam.addListener(new ClickListener()
//            {
//                @Override
//                public void clicked(InputEvent event, float x, float y)
//                {
//                    dispose();
//                    game.setScreen(new TeamMatchmackingScreen(game, emoteManager, userNameInput.getText(), ""));
//                }
//            });


//            btnJoinTeam.addListener(new ClickListener()
//            {
//                @Override
//                public void clicked(InputEvent event, float x, float y)
//                {
//
//					/*long teamId ;
//					try {
//						teamId = Long.parseLong(teamIdInput.getText());
//					} catch (NumberFormatException e) {
//						e.printStackTrace();
//						return;
//					}*/
//
//                    dispose();
//
//                    game.setScreen(new TeamMatchmackingScreen(game, emoteManager, userNameInput.getText(), teamIdInput.getText()));
//                }
//            });
            btnEmoteManager.addListener(new ClickListener()
            {
                public void clicked(InputEvent event, float x, float y)
                {
                    IoLogger.log(TAG, "btnEmoteManager click");
                    emoteManager.showEM();
                }
            });

            btnSettings.addListener(new ClickListener()
            {
                public void clicked(InputEvent event, float x, float y)
                {
                    settings.showSettings();
                }
            });

            regionDropdown = new SelectBox<String>(skin);
            final Array<String> serverSelect = new Array<String>();
			/*for (Region r : Region.values())
			{
				serverSelect.add(r.name());
			}*/
            serverSelect.add(Region.EUROPE.name());
            regionDropdown.setItems(serverSelect);

            if (!getGame().getGamePreferences().getString(PREFERENCES_REGION, "").equals(""))
            {
                regionDropdown.setSelected(getGame().getGamePreferences().getString(PREFERENCES_REGION));
                GameClient.getGame().setClientRegion(getGame().getGamePreferences().getString(PREFERENCES_REGION));
            }

            gameModeDropdown = new SelectBox<String>(skin);
            final Array<String> tmp = new Array<String>();
//            for (GameType r : GameType.values())
//            {
//                tmp.add(r.name());
//            }
            tmp.add("EUROPE1");
            tmp.add("EUROPE2");
            tmp.add("EUROPE3");
            tmp.add("EUROPE4");
            gameModeDropdown.setItems(tmp);

            if (!getGame().getGamePreferences().getString(PREFERENCES_MODE, "").equals(""))
            {
                gameModeDropdown.setSelected(getGame().getGamePreferences().getString(PREFERENCES_MODE));
                GameClient.getGame().setClientRegion(getGame().getGamePreferences().getString(PREFERENCES_MODE));
            }

            IoLogger.log(TAG, "Buttons created");
            connectionStatus = new Label("", skin);
            matchMakingStatus = new Label("", skin);
            errorsLabel = new Label("", skin);

            Table rootTable = new Table(skin);
            rootTable.setFillParent(true);
            rootTable.top();


            Sprite backgroundRootTable = GameClient.mainMenu.createSprite("mainMenu");
            Image imageRootTable = new Image(backgroundRootTable);
            Sprite backgroundMiddlTable = new Sprite(new Texture(Gdx.files.internal("mainMenu/background.png")));
            Image imageMidllTable = new Image(backgroundMiddlTable);


            Table topTable = new Table();
            Table middleTable = new Table();

            middleTable.setPosition(viewport.getScreenWidth()/6, viewport.getScreenHeight()/6);

            float height = 1;
            float width = 1;

            rootTable.setBackground(imageRootTable.getDrawable());
            middleTable.setBackground(imageMidllTable.getDrawable());
            if (Gdx.app.getType().equals(Application.ApplicationType.Android)) {
                height = 2;
                width = 2;
                isAndroid = true;
            }else {
                isAndroid = false;
            }
            middleTable.add(btnPlay).height(Gdx.graphics.getHeight()/14*height).width(Gdx.graphics.getWidth()/3*width).pad(7,10,10,10).colspan(2);
            middleTable.row();
//            button from change gameModeDropdown  regionDropdown
//            middleTable.add(gameModeDropdown).height(Gdx.graphics.getHeight()/10*height).width(Gdx.graphics.getWidth()/7*width).padBottom(10).padLeft(10).padRight(10);
            middleTable.add(userNameInput).height(Gdx.graphics.getHeight()/14*height).width(Gdx.graphics.getWidth()/6*width).pad(0,7,10,0);
            middleTable.add(gameModeDropdown).height(Gdx.graphics.getHeight()/14*height).width(Gdx.graphics.getWidth()/6*width).pad(0,7,10,10);
            middleTable.row();
            middleTable.add(btnSkinManager).height(Gdx.graphics.getHeight()/14*height).width(Gdx.graphics.getWidth()/6*width).pad(0,7,10,0);
            middleTable.add(btnEmoteManager).height(Gdx.graphics.getHeight()/14*height).width(Gdx.graphics.getWidth()/6*width).pad(0,7,10,10);
            middleTable.row();
            middleTable.add(btnHowtoplay).height(Gdx.graphics.getHeight()/14*height).width(Gdx.graphics.getWidth()/6*width).pad(0,7,10,0);
            middleTable.add(btnSettings).height(Gdx.graphics.getHeight()/14*height).width(Gdx.graphics.getWidth()/6*width).pad(0,7,10,10);

            rootTable.add(topTable).fillX().height(viewport.getScreenHeight()/5);
            rootTable.row();
            //height(middleTable.getHeight());
           // rootTable.add(middleTable).top().padTop(40).height(viewport.getScreenHeight()/3);//height(middleTable.getHeight());
            if (Gdx.app.getType().equals(Application.ApplicationType.Android)) {
                rootTable.setSize(viewport.getWorldWidth()/2,viewport.getWorldHeight()/2);
                rootTable.add(middleTable).top().padTop(40);
            }
            else{
                rootTable.add(middleTable).top().padTop(67).height((viewport.getScreenHeight()/3));

            }
            rootTable.row();


          //  rootTable.debugAll();



            emoteManager.setMenu(rootTable);
            skinManager.setMenu(rootTable);
            settings.setMenu(rootTable);

            tableInfoBoard = new Table();
            TextButton closeInfoTable = new TextButton("CLOSE", skin);

            tableInfoBoard.setPosition(Gdx.graphics.getWidth() / 3, Gdx.graphics.getHeight() / 3);
            tableInfoBoard.setSize(Gdx.graphics.getWidth() / 3, Gdx.graphics.getHeight() / 2);

            String reallyLongString = "MOVEMENT:[W][A][S][D]\n" +
                    "ATTACK: [LEFT CLICK]\n" +
                    "CYCLE WEAPONS:[Q][E]\n" +
                    "EMOJI: [RIGHT CLICK]\n" +
                    "EXIT: [ESCAPE]\n" +
                    "PICKUP ITEMS:[F]\n" +
                    "MARKER: [M]\n" +
                    "RECHARGE: [R]\n";

            infoBoard = new Label("HOW TO PLAY", skin);
            Label label1 = new Label(reallyLongString, skin);

            infoBoard.setColor(Color.BLACK);
            tableInfoBoard.setBackground(skin.getDrawable("default-window"));
            tableInfoBoard.add(label1).center();
            tableInfoBoard.row();

            tableInfoBoard.add(closeInfoTable).width(100);


            btnHowtoplay.addListener(new ClickListener()
            {
                @Override
                public void clicked(InputEvent event, float x, float y)
                {
                    stage.addActor(tableInfoBoard);
                }
            });
            closeInfoTable.addListener(new ClickListener()
            {
                @Override
                public void clicked(InputEvent event, float x, float y)
                {
                    tableInfoBoard.remove();
                }
            });

            //==========================================================================

            regionDropdown.addListener(new ChangeListener()
            {
                @Override
                public void changed(ChangeEvent event, Actor actor)
                {
                    IoLogger.log("MainMenu select region: ", regionDropdown.getSelected());
                    getGame().getGamePreferences().putString(PREFERENCES_REGION, regionDropdown.getSelected());
                    getGame().getGamePreferences().flush();

                    connectionToServersWithSquadsAndGameTypes();
                    btnPlay.setTouchable(Touchable.enabled);
                    btnPlay.setText("PLAY");
                }
            });

            gameModeDropdown.addListener(new ChangeListener()
            {
                @Override
                public void changed(ChangeEvent event, Actor actor)
                {
                    IoLogger.log("MainMenu select gamemode: ", gameModeDropdown.getSelected());
                    getGame().getGamePreferences().putString(PREFERENCES_MODE, gameModeDropdown.getSelected());
                    getGame().getGamePreferences().flush();

                    connectionToServersWithSquadsAndGameTypes();
                    btnPlay.setTouchable(Touchable.enabled);
                    btnPlay.setText("PLAY");
                }
            });

            userNameInput.setTextFieldListener(new TextField.TextFieldListener()
            {
                @Override
                public void keyTyped(TextField textField, char c)
                {
                    if (!userNameInput.getText().isEmpty())
                    {
                        userNameInput.setColor(Color.WHITE);
                    }
                }
            });

            stage.setKeyboardFocus(userNameInput);
            stage.addActor(rootTable);

            //scheduleConnectionToSelectedRegion();

            firstConnectionTask = new Timer.Task()
            {
                @Override
                public void run()
                {
                    //updateGameServersData();
                    connectionToServersWithSquadsAndGameTypes();
                }
            };

            updateserversInfoTask = new Timer.Task()
            {
                @Override
                public void run()
                {
                    //updateGameServersData();
                }
            };


            Timer.schedule(firstConnectionTask, 0.1f);
            Timer.schedule(updateserversInfoTask, 0.1f, 5f);


            game.getNetworkManager().addPacketHandler(this);
        }
        catch (Exception e)
        {
            //game.getNetworkManager().sendPacket(new ExceptionPacket("MainMenuScreen constructor",e.getMessage()));

            getGame().getMasterServer().postExceptionDetails(e, "MainMenuScreen constructor");
            IoLogger.log("constructor", e.getMessage());
            e.printStackTrace();
        }
        IoLogger.log("SHOW_BANNER_HERE");


    }

    private void scheduleConnectionToSelectedRegion()
    {
        Timer.schedule(new Timer.Task()
        {
            @Override
            public void run()
            {
                findServerToConnect(regionDropdown.getSelected());
            }
        }, 0.1f);
    }

    private void updateGameServersData()
    {
        final MasterServer.RequestResult res = GameClient.getGame().getMasterServer().getAllGameServersInfo();
        Timer.schedule(new Timer.Task()
        {
            @Override
            public void run()
            {
                if (res.isFailed() || (res.successfullresponse != null && res.successfullresponse.getStatus().equals(SC_OK)))
                {
                    errorsLabel.setText("Master server request failed");
                    errorsLabel.setColor(Color.RED);
                }

                String serverInfo = res.response;


                try
                {
                    serversInfo.clear();
                    ArrayList<JsonValue> list = json.fromJson(ArrayList.class, serverInfo);
                    for (JsonValue v : list)
                    {
                        serversInfo.add(json.readValue(HttpServerBaseDto.class, v));
                    }

                    matchMakingStatus.setText("Game servers info updated");
                    matchMakingStatus.setColor(Color.GREEN);
                    IoLogger.log("updateGameServersData: ", serversInfo.toString());


                }

                catch (Exception e)
                {
					/*errorsLabel.setText("failed to receive Game servers info");
					errorsLabel.setColor(Color.RED);*/
                   // e.printStackTrace();
                }
            }
        }, DEFAULT_TIMEOUT);
    }


    private void findServerToConnect(final String region)
    {
        matchMakingStatus.setText("Finding servers in " + region);
        matchMakingStatus.setColor(Color.RED);
        getGame().getNetworkManager().disconnect();
        Gdx.input.setInputProcessor(null);


        getGame().getNetworkManager().addPacketHandler(this);

        if (region.equals(Region.EUROPE.name())) //todo testing only
        {
            ConnectionToServer(3);
            return;
        }


        final MasterServer.RequestResult res = GameClient.getGame().getMasterServer().getGameServerInRegion(region);
        Timer.schedule(new Timer.Task()
        {
            @Override
            public void run()
            {
                if (res.isFailed() || (res.successfullresponse != null && res.successfullresponse.getStatus().equals(SC_OK)))
                {
                    errorsLabel.setText("Master server request failed");
                    errorsLabel.setColor(Color.RED);
                }

                String serverInfo = res.response;


                IoLogger.log("getGameServerInRegion: ", serverInfo);
                connectionToServerNew(3, serverInfo);
            }
        }, DEFAULT_TIMEOUT);
    }


    private void loginToGameServer()
    {
        if (userNameInput.getText().isEmpty())
        {
            IoLogger.log("Login", "Login failed");
        }
        else
        {
            loginReqSent = true;
            game.getGamePreferences().putString(PREFERENCES_USERNAME, userNameInput.getText());
            game.getGamePreferences().flush();

            game.getNetworkManager().sendPacket(new LoginPacket(userNameInput.getText(), skinManager.getStringNameSkin(), game.getClientId()));
        }
    }

    public void connectionToServersWithSquadsAndGameTypes()
    {
        getGame().getNetworkManager().disconnect();
        getGame().getNetworkManager().addPacketHandler(this);

        if (regionDropdown.getSelected().equals(Region.EUROPE.name())) //todo testing only
        {
            ConnectionToServer(2);
            return;
        }


        boolean found = false;


        for (HttpServerBaseDto s : serversInfo)
        {
            if (s.getRegion().name().equals(regionDropdown.getSelected()) &&
                    s.getGameType().name().equals(gameModeDropdown.getSelected()))
            {
                found = true;

                try
                {

                    game.getNetworkManager().connect(s.getUrl().replaceFirst("http://", "").replaceFirst("https://", ""), PRODUCTION_SERVER_PORT);


                    getGame().getNetworkManager().addPacketHandler(this);

                    connectionStatus.setText("Connected!");
                    connectionStatus.setColor(Color.GREEN);
                    matchMakingStatus.setText("Server found : " + s.getUrl());
                    matchMakingStatus.setColor(Color.RED);
                    break;
                }
                catch (Exception e)
                {

                    e.printStackTrace();

                    //todo for testing only
                    connectionStatus.setText("");
                    errorsLabel.setText("Connection problem : " + e.getMessage());
                    errorsLabel.setColor(Color.RED);
                }
            }
        }

        if (!found)
        {
            errorsLabel.setText("Cannot find non-full " + gameModeDropdown.getSelected() + " server in " + regionDropdown.getSelected());
            errorsLabel.setColor(Color.RED);
            connectionStatus.setText("");
        }
    }


    @Deprecated
    public void connectionToServerNew(int retryCount, String serverInfo)
    {
        while (retryCount > 0)
        {
            try
            {
                //{"id":9,"region":"EUROPE","playersNumber":5,"active":true,"lastHeartbeat":1522417545015,"url":"localhost:8080"}
                JsonValue base = jsonReader.parse(serverInfo);
                String url = base.getString("url");
                game.getNetworkManager().connect(url.replaceFirst("http://", "").replaceFirst("https://", ""), PRODUCTION_SERVER_PORT);

                getGame().getNetworkManager().addPacketHandler(this);
                matchMakingStatus.setText("Server found : " + url);
                matchMakingStatus.setColor(Color.GREEN);
                break;
            }
            catch (Exception e)
            {
                IoLogger.log("Retry to connect", e.getMessage());
                e.printStackTrace();


                retryCount--;
                if (retryCount<=0)
                {
                    btnPlay.setText("PLAY");
                    btnPlay.setTouchable(Touchable.enabled);
                }
                System.out.println(retryCount+" retryCount");
                connectionStatus.setText("");
                errorsLabel.setText("Connection problem : " + e.getMessage());
                errorsLabel.setColor(Color.RED);
            }
        }

    }

    @Deprecated
    public void ConnectionToServer(int retryCount)
    {

        String url = null;
        while (retryCount > 0)
        {

            try
            {
                if (gameModeDropdown.getSelected().equals("EUROPE1"))
                {
                    url = PRODUCTION_SERVER_URL_SOLO;
                }
                if (gameModeDropdown.getSelected().equals("EUROPE2"))
                {
                    url = PRODUCTION_SERVER_URL_SOLO2;
                }
                if (gameModeDropdown.getSelected().equals("EUROPE3"))
                {
                    url = PRODUCTION_SERVER_URL_SOLO3;
                }
                if (gameModeDropdown.getSelected().equals("EUROPE4"))
                {
                    url = PRODUCTION_SERVER_URL_SOLO4;
                }

                if (PRODUCTION_MODE)
                {
                    game.getNetworkManager().connect(url, 80); //uncomment it for prod
                    getGame().getNetworkManager().addPacketHandler(this);
                    connectionStatus.setText("Connected!");
                    //btnPlay.setColor(Color.GREEN);
                    connectionStatus.setColor(Color.GREEN);
                    break;
                }
                else
                {
                    game.getNetworkManager().connect(LOCAL_SERVER_URL, LOCAL_SERVER_PORT);
                    getGame().getNetworkManager().addPacketHandler(this);
                    connectionStatus.setText("Connected!");
                    //btnPlay.setColor(Color.GREEN);

                    connectionStatus.setColor(Color.GREEN);
                    break;
                }
            }
            catch (Exception e)
            {
                connectionStatus.setText("");
                //btnPlay.setColor(Color.BROWN);


                retryCount--;

                if (retryCount<=0)
                {
                    btnPlay.setText("PLAY");
                    btnPlay.setTouchable(Touchable.enabled);
                }
                System.out.println(retryCount+" retryCount");
                IoLogger.log("Retry to connect", e.getMessage());
            }
        }

    }


    @Override
    public void render(float delta)
    {
        try
        {
            Gdx.gl.glClearColor(0.68f, 0.62f, 0.57f, 1);
            Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

            SpriteBatch batch = game.getBatch();
            batch.begin();
            stage.act();
            stage.draw();
            emoteManager.draw(delta);
            skinManager.draw(delta);
            inputHandle();
            batch.end();


        }
        catch (Exception e)
        {
            //game.getNetworkManager().sendPacket(new ExceptionPacket("MainMenuScreen render",e.getMessage()));
            IoLogger.log("render", e.getMessage());

        }


//		if (Gdx.input.isTouched() && !loginReqSent)
//		{
//
//			//PlayerCell myPlayer =  new PlayerCell(game.getBatch(), game.getShapeRenderer(), game.getFont(), "", 0,0, 100,  "1", 5);
//			//game.setScreen(new GameScreen(game, myPlayer));
//
//			loginReqSent = true;
//			game.getNetworkManager().sendPacket(new LoginPacket("Igor", "pass"));
//		}
    }

    public void inputHandle()
    {
        if (Gdx.input.isKeyJustPressed(Input.Keys.ENTER))
        {
            loginToGameServer();
        }


        if (Gdx.input.isButtonPressed(Input.Buttons.LEFT))
        {
            Vector3 touchPos = new Vector3();
            touchPos.set(Gdx.input.getX(), Gdx.input.getY(), 0);
            //System.out.println(Gdx.input.getX()+" | "+ Gdx.input.getY());

        }


    }


    @Override
    public void show()
    {
        Gdx.input.setInputProcessor(stage);
        btnPlay.setTouchable(Touchable.enabled);
        btnPlay.setText("PLAY");
        firstConnectionTask.run();
        updateserversInfoTask.run();
        musicMainMenu = Gdx.audio.newMusic(Gdx.files.internal("sounds/main_menu_officially.mp3"));
        musicMainMenu.play();



    }

    @Override
    public void resize(int width, int height)
    {
        stage.getViewport().update(width, height, true);
        viewport.update(width,height);
    }

    @Override
    public void pause()
    {

    }

    @Override
    public void resume()
    {
        Gdx.input.setInputProcessor(stage);
        btnPlay.setTouchable(Touchable.enabled);
        btnPlay.setText("PLAY");
        firstConnectionTask.run();
        updateserversInfoTask.run();
    }

    @Override
    public void hide()
    {

    }


    @Override
    public void dispose()
    {
        GameClient.getGame().getSoundManager().stopSound(MAIN_MENU_OFFICIALLY);
        try
        {
            game.getNetworkManager().removePacketHandler(this);

            firstConnectionTask.cancel();
            updateserversInfoTask.cancel();
        }
        catch (Exception e)
        {
            //game.getNetworkManager().sendPacket(new ExceptionPacket("MainMenuScreen dispose",e.getMessage()));
            IoLogger.log("render", e.getMessage());
            e.printStackTrace();
        }
        musicMainMenu.dispose();
    }

    @Override
    public boolean handlePacket(Packet packet)
    {

        try
        {
            if (packet.packetType == PacketType.LOGIN_RESPONSE)
            {
                IoLogger.log("", "handlePacket LOGIN_RESPONSE  MMS");
                LoginResponsePacket p = (LoginResponsePacket) packet;

                PlayerCell myPlayer = new PlayerCell(game.getBatch(), game.getGameShapeRenderer(), GameClient.getFont32Black(), p.name, p.x, p.y, 100,
                        p.playerId, PLAYERS_MAX_HEALTH, PLAYERS_MAX_HEALTH, PLAYERS_MAX_STAMINA, PLAYERS_MAX_STAMINA, p.currentXp, p.playerSkinCollor);
                myPlayer.setEmoteNames(emoteManager.getEmoteList());

                game.setScreen(new GameScreen(game, myPlayer));
                dispose();
                return true;
            }

            if (packet.packetType == SERVER_STATE_INFO)
            {
                GameStateChangedPacket p = (GameStateChangedPacket) packet;
                GameClient.getGame().setGameType(p.gameType);
                return true;
            }
        }
        catch (Exception e)
        {
            //game.getNetworkManager().sendPacket(new ExceptionPacket("MainMenuScreen handlePacket",e.getMessage()));
            IoLogger.log("MainMenuScreen handlePacket", e.getMessage());
            e.printStackTrace();
        }

        return false;
    }


    public Label getConnectionStatus()
    {
        return connectionStatus;
    }
}