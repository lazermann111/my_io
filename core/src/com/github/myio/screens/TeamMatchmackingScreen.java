package com.github.myio.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.CheckBox;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.SelectBox;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.JsonReader;
import com.badlogic.gdx.utils.Timer;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.github.myio.GameClient;
import com.github.myio.dto.masterserver.GameType;
import com.github.myio.dto.masterserver.Region;
import com.github.myio.dto.masterserver.TeamDto;
import com.github.myio.entities.PlayerCell;
import com.github.myio.master.MasterServer;
import com.github.myio.networking.ClientPacketHandler;
import com.github.myio.networking.Packet;
import com.github.myio.networking.PacketType;
import com.github.myio.networking.packet.GameStateChangedPacket;
import com.github.myio.networking.packet.LoginResponsePacket;
import com.github.myio.networking.packet.TeamLoginPacket;
import com.github.myio.tools.IoLogger;
import com.github.myio.ui.EmoteManager;
import com.github.myio.ui.SkinManager;

import static com.badlogic.gdx.net.HttpStatus.SC_OK;
import static com.github.myio.GameClient.getGame;
import static com.github.myio.GameConstants.LOCAL_SERVER_PORT;
import static com.github.myio.GameConstants.LOCAL_SERVER_URL;
import static com.github.myio.GameConstants.PLAYERS_MAX_HEALTH;
import static com.github.myio.GameConstants.PLAYERS_MAX_STAMINA;
import static com.github.myio.GameConstants.PRODUCTION_MODE;
import static com.github.myio.GameConstants.PRODUCTION_SERVER_PORT;
import static com.github.myio.StringConstants.PREFERENCES_MODE;
import static com.github.myio.StringConstants.PREFERENCES_REGION;
import static com.github.myio.master.MasterServer.DEFAULT_TIMEOUT;
import static com.github.myio.networking.PacketType.SERVER_STATE_INFO;

public class TeamMatchmackingScreen implements Screen, ClientPacketHandler {

    final String TAG = "TeamMatchmackingScreen";
    private GameClient game;
    private boolean loginReqSent;

    private Stage stage;
    private Skin skin;
    private Table table;
    private Table rootTable;
    private TextButton btnPlay;
    private TextButton btnLeave;



    private Label name;
    private Label connectionStatus;
    private Label infoBoard;
    private SelectBox<String> regionDropdown;
    private SelectBox<String> gameModeDropdown;
    private CheckBox autofillChck;




    private TeamDto teamInfo = new TeamDto();
    private String playerName;
    private JsonReader jsonReader = new JsonReader();
    private	Json json = new Json();
    Timer.Task updateTeamInfoTask;
    Timer.Task createTeamTask;

    private Label teamIdLabel;
    private Label member1Name;
    private Label member2Name;
    private Label member3Name;
    private Label member4Name;
    private Label[] memberNamesLabels;

   // Timer.Task updateserversInfoTask;
    SelectBox<String> regionList;
    float width;
    float height;
    private String teamId;
    private boolean isTeamLeader;
    private EmoteManager emoteManager;
    private SkinManager skinManager;


    public TeamMatchmackingScreen(final GameClient game,final EmoteManager em, final String p, final String teamId) {
        this.game = game;
        this.playerName = p == null || p.isEmpty() ? "Player" : p;
        this.teamId = teamId;
        emoteManager = em;

        if(teamId.isEmpty()) // team need to be created by leader
        {
            isTeamLeader = true;
            createTeamTask = new Timer.Task() {
                @Override
                public void run() {
                    createTeam();
                }
            };

            Timer.schedule(createTeamTask, 1f,1f);
        }
        else
        {
            joinTeam(teamId, playerName);
        }

        try {

            Viewport viewport = game.getUiViewport();
            stage = new Stage(viewport);
            skin = new Skin(Gdx.files.internal("atlas/default/skin/uiskin.json"),new TextureAtlas("atlas/default/skin/uiskin.atlas"));


            btnPlay = new TextButton("PLAY",skin); //visible only to team leader
            btnLeave = new TextButton("LEAVE TEAM",skin);

            btnPlay.addListener(new ClickListener(){
                @Override
                public void clicked(InputEvent event, float x, float y) {

                  if(isTeamLeader)
                  {
                      if(gameModeDropdown.getSelected().equals(GameType.DUO.name()) && teamInfo.getTeamMemberNames() != null && teamInfo.getTeamMemberNames().length >2)
                      {
                          connectionStatus.setText("cannot start DUO game with more than 2 team members");
                          return;
                      }
                      startMatch();
                  }
                  else {
                      connectionStatus.setText("You are not team leader, so you cant start match");
                  }
                }
            });

            btnLeave.addListener(new ClickListener(){
                @Override
                public void clicked(InputEvent event, float x, float y) {
                    leaveTeam();
                    dispose();
                    game.setScreen(new MainMenuScreen(game));
                }
            });


            regionDropdown = new SelectBox<String>(skin);
            final Array<String> serverSelect = new Array<String>();
            for (Region r : Region.values())
            {
                serverSelect.add(r.name());
            }
            regionDropdown.setItems(serverSelect);



            gameModeDropdown = new SelectBox<String>(skin);
            final Array<String> tmp = new Array<String>();
            tmp.add(GameType.DUO.name());
            tmp.add(GameType.SQUAD.name());
            gameModeDropdown.setItems(tmp);


            autofillChck = new CheckBox("AUTOFILL", skin);

            //==========================================================================

            regionDropdown.addListener(new ChangeListener() {
                @Override
                public void changed(ChangeEvent event, Actor actor) {
                    IoLogger.log("MainMenu select region: ", regionDropdown.getSelected());
                    getGame().getGamePreferences().putString(PREFERENCES_REGION, regionDropdown.getSelected());
                    getGame().getGamePreferences().flush();

                    // connectionToServersWithSquadsAndGameTypes();
                }
            });

            gameModeDropdown.addListener(new ChangeListener() {
                @Override
                public void changed(ChangeEvent event, Actor actor) {
                    IoLogger.log("MainMenu select gamemode: ", gameModeDropdown.getSelected());
                    getGame().getGamePreferences().putString(PREFERENCES_MODE, gameModeDropdown.getSelected());
                    getGame().getGamePreferences().flush();

                    // connectionToServersWithSquadsAndGameTypes();
                }
            });


            name = new Label("Name",skin);
            name.setColor(Color.BLACK);
            connectionStatus = new Label("",skin);

            rootTable = new Table(skin);
            rootTable.setFillParent(true);
            table = new Table(skin);

            width = stage.getWidth()/2;
            height = stage.getHeight()/2;

            table.setPosition(width,height);

            member1Name = new Label("Member 1", skin);
            member2Name = new Label("Member 2", skin);
            member3Name = new Label("Member 3", skin);
            member4Name = new Label("Member 4", skin);
            teamIdLabel = new Label("TEAM ID", skin);
            teamIdLabel.setColor(Color.GOLDENROD);

            memberNamesLabels = new Label[4];
            memberNamesLabels[0] = member1Name;
            memberNamesLabels[1] = member2Name;
            memberNamesLabels[2] = member3Name;
            memberNamesLabels[3] = member4Name;

            //table.add(name).height(30);
           // table.add(userNameInput).width(160).height(40);
            table.row();
            table.add(regionList).center().width(160).height(40);
            table.add(regionDropdown).colspan(2).center().width(150).height(30);
            table.row();

            table.add(gameModeDropdown).colspan(2).center().width(150).height(30);
            table.add(connectionStatus).height(30);
            table.row();

            table.add(autofillChck).colspan(2).center().width(150).height(30);
            table.row();

            table.add(btnPlay).center().width(160).height(40);
            table.row();

            table.add(btnLeave).center().width(160).height(40);
            table.row();


            table.add(teamIdLabel).height(30);
            table.row();
            table.row();

            table.add(member1Name).height(30);
            table.row();

            table.add(member2Name).height(30);
            table.row();

            table.add(member3Name).height(30);
            table.row();

            table.add(member4Name).height(30);
            table.row();

            rootTable.add(table);
            stage.addActor(rootTable);
            stage.setDebugInvisible(true);

            //scheduleConnectionToSelectedRegion();

            updateTeamInfoTask = new Timer.Task() {
                @Override
                public void run() {
                    updateTeamInfo();
                }
            };



            Timer.schedule(updateTeamInfoTask, 0.1f, 1f );


            game.getNetworkManager().addPacketHandler(this);
        } catch (Exception e) {
            //game.getNetworkManager().sendPacket(new ExceptionPacket("MainMenuScreen constructor",e.getMessage()));

            getGame().getMasterServer().postExceptionDetails(e, "MainMenuScreen constructor");
            IoLogger.log("constructor", e.getMessage());
            e.printStackTrace();
        }


    }



    private void joinTeam(String  teamId, String memberName)
    {
        MasterServer.RequestResult res = GameClient.getGame().getMasterServer().joinTeam(memberName,teamId);
    }

    private void updateTeamInfo()
    {

        if(teamId.isEmpty())
        {
            IoLogger.log(TAG, "updateTeamInfo no team set, should be a leader then");
            return;
        }


        final MasterServer.RequestResult res = GameClient.getGame().getMasterServer().getTeamUpdateInfo(teamId);
        Timer.schedule(new Timer.Task() {
            @Override
            public void run() {
                if(res.isFailed() || (res.successfullresponse != null &&  res.successfullresponse.getStatus().equals(SC_OK)))
                {
                    connectionStatus.setText("Master server request failed");
                    connectionStatus.setColor(Color.RED);
                }
                String response =	res.response;
                try	{

                    TeamDto info = json.fromJson(TeamDto.class,response);
                    teamInfo = info;

                    for (int i = 0; i < info.getTeamMemberNames().length; i++)
                    {
                       memberNamesLabels[i].setText(info.getTeamMemberNames()[i]);
                        if(info.getTeamMemberNames()[i].equals(info.getTeamLeaderName()))
                            memberNamesLabels[i].setColor(Color.GREEN);

                        else
                            memberNamesLabels[i].setColor(Color.BLACK);
                    }

                    teamIdLabel.setText("TEAM ID: " + info.getId());

                    if(info.isIngame())
                    {
                        if(loginReqSent) return;
                        updateTeamInfoTask.cancel();
                        connectToHttpServer();
                    }

                    //todo remove in production
                    connectionStatus.setText("Team info updated");
                    connectionStatus.setColor(Color.GREEN);
                    IoLogger.log("updateTeamInfo: ", info.toString());
                }

                catch (Exception e){


                    connectionStatus.setText("failed to receive Team info "  + e.getMessage());
                    connectionStatus.setColor(Color.RED);
                    e.printStackTrace();
                }
            }
        }, DEFAULT_TIMEOUT);
    }



    private void startMatch()
    {
        final MasterServer.RequestResult res = GameClient.getGame().
                getMasterServer()
                .startTeamMatch(regionDropdown.getSelected(), gameModeDropdown.getSelected(), false, teamId);
        Timer.schedule(new Timer.Task() {
            @Override
            public void run() {
                if(res.isFailed() || res.response==null || (res.successfullresponse != null &&  res.successfullresponse.getStatus().equals(SC_OK)))
                {
                    connectionStatus.setText("Master server request failed");
                    connectionStatus.setColor(Color.RED);
                }
                 else   if(res.response.contains("Cannot find server"))
                {
                    connectionStatus.setText(res.response);
                    connectionStatus.setColor(Color.RED);
                }

                //team leader will join match in updateTeamInfo() as well as  others


               /* String response =	res.response;


                try	{

                    TeamDto connectionInfo = json.fromJson(TeamDto.class,response);

                    //todo remove in production
                    connectionStatus.setText("Match started");
                    connectionStatus.setColor(Color.GREEN);
                    IoLogger.log("startMatch: ","team is " + connectionInfo );
                }

                catch (Exception e){
                    //todo remove in production
                    connectionStatus.setText("failed to receive Team info");
                    connectionStatus.setColor(Color.RED);
                    e.printStackTrace();
                }*/
            }
        }, DEFAULT_TIMEOUT);
    }

    private void connectToHttpServer()
    {
        getGame().getNetworkManager().disconnect();
        getGame().getNetworkManager().addPacketHandler(this);


        try {

            if(!PRODUCTION_MODE)
            {
                //todo remove
                game.getNetworkManager().connect(LOCAL_SERVER_URL, LOCAL_SERVER_PORT);
            }
            else {
                game.getNetworkManager()
                        .connect(teamInfo.getServerToConnect()
                        .replaceFirst("http://", "")
                        .replaceFirst("https://", ""), PRODUCTION_SERVER_PORT);
            }


            getGame().getNetworkManager().addPacketHandler(this);
            connectionStatus.setText("Server found : " + teamInfo.getServerToConnect());
            connectionStatus.setColor(Color.GREEN);

            loginToGameServer();

        }
        catch (Exception e){

            e.printStackTrace();

            //todo for testing only
            connectionStatus.setText("Connection problem : " + e.getMessage());
            connectionStatus.setColor(Color.RED);
        }
    }

    private void loginToGameServer()
    {
        if(loginReqSent) return;
       loginReqSent = true;
        game.getNetworkManager().sendPacket(
                new TeamLoginPacket(isTeamLeader, teamInfo.getGameType(),

                        teamInfo.getTeamMemberNames().length, teamId, teamInfo.isAutoFill(), playerName, getGame().getClientId(),skinManager.getStringNameSkin()));
    }


    @Override
    public void render(float delta) {
        try {
            Gdx.gl.glClearColor(0.68f,0.62f,0.57f, 1);
            Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

            SpriteBatch batch = game.getBatch();
            batch.begin();
            stage.act();
            stage.draw();
            inputHandle();
            batch.end();


        } catch (Exception e) {
            //game.getNetworkManager().sendPacket(new ExceptionPacket("MainMenuScreen render",e.getMessage()));
            IoLogger.log("render", e.getMessage());

        }

    }

    public void inputHandle() {
       // if (Gdx.input.isKeyJustPressed(Input.Keys.ENTER)) loginToGameServer();


        if (Gdx.input.isButtonPressed(Input.Buttons.LEFT)) {
            Vector3 touchPos = new Vector3();
            touchPos.set(Gdx.input.getX(), Gdx.input.getY(), 0);
        }


    }


    @Override
    public void show() {
        Gdx.input.setInputProcessor(stage);

    }

    @Override
    public void resize(int width, int height) {
        stage.getViewport().update(width, height);


        this.width = width;
        this.height = height;

    }

    @Override
    public void pause() {


    }

    @Override
    public void resume() {
        Gdx.input.setInputProcessor(stage);


    }

    @Override
    public void hide() {


    }

    @Override
    public void dispose() {

        try {
            game.getNetworkManager().removePacketHandler(this);
            if(createTeamTask != null)  createTeamTask.cancel();
            updateTeamInfoTask.cancel();
           // updateserversInfoTask.cancel();
        } catch (Exception e) {
            //game.getNetworkManager().sendPacket(new ExceptionPacket("MainMenuScreen dispose",e.getMessage()));
            IoLogger.log("render", e.getMessage());
            e.printStackTrace();
        }
    }

    @Override
    public boolean handlePacket(Packet packet) {

        try {
            if(packet.packetType == PacketType.LOGIN_RESPONSE)
            {
                //IoLogger.log("","handlePacket MMS");
                LoginResponsePacket p = (LoginResponsePacket) packet;

                PlayerCell myPlayer =  new PlayerCell(game.getBatch(), game.getGameShapeRenderer(), GameClient.getFont32Black(), p.name, p.x,p.y, 100,
                        p.playerId, PLAYERS_MAX_HEALTH, PLAYERS_MAX_HEALTH, PLAYERS_MAX_STAMINA, PLAYERS_MAX_STAMINA, p.currentXp,p.playerSkinCollor);



                myPlayer.setEmoteNames(emoteManager.getEmoteList());

                game.setScreen(new GameScreen(game, myPlayer));
                dispose();
                return true;
            }

            if(packet.packetType == SERVER_STATE_INFO)
            {
                GameStateChangedPacket p = (GameStateChangedPacket) packet;
                GameClient.getGame().setGameType(p.gameType);
                return true;
            }
        } catch (Exception e) {
            //game.getNetworkManager().sendPacket(new ExceptionPacket("MainMenuScreen handlePacket",e.getMessage()));
            IoLogger.log(TAG, e.getMessage());
            e.printStackTrace();
        }

        return false;
    }



    private void createTeam()
    {
        final MasterServer.RequestResult res = GameClient.getGame().getMasterServer().createTeam(playerName);
        Timer.schedule(new Timer.Task() {
            @Override
            public void run() {
                if(res.isFailed() || (res.successfullresponse != null &&  res.successfullresponse.getStatus().equals(SC_OK)))
                {
                    connectionStatus.setText("Master server request failed");
                    connectionStatus.setColor(Color.RED);
                }

                String response =	res.response;


                try	{

                    teamId = json.fromJson(String.class,response);

                    createTeamTask.cancel();
                    //todo remove in production
                    connectionStatus.setText("Team created");
                    connectionStatus.setColor(Color.GREEN);
                    IoLogger.log("updateTeamInfo: ","teamId is " + teamId );
                }

                catch (Exception e){
                    //todo remove in production
                    connectionStatus.setText("failed to create Team  " + e.getMessage());
                    connectionStatus.setColor(Color.RED);
                    e.printStackTrace();
                }
            }
        }, DEFAULT_TIMEOUT);
    }

    private void leaveTeam ()
    {
        final MasterServer.RequestResult res = GameClient.getGame().getMasterServer().leaveTeam(playerName, teamId);
        Timer.schedule(new Timer.Task() {
            @Override
            public void run() {
                if(res.isFailed() || (res.successfullresponse != null &&  res.successfullresponse.getStatus().equals(SC_OK)))
                {
                    connectionStatus.setText("Master server request failed");
                    connectionStatus.setColor(Color.RED);
                }

                /*String response =	res.response;


                try	{

                    teamId = json.fromJson(Long.class,response);


                    //todo remove in production
                    connectionStatus.setText("Team created");
                    connectionStatus.setColor(Color.GREEN);
                    IoLogger.log("updateTeamInfo: ","teamId is " + teamId );
                }

                catch (Exception e){
                    //todo remove in production
                    connectionStatus.setText("failed to receive Team info");
                    connectionStatus.setColor(Color.RED);
                    e.printStackTrace();
                }*/
            }
        }, DEFAULT_TIMEOUT);
    }


}