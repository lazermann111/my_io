package com.github.myio.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.github.myio.GameClient;


/**
 * Created by Alexsandr on 02.07.2018.
 */

public class FadeScreen implements Screen
{
    private GameClient fGame;
    private Stage stage;
    private Sprite sprite;
    private Table fadeTable;
    private Image image;
    private Texture texture;
    Skin skin;


    public FadeScreen(GameClient game)
    {
        this.fGame = game;
        stage = new Stage( );
        //texture = new Texture(Gdx.files.internal("images/black.jpg"));
//        image = new Image(texture);

    }


    private float flashTimer;
    private  boolean flashing  ;
    public static final int FLASH_TIME = 3;

    public void fadeEffect(float deltaTime )
    {
        if(!flashing) return;
        flashTimer += deltaTime;
        float alpha = flashTimer/FLASH_TIME;

        //*Gdx.gl.glEnable(GL20.GL_BLEND);
        Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
        GameClient.getGame().getGameShapeRenderer().begin(ShapeRenderer.ShapeType.Filled);
        GameClient.getGame().getGameShapeRenderer().setColor(new Color(0, 0, 0,alpha));
        GameClient.getGame().getGameShapeRenderer().rect(0, 0, 1000, 1000);
        GameClient.getGame().getGameShapeRenderer().end();

        Gdx.gl.glDisable(GL20.GL_BLEND);

//*       Gdx.gl.glEnable(GL20.GL_BLEND);
        Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);

        //  IoLogger.log("RM", alpha+"");
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        Gdx.gl.glClearColor(1,1,1,alpha);
        Gdx.gl.glDisable(GL20.GL_BLEND);
        if(flashTimer >= FLASH_TIME)
            flashing = false;
    }

    @Override
    public void show() {

    }

    @Override
    public void render(float delta) {
        stage.act();
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }
}
