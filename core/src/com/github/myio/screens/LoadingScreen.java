package com.github.myio.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Animation.PlayMode;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.TimeUtils;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.github.myio.GameClient;
import com.github.myio.GameConstants;
import com.github.myio.tools.LoadingBarPart;
import com.github.myio.ui.IDrawable;
import com.github.myio.ui.LoadingBar;

import java.util.ArrayList;

/**
 * Created by Alexsandr on 05.07.2018.
 */

public class LoadingScreen   implements Screen {

    private GameClient game;
    private SpriteBatch sb;


    private AtlasRegion title;
    private Animation flameAnimation;

    public final int IMAGE = 0;        // loading images
    public final int FONT = 1;        // loading fonts
    public final int PARTY = 2;        // loading particle effects
    public final int SOUND = 3;        // loading sounds
    public final int MUSIC = 4;        // loading music
    public final int SKIN = 5;     //loading skins

    private int currentLoadingStage = 0;

    // timer for exiting loading screen
    public float countDown = 1f;
    private float stateTime;
    private AtlasRegion dash;

    private Stage stage;

    private LoadingBar loadingBar;

    private ArrayList<IDrawable> UI;
    private int load = 0;

    private TextureAtlas atlas;

    private Image loadingFrame;
    private Image loadingBarHidden;
    private Image screenBg;
    private Image loadingBg;
    private Image image;

    private Label textLoading;

    private BitmapFont font;
    private Skin skin;


    private Texture texture;

    public LoadingScreen(GameClient game) {
        this.game = game;
        sb = new SpriteBatch();
        /*sb.setBlendFunction(GL20.GL_SRC_ALPHA, GL20.GL_ONE);*/
        stage = new Stage(new ScreenViewport());
        UI = new ArrayList<IDrawable>();
        loadingBar = new LoadingBar(load);
        texture = new Texture(Gdx.files.internal("images/mean.png"));
        font = new BitmapFont(Gdx.files.internal("fonts/font32White.fnt"), false);
        image = new Image(texture);


        brg();
        loadAssets();

        // initiate queueing of images but don't start loading
        game.loadAsset.queueAddImages();
        game.loadAsset.manager.finishLoading();

        UI.add(loadingBar);
        System.out.println("Loading images....");
    }

    private Image titleImage;
    private Table table;
    private Table loadingTable;

    private void brg() {
        table = new Table();
        table.setFillParent(true);
        table.setDebug(false);
        table.add(image).width(300).height(300); /*.align(Align.center).pad(2, 0, 0, 0).colspan(2);*/
        table.row(); // move to next row
        //table.add(loadingTable).width(400);

        stage.addActor(table);
    }

    @Override
    public void show() {

        // load loading images and wait until finished
     /*   game.loadAsset.queueAddLoadingImages();
        game.loadAsset.manager.finishLoading();

        // get images used to display loading progress
      // atlas = game.loadAsset.manager.get("atlas/mainMenu/newSkin.atlas");
        title = atlas.findRegion("staying-alight-logo");
        dash = atlas.findRegion("loading-dash");

        flameAnimation = new Animation(0.07f, atlas.findRegions("flames/flames"), PlayMode.LOOP);

        // initiate queueing of images but don't start loading
        game.loadAsset.queueAddImages();
        System.out.println("Loading images....");

        stateTime = 0f;*/
       /* titleImage = new Image(title);



        loadingTable = new Table();
        loadingTable.add(new LoadingBarPart(dash,flameAnimation));
        loadingTable.add(new LoadingBarPart(dash,flameAnimation));
        loadingTable.add(new LoadingBarPart(dash,flameAnimation));
        loadingTable.add(new LoadingBarPart(dash,flameAnimation));
        loadingTable.add(new LoadingBarPart(dash,flameAnimation));
        loadingTable.add(new LoadingBarPart(dash,flameAnimation));
        loadingTable.add(new LoadingBarPart(dash,flameAnimation));
        loadingTable.add(new LoadingBarPart(dash,flameAnimation));
        loadingTable.add(new LoadingBarPart(dash,flameAnimation));
        loadingTable.add(new LoadingBarPart(dash,flameAnimation));


        */
    }

    private void drawLoadingBar(int stage, TextureRegion currentFrame) {
        for (int i = 0; i < stage; i++) {
            sb.draw(currentFrame, 50 + (i * 50), 150, 50, 50);
            sb.draw(dash, 35 + (i * 50), 140, 80, 80);
        }
    }

    private void loadAssets() {
        // load loading images and wait until finished
        game.loadAsset.queueAddLoadingImages();


        game.loadAsset.manager.finishLoading();


        // get images used to display loading progress
      /*  atlas = game.loadAsset.manager.get("images/loading.atlas");
        title = atlas.findRegion("staying-alight-logo");
        dash = atlas.findRegion("loading-dash");
        flameAnimation = new Animation(0.07f, atlas.findRegions("flames/flames"), PlayMode.LOOP);*/
    }

    private long timeUpdate;
    private float timerGame = 100f;
    private long startTime = 0;

    private boolean l = true;

    private boolean lod() {
        if (game.loadAsset.manager.update()) { // Load some, will return true if done loading
            currentLoadingStage += 1;


            load++;


            if (currentLoadingStage <= 5) {

              /*  loadingTable.getCells().get((currentLoadingStage-1)*2).getActor().setVisible(true);  // new
                loadingTable.getCells().get((currentLoadingStage-1)*2+1).getActor().setVisible(true);*/
            }
            switch (currentLoadingStage) {
                case FONT:
                    System.out.println("Loading fonts....");
                    game.loadAsset.queueAddFonts();
                    game.loadAsset.manager.finishLoading();
                    break;

                case PARTY:
                    System.out.println("Loading Particle Effects....");
                    game.loadAsset.queueAddParticleEffects();
                    game.loadAsset.manager.finishLoading();


                    break;
                case SOUND:
                    System.out.println("Loading Sounds....");
                    game.loadAsset.queueAddSounds();
                    game.loadAsset.manager.finishLoading();


                    break;
                case MUSIC:
                    System.out.println("Loading music....");
                    game.loadAsset.queueAddMusic();
                    game.loadAsset.manager.finishLoading();


                    break;
                case SKIN:
                    System.out.println("Loading skins....");
                    game.loadAsset.queueAddSkin();
                    game.loadAsset.manager.finishLoading();


                    break;
                case 6:
                    System.out.println("Finished");
                    game.loadAsset.manager.finishLoading();

                    break;
            }
        }
        return l;


    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

      /*  for (IDrawable drawable : UI)
        {
            drawable.draw(delta);

        }*/
        sb.begin();
        font.draw(sb, "Loading" + load, 70, 60);
        sb.end();

      /*  if (game.loadAsset.manager.update()) { // Load some, will return true if done loading
            currentLoadingStage+= 1;

            load++;

            if(currentLoadingStage <= 5){

              *//*  loadingTable.getCells().get((currentLoadingStage-1)*2).getActor().setVisible(true);  // new
                loadingTable.getCells().get((currentLoadingStage-1)*2+1).getActor().setVisible(true);*//*
            }
            switch(currentLoadingStage){
                case FONT:
                    System.out.println("Loading fonts....");
                    game.loadAsset.queueAddFonts();
                    game.loadAsset.manager.finishLoading();

                    break;

                case PARTY:
                    System.out.println("Loading Particle Effects....");
                    game.loadAsset.queueAddParticleEffects();
                    game.loadAsset.manager.finishLoading();

                    break;
                case SOUND:
                    System.out.println("Loading Sounds....");
                    game.loadAsset.queueAddSounds();
                    game.loadAsset.manager.finishLoading();

                    break;
                case MUSIC:
                    System.out.println("Loading music....");
                    game.loadAsset.queueAddMusic();
                    game.loadAsset.manager.finishLoading();

                    break;
                case SKIN:
                    System.out.println("Loading skins....");
                    game.loadAsset.queueAddSkin();
                    game.loadAsset.manager.finishLoading();

                    break;
                case 6:
                    System.out.println("Finished");
                    game.loadAsset.manager.finishLoading();
                    break;
            }*/
                if(lod() ) {
                    if (currentLoadingStage > 6) {
                        countDown -= delta;
                        currentLoadingStage = 6;

                        if (countDown < 0) {

                            game.changeScreen(GameClient.APPLICATION);
                        }
                    }
                }

        stage.act();
        stage.draw();
    }
       /* Gdx.gl.glClearColor(0,0,0,1);
        Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        stateTime += delta; // Accumulate elapsed animation time
        // Get current frame of animation for the current stateTime
      // TextureRegion currentFrame = (TextureRegion) flameAnimation.getKeyFrame(stateTime, true);

        sb.begin();
       // drawLoadingBar(currentLoadingStage * 2, currentFrame);
       // sb.draw(title, 135, 250);
        sb.end();

        if (game.loadAsset.manager.update()) { // Load some, will return true if done loading
            currentLoadingStage+= 1;
            switch(currentLoadingStage){
                case FONT:
                    System.out.println("Loading fonts....");
                    game.loadAsset.queueAddFonts();
                    break;
                case PARTY:
                    System.out.println("Loading Particle Effects....");
                    game.loadAsset.queueAddParticleEffects();
                    break;
                case SOUND:
                    System.out.println("Loading Sounds....");
                    game.loadAsset.queueAddSounds();
                    break;
                case MUSIC:
                    System.out.println("Loading fonts....");
                    game.loadAsset.queueAddMusic();
                    break;
                case 5:
                    System.out.println("Finished");
                    break;
            }
            if (currentLoadingStage >5){
                countDown -= delta;
                currentLoadingStage = 5;
                if(countDown < 0){
                    game.changeScreen(GameClient.APPLICATION);
                }
            }
        }*/



    @Override
    public void resize(int width, int height) {
        // TODO Auto-generated method stub

    }

    @Override
    public void pause() {
        // TODO Auto-generated method stub

    }

    @Override
    public void resume() {
        // TODO Auto-generated method stub

    }

    @Override
    public void hide() {
        // TODO Auto-generated method stub

    }

    @Override
    public void dispose() {
        // TODO Auto-generated method stub

    }

}
