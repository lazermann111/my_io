package com.github.myio.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Sound;



public class SoundTest implements Screen {


    Sound sound;


    @Override
    public void show () {

        //Music music = Gdx.audio.newMusic(Gdx.files.internal("sounds/rocket_launch.mp3"));
        sound = Gdx.audio.newSound(Gdx.files.internal("sounds/expl1.mp3"));
       long id =  sound.play();
        //music.play();
       // sound.pause();
    }

    @Override
    public void render (float d)

    {

        //sound.play(1.0f,0.0f,0.8f);
    }



    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        sound.dispose();
    }


}
