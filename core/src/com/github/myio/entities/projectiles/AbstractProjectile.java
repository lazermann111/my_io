package com.github.myio.entities.projectiles;


import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.g2d.ParticleEffectPool;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.utils.Pool;
import com.github.myio.GameClient;
import com.github.myio.entities.Collidable;
import com.github.myio.entities.IoPoint;
import com.github.myio.entities.PlayerCell;
import com.github.myio.entities.map.MapEntityCell;
import com.github.myio.entities.map.TacticalShield;
import com.github.myio.entities.tile.Tile;
import com.github.myio.enums.ProjectileType;
import com.github.myio.enums.TileType;
import com.github.myio.networking.NetworkManager;
import com.github.myio.networking.packet.ProjectileLaunchedPacket;
import com.github.myio.tools.SoundManager;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static com.github.myio.GameConstants.DAMAG_WITH_ARMOR;
import static com.github.myio.GameConstants.PLAYERS_MIN_ARMOR;
import static com.github.myio.GameConstants.isServer;

public abstract class AbstractProjectile implements Pool.Poolable {

    public ProjectileType projectileType;
    protected ShapeRenderer drawer;

    protected IoPoint origin;
    protected IoPoint destination;
    protected IoPoint currentPosition;
    protected IoPoint previousPosition;
    protected float scatter;

    protected ParticleEffectPool.PooledEffect trailEffect;

    protected ParticleEffectPool.PooledEffect explosion;
    protected SpriteBatch batch;
    protected Camera camera; // reference to current player pos


    public int damage;
    public float speed; //units per second
    public int maxDistance;
    public int radius;

    public String shooterId;

    protected boolean reachedDestination = false;
    protected long reachedDestinationTime = 0;
    protected boolean isShowingSplash;
    public IoPoint getCurrentPosition() {
        return currentPosition;
    }

    public abstract ProjectileLaunchedPacket launch(NetworkManager networkManager, IoPoint start, IoPoint end, String shooterId, float scatter);

    public ParticleEffectPool.PooledEffect getTrailEffect() {
        return trailEffect;
    }

    public ParticleEffectPool.PooledEffect getExplosionEffect() {
        return explosion;
    }

    //client-side
    public abstract void drawTrail();

    //both server and client side
    public abstract void update(float deltaTime);

    //client-side
    public abstract void render(float deltaTime);

    //both server and client side
    public void collide(Collidable cell)
    {
        if(isServer && cell != null &&  cell instanceof PlayerCell)
        {
            PlayerCell player = (PlayerCell) cell;

            if(player.getCurrentArmor() <= PLAYERS_MIN_ARMOR)
            {
                player.setCurrentHealth(player.getCurrentHealth() - damage);
            }else{
                if (damage <= player.getCurrentArmor())
                {
                    player.setCurrentArmor(player.getCurrentArmor() - ((int) (damage * DAMAG_WITH_ARMOR)));
                }else {
                        int differentDamage =  ((int) (damage * DAMAG_WITH_ARMOR)) - player.getCurrentArmor();
                        player.setCurrentArmor(PLAYERS_MIN_ARMOR);
                        player.setCurrentHealth(player.getCurrentHealth() - differentDamage);

                }

            }
        }

        if(isServer && cell != null &&  cell instanceof MapEntityCell)
        {
            MapEntityCell mapEntityCell = (MapEntityCell) cell;
            if (mapEntityCell.isBreakable())
                mapEntityCell.setDurability(mapEntityCell.getDurability() - damage);

        }

        if(!isServer)
        {
            if(cell instanceof PlayerCell)
            {
                playerHitEffects((PlayerCell)cell);
            }
            else if(cell instanceof TacticalShield && !(this instanceof Rocket) && !(this instanceof GrenadeAmmo)&& !(this instanceof FlashbangAmmo))
            {
                GameClient.getGame().getSoundManager().playSoundWithPanAndVolume(SoundManager.SHIELD_HIT,
                        new IoPoint(camera.position.x, camera.position.y), currentPosition );
            }

            if (cell instanceof Tile)
                switch (((Tile)cell).getTileType()){
                    case WALL_BROWN:
                        GameClient.getGame().getSoundManager().playSoundWithPanAndVolume(SoundManager.WALLHIT,
                            new IoPoint(camera.position.x, camera.position.y), currentPosition );

                        break;
                    case WALL_BUSH_BROWN:
                        break;

                    case WALL_YELLOW:
                        GameClient.getGame().getSoundManager().playSoundWithPanAndVolume(SoundManager.WALL_YELLOW,
                                new IoPoint(camera.position.x, camera.position.y), currentPosition );
                        break;
                }


            if (cell instanceof MapEntityCell)
            switch (((MapEntityCell)cell).getType()){
                case BUSH:
                    GameClient.getGame().getSoundManager().playSoundWithPanAndVolume(SoundManager.BUSH,
                            new IoPoint(camera.position.x, camera.position.y), currentPosition );
                    break;
                case SPIDER_WEB:
                    GameClient.getGame().getSoundManager().playSoundWithPanAndVolume(SoundManager.SPIDER_WEB,
                            new IoPoint(camera.position.x, camera.position.y), currentPosition );
                    break;
                case SPIDER_WEB_BIG:
                    GameClient.getGame().getSoundManager().playSoundWithPanAndVolume(SoundManager.SPIDER_WEB,
                            new IoPoint(camera.position.x, camera.position.y), currentPosition );
                    break;
                case STONE_SMALL:
                    GameClient.getGame().getSoundManager().playSoundWithPanAndVolume(SoundManager.STONE_SMALL,
                            new IoPoint(camera.position.x, camera.position.y), currentPosition );
                    break;
                case STONE_BIG:
                    GameClient.getGame().getSoundManager().playSoundWithPanAndVolume(SoundManager.STONE_BIG,
                            new IoPoint(camera.position.x, camera.position.y), currentPosition );
                    break;
                case STONE_GREEN:
                    GameClient.getGame().getSoundManager().playSoundWithPanAndVolume(SoundManager.STONE_GREEN,
                            new IoPoint(camera.position.x, camera.position.y), currentPosition );
                    break;

            }

            soundAndSplash();
            isShowingSplash = true;
        }
    }

    public abstract boolean  canBeRemoved();
    public abstract void soundAndSplash();

    public  void playerHitEffects(PlayerCell cell)
    {
        GameClient.getGame().getSoundManager().playSoundWithPanAndVolume(SoundManager.HIT,
                new IoPoint(camera.position.x, camera.position.y), currentPosition );

        cell.bloodSplash(currentPosition);
    };


    public boolean reachedDestination()
    {
        if(currentPosition.distance(destination) < radius * 3 && !reachedDestination) {
            reachedDestination = true;
            reachedDestinationTime = System.currentTimeMillis();
        }
        else if(reachedDestination)
            reachedDestination = true;
        else
            reachedDestination = false;
        return /*currentPosition.distance(destination) > maxDistance ||*/  reachedDestination;
    }

    public long getReachedDestinationTime() {
        return reachedDestinationTime;
    }

    public boolean canBeDeleted() { //for grenade delay
        return true;
    }

    public void applyScatterAngle()
    {

    }
    public boolean hasSplashDamage()
    {
        return false;
    }

    public int getSplashRadius()
    {
        return 0;
    }

    public ArrayList<PlayerCell> applySplashDamage(Collection<PlayerCell> playerCellList, Collection<MapEntityCell> nearestEntities, TileType[][] nearestTiles)
    {
        return null;
    }

    public void collide(List<MapEntityCell> nearestEntities, List<Tile> nearestTiles) {

    }

    public void applyDamageToPit(Collection<PlayerCell> values, MapEntityCell c) {

    }


    @Override
    public void reset() {
        reachedDestination = false;
        reachedDestinationTime = 0;
        isShowingSplash = false;
    }
}
