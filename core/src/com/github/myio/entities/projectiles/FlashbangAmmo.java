package com.github.myio.entities.projectiles;


import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Pool;
import com.github.myio.GameClient;
import com.github.myio.entities.Collidable;
import com.github.myio.entities.IoPoint;
import com.github.myio.entities.PlayerCell;
import com.github.myio.entities.map.MapEntityCell;
import com.github.myio.entities.tile.Tile;
import com.github.myio.enums.MapEntityType;
import com.github.myio.enums.ProjectileType;
import com.github.myio.networking.NetworkManager;
import com.github.myio.networking.packet.ProjectileLaunchedPacket;
import com.github.myio.tools.IoLogger;
import com.github.myio.tools.SoundManager;

import java.util.List;

import static com.github.myio.GameConstants.TIME_TO_GRENADE_EXPLODE;
import static com.github.myio.GameConstants.isServer;
import static com.github.myio.StringConstants.FLASH_GRENADE;

public class FlashbangAmmo extends AbstractProjectile implements Pool.Poolable   {
    Sprite sprite;
    ParticlesPoolManager particlesPoolManager;
    private double flashRadius = 1500;
    float flyEffect = 1;
    double initialDistance = 0;
    boolean isLaunch = true;

    public FlashbangAmmo( ) {
    }


    public FlashbangAmmo(ShapeRenderer renderer) {
        drawer = renderer;
        damage = 0;
        projectileType = ProjectileType.FLASHBANG;
        speed = 1700;
        maxDistance = 1500;
        radius = 10;
    }

    public FlashbangAmmo(ShapeRenderer renderer, SpriteBatch b, Camera c, ProjectileLaunchedPacket packet) {

        drawer = renderer;

        damage = packet.damage;
        speed = packet.speed;
        maxDistance = packet.maxDistance;
        damage = packet.damage;
        origin = packet.start.copy();
        currentPosition = packet.start.copy();
        destination = packet.end;
        shooterId = packet.shooterId;
        scatter = packet.scatter;
        projectileType = ProjectileType.FLASHBANG;
        radius = 10;


        if(isServer) return;



        this.sprite = GameClient.cellAtlas.createSprite(FLASH_GRENADE);
        batch = b;
        camera = c;
    }

    public void assignFromPacket (ShapeRenderer renderer, SpriteBatch b, Camera c, ProjectileLaunchedPacket packet,ParticlesPoolManager particlesPoolManager) {

        this.particlesPoolManager =particlesPoolManager;
        drawer = renderer;

        damage = packet.damage;
        speed = packet.speed;
        maxDistance = packet.maxDistance;
        damage = packet.damage;
        origin = packet.start.copy();
        currentPosition = packet.start.copy();
        destination = packet.end;
        shooterId = packet.shooterId;
        scatter = packet.scatter;
        projectileType = ProjectileType.FLASHBANG;
        radius = 10;
        speed = (float) (speed * destination.distance(origin) / maxDistance);
       // System.out.println("speed  " + speed);

        applyScatterAngle();
        if(isServer) return;

        this.sprite = GameClient.cellAtlas.createSprite(FLASH_GRENADE);

        batch = b;
        camera = c;
        }

    @Override
    public void applyScatterAngle() {
    }

    @Override
    public ProjectileLaunchedPacket launch(NetworkManager networkManager, IoPoint start, IoPoint end, String shooterId, float scatter) {
        this.origin = start;
        this.destination = end;
        this.currentPosition = start.copy();
        ProjectileLaunchedPacket res =new ProjectileLaunchedPacket(origin,destination, speed,damage,maxDistance, projectileType,shooterId, this.scatter);
        networkManager.sendPacket(res);

        return res;
    }

    @Override
    public void drawTrail() {
        if(isShowingSplash) return;
    }

    @Override
    public void update(float deltaTime) {

        if(!reachedDestination()) {
            double distance = destination.distance(currentPosition);

            Vector2 forward = new Vector2(destination.getX() - currentPosition.getX(), destination.getY() - currentPosition.getY());
            forward.scl((float) (speed / distance) * deltaTime);

            currentPosition.x += forward.x;
            currentPosition.y += forward.y;

            if(isLaunch) {
                initialDistance = distance;
                isLaunch = false;
            }
            flyEffect = getflyEffect(distance);
        }
    }



    @Override
    public void render(float deltaTime) {
        if(!isShowingSplash)
        {
            sprite.draw(batch);

            if(reachedDestination()) {
                sprite.rotate(10);
            }
            else
                sprite.rotate(5);
            sprite.setPosition(currentPosition.getX(), currentPosition.getY());
            sprite.setScale(flyEffect);


        }

        else if(!flashing)
        //if(canBeDeleted())
        {
            flashing = true;
            if(GameClient.getGame().getLocalPlayer().getPosition().distance(currentPosition) < flashRadius)
                GameClient.getGame().getRenderManager().flash();
        }



    }
  //  private float flashTimer;
    private  boolean flashing  ;
  //  public static final int FLASH_TIME = 3;

    @Override
    public void collide(Collidable cell) {

        if(isServer) return; //no effect on server

        PlayerCell p = GameClient.getGame().getLocalPlayer();
        if(p == null) return;

        //IoPoint playerPos = p.getPosition();
        //IoPoint flash = currentPosition;

        //if(playerPos.distance(flash) > 1000) return; ///todo replace and check if we have walls between

        soundAndSplash();
        isShowingSplash = true;

    }

    @Override
    public void collide(List<MapEntityCell> nearestEntities, List<Tile> nearestTiles) {
        if(isServer) return;

        if(canBeDeleted()) {
            PlayerCell localPlayer = GameClient.getGame().getLocalPlayer();
            boolean isPit = false;

            for (MapEntityCell mapEntityCell : nearestEntities) {
                if (mapEntityCell.getType() == MapEntityType.PIT||mapEntityCell.getType() == MapEntityType.PIT_TRAP)
                    if (mapEntityCell.collidesFlashPit(this)) {
                        soundAndSplash();
                        if (mapEntityCell.getPlayersId().contains(localPlayer.getId())) {
                            isShowingSplash = true;
                        } else {
                            flashing = true;
                        }
                        return; //if flash in pit
                    }
            }

            if (localPlayer.pit != null && !flashing) {
                soundAndSplash();
                flashing = true;
                return; //if player inside pit and flash outside pit
            }

            //Using shelters
            IoPoint playerPos = localPlayer.getPosition();
            IoPoint flash = getCurrentPosition();
            Vector2 vector2 = new Vector2(flash.getX() - playerPos.getX(), flash.getY() - playerPos.getY());
            float angle = vector2.angle();
            //IoLogger.log("Angle: " + angle);
            for (int i = 10; i < playerPos.distance(flash); i++) {
                vector2.setLength(i);
                //IoLogger.log("Vector angle: " + vector2.angle());
                PlayerCell nextStep = new PlayerCell();
                nextStep.setX(vector2.x + localPlayer.getX());
                nextStep.setY(vector2.y + localPlayer.getY());
                nextStep.setCollisionRadius(10);
                for (MapEntityCell entityCell : nearestEntities) {
                    if (entityCell.isObstacle() && entityCell.getType() != MapEntityType.PIT && entityCell.collides(nextStep)
                            || entityCell.collides(nextStep) && entityCell.getType() == MapEntityType.TACTICAL_SHIELD) {
                        flashing = true;
                        soundAndSplash();
                        IoLogger.log("HERE ENTITY");
                        return;
                    }
                }

                for (Tile tile : nearestTiles) {
                    if (tile.isObstacle && tile.collides(nextStep)) {
                        flashing = true;
                        soundAndSplash();
                        IoLogger.log("HERE WALL");
                        return;
                    }
                }


            }

            //if no shelters
            if (!flashing) {
                IoLogger.log("No walls");
                soundAndSplash();
                isShowingSplash = true;
                //flashing = true;
            }
        }
        //vector2.setLength(300);
        /*ShapeRenderer shapeRenderer = new ShapeRenderer();
        shapeRenderer.setColor(Color.RED);
        shapeRenderer.begin(ShapeRenderer.ShapeType.Line);
        shapeRenderer.line(p.getX(), p.getY(), destination.x, destination.y, Color.BLUE, Color.RED);
        shapeRenderer.end();*/
    }

    public boolean canBeRemoved()
    {
        return isServer || flashing;
    }

    @Override
    public boolean canBeDeleted() {
        if(getReachedDestinationTime() != 0 && System.currentTimeMillis() - getReachedDestinationTime() > TIME_TO_GRENADE_EXPLODE) {
            return true;
        }
        return false;
      // return true;
    }

    public void playerHitEffects(PlayerCell c) {

    }


    public void soundAndSplash() {
        if (isShowingSplash) return;

        GameClient.getGame().getSoundManager().playSoundWithPanAndVolume(SoundManager.FLASHBANG, new IoPoint(camera.position.x, camera.position.y),  currentPosition );
    }

    @Override
    public void reset() {
        super.reset();

        damage = 10;
        projectileType = ProjectileType.FLASHBANG;
        speed = 1000;
        maxDistance = 200;
        radius = 10;
    }
    public float getflyEffect(double dist) {
        double x = (initialDistance - dist) / initialDistance;
        if (x > 0 && x <= 0.1 || x > 0.9 && x < 1) {
            return 1.1f;
        }
        if (x > 0.1 && x <= 0.2 || x > 0.8 && x <= 0.9) {
            return 1.5f;
        }
        if (x > 0.2 && x <= 0.3 || x > 0.7 && x <= 0.8) {
            return 2f;
        }
        if (x > 0.3 && x <= 0.4 || x > 0.6 && x <= 0.7) {
            return 2.5f;
        }
        if (x > 0.4 && x <= 0.5 || x > 0.5 && x <= 0.6) {
            return 3f;
        }
        if (x == 0.5) {
            return 4f;
        }
        /*double x, delta_y, y_max, y_current;
        x = (float) ( - dist);
        x = 1500;
        y_max = 500;
        y_current = - (0.0009) * x * x + 0.668 * x;
        delta_y = y_max - y_current;
        return (float) (1 + delta_y / y_max);*/

        return 1;
    }
}
