package com.github.myio.entities.projectiles;

import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Pool;
import com.github.myio.GameClient;
import com.github.myio.GameConstants;
import com.github.myio.StringConstants;
import com.github.myio.entities.IoPoint;
import com.github.myio.entities.PlayerCell;
import com.github.myio.entities.map.MapEntityCell;
import com.github.myio.entities.tile.TileDetails;
import com.github.myio.enums.MapEntityType;
import com.github.myio.enums.ProjectileType;
import com.github.myio.enums.TileType;
import com.github.myio.networking.NetworkManager;
import com.github.myio.networking.packet.ProjectileLaunchedPacket;
import com.github.myio.tools.IoLogger;
import com.github.myio.tools.SoundManager;

import java.util.ArrayList;
import java.util.Collection;

import static com.github.myio.GameConstants.PLAYERS_MIN_ARMOR;
import static com.github.myio.GameConstants.TILE_HEIGHT;
import static com.github.myio.GameConstants.TILE_WIDTH;
import static com.github.myio.GameConstants.TIME_TO_GRENADE_EXPLODE;
import static com.github.myio.GameConstants.isServer;
import static com.github.myio.StringConstants.FRAG_GRENADE;


public class GrenadeAmmo extends AbstractProjectile implements Pool.Poolable
{

    Sprite sprite;
    ParticlesPoolManager particlesPoolManager;
    float flyEffect = 1;
    double initialDistance = 0;
    boolean isLaunch = true;

    public GrenadeAmmo()
    {
    }

    private int splashRadius = 1200;

    public GrenadeAmmo(ShapeRenderer renderer)
    {
        drawer = renderer;
        damage = 90;
        projectileType = ProjectileType.FRAG_GRENADE;
        speed = 1700;
        maxDistance = 1500;
        radius = 10;
    }

    public GrenadeAmmo(ShapeRenderer renderer, SpriteBatch b, Camera c, ProjectileLaunchedPacket packet)
    {

        drawer = renderer;

        damage = packet.damage;
        speed = packet.speed;
        maxDistance = packet.maxDistance;
        damage = packet.damage;
        origin = packet.start.copy();
        currentPosition = packet.start.copy();
        destination = packet.end;
        shooterId = packet.shooterId;
        scatter = packet.scatter;
        projectileType = ProjectileType.FRAG_GRENADE;
        radius = 10;

        applyScatterAngle();
        if (isServer)
        {
            return;
        }


        explosion = particlesPoolManager.obtainParticleEffect(StringConstants.GRENADE_EXPLOSION_EFFECT);


        this.sprite = GameClient.cellAtlas.createSprite(FRAG_GRENADE);

        batch = b;
        camera = c;
    }

    public void assignFromPacket(ShapeRenderer renderer, SpriteBatch b, Camera c, ProjectileLaunchedPacket packet, ParticlesPoolManager particlesPoolManager)
    {

        this.particlesPoolManager = particlesPoolManager;
        drawer = renderer;

        damage = packet.damage;
        speed = packet.speed;

        maxDistance = packet.maxDistance;
        damage = packet.damage;
        origin = packet.start.copy();
        currentPosition = packet.start.copy();
        destination = packet.end;
        shooterId = packet.shooterId;
        scatter = packet.scatter;
        projectileType = ProjectileType.FRAG_GRENADE;
        radius = 10;
        speed = (float) (speed * destination.distance(origin) / maxDistance);
        //System.out.println("speed  " + speed);

        applyScatterAngle();
        if (isServer)
        {
            return;
        }

        // trailEffect = new ParticleEffect();
//        trailEffect.load(Gdx.files.internal("particles/fireTrail.p"),Gdx.files.internal("particles"));
//        trailEffect.setPosition(currentPosition.getX(),currentPosition.getY());
//        trailEffect.start();

        explosion = particlesPoolManager.obtainParticleEffect(StringConstants.GRENADE_EXPLOSION_EFFECT);
        this.sprite = GameClient.cellAtlas.createSprite(FRAG_GRENADE);
        sprite.setOrigin(sprite.getWidth() / 2, sprite.getHeight() / 2 - 5);

        batch = b;
        camera = c;
        GameClient.getGame().getSoundManager().playSoundWithPanAndVolume(SoundManager.GRENADE_THROW, new IoPoint(camera.position.x, camera.position.y), currentPosition);
        //GameClient.getGame().getSoundManager().playSoundWithPanAndVolume(SoundManager.DEATH, new IoPoint(camera.position.x, camera.position.y),  currentPosition);
    }


    @Override
    public void applyScatterAngle()
    {

        Vector2 v = destination.toVector2().sub(origin.toVector2());

        destination = origin.add(v);

    }

    @Override
    public ProjectileLaunchedPacket launch(NetworkManager networkManager, IoPoint start, IoPoint end, String shooterId, float scatter)
    {
        this.origin = start;
        this.destination = end;
        this.currentPosition = start.copy();
        ProjectileLaunchedPacket res = new ProjectileLaunchedPacket(origin, destination, speed, damage, maxDistance, projectileType, shooterId, this.scatter);
        networkManager.sendPacket(res);

        return res;
    }

    @Override
    public void drawTrail()
    {
//         drawer.set(ShapeRenderer.ShapeType.Filled);
        if (isShowingSplash)
        {
            return;
        }
//        drawer.setColor(Color.BLACK);
//
//        drawer.rectLine(currentPosition.getX(), currentPosition.getY(), origin.x ,origin.y , 5);
    }

    @Override
    public void update(float deltaTime)
    {
        if (!reachedDestination())
        {
            double distance = destination.distance(currentPosition);

            Vector2 forward = new Vector2(destination.getX() - currentPosition.getX(), destination.getY() - currentPosition.getY());
            forward.scl((float) (speed / distance) * deltaTime);

            currentPosition.x += forward.x;
            currentPosition.y += forward.y;

            if (isLaunch)
            {
                initialDistance = distance;
                isLaunch = false;
            }
            flyEffect = getflyEffect(distance);

        }

        //IoLogger.log("forward = " + forward);
        //IoLogger.log("currentPosition = " + currentPosition);

        if (isServer)
        {
            return;
        }

        //trailEffect.setPosition(currentPosition.getX(),currentPosition.getY());
        //trailEffect.update(deltaTime);

    }

    @Override
    public void render(float deltaTime)
    {

        if (!isShowingSplash)
        {


            sprite.draw(batch);
            if (reachedDestination())
            {
                sprite.rotate(10);
            }
            else
            {
                sprite.rotate(5);
            }
            sprite.setPosition(currentPosition.getX(), currentPosition.getY());
            sprite.setScale(flyEffect);
        }

        if (isShowingSplash && explosion != null && !explosion.isComplete())
        {

            explosion.draw(batch, deltaTime);

        }


    }

    public boolean canBeRemoved()
    {
        return isServer || explosion.isComplete();
    }

    @Override
    public boolean canBeDeleted()
    {
        if (getReachedDestinationTime() != 0 && System.currentTimeMillis() - getReachedDestinationTime() > TIME_TO_GRENADE_EXPLODE)
        {
            return true;
        }
        return false;
    }


    public void soundAndSplash()
    {
        if (isShowingSplash)
        {
            return;
        }
        {

            explosion.setPosition(currentPosition.x, currentPosition.y);
            explosion.start();
            GameClient.getGame().getSoundManager().playSoundWithPanAndVolume(SoundManager.EXPLOSION1, new IoPoint(camera.position.x, camera.position.y), currentPosition);
        }
    }

    @Override
    public void reset()
    {
        super.reset();
    }

    @Override
    public boolean hasSplashDamage()
    {
        return true;
    }

    @Override
    public int getSplashRadius()
    {
        return splashRadius;
    }

    @Override
    public ArrayList<PlayerCell> applySplashDamage(Collection<PlayerCell> playerCellList, Collection<MapEntityCell> nearestEntities, TileType[][] nearestTiles)
    {
        ArrayList<PlayerCell> damagedPlayers = new ArrayList<PlayerCell>();

        for (PlayerCell player : playerCellList)
        {
            boolean isHereObstacles = false;
            IoPoint playerPos = player.getPosition();
            IoPoint grenade = getCurrentPosition();
            Vector2 vector2 = new Vector2(grenade.getX() - playerPos.getX(), grenade.getY() - playerPos.getY());
            float angle = vector2.angle();
            //IoLogger.log("Angle: " + angle);
            for (int i = 10; i < playerPos.distance(grenade); i++)
            {
                vector2.setLength(i);
                //IoLogger.log("Vector angle: " + vector2.angle());
                PlayerCell nextStep = new PlayerCell();
                nextStep.setX(vector2.x + player.getX());
                nextStep.setY(vector2.y + player.getY());
                nextStep.setCollisionRadius(10);
                for (MapEntityCell entityCell : nearestEntities)
                {
                    if (entityCell.isObstacle() && entityCell.getType() != MapEntityType.PIT && entityCell.collides(nextStep)
                            || entityCell.collides(nextStep) && entityCell.getType() == MapEntityType.TACTICAL_SHIELD)
                    {
                        IoLogger.log("HERE ENTITY");
                        isHereObstacles = true;
                        break;
                    }
                }

                if (!isHereObstacles)
                {
                    TileDetails t = getTile(nextStep.x, nextStep.y, nearestTiles);
                    if (t == null || t.isObstacle())
                    {
                        IoLogger.log("HERE TILE OBSTACLE");
                        isHereObstacles = true;
                    }
                }
                if (isHereObstacles)
                {
                    break;
                }
            }


            if (!isHereObstacles)
            {
                double dist = player.getPosition().distance(currentPosition);

                if (dist < splashRadius)
                {
                    if (player.getCurrentArmor() <= PLAYERS_MIN_ARMOR)
                    {
                        player.setCurrentHealth((int) (player.getCurrentHealth() - damage * (1 - dist / splashRadius)));
                        damagedPlayers.add(player);
                    }
                    else
                    {
                        if ((damage * (1 - dist / splashRadius)) <= player.getCurrentArmor())
                        {
                            player.setCurrentArmor(player.getCurrentArmor() - (int) ((damage * (1 - dist / splashRadius))));
                            damagedPlayers.add(player);
                        }
                        else
                        {
                            int differentDamage = ((int) ((damage * (1 - dist / splashRadius)) - player.getCurrentArmor()));
                            player.setCurrentArmor(PLAYERS_MIN_ARMOR);
                            player.setCurrentHealth(player.getCurrentHealth() - differentDamage);
                            damagedPlayers.add(player);

                        }
                    }
                }
            }

            /*if (player.getCurrentArmor() <= PLAYERS_MIN_ARMOR){
                if(dist < splashRadius) {
                    player.setCurrentHealth((int) (player.getCurrentHealth() - damage * (1 - dist / splashRadius)));
                    damagedPlayers.add(player);
                }
            }else {
                if(dist < splashRadius && (damage*(1- dist/splashRadius)) <= player.getCurrentArmor()){
                    player.setCurrentArmor(player.getCurrentArmor() -(int) ((damage*(1- dist/splashRadius))));
                    damagedPlayers.add(player);
                }else {
                    int differentDamage =  ((int) ((damage*(1- dist/splashRadius)) - player.getCurrentArmor()));
                    player.setCurrentArmor(PLAYERS_MIN_ARMOR);
                    player.setCurrentHealth(player.getCurrentHealth() - differentDamage);
                    damagedPlayers.add(player);

                }
            }*/

        }
        return damagedPlayers;
    }

    public TileDetails getTile(float x, float y, TileType[][] tiles)
    {

        int tileCountY = tiles.length;
        int tileCountX = tiles[0].length;
        int i = (int) (y / TILE_HEIGHT);
        int j = (int) (x / TILE_WIDTH);

        if (i < 0 || j < 0)
        {
            return null;
        }
        if (i >= tileCountY || j >= tileCountX)
        {
            return null;
        }
        return GameConstants.TILE_DETAILS.get(tiles[i][j]);
    }


    @Override
    public void applyDamageToPit(Collection<PlayerCell> playerCellList, MapEntityCell c)
    {
        for (PlayerCell player : playerCellList)
        {
            if (player.pit != null && player.pit.equals(c.getId()))
            {
                double dist = player.getPosition().distance(currentPosition);
                if (dist < splashRadius)
                {
                    if (player.getCurrentArmor() <= PLAYERS_MIN_ARMOR)
                    {
                        player.setCurrentHealth((int) (player.getCurrentHealth() - damage * (1 - dist / splashRadius)));
                    }
                    else
                    {
                        player.setCurrentArmor((int) (player.getCurrentArmor() - damage * (1 - dist / splashRadius)));
                    }
                }


            }
        }
    }

    public float getflyEffect(double dist)
    {
        double x = (initialDistance - dist) / initialDistance;
        if (x > 0 && x <= 0.1 || x > 0.9 && x < 1)
        {
            return 1.1f;
        }
        if (x > 0.1 && x <= 0.2 || x > 0.8 && x <= 0.9)
        {
            return 1.5f;
        }
        if (x > 0.2 && x <= 0.3 || x > 0.7 && x <= 0.8)
        {
            return 2f;
        }
        if (x > 0.3 && x <= 0.4 || x > 0.6 && x <= 0.7)
        {
            return 2.5f;
        }
        if (x > 0.4 && x <= 0.5 || x > 0.5 && x <= 0.6)
        {
            return 3f;
        }
        if (x == 0.5)
        {
            return 4f;
        }
        /*double x, delta_y, y_max, y_current;
        x = (float) ( - dist);
        x = 1500;
        y_max = 500;
        y_current = - (0.0009) * x * x + 0.668 * x;
        delta_y = y_max - y_current;
        return (float) (1 + delta_y / y_max);*/

        return 1;
    }
}
