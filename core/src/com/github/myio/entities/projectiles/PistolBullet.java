package com.github.myio.entities.projectiles;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Pool;
import com.badlogic.gdx.utils.Timer;
import com.github.myio.GameClient;
import com.github.myio.StringConstants;
import com.github.myio.entities.IoPoint;
import com.github.myio.enums.ProjectileType;
import com.github.myio.networking.NetworkManager;
import com.github.myio.networking.packet.ProjectileLaunchedPacket;
import com.github.myio.tools.SoundManager;

import static com.github.myio.GameConstants.isServer;

/**
 * Created by taras on 05.03.2018.
 */

public class PistolBullet extends AbstractProjectile implements Pool.Poolable {
    Sprite sprite;
    Vector2 forward;
    SpriteBatch batch;
    ParticlesPoolManager particlesPoolManager;

    public PistolBullet() {
    }


    public PistolBullet(ShapeRenderer renderer) {

        drawer = renderer;
        damage = 20;
        projectileType = ProjectileType.PISTOL_BULLET;
        speed = 4000;
        maxDistance = 1500;
        radius = 10;
    }

    public PistolBullet(ShapeRenderer renderer, SpriteBatch b, Camera c, ProjectileLaunchedPacket packet) {

        drawer = renderer;

        damage = packet.damage;
        speed = packet.speed;
        maxDistance = packet.maxDistance;
        damage = packet.damage;
        origin = packet.start.copy();;
        currentPosition = packet.start.copy();
        destination = packet.end;
        shooterId = packet.shooterId;
        scatter = packet.scatter;
        projectileType = ProjectileType.PISTOL_BULLET;
        radius = 10;

        applyScatterAngle();
        if(isServer) return;

        trailEffect = particlesPoolManager.obtainParticleEffect(StringConstants.PISTOL_EFFECT);
        //trailEffect.load(Gdx.files.internal("particles/shot.p"),Gdx.files.internal("particles"));
        trailEffect.setPosition(currentPosition.getX(),currentPosition.getY());
        trailEffect.start();

        this.sprite = new Sprite(new Texture(Gdx.files.internal("atlas/patrons/9_MM.png")));
      //  bloodEffect = new ParticleEffect();
    //    bloodEffect.load(Gdx.files.internal("particles/bloodSplash2.p"),Gdx.files.internal("particles"));


        batch = b;
        camera = c;
        GameClient.getGame().getSoundManager().playSoundWithPanAndVolume(SoundManager.PISTOL, new IoPoint(camera.position.x, camera.position.y),  currentPosition);
        Timer.schedule(new Timer.Task() {
            @Override
            public void run() {
                GameClient.getGame().getSoundManager().playSoundWithPanAndVolume(SoundManager.SHELL, new IoPoint(camera.position.x, camera.position.y),  currentPosition);
            }
        }, 0.25f);
    }
    public void assignFromPacket(ShapeRenderer renderer, SpriteBatch b, Camera c, ProjectileLaunchedPacket packet,ParticlesPoolManager particlesPoolManager) {

        this.particlesPoolManager = particlesPoolManager;
        drawer = renderer;

        damage = packet.damage;
        speed = packet.speed;
        maxDistance = packet.maxDistance;
        damage = packet.damage;
        origin = packet.start.copy();;
        currentPosition = packet.start.copy();
        destination = packet.end;
        shooterId = packet.shooterId;
        scatter = packet.scatter;
        projectileType = ProjectileType.PISTOL_BULLET;
        radius = 10;

        applyScatterAngle();
        if(isServer) return;


        trailEffect = particlesPoolManager.obtainParticleEffect(StringConstants.PISTOL_EFFECT);

//        trailEffect = new ParticleEffect();
//        trailEffect.load(Gdx.files.internal("particles/shot.p"),Gdx.files.internal("particles"));
        trailEffect.setPosition(currentPosition.getX(),currentPosition.getY());
        trailEffect.start();

        this.sprite = new Sprite(new Texture(Gdx.files.internal("atlas/patrons/9_MM.png")));
        sprite.setSize(8,22);
        sprite.setOrigin(sprite.getWidth()/2, sprite.getHeight()/2);

        // explosion = new ParticleEffect();
        // explosion.load(Gdx.files.internal("particles/salut11.p"),Gdx.files.internal("particles"));

        batch = b;
        camera = c;
        GameClient.getGame().getSoundManager().playSoundWithPanAndVolume(SoundManager.PISTOL, new IoPoint(camera.position.x, camera.position.y),  currentPosition);
        Timer.schedule(new Timer.Task() {
            @Override
            public void run() {
                GameClient.getGame().getSoundManager().playSoundWithPanAndVolume(SoundManager.SHELL, new IoPoint(camera.position.x, camera.position.y),  currentPosition);
            }
        }, 0.25f);
    }


    @Override
    public void applyScatterAngle() {

        Vector2 v = destination.toVector2().sub(origin.toVector2());
       // v.rotate(scatter);

        destination = origin.add(v);

    }

    @Override
    public ProjectileLaunchedPacket launch(NetworkManager networkManager, IoPoint start, IoPoint end, String shooterId, float scatter) {
        this.origin = start;
        this.destination = end;
        this.currentPosition = start.copy();
        this.scatter = GameClient.nextFloat(-5,5);

        ProjectileLaunchedPacket res = new ProjectileLaunchedPacket(origin,destination, speed,damage,maxDistance, projectileType,shooterId, this.scatter);

        networkManager.sendPacket(res);

        return res;
    }

    @Override
    public void drawTrail() {

    }

    @Override
    public void update(float deltaTime) {
        double distance = destination.distance(currentPosition);

        forward = new Vector2(destination.getX() - currentPosition.getX(), destination.getY() - currentPosition.getY());
        forward.scl((float) (speed/distance)*deltaTime);

        currentPosition.x += forward.x;
        currentPosition.y += forward.y;

        //IoLogger.log("forward = " + forward);
        //IoLogger.log("currentPosition = " + currentPosition);

        if(isServer) return;

        trailEffect.setPosition(currentPosition.getX(),currentPosition.getY());
        trailEffect.update(deltaTime);
    }

    @Override
    public void render(float deltaTime) {
// drawer.begin();
        if(!isShowingSplash) {
            trailEffect.draw(batch, deltaTime);
            sprite.draw(batch);

            sprite.setPosition(currentPosition.getX() - sprite.getWidth()/2, currentPosition.getY()-sprite.getHeight()/2);
            sprite.setRotation(forward.angle()+270);


//            //   batch.begin();
//            trailEffect.draw(batch, deltaTime);
//            //   batch.end();
//            drawer.set(ShapeRenderer.ShapeType.Filled);
//            drawer.setColor(Color.BLACK);
//            drawer.circle(currentPosition.getX(), currentPosition.getY(), radius);
        }

        if(isShowingSplash && explosion != null && !explosion.isComplete())
        {
            //   batch.begin();
            //  pe.update(deltaTime);
            explosion.draw(batch, deltaTime);
            //    batch.end();
        }

        //  drawer.end();
    }

    @Override
    public boolean canBeRemoved() {
        return true;
    }

    @Override
    public void soundAndSplash() {

    }






    @Override
    public void reset() {
        super.reset();
    }
}
