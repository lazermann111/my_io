package com.github.myio.entities.projectiles;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.ParticleEffectPool;
import com.badlogic.gdx.utils.Array;
import com.github.myio.StringConstants;

/**
 * Created by taras on 15.03.2018.
 */

public class ParticlesPoolManager {

    private ParticleEffect particleKnife;  // todo Texture pool
    private ParticleEffect particleRocket;  // todo Texture pool
    private ParticleEffect particleBullet;
    //
    //private ParticleEffect particleGrenadeAmmo; // todo Texture pool
    private ParticleEffect particleShotgunAmmo;
    private ParticleEffect particlePistol;

    private ParticleEffect particleDeath;

    private ParticleEffect particleExplosionGrenade;
    private ParticleEffect particleExplosionRocket;
    private ParticleEffect particleGrenadeLauncherTail;
    private ParticleEffect particleGrenadeLauncherExplosion;

    private ParticleEffectPool poolKnife;
    private ParticleEffectPool poolRocket;
    private ParticleEffectPool poolBullet;
    private ParticleEffectPool poolShotgunAmmo;
    private ParticleEffectPool poolPistol;
    private ParticleEffectPool poolExplosionGrenade;
    private ParticleEffectPool poolExplosionRocket;
    private ParticleEffectPool poolGrenadeLauncherTail;
    private ParticleEffectPool poolGrenadeLauncherExplosion;

    private ParticleEffectPool poolDeath;

    private Array<ParticleEffectPool.PooledEffect> effects;


    public ParticlesPoolManager() {

        particleKnife = new ParticleEffect();
        particleKnife.load(Gdx.files.internal("particles/knife"),Gdx.files.internal("particles"));

        particleRocket = new ParticleEffect();
        particleRocket.load(Gdx.files.internal("particles/rocket_flight.p"),Gdx.files.internal("particles")); // TODO place for снаряд

        particleBullet = new ParticleEffect();
        particleBullet.load(Gdx.files.internal("particles/shot.p"),Gdx.files.internal("particles"));


        particleShotgunAmmo = new ParticleEffect();
        particleShotgunAmmo.load(Gdx.files.internal("particles/shot.p"),Gdx.files.internal("particles"));


        particlePistol = new ParticleEffect();
        particlePistol.load(Gdx.files.internal("particles/shot.p"),Gdx.files.internal("particles"));


        particleExplosionGrenade = new ParticleEffect();
        particleExplosionGrenade.load(Gdx.files.internal("particles/grenadeExplosion_1"),Gdx.files.internal("particles"));

        particleExplosionRocket = new ParticleEffect();
        particleExplosionRocket.load(Gdx.files.internal("particles/BangRoket4.p"),Gdx.files.internal("particles")); //TODO this place for Explosion effect path

        poolKnife = new ParticleEffectPool(particleKnife, 100, 200);//TODO change this parametr
        particleDeath = new ParticleEffect();
        particleDeath.load(Gdx.files.internal("particles/DeathBlowRed.p"),Gdx.files.internal("particles"));

        particleGrenadeLauncherTail = new ParticleEffect();
       particleGrenadeLauncherTail.load(Gdx.files.internal("particles/grenadeLauncherTail.p"),Gdx.files.internal("particles")); //TODO this place for Explosion effect path

        particleGrenadeLauncherExplosion = new ParticleEffect();
        particleGrenadeLauncherExplosion.load(Gdx.files.internal("particles/grenadeLauncherExplosion.p"),Gdx.files.internal("particles")); //TODO this place for Explosion effect path


        //particleDeath.load();

        poolRocket = new ParticleEffectPool(particleRocket, 100, 200);
        poolRocket = new ParticleEffectPool(particleRocket, 100, 200);//TODO change this parametr
        poolBullet = new ParticleEffectPool(particleBullet, 100, 200);
        poolShotgunAmmo = new ParticleEffectPool(particleShotgunAmmo, 100, 200);
        poolPistol = new ParticleEffectPool(particlePistol, 100, 200);

        poolExplosionGrenade = new ParticleEffectPool(particleExplosionGrenade, 100, 200);
        poolExplosionRocket = new ParticleEffectPool(particleExplosionRocket, 100, 200);
        poolGrenadeLauncherTail = new ParticleEffectPool(particleGrenadeLauncherTail,100,200);
        poolGrenadeLauncherExplosion = new ParticleEffectPool(particleGrenadeLauncherExplosion,100,200);

        poolDeath = new ParticleEffectPool(particleDeath,1,10);

    }

    public void freeParticleEffect (ParticleEffectPool.PooledEffect p){
     if(p!=null)  p.free();
    }


    public ParticleEffectPool.PooledEffect obtainParticleEffect (int poolEffect){

        switch (poolEffect){
            case StringConstants.KNIFE_EFFECT:
                return poolKnife.obtain();

            case StringConstants.ROCKET_EFFECT:
                return poolRocket.obtain();

            case StringConstants.BULLET_EFFECT:
                return poolBullet.obtain();

            case StringConstants.SHOTGUNAMMO_EFFECT:
                return poolShotgunAmmo.obtain();

            case StringConstants.PISTOL_EFFECT:
                return poolPistol.obtain();

            case StringConstants.GRENADE_EXPLOSION_EFFECT:
                return poolExplosionGrenade.obtain();

            case StringConstants.ROCKET_EXPLOSION_EFFECT:
                return poolExplosionRocket.obtain();

            case StringConstants.DEATH_EFFECT:
                return poolDeath.obtain();

            case StringConstants.GRENADE_LAUNCHER_EFFECT:
                return poolGrenadeLauncherTail.obtain();

            case StringConstants.GRENADE_LAUNCHER_EXPLOSION_EFFECT:
                return poolGrenadeLauncherExplosion.obtain();
        }


        return null;
    }

}
