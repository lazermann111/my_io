package com.github.myio.entities.projectiles;

import com.badlogic.gdx.utils.Pool;
import com.github.myio.GameClient;
import com.github.myio.networking.packet.ProjectileLaunchedPacket;
import com.github.myio.networking.packet.ShotgunShotPacket;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by taras on 14.03.2018.
 */

public class ProjectileManager {

    GameClient game;
    ParticlesPoolManager particlesPoolManager;

    public ProjectileManager(GameClient game,ParticlesPoolManager particlesPoolManager) {
        this.game = game;
        this.particlesPoolManager = particlesPoolManager;
    }



    Pool<MachineGunBullet> bulletPool = new Pool<MachineGunBullet>() {
        @Override
        protected MachineGunBullet newObject() {
            return new MachineGunBullet();
        }
    };
    Pool<AK47Bullet> AK47BulletPool = new Pool<AK47Bullet>() {
        @Override
        protected AK47Bullet newObject() {
            return new AK47Bullet();
        }
    };

    Pool<GrenadeAmmo> grenadeAmmoPool = new Pool<GrenadeAmmo>() {
        @Override
        protected GrenadeAmmo newObject() {
            return new GrenadeAmmo();
        }
    };
    Pool<GrenadeLauncherAmmo> grenadeLauncherAmmoPool = new Pool<GrenadeLauncherAmmo>() {
        @Override
        protected GrenadeLauncherAmmo newObject() {
            return new GrenadeLauncherAmmo();
        }
    };


    Pool<FlashbangAmmo> flashBangPool = new Pool<FlashbangAmmo>() {
        @Override
        protected FlashbangAmmo newObject() {
            return new FlashbangAmmo();
        }
    };

    Pool<PistolBullet> pistolPool =  new Pool<PistolBullet>(){
        @Override
        protected PistolBullet newObject() {
            return new PistolBullet();
        }
    };

    Pool<Rocket> rocketPool = new Pool<Rocket>() {
        @Override
        protected Rocket newObject() {
            return new Rocket();
        }
    };
    Pool<ShotgunAmmo> shotgunAmmoPool= new Pool<ShotgunAmmo>() {
        @Override
        protected ShotgunAmmo newObject() {
            return new ShotgunAmmo();
        }
    };

    Pool<Knife> knifePool = new Pool<Knife>() {
        @Override
        protected Knife newObject() {
            return new Knife();
        }
    };


    public void freeProjectile(AbstractProjectile projectile)
    {
        switch (projectile.projectileType) {
            case ROCKET:
                rocketPool.free((Rocket) projectile);
                break;

            case MACHINEGUN_BULLET:
                bulletPool.free((MachineGunBullet) projectile);
                break;

            case SHOTGUN:
                shotgunAmmoPool.free((ShotgunAmmo) projectile);
                break;

            case PISTOL_BULLET:
                pistolPool.free((PistolBullet) projectile);
                break;

            case FRAG_GRENADE:
                grenadeAmmoPool.free((GrenadeAmmo) projectile);
                break;

            case FLASHBANG:
                flashBangPool.free((FlashbangAmmo) projectile);
                break;

            case KNIFE:
                knifePool.free((Knife) projectile);
                break;

            case GRENADE_LAUNCHER_AMMO:
                grenadeLauncherAmmoPool.free((GrenadeLauncherAmmo) projectile);
                break;

            case AK47_BULLET:
                AK47BulletPool.free((AK47Bullet) projectile);
                break;
        }
    }

    public List<AbstractProjectile> obtainProjectile(ProjectileLaunchedPacket packet)
    {
        List<AbstractProjectile> res = new ArrayList<AbstractProjectile>();
        switch (packet.type) {
            case ROCKET:
                Rocket rocket = rocketPool.obtain();
                rocket.assignFromPacket(game.getGameShapeRenderer(), game.getBatch(), game.getCamera(), packet,particlesPoolManager);
                //IoLogger.log("ProjectileManager ", " rock free " + rocketPool.getFree());
                res.add(rocket);
                return res;

            case MACHINEGUN_BULLET:
                MachineGunBullet bullet = bulletPool.obtain();
                bullet.assignFromPacket(game.getGameShapeRenderer(), game.getBatch(), game.getCamera(), packet,particlesPoolManager);
                //IoLogger.log("ProjectileManager ", " bullet free " + bulletPool.getFree());
                res.add(bullet);
                return res;

            case SHOTGUN:

                ShotgunShotPacket shotPacket = (ShotgunShotPacket) packet;
                boolean soundPlayed = false;
                for (float a : shotPacket.scatterAngles)
                {
                    ShotgunAmmo shotgunAmmo = shotgunAmmoPool.obtain();
                    packet.scatter = a;
                    shotgunAmmo.assignFromPacket(game.getGameShapeRenderer(), game.getBatch(), game.getCamera(), packet ,particlesPoolManager, !soundPlayed);
                    soundPlayed = true;

                    res.add(shotgunAmmo);
                }

                //IoLogger.log("ProjectileManager ", " shotgun free " + shotgunAmmoPool.getFree());
                return res;

            case PISTOL_BULLET:
                PistolBullet pistolBullet = pistolPool.obtain();
                pistolBullet.assignFromPacket(game.getGameShapeRenderer(), game.getBatch(), game.getCamera(), packet,particlesPoolManager);
                //IoLogger.log("ProjectileManager ", " pistolBullet free " + pistolPool.getFree());
                res.add(pistolBullet);
                return res;

            case FRAG_GRENADE:
                GrenadeAmmo grenadeAmmo = grenadeAmmoPool.obtain();
                grenadeAmmo.assignFromPacket(game.getGameShapeRenderer(), game.getBatch(), game.getCamera(), packet,particlesPoolManager);
                //IoLogger.log("ProjectileManager ", " grenade free " + grenadeAmmoPool.getFree());
                res.add(grenadeAmmo);
                return res;

            case FLASHBANG:
                FlashbangAmmo flashBang = flashBangPool.obtain();
                flashBang.assignFromPacket(game.getGameShapeRenderer(), game.getBatch(), game.getCamera(), packet,particlesPoolManager);
                res.add(flashBang);
                return res;

            case KNIFE:
                Knife knife = knifePool.obtain();
                knife.assignFromPacket(game.getGameShapeRenderer(), game.getBatch(), game.getCamera(), packet,particlesPoolManager);
                res.add(knife);
                return res;

            case GRENADE_LAUNCHER_AMMO:
                GrenadeLauncherAmmo grenadeLauncherAmmo = grenadeLauncherAmmoPool.obtain();
                grenadeLauncherAmmo.assignFromPacket(game.getGameShapeRenderer(), game.getBatch(), game.getCamera(), packet,particlesPoolManager);
                //IoLogger.log("ProjectileManager ", " grenade free " + grenadeAmmoPool.getFree());
                res.add(grenadeLauncherAmmo);
                return res;

            case AK47_BULLET:
                AK47Bullet AKBullet = AK47BulletPool.obtain();
                AKBullet.assignFromPacket(game.getGameShapeRenderer(), game.getBatch(), game.getCamera(), packet,particlesPoolManager);
                //IoLogger.log("ProjectileManager ", " bullet free " + bulletPool.getFree());
                res.add(AKBullet);
                return res;
        }
        return null;
    }
}
