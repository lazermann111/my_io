package com.github.myio.entities.projectiles;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Pool;
import com.badlogic.gdx.utils.Timer;
import com.github.myio.GameClient;
import com.github.myio.StringConstants;
import com.github.myio.entities.IoPoint;
import com.github.myio.enums.ProjectileType;
import com.github.myio.networking.NetworkManager;
import com.github.myio.networking.packet.ProjectileLaunchedPacket;
import com.github.myio.tools.SoundManager;

import static com.github.myio.GameConstants.isServer;



public class AK47Bullet extends AbstractProjectile implements Pool.Poolable {
    Sprite sprite;
    double initialDistance = 0;
    boolean isLaunch = true;

    private ParticlesPoolManager particlesPoolManager;
    public AK47Bullet() {
    }

    public AK47Bullet(ShapeRenderer renderer) {

        drawer = renderer;
        damage = 15;
        projectileType = ProjectileType.AK47_BULLET;
        speed = 4000;
        maxDistance = 2000;
        radius = 10;
    }

    public AK47Bullet(ShapeRenderer renderer, SpriteBatch b, Camera c, ProjectileLaunchedPacket packet) {

        drawer = renderer;

        damage = packet.damage;
        speed = packet.speed;
        maxDistance = packet.maxDistance;
        damage = packet.damage;
        origin = packet.start.copy();;
        currentPosition = packet.start.copy();
        destination = packet.end;
        shooterId = packet.shooterId;
        scatter = packet.scatter;
        projectileType = ProjectileType.AK47_BULLET;
        radius = 10;

        applyScatterAngle();
        if(isServer) return;

        trailEffect = particlesPoolManager.obtainParticleEffect(StringConstants.BULLET_EFFECT);
        // trailEffect.load(Gdx.files.internal("particles/shot.p"),Gdx.files.internal("particles"));
        trailEffect.setPosition(currentPosition.getX(),currentPosition.getY());
        trailEffect.start();

        this.sprite = new Sprite(new Texture(Gdx.files.internal("atlas/patrons/7x62.png")));
        sprite.setOrigin(sprite.getWidth()/2, sprite.getHeight()/2 );

        // explosion = new ParticleEffect();
        // explosion.load(Gdx.files.internal("particles/salut11.p"),Gdx.files.internal("particles"));

        batch = b;
        camera = c;
        GameClient.getGame().getSoundManager().playSoundWithPanAndVolume(SoundManager.MACHINEGUN, new IoPoint(camera.position.x, camera.position.y),  currentPosition);
        Timer.schedule(new Timer.Task() {
            @Override
            public void run() {
                GameClient.getGame().getSoundManager().playSoundWithPanAndVolume(SoundManager.SHELL, new IoPoint(camera.position.x, camera.position.y),  currentPosition);
            }
        }, 0.25f);
    }

    public void assignFromPacket (ShapeRenderer renderer, SpriteBatch b, Camera c, ProjectileLaunchedPacket packet,ParticlesPoolManager particlesPoolManager) {

        this.particlesPoolManager = particlesPoolManager;
        drawer = renderer;

        damage = packet.damage;
        speed = packet.speed;
        maxDistance = packet.maxDistance;
        damage = packet.damage;
        origin = packet.start.copy();;
        currentPosition = packet.start.copy();
        destination = packet.end;
        shooterId = packet.shooterId;
        scatter = packet.scatter;
        projectileType = ProjectileType.AK47_BULLET;
        radius = 10;

        applyScatterAngle();
        if(isServer) return;


        trailEffect = particlesPoolManager.obtainParticleEffect(StringConstants.BULLET_EFFECT);

        trailEffect.setPosition(currentPosition.getX(),currentPosition.getY());
        trailEffect.start();

        this.sprite = new Sprite(new Texture(Gdx.files.internal("atlas/patrons/7x62.png")));
        sprite.setSize(11,40);

        sprite.setOrigin(sprite.getWidth()/2, sprite.getHeight()/2);
//        sprite.setSize(11,40);
        // explosion = new ParticleEffect();
        // explosion.load(Gdx.files.internal("particles/salut11.p"),Gdx.files.internal("particles"));

        batch = b;
        camera = c;

        GameClient.getGame().getSoundManager().playSoundWithPanAndVolume(SoundManager.AK47, new IoPoint(camera.position.x, camera.position.y),currentPosition);
        Timer.schedule(new Timer.Task() {
            @Override
            public void run() {
                GameClient.getGame().getSoundManager().playSoundWithPanAndVolume(SoundManager.SHELL, new IoPoint(camera.position.x, camera.position.y),  currentPosition);
            }
        }, 0.25f);

    }


    @Override
    public void applyScatterAngle() {

        Vector2 v = destination.toVector2().sub(origin.toVector2());
        v.rotate(scatter);

        destination = origin.add(v);

    }

    @Override
    public ProjectileLaunchedPacket launch(NetworkManager networkManager, IoPoint start, IoPoint end, String shooterId, float scatter) {
        this.origin = start;
        this.destination = end;
        this.currentPosition = start.copy();


        ProjectileLaunchedPacket res =new ProjectileLaunchedPacket(origin,destination, speed,damage,maxDistance, projectileType,shooterId, scatter);
        networkManager.sendPacket(res);

        return res;
    }

    @Override
    public void drawTrail() {
        // drawer.set(ShapeRenderer.ShapeType.Filled);
//        if(isShowingSplash) return;
//        drawer.setColor(Color.BLACK);
//
//        drawer.rectLine(currentPosition.getX(), currentPosition.getY(), origin.x ,origin.y , 5);
    }

    Vector2 forward;
    @Override
    public void update(float deltaTime) {

        double distance = destination.distance(currentPosition);

         forward = new Vector2(destination.getX() - currentPosition.getX(), destination.getY() - currentPosition.getY());
        forward.scl((float) (speed/distance)*deltaTime);

        currentPosition.x += forward.x;
        currentPosition.y += forward.y;


        //IoLogger.log("forward = " + forward);
        //IoLogger.log("currentPosition = " + currentPosition);
        if(isLaunch) {
            initialDistance = distance;
            isLaunch = false;
        }
        if(isServer) return;


        trailEffect.setPosition(currentPosition.getX(),currentPosition.getY());
        trailEffect.update(deltaTime);

    }

    @Override
    public void render(float deltaTime) {
        // drawer.begin();
        if(!isShowingSplash)
        {
            //   batch.begin();
            trailEffect.draw(batch, deltaTime);
            //   batch.end();

            sprite.draw(batch);

            sprite.setPosition(currentPosition.getX() - sprite.getWidth()/2, currentPosition.getY()-sprite.getHeight()/2);
//            sprite.setScale(flyEffect);
            sprite.setRotation(forward.angle()+270);

//            drawer.set(ShapeRenderer.ShapeType.Filled);
//            drawer.setColor(Color.BLACK);
//            drawer.circle(currentPosition.getX(), currentPosition.getY(), radius);
        }

        if(isShowingSplash && explosion != null && !explosion.isComplete())
        {
            //   batch.begin();
            //  pe.update(deltaTime);
            explosion.draw(batch, deltaTime);
            //    batch.end();
        }

        //  drawer.end();
    }
    public boolean canBeRemoved()
    {

        return true; // no splash needed
    }


    public void soundAndSplash()
    {
        //if(isShowingSplash) return;
        {
//            explosion.setPosition(currentPosition.x ,currentPosition.y);
            //           explosion.start();
            //GameClient.getGame().getSoundManager().playSoundWithPanAndVolume(SoundManager.EXPLOSION1, new IoPoint(camera.position.x, camera.position.y),  currentPosition );
        }}


    @Override
    public void reset() {

        super.reset();
        damage = 10;
        projectileType = ProjectileType.AK47_BULLET;
        speed = 3000;
        maxDistance = 2000;
        radius = 10;
    }


}
