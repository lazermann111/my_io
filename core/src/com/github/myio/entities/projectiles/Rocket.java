package com.github.myio.entities.projectiles;


import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Pool;
import com.github.myio.GameClient;
import com.github.myio.GameConstants;
import com.github.myio.StringConstants;
import com.github.myio.entities.IoPoint;
import com.github.myio.entities.PlayerCell;
import com.github.myio.entities.map.MapEntityCell;
import com.github.myio.entities.tile.TileDetails;
import com.github.myio.enums.MapEntityType;
import com.github.myio.enums.ProjectileType;
import com.github.myio.enums.TileType;
import com.github.myio.networking.NetworkManager;
import com.github.myio.networking.packet.ProjectileLaunchedPacket;
import com.github.myio.tools.IoLogger;
import com.github.myio.tools.SoundManager;

import java.util.ArrayList;
import java.util.Collection;

import static com.github.myio.GameConstants.PLAYERS_MIN_ARMOR;
import static com.github.myio.GameConstants.TILE_HEIGHT;
import static com.github.myio.GameConstants.TILE_WIDTH;
import static com.github.myio.GameConstants.isServer;

public class Rocket extends AbstractProjectile implements Pool.Poolable {
    Sprite sprite;
    Vector2 forward;
    ParticlesPoolManager particlesPoolManager;
    public Rocket() {
    }
    private int splashRadius = 1700;
    public Rocket(ShapeRenderer renderer) {
        drawer = renderer;
        damage = 110;
        projectileType = ProjectileType.ROCKET;
        speed = 2000;
        maxDistance = 4000;
        radius = 30;
    }

    public Rocket(ShapeRenderer renderer, SpriteBatch b, Camera c, ProjectileLaunchedPacket packet) {

        drawer = renderer;

        damage = packet.damage;
        projectileType = ProjectileType.ROCKET;
        speed = packet.speed;
        maxDistance = packet.maxDistance;
        damage = packet.damage;
        origin = packet.start;
        currentPosition = packet.start.copy();
        shooterId = packet.shooterId;
        destination = packet.end;
        radius = 30;

        if(isServer) return;

        trailEffect = particlesPoolManager.obtainParticleEffect(StringConstants.ROCKET_EFFECT);
//        trailEffect.load(Gdx.files.internal("particles/fireTrail.p"),Gdx.files.internal("particles"));
        trailEffect.setPosition(currentPosition.getX(),currentPosition.getY());
        trailEffect.start();

        explosion = particlesPoolManager.obtainParticleEffect(StringConstants.ROCKET_EXPLOSION_EFFECT);

        this.sprite = new Sprite(new Texture("atlas/patrons/RPG_AMMO.png"));


        batch = b;
        camera = c;
        GameClient.getGame().getSoundManager().playSoundWithPanAndVolume(SoundManager.ROCKET, new IoPoint(camera.position.x, camera.position.y),  currentPosition);
    }



    public void assignFromPacket (ShapeRenderer renderer, SpriteBatch b, Camera c, ProjectileLaunchedPacket packet,ParticlesPoolManager particlesPoolManager) {

        this.particlesPoolManager = particlesPoolManager;

        drawer = renderer;

        damage = packet.damage;
        projectileType = ProjectileType.ROCKET;
        speed = packet.speed;
        maxDistance = packet.maxDistance;
        damage = packet.damage;
        origin = packet.start;
        currentPosition = packet.start.copy();
        shooterId = packet.shooterId;
        destination = packet.end;
        radius = 30;

        if(isServer) return;


        trailEffect = particlesPoolManager.obtainParticleEffect(StringConstants.ROCKET_EFFECT);
//        trailnPAEffect.load(Gdx.files.internal("particles/fireTrail.p"),Gdx.files.internal("particles"));
        trailEffect.setPosition(currentPosition.getX(),currentPosition.getY());
        trailEffect.start();

        explosion = particlesPoolManager.obtainParticleEffect(StringConstants.ROCKET_EXPLOSION_EFFECT);

        this.sprite = new Sprite(new Texture("atlas/patrons/RPG_AMMO.png"));
        sprite.setSize(40,110);
        sprite.setOrigin(sprite.getWidth()/2,sprite.getHeight()/2);

        batch = b;
        camera = c;
        GameClient.getGame().getSoundManager().playSoundWithPanAndVolume(SoundManager.ROCKET, new IoPoint(camera.position.x, camera.position.y),  currentPosition);
    }




    @Override
    public ProjectileLaunchedPacket launch(NetworkManager networkManager, IoPoint start, IoPoint end, String shooterId, float scatter) {
        this.origin = start;
        this.destination = end;
        this.currentPosition = start;

        ProjectileLaunchedPacket res = new ProjectileLaunchedPacket(origin,destination, speed,damage,maxDistance, projectileType,shooterId, this.scatter);
        networkManager.sendPacket(res);

        return res;
    }

    @Override
    public void drawTrail() {
        // drawer.set(ShapeRenderer.ShapeType.Filled);
        if(isShowingSplash) return;

//        drawer.setColor(Color.BLACK);
//        drawer.rectLine(currentPosition.getX(), currentPosition.getY(), origin.x ,origin.y , 20);
    }

    @Override
    public void update(float deltaTime) {

        double distance = destination.distance(currentPosition);

        forward = new Vector2(destination.getX() - currentPosition.getX(), destination.getY() - currentPosition.getY());

        forward.scl((float) (speed/distance)*deltaTime);

        currentPosition.x += forward.x;
        currentPosition.y += forward.y;
        if(isServer) return;

        if(isShowingSplash) return;
        trailEffect.setPosition(currentPosition.getX(),currentPosition.getY());
        trailEffect.update(deltaTime);

    }

    @Override
    public void render(float deltaTime) {
        // drawer.begin();
        // if(!isShowingSplash && !reachedDestination())
        {
            sprite.draw(batch);
            sprite.setPosition(currentPosition.getX() - sprite.getWidth()/2, currentPosition.getY()-sprite.getHeight()/2);
            sprite.setRotation(forward.angle()+270);
            // batch.begin();
            trailEffect.draw(batch, deltaTime);
            //  batch.end();
        }

        /*if(!isShowingSplash)
        {
            drawer.set(ShapeRenderer.ShapeType.Filled);
            drawer.setColor(Color.RED);
            drawer.circle(currentPosition.getX(), currentPosition.getY(), radius);
        }*/

        if(isShowingSplash && explosion != null && !explosion.isComplete())
        {
            // batch.begin();
            //  pe.update(deltaTime);
            explosion.draw(batch, deltaTime);
            //  batch.end();
        }

        //  drawer.end();
    }
    public boolean canBeRemoved()
    {
        return isServer || explosion.isComplete();
    }



    public void soundAndSplash()
    {
        if(isShowingSplash) return;
        {
            explosion.setPosition(currentPosition.x ,currentPosition.y);
            explosion.start();
            GameClient.getGame().getSoundManager().playSoundWithPanAndVolume(SoundManager.EXPLOSION1, new IoPoint(camera.position.x, camera.position.y),  currentPosition );
        }



    }





    @Override
    public void reset() {
        super.reset();
    }

    @Override
    public boolean hasSplashDamage() {
        return true;
    }

    @Override
    public int getSplashRadius() {
        return splashRadius;
    }

    @Override
    public ArrayList<PlayerCell> applySplashDamage(Collection<PlayerCell> playerCellList, Collection<MapEntityCell> nearestEntities, TileType[][] nearestTiles) {
        ArrayList<PlayerCell> damagedPlayers = new ArrayList<PlayerCell>();

        for (PlayerCell player : playerCellList) {
            boolean isHereObstacles = false;
            IoPoint playerPos = player.getPosition();
            IoPoint grenade = getCurrentPosition();
            Vector2 vector2 = new Vector2(grenade.getX() - playerPos.getX(), grenade.getY() - playerPos.getY());
            float angle = vector2.angle();
            //IoLogger.log("Angle: " + angle);
            for (int i = 10; i < playerPos.distance(grenade); i++) {
                vector2.setLength(i);
                //IoLogger.log("Vector angle: " + vector2.angle());
                PlayerCell nextStep = new PlayerCell();
                nextStep.setX(vector2.x + player.getX());
                nextStep.setY(vector2.y + player.getY());
                nextStep.setCollisionRadius(10);
                for (MapEntityCell entityCell : nearestEntities) {
                    if (entityCell.isObstacle() && entityCell.getType() != MapEntityType.PIT && entityCell.collides(nextStep)
                            || entityCell.collides(nextStep) && entityCell.getType() == MapEntityType.TACTICAL_SHIELD) {
                        IoLogger.log("HERE ENTITY");
                        isHereObstacles = true;
                        break;
                    }
                }

                if (!isHereObstacles) {
                    TileDetails t = getTile(nextStep.x, nextStep.y, nearestTiles);
                    if (t == null || t.isObstacle()) {
                        IoLogger.log("HERE TILE OBSTACLE");
                        isHereObstacles = true;
                    }
                }
                if (isHereObstacles)
                    break;
            }


            if (!isHereObstacles) {
                double dist = player.getPosition().distance(currentPosition);

                if (dist < splashRadius) {
                    if (player.getCurrentArmor() <= PLAYERS_MIN_ARMOR) {
                        player.setCurrentHealth((int) (player.getCurrentHealth() - damage * (1 - dist / splashRadius)));
                        damagedPlayers.add(player);
                    } else {
                        if ((damage * (1 - dist / splashRadius)) <= player.getCurrentArmor()) {
                            player.setCurrentArmor(player.getCurrentArmor() - (int) ((damage * (1 - dist / splashRadius))));
                            damagedPlayers.add(player);
                        } else {
                            int differentDamage = ((int) ((damage * (1 - dist / splashRadius)) - player.getCurrentArmor()));
                            player.setCurrentArmor(PLAYERS_MIN_ARMOR);
                            player.setCurrentHealth(player.getCurrentHealth() - differentDamage);
                            damagedPlayers.add(player);

                        }
                    }
                }
            }

            /*if (player.getCurrentArmor() <= PLAYERS_MIN_ARMOR){
                if(dist < splashRadius) {
                    player.setCurrentHealth((int) (player.getCurrentHealth() - damage * (1 - dist / splashRadius)));
                    damagedPlayers.add(player);
                }
            }else {
                if(dist < splashRadius && (damage*(1- dist/splashRadius)) <= player.getCurrentArmor()){
                    player.setCurrentArmor(player.getCurrentArmor() -(int) ((damage*(1- dist/splashRadius))));
                    damagedPlayers.add(player);
                }else {
                    int differentDamage =  ((int) ((damage*(1- dist/splashRadius)) - player.getCurrentArmor()));
                    player.setCurrentArmor(PLAYERS_MIN_ARMOR);
                    player.setCurrentHealth(player.getCurrentHealth() - differentDamage);
                    damagedPlayers.add(player);

                }
            }*/

        }
        return damagedPlayers;
    }

    public TileDetails getTile(float x, float y, TileType[][] tiles)
    {

        int tileCountY = tiles.length;
        int tileCountX = tiles[0].length;
        int i = (int) (y / TILE_HEIGHT);
        int j = (int) (x / TILE_WIDTH);

        if (i < 0 || j < 0){ return null; }
        if (i >= tileCountY || j >= tileCountX){ return null; }
        return GameConstants.TILE_DETAILS.get(tiles[i][j]);
    }
}