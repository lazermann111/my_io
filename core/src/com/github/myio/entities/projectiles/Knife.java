package com.github.myio.entities.projectiles;

import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.utils.Pool;
import com.github.myio.GameClient;
import com.github.myio.StringConstants;
import com.github.myio.entities.IoPoint;
import com.github.myio.enums.ProjectileType;
import com.github.myio.networking.NetworkManager;
import com.github.myio.networking.packet.ProjectileLaunchedPacket;
import com.github.myio.tools.SoundManager;

import static com.github.myio.GameConstants.isServer;

/**
 * Created by taras on 05.03.2018.
 */

public class Knife extends AbstractProjectile implements Pool.Poolable {

    ParticlesPoolManager particlesPoolManager;

    public Knife() {

    }

    public Knife(ShapeRenderer renderer) {
        drawer = renderer;
        damage = 20;
        projectileType = ProjectileType.KNIFE;
        maxDistance = 50;
        speed = 300;
        radius = 10;
    }

    public Knife(ShapeRenderer renderer, SpriteBatch b, Camera c, ProjectileLaunchedPacket packet) {

        drawer = renderer;

        damage = packet.damage;
        speed = packet.speed;
        maxDistance = packet.maxDistance;
        damage = packet.damage;
        origin = packet.start.copy();;
        currentPosition = packet.start.copy();
        destination = packet.end;
        shooterId = packet.shooterId;
        scatter = packet.scatter;
        projectileType = ProjectileType.KNIFE;
        radius = 10;

        if(isServer) return;

        trailEffect = particlesPoolManager.obtainParticleEffect(StringConstants.KNIFE_EFFECT);
        //trailEffect.load(Gdx.files.internal("particles/knife"),Gdx.files.internal("particles"));
        trailEffect.setPosition(currentPosition.getX(),currentPosition.getY());
        trailEffect.start();



        batch = b;
        camera = c;
        GameClient.getGame().getSoundManager().playSoundWithPanAndVolume(SoundManager.KNIFE_WHOOSH, new IoPoint(camera.position.x, camera.position.y),  currentPosition);//TODO: change sound on knife
    }

    public void assignFromPacket(ShapeRenderer renderer, SpriteBatch b, Camera c, ProjectileLaunchedPacket packet,ParticlesPoolManager particlesPoolManager) {

        this.particlesPoolManager = particlesPoolManager;
        drawer = renderer;

        damage = packet.damage;
        speed = packet.speed;
        maxDistance = packet.maxDistance;
        damage = packet.damage;
        origin = packet.start.copy();;
        currentPosition = packet.start.copy();
        destination = packet.end;
        shooterId = packet.shooterId;
        scatter = packet.scatter;
        projectileType = ProjectileType.KNIFE;
        radius = 10;

        //applyScatterAngle();
        if(isServer) return;


        trailEffect = particlesPoolManager.obtainParticleEffect(StringConstants.KNIFE_EFFECT);
        //trailEffect.load(Gdx.files.internal("particles/knife"),Gdx.files.internal("particles"));
        trailEffect.setPosition(currentPosition.getX(),currentPosition.getY());
        trailEffect.start();

//        trailEffect = new ParticleEffect();
//        trailEffect.load(Gdx.files.internal("particles/shot.p"),Gdx.files.internal("particles"));
        trailEffect.setPosition(currentPosition.getX(),currentPosition.getY());
        trailEffect.start();


        // explosion = new ParticleEffect();
        // explosion.load(Gdx.files.internal("particles/salut11.p"),Gdx.files.internal("particles"));

        batch = b;
        camera = c;
        GameClient.getGame().getSoundManager().playSoundWithPanAndVolume(SoundManager.KNIFE_WHOOSH, new IoPoint(camera.position.x, camera.position.y),  currentPosition);//TODO: change sound on knife
    }



    @Override
    //todo  its really very wrong, we shouldnt launch knife projectile
    public ProjectileLaunchedPacket launch(NetworkManager networkManager, IoPoint start, IoPoint end, String shooterId, float scatter) {
        this.origin = end;
        this.destination = end;
        this.currentPosition = end.copy();
        this.scatter = 0;


        ProjectileLaunchedPacket res = new ProjectileLaunchedPacket(origin,destination, speed,damage,maxDistance, projectileType,shooterId, this.scatter);
        networkManager.sendPacket(res);


        return res;
    }

    @Override
    public void drawTrail() {

    }

    @Override
    public void update(float deltaTime) {
        //IoLogger.log("forward = " + forward);
        //IoLogger.log("currentPosition = " + currentPosition);

        if(isServer) return;

        trailEffect.setPosition(currentPosition.getX(),currentPosition.getY());
        trailEffect.update(deltaTime);
    }

    @Override
    public void render(float deltaTime) {
        if(!isShowingSplash)
        {
            //   batch.begin();
            trailEffect.draw(batch, deltaTime);
            //   batch.end();
            /*drawer.set(ShapeRenderer.ShapeType.Filled);
            drawer.setColor(Color.BLACK);
            drawer.circle(currentPosition.getX(), currentPosition.getY(), radius);*/
        }

        if(isShowingSplash && explosion != null && !explosion.isComplete())
        {
            //   batch.begin();
            //  pe.update(deltaTime);
            explosion.draw(batch, deltaTime);
            //    batch.end();
        }
    }

    @Override
    public boolean canBeRemoved() {
        return true;
    }

    @Override
    public void soundAndSplash() {

    }


    @Override
    public void reset() {
        super.reset();
    }
}
