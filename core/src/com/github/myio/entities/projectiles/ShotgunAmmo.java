package com.github.myio.entities.projectiles;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Pool;
import com.badlogic.gdx.utils.Timer;
import com.github.myio.GameClient;
import com.github.myio.StringConstants;
import com.github.myio.entities.IoPoint;
import com.github.myio.enums.ProjectileType;
import com.github.myio.networking.NetworkManager;
import com.github.myio.networking.packet.ProjectileLaunchedPacket;
import com.github.myio.networking.packet.ShotgunShotPacket;
import com.github.myio.tools.SoundManager;

import static com.github.myio.GameConstants.SHOTGUN_PELLETS_AMMOUNT;
import static com.github.myio.GameConstants.isServer;

public class ShotgunAmmo extends AbstractProjectile implements Pool.Poolable{
    Sprite sprite;
    Vector2 forward;
    ParticlesPoolManager particlesPoolManager;
    public ShotgunAmmo() {
    }

    public ShotgunAmmo(ShapeRenderer renderer) {
        drawer = renderer;
        damage = 20;
        projectileType = ProjectileType.SHOTGUN;
        speed = 3000;
        maxDistance = 1500;
        radius = 10;
    }

    public ShotgunAmmo(ShapeRenderer renderer, SpriteBatch b, Camera c, ProjectileLaunchedPacket packet) {

        drawer = renderer;

        damage = packet.damage;
        projectileType = ProjectileType.SHOTGUN;
        speed = packet.speed;
        maxDistance = packet.maxDistance;
        damage = packet.damage;
        origin = packet.start.copy();;
        currentPosition = packet.start.copy();
        scatter = packet.scatter;
        destination = packet.end;
        radius = 10;
        shooterId = packet.shooterId;


        applyScatterAngle();
        if(isServer) return;

        trailEffect = particlesPoolManager.obtainParticleEffect(StringConstants.SHOTGUNAMMO_EFFECT);
        //trailEffect.load(Gdx.files.internal("particles/shot.p"),Gdx.files.internal("particles"));
        trailEffect.setPosition(currentPosition.getX(),currentPosition.getY());
        trailEffect.start();

        this.sprite = new Sprite(new Texture(Gdx.files.internal("atlas/patrons/12_KALL.png")));


        batch = b;
        camera = c;
        GameClient.getGame().getSoundManager().playSoundWithPanAndVolume(SoundManager.SHOTGUN_AMMO, new IoPoint(camera.position.x, camera.position.y),  currentPosition);
        GameClient.getGame().getSoundManager().playSoundWithPanAndVolume(SoundManager.RELOAD, new IoPoint(camera.position.x, camera.position.y),  currentPosition );
    }
    public void assignFromPacket(ShapeRenderer renderer, SpriteBatch b, Camera c, ProjectileLaunchedPacket packet,ParticlesPoolManager particlesPoolManager, boolean playSound) {

        this.particlesPoolManager = particlesPoolManager;
        drawer = renderer;

        damage = packet.damage;
        projectileType = ProjectileType.SHOTGUN;
        speed = packet.speed;
        maxDistance = packet.maxDistance;
        damage = packet.damage;
        origin = packet.start.copy();
        currentPosition = packet.start.copy();
        scatter = packet.scatter;
        destination = packet.end;
        radius = 10;
        shooterId = packet.shooterId;


        applyScatterAngle();
        if(isServer) return;


        trailEffect = particlesPoolManager.obtainParticleEffect(StringConstants.SHOTGUNAMMO_EFFECT);
       // trailEffect.load(Gdx.files.internal("particles/shot.p"),Gdx.files.internal("particles"));
        trailEffect.setPosition(currentPosition.getX(),currentPosition.getY());
        trailEffect.start();

        this.sprite = new Sprite(new Texture(Gdx.files.internal("atlas/patrons/12_KALL.png")));
        sprite.setSize(20,20);
        sprite.setOrigin(sprite.getWidth()/2, sprite.getHeight()/2);

        batch = b;
        camera = c;

        if(!playSound) return;
        GameClient.getGame().getSoundManager().playSoundWithPanAndVolume(SoundManager.SHOTGUN_AMMO, new IoPoint(camera.position.x, camera.position.y),  currentPosition);
        Timer.schedule(new Timer.Task() {
            @Override
            public void run() {
                GameClient.getGame().getSoundManager().playSoundWithPanAndVolume(SoundManager.RELOAD, new IoPoint(camera.position.x, camera.position.y),  currentPosition );
            }
        },0.5f);

    }


    @Override
    public ProjectileLaunchedPacket launch(NetworkManager networkManager, IoPoint start, IoPoint end, String shooterId, float scatter) {
        this.origin = start;
        this.destination = end;
        this.currentPosition =  start.copy();
        this.scatter = GameClient.nextFloat(-15,15);
        float[] scatterAngles = new float[SHOTGUN_PELLETS_AMMOUNT];
        for (int i = 0; i< SHOTGUN_PELLETS_AMMOUNT; i++){
            scatterAngles[i]  = GameClient.nextFloat(-15,15);
        }


        //ProjectileLaunchedPacket res = new ShotgunShotPacket(origin, destination, speed, damage, maxDistance, projectileType,shooterId, null);
        ProjectileLaunchedPacket res = new ShotgunShotPacket(origin, destination, speed, damage, maxDistance, projectileType,shooterId, scatterAngles);
        networkManager.sendPacket(res);

        return res;
    }

    @Override
    public void applyScatterAngle() {

        Vector2 v = destination.toVector2().sub(origin.toVector2());
        v.rotate(scatter);

        destination = origin.add(v);

    }

    @Override
    public void drawTrail() {
      /*  if(isShowingSplash) return;
        drawer.setColor(Color.BLACK);

        drawer.rectLine(currentPosition.getX(), currentPosition.getY(), origin.x ,origin.y , 5);*/
    }
    @Override
    public void update(float deltaTime) {
        double distance = destination.distance(currentPosition);

         forward = new Vector2(destination.getX() - currentPosition.getX(), destination.getY() - currentPosition.getY());
        forward.scl((float) (speed/distance)*deltaTime);

        currentPosition.x += forward.x;
        currentPosition.y += forward.y;




        if(isServer) return;

        trailEffect.setPosition(currentPosition.getX(),currentPosition.getY());
        trailEffect.update(deltaTime);
    }

    @Override
    public void render(float deltaTime) {
        if(!isShowingSplash)
        {
            trailEffect.draw(batch, deltaTime);
            sprite.draw(batch);
            sprite.setPosition(currentPosition.getX() - sprite.getWidth()/2, currentPosition.getY()-sprite.getHeight()/2);
            sprite.setRotation(forward.angle()+270);

        }

        if(isShowingSplash && explosion != null && !explosion.isComplete())
        {

           // batch.begin();
            //  pe.update(deltaTime);
            explosion.draw(batch, deltaTime);
         //   batch.end();
        }
    }
    public void soundAndSplash()
    {
       /* if(isShowingSplash) return;
        {
            explosion.setPosition(currentPosition.x ,currentPosition.y);
            explosion.start();
            //GameClient.getGame().getSoundManager().playSoundWithPanAndVolume(SoundManager.EXPLOSION1, new IoPoint(camera.position.x, camera.position.y),  currentPosition );
        }*/
        }


    @Override
    public boolean canBeRemoved() {
        return true ; // no effects to display
    }


    @Override
    public void reset() {
        super.reset();
    }
}
