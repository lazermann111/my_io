package com.github.myio.entities;


import com.badlogic.gdx.math.Vector2;
import com.github.czyzby.websocket.serialization.SerializationException;
import com.github.czyzby.websocket.serialization.Transferable;
import com.github.czyzby.websocket.serialization.impl.Deserializer;
import com.github.czyzby.websocket.serialization.impl.Serializer;

public class IoPoint implements Transferable<IoPoint> {

    public float x,y;

    public IoPoint() {
    }

    public double distance(IoPoint other)
    {
        return Math.hypot(x - other.x, y - other.y);
    }

    public IoPoint(float x, float y) {
        this.x = x;
        this.y = y;
    }

    public float getX() {
        return x;
    }

    public IoPoint add (Vector2 other)
    {
        x+=other.x;
        y+=other.y;
        return this;
    }

    public IoPoint copy ()
    {
        return new IoPoint(x,y);
    }

    public Vector2 toVector2 ()
    {
        return new Vector2(x,y);
    }

    public void setX(int x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    @Override
    public String toString() {
        return "x=" + x +
                ", y=" + y ;}

    @Override
    public void serialize(Serializer serializer) throws SerializationException {
        serializer.serializeFloat(x).serializeFloat(y);
    }

    @Override
    public IoPoint deserialize(Deserializer deserializer) throws SerializationException {
        return new IoPoint(deserializer.deserializeFloat(),deserializer.deserializeFloat());
    }
}
