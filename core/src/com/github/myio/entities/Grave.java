package com.github.myio.entities;


import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.github.myio.EntityManager;
import com.github.myio.GameClient;
import com.github.myio.screens.GameScreen;

import static com.github.myio.StringConstants.MARKER;



public class Grave extends Cell {
    Texture texture;


    public Grave(SpriteBatch batch, ShapeRenderer shapeRenderer, float x, float y, int radius, String id, int rotation) {
        super(batch, shapeRenderer, x, y, radius, id);
        rotationAngle = rotation;

        texture = new Texture(Gdx.files.internal("atlas/RIP.png"));

        sprite = new Sprite(texture);
        sprite.setSize(texture.getWidth()/2,texture.getHeight()/2);
        sprite.setPosition(x - texture.getWidth()/4,y - texture.getHeight()/4);
//        sprite.setRotation(rotationAngle-90);

    }

    @Override
    public void rendererDraw(float delta) {

        sprite.draw(batch);
    }
}

