package com.github.myio.entities.weapons;


import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;
import com.github.myio.GameClient;
import com.github.myio.StringConstants;
import com.github.myio.entities.IoPoint;
import com.github.myio.entities.PlayerCell;
import com.github.myio.entities.projectiles.AbstractProjectile;
import com.github.myio.enums.WeaponType;
import com.github.myio.inventory.InventoryItemSlot;
import com.github.myio.inventory.InventoryManager;
import com.github.myio.networking.NetworkManager;
import com.github.myio.networking.packet.ProjectileLaunchedPacket;
import com.github.myio.networking.packet.UseAmmoPacket;

public abstract class AbstractRangeWeapon
{
    protected float scatterRange = 0;

    //ROCKET(1), MACHINEGUN(2), RAIL(3), PLASMA(4), SHOTGUN_AMMO(5);
    public final static int WEAPON_TYPE_BAZOOKA = 1;
    public final static int WEAPON_TYPE_MACHINE_GUN = 2;
    public final static int WEAPON_TYPE_SHOTGUN = 3;
    public final static int WEAPON_TYPE_PISTOL = 4;
    public final static int WEAPON_TYPE_KNIFE = 5;
    public final static int WEAPON_TYPE_GRENADE = 6;
    public final static int WEAPON_TYPE_FLASH = 7;
    public final static int WEAPON_TYPE_GRENADE_LAUNCHER = 8;

    public int timerForReacheng;


    public AbstractProjectile projectileBlueprint;
    protected PlayerCell owner;
    AmmunitionType ammunitionType;

    //dynamic fields
    public Integer totalAmmoAmount; // all ammos; important!! should be reference value
    public int clipAmmoAmount;  // ammos in current clip(V OBOIME)

    //final fields
    protected int clipMaxAmmoAmount; // max clip capacity in clip(OBOIMA)
    protected int maxAmmoAmount; // max ammo amount in general(all patron that can be used for this weapon as well as for other weapons)

    public WeaponType weaponType;
    protected boolean canShot = true;
    protected long cooldown;
    protected short kickback;
    protected int decelerationTime;
    protected float reloadTime;//in milliseconds
    protected boolean isReloading = false;
    String mBlueprint;

    protected ShapeRenderer drawer;


    public boolean canBeReload(InventoryManager inventoryManager, AbstractRangeWeapon weapon)
    {
            if (!(clipAmmoAmount == clipMaxAmmoAmount) && checkAmmo(inventoryManager) && !weapon.getReloading())
            {
                    return true;
            }
            return false;
    }

    private int clipAmmoAmountBeforeReload;//Temp variable to stop reloading
    private boolean cancelReloading = false;

    public boolean isCancelReloading()
    {
        return cancelReloading;
    }

    public void setCancelReloading(boolean cancelReloading)
    {
        this.cancelReloading = cancelReloading;
    }

    public void resetClipAmount()
    {
        clipAmmoAmount = clipAmmoAmountBeforeReload;
    }

    public boolean reload(InventoryManager inventoryManager, boolean isClient, int patronForReload) // patronForReload: for client = 0, for server = p.mPatron
    {
        int needPatronForReload = 0;
        String ammoBlueprint = getWeaponAmmoBlueprint(weaponType);
        clipAmmoAmountBeforeReload = clipAmmoAmount;
        int ammoBefore = inventoryManager.getAmmo(ammoBlueprint);

        if(isClient) {
            needPatronForReload = clipMaxAmmoAmount - clipAmmoAmount;
            GameClient.getGame().getNetworkManager().sendPacket(new UseAmmoPacket(GameClient.getGame().getLocalPlayer().getId(), this.weaponType.name(), needPatronForReload));

        } else {needPatronForReload = patronForReload;}

        if (needPatronForReload < ammoBefore)
        {
            inventoryManager.takeAmmoFromSlot(this, needPatronForReload, ammoBlueprint);
            clipAmmoAmount = clipMaxAmmoAmount;
        }
        else
        {
            inventoryManager.takeAmmoFromSlot(this, ammoBefore, ammoBlueprint);
             if ((ammoBefore+clipAmmoAmount) <= clipMaxAmmoAmount )
             {
                 clipAmmoAmount = ammoBefore+clipAmmoAmount;

             }else {
                clipAmmoAmount = clipMaxAmmoAmount;
             }
        }
        int ammoAfterTakeReload = inventoryManager.getAmmo(ammoBlueprint);
        totalAmmoAmount = clipAmmoAmount + ammoAfterTakeReload;
       // System.out.println("reload(): ammoAfterTakeReload = " + ammoAfterTakeReload + " isClient = " + isClient);
        setReloading(false);
        return true;

    }

    public boolean reloadShotgun(InventoryManager inventoryManager, boolean isClient, int patronForReload)
    {
        int needPatronForReload = 0;
        clipAmmoAmountBeforeReload = clipAmmoAmount;

        String ammoBlueprint = getWeaponAmmoBlueprint(weaponType);
        int ammoBefore = inventoryManager.getAmmo(ammoBlueprint);

        if(isClient) {
            needPatronForReload = clipMaxAmmoAmount - clipAmmoAmount;
            GameClient.getGame().getNetworkManager().sendPacket(new UseAmmoPacket(GameClient.getGame().getLocalPlayer().getId(), this.weaponType.name(), needPatronForReload));

        } else {needPatronForReload = patronForReload;}
        if (needPatronForReload < ammoBefore)
        {
            inventoryManager.takeAmmoFromSlot(this, 1, ammoBlueprint);
            ++clipAmmoAmount;
        }
        else
        {
            inventoryManager.takeAmmoFromSlot(this, 1, ammoBlueprint);
            if ((ammoBefore+clipAmmoAmount)<= clipMaxAmmoAmount )
            {
                ++clipAmmoAmount;

            }else {
                ++clipAmmoAmount;
            }

        }

        int ammoAfterTakeReaload = inventoryManager.getAmmo(ammoBlueprint);

        if (clipAmmoAmount > clipMaxAmmoAmount)
        {
            clipAmmoAmount = clipMaxAmmoAmount;
        }

       ++totalAmmoAmount;
        return true;

    }


    public ProjectileLaunchedPacket fire(IoPoint start, IoPoint touchPoint, int rotationAngle, String shooterId, InventoryItemSlot slot)
    {

        if (clipAmmoAmount > 0 && canShoot())
        {
            Vector2 endVector = new Vector2(0, projectileBlueprint.maxDistance);
            endVector.rotate(rotationAngle + 180);
            IoPoint end = new IoPoint(start.getX() + endVector.x, start.getY() + endVector.y);
            --clipAmmoAmount;
            --totalAmmoAmount;

            //slot.setAmount(clipAmmoAmount);
            lastShotTime = System.currentTimeMillis();
            canShot = false;
            return projectileBlueprint.launch(GameClient.getGame().getNetworkManager(), start, end, shooterId, 0);
        }


        return null;


    }

    public ProjectileLaunchedPacket fire(IoPoint start, IoPoint touchPoint, int rotationAngle, String shooterId, NetworkManager networkManager)
    {

        if (clipAmmoAmount > 0 && canShoot())
        {
            Vector2 endVector = new Vector2(0, projectileBlueprint.maxDistance);
            endVector.rotate(rotationAngle + 180);
            IoPoint end = new IoPoint(start.getX() + endVector.x, start.getY() + endVector.y);
            --clipAmmoAmount;
            --totalAmmoAmount;
            lastShotTime = System.currentTimeMillis();
            canShot = false;
            float scatter = GameClient.nextFloat(-5, 5);
            return projectileBlueprint.launch(networkManager, start, end, shooterId, scatter);
        }

        return null;


    }

    public long getLastShotTime()
    {
        return lastShotTime;
    }

    public short getKickback()
    {
        return kickback;
    }

    public int getDecelerationTime()
    {
        return decelerationTime;
    }

    public long getCooldown()
    {
        return cooldown;
    }

    public static AbstractRangeWeapon Factory(WeaponType w, ShapeRenderer renderer, PlayerCell owner)
    {

        switch (w)
        {
            case SHOTGUN:
                return new ShotGun(renderer, owner);
            case MACHINE_GUN:
                return new MachineGun(renderer, owner);
            case AK47:
                return new AK47(renderer,owner);
            case PISTOL:
                return new Pistol(renderer, owner);
            case ROCKET:
                return new Bazooka(renderer, owner);
            case FRAG_GRENADE:
                return new Grenade(renderer, owner);
            case FLASHBANG:
                return new FlashBang(renderer, owner);
            case GRENADE_LAUNCHER:
                return new GrenadeLauncher(renderer, owner);
            case KNIFE:
                return new Knife(renderer, owner);

        }
        return null;
    }

    public static AbstractRangeWeapon Factory(String blueprint, ShapeRenderer renderer, PlayerCell owner)
    {
        if (blueprint.equals(StringConstants.SHOTGUN))
        {
            return new ShotGun(renderer, owner);
        }
        else if (blueprint.equals(StringConstants.MACHINEGUN))
        {
            return new MachineGun(renderer, owner);
        }
        else if (blueprint.equals(StringConstants.PISTOL))
        {
            return new Pistol(renderer, owner);
        }
        else if (blueprint.equals(StringConstants.BAZOOKA))
        {
            return new Bazooka(renderer, owner);
        }
        else if (blueprint.equals(StringConstants.FRAG_GRENADE))
        {
            return new Grenade(renderer, owner);
        }
        else if (blueprint.equals(StringConstants.FLASH_GRENADE))
        {
            return new FlashBang(renderer, owner);
        }
        else if (blueprint.equals(StringConstants.GRENADE_LAUNCHER))
        {
            return new GrenadeLauncher(renderer, owner);
        }
        else if (blueprint.equals(StringConstants.AK47))
        {
            return new AK47(renderer, owner);
        }
        return null;
    }


    /*public static ArrayList<AbstractRangeWeapon> Factory2(List <AbstractWeaponDto> w, ShapeRenderer renderer)
    {
        if(w == null) return null;
        ArrayList<AbstractRangeWeapon> res = new ArrayList<AbstractRangeWeapon>();

        for (AbstractWeaponDto d : w)
        {
            switch (d.weaponType) {
                case WEAPON_TYPE_SHOTGUN:
                    res.add(new ShotGun(renderer)) ;

                case WEAPON_TYPE_MACHINE_GUN:
                    res.add(new MachineGun(renderer)) ;
                case WEAPON_TYPE_PISTOL:
                    res.add(new Pistol(renderer));
                case WEAPON_TYPE_BAZOOKA:
                    res.add(new Bazooka(renderer)) ;
                case WEAPON_TYPE_GRENADE:
                    res.add(new Grenade(renderer)) ;
            }
        }
        return res;
    }*/

    public abstract void draw(float playerX, float playerY, float playerAngle);

    protected long lastShotTime;

    public void setCanShot(boolean canShot)
    {

        this.canShot = canShot;
    }


    public boolean canShoot()
    {
        return (lastShotTime + cooldown) < System.currentTimeMillis() && canShot /* && !owner.hasShield()*/ && !owner.isUsingInventory;
    }

    public int getMaxAmmoAmount()
    {
        return maxAmmoAmount;
    }

    public int getClipMaxAmmoAmount()
    {
        return clipMaxAmmoAmount;
    }

    public float getReloadTime()
    {
        return reloadTime;
    }
    public void setReloadTime(float reloadTime)
    {
        this.reloadTime = reloadTime;
    }

    public boolean checkAmmo(InventoryManager inventoryManager)
    {
        String ammoBlueprint = getWeaponAmmoBlueprint(weaponType);
        int ammo = inventoryManager.getAmmo(ammoBlueprint);
        if (ammo > 0)
        {
            return true;
        }
        return false;
    }

    public int quantityAmmoForAlonReload (InventoryManager inventoryManager)
    {

        String ammoBlueprint = getWeaponAmmoBlueprint(weaponType);
        int ammoBefore = inventoryManager.getAmmo(ammoBlueprint);
        int needPatronForReload = clipMaxAmmoAmount - clipAmmoAmount;

        if (needPatronForReload <= ammoBefore)
        {
            return needPatronForReload;
        }
        else
        {

            if(clipMaxAmmoAmount > (clipAmmoAmount + ammoBefore))
            {
                return ammoBefore;
            }else {
                return clipMaxAmmoAmount;
            }

        }
    }
    public String getWeaponAmmoBlueprint(WeaponType weaponType)
    {
        if (weaponType.equals(WeaponType.SHOTGUN))
        {
            return StringConstants.HARD_PATRON;
        }
        if (weaponType.equals(WeaponType.MACHINE_GUN))
        {
            return StringConstants.MEDIUM_PATRON;
        }
        if (weaponType.equals(WeaponType.PISTOL))
        {
            return StringConstants.MEDIUM_PATRON;
        }
        if (weaponType.equals(WeaponType.ROCKET))
        {
            return StringConstants.ROCKET_PATRON;
        }
        if (weaponType.equals(WeaponType.GRENADE_LAUNCHER))
        {
            return StringConstants.MINI_HARD_PATRON;
        }
        if (weaponType.equals(WeaponType.AK47))
        {
            return StringConstants.SMALL_PATRON;
        }
        return null;
    }

 public int getTimerForReacheng () {return timerForReacheng;}

    public void setClipAmmoAmount(int ammoAmount) {
        clipAmmoAmount = ammoAmount;
    }
    public Integer getTotalAmmoAmount() {
        return totalAmmoAmount;
    }

    public void setTotalAmmoAmount(Integer totalAmmoAmount) {
        this.totalAmmoAmount = totalAmmoAmount;
    }

    public boolean getReloading() {
        return isReloading;
    }

    public void setReloading(boolean reloading) {
        isReloading = reloading;
    }
}
