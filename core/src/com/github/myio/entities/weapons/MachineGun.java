package com.github.myio.entities.weapons;

import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;
import com.github.myio.GameClient;
import com.github.myio.entities.IoPoint;
import com.github.myio.entities.PlayerCell;
import com.github.myio.entities.projectiles.MachineGunBullet;
import com.github.myio.enums.WeaponType;
import com.github.myio.inventory.InventoryItemSlot;
import com.github.myio.networking.packet.ProjectileLaunchedPacket;


public class MachineGun extends AbstractRangeWeapon {

    public MachineGun(){}

    public MachineGun(ShapeRenderer renderer, PlayerCell o)
    {
        owner = o;
        this.projectileBlueprint = new MachineGunBullet(renderer);
        drawer = renderer;
        weaponType = WeaponType.MACHINE_GUN;
        cooldown = 80;
        clipMaxAmmoAmount = 30;
        maxAmmoAmount=clipMaxAmmoAmount*3;
        kickback = 4;
        decelerationTime = 200;
        totalAmmoAmount = clipMaxAmmoAmount;
        clipAmmoAmount = clipMaxAmmoAmount;
        reloadTime = 2000.0f;
        ammunitionType = AmmunitionType.MEDIUM_PATRON;
        timerForReacheng = 2;
    }

    @Override
    public void draw(float playerX, float playerY, float playerAngle) {
       /* drawer.set(ShapeRenderer.ShapeType.Filled);
        drawer.setColor(Color.BLACK);
        drawer.rect(playerX,playerY,0,0,-30,-100,1,1,playerAngle);*/
    }

    @Override
    public ProjectileLaunchedPacket fire(IoPoint start, IoPoint touchPoint, int rotationAngle, String shooterId, InventoryItemSlot slot) {

        if(clipAmmoAmount > 0 && canShoot()) {

            calculateScatter();

            Vector2 endVector = new Vector2(0, projectileBlueprint.maxDistance);
            endVector.rotate(rotationAngle + 180);
            IoPoint end = new IoPoint(start.getX() + endVector.x, start.getY() + endVector.y);
            --clipAmmoAmount;
           // --totalAmmoAmount;
            //slot.setAmount(totalAmmoAmount);
            lastShotTime = System.currentTimeMillis();
            canShot = false;
            float scatter = GameClient.nextFloat(-scatterRange, scatterRange);;
            return  projectileBlueprint.launch(GameClient.getGame().getNetworkManager(),start,end,shooterId, scatter);
        }

        return null;
    }

    private void calculateScatter() {
        if(System.currentTimeMillis() - lastShotTime < cooldown + cooldown/5) {
            scatterRange += 1.2;
        }
        else {
            if(owner.isAim())
                scatterRange = 0;
            else
                scatterRange = 3;
            if(owner.isMoving()) {
                scatterRange += 3;
            }
        }
    }

    @Override
    public boolean canShoot()
    {
        return (lastShotTime + cooldown) < System.currentTimeMillis();// && !owner.hasShield();
    }
}
