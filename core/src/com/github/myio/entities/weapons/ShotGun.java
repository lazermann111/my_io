package com.github.myio.entities.weapons;

import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;
import com.github.myio.GameClient;
import com.github.myio.entities.IoPoint;
import com.github.myio.entities.PlayerCell;
import com.github.myio.entities.projectiles.ShotgunAmmo;
import com.github.myio.enums.WeaponType;
import com.github.myio.inventory.InventoryItemSlot;
import com.github.myio.networking.packet.ProjectileLaunchedPacket;

/**
 * Created by taras on 19.02.2018.
 */

public class ShotGun extends AbstractRangeWeapon {

    public ShotGun(){}

    public ShotGun(ShapeRenderer renderer, PlayerCell o)
    {
        owner = o;
        this.projectileBlueprint = new ShotgunAmmo(renderer);
        drawer = renderer;
        weaponType = WeaponType.SHOTGUN;
        cooldown = 700;
        kickback = 6;
        decelerationTime = 400;
        clipMaxAmmoAmount = 5;
        maxAmmoAmount = clipMaxAmmoAmount*3;
        totalAmmoAmount = clipMaxAmmoAmount;
        clipAmmoAmount = clipMaxAmmoAmount;
        reloadTime = 1000.0f;
        timerForReacheng = 0;


    }

    @Override
    public void draw(float playerX, float playerY, float playerAngle) {
     /*   drawer.set(ShapeRenderer.ShapeType.Filled);
        drawer.setColor(Color.YELLOW);
        drawer.rect(playerX,playerY,0,0,-30,-100,1,1,playerAngle);*/
    }

    @Override
    public ProjectileLaunchedPacket fire(IoPoint start, IoPoint touchPoint, int rotationAngle, String shooterId, InventoryItemSlot slot) {
        if(clipAmmoAmount >0 && canShoot()) {
            Vector2 endVector = new Vector2(0, projectileBlueprint.maxDistance);
            endVector.rotate(rotationAngle + 180);
            IoPoint end = new IoPoint(start.getX() + endVector.x, start.getY() + endVector.y);
            --clipAmmoAmount;
           // --totalAmmoAmount;
           // slot.setAmount(totalAmmoAmount);
            lastShotTime = System.currentTimeMillis();

            canShot = false;
            return projectileBlueprint.launch(GameClient.getGame().getNetworkManager(),start,end,shooterId, 0);

           // return n
        }

        return null;
    }

    @Override
    public float getReloadTime() {
        return (clipMaxAmmoAmount - clipAmmoAmount) * reloadTime/5.0f;
    }
}
