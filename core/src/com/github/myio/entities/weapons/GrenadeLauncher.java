package com.github.myio.entities.weapons;

import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;
import com.github.myio.GameClient;
import com.github.myio.entities.IoPoint;
import com.github.myio.entities.PlayerCell;
import com.github.myio.entities.projectiles.GrenadeLauncherAmmo;
import com.github.myio.enums.WeaponType;
import com.github.myio.inventory.InventoryItemSlot;
import com.github.myio.networking.packet.ProjectileLaunchedPacket;

public class GrenadeLauncher extends AbstractRangeWeapon{
    public GrenadeLauncher(){}

    public GrenadeLauncher(ShapeRenderer renderer, PlayerCell o)
    {
        owner = o;
        this.projectileBlueprint = new GrenadeLauncherAmmo(renderer);
        drawer = renderer;
        weaponType = WeaponType.GRENADE_LAUNCHER;
        cooldown = 1000;
        clipMaxAmmoAmount =6;
        clipAmmoAmount =6;
        maxAmmoAmount=6;
        totalAmmoAmount = 6;
        reloadTime = 2000.0f;
        timerForReacheng = 2;
        ammunitionType = AmmunitionType.MINI_HARD_PATRON;
    }

    @Override
    public ProjectileLaunchedPacket fire(IoPoint start, IoPoint touchPoint, int rotationAngle, String shooterId, InventoryItemSlot slot) {
        if(clipAmmoAmount > 0 && canShoot()) {
            --clipAmmoAmount;
           // --totalAmmoAmount;
           // slot.setAmount(totalAmmoAmount);
            lastShotTime = System.currentTimeMillis();
            canShot = false;

            if(Math.hypot(start.x - touchPoint.x, start.y - touchPoint.y) > projectileBlueprint.maxDistance) {
                Vector2 endVector = new Vector2(0, projectileBlueprint.maxDistance);
                endVector.rotate(rotationAngle + 180);
                IoPoint end = new IoPoint(start.getX() + endVector.x, start.getY() + endVector.y);
                return  projectileBlueprint.launch(GameClient.getGame().getNetworkManager(),start,end,shooterId, 0);
            }
            return  projectileBlueprint.launch(GameClient.getGame().getNetworkManager(),start,touchPoint,shooterId, 0);
        }

        return null;
    }

    @Override
    public void draw(float playerX, float playerY, float playerAngle) {
//        drawer.set(ShapeRenderer.ShapeType.Filled);
//        drawer.setColor(Color.BLACK);
//        drawer.rect(playerX,playerY,0,0,-30,-100,1,1,playerAngle);
    }
}
