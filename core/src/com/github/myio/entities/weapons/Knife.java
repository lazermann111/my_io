package com.github.myio.entities.weapons;

import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;
import com.github.myio.GameClient;
import com.github.myio.entities.IoPoint;
import com.github.myio.entities.PlayerCell;
import com.github.myio.enums.WeaponType;
import com.github.myio.inventory.InventoryItemSlot;
import com.github.myio.networking.packet.ProjectileLaunchedPacket;


public class Knife extends AbstractRangeWeapon {


    public Knife() {
    }

    public Knife(ShapeRenderer renderer, PlayerCell o) {

        owner = o;
        drawer = renderer;
        weaponType = WeaponType.KNIFE;
        cooldown = 350;
        projectileBlueprint = new com.github.myio.entities.projectiles.Knife(renderer);
        totalAmmoAmount = 1;
        clipAmmoAmount = 1;//need for ability to kick. If clipAmmo == 0 you won't kick
        timerForReacheng = 2;
    }

    @Override
    public ProjectileLaunchedPacket fire(IoPoint start, IoPoint touchPoint, int rotationAngle, String shooterId, InventoryItemSlot slot) {
        if(canShoot()) {
            Vector2 endVector = new Vector2(0, projectileBlueprint.maxDistance);
            endVector.rotate(rotationAngle + 180);
            IoPoint end = new IoPoint(start.getX() + endVector.x, start.getY() + endVector.y);
            lastShotTime = System.currentTimeMillis();
            canShot = false;
            //slot.setAmount(totalAmmoAmount);
            return  projectileBlueprint.launch(GameClient.getGame().getNetworkManager(),start,end,shooterId, 0);
        }
        return null;
    }


    @Override
    public void draw(float playerX, float playerY, float playerAngle) {
//        drawer.set(ShapeRenderer.ShapeType.Filled);
//        drawer.setColor(Color.BLACK);
//        drawer.rect(playerX,playerY,0,0,-30,-100,1,1,playerAngle);
    }
}
