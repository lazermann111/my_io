package com.github.myio.entities.weapons;

import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;
import com.github.myio.GameClient;
import com.github.myio.entities.IoPoint;
import com.github.myio.entities.PlayerCell;
import com.github.myio.entities.projectiles.FlashbangAmmo;
import com.github.myio.enums.WeaponType;
import com.github.myio.inventory.InventoryItemSlot;
import com.github.myio.networking.packet.ProjectileLaunchedPacket;

/**
 * Created by Igor on 3/27/2018.
 */

public class FlashBang extends AbstractRangeWeapon {

    public FlashBang(){}

    public FlashBang(ShapeRenderer renderer, PlayerCell o)
    {
        owner = o;
        this.projectileBlueprint = new FlashbangAmmo(renderer);
        drawer = renderer;
        weaponType = WeaponType.FLASHBANG;
        cooldown = 1000;
        clipMaxAmmoAmount =10;
        clipAmmoAmount = 1;
        maxAmmoAmount = 2;
        totalAmmoAmount = 1;
        timerForReacheng = 2;
    }

    @Override
    public ProjectileLaunchedPacket fire(IoPoint start, IoPoint touchPoint, int rotationAngle, String shooterId, InventoryItemSlot slot) {
        if(clipAmmoAmount > 0 && canShoot()) {
            --clipAmmoAmount;
            --totalAmmoAmount;
            //slot.setAmount(clipAmmoAmount);
            lastShotTime = System.currentTimeMillis();
            canShot = false;

            if(Math.hypot(start.x - touchPoint.x, start.y - touchPoint.y) > projectileBlueprint.maxDistance) {
                Vector2 endVector = new Vector2(0, projectileBlueprint.maxDistance);
                endVector.rotate(rotationAngle + 180);
                IoPoint end = new IoPoint(start.getX() + endVector.x, start.getY() + endVector.y);
                return  projectileBlueprint.launch(GameClient.getGame().getNetworkManager(),start,end,shooterId, 0);
            }
            return  projectileBlueprint.launch(GameClient.getGame().getNetworkManager(),start,touchPoint,shooterId, 0);
        }

        return null;
    }


    @Override
    public void draw(float playerX, float playerY, float playerAngle) {

    }
}
