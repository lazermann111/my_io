package com.github.myio.entities.item;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.MathUtils;
import com.github.myio.GameClient;
import com.github.myio.StringConstants;
import com.github.myio.entities.Cell;
import com.github.myio.entities.projectiles.AbstractProjectile;
import com.github.myio.enums.ItemType;

import static com.github.myio.GameConstants.isBotSimulator;
import static com.github.myio.GameConstants.isServer;

public class ItemCell extends Cell {

	private ItemType itemType;
    //private int xpForHp;
	private String item_blueprint;
    private final float refreshTime = 1.0f; // refresh to new image every 1 seconds.
    private float counter = refreshTime;
	private Sprite sprite;

	//todo remove this hack
	public int totalAmmoAmount; //used only for weapon cells
	public int clipAmmoAmount;
	public static int DEFAULT_AMMO_AMOUNT = -2; //tell us to use default values from AbstractRAngeWeapon's inheritors constructor
	public static int DEFAULT_CLIP_AMMO_AMOUNT = -2;


	public ItemCell(SpriteBatch batch, ShapeRenderer shapeRenderer, float x, float y, String id, String[] strings) {
	}

	public ItemCell(SpriteBatch batch, ShapeRenderer shapeRenderer,
                    float posX, float posY, String id, ItemType itemType,String item_blueprint, int totalAmmoAmount, int clipAmmoAmount)
	{

		//super(batch, shapeRenderer, posX, posY, 100, id);
		this.batch = batch;
		this.shapeRenderer = shapeRenderer;
		this.x = posX;
		this.y = posY;
		this.id = id;
		this.itemType = itemType;
		this.item_blueprint = item_blueprint;
		this.totalAmmoAmount = totalAmmoAmount;
		this.clipAmmoAmount = clipAmmoAmount;


        if (!isServer && !isBotSimulator) {
			if (itemType.equals(ItemType.PATRON)){
				int i = MathUtils.random(0,360);
				sprite = GameClient.cellAtlas.createSprite(item_blueprint);
				sprite.setRotation(i);
			}else {
				sprite = GameClient.cellAtlas.createSprite(item_blueprint);
			}
			sprite.setPosition(x - sprite.getWidth()/2, y - sprite.getHeight()/2);
			if(sprite.getWidth() > sprite.getHeight()) {
				setCollisionRadius(sprite.getWidth()/2);
			}
			else
				setCollisionRadius(sprite.getHeight()/2);
			sprite.setOrigin(sprite.getWidth()/2, sprite.getHeight()/2);
		}
    }

    @Override
    public void rendererDraw(float deltaTime) {

       // batch.begin();
            counter -= deltaTime;

            animateLabel();
           	sprite.draw(batch);

            if(counter < 0){
                counter = refreshTime;
        }
       // batch.end();
    }

	float scale = 1;
	boolean increase = true;
	private void animateLabel() {
		if(increase)
			scale+=0.003f;
		else
			scale-=0.003f;
		if(scale > 1.2) {
			increase = false;
		}
		else if(scale < 1) {
			increase = true;
		}
		sprite.setScale(scale);
	}

	public String getItem_blueprint() {return item_blueprint;}

	public ItemType getItemType() {
		return itemType;
	}

	public void setItemType(ItemType itemType) {
		this.itemType = itemType;
	}

    public int getClipAmmoAmount()
    {
        return clipAmmoAmount;
    }

    public void setClipAmmoAmount(int clipAmmoAmount)
    {
        this.clipAmmoAmount = clipAmmoAmount;
    }

    public int getTotalAmmoAmount()
	{
		return totalAmmoAmount;
	}

	public void setTotalAmmoAmount(int totalAmmoAmount)
	{
		this.totalAmmoAmount = totalAmmoAmount;
	}


	//Count patron in one clip when pick patron
    public int getPatron()
	{
		if (getItem_blueprint().equals(StringConstants.SMALL_PATRON))
		{
			return 30;
		}else if (getItem_blueprint().equals(StringConstants.MEDIUM_PATRON)){
			return 30;
		}else if (getItem_blueprint().equals(StringConstants.MINI_HARD_PATRON)){
			return 6;
		}else if (getItem_blueprint().equals(StringConstants.HARD_PATRON)){
			return 5;
		}else if (getItem_blueprint().equals(StringConstants.ROCKET_PATRON)){
			return 1;
		}
			return 0;
	}

	@Override
	public boolean collides(Cell another)
	{
		//collisionRadius = radiusItem + radiusItem * 10.0f/100.0f;
		//another.collisionRadius = another.radiusItem + another.radiusItem * 10.0f/100.0f;
		//IoLogger.log("Here");
		return Math.hypot(x - another.x, y - another.y) - getCollisionRadius() - another.getCollisionRadius()/10 <=0;
	}
	@Override
	public boolean collides(AbstractProjectile another)
	{

		//another.collisionRadius = another.radiusItem + another.radiusItem * 10.0f/100.0f;
		//IoLogger.log("Gere");
		return Math.hypot(x - another.getCurrentPosition().getX(), y - another.getCurrentPosition().getY()) - getCollisionRadius() - another.radius <=0;
	}


	@Override
	public String toString() {
		return "ItemCell{" +
				"itemType=" + itemType +
				", item_blueprint='" + item_blueprint + '\'' +
				", clipAmmoAmount='" + totalAmmoAmount + '\'' +
				"} " + super.toString();
	}
}
