package com.github.myio.entities.item;

/**
 * Helps to create relation between ItemType enum and specific properties of cell on client
 */

public class ItemDetails {

    private String textureName;
    private int xpGiven;
    private int radius;
    private int addPatron;

    public ItemDetails(String spritePath, int xpGiven) {
        this.textureName = spritePath;
        this.xpGiven = xpGiven;
    }


    public ItemDetails(){}

    public int getAddPatron() {
        return addPatron;
    }

    public void setAddPatron(int addPatron) {
        this.addPatron = addPatron;
    }

    public String getTextureName() {
        return textureName;
    }

    public void setTextureName(String textureName) {
        this.textureName = textureName;
    }

    public int getXpGiven() {
        return xpGiven;
    }

    public void setXpGiven(int xpGiven) {
        this.xpGiven = xpGiven;
    }

    public int getRadius() {
        return radius;
    }

    public void setRadius(int radius) {
        this.radius = radius;
    }
}
