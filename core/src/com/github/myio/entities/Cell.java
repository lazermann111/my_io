package com.github.myio.entities;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.github.myio.entities.map.TacticalShield;
import com.github.myio.entities.projectiles.AbstractProjectile;

import java.util.ArrayList;

import static com.github.myio.GameConstants.TILE_HEIGHT;

public abstract class Cell implements Collidable  {
	protected SpriteBatch batch;
	protected GlyphLayout glyphLayout;
	protected ShapeRenderer shapeRenderer;


	protected String id;
	protected Color color;
	protected Texture texture;
	protected Sprite sprite;
	public float x;
	public float y;

	// internal variables for interpolation
	protected float xPrevious, yPrevious; //used for interpolation
	protected float velocityX, velocityY; //used for interpolation
	protected float xInterploated, yInterpoldated; //used for interpolation



	protected float rotationPrev; //used for interpolation
	protected float rotationVelocity; //used for interpolation
	protected float rotationInterpolated; //used for interpolation


	public int radius = 200;
	protected int rotationAngle;
	protected float collisionRadius = 110;
	protected float interactionRadius;

	protected ArrayList<String> playersId = new ArrayList<String>();//player's id that interact with cell

	protected boolean insidePit = false;//for player
	protected boolean underWeb = false;

	protected boolean isPit = false;

	public Cell() {
		radius = (int) (TILE_HEIGHT/2);
		collisionRadius = radius + radius * 10f/100f;

	}

	public Cell(SpriteBatch batch, ShapeRenderer shapeRenderer, float x, float y, int radius, String id) {
		super();
		this.batch = batch;
		this.shapeRenderer = shapeRenderer;
		this.x = x;
		this.y = y;

		this.radius = radius;
		this.color = new Color((float) Math.random(), (float) Math.random(),
				(float) Math.random(), 1);
		this.id = id;
		xPrevious = x;
		yPrevious = y;
		collisionRadius = radius + radius * 10.0f/100.0f;
	}

	public void rendererDraw(float delta) {

		//we have no pure Cell instances in game
		/*
		shapeRenderer.setColor(color);

		velocityX = x - xPrevious;
		velocityY = y - yPrevious;

		//xInterploated = (x + xPrevious) /2;
		xInterploated =xPrevious+ velocityX * delta *2f* 1000/SERVER_SEND_UPDATES_RATE;
		//yInterpoldated = (y + yPrevious) /2;
		yInterpoldated =yPrevious+ velocityY * delta *2f* 1000/SERVER_SEND_UPDATES_RATE;

		//IoLogger.log("xInterploated = " + xInterploated);
		//IoLogger.log("yInterpoldated = " + yInterpoldated);
		collisionRadius = radius + radius * 10.0f/100.0f;
		shapeRenderer.set(ShapeRenderer.ShapeType.Filled);
		shapeRenderer.circle(xInterploated, yInterpoldated, radius);
		//shapeRenderer.circle(x, y, mass);

		xPrevious = xInterploated;
		yPrevious = yInterpoldated;
		//shapeRenderer.end();*/
	}

	public boolean collides(Cell another)
	{
		//collisionRadius = radiusItem + radiusItem * 10.0f/100.0f;
		//another.collisionRadius = another.radiusItem + another.radiusItem * 10.0f/100.0f;

	/*	shapeRenderer.begin(ShapeRenderer.ShapeType.Line);
		shapeRenderer.rect(x, y , 10, 10);
		shapeRenderer.end();
		shapeRenderer.begin(ShapeRenderer.ShapeType.Line);
		shapeRenderer.rect(another.getX(), another.getY(), 10, 10);
		shapeRenderer.line(x , y , another.getX(),  another.getY());
		shapeRenderer.end();
		//drawRadius
		shapeRenderer.begin(ShapeRenderer.ShapeType.Line);
		shapeRenderer.rect(another.getX(), another.getY(), 10, 10);
		shapeRenderer.line(x , y , x + radius,  y + radius);
		shapeRenderer.end();
		if(Math.hypot(x - another.x, y - another.y) - getCollisionRadius() - another.getCollisionRadius() <= 0) {
			IoLogger.log("Cell radius: " + another.getCollisionRadius() + "\nRadius: " + collisionRadius);
		}


*/

		/*shapeRenderer.begin(ShapeRenderer.ShapeType.Line);
		shapeRenderer.circle(another.x, another.y, another.getCollisionRadius());
		shapeRenderer.end();*/

		if (another instanceof TacticalShield)
		{
			return Math.hypot(x - another.x, y - another.y) - getCollisionRadius() - another.getCollisionRadius() <= 0;
		}

		if(this.isPit()) {
			if (!another.isInsidePit()) {
				//IoLogger.log("Result: " + (Math.hypot(x - another.x, y - another.y) - another.getCollisionRadius() + getCollisionRadius()));
				return Math.hypot(x - another.x, y - another.y) - getCollisionRadius() - another.getCollisionRadius() <= 0;
			}
			else {
				//IoLogger.log("Result: " + (Math.hypot(x - another.x, y - another.y) - another.getCollisionRadius() + getCollisionRadius()));
				return Math.hypot(x - another.x, y - another.y) >= getCollisionRadius() - another.getCollisionRadius();
			}
		}
		//experimental feature
		else if(this instanceof PlayerCell && another instanceof PlayerCell)
			return false;
		else
			return Math.hypot(x - another.x, y - another.y) - getCollisionRadius() - another.getCollisionRadius() <= 0;
	}
	public IoPoint getPosition()
	{
		return new IoPoint(x, y);
	}
	public boolean collides(AbstractProjectile another)
	{

		//another.collisionRadius = another.radiusItem + another.radiusItem * 10.0f/100.0f;
		//IoLogger.log("Collide  projectile");
		return Math.hypot(x - another.getCurrentPosition().getX(), y - another.getCurrentPosition().getY()) - getCollisionRadius() - another.radius <=0;
	}

	public void translate(int offsetX, int offsetY) {

		x += offsetX;
		y += offsetY;
	}

	public float getInteractionRadius()
	{
		return getCollisionRadius()*1.1f;
	}

	public boolean isPit() {
		return isPit;
	}

	public void setPit(boolean pit) {
		isPit = pit;
	}

	public boolean isInsidePit() {
		return insidePit;
	}

	public void setInsidePit(boolean insidePit) {
		this.insidePit = insidePit;
	}

	public boolean isUnderWeb() {
		return underWeb;
	}

	public void setUnderWeb(boolean underWeb) {
		this.underWeb = underWeb;
	}

	public float getCollisionRadius() {
		return collisionRadius;
	}

	public void setCollisionRadius(float collisionRadius) {
		this.collisionRadius = collisionRadius;
	}

	public ArrayList<String> getPlayersId() {
		return playersId;
	}

	public String getId() {
		return id;
	}

	public float getX() {
		return this.x;
	}

	public float getY() {
		return this.y;
	}

	public void setX(float v) {
		//IoLogger.log("setX  = " + x);
		//IoLogger.log("xPrevious = " + xPrevious);
		xPrevious = x;
		this.x = v;
	}

	public void setY(float v) {
		yPrevious = y;
		this.y = v;
	}
	public void setYMinus(float v) {
		yPrevious = y;
		this.y -= v;
	}
	public void setXMinus(float v) {
		xPrevious = x;
		this.x -= v;
	}

	public void setYPlus(float v) {
		yPrevious = y;
		this.y += v;
	}
	public void setXPlus(float v) {
		xPrevious = x;
		this.x += v;
	}
	public int getRotationAngle() {
		return rotationAngle;
	}

	public void setRotationAngle(int rotationAngle) {
		rotationPrev = rotationAngle;
		this.rotationAngle = rotationAngle;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Color getColor() {
		return color;
	}

	public void setColor(Color color) {
		this.color = color;
	}

	public Texture getTexture() {
		return texture;
	}

	public void setTexture(Texture texture) {
		this.texture = texture;
	}

	public int getRadius() {
		return radius;
	}

	public void setRadius(int m) {
		this.radius = m;
	}

	public float getVelocityX() {
		return velocityX;
	}

	public float getVelocityY() {
		return velocityY;
	}

	@Override
	public String toString() {
		return "Cell{" +
				", posX=" + x +
				", posY=" + y +
				", radius=" + radius +
				'}';
	}

	public void rendererDrawDebug(float delta) {
		shapeRenderer.circle(x, y, collisionRadius);
	}

}
