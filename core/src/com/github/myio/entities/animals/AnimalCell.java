package com.github.myio.entities.animals;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.github.myio.GameClient;
import com.github.myio.GameConstants;
import com.github.myio.entities.Cell;
import com.github.myio.entities.IoPoint;
import com.github.myio.networking.NetworkManager;
import com.github.myio.networking.packet.HeartBeatPacket;

import static com.github.myio.GameConstants.SERVER_SEND_UPDATES_RATE;
import static com.github.myio.GameConstants.isServer;
import static com.github.myio.StringConstants.LION;

/**
 * Created by dell on 30.03.2018.
 */

public class AnimalCell extends Cell{
    private long lastHeartbeat;
    private int currentHealth;
    public boolean canBeKilled = true;


    public AnimalCell(SpriteBatch batch, ShapeRenderer shapeRenderer, float x, float y, int radius, String id) {
        super(batch, shapeRenderer, x, y, radius, id);
    }
    public AnimalCell(SpriteBatch batch, ShapeRenderer shapeRenderer,
                      float x, float y, int radius, String id,
                      int currentHealth) {
        super(batch, shapeRenderer, x, y, radius,id);
        this.currentHealth = currentHealth;

        if (!isServer) {
            //	IoLogger.log("ITEMCELL",item_blueprint);
            texture = GameClient.getGame().getAssetManager().getTexture(LION);
            sprite = new Sprite(texture);
            sprite.setPosition(x - sprite.getWidth()/2, y - sprite.getHeight()/2);
            if(texture.getWidth() > texture.getHeight()) {
                setCollisionRadius(texture.getWidth()/2);
            }
            else
                setCollisionRadius(texture.getHeight()/2);
        }
    }

    public static float interpolation_const = 0.8f;

    @Override
    public void rendererDraw(float delta) {

        velocityX = x - xPrevious;
        velocityY = y - yPrevious;
        rotationVelocity = rotationAngle - rotationPrev;

        xInterploated =xPrevious+ velocityX * delta *interpolation_const* 1000/SERVER_SEND_UPDATES_RATE;
        yInterpoldated =yPrevious+ velocityY * delta *interpolation_const* 1000/SERVER_SEND_UPDATES_RATE;
        rotationInterpolated =rotationPrev + rotationVelocity * delta *interpolation_const* 1000/SERVER_SEND_UPDATES_RATE;


        sprite.setPosition(xInterploated - sprite.getOriginX(), yInterpoldated - sprite.getOriginY());
        sprite.setRotation(rotationInterpolated + 180);

        batch.begin();  // todo optimize
        sprite.draw(batch);
        batch.end();


        xPrevious = xInterploated;
        yPrevious = yInterpoldated;
        rotationPrev = rotationInterpolated;

    }

    public void setCurrentHealth(int currentHealth) {
        this.currentHealth = currentHealth;
    }

    public int getCurrentHealth() {
        return currentHealth;
    }

    public void setPosition(IoPoint point) {
        xPrevious=x;
        yPrevious=y;
        x = point.getX();
        y = point.getY();
    }

    public IoPoint getPosition()
    {
        return new IoPoint(x, y);
    }

    public void	sendHeartBeat(NetworkManager networkManager) {
        if(lastHeartbeat + GameConstants.HEARTBEAT_RATE > System.currentTimeMillis()) return;
        networkManager.sendPacket(new HeartBeatPacket(id, x, y, rotationAngle, HeartBeatPacket.PLAYER));
        //networkManager.sendPacket(new HeartBeatPacket(id, x, y));
        lastHeartbeat = System.currentTimeMillis();
        //IoLogger.log("Sent heartbeat");
    }

    public boolean isMoving()
    {
        return velocityX != 0 || velocityY !=0;
    }

    public void rendererDrawDebug(float delta) {
        shapeRenderer.circle(xInterploated, yInterpoldated, getCollisionRadius());
    }


}
