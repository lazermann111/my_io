package com.github.myio.entities.tile;


import com.badlogic.gdx.graphics.Color;



public class TileDetails {
    private Color tileColor;
    private float width;
    private float height;
    private boolean isObstacle;

    public TileDetails(Color tileColor, float width, float height, boolean isObstacle) {
        this.tileColor = tileColor;
        this.width = width;
        this.height = height;
        this.isObstacle = isObstacle;
    }

    public Color getTileColor() {
        return tileColor;
    }

    public void setTileColor(Color tileColor) {
        this.tileColor = tileColor;
    }

    public float getWidth() {
        return width;
    }

    public void setWidth(float width) {
        this.width = width;
    }

    public float getHeight() {
        return height;
    }

    public void setHeight(float height) {
        this.height = height;
    }

    public boolean isObstacle() {
        return isObstacle;
    }

    public void setObstacle(boolean obstacle) {
        isObstacle = obstacle;
    }


}
