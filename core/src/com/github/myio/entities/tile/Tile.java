package com.github.myio.entities.tile;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.MathUtils;
import com.github.myio.GameClient;
import com.github.myio.GameConstants;
import com.github.myio.entities.Cell;
import com.github.myio.entities.Collidable;
import com.github.myio.entities.projectiles.AbstractProjectile;
import com.github.myio.enums.TileType;
import com.github.myio.tools.IoLogger;

import static com.github.myio.GameConstants.FLOOR_BROWN_TEXTURES;
import static com.github.myio.GameConstants.FLOOR_GREEN_TEXTURE;
import static com.github.myio.GameConstants.FLOOR_YELLOW_TEXTURE;
import static com.github.myio.GameConstants.TILE_WIDTH;
import static com.github.myio.GameConstants.isServer;
import static com.github.myio.StringConstants.WALL_BROWN;
import static com.github.myio.StringConstants.WALL_BUSH_BROWN;
import static com.github.myio.StringConstants.WALL_YELLOW;


public class Tile implements Collidable {
	
	protected String id;
	protected String name;
	protected float x;
	protected float y;
	protected float width;
	protected float height;
	protected float rotation = 0;
	protected TileType tileType;
	private Color tileColor;

	// Row index of tile in Tile[][] structure
	protected int i;

	protected float collisionRadius = TILE_WIDTH/2;

	// Column index of tile in Tile[][] structure
	protected int j;

	protected Texture texture;
	protected Sprite sprite ;
	public boolean isObstacle;
	public boolean shouldRender;


	protected SpriteBatch batch;
	protected ShapeRenderer shapeRenderer;




	public Tile(){}
	public Tile (SpriteBatch batch, ShapeRenderer shapeRenderer, String id, float x, float y, TileType tileType, int i, int j) {
		this.id = id;
		this.x = x;
		this.y = y;
		this.tileType = tileType;
		this.batch = batch;
		this.shapeRenderer = shapeRenderer;
		TileDetails tileDetails = GameConstants.TILE_DETAILS.get(tileType);
		width = tileDetails.getWidth();
		height = tileDetails.getHeight();
		tileColor = tileDetails.getTileColor();
		this.i = i;
		this.j = j;
		isObstacle = tileDetails.isObstacle();


		if(!isServer)
		{

		}

	}

	public void initWallSprite(TileType tileType) {
		if(sprite != null) return;

		if (tileType.equals(TileType.WALL_YELLOW))
		{
			sprite = GameClient.cellAtlas.createSprite(WALL_YELLOW);
		}else if (tileType.equals(TileType.WALL_BROWN)) {
			sprite = GameClient.cellAtlas.createSprite(WALL_BROWN);

		}else if (tileType.equals(TileType.WALL_BUSH_BROWN)){
			sprite = GameClient.cellAtlas.createSprite(WALL_BUSH_BROWN);
		} else {
			IoLogger.log("Tile.clas initWallSprite() "," Type not found");
		}

		sprite.setSize(width, height);
		sprite.setPosition(x - sprite.getWidth()/2, y - sprite.getHeight()/2);
		sprite.setOrigin(sprite.getWidth() / 2, sprite.getHeight() / 2);
		sprite.setRotation(rotation);
	}
	//Random rnd = new Random();
	private Texture getFloorTexture() {

		//int i = rnd.nextInt(FLOOR_TEXTURES.size());
		return GameClient.getGame().getAssetManager().getTexture((String) FLOOR_BROWN_TEXTURES.toArray()[i]);
	}

	public void initFloorSprite(TileType tileType) {
		if(sprite != null) return;
		if (tileType.equals(TileType.FLOOR_BROWN))
		{
			sprite = GameClient.cellAtlas.createSprite(FLOOR_BROWN_TEXTURES.get(MathUtils.random(0,3)));
		}else if(tileType.equals(TileType.FLOOR_YELLOW)) {
			sprite = GameClient.cellAtlas.createSprite(FLOOR_YELLOW_TEXTURE.get(MathUtils.random(0,3)));
		}else if (tileType.equals(TileType.FLOOR_GREEN)){
			sprite = GameClient.cellAtlas.createSprite(FLOOR_GREEN_TEXTURE.get(MathUtils.random(0,3)));
		}

		sprite.setSize(width, height);
		sprite.setPosition(x - sprite.getWidth()/2, y - sprite.getHeight()/2);
		sprite.setOrigin(sprite.getWidth() / 2, sprite.getHeight() / 2);
		sprite.setRotation(rotation);
	}


	public TileType getTileType() {
		return tileType;
	}

	public void setTileType(TileType tileType) {
		this.tileType = tileType;
	}

	public void setRotation(float rotation) {
		//IoLogger.log("Rotation: " + rotation);
		//sprite.rotate(rotation);
		this.rotation = rotation;
	}

	public void setTexture(Texture texture) {
		this.texture = texture;
	}

	public void draw () {
		//batch.draw(texture, x, y);
//		shapeRenderer.begin();
		//shapeRenderer.set(ShapeRenderer.ShapeType.Filled);

		//shapeRenderer.setColor(tileColor);
		//if(currentZone.equals(TileType.WALL_BROWN) && sprite != null) {
			//batch.begin();
		try {
			sprite.draw(batch);
		}
		catch (Exception e) {

		}

			//batch.end();
		//}
		//else shapeRenderer.rect(x, y, width, height);
//

		//shapeRenderer.setColor(Color.valueOf("4b3c31"));
		//shapeRenderer.rect(x, y, width, height);
		/*texture = GameClient.getGame().getAssetManager().getTexture("1");
		batch.draw(texture, x, y, width, height);*/
	//	shapeRenderer.end();
		/*batch.begin();
		sprite.draw(batch);
		batch.end();*/
	}

	public String getId () {
		return id;
	}

	public String getName () {
		return name;
	}

	public float getX() {
		return x;
	}

	public void setX(float x) {
		this.x = x;
	}

	public float getY() {
		return y;
	}


	public int getI() {
		return i;
	}

	public void setI(int i) {
		this.i = i;
	}

	public int getJ() {
		return j;
	}

	public void setJ(int j) {
		this.j = j;
	}

	public void setY(float y) {
		this.y = y;
	}

	@Override
	public boolean collides(Cell another) {

		//collisionRadius = radius + radius * 10.0f/100.0f;
		//another.collisionRadius = another.radius + another.radius * 10.0f/100.0f;
		/*shapeRenderer.begin(ShapeRenderer.ShapeType.Line);
		shapeRenderer.rect(x, y , 10, 10);
		shapeRenderer.end();
		shapeRenderer.begin(ShapeRenderer.ShapeType.Line);
		shapeRenderer.rect(another.getX(), another.getY(), 10, 10);
		shapeRenderer.line(x , y , another.getX(),  another.getY());
		shapeRenderer.end();
		double distance = Math.hypot(x + width/2 - another.x , y + height/2 - another.y );
		double value = distance - (width/2) - another.getCollisionRadius()/1.5;*/
		return Math.hypot(x - another.x , y - another.y ) - (width/2) - another.getCollisionRadius()<=0;
	}

	@Override
	public boolean collides(AbstractProjectile another) {

		return Math.hypot(x - another.getCurrentPosition().x , y - another.getCurrentPosition().y ) - collisionRadius - another.radius <=0;
		//return Math.hypot(x - another.getCurrentPosition().x , y - another.getCurrentPosition().y ) - ((width + height)*2) - another.radius*2 <=0;
	}

	public void drawDebug() {
		shapeRenderer.circle(x, y, collisionRadius);
	}
}
