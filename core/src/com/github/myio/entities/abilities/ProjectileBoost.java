package com.github.myio.entities.abilities;



public class ProjectileBoost extends AbstractAbility {


    @Override
    public void start() {
        player.getCurrentWeapon().projectileBlueprint.speed *= 1.3;
    }

    @Override
    public void update() {

    }

    @Override
    public void end() {
        player.getCurrentWeapon().projectileBlueprint.speed /= 1.3;
    }
}
