package com.github.myio.entities.abilities;


import com.github.myio.entities.PlayerCell;

public abstract class AbstractAbility {


    public float duration; //in seconds
    public PlayerCell player;

    public abstract void start();
    public abstract void update();
    public abstract void end();

}
