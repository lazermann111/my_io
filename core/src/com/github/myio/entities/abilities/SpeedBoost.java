package com.github.myio.entities.abilities;



public class SpeedBoost extends AbstractAbility {
    @Override
    public void start() {
        player.movementSpeed *=1.3;
    }

    @Override
    public void update() {
     //shouldnt be rendered
    }

    @Override
    public void end() {
        player.movementSpeed /=1.3;
    }
}
