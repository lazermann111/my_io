package com.github.myio.entities;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.utils.Timer;
import com.github.myio.GameClient;
import com.github.myio.GameConstants;
import com.github.myio.entities.item.ItemCell;
import com.github.myio.entities.map.TacticalShield;
import com.github.myio.entities.projectiles.ParticlesPoolManager;
import com.github.myio.entities.weapons.AK47;
import com.github.myio.entities.weapons.AbstractRangeWeapon;
import com.github.myio.entities.weapons.Bazooka;
import com.github.myio.entities.weapons.FlashBang;
import com.github.myio.entities.weapons.Grenade;
import com.github.myio.entities.weapons.GrenadeLauncher;
import com.github.myio.entities.weapons.Knife;
import com.github.myio.entities.weapons.MachineGun;
import com.github.myio.entities.weapons.Pistol;
import com.github.myio.entities.weapons.ShotGun;
import com.github.myio.enums.ItemType;
import com.github.myio.enums.WeaponType;
import com.github.myio.inventory.InventoryManager;
import com.github.myio.networking.NetworkManager;
import com.github.myio.networking.packet.CurrentWorldSnapshotRequest;
import com.github.myio.networking.packet.HeartBeatPacket;
import com.github.myio.networking.packet.ItemPickedPacket;
import com.github.myio.networking.packet.PitInteractionPacker;
import com.github.myio.tools.IoLogger;
import com.github.myio.tools.SoundManager;
import com.github.myio.ui.ArmorBar;
import com.github.myio.ui.EmotesMenu;
import com.github.myio.ui.HealthBar;
import com.github.myio.ui.StaminaBar;

import java.util.ArrayList;
import java.util.List;

import static com.github.myio.GameConstants.EMOTE_PRESENTATION_TIME;
import static com.github.myio.GameConstants.PLAYERS_MIN_ARMOR;
import static com.github.myio.GameConstants.PLAYER_DEFAULT_RADIUS;
import static com.github.myio.GameConstants.PLAYER_DEFAULT_SPEED;
import static com.github.myio.GameConstants.PLAYER_INITIAL_SPEED;
import static com.github.myio.GameConstants.PLAYER_SIGHT_RADIUS;
import static com.github.myio.GameConstants.SERVER_SEND_UPDATES_RATE;
import static com.github.myio.GameConstants.TILE_HEIGHT;
import static com.github.myio.GameConstants.TILE_WIDTH;
import static com.github.myio.GameConstants.isBotSimulator;
import static com.github.myio.GameConstants.isServer;
import static com.github.myio.StringConstants.ARMOR_HELMET;
import static com.github.myio.StringConstants.GOGOGO;
import static com.github.myio.StringConstants.HP_100;
import static com.github.myio.StringConstants.HP_30;
import static com.github.myio.StringConstants.HP_60;
import static com.github.myio.StringConstants.OKLETSGO;
import static com.github.myio.StringConstants.SKIN_AK47;
import static com.github.myio.StringConstants.SKIN_BAZOOKA;
import static com.github.myio.StringConstants.SKIN_DEFAULT;
import static com.github.myio.StringConstants.SKIN_FLASHBANG;
import static com.github.myio.StringConstants.SKIN_FRAG_GRENADE;
import static com.github.myio.StringConstants.SKIN_GRENADE_LANCHER;
import static com.github.myio.StringConstants.SKIN_KNIFE;
import static com.github.myio.StringConstants.SKIN_MACHINEGUN;
import static com.github.myio.StringConstants.SKIN_PISTOL;
import static com.github.myio.StringConstants.SKIN_RED_DEFAULT;
import static com.github.myio.StringConstants.SKIN_SHOTGUN;

public class PlayerCell extends Cell
{

    private Vector2 forward = new Vector2(0, 0);
    private int speed = PLAYER_INITIAL_SPEED;
    private BitmapFont font;
    private String username;
    //private boolean isPredator = true;
    private long lastHeartbeat;
    private GameClient game;
    private int maxHealth, currentHealth;
    private float maxStamina, currentStamina;
    private int maxArmor = GameConstants.PLAYERS_MAX_ARMOR, currentArmor;
    //private int score; replaced by  PlayerSessionStatsDto.killsNumber
    private int squadId = -1; //no squad by default
    public float movementSpeed = PLAYER_DEFAULT_SPEED;
    private boolean isVisible = true;
    private boolean isLabelVisible = false;
    private String clientId;

    private Color squadColor;

    ArrayList<Sprite> sprites;
    int i = 0; // temp var

    float offsetX = 0;//temp var
    float offSetY = 0;//temp var

    float xOr = 121;
    float yOr = 147;

    private IoPoint gunEnd = new IoPoint();
    private boolean isRotatedAfterShot = true;

    //private InventoryManager inventoryManager;
    //private ItemDetails itemDetails;
    private Image emoteImg;
    private Sprite armorSprite;
    private ArrayList<String> mEmotesName;
    private HealthBar mHealthBar;
    private ArmorBar mArmorBar;

    private StaminaBar mStaminaBar;



    //rotation after step
    private boolean isLeft = false;
    private boolean isRotatedAfterStep = true;
    private float angleForStepRotation = 1;
    private short knifeKickAngle = 0;

    private GroundType playerGroundType = GroundType.MUD;
    private String playerGroundTypeSound = SoundManager.FOOTSTEPS_MUD;

    public String pit;
    private boolean isAim = false;

    public boolean isUsingInventory = false;
    public String playerSkinColor;

    // we can disable heartbeats sending from server for dead players, if he still wants to be on server and spectate
    private boolean canSendHeartBeats = true;

    private InventoryManager mInventoryManager;

    protected ParticleEffect deathEffect;
    protected ParticleEffect bloodEffect;


    private TacticalShield interectedTacticalShild;
    private boolean deathEffectStarted;
    private boolean bloodEffectStarted;


    private boolean suppressed = false;
    private String killerId;//temp variable, to save enemy you was injured by
    private IoPoint positionBeforeInteraction;



    public PlayerCell()
    {
        radius = PLAYER_DEFAULT_RADIUS;
        setCollisionRadius(radius + radius * 10.0f / 100.0f);
        setInsidePit(false);
    }

    private float accelerationMultiplier = 1, rotationMultiplier = 1;

    public TacticalShield shiled; // todo dirty atlasAll, should be in InventoryManager


    public PlayerCell(String username, String id, float x, float y)
    {
        //super(batch, shapeRenderer, d, e, mass, hue);
        this.username = username;
        this.id = id;
        this.x = x;
        this.y = y;
        radius = PLAYER_DEFAULT_RADIUS;

        currentHealth = GameConstants.PLAYERS_MAX_HEALTH;
        maxHealth = GameConstants.PLAYERS_MAX_HEALTH;

        currentStamina = GameConstants.PLAYERS_MAX_STAMINA;
        maxStamina = GameConstants.PLAYERS_MAX_STAMINA;


    }

    public PlayerCell(SpriteBatch batch, ShapeRenderer shapeRenderer,
                      BitmapFont font, String username, float x, float y, int radius, String id,
                      int maxHealth, int currentHealth, float maxStamina, float currentStamina, int score, String playerSkinCollor)
    {
        super(batch, shapeRenderer, x, y, radius, id);
        this.username = username;
        this.playerSkinColor = playerSkinCollor;

        this.maxHealth = maxHealth;
        this.currentHealth = currentHealth;

        this.maxStamina = maxStamina;
        this.currentStamina = currentStamina;



        //this.score = score;

        if (font == null)
        {
            return; // bot case
        }
        this.font = font;
        this.glyphLayout = new GlyphLayout();
        this.glyphLayout.setText(font, username);


        // playerSkinCollor = SKIN_DEFAULT;


        sprite = GameClient.playerSkin.createSprite(playerSkinCollor + SKIN_KNIFE);
        sprite.setOrigin(xOr, yOr);
        gunEnd = new IoPoint(130f, 50);
        /*sprite.setSize(radiusItem*2, radiusItem*2);
        sprite.setOrigin(sprite.getWidth()/2, sprite.getHeight()/2);*/


        //itemDetails = new ItemDetails();
        if (!isServer)
        {
            mHealthBar = new HealthBar(maxHealth);

            mStaminaBar = new StaminaBar(maxStamina);

            mArmorBar = new ArmorBar(maxArmor);
            armorSprite = GameClient.playerSkin.createSprite(ARMOR_HELMET); // todo replace
            armorSprite.setOrigin(xOr, yOr);
            bloodEffect = new ParticleEffect();
            bloodEffect.load(Gdx.files.internal("particles/bloodSplash_3"), Gdx.files.internal("particles"));


            String deathFxString = deathFxAssetPath();
            deathEffect = new ParticleEffect();
            deathEffect.load(Gdx.files.internal(deathFxString), Gdx.files.internal("particles"));

        }
    }


    private String deathFxAssetPath()

    {
        if (playerSkinColor.equals(SKIN_DEFAULT))
        { return "particles/deathBlow.p"; }

        if (playerSkinColor.equals(SKIN_RED_DEFAULT))
        { return "particles/deathBlow.p"; }

        return "particles/deathBlow.p";
    }

    public void bloodSplash(IoPoint position)
    {
        bloodEffectStarted = true;
        //  bloodEffect = particlesPoolManager.obtainParticleEffect(StringConstants.DEATH_EFFECT);
        bloodEffect.setPosition(position.x, position.y);
        //  IoLogger.log("PlayerCell.deathEffect " + getPosition().x + " | " + getPosition().y);
        bloodEffect.start();
    }

    public boolean isDead(boolean dead)
    {
        return dead;
    }

    public Vector2 getForward()
    {
        return forward;
    }

    public boolean isAim()
    {
        return isAim;
    }

    public int getSpeed()
    {
        return speed;
    }

    public void setSpeed(int speed)
    {
        this.speed = speed;
    }

    public void setAim(boolean aim)
    {
        isAim = aim;
    }

    public float getAngleForStepRotation()
    {
        if (isLeft)
        {
            angleForStepRotation -= 0.5;
            if (angleForStepRotation == -5)
            { isLeft = false; }
        }
        else
        {
            angleForStepRotation += 0.5;
            if (angleForStepRotation == 5)
            { isLeft = true; }
        }
        return angleForStepRotation;
    }

    public boolean isRotatedAfterStep()
    {
        return isRotatedAfterStep;
    }

    public void setRotatedAfterStep(boolean rotatedAfterStep)
    {
        isRotatedAfterStep = rotatedAfterStep;
    }

    private int currentPlayerAlpha = 1;

    public void setPlayerAlpha(int a)
    {
        currentPlayerAlpha = a;
        sprite.setAlpha(a);
        armorSprite.setAlpha(a);
    }

    public void removePlayerAlpha()
    {
        currentPlayerAlpha = 1;
        sprite.setAlpha(1);
        armorSprite.setAlpha(1);
    }

    public void voiceCommandOteherPlayers(String comand){

       if (comand.equals(GOGOGO))
       {
           GameClient.getGame().getSoundManager().playSoundWithPanAndVolume(SoundManager.GO_GO_GO,
                   new IoPoint(GameClient.getGame().getCamera().position.x, GameClient.getGame().getCamera().position.y), new IoPoint(x, y), 4);
       }
       else if(comand.equals(OKLETSGO)){
           GameClient.getGame().getSoundManager().playSoundWithPanAndVolume(SoundManager.OKEY_LETS_GO,
                   new IoPoint(GameClient.getGame().getCamera().position.x, GameClient.getGame().getCamera().position.y), new IoPoint(x, y), 4);
       }
    }

    public static float interpolation_const = 0.55f;

	/*short offset = 0;
            if(Math.abs(rotationPrev - rotationAngle) < 180) {
        		offset = -1;
			}
			else if(Math.abs(rotationPrev - rotationAngle) > 180)
				offset = 1;
        	else if(rotationPrev - rotationAngle < 3 && rotationPrev - rotationAngle > -3 )
        		offset = 0;
        	rotationVelocity = offset;
        	if(offset != 0)
        		rotationInterpolated = rotationPrev + offset;
        	else
        		rotationInterpolated = rotationAngle;*/

    private short tempPrevRotation;


    public void localRendererDraw(float delta)
    {
        velocityX = x - xPrevious;//for shooting scatter
        velocityY = y - yPrevious;
        if (!hasShield())
        {
            /*rotationVelocity = rotationAngle - rotationPrev;
            rotationInterpolated = rotationPrev + rotationVelocity * delta * interpolation_const * 1000 / SERVER_SEND_UPDATES_RATE;
			tempPrevRotation = (short) rotationInterpolated;*/
            //todo Nikita add shield rotation logic
            sprite.setRotation(rotationAngle + 180);  //todo (Igor) no magic numbers
            tempPrevRotation = (short) rotationAngle;
            rotationInterpolated = rotationAngle;
        }
        else
        {
            short offset = 0;
            if (rotationAngle % 2 != 0)
            { rotationAngle++; }

            if (tempPrevRotation > 0 && tempPrevRotation < 89 && rotationAngle < -90)
            {
                offset = 2;
            }
            else if (tempPrevRotation >= 90 && rotationAngle < -90)
            {
                offset = 2;
                if (tempPrevRotation > 180)
                { tempPrevRotation = -180; }
            }
            else if (tempPrevRotation < -90 && rotationAngle > 90)
            {
                if (tempPrevRotation < -180)
                { tempPrevRotation = 180; }
                offset = -2;
            }
            else if (tempPrevRotation < rotationAngle)
            {
                offset = 2;
            }
            else if (tempPrevRotation > rotationAngle)
            { offset = -2; }
            else if (tempPrevRotation == rotationAngle)
            { offset = 0; }

            rotationInterpolated = tempPrevRotation + offset;

            tempPrevRotation = (short) rotationInterpolated;
            sprite.setRotation(tempPrevRotation + 180);  //todo (Igor) no magic numbers*/
        }
        sprite.setPosition(x - sprite.getOriginX(), y - sprite.getOriginY());
        sprite.draw(batch);

        if (deathEffectStarted)
        {
            if (deathEffect.isComplete())
            {
                deathEffectStarted = false;
            }
            else
            {
                deathEffect.draw(batch, delta);
            }
        }

        if (bloodEffectStarted)
        {
            if (bloodEffect.isComplete())
            {
                bloodEffectStarted = false;
            }
            else
            {
                bloodEffect.draw(batch, delta);
            }
        }
        if (getCurrentArmor() > PLAYERS_MIN_ARMOR)
        {
            armorSprite.setPosition(x - sprite.getOriginX(), y - sprite.getOriginY());
            armorSprite.setRotation(rotationAngle + 180);
            armorSprite.draw(batch);
        }
        else  {

            armorSprite.setPosition(xInterploated - sprite.getOriginX(), yInterpoldated - sprite.getOriginY());

        }
        if (emoteImg != null)
        {

            emoteImg.setPosition(sprite.getX() + 60, sprite.getY() + 300);
            emoteImg.draw(batch, 1);
        }

        if (isLabelVisible)
        {
            BitmapFont font = GameClient.getFont32White();
            Label.LabelStyle style = new Label.LabelStyle(font, Color.GREEN);
            Label label = new Label(username, style);
            label.setPosition(sprite.getX() + sprite.getWidth() / 2, sprite.getY() - 15);
            label.draw(batch, 1);
        }

        xPrevious = xInterploated;
        yPrevious = yInterpoldated;
    }

    @Override
    public void rendererDraw(float delta)
    {

        velocityX = x - xPrevious;
        velocityY = y - yPrevious;
        //rotationVelocity = rotationAngle - rotationPrev;
        if (!hasShield())
        {
            rotationVelocity = rotationAngle - rotationPrev;
            rotationInterpolated = rotationPrev + rotationVelocity * delta * interpolation_const * 1000 / SERVER_SEND_UPDATES_RATE;
            tempPrevRotation = (short) rotationInterpolated;
        }
        else
        {
            short offset = 0;
            if (rotationAngle % 2 != 0)
            { rotationAngle++; }

            if (tempPrevRotation > 0 && tempPrevRotation < 89 && rotationAngle < -90)
            {
                offset = 2;
            }
            else if (tempPrevRotation >= 90 && rotationAngle < -90)
            {
                offset = 2;
                if (tempPrevRotation > 180)
                { tempPrevRotation = -180; }
            }
            else if (tempPrevRotation < -90 && rotationAngle > 90)
            {
                if (tempPrevRotation < -180)
                { tempPrevRotation = 180; }
                offset = -2;
            }
            else if (tempPrevRotation < rotationAngle)
            {
                offset = 2;
            }
            else if (tempPrevRotation > rotationAngle)
            { offset = -2; }
            else if (tempPrevRotation == rotationAngle)
            { offset = 0; }

            rotationInterpolated = tempPrevRotation + offset;

            tempPrevRotation = (short) rotationInterpolated;
        }

        xInterploated = xPrevious + velocityX * delta * interpolation_const * 1000 / SERVER_SEND_UPDATES_RATE;
        yInterpoldated = yPrevious + velocityY * delta * interpolation_const * 1000 / SERVER_SEND_UPDATES_RATE;
        // rotationInterpolated =rotationPrev + rotationVelocity * delta *interpolation_const* 1000/SERVER_SEND_UPDATES_RATE;

        //IoLogger.log("xInterploated = " + xInterploated);
        //IoLogger.log("yInterpoldated = " + yInterpoldated);

        //shapeRenderer.setAutoShapeType(true);
        //shapeRenderer.set(ShapeRenderer.ShapeType.Line);
        //shapeRenderer.circle(xInterploated, yInterpoldated, getCollisionRadius());//Debug circle

        //sprite.setOrigin(sprite.getWidth()/2, sprite.getHeight()/2);
        //sprite.setPosition(xInterploated - radius, yInterpoldated-radius);
        sprite.setPosition(xInterploated - sprite.getOriginX(), yInterpoldated - sprite.getOriginY());
        //sprite.setSize(radius*2, radius*2);
        sprite.setRotation(rotationInterpolated + 180);  //todo (Igor) no magic numbers

        //batch.begin();
        sprite.draw(batch);

        if (deathEffectStarted)
        {

            if (deathEffect.isComplete())
            {
                deathEffectStarted = false;
            }
            else{
                deathEffect.draw(batch, delta);
               // deathPicture.draw(batch);
            }
        }

        if (bloodEffectStarted)
        {
            if (bloodEffect.isComplete())
            {
                bloodEffectStarted = false;
            }
            else
            {
                bloodEffect.draw(batch, delta);
            }
        }
        //batch.end();

        if (getCurrentArmor() > PLAYERS_MIN_ARMOR) {
            armorSprite.setPosition(xInterploated - sprite.getOriginX(), yInterpoldated - sprite.getOriginY());
            armorSprite.setRotation(rotationAngle + 180);
            armorSprite.draw(batch);
        }
        else {
            armorSprite.setPosition(xInterploated - sprite.getOriginX(), yInterpoldated - sprite.getOriginY());
        }

        if (emoteImg != null)
        {
            //batch.begin();
            emoteImg.setScale(2);
            emoteImg.setPosition(sprite.getX() + 60, sprite.getY() + 300);
            emoteImg.draw(batch, 1);
            //batch.end();
        }

        if (isLabelVisible)
        {
            BitmapFont font = GameClient.getFont32White();
            Label.LabelStyle style = new Label.LabelStyle(font, Color.GREEN);
            Label label = new Label(username, style);
            label.setPosition(sprite.getX() , sprite.getY() + sprite.getHeight() + 5);
            label.draw(batch, 1);
        }

        xPrevious = xInterploated;
        yPrevious = yInterpoldated;
        rotationPrev = rotationInterpolated;

    }

    public float getRotationInterpolated()
    {
        return rotationInterpolated;
    }


    public void deathEffect(ParticlesPoolManager particlesPoolManager)
    {
        deathEffectStarted = true;
        // deathEffect = particlesPoolManager.obtainParticleEffect(StringConstants.DEATH_EFFECT);
        deathEffect.setPosition(getPosition().x, getPosition().y);
        IoLogger.log("PlayerCell.deathEffect " + getPosition().x + " | " + getPosition().y);
        deathEffect.start();



       /* deathEffectStarted = true;
        ParticleEffect effect = new ParticleEffect();
		effect.load(Gdx.files.internal("particles/DeathBlowYellow.p"),Gdx.files.internal("particles"));
		effect.setPosition(getPosition().x,getPosition().y);
		IoLogger.log("PlayerCell.deathEffect "+getX()+" | "+getY());
		effect.start();*/
    }

    public void drawCircle()
    {
        ShapeRenderer shape = new ShapeRenderer();
        shape.begin(ShapeRenderer.ShapeType.Filled);
        shape.circle(xInterploated - radius + offsetX, yInterpoldated - radius + offSetY, 100);
        shape.setColor(Color.BLACK);
        shape.end();
    }

    Color gridColor = Color.LIGHT_GRAY.mul(1, 1, 1, 0.2f);


    public void setDefaultInventory(boolean hasVisuals, boolean isClient)
    {
        Knife knife = new Knife(null, this);

        ArrayList abstractWeaponList = new ArrayList<AbstractRangeWeapon>();
        abstractWeaponList.add(null);
        abstractWeaponList.add(null);
        abstractWeaponList.add(knife);
        abstractWeaponList.add(null);
        abstractWeaponList.add(null);

        InventoryManager inventoryManager = new InventoryManager(this, hasVisuals, isClient, abstractWeaponList);
        setInventoryManager(inventoryManager);

        setCurrentWeapon(knife);
    }

    public void drawGrid()
    {


        float xMin = x - PLAYER_SIGHT_RADIUS - (x - PLAYER_SIGHT_RADIUS) % TILE_WIDTH;
        float xMax = x + PLAYER_SIGHT_RADIUS - (x + PLAYER_SIGHT_RADIUS) % TILE_WIDTH;

        float yMin = y - PLAYER_SIGHT_RADIUS - (y - PLAYER_SIGHT_RADIUS) % TILE_HEIGHT;
        float yMax = y + PLAYER_SIGHT_RADIUS - (y + PLAYER_SIGHT_RADIUS) % TILE_HEIGHT;

        shapeRenderer.setColor(Color.BLACK);

        for (float i = 0;
             i < xMax - xMin;
             i += TILE_WIDTH)
        {
            shapeRenderer.rectLine(xMin, yMin + i, xMax, yMin + i, 2);
            for (float j = 0;
                 j < yMax - yMin;
                 j += TILE_HEIGHT)
            {
                shapeRenderer.rectLine(xMin + j, yMin, xMin + j, yMax, 2);
            }

        }

    }

    public void drawPlayerLabel()
    {
		/*font.draw(batch, getPosition().toString(), (float) xInterploated - glyphLayout.width / 2,
				(float) yInterpoldated + glyphLayout.height / 2);*/
		/*font.draw(batch, ".", (float) sprite.getX() + sprite.getOriginX(),
				(float) sprite.getY() + sprite.getOriginY());*/
        isLabelVisible = true;
    }

    public void showMyEmote(String checkedEmoteName)
    {
        emoteImg = new Image(GameClient.uiAtlas.createSprite(checkedEmoteName));
        emoteImg.setName(checkedEmoteName);
        Timer.schedule(
                new Timer.Task()
                {
                    @Override
                    public void run()
                    {
                        emoteImg = null;
                        IoLogger.log(username, "checkedEmote is deleted");
                    }
                },
                EMOTE_PRESENTATION_TIME);
    }


    public String getUsername()
    {
        return this.username;
    }

    public void setId(String i)
    {
        this.id = i;
    }

    public String getId()
    {
        return this.id;
    }

    public void setPosition(IoPoint point)
    {


        xPrevious = x;
        yPrevious = y;
        x = point.getX();
        y = point.getY();
        //IoLogger.log("setPosition  xPrevious = " + xPrevious);
        //IoLogger.log("setPosition  yPrevious = " + yPrevious);
        //IoLogger.log("setPosition  x = " + x);
        //IoLogger.log("setPosition  y = " + y);
    }


    public IoPoint getFireCoords()
    {
        Vector2 vector2 = new Vector2(sprite.getX() + gunEnd.getX() - x, sprite.getY() + gunEnd.getY() - y);
        vector2.rotate(rotationAngle + 180);
        return new IoPoint(vector2.x + x, vector2.y + y);
    }

    public Vector3 getPositionVec()
    {
        return new Vector3(x, y, 0);
    }

    public boolean isVisible()
    {
        return isVisible;
    }

    public void setVisible(boolean visible)
    {
        isVisible = visible;
    }


    public void sendHeartBeat(NetworkManager networkManager)
    {
        if (lastHeartbeat + GameConstants.HEARTBEAT_RATE > System.currentTimeMillis()){ return; }
        if (!canSendHeartBeats){ return; }
        networkManager.sendPacket(new HeartBeatPacket(id, x, y, (int) rotationInterpolated, HeartBeatPacket.PLAYER));
        //networkManager.sendPacket(new HeartBeatPacket(id, x, y));
        lastHeartbeat = System.currentTimeMillis();
        //IoLogger.log("Sent heartbeat");
    }

    public void forceHeartBeat(NetworkManager networkManager)
    {
        if (!canSendHeartBeats){ return; }
        networkManager.sendPacket(new HeartBeatPacket(id, x, y, (int) rotationInterpolated, HeartBeatPacket.PLAYER));
    }

    public void consumeItem(NetworkManager networkManager, ItemCell item)
    {
        if (item.getItemType().equals(ItemType.WEAPON))
        {
            if (playerAlreadyHasThisWeapon(item))
            {
                //todo:show label that player already has this weapon
                IoLogger.log("We already have this weapon!!!");
                return;
            }
        }
        if (item.getItemType().equals(ItemType.HP))
        {
            networkManager.sendPacket(new ItemPickedPacket(item.getId(), id));
            return;
        }


        networkManager.sendPacket(new ItemPickedPacket(item.getId(), id));

        if (isBotSimulator){ return; }


        Camera gameCam = GameClient.getGame().getCamera();
        switch (item.getItemType())
        {
            case HP:
                GameClient.getGame().getSoundManager().playSoundWithPanAndVolume(SoundManager.HP,
                        new IoPoint(gameCam.position.x, gameCam.position.y), getPosition());
                break;
            case PATRON:
                GameClient.getGame().getSoundManager().playSoundWithPanAndVolume(SoundManager.PATRON,
                        new IoPoint(gameCam.position.x, gameCam.position.y), getPosition());
                break;
            case WEAPON:
                GameClient.getGame().getSoundManager().playSoundWithPanAndVolume(SoundManager.WEAPON,
                        new IoPoint(gameCam.position.x, gameCam.position.y), getPosition());
                break;
            case ARMOR:
                GameClient.getGame().getSoundManager().playSoundWithPanAndVolume(SoundManager.ARMOR,
                        new IoPoint(gameCam.position.x, gameCam.position.y), getPosition());
                break;
            case AMMUNITION:
                GameClient.getGame().getSoundManager().playSoundWithPanAndVolume(SoundManager.AMMUNITION,
                        new IoPoint(gameCam.position.x, gameCam.position.y), getPosition());
                break;
        }


    }

    private boolean playerAlreadyHasThisWeapon(ItemCell item)
    {
        AbstractRangeWeapon weaponInstance = AbstractRangeWeapon.Factory(item.getItem_blueprint(),
                shapeRenderer, this);
        if (weaponInstance == null)
        {
            Gdx.app.error("ConsumeItem", "Wrong Item_blueprint");
            return false;
        }

        for (AbstractRangeWeapon weaponTemp : getWeaponList())
        {
            if (weaponTemp == null)
            { continue; }
            if (weaponTemp.getClass() == weaponInstance.getClass())
            {
                if (weaponInstance instanceof Grenade || weaponInstance instanceof FlashBang)
                {
                    if (weaponTemp.clipAmmoAmount < 2)
                    { return false; }
                }
                if (weaponTemp.totalAmmoAmount != weaponTemp.getMaxAmmoAmount())
                { return false; }
                //todo: add label that player already have this weapon
                return true;
            }
        }
        return false;
    }

	/*public void dealDamage(NetworkManager networkManager, String localPlayerId, String enemyId) {
		networkManager.sendPacket(new DealDamagePacket(localPlayerId, enemyId));

	}*/


    public void setUsername(String username)
    {
        this.username = username;
    }

    public int getMaxHealth()
    {
        return maxHealth;
    }

    public void setMaxHealth(int maxHealth)
    {
        this.maxHealth = maxHealth;
    }

    public float getMaxStamina() {
        return maxStamina;
    }

    public void setMaxStamina(float maxStamina) {
        this.maxStamina = maxStamina;
    }

    public float getCurrentStamina() {
        return currentStamina;
    }

    public void setCurrentStamina(float currentStamina) {
        this.currentStamina = currentStamina;
        if(!isServer && !isBotSimulator)
        {
            mStaminaBar.setStamina((float)currentStamina / maxStamina);
        }
    }

    public int getCurrentHealth()
    {
        return currentHealth;
    }

    public void setCurrentHealth(int currentHealth)
    {
        this.currentHealth = currentHealth;
        if (!isServer && !isBotSimulator)
        {
            mHealthBar.setHealth((float) currentHealth / maxHealth);
        }

    }


//	public int getScore() {
//		return score;
//	}
//
//	public void setScore(int score) {
//		this.score = score;
//	}

    @Override
    public String toString()
    {
        return "PlayerCell{" +
                "username='" + username + '\'' +
                ", id=" + id +
                ", currentHealth=" + currentHealth +
                ", squadId=" + squadId +
                "} " + super.toString();
    }


    public AbstractRangeWeapon getCurrentWeapon()
    {
       // return getInventoryManager().getCurrentWeapon();
        return currentWeapon;
    }

    private AbstractRangeWeapon currentWeapon;

    public void setCurrentWeapon(AbstractRangeWeapon currentWeapon)
    {
        if (mInventoryManager != null) //null for all players except local player, so method just switches skins in case of enemies
        {
            mInventoryManager.setCurrentWeapon(currentWeapon);
        }
        this.currentWeapon = currentWeapon;
        changeSkin(currentWeapon);
    }


    public void changeSkin(AbstractRangeWeapon currentWeapon)
    {

        if (isServer || isBotSimulator){ return; }
        if (currentWeapon instanceof MachineGun)
        {
            sprite = GameClient.playerSkin.createSprite(playerSkinColor + SKIN_MACHINEGUN);

            sprite.setOrigin(xOr, yOr);
            gunEnd = new IoPoint(168, 105);
        }
        else  if (currentWeapon instanceof AK47)
        {
            sprite = GameClient.playerSkin.createSprite(playerSkinColor + SKIN_AK47);

            sprite.setOrigin(xOr, yOr);
            gunEnd = new IoPoint(168, 105);
        }
        else if (currentWeapon instanceof Knife)
        {
            sprite = GameClient.playerSkin.createSprite(playerSkinColor + SKIN_KNIFE);

            sprite.setOrigin(xOr, yOr);
            gunEnd = new IoPoint(130f, 50);
        }
        else if (currentWeapon instanceof ShotGun)
        {
            sprite = GameClient.playerSkin.createSprite(playerSkinColor + SKIN_SHOTGUN);

            sprite.setOrigin(xOr, yOr);
            gunEnd = new IoPoint(180, 163);
        }
        else if (currentWeapon instanceof Pistol)
        {
            sprite = GameClient.playerSkin.createSprite(playerSkinColor + SKIN_PISTOL);

            sprite.setOrigin(xOr, yOr);
            gunEnd = new IoPoint(178, 47);
        }
        else if (currentWeapon instanceof Bazooka)
        {

            sprite = GameClient.playerSkin.createSprite(playerSkinColor + SKIN_BAZOOKA);

            sprite.setOrigin(xOr, yOr);
            gunEnd = new IoPoint(195, 162);
        }
        else if (currentWeapon instanceof Grenade)
        {

            sprite = GameClient.playerSkin.createSprite(playerSkinColor + SKIN_FRAG_GRENADE);

            sprite.setOrigin(xOr, yOr);
            gunEnd = new IoPoint(188.5f, 60);
        }
        else if (currentWeapon instanceof FlashBang)
        {

            sprite = GameClient.playerSkin.createSprite(playerSkinColor + SKIN_FLASHBANG);

            sprite.setOrigin(xOr, yOr);
            gunEnd = new IoPoint(188.5f, 60);
        }
        else if (currentWeapon instanceof GrenadeLauncher)
        {

            sprite = GameClient.playerSkin.createSprite(playerSkinColor+ SKIN_GRENADE_LANCHER);

            sprite.setOrigin(xOr, yOr);
            gunEnd = new IoPoint(175, 115);

        }
        sprite.setAlpha(currentPlayerAlpha);
    }

    public void setCurrentWeapon(WeaponType type)
    {
        for (AbstractRangeWeapon w : getWeaponList())
        {
            if (w == null)
            { continue; }
            if (w.weaponType.equals(type))
            {
                setCurrentWeapon(w);
                changeSkin(type);
                break;
            }
        }
    }

    public boolean waitingForCurrentWorldSnapshot;

    public void requestWorldSnapshot(NetworkManager networkManager)
    {
        if (waitingForCurrentWorldSnapshot){ return; }

        waitingForCurrentWorldSnapshot = true;
        networkManager.sendPacket(new CurrentWorldSnapshotRequest());
    }

    private void changeSkin(WeaponType type)
    { // For enemies
        if (isServer || isBotSimulator){ return; }
        if (type.equals(WeaponType.SHOTGUN))
        {

            sprite = GameClient.playerSkin.createSprite(playerSkinColor + SKIN_SHOTGUN);

            sprite.setOrigin(xOr, yOr);
            gunEnd = new IoPoint(175, 115);

        }
        else if (type.equals(WeaponType.KNIFE))
        {

            sprite = GameClient.playerSkin.createSprite(playerSkinColor + SKIN_KNIFE);

            sprite.setOrigin(xOr, yOr);
            gunEnd = new IoPoint(130f, 50);

        }
        else if (type.equals(WeaponType.MACHINE_GUN))
        {

            sprite = GameClient.playerSkin.createSprite(playerSkinColor + SKIN_MACHINEGUN);

            sprite.setOrigin(xOr, yOr);
            gunEnd = new IoPoint(175, 120);
        }
        else if (type.equals(WeaponType.PISTOL))
        {

            sprite = GameClient.playerSkin.createSprite(playerSkinColor + SKIN_PISTOL);

            sprite.setOrigin(xOr, yOr);
            gunEnd = new IoPoint(169, 55);
        }
        else if (type.equals(WeaponType.ROCKET))
        {

            sprite = GameClient.playerSkin.createSprite(playerSkinColor + SKIN_BAZOOKA);

            sprite.setOrigin(xOr, yOr);
            gunEnd = new IoPoint(185, 175);

        }
        else if (type.equals(WeaponType.FRAG_GRENADE))
        {

            sprite = GameClient.playerSkin.createSprite(playerSkinColor + SKIN_FRAG_GRENADE);

            sprite.setOrigin(xOr, yOr);
            gunEnd = new IoPoint(188.5f, 30);
        }
        else if (type.equals(WeaponType.FLASHBANG))
        {

            sprite = GameClient.playerSkin.createSprite(playerSkinColor + SKIN_FLASHBANG);

            sprite.setOrigin(xOr, yOr);
            gunEnd = new IoPoint(188.5f, 30);
        }
        else if (type.equals(WeaponType.GRENADE_LAUNCHER))
        {

            sprite = GameClient.playerSkin.createSprite(playerSkinColor+ SKIN_GRENADE_LANCHER);

            sprite.setOrigin(xOr, yOr);
            gunEnd = new IoPoint(150, 60);
        }

        sprite.setAlpha(currentPlayerAlpha);
    }

    public void setEnemyCurrentWeapon(WeaponType type, PlayerCell owner)
    {
        //currentWeapon = AbstractRangeWeapon.Factory(type.getIntValue(), shapeRenderer, owner);
        changeSkin(type);
    }

    public void setWeaponList(List<AbstractRangeWeapon> weaponList)
    {
        this.getInventoryManager().setWeaponList(weaponList);
    }

    public List<AbstractRangeWeapon> getWeaponList()
    {
        return getInventoryManager().getWeaponList();
    }


    private int markersAvailable = 5;

    public Marker createMarker()
    {
        if (markersAvailable <= 0){ return null; }

        --markersAvailable;
        Marker m = new Marker(batch, shapeRenderer, x, y, radius, markersAvailable + "", rotationAngle);

        Camera gameCam = GameClient.getGame().getCamera();
        GameClient.getGame().getSoundManager().playSoundWithPanAndVolume(SoundManager.MARKER,
                new IoPoint(gameCam.position.x, gameCam.position.y), getPosition());


        return m;
    }
    public Grave createGrave()
    {
        Grave m = new Grave(batch, shapeRenderer, x, y, radius, markersAvailable + "", rotationAngle);



        return m;
    }


//	public void nextTexture() {
//		i++;
//		if(i == 7) {
//			i = 0;
//		}
//		texture = textures.get(i);
//		if(textures.get(i).equals(GameClient.getGame().getAssetManager().getTexture(BLUE_NO_WEAPON))) {
//			sprite.setSize(215, 195);
//			sprite.setOrigin(112, 95);
//			gunEnd = new IoPoint(195, 210);
//		}
//		if(textures.get(i).equals(GameClient.getGame().getAssetManager().getTexture(BLUE_SKIN_BAZOOKA))) {
//			sprite.setSize(215, 390);
//			sprite.setOrigin(108, 186);
//			gunEnd = new IoPoint(185, 420);
//		}
//		if(textures.get(i).equals(GameClient.getGame().getAssetManager().getTexture(BLUE_SKIN_GRENADE))) {
//			sprite.setSize(215, 190);
//			sprite.setOrigin(112, 90);
//			gunEnd = new IoPoint(185, 210);
//		}
//		if(textures.get(i).equals(GameClient.getGame().getAssetManager().getTexture(BLUE_SKIN_FLASHBANG))) {
//			sprite.setSize(215, 190);
//			sprite.setOrigin(112, 90);
//			gunEnd = new IoPoint(185, 210);
//		}
//		if(textures.get(i).equals(GameClient.getGame().getAssetManager().getTexture(BLUE_SKIN_KNIFE))) {
//			sprite.setSize(295, 190);
//			sprite.setOrigin(108, 85);
//			gunEnd = new IoPoint(190, 205);
//		}
//		if(textures.get(i).equals(GameClient.getGame().getAssetManager().getTexture(BLUE_SKIN_MACHINE_GUN))) {
//			sprite.setSize(230, 310);
//			sprite.setOrigin(110, 90);
//			gunEnd = new IoPoint(165, 330);
//		}
//		if(textures.get(i).equals(GameClient.getGame().getAssetManager().getTexture(BLUE_SKIN_PISTOL))) {
//			sprite.setSize(215, 245);
//			sprite.setOrigin(112, 90);
//			gunEnd = new IoPoint(160, 265);
//		}
//		if(textures.get(i).equals(GameClient.getGame().getAssetManager().getTexture(BLUE_SKIN_SHOTGUN))) {
//			sprite.setSize(215, 295);
//			sprite.setOrigin(112, 90);
//			gunEnd = new IoPoint(165, 315);
//		}
//		sprite.setTexture(texture);
//	}





    public boolean isMoving()
    {
        return velocityX != 0 || velocityY != 0;
    }


    public boolean isRotatedAfterShot()
    {
        return isRotatedAfterShot;
    }

    public void setRotatedAfterShot(boolean rotatedAfterShot)
    {
        isRotatedAfterShot = rotatedAfterShot;
    }

    public EmotesMenu getEmotesMenu()
    {
        return new EmotesMenu(mEmotesName);
    }

    public void setEmoteNames(ArrayList<String> list)
    {
        mEmotesName = list;
    }

    public boolean hasArmor()
    {
        if (getCurrentArmor()<= PLAYERS_MIN_ARMOR)
        {
            return false;
        }else return true;
    }
    public HealthBar getHealthBar() {return mHealthBar;}

    public StaminaBar getStaminaBar(){return  mStaminaBar;}


    public ArmorBar getmArmorBar() {return mArmorBar;}

    public int getCurrentArmor()
    {
        return currentArmor;
    }

    public void setCurrentArmor(int currentArmor)
    {
        this.currentArmor = currentArmor;
        if (!isServer && !isBotSimulator){ mArmorBar.setArmor((float) currentArmor / maxArmor); }

    }

    public int getMaxArmor() {return maxArmor;}

    public void setMaxArmor(int maxArmor)
    {
        this.maxArmor = maxArmor;
    }

    public void rendererDrawDebug(float delta)
    {
        shapeRenderer.circle(xInterploated, yInterpoldated, getCollisionRadius());
    }

    public boolean hasShield()
    {
        return shiled != null;
    }

    public TacticalShield getInterectedTacticalShild()
    {
        return interectedTacticalShild;
    }

    public void setInterectedTacticalShild(TacticalShield interectedTacticalShild)
    {
        this.interectedTacticalShild = interectedTacticalShild;
    }

    public float getAccelerationMultiplier()
    {
        return accelerationMultiplier;
    }

    public void setAccelerationMultiplier(float accelerationMultiplier)
    {
        this.accelerationMultiplier = accelerationMultiplier;
    }

    public float getRotationMultiplier()
    {
        return rotationMultiplier;
    }

    public void setRotationMultiplier(float rotationMultiplier)
    {
        this.rotationMultiplier = rotationMultiplier;
    }

    private boolean moveBack = false;

    public float getKnifeKickAngle()
    {
        if (!moveBack)
        {
            knifeKickAngle += 10;
            if (knifeKickAngle >= 80)
            {
                moveBack = true;
            }
        }
        else
        {
            knifeKickAngle -= 20;
            if (knifeKickAngle <= 0)
            {
                moveBack = false;
                knifeKickAngle = 0;
                setRotatedAfterShot(true);
            }
        }
        return knifeKickAngle;
    }

	/*@Override
	public boolean collides(Cell another)
	{
		if(another instanceof MapEntityCell && !(another instanceof TacticalShield)) {
			if(!insidePit)
				return Math.hypot(x - another.x, y - another.y) - getCollisionRadius() - another.getCollisionRadius() <=0;
			else
				return Math.hypot(x - another.x, y - another.y) >= another.getCollisionRadius() - getCollisionRadius();
		}
		else
			return Math.hypot(x - another.x, y - another.y) - getCollisionRadius() - another.getCollisionRadius() <=0;
	}*/


    public int getSquadId()
    {
        return squadId;
    }

    public void setSquadId(int squadId)
    {
        this.squadId = squadId;
    }

    public void setInventoryManager(InventoryManager inventoryManager)
    {
        mInventoryManager = inventoryManager;
    }

    public InventoryManager getInventoryManager()
    {
        return mInventoryManager;
    }

    public String getPlayerSkinColor() {return playerSkinColor;}

    public void setPlayerSkinColor(String playerSkinColor) {this.playerSkinColor = playerSkinColor;}

    public void setCanSendHeartBeats(boolean canSendHeartBeats)
    {
        this.canSendHeartBeats = canSendHeartBeats;
    }


    public void healByMedKit(String medkitBlueprint)
    {
        if (medkitBlueprint.equals(HP_100))
        {
            int health = Math.min(maxHealth, getCurrentHealth() + 100);
            setCurrentHealth(health);
            mInventoryManager.useItem(medkitBlueprint);
        }
        if (medkitBlueprint.equals(HP_60))
        {
            int health = Math.min(maxHealth, getCurrentHealth() + 60);
            setCurrentHealth(health);
            mInventoryManager.useItem(medkitBlueprint);
        }
        if (medkitBlueprint.equals(HP_30))
        {
            int health = Math.min(maxHealth, getCurrentHealth() + 30);
            setCurrentHealth(health);
            mInventoryManager.useItem(medkitBlueprint);
        }


    }

    //client-side method
    public void die(ParticlesPoolManager particlesPoolManager)
    {
        if (isBotSimulator || isServer){ return; }
        if (pit != null)
        {
            GameClient.getGame().getNetworkManager().sendPacket(new PitInteractionPacker(getId(), false, pit));
        }

        Camera gameCam = GameClient.getGame().getCamera();
        GameClient.getGame().getSoundManager().playSoundWithPanAndVolume(SoundManager.DEATH,
                new IoPoint(gameCam.position.x, gameCam.position.y), getPosition());
        deathEffect(particlesPoolManager);
    }

    public String getClientId()
    {
        return clientId;
    }

    public void setClientId(String clientId)
    {
        this.clientId = clientId;
    }

    public boolean isSuppressed()
    {
        return suppressed;
    }

    public void setSuppressed(boolean suppressed)
    {
        this.suppressed = suppressed;
    }

    public void setKillerId(String killerId)
    {
        this.killerId = killerId;
    }

    public String getKillerId()
    {
        return killerId;
    }

    public void setPositionBeforeInteraction(IoPoint positionBeforeInteraction)
    {
        this.positionBeforeInteraction = positionBeforeInteraction;
    }

    public IoPoint getPositionBeforeInteraction()
    {
        return positionBeforeInteraction;
    }

    public GroundType getPlayerGroundType()
    {
        return playerGroundType;
    }

    public void setPlayerGroundType(GroundType playerGroundType)
    {
        this.playerGroundType = playerGroundType;
        if (playerGroundType.equals(GroundType.MUD))
        { playerGroundTypeSound = SoundManager.FOOTSTEPS_MUD; }
        if (playerGroundType.equals(GroundType.SAND))
        { playerGroundTypeSound = SoundManager.FOOTSTEPS_SAND; }
        if (playerGroundType.equals(GroundType.STONE))
        { playerGroundTypeSound = SoundManager.FOOTSTEPS_STONE; }
        if (playerGroundType.equals(GroundType.WET))
        { playerGroundTypeSound = SoundManager.FOOTSTEPS_WET; }
    }

    public String getPlayerGroundTypeSound()
    {
        return playerGroundTypeSound;
    }

    public enum GroundType
    {
        MUD, WET, STONE, SAND
    }

    public Color getSquadColor() {
        return squadColor;
    }

    public void setSquadColor(Color squadColor) {
        this.squadColor = squadColor;
    }

    private boolean isStamina;
    public boolean isPlayerHaveStamina()
    {
        if(currentStamina > 0)
        {
            isStamina = true;
            return isStamina;
        }
        else{
            isStamina = false;
            return isStamina;
        }
    }

}
