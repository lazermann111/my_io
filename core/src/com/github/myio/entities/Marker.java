package com.github.myio.entities;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.github.myio.GameClient;

import static com.github.myio.StringConstants.MARKER;

/**
 * Client-side marker without syncing with server
 */

public class Marker extends Cell {


    public Marker(SpriteBatch batch, ShapeRenderer shapeRenderer, float x, float y, int radius, String id, int rotation) {
        super(batch, shapeRenderer, x, y, radius, id);
        rotationAngle = rotation;


        sprite = GameClient.cellAtlas.createSprite(MARKER);
        sprite.setPosition(x,y);
        sprite.setRotation(rotationAngle-90);
    }

    @Override
    public void rendererDraw(float delta) {

       sprite.draw(batch);
    }
}
