package com.github.myio.entities;

import com.github.myio.entities.projectiles.AbstractProjectile;


public interface Collidable {


    public boolean collides(Cell another);


    public boolean collides(AbstractProjectile another);

}
