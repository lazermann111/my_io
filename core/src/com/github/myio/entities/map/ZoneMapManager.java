package com.github.myio.entities.map;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Rectangle;
import com.github.myio.entities.IoPoint;
import com.github.myio.enums.MapEntityType;
import com.github.myio.tools.IoLogger;

import java.util.ArrayList;
import java.util.List;


public class ZoneMapManager   {

    private Rectangle redRectangle;
    private Rectangle yellowRectangle;


    private List<ZoneTile> greenTiles = new ArrayList<ZoneTile>();
    private List<ZoneTile> yellowTiles = new ArrayList<ZoneTile>();
    private List<ZoneTile> redTiles = new ArrayList<ZoneTile>();




    //client-side
    public void addTile(MapEntityCell t)
    {
        switch (t.getType()) {
            case GREEN_ZONE:
                greenTiles.add(new ZoneTile(t.getX(),t.getY(), t));
                break;
            case YELLOW_ZONE:
                yellowTiles.add(new ZoneTile(t.getX(),t.getY(), t));
                break;
            case RED_ZONE:
                redTiles.add(new ZoneTile(t.getX(),t.getY(), t));
                break;
        }
    }

    //server-side
    public void addTile(MapEntityCell t, float x, float y)
    {
        switch (t.getType()) {
            case GREEN_ZONE:
                greenTiles.add(new ZoneTile(x,y,t));
                break;
            case YELLOW_ZONE:
                yellowTiles.add(new ZoneTile(x,y,t));
                break;
            case RED_ZONE:
                redTiles.add(new ZoneTile(x,y,t));
                break;
        }
    }

    public void initZones()
    {
        if(!(yellowTiles.size() > 0) && !(yellowTiles.size() > 0) && Gdx.app != null) {
            IoLogger.log("Init zones", "No yellow and red tiles");
            return;
        }
        else if(!(yellowTiles.size() > 0) && Gdx.app != null) {
            IoLogger.log("Init zones", "No yellow tiles");
            return;
        }
        else if(!(redTiles.size() > 0) && Gdx.app != null) {
            IoLogger.log("Init zones", "No red tiles");
            return;
        }
        this.yellowRectangle =  initZone(yellowTiles);
        this.redRectangle = initZone(redTiles);
    }


    private Rectangle initZone( List<ZoneTile> tiles)
    {
        ZoneTile bottomLeftTile = bottomLeftTile(tiles);
        ZoneTile diagonalTile = diagonalTile(bottomLeftTile, tiles);

        if(bottomLeftTile == null || diagonalTile == null) return null;
        return new Rectangle(bottomLeftTile.x, bottomLeftTile.y, diagonalTile.x - bottomLeftTile.x, diagonalTile.y - bottomLeftTile.y);
    }

    public MapEntityType getPlayerZone(IoPoint playerPos)
    {
        if (redRectangle != null && yellowRectangle != null){
            if(redRectangle.contains(playerPos.x, playerPos.y)){
                return MapEntityType.RED_ZONE;
            }
            if(yellowRectangle.contains(playerPos.x, playerPos.y)) return MapEntityType.YELLOW_ZONE;
        }

        return MapEntityType.GREEN_ZONE;
    }
    private ZoneTile diagonalTile(ZoneTile origin, List<ZoneTile> tiles)
    {
        ZoneTile res = null;
        int dist = 0;


        for(ZoneTile t : tiles)
            if (origin.getPosition().distance(t.getPosition()) > dist)
            {
                dist = (int) origin.getPosition().distance(t.getPosition());
                res =t;
            }


        return res;
    }

    private ZoneTile bottomLeftTile(List<ZoneTile> tiles)
    {
        ZoneTile res = null;
        int minX = Integer.MAX_VALUE;
        int minY = Integer.MAX_VALUE;

        for(ZoneTile t : tiles)
            if (t.x <= minX && t.y <= minY)
            {
                minX = (int) t.x;
                minY = (int) t.y;
                res =t;
            }


        return res;
    }



    public class ZoneTile {

        public float x;
        public float y;
        public MapEntityCell mapEntityCell;

        public IoPoint getPosition()
        {
            return new IoPoint(x,y);
        }

        public ZoneTile(float x, float y, MapEntityCell mapEntityCell) {
            this.x = x;
            this.y = y;
            this.mapEntityCell = mapEntityCell;
        }
    }
}
