package com.github.myio.entities.map;


import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;
import com.github.myio.GameClient;
import com.github.myio.GameConstants;
import com.github.myio.entities.PlayerCell;
import com.github.myio.enums.MapEntityType;
import com.github.myio.networking.NetworkManager;
import com.github.myio.networking.packet.HeartBeatPacket;
import com.github.myio.networking.packet.ShieldInteractionPacket;
import com.github.myio.tools.IoLogger;

import static com.github.myio.GameConstants.SERVER_SEND_UPDATES_RATE;
import static com.github.myio.entities.PlayerCell.interpolation_const;

public class TacticalShield extends MapEntityCell {

    private static final java.lang.String TAG = "TacticalShield" ;
    private PlayerCell owner;
    public boolean inUse;
    private long lastHeartbeat;
    private Vector2 vector2 = new Vector2(0, 100);



    public TacticalShield(SpriteBatch batch, ShapeRenderer shapeRenderer, float posX, float posY, String id, MapEntityType type) {
        super(batch, shapeRenderer, posX, posY, id, type);
    }

    public void interact(PlayerCell playerCell)
    {
        interact(playerCell, GameClient.getGame().getNetworkManager());
    }

    public void interact(PlayerCell playerCell, NetworkManager networkManager)
    {

        if(owner != null && !owner.equals(playerCell))
        {
            //Gdx.app.error("SH", "interact already in use!");
            return;
        }
        networkManager.sendPacket(new ShieldInteractionPacket(playerCell.getId(), getId(), !inUse));
    }

    public void startInteractionClient(PlayerCell o)
    {
        //IoLogger.log("SH", "startInteractionClient");
        inUse= true;
        owner = o;
        //vector2 = owner.getForward();
        //vector2.setLength(100);

        owner.setAccelerationMultiplier(0.4f);
        owner.setRotationMultiplier(0.2f);
        o.shiled = this; // todo I really apologize for this code :D


    }

    @Override
    public float getInteractionRadius()
    {
        return super.getCollisionRadius()*1.1f;
    }

    public void finishInteractionClient(PlayerCell o)
    {


        //IoLogger.log("SH", "finishInteractionClient");
        inUse= false;
        o.shiled = null;

        if(owner != null)
        {
            vector2 = owner.getForward();
            vector2.setLength(100);
            owner.setRotationMultiplier(1f);
            owner.setAccelerationMultiplier(1f);
            owner = null;
        }
        else
            IoLogger.log(TAG, "finishInteractionClient null owner, maybe its just delayed packet");



    }

    public TacticalShield(SpriteBatch batch, ShapeRenderer shapeRenderer, float posX, float posY, String id, MapEntityType type, PlayerCell owner)
    {
        super(batch, shapeRenderer, posX, posY, id, type);
        //sprite.setOrigin();
    }

    public TacticalShield(SpriteBatch batch, ShapeRenderer shapeRenderer, float d, float e, int radius, String id, PlayerCell owner)
    {
        super(batch, shapeRenderer, d, e, radius, id);
        //sprite.setOrigin();
    }

    @Override
    public void rendererDraw(float delta) {

        if(owner == null)
        {
            sprite.setRotation(rotationAngle + 180);
            sprite.setPosition(x - sprite.getOriginX(), y - sprite.getOriginY());
            sprite.draw(batch);
            return;
        }
        vector2.setAngle(owner.getRotationInterpolated() - 90);
        //vector2 = owner.getForward();
        //vector2.setAngle(owner.getForward().angle() - 90);
        x = owner.x + vector2.x;
        y = owner.y + vector2.y;

        velocityX = x - xPrevious;
        velocityY = y - yPrevious;
        //rotationVelocity = owner.getRotationAngle() - rotationPrev;

        xInterploated = xPrevious+ velocityX * delta *interpolation_const* 1000/SERVER_SEND_UPDATES_RATE;
        yInterpoldated = yPrevious+ velocityY * delta *interpolation_const* 1000/SERVER_SEND_UPDATES_RATE;
      //  rotationInterpolated =rotationPrev + rotationVelocity * delta *interpolation_const* 1000/SERVER_SEND_UPDATES_RATE;

        sprite.setPosition(xInterploated - sprite.getOriginX(), yInterpoldated - sprite.getOriginY());

        sprite.setRotation(owner.getRotationInterpolated() + 180);
        //sprite.setRotation(owner.getRotationAngle() + 180);

        sprite.draw(batch);
        xPrevious = xInterploated;
        yPrevious = yInterpoldated;
        rotationPrev = rotationInterpolated;

        if(lastHeartbeat + GameConstants.HEARTBEAT_RATE > System.currentTimeMillis()) return;
        GameClient.getGame().getNetworkManager().sendPacket(new HeartBeatPacket(id, x, y, rotationAngle, HeartBeatPacket.TACTICAL_SHIELD));
        lastHeartbeat = System.currentTimeMillis();
    }

    public PlayerCell getOwner()
    {
        return owner;
    }

    @Override
    public String toString() {
        return "TacticalShield{" +
                "owner=" + owner +
                ", inUse=" + inUse +
                "} " + super.toString();
    }
}
