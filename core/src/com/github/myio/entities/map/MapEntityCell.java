package com.github.myio.entities.map;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.github.myio.GameClient;
import com.github.myio.entities.Cell;
import com.github.myio.entities.projectiles.AbstractProjectile;
import com.github.myio.enums.MapEntityType;
import com.github.myio.tools.IoLogger;

import java.util.Random;

import static com.github.myio.GameConstants.MAP_ENTITIES;
import static com.github.myio.GameConstants.isBotSimulator;
import static com.github.myio.GameConstants.isServer;

/**
 * Map entity, like  tree, rock, not necessarily collidable
 */

public class MapEntityCell extends Cell {

    private MapEntityType type;
    //private int drawableRadius;
   // private float collisionRadius;
    private MapEntityDetails details;
    private boolean breakable;
    private boolean specialEffect;
    private int durability; //actual only for breakable entities




    public MapEntityCell(SpriteBatch batch, ShapeRenderer shapeRenderer,
                         float posX, float posY, String id, MapEntityType type) {

        super(batch, shapeRenderer, posX, posY, 0, id);
        Random r = new Random();
        MapEntityDetails d = MAP_ENTITIES.get(type);
        //System.out.println(type);

        collisionRadius = d.getColissionsRadius();
        breakable = d.isBreakable();
        specialEffect = d.hasSpecialeffect();
        durability = 70;
        this.type = type;
        if(type.equals(MapEntityType.PIT)||type.equals(MapEntityType.PIT_TRAP2))
            setPit(true);
        details = d;



        if (!isServer && !isBotSimulator)
        {
            sprite = GameClient.cellAtlas.createSprite(d.getTextureVariations()[ r.nextInt(d.getTextureVariations().length)]);
            sprite.setSize(sprite.getWidth(), sprite.getHeight());
            //collisionRadius = (sprite.getWidth() + sprite.getHeight()) / 4 + (sprite.getWidth() + sprite.getHeight()) / 4 * 10f/100f;
            if(this.type.equals(MapEntityType.BUSH)) {
                sprite.setOrigin(sprite.getWidth()/2, sprite.getHeight()/2);
                sprite.setRotation(90);
            }
            else if(this.type.equals(MapEntityType.SPIDER_WEB)) {
                collisionRadius /= 2;
            }
            sprite.setPosition(x - sprite.getWidth()/2, y - sprite.getHeight()/2);
            switch (type) {
                case GREEN_ZONE:
                case YELLOW_ZONE:
                case RED_ZONE:
                    GameClient.getGame().getZoneMapManager().addTile(this);
                    break;
            }

        }
    }

    public MapEntityCell(SpriteBatch batch, ShapeRenderer shapeRenderer, float d, float e, int radius, String id) {
        super(batch, shapeRenderer, d, e, radius, id);
    }

    public MapEntityCell() {

    }

    public float getCollisionRadius() {
        return collisionRadius;
    }

    @Override
    public void rendererDraw(float delta) {
		//batch.begin();
		sprite.draw(batch);
		//batch.end();
    }

    public MapEntityType getType() {
        return type;
    }


    public boolean isObstacle() {
        return details.isObstacle();
    }


    public boolean isBreakable() {
        return breakable;
    }

    public void setBreakable(boolean breakable) {
        this.breakable = breakable;
    }

    public boolean hasSpecialEffect(){
        return specialEffect;
    }
    public int getDurability() {
        return durability;
    }

    @Override
    public float getInteractionRadius()
    {
        return super.getInteractionRadius()*1.1f;
    }

    @Override
    public boolean collides(AbstractProjectile another) {
        if(!isServer) {
            if (GameClient.getGame().getLocalPlayer().isInsidePit()) {
                if(Math.hypot(x - another.getCurrentPosition().getX(), y - another.getCurrentPosition().getY()) >= getCollisionRadius() - another.radius)
                    IoLogger.log("Collide: " + getType().name());
                //return false;
                return Math.hypot(x - another.getCurrentPosition().getX(), y - another.getCurrentPosition().getY()) >= getCollisionRadius() - another.radius;
            } else
                return Math.hypot(x - another.getCurrentPosition().getX(), y - another.getCurrentPosition().getY()) - getCollisionRadius() - another.radius <= 0;
        }
        else {
            if(getPlayersId().contains(another.shooterId))//if inside pit
                return Math.hypot(x - another.getCurrentPosition().getX(), y - another.getCurrentPosition().getY()) >= getCollisionRadius() - another.radius;
            else
                return Math.hypot(x - another.getCurrentPosition().getX(), y - another.getCurrentPosition().getY()) - getCollisionRadius() - another.radius <= 0;
        }
    }

    public boolean collidesFlashPit(AbstractProjectile another) {
        return Math.hypot(x - another.getCurrentPosition().getX(), y - another.getCurrentPosition().getY()) - getCollisionRadius() - another.radius <= 0;
    }

    public void setDurability(int durability) {
        this.durability = durability;
    }


    @Override
    public String toString() {
        return "MapEntityCell{" +
                "type=" + type +
                "} " + super.toString();
    }
}
