package com.github.myio.entities.map;


import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.github.myio.dto.CapturablePointDto;
import com.github.myio.enums.MapEntityType;

import static com.github.myio.GameConstants.CONQUEST_BLUE_TEAM_ID;
import static com.github.myio.GameConstants.CONQUEST_CAPTURABLE_POINT_INTERACTION_RADIUS;
import static com.github.myio.GameConstants.CONQUEST_RED_TEAM_ID;

/**
  * Class that unions map entity representation and CapturablePoint logic
 **/

public class CapturablePointClient extends MapEntityCell
{

    // data that we receiving from server on each frame
    public CapturablePointDto pointDto;

    public CapturablePointClient(SpriteBatch batch, ShapeRenderer shapeRenderer, float posX, float posY, String id, MapEntityType type)
    {
        super(batch, shapeRenderer, posX, posY, id, type);
    }



    @Override
    public void rendererDraw(float delta)
    {

        sprite.draw(batch);

    }

    public void drawPointRadius(float delta)
    {
        if(pointDto == null) return;


        if(pointDto.owner == -1)
            shapeRenderer.setColor(Color.GRAY);

        if(pointDto.owner == CONQUEST_RED_TEAM_ID)
            shapeRenderer.setColor(Color.RED);

        if(pointDto.owner == CONQUEST_BLUE_TEAM_ID)
            shapeRenderer.setColor(Color.BLUE);


        //point contested
        if(pointDto.capturingTeam != -1)
            shapeRenderer.setColor(Color.CYAN);

        shapeRenderer.arc(x, y, CONQUEST_CAPTURABLE_POINT_INTERACTION_RADIUS, 0, pointDto.owner == -1 ? pointDto.capturingProgress * 3.6f : 360);
    }


    @Override
    public String toString()
    {
        return "CapturablePointClient{" +
                "pointDto=" + pointDto!=null? pointDto.toStringBase() :"" +
                "} " + super.toString();
    }
}
