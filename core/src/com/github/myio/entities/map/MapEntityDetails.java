package com.github.myio.entities.map;


public class MapEntityDetails {


    private String[] textureVariations;


    //private int drawableRadius;
    private float colissionsRadius;
    private boolean isObstacle;
    private boolean breakable; // like healing stone. we need to apply effect on collision with player
    private boolean specialEffect;


    public MapEntityDetails(  String[]   textureVariations,int colissionsRadius, boolean isObstacle, boolean breakable,boolean specialEffect ) {
        this.textureVariations = textureVariations;
        //this.drawableRadius = drawableRadius;
        this.isObstacle = isObstacle;
        this.breakable = breakable;
        this.specialEffect = specialEffect;
        this.colissionsRadius = colissionsRadius + colissionsRadius*10f/100f;
    }

    public float getColissionsRadius() {
        return colissionsRadius;
    }

    public String[] getTextureVariations() {
        return textureVariations;
    }

//    public int getRadius() {
//        return drawableRadius;
//    }

    public boolean isObstacle() {
        return isObstacle;
    }



    public boolean isBreakable() {
        return breakable;
    }
    public boolean hasSpecialeffect() {
        return specialEffect;
    }
}
