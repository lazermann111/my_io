package com.github.myio.desktop;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.github.czyzby.websocket.CommonWebSockets;
import com.github.myio.GameClient;

public class DesktopLauncher {
    public static void main(final String[] arg) {

//        //FULLSCREEN GAME ON DESKTOP
//        LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
//        config.width = LwjglApplicationConfiguration.getDesktopDisplayMode().width;
//        config.height = LwjglApplicationConfiguration.getDesktopDisplayMode().height;
//        config.fullscreen = true;
//        new LwjglApplication(new GameClient(), config);


        // Initiating web sockets module: 
        CommonWebSockets.initiate();
        createApplication();
    }

    private static Application createApplication() {
        final LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
        config.width = 1080;
        config.height = 720;
        config.useGL30 = true;
        return new LwjglApplication(new GameClient(), config);
    }
}
